#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
#
# EXAMPLE
#
# Import fem2ufo module
import fem2ufo as f2u
#
#
# ----------------------------------------------------------------------------------
#                                 FE Model Manipulation
# ----------------------------------------------------------------------------------
#
# Initiate model object
f2u_model = f2u.SESAM_model()

# Define path to the working directory
path = r'C:\Temp\seismic_demo'
f2u_model.directory_path(path)

# Define input units (mandatory)
f2u_model.units(force='N', length='mm', mass='kg')

# input moddel data
f2u_model.geometry('jacket_t1.fem') # mandatory
f2u_model.foundation('jacket_GENSOD.LIS')
#
#
# converting load to mass
f2u_model.load_to_mass('CASE103_',  'CASE104_', 'CASE105_',
                       'CASE106_',  'CASE107_', 'CASE108_',
                       'CASE109_', factor=0.95, option='node')
#
#
# ----------------------------------------------------------------------------------
#                    Time Histories Database Manipulation
# ----------------------------------------------------------------------------------
#
# set database model
database_1 = f2u.TimeSeries()
#
path = r'C:\Temp\seismic_demo\time_history'
database_1.directory_path(path)
#
# Read excel file
file = '1000yr_TH.xlsx'
sheet = 'test'
database_1.excel_file(file, sheet)
#
database_1.units(acceleration='foot/sec**2')
# define first row to read
database_1.first_row_to_read = 6
#
# Read Data
columns = ['A', 'B', 'C', 'D']
names = ['time', 'accel_x', 'accel_y', 'accel_z']
database_1.get_columns(columns, names)
#
# Set time histories
database_1.set_time_serie(name='1000yr_TH_accel_x',
                          time='time',
                          acceleration='accel_x')
#
database_1.set_time_serie(name='1000yr_TH_accel_y',
                          time='time',
                          acceleration='accel_y')
#
database_1.set_time_serie(name='1000yr_TH_accel_z',
                          time='time',
                          acceleration='accel_z')
#
#database_1.plot_time_serie('1000yr_TH_accel_x')
#
#database_1.plot_time_serie('1000yr_TH_accel_x')
#
#database_1.plot_response_sdof('1000yr_TH_accel_x', period=1.5)
#
#
# ----------------------------------------------------------------------------------
#                                  Conversion Process
# ----------------------------------------------------------------------------------
#
# Start conversion
usfos = f2u.USFOS_conversion()
#
# Input f2u concept model
usfos.model(f2u_model)
# Define path to the working directory
path = r'C:\Temp\seismic_demo\usfos_model'
usfos.directory_path(path)
#
usfos.element_group_assign_nonstructural('RISER_1')
usfos.element_group_assign_nonstructural('RISER_2')
usfos.element_group_assign_nonstructural('R1_suppt')
usfos.element_assign_nonstructural(654)
#
usfos.element_group_assign_linear('R2_suppt')
usfos.element_assign_linear(278)
#
# Analysis type
usfos.analysis('seismic')

#
# Input time histories
#
usfos.time_history(name='time_history_1', 
                   number=1001,
                   time_serie = database_1,
                   time_serie_name='1000yr_TH_accel_x')
#
usfos.time_history(name='time_history_2', 
                   number=1002,
                   time_serie = database_1,
                   time_serie_name='1000yr_TH_accel_y')
#
usfos.time_history(name='time_history_3', 
                   number=1003,
                   time_serie = database_1,
                   time_serie_name='1000yr_TH_accel_z')
#
#
#
usfos.assign_soil_displacement(time_history_name='time_history_1',
                               dof='x',
                               factor=0.10,
                               depth_below_mudline=100000) # length units in mm
#
usfos.assign_soil_displacement(time_history_name='time_history_2',
                               dof='y',
                               factor=0.10,
                               depth_below_mudline=100000) # length units in mm
#
usfos.assign_soil_displacement(time_history_name='time_history_3',
                               dof='z',
                               factor=0.10,
                               depth_below_mudline=100000) # length units in mm
#
#
# 
# print ufo model file
usfos.print_model()
# print ufo control file
usfos.print_control()
# print time histories in usfos format
usfos.print_time_history(file_name='10_example',
                         time_history_case='displacement')
#
print('')
print('------------------------ END -----------------------')
#ExitProg = input('Press Enter Key To Continue')