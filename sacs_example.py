#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
#
# EXAMPLE
#
## Import fem2ufo module
import fem2ufo as f2u
#
# ----------------------------------------------------------------------------------
#                                 FE Model Manipulation
# ----------------------------------------------------------------------------------
#
## Initiate model object
model = f2u.SACS_model()

## Define path to the working directory
path = r"D:\python\f2u3\examples\sacs"
#path = r"D:\Chava\Documents\sacs\okume\compressor"
path = r"D:\Chava\Documents\sacs\okume\TLP"
#path = r"/media/chava/Backup/python/f2u_examples/sacs"
model.directory_path(path)


## input sacs model data
model.geometry('foxtrot11.inp') # mandatory
#model.geometry('Geometry_sac.inp') # mandatory
#model.loading('Inplace_sea.inp')

## Optional
## Foundation file option
#model.foundation('psiinp.inp')

## Optional
## Metocean file option
#_name_file = 'seainp.inp'
# design_condition='operating
#model.metocean(_name_file, design_condition)
# 
#
#
# ----------------------------------------------------------------------------------
#                                  Conversion Process
# ----------------------------------------------------------------------------------
#

usfos = f2u.USFOS_conversion()
usfos.model(model)
# print ufo model file
usfos.print_model()
# print ufo control file
usfos.print_control()


## Convert sacs model to GeniE format
#GeniE_js = f2u.GeniE_conversion()
#GeniE_js.model(model)
## let fem2ufo to traslate cone sections
#GeniE_js.print_model(cone_auto=True)
## Optional convert load combination and foundation data in GeniE format
#GeniE_js.print_analysis()
#GeniE_js.print_load_combinations()


## Print out load combination and foundation data in LoCmatrix format
#model_csv = f2u.CSV_conversion()
#model_csv.model(model)
#model_csv.print_load_combination()
#model_csv.print_foundation()

#
print('')
print('------------------------ END -----------------------')
ExitProg = input('Press Enter Key To Continue')