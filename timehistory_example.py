#
# import fem2ufo
import fem2ufo as f2u
#
#

#
# Provide path to data base
#
path = r"D:\svort\Documents\python\f2u_examples\time_history"
path = r'D:\python\f2u_examples\time_history'
#path = r"C:\Temp\Temp\python\f2u30\examples\time_history"
#path = r'C:\Temp\seismic_demo\time_history'
#
# ---------------------------
# excel input format example
# --------------------------
#
# set time serie database model
database_1 = f2u.TimeSeries()
database_1.directory_path(path)
#
# Accelreration data units must be provided ( default in SI g)
database_1.units(acceleration='foot/sec**2')
#
# Excel file must be provide in xlsx format and sheet name with the time series data
database_1.excel_file(file='1000yr_TH.xlsx', sheet='test')
#
# Option 1
# Define rows and colums to be read. Colums must have names
#
database_1.first_row_to_read = 6
#
database_1.get_columns(colums=['A', 'B', 'c', 'd'], 
                       names=['time', 'accel_x', 'accel_y', 'accel_z'])
#
# setting time series from colum's names
#
database_1.set_time_serie(name='1000yr_TH_accel_x',
                          time='time',
                          acceleration='accel_x')
#
database_1.set_time_serie(name='1000yr_TH_accel_y',
                          time='time',
                          acceleration='accel_y')
#
database_1.set_time_serie(name='1000yr_TH_accel_z',
                          time='time',
                          acceleration='accel_z')
#
# Plotting time series
#
database_1.plot_time_serie('1000yr_TH_accel_x')
#
database_1.plot_response_sdof('1000yr_TH_accel_x', period=1.5)
#
database_1.plot_spectral_density('1000yr_TH_accel_x')
#
# response spectra can take few seconds
database_1.plot_response_spectra('1000yr_TH_accel_y', Tmax=4)
#
# writing time series columns to a csv file
#
database_1.write_csv(file_name='database_1_csv', 
                     column_names=['time', 'accel_x', 'accel_y', 'accel_z'])
#
#
# -------------------------
# csv input format example
# -------------------------
#
database_2 = f2u.TimeSeries()
database_2.directory_path(path)
# 
# For this case, accelerations are in g units but we are 
# also reading velocity and displacement series in foot units
database_2.units(length='foot', acceleration='g')
#
file = '10K_TH.csv'
database_2.csv_file(file)
#
# Option 2
# set time serie by defining the rows and columns directly
#
database_2.read_time_serie(name='ALE_10K_TH', 
                           time='a', 
                           acceleration='b',
                           velocity='c',
                           displacement='d',
                           first_row_to_read=4)
#
# Writing time serie to a csv file
#
database_2.write_csv(file_name='database_2_csv', 
                     time_history_name='ALE_10K_TH')
#
#
# -------------------------
# text input format example
# -------------------------
#
database_3 = f2u.TimeSeries()
database_3.directory_path(path)
# 
# For this case, accelerations are in g units
database_3.units(acceleration='g')
#
file = 'artificial.dat'
database_3.text_file(file, file_format='dat')
#
# Set another time serie directly
#
database_3.read_time_serie(name='artificial_test', 
                           time='a', 
                           acceleration='b',
                           first_row_to_read=4)
#
database_3.plot_time_serie('artificial_test')
#
# ----------------------------------------
# Time series conversion to usfos format
# ----------------------------------------
#
usfos = f2u.USFOS_conversion()
#
# Data from excel
usfos.time_history(name='lev_1', number=1001,
                   time_serie=database_1,
                   time_serie_name='1000yr_TH_accel_x')
#
usfos.time_history(name='lev_2', number=1002,
                   time_serie=database_1,
                   time_serie_name= '1000yr_TH_accel_y')
#
usfos.time_history(name='lev_3', number=1003,
                   time_serie=database_1,
                   time_serie_name='1000yr_TH_accel_z')
#
# Data from csv file
usfos.time_history(name='lev_4', number=1004,
                   time_serie=database_2,
                   time_serie_name='ALE_10K_TH')
#
# Data from text file
usfos.time_history(name='lev_5', number=1005,
                   time_serie=database_3,
                   time_serie_name='artificial_test')
#
# Print time histories in usfos format
usfos.print_time_history(file_name='10_example')
#
#
print('')
print('------------------------ END -----------------------')
ExitProg = input('Press Enter Key To Continue')
#
#
