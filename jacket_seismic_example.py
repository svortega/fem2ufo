#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
#
# EXAMPLE
#
# Import fem2ufo module
#
import fem2ufo as f2u
#
#
# ----------------------------------------------------------------------------------
#                                 FE Model Manipulation
# ----------------------------------------------------------------------------------
#
# Initiate model object
f2u_model = f2u.SESAM_model()

# Define path to the working directory
# mac
#path = '/Volumes/ANALYSIS/Python/fem2ufo_3/examples/jacket'
# windows
path = r'D:\svort\Documents\python\f2u_examples\jacket'
path = r'D:\python\f2u_examples\jacket'
#path = r'C:\Temp\Temp\python\f2u30\examples\jacket'

f2u_model.directory_path(path)

# Define input units (mandatory)
f2u_model.units(force='N', length='mm', mass='kg')

# input moddel data
f2u_model.geometry('jacket_t1.fem') # mandatory
f2u_model.loading('jacket_l1.fem')
f2u_model.foundation('jacket_GENSOD.LIS')
f2u_model.metocean('jacket_wajac.inp')

#
# converting load to mass
#
#f2u_model.load_to_mass(1, 2, 100, factor=1.0, option='node')
#f2u_model.load_to_mass(101, factor=1.2, option='member')
#
#
#
# ----------------------------------------------------------------------------------
#                    Time Histories Database Manipulation
# ----------------------------------------------------------------------------------
#
path = r'D:\python\f2u_examples\time_history'
# set database model
database_1 = f2u.TimeSeries()

#path = r'D:\svort\Documents\python\f2u_examples\jacket'
database_1.directory_path(path)
#
# Read excel file
file = '1000yr_TH.xlsx'
sheet = 'test'
database_1.excel_file(file, sheet)
#
database_1.units(acceleration='foot/sec**2')
# define first row to read
database_1.first_row_to_read = 6
#
# Read Data
columns = ['A', 'B', 'C', 'D']
names = ['time', 'accel_x', 'accel_y', 'accel_z']
database_1.get_columns(columns, names)
#
# Set time histories
database_1.set_time_serie(name='1000yr_TH_accel_x',
                          time='time',
                          acceleration='accel_x')
#
database_1.set_time_serie(name='1000yr_TH_accel_y',
                          time='time',
                          acceleration='accel_y')
#
database_1.set_time_serie(name='1000yr_TH_accel_z',
                          time='time',
                          acceleration='accel_z')
#
#database_1.plot_time_serie('1000yr_TH_accel_x')
#
#
#
database_2 = f2u.TimeSeries()

database_2.directory_path(path)
#
database_2.units(length='foot', acceleration='g')
#
database_2.csv_file(file='10K_TH.csv')
#
#
database_2.read_time_serie(name='ALE_10K_TH', 
                           time='a', 
                           acceleration='b',
                           velocity='c',
                           displacement='d',
                           first_row_to_read=4)
#
#
#
# ----------------------------------------------------------------------------------
#                                  Conversion Process
# ----------------------------------------------------------------------------------
#
# Start conversion
usfos = f2u.USFOS_conversion()
#
# Input f2u concept model
usfos.model(f2u_model)

# Analysis type
usfos.analysis('seismic')

#
# Input time histories
#
usfos.time_history(name='time_history_1', 
                   number=1001,
                   time_serie = database_1,
                   time_serie_name='1000yr_TH_accel_x')
#
usfos.time_history(name='time_history_2', 
                   number=1002,
                   time_serie = database_1,
                   time_serie_name='1000yr_TH_accel_y')
#
usfos.time_history(name='time_history_3', 
                   number=1003,
                   time_serie = database_1,
                   time_serie_name='1000yr_TH_accel_z')
#
#
#
usfos.assign_soil_displacement(time_history_name='time_history_1',
                               dof='x',
                               factor=0.3048,
                               depth_below_mudline=100000) # length units in mm
#
usfos.assign_soil_displacement(time_history_name='time_history_2',
                               dof='y',
                               factor=0.3048,
                               depth_below_mudline=100000) # length units in mm
#
usfos.assign_soil_displacement(time_history_name='time_history_3',
                               dof='z',
                               factor=0.3048,
                               depth_below_mudline=100000) # length units in mm
#
#
#
#
# Input time histories
#
usfos.time_history(name='time_history_4', 
                   number=1004,
                   time_serie = database_2,
                   time_serie_name='ALE_10K_TH')
#
#
#
usfos.assign_node_displacement(time_history_name='time_history_4',
                               node_number = 21,
                               dof='x')
#
usfos.assign_node_displacement(time_history_name='time_history_4',
                               element_name = 'Bm100',
                               element_end = 1,
                               dof='y')
#
#
usfos.assign_pile_subsidence(time_history_name='time_history_4',
                             pile_name = 'Pile10',
                             dof='z')
# 
# print ufo model file
usfos.print_model()
# print ufo control file
usfos.print_control()
# print time histories in usfos format
usfos.print_time_history(file_name='10_example',
                         time_history_case='displacement')
#
print('')
print('------------------------ END -----------------------')
#ExitProg = input('Press Enter Key To Continue')