#
#
import fem2ufo as f2u
#
#

path = r"C:\Temp\Python\f2u_dev\examples\weight_report"


# set database model
wt_db = f2u.WEIGHT_database()

wt_db.directory_path(path)

wt_db.units(force='kN', length='m',
            mass='tonne')
#
# 
#
file = 'weight_data_1.csv'
wt_db.database(file,
               file_format = 'csv')
#
wt_db.set_database_row(start = 2,
                       end = 21)
#
#
wt_db.set_weight_component('Topside')
#
# wt_db.set_weight_item(discipline = 'structural')
#
# wt_db.set_coordinate_system(system = 'global')
#
# 
#
wt_db.assign_discipline_code('mechanical', 'MC')
#
#
wt_db.read_columns(tag = 'M',
                   description = 'C',
                   module = 'I',
                   discipline = 'H',
                   status = 'A')
# 
wt_db.read_columns(mass = 'permanent',
                   magnitud = 'R',
                   CoG_east = 'N',
                   CoG_north = 'O',
                   CoG_vertical = 'P')
# 
wt_db.read_columns(mass = 'variable',
                   magnitud = 'R-Z',
                   CoG_east = 'AA',
                   CoG_north = 'AB',
                   CoG_vertical = 'AC')
#
wt_db.get_database()
#
#
print('ok')
#