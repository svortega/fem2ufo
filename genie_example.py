#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
#
#
import fem2ufo as f2u
#
#
#
# ----------------------------------------------------------------------------------
#                                GeniE Model Manipulation
# ----------------------------------------------------------------------------------
#

## Initiate model object
f2u_model = f2u.GENIE_model()

path = r"C:\Temp\TSW\TT\Shanice"
f2u_model.directory_path(path)


## Input model data. 
## This is usually a clean js model generated in GeniE with load combinations
## and foundation data
f2u_model.js_model('Mahogany_B_Walkway_Reduced_Sections_7Jun19_3.js')

#
#
# ----------------------------------------------------------------------------------
#                                  Conversion Process
# ----------------------------------------------------------------------------------
#

model_csv = f2u.CSV_conversion()
model_csv.model(f2u_model)
model_csv.print_load_combination()
#model_csv.print_foundation()

print('')
print('------------------------ END -----------------------')
ExitProg = input('Press Enter Key To Continue')