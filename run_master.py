# 
# Copyright (c) 2009-2018 fem2ufo
# 

import fem2ufo as f2u

if __name__ == "__main__":
    #
    # --------------------------------------------------------------------
    #                       Read master.fem input file 
    # --------------------------------------------------------------------
    #
    print('')
    print('------------------- INPUT MASTER -------------------')
    master_file = input('--- Enter Master Input File: ')
    #
    #master_file = 'master_shim'
    #master_file = 'masterjacket_seismic'
    #master_file = 'masterBeam_point'
    #master_file = 'master_Flambouyant'
    #
    #master_file = 'master_chr1'
    #master_file = 'master_tt'
    #
    # --------------------------------------------------------------------
    #                        Master File Manipulation
    # --------------------------------------------------------------------
    #
    f2u.master_input(master_file)
    # 
    #
    print('')
    print('------------------------ END -----------------------')
    ExitProg = input('Press Enter Key To Continue')
    #
    #