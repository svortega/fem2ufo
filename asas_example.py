#
import fem2ufo as f2u
#
#
# ----------------------------------------------------------------------------------
#                                 FE Model Manipulation
# ----------------------------------------------------------------------------------
#
#

## path to the asas model folder
path = r"D:\python\f2u_examples\East_Azeri"

## set model
f2u_model = f2u.ASAS_model()
f2u_model.directory_path(path)
f2u_model.global_units(force='kN', length='m')

## input Model
geometry = 'jack.DAT'
f2u_model.substructure(geometry,
                       super_element=False,
                       conductor_bell=True,
                       general_section=True)

## Loading input
f2u_model.loading('jackloads.dat')


## material input
f2u_model.Fy('yieldmate.dat') # main material must be input first
f2u_model.Fy('yieldprops.dat')
f2u_model.Fy('yieldpropspua.dat')
f2u_model.Fy('yieldpropspub.dat')
f2u_model.Fy('yieldpropspuc.dat')
f2u_model.Fy('yieldpropspud.dat')

## joint input
f2u_model.joints('pujointa.dat')
f2u_model.joints('pujointb.dat')
f2u_model.joints('pujointc.dat')
f2u_model.joints('pujointd.dat')

## Code check input
f2u_model.code_check('bm_effleng_jack.dat')
f2u_model.code_check('bm_effleng_blp.dat')

## load combination
data_file = 'loadcombjacketopr.dat'
design_condition ='operating'
combination_name = 'Oper'
f2u_model.load_combination(data_file, 
                           design_condition,
                           combination_name)

## Foundation input
soil = 'spl.dat'
f2u_model.foundation(soil)

## Hydrodynamic input
meto4 = 'opr-wv.dat'
f2u_model.metocean(meto4, combination_name)

#
#
# ----------------------------------------------------------------------------------
#                                  Conversion Process
# ----------------------------------------------------------------------------------
#

GeniE_js = f2u.GeniE_conversion()
GeniE_js.model(f2u_model)
## define a prefix so all superelements can be assembled in GeniE
## define cone sections automatic
GeniE_js.print_model(subfix='jacket', cone_auto=True)
GeniE_js.print_analysis()
GeniE_js.print_load_combinations()
#
#
print('')
print('------------------------ END -----------------------')
ExitProg = input('Press Enter Key To Continue')