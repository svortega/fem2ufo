#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
#
# EXAMPLE
#
import fem2ufo as f2u
#
#
# ----------------------------------------------------------------------------------
#                    Time Histories Database Manipulation
# ----------------------------------------------------------------------------------
#
# set database model
database_1 = f2u.TimeSeries()

path = r'C:\Temp\TSW\AGT\Chirag_1\TH'
path = r'/media/chava/Backup/python/f2u_examples/time_history'
path = r"D:\python\f2u_examples\time_history"

database_1.directory_path(path)
#
# Read excel file
file = 'Chirag_CTR_12_ALE_BE_20EW.xlsx'
sheet = 'Acceleration'
database_1.excel_file(file, sheet)
#
database_1.units(acceleration='metre/sec**2')
# define first row to read
database_1.first_row_to_read = 3
#
# Read Data
columns = ['A', 'AA', 'BB', 'AC']
names = ['time', 'accel_AA', 'accel_BB', 'accel_AC']
database_1.get_columns(columns, names)
#
# Set time histories
database_1.set_time_serie(name='250yr_TH_accel_AA',
                          time='time',
                          acceleration='accel_AA')
#
database_1.set_time_serie(name='250yr_TH_accel_BB',
                          time='time',
                          acceleration='accel_BB')
#
database_1.set_time_serie(name='250yr_TH_accel_AC',
                          time='time',
                          acceleration='accel_AC')
#
#
# plot series as-is
database_1.plot_time_serie('250yr_TH_accel_AC')
database_1.plot_response_sdof('250yr_TH_accel_AC', period=1.5)
#
# add 20 seconds at the end of the time series with acceleration value = 0
#database_1.time_expand(time_history_name='250yr_TH_accel_AC', 
#                       time=20, value=0)
#
# plot series with extended time
#database_1.plot_time_serie('250yr_TH_accel_AC')
#database_1.plot_response_sdof('250yr_TH_accel_AC', period=1.5)
#
# Plot response spectra and dump sdof response data in csv files (acc, vel and disp)
database_1.plot_response_spectra(series_name='250yr_TH_accel_AC',
                                 Tmax=10, dt=0.10, damping=0.05,
                                 print_file=True)