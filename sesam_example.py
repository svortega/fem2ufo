#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
#
#
import fem2ufo as f2u
#
#
#
# ----------------------------------------------------------------------------------
#                                GeniE Model Manipulation
# ----------------------------------------------------------------------------------
#

# Initiate model object
f2u_model = f2u.SESAM_model()

# Define path to the working directory
# mac
#path = '/Volumes/ANALYSIS/Python/fem2ufo_3/examples/jacket'
# windows
path = r'D:\python\f2u3\examples\jacket'
#path = r'C:\Temp\TSW\TT\Immortelle\usfos'

f2u_model.directory_path(path)

# Define input units (mandatory)
f2u_model.units(force='N', length='mm', mass='kg')

# input moddel data
f2u_model.geometry('jacket_T1.fem') # mandatory
f2u_model.loading('jacket_L1.fem')
#
#foundation_units = ['millimetre', '1*kilogram', None, None, 'meganewton', None]
f2u_model.foundation('jacket_GENSOD.LIS') 
                 #foundation_units=foundation_units)
#
f2u_model.metocean('jacket_wajac.inp')
#
# ----------------------------------------------------------------------------------
#                                  Conversion Process
# ----------------------------------------------------------------------------------
#
model_csv = f2u.CSV_conversion()
model_csv.model(f2u_model)
model_csv.units(force='N', length='m')
model_csv.print_load_combination()
model_csv.print_foundation()
#
GeniE_js = f2u.GeniE_conversion(find_overlap=False)
GeniE_js.model(f2u_model)
GeniE_js.print_model(subfix="")
#GeniE_js.print_analysis()
#GeniE_js.print_load_combinations()
#

print('')
print('------------------------ END -----------------------')
ExitProg = input('Press Enter Key To Continue')