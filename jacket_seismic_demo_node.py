#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
#
# EXAMPLE
#
# Import fem2ufo module
import fem2ufo as f2u
#
#
# ----------------------------------------------------------------------------------
#                                 FE Model Manipulation
# ----------------------------------------------------------------------------------
#
# Initiate model object
f2u_model = f2u.SESAM_model()

# Define path to the working directory
path = r'C:\Temp\seismic_demo'
f2u_model.directory_path(path)

# Define input units (mandatory)
f2u_model.units(force='N', length='mm', mass='kg')

# input moddel data
f2u_model.geometry('jacket_t1.fem') # mandatory
#f2u_model.foundation('jacket_GENSOD.LIS')
#
#
# converting load to mass
f2u_model.load_to_mass('CASE103_',  'CASE104_', 'CASE105_',
                       'CASE106_',  'CASE107_', 'CASE108_',
                       'CASE109_', factor=0.95, option='node')
#
#
#
# ----------------------------------------------------------------------------------
#                    Time Histories Database Manipulation
# ----------------------------------------------------------------------------------
#
# set database model
database_2 = f2u.TimeSeries()

path = r'C:\Temp\seismic_demo\time_history'
database_2.directory_path(path)
#
database_2.units(length='foot', acceleration='g')
#
database_2.csv_file(file='10K_TH.csv')
#
#
database_2.read_time_serie(name='ALE_10K_TH', 
                           time='a', 
                           acceleration='b',
                           velocity='c',
                           displacement='d',
                           first_row_to_read=4)
#
#
#
# ----------------------------------------------------------------------------------
#                                  Conversion Process
# ----------------------------------------------------------------------------------
#
# Start conversion
usfos = f2u.USFOS_conversion()
#
# Input f2u concept model
usfos.model(f2u_model)
# Define path to the working directory
path = r'C:\Temp\seismic_demo\usfos_model_node'
usfos.directory_path(path)
#
usfos.element_group_assign_nonstructural('RISER_1')
usfos.element_group_assign_nonstructural('RISER_2')
usfos.element_group_assign_nonstructural('R1_suppt')
usfos.element_assign_nonstructural(654)
#
usfos.element_group_assign_linear('R2_suppt')
usfos.element_assign_linear(278)

# Analysis type
usfos.analysis('seismic')

#
#
# Input time histories
#
usfos.time_history(name='time_history_4', 
                   number=1004,
                   time_serie = database_2,
                   time_serie_name='ALE_10K_TH')
#
#
#
usfos.assign_node_displacement(time_history_name='time_history_4',
                               node_number = [16, 47, 43, 318, 317, 209, 21, 210],
                               dof='x')
#
# 
# print ufo model file
usfos.print_model()
# print ufo control file
usfos.print_control()
# print time histories in usfos format
usfos.print_time_history(file_name='10_example',
                         time_history_case='displacement')
#
print('')
print('------------------------ END -----------------------')
#ExitProg = input('Press Enter Key To Continue')