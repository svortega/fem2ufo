# file abaout.py
# 
# Copyright (c) 2013 fem2ufo
# 
#
#
#
#
# --------------------------------------------------------------------------------------
# PROGRAM ABAOUT         : 
#                          
# DATE                   : Version 1.0, DEC 2012
# --------------------------------------------------------------------------------------
#
#   VERSION  BY    DESCRIPTION
#   -------  ---   ---------------------------------------------------------------------
#
#    1.0-0  svo    Module to
#
# --------------------------------------------------------------------------------------
#
import math
#
#
def findElemGroups(section, element, material):
    #
    # import copy
    #
    # _secDic = {_sec.name : [_elem.number
    _secDic = {_sec.number: [_elem.name
                             for _elem in element.values()
                             if _sec.name == _elem.section[0].name]
               for _sec in section.values()}
    # print(_dic)
    # _matDic = {_mat.name : [_elem.number
    _matDic = {_mat.number: [_elem.name
                             for _elem in element.values()
                             if _mat.name == _elem.material[0].name]
               for _mat in material.values()}

    #
    _elset = {}
    for secKey, secValue in _secDic.items():
        for matKey, matValue in _matDic.items():
            _nono = {matKey: [secElem
                              for secElem in secValue
                              for matElem in matValue if secElem == matElem]}

            if any(x != [] for x in _nono.values()):
                try:
                    _elset[secKey].update(_nono)
                except KeyError:
                    _elset[secKey] = _nono

    # _ana ={matKey : [secNumber for secNumber in secValue for matNumber in matValue if secNumber == matNumber]
    #       for secKey, secValue in _secDic.items() for matKey, matValue in _matDic.items()}

    # _elset = {}
    # for secKey, secValue in _secDic.items():
    #    for matKey, matValue in _matDic.items():
    #
    _elset2 = {}
    # compare = lambda a,b: [1 for i,j in zip(a,b) if i==j]
    #
    for _secNo, _matD in _elset.items():
        # print('')
        # print('section :',_secNo)
        _nono = {}
        for _matNo, _membList in _matD.items():
            # print('material :',_matNo)
            _size = len(_membList)

            _membList2 = list(_membList)
            _flat = []
            _sets = {}
            # _name = 1
            for i in range(_size):
                _membNo1 = _membList[i]
                _memb1 = element[_membNo1].name
                _vector1 = element[_membNo1].guidepoint[0].cos
                # _name = 's'+str(_secNo)+'_m'+str(_matNo)+'_v'+str(_vector1)
                # _name = i+1

                try:

                    # print('flat ++> ',_flat)
                    if _memb1 in _flat:
                        # print('continue  +', _memb1, _flat)
                        continue
                        # else:
                        #    _name = element[_membNo1].guidepoint[0].number
                        #    _sets[_name] = [_memb1]
                        #    print('')
                        #    print('* New Vector :', _memb1, _vector1)
                except:
                    pass

                _name = element[_membNo1].guidepoint[0].number
                _sets[_name] = [_memb1]
                # print('')
                # print('** New Vector :', _memb1, _name, _vector1)


                _size2 = len(_membList2)

                for j in range(i, _size2):
                    _membNo2 = _membList2[j]
                    _memb2 = element[_membNo2].name
                    _vector2 = element[_membNo2].guidepoint[0].cos

                    if _memb1 == _memb2: continue
                    if _vector1 == _vector2:
                        if _memb2 in _sets[_name]:
                            # print('continue ++', _memb1, _memb2)
                            continue
                        else:
                            _sets[_name].append(_memb2)
                            # print('append ',_memb1, _memb2, _vector2)
                            # else:
                            #    print('skip => ',_memb1, _memb2, _vector2)
                #
                _flat.extend(_sets[_name])
                _membList2 = list(set(_membList) - set(_flat))
                # print('new list --> ', _membList2)
            #
            # try:
            #    _nono[_matNo].update(_sets)
            #    print('append',_sets[_name])
            # except :
            _nono[_matNo] = _sets
            # print('start',_sets)
        #
        # try: _elset2[_secNo].udate(_nono)
        # except :
        _elset2[_secNo] = _nono
        #
    #
    return _elset2
    #


#
#
#
def findElement(_type):
    if str(_type).lower() == 'ibeam':
        _typeElem = 'B31'

    elif str(_type).lower() == 'pipe':
        _typeElem = 'PIPE31'

    else:
        print('element type {:} no yet defined'.format(_type))
        exit()

    return _typeElem


#
#
def printTubSec(_sec):
    ABAfile = []
    _DY = _sec.properties.D
    _T = _sec.properties.Tw

    ABAfile.append(" {:},  {:} \n".format(_DY, _T))
    # ABAfile.append(" {:10.0f}  {:10.0f} \n" .format(_DI, _T))
    return ABAfile


#
def printIsec(_sec):
    ABAfile = []
    l = _sec.properties.D
    ABAfile.append("  {:},  {:},  {:},  {:},"
                   .format(l, _sec.properties.D, _sec.properties.Bfb,
                           _sec.properties.Bft))

    ABAfile.append("  {:},  {:},  {:} \n"
                   .format(_sec.properties.Tfb, _sec.properties.Tft,
                           _sec.properties.Tw))

    return ABAfile


#
#
def printHeadLine(head, length=84):
    #
    ABAfile = []
    _head = str(head).upper()
    _space = length - 2 - len(_head)
    ABAfile.append("**\n")
    ABAfile.append("**\n")
    ABAfile.append("**{:}\n".format(length * '-'))
    ABAfile.append("**{:} {:}\n".format(_space * " ", _head))
    ABAfile.append("**{:}\n".format(length * '-'))
    ABAfile.append("**\n")
    ABAfile.append("**\n")
    #
    return ABAfile


#
def printheader():
    import datetime
    from time import gmtime, strftime

    _today = datetime.date.today()
    _time = strftime("%H:%M:%S", gmtime())

    # ABAfile = []
    header = 'HEADER'
    ABAfile = printHeadLine(header)
    ABAfile.append("** {:} Abaqus Input Deck \n".format(64 * "-"))
    ABAfile.append("** {:} Converted: \n".format(64 * " "))
    ABAfile.append("** {:} Time : {:}\n".format(64 * " ", _time))
    ABAfile.append("** {:} Date : {:}\n".format(64 * " ", _today))
    ABAfile.append("**\n")
    ABAfile.append("*HEADING\n")
    ABAfile.append("abaqus model converted by fem2ufo\n")
    #
    ABAfile.append("**\n")
    ABAfile.append("*PREPRINT, ECHO=YES, MODEL=YES, HISTORY=YES \n")
    ABAfile.append("**\n")

    return ABAfile


#
def printNode(node):
    ABAfile = []
    header = 'NODE SECTION'
    printHeadLine(header)
    ABAfile.append("*NODE\n")
    ABAfile.append("**   Number    X - coordinate   Y - coordinate   Z - coordinate \n")
    for _node in node.values():
        ABAfile.append(" {:10.0f},  {: 1.8E}, {: 1.8E}, {: 1.8E}\n"
                       .format(_node.number,
                               _node.coordinate[0], _node.coordinate[1], _node.coordinate[2]))

    return ABAfile


#
def printBeamElement(element, section, material, elsets):
    ABAfile = []
    header = 'BEAM ELEMENT SECTION'
    printHeadLine(header)
    # for _sec in section.values():
    #    print('---',_sec.name, _sec.number)
    #    for _elem in element.values():
    #        if _sec.name == _elem.section[0].name:
    #            print(_elem.number, _elem.section[0].name)

    for secNumber, secItems in elsets.items():
        for matNumber, matItem in secItems.items():
            for _vecNo, _items in matItem.items():
                _rows = 1
                _type = section[secNumber].shape
                _type = findElement(_type)
                _elsetName = 's' + str(secNumber) + '_m' + str(matNumber) + '_v' + str(_vecNo)
                ABAfile.append("**\n")
                ABAfile.append("*ELEMENT, TYPE={:}, ELSET= {:}\n".format(_type, _elsetName))
                ABAfile.append("** Element No {:} Node 1 {:} Node 2 {:} ...Node n\n"
                               .format(6 * " ", 6 * " ", 3 * " "))

                for _item in _items:
                    ABAfile.append(" {:12.0f},".format(element[_item].number))

                    _total = len(element[_item].node)

                    for x in range(_total):
                        #
                        if _rows == 4 or _rows == _total:
                            ABAfile.append(" {:12.0f} \n".format(element[_item].node[x].number))
                            _rows = 0
                        else:
                            ABAfile.append(" {:12.0f},".format(element[_item].node[x].number))
                        _rows += 1
                        #

    return ABAfile


#
def printNodalElement(node):
    header = 'NODAL ELEMENT SECTION'
    ABAfile = printHeadLine(header)

    return ABAfile


#
def printSections(element, section, material, elsets):
    header = 'SECTION PROPERTIES SECTION'
    ABAfile = printHeadLine(header)

    for secNumber, secItems in elsets.items():
        for matNumber, matItem in secItems.items():
            for _vecNo, _items in matItem.items():
                ABAfile.append("**\n")
                _elsetName = 's' + str(secNumber) + '_m' + str(matNumber) + '_v' + str(_vecNo)
                #
                ABAfile.append("*BEAM SECTION, POISSON=0.5, ELSET={:}, MATERIAL={:}, SECTION="
                               .format(_elsetName, material[int(matNumber)].name))

                if section[int(secNumber)].shape == 'PIPE':
                    ABAfile.append("PIPE \n")
                    ABAfile.extend(printTubSec(section[int(secNumber)]))

                elif section[int(secNumber)].shape == 'IBEAM':
                    ABAfile.append("I \n")
                    ABAfile.extend(printIsec(section[int(secNumber)]))

                _vector = element[_items[0]].guidepoint[0].cos
                ABAfile.append(" {:},  {:},  {:}\n"
                               .format(_vector[0], _vector[1], _vector[2]))
    # for _sec in section.values():
    #    TFEMmod.write("*BEAM SECTION, POISSON=0.5, ELSET={:}  MATERIAL={:1.8E}  SECTION={:1.8E}\n"
    #                          .format(4,_sec.name,_sec.dummy,_sec.type_))        
    return ABAfile


#
def printCouplings():
    ABAfile = []
    header = 'COUPLING SECTION'
    printHeadLine(header)
    #
    ABAfile.append("**\n")
    ABAfile.append("** {:} Kinematic Coupling \n".format(63 * "-"))
    ABAfile.append("**\n")
    ABAfile.append("**\n")
    ABAfile.append("** {:} Distributed Coupling \n".format(61 * "-"))
    ABAfile.append("**\n")

    return ABAfile


#
def printSlideLines():
    ABAfile = []
    header = 'SLIDELINE SECTION'
    printHeadLine(header)

    return ABAfile


#
def printTieConstrains():
    ABAfile = []
    header = 'TIE CONSTRAINT SECTION'
    printHeadLine(header)

    return ABAfile


#
def printReleases():
    ABAfile = []
    header = 'RELEASES SECTION'
    printHeadLine(header)

    return ABAfile


#
def printMaterials(material):
    ABAfile = []
    header = 'MATERIAL SECTION'
    printHeadLine(header)

    for _mat in material.values():
        ABAfile.append("*MATERIAL, NAME = {:}\n".format(_mat.name))
        ABAfile.append("**\n")
        ABAfile.append("*DENSITY \n")
        ABAfile.append(" {:},  {:} \n".format(_mat.density, 0))
        ABAfile.append("**\n")
        ABAfile.append("*ELASTIC,TYPE=ISOTROPIC \n")
        ABAfile.append(" {:},  {:},   {:} \n".format(_mat.E, _mat.poisson, 0))
        ABAfile.append("**\n")
        ABAfile.append("*PLASTIC,HARDENING=ISOTROPIC \n")
        ABAfile.append(" {:},  {:} \n".format(_mat.Fy * 1.10, 0.00))
        ABAfile.append(" {:},  {:} \n".format(_mat.Fy * 1.00, 0.01))
        ABAfile.append(" {:},  {:} \n".format(_mat.Fy * 0.10, 0.05))
        ABAfile.append(" {:},  {:} \n".format(_mat.Fy * 0.01, 1.00))
        ABAfile.append("**\n")
        ABAfile.append("*DAMPING, ALPHA=0.050000, BETA=0.005000\n")
        # ABAfile.append("*DAMPING, ALPHA=")
        # ABAfile.append(" {:}  {:} \n".format(_mat.alpha, _mat.alpha))
        ABAfile.append("**\n")
        ABAfile.append("**\n")

    return ABAfile


#
def printBoundary(node):
    ABAfile = []
    header = 'BOUNDARY CONDITION SECTION'
    printHeadLine(header)

    ABAfile.append("*BOUNDARY \n")

    for _node in node.values():
        try:
            NoDOF = len(_node.boundary)
            _test = 1.0 / NoDOF

            if 0 in _node.boundary:
                for i in range(NoDOF):
                    if _node.boundary[i] != 0:
                        ABAfile.append("{:10.0f}, {:10.0f}, {:10.0f} \n"
                                       .format(_node.name, i + 1, i + 1))
            else:
                ABAfile.append("{:10.0f}, {:10.0f}, {:10.0f} \n"
                               .format(_node.name, 1, NoDOF))
        except:
            pass

    ABAfile.append("**\n")

    return ABAfile


#
def printRetainedDOF():
    ABAfile = []
    header = 'RETAINED NODAL DOFs (SUPERNODES) SECTION'
    printHeadLine(header)

    return ABAfile


#
def printSet(sets):
    header = 'SET SECTION'
    ABAfile = printHeadLine(header)
    ABAfile.append("**\n")
    ABAfile.append("** {:} Member Sets \n".format(70 * "-"))
    ABAfile.append("**\n")

    for _set in sets.values():
        ABAfile.append("**\n")
        _size = 80 - len(str(_set.number)) - 12
        ABAfile.append("** {:} Set Number : {:} \n".format(_size * " ", _set.number))
        ABAfile.append("*ELSET, ELSET = {:}\n".format(_set.name))
        _NoSteps = printIter2(ABAfile, _set.items, 0, 1)

    ABAfile.append("**\n")
    ABAfile.append("** {:} Node Sets \n".format(63 * "-"))
    ABAfile.append("**\n")

    return ABAfile


#
def printIter2(_item, _ini=0, _noRows=0):
    ABAfile = []
    _NoParts = len(_item)
    for x in range(_ini, _NoParts):

        if _noRows == 6 and (x + 1) < _NoParts:
            ABAfile.append(" {:12.0f}, \n".format(_item[x]))
            # ABAfile.append("        ")
            _noRows = 0

        elif (x + 1) == _NoParts:
            ABAfile.append(" {:12.0f} \n".format(_item[x]))

        else:
            ABAfile.append(" {:12.0f},".format(_item[x]))
        _noRows += 1

    ABAfile.append("\n")

    return ABAfile


#
def printStep():
    header = 'STEP DATA SECTION'
    ABAfile = printHeadLine(header)

    ABAfile.append("**\n")
    ABAfile.append("*Step, name=Axial_Load, nlgeom=YES, extrapolation=PARABOLIC, inc=999999\n")
    ABAfile.append("*Static\n")
    ABAfile.append("0.01, 1., 1e-05, 1.\n")
    ABAfile.append("**\n")
    ABAfile.append("**\n")

    return ABAfile


#
def printLoad(load):
    header = 'LOADING SECTION'
    ABAfile = printHeadLine(header)

    for _loads in load.values():
        ABAfile.append("**\n")
        ABAfile.append("**\n")
        ABAfile.append("**LOADCASE, NAME = {:}\n".format(_loads.name))
        _size = 80 - len(str(_loads.number)) - 12
        ABAfile.append("** {:} Load Case No {:} \n"
                       .format(_size * "-", _loads.number))
        ABAfile.append("**\n")
        # nodal load
        ABAfile.append("**\n")
        ABAfile.append("** {:} Point Loads\n".format(70 * "-"))
        ABAfile.append("**\n")
        for _node in _loads.node:
            ABAfile.append("**\n")
            ABAfile.append("*CLOAD, OP = NEW\n")

            for i in range(len(_node.point)):
                if _node.point[i] != 0:
                    ABAfile.append("{:12.0f}, {:12.0f}, {: 1.8E}\n"
                                   .format(_node.number, i + 1, _node.point[i]))
        #
        # nodal load
        ABAfile.append("**\n")
        ABAfile.append("** {:} Distributed Loads\n".format(64 * "-"))
        ABAfile.append("**\n")
        # for _elem in _loads.element:
        #    pass
        #
        ABAfile.append("**\n")
        ABAfile.append("**END LOADCASE\n")
        ABAfile.append("**\n")

    return ABAfile


#
def printOutput():
    header = 'OUTPUT REQUEST SECTION'
    ABAfile = printHeadLine(header)
    ABAfile.append("**\n")
    ABAfile.append("**\n")
    ABAfile.append("*Restart, write, frequency=1 \n")
    ABAfile.append("**\n")
    ABAfile.append("**\n")
    ABAfile.append("** {:} Field Output : Node \n".format(62 * "-"))
    ABAfile.append("*Output, field\n")
    ABAfile.append("*Node Output\n")
    ABAfile.append("CF, RF, U\n")
    #
    ABAfile.append("**\n")
    ABAfile.append("** {:} Field Output : Element \n".format(59 * "-"))
    ABAfile.append("*Element Output, directions=YES\n")
    ABAfile.append("LE, PE, PEEQ, PEMAG, S\n")
    ABAfile.append("*Output, history, frequency=0\n")
    ABAfile.append("*End Step\n")
    ABAfile.append("**\n")

    return ABAfile


#
def printEOF(length=84):
    ABAfile = []
    #
    ABAfile.append("**\n")
    ABAfile.append("**\n")
    ABAfile.append("**{:}\n".format(length * '-'))
    ABAfile.append("**{:} fem2ufo is Python Powered \n".format(32 * " "))
    ABAfile.append("**{:} http://www.python.org/ \n".format(34 * " "))
    ABAfile.append("**{:} EOF \n".format(43 * " "))
    ABAfile.append("**{:}\n".format(length * '-'))
    ABAfile.append("**\n")
    #
    return ABAfile


#
#
def printAbaqusFile(node, section, element, material, sets, load):
    #
    print('')
    print('------------------- ABAOUT Module ------------------')
    print('--- Writing abaqus input file')
    #
    #
    #
    ABAout = printheader()
    #
    ABAout.extend(printNode(node))
    #
    elsets = findElemGroups(section, element, material)
    #
    ABAout.extend(printBeamElement(element, section, material, elsets))
    ABAout.extend(printNodalElement(node))
    ABAout.extend(printSections(element, section, material, elsets))
    #
    ABAout.extend(printCouplings())
    ABAout.extend(printSlideLines())
    ABAout.extend(printTieConstrains())
    ABAout.extend(printReleases())
    ABAout.extend(printMaterials(material))
    ABAout.extend(printRetainedDOF())
    ABAout.extend(printSet(sets))
    #
    ABAout.extend(printStep())
    ABAout.extend(printBoundary(node))
    ABAout.extend(printLoad(load))
    ABAout.extend(printOutput())
    ABAout.extend(printEOF())
    #
    return ABAout
    # print('--> ok')
    #

#
#
#
if __name__ == "__main__":
    #
    import os, sys
    import REDFEM as redfem
    #
    #
    directory = "."
    files = [v for v in os.listdir(directory) if os.path.splitext(v)[1] == ".FEM"]
    files2 = [v for v in os.listdir(directory) if os.path.splitext(v)[1] == ".fem"]
    print("List of fileNames:")
    print(files)
    print(files2)
    #
    #
    print('-------------------- T.FEM INPUT -------------------')
    TfemFile = input('Enter T.fem input file: ')
    print('')
    #
    #
    print('-------------------- L.FEM INPUT -------------------')
    LfemFile = input('Enter L.fem input file: ')
    print('')
    #
    #
    (NODE, SECTION, ELEMENT,
     MATERIAL, VECTOR, ECCEN, HINGE, SETS,
     JOINT, LOAD, UserData,
     CONCEPT, ConcTemp, MeshSC) = redfem.sesamFEMreader(TfemFile, LfemFile)
    #
    printAbaqusFile(NODE, SECTION, ELEMENT, MATERIAL, SETS, LOAD)
    #
    print('end')
    #
    #
    #
