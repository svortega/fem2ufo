#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports



# package imports
#

#
# ====================================================================
#                             FE model
# ====================================================================
#
#
def printAdmin(_space, userData):
    XMLmod = []
    _field = _space

    XMLmod.append('{:s}<administrative> \n'.format(_field))

    for _user in userData.values():
        XMLmod.append('{:s}<file_name> "{}" </file_name>\n'
                      .format(2 * ' ' + _field, _user.file_name))
        XMLmod.append('{:s}<user> {} </user>\n'
                      .format(2 * ' ' + _field, _user.user))
        XMLmod.append('{:s}<date> {} </date>\n'
                      .format(2 * ' ' + _field, _user.date))
        XMLmod.append('{:s}<version> {} </version>\n'
                      .format(2 * ' ' + _field, _user.version1))

    XMLmod.append('{:s}</administrative> \n'.format(_field))

    return XMLmod


#
def printSection(_space, section):
    XMLmod = []
    _field = _space

    XMLmod.append('{:s}<sections> \n'.format(_field))

    for _sec in section.values():
        XMLmod.append('{:s}<section name = "{}"> \n'
                      .format(2 * ' ' + _field, _sec.name))

        XMLmod.append('{:s}<number> {} </number> \n'.format(4 * ' ' + _field, _sec.number))
        XMLmod.append('{:s}<type> {} </type> \n'.format(4 * ' ' + _field, _sec.type))
        XMLmod.append('{:s}<shape> {} </shape> \n'.format(4 * ' ' + _field, _sec.shape))

        XMLmod.append('{:s}<dimensions>\n'.format(4 * ' ' + _field))
        # for _prop in _sec.properties.itervalues():
        XMLmod.append('{:s}<D> {} </D> \n'
                      .format(6 * ' ' + _field, _sec.properties.D))
        XMLmod.append('{:s}<Tw> {} </Tw> \n'
                      .format(6 * ' ' + _field, _sec.properties.Tw))
        if _sec.properties.Bft != 0:
            XMLmod.append('{:s}<Bft> {} </Bft> \n'
                          .format(6 * ' ' + _field, _sec.properties.Bft))
            XMLmod.append('{:s}<Tft> {} </Tft> \n'
                          .format(6 * ' ' + _field, _sec.properties.Tft))

        if _sec.properties.Bfb != 0:
            XMLmod.append('{:s}<Bfb> {} </Bfb> \n'
                          .format(6 * ' ' + _field, _sec.properties.Bfb))
            XMLmod.append('{:s}<Tfb> {} </Tfb> \n'
                          .format(6 * ' ' + _field, _sec.properties.Tfb))

        XMLmod.append('{:s}</dimensions>\n'.format(4 * ' ' + _field))

        XMLmod.append('{:s}<shear_factor>\n'.format(4 * ' ' + _field))
        XMLmod.append('{:s}<vertical> {} </vertical> \n'.format(6 * ' ' + _field, _sec.SFV))
        XMLmod.append('{:s}<horizontal> {} </horizontal> \n'.format(6 * ' ' + _field, _sec.SFH))
        XMLmod.append('{:s}</shear_factor>\n'.format(4 * ' ' + _field))

        XMLmod.append('{:s}</section>\n'.format(2 * ' ' + _field))

    XMLmod.append('{:s}</sections> \n'.format(_field))

    return XMLmod


#
def printMaterial(_space, materials):
    XMLmod = []
    _field = _space

    XMLmod.append('{:s}<materials> \n'.format(_field))

    for _mat in materials.values():
        XMLmod.append('{:s}<material name = "{}"> \n'
                      .format(2 * ' ' + _field, _mat.name))

        XMLmod.append('{:s}<number> {} </number> \n'
                      .format(4 * ' ' + _field, _mat.number))
        XMLmod.append('{:s}<type> {} </type> \n'
                      .format(4 * ' ' + _field, _mat.type))
        XMLmod.append('{:s}<yield_stress> {} </yield_stress> \n'
                      .format(4 * ' ' + _field, _mat.Fy))
        XMLmod.append('{:s}<youngs_modulus> {} </youngs_modulus> \n'
                      .format(4 * ' ' + _field, _mat.E))
        XMLmod.append('{:s}<poissons_ratio> {} </poissons_ratio> \n'
                      .format(4 * ' ' + _field, _mat.poisson))
        XMLmod.append('{:s}<density> {} </density> \n'
                      .format(4 * ' ' + _field, _mat.density))

        if _mat.alpha != 0:
            XMLmod.append('{:s}<thermal_expansion> {} </thermal_expansion> \n'
                          .format(4 * ' ' + _field, _mat.alpha))

        if _mat.damping != 0:
            XMLmod.append('{:s}<damping> {} </damping> \n'
                          .format(4 * ' ' + _field, _mat.damping))

        if _mat.stiffness != 0:
            XMLmod.append('{:s}<stiffness> {} </stiffness> \n'
                          .format(4 * ' ' + _field, _mat.stiffness))

        if _mat.spring != 0 or _mat.spring != 'N/A':
            XMLmod.append('{:s}<spring> {} </spring> \n'
                          .format(4 * ' ' + _field, _mat.spring))

        XMLmod.append('{:s}</material> \n'.format(2 * ' ' + _field))

    XMLmod.append('{:s}</materials> \n'.format(_field))

    return XMLmod


#
def printNode(_space, nodes):
    XMLmod = []
    _field = _space

    XMLmod.append('{:s}<nodes> \n'.format(_field))

    for _node in nodes.values():
        # for _node in nodes[i].iteritems():
        # print(_node.name, _node.X)
        XMLmod.append('{:s}<node name = "{}"> \n'.format(2 * ' ' + _field, _node.name))
        XMLmod.append('{:s}<number> {} </number> \n'.format(4 * ' ' + _field, _node.number))
        XMLmod.append('{:s}<coordinates system = "{}"> \n'.format(6 * ' ' + _field, 'cartesian'))
        XMLmod.append('{:s}<x> {} </x> \n'.format(8 * ' ' + _field, _node.coordinate[0]))
        XMLmod.append('{:s}<y> {} </y> \n'.format(8 * ' ' + _field, _node.coordinate[1]))
        XMLmod.append('{:s}<z> {} </z> \n'.format(8 * ' ' + _field, _node.coordinate[2]))
        XMLmod.append('{:s}</coordinates> \n'.format(6 * ' ' + _field))
        XMLmod.append('{:s}</node>\n'.format(2 * ' ' + _field))

    XMLmod.append('{:s}</nodes> \n'.format(_field))

    return XMLmod


#
def printElements(_space, elements):
    XMLmod = []
    _field = _space

    XMLmod.append('{:s}<elements> \n'.format(_field))

    for _element in elements.values():
        XMLmod.append('{:s}<element name = "{}"> \n'
                      .format(2 * ' ' + _field, _element.name))

        XMLmod.append('{:s}<number> {} </number> \n'
                      .format(4 * ' ' + _field, _element.number))
        XMLmod.append('{:s}<type> {} </type> \n'
                      .format(4 * ' ' + _field, _element.type.name))
        XMLmod.append('{:s}<material> {} </material> \n'
                      .format(4 * ' ' + _field, _element.material.name))

        # nodes
        XMLmod.append('{:s}<nodes>\n'.format(4 * ' ' + _field))
        for _node in _element.node:
            XMLmod.append('{:s}<node> {} </node>\n'
                          .format(6 * ' ' + _field, _node.name))
        XMLmod.append('{:s}</nodes>\n'.format(4 * ' ' + _field))

        # section
        XMLmod.append('{:s}<sections>\n'.format(4 * ' ' + _field))
        for _sec in _element.section:
            XMLmod.append('{:s}<section> {} </section>\n'
                          .format(6 * ' ' + _field, _sec.name))
        XMLmod.append('{:s}</sections>\n'.format(4 * ' ' + _field))

        # if _element.offsets:
        XMLmod.append('{:s}<offsets>\n'.format(4 * ' ' + _field))
        for _offset in _element.offsets:
            XMLmod.append('{:s}<offsetxx> {} </offsetxx>\n'
                          .format(6 * ' ' + _field, _offset))
        XMLmod.append('{:s}</offsets>\n'.format(4 * ' ' + _field))

        # if _element.releases:
        XMLmod.append('{:s}<releases>\n'.format(4 * ' ' + _field))
        for _release in _element.releases:
            XMLmod.append('{:s}<release> {} </release>\n'
                          .format(6 * ' ' + _field, _release.name))
        XMLmod.append('{:s}</releases>\n'.format(4 * ' ' + _field))

        # if _element.guidepoint:
        XMLmod.append('{:s}<guidepoint>\n'.format(4 * ' ' + _field))
        for _guidepoint in _element.guidepoint:
            XMLmod.append('{:s}<number> {} </number>\n'
                          .format(6 * ' ' + _field, _guidepoint.number))
        XMLmod.append('{:s}</guidepoint>\n'.format(4 * ' ' + _field))

        XMLmod.append('{:s}</element> \n'.format(2 * ' ' + _field))

    XMLmod.append('{:s}</elements> \n'.format(_field))

    return XMLmod


#
def printLoads(_space, loads):
    XMLmod = []
    _field = _space

    XMLmod.append('{:s}<loads> \n'.format(_field))

    for _load in loads.values():
        XMLmod.append('{:s}<load name = "{}"> \n'
                      .format(_field + 2 * ' ', _load.name))
        # XMLmod.append('{:s}<title> {} </title> \n'
        #                  .format(_field + 4*' ', _load.title))
        XMLmod.append('{:s}<number> {} </number> \n'
                      .format(_field + 4 * ' ', _load.number))

        XMLmod.append('{:s}<gravity> \n'
                      .format(_field + 4 * ' '))
        # for _grav in _load.gravity:
        #    XMLmod.append('{:s}<gx> {} </gx> \n'
        #                  .format(_field + 6*' ', _grav.gx))

        XMLmod.append('{:s}</gravity> \n'
                      .format(_field + 4 * ' '))

        XMLmod.append('{:s}<node> \n'
                      .format(_field + 4 * ' '))

        for _node in _load.node:
            XMLmod.append('{:s}<number> {} </number> \n'
                          .format(_field + 6 * ' ', _node.number))

        XMLmod.append('{:s}</node> \n'
                      .format(_field + 4 * ' '))

        XMLmod.append('{:s}<element> \n'
                      .format(_field + 4 * ' '))

        for _memb in _load.element:
            XMLmod.append('{:s}<number> {} </number> \n'
                          .format(_field + 6 * ' ', _memb.number))
            # XMLmod.append('{:s}<name> {} </name> \n'
            #              .format(_field + 6*' ', _memb.name))

        XMLmod.append('{:s}</element> \n'
                      .format(_field + 4 * ' '))

        XMLmod.append('{:s}</load>\n'
                      .format(_field + 2 * ' '))

    XMLmod.append('{:s}</loads> \n'.format(_field))

    return XMLmod


#
def printBoundary():
    XMLmod = []
    XMLmod.append('<{}boundary> \n'.format(''))
    XMLmod.append('<{}ix> {} </ix> \n'.format('', 'ix'))
    XMLmod.append('<{}iy> {} </iy> \n'.format('', 'iy'))
    XMLmod.append('<{}iz> {} </iz> \n'.format('', 'iz'))
    XMLmod.append('<{}irx> {} </irx> \n'.format('', 'irx'))
    XMLmod.append('<{}iry> {} </iry> \n'.format('', 'iry'))
    XMLmod.append('<{}irz> {} </irz> \n'.format('', 'irz'))
    XMLmod.append('<{}/boundary> \n'.format(''))

    return XMLmod


#
def HTML5template(userData, section, materials, elements, nodes,
                  loads):
    today = datetime.date.today()
    XMLmod = []

    XMLmod.append('<?xml version="1.0" encoding="ISO-8859-1"?> \n')
    XMLmod.append('<SIM_FEM_database_protocol version = "0.1"> \n')
    # XMLmod.append('<meta charset=utf-8> \n')
    # OutputFile.write('<meta name="viewport" content="width=620">'+"\n")

    # XMLmod.append('<title>HTML5 Database Demo: Model Geometry Database</title> \n')
    # OutputFile.write(' <PRAS_data_base version="0.1">'+"\n")
    # OutputFile.write('  </PRAS_data_base>'+"\n")
    # XMLmod.append('</header> \n')
    # XMLmod.append('<body> \n')

    _modelField = 2 * ' '
    XMLmod.append('{:s}<model name = "test"> \n'.format(_modelField))

    _adminField = 2 * _modelField
    XMLmod = []
    XMLmod.extend(printAdmin(_adminField, userData))

    # XMLmod.append('</header> \n')

    # start property section
    # _artField = 2*_modelField
    #
    _loadField = 2 * _modelField
    XMLmod.append('{:s}<units> \n'.format(_loadField))
    XMLmod.append('{:s}</units> \n'.format(_loadField))

    _propField = 2 * _modelField
    XMLmod.append('{:s}<property> \n'.format(_propField))

    _secSpace = 3 * _modelField
    XMLmod = []
    XMLmod.extend(printSection(_secSpace, section))

    _matSpace = 3 * _modelField
    XMLmod.extend(printMaterial(_matSpace, materials))

    # XMLmod.append('{:s}<loads> \n'.format(_secSpace))
    # XMLmod.append('{:s}</loads> \n'.format(_secSpace))

    XMLmod.append('{:s}</property> \n'.format(_propField))
    # XMLmod.append('{:s}</articule> \n'.format(_artField))
    # end property section

    # start geometry section
    # XMLmod.append('{:s}<articule> \n'.format(_artField))

    # XMLmod = []
    _geomField = 2 * _modelField
    XMLmod.append('{:s}<geometry> \n'.format(_geomField))

    # _OutputFile.close()

    _nodeSpace = 3 * _modelField
    # XMLmod = []
    XMLmod.extend(printNode(_nodeSpace, nodes))

    _elemSpace = 3 * _modelField
    XMLmod.extend(printElements(_elemSpace, elements))

    XMLmod.append('{:s}</geometry> \n'.format(_geomField))
    # end geometry section

    _hydroField = 2 * _modelField
    XMLmod = []
    XMLmod.extend(printLoads(_hydroField, loads))

    # _hydroField = 2*_modelField
    XMLmod.append('{:s}<hydrodynamics> \n'.format(_hydroField))
    XMLmod.append('{:s}</hydrodynamics> \n'.format(_hydroField))

    _foundField = 2 * _modelField
    XMLmod.append('{:s}<foundations> \n'.format(_foundField))
    XMLmod.append('{:s}</foundations> \n'.format(_foundField))

    XMLmod.append('{:s}</model> \n'.format(_modelField))
    XMLmod.append('</SIM_FEM_database_protocol> \n')

    return XMLmod


#