#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports



# package imports
#

#
# ====================================================================
#                             WCReport
# ====================================================================
#
#
def print_area(areas, _space):
    """
    """
    XMLmod = []
    _field = 3 * _space
    _area_out = {}

    # area section
    for _area in areas.values():

        # start areas loop
        XMLmod.append('{:s}<area name="{:}">\n'
                      .format(_field, _area.name))

        # print description
        XMLmod.append('{:s}<description> "{:}" </description>\n'
                      .format(2 * ' ' + _field, _area.description))

        # print disciplines
        _disc_out, _area_mass = print_discipline(_area.discipline,
                                                 _space)

        XMLmod.extend(_disc_out)

        # print total mass
        _disc_out, _mass_out = print_total_mass(_area_mass,
                                                _field,
                                                'area')

        XMLmod.extend(_disc_out)

        for _name, _mass in _mass_out.items():
            try:
                _area_out[_name].append(_mass)

            except KeyError:
                _area_out[_name] = [_mass]

                # cloase areas loop
        XMLmod.append('{:s}</area>\n'.format(_field))

    return XMLmod, _area_out


#
def print_discipline(discipline, _space):
    """
    """
    XMLmod = []
    _field = 4 * _space
    _disc_out = {}

    # discipline section
    for _discipline in discipline.values():
        #

        XMLmod.append('{:s}<discipline name="{:}">\n'
                      .format(_field, _discipline.name))

        # print description
        XMLmod.append('{:s}<description> "{:}" </description>\n'
                      .format(2 * ' ' + _field, _discipline.description))

        # start item loop
        _item_out, _disc_mass = print_item(_discipline.item,
                                           _space)
        XMLmod.extend(_item_out)

        # print total mass
        disc_out, _mass_out = print_total_mass(_disc_mass,
                                               4 * _space,
                                               'discipline')
        XMLmod.extend(disc_out)

        for _name, _mass in _mass_out.items():
            try:
                _disc_out[_name].append(_mass)

            except KeyError:
                _disc_out[_name] = [_mass]

                # close discipline loop
        XMLmod.append('{:s}</discipline>\n'.format(_field))

    return XMLmod, _disc_out


#
def print_item(item, _space):
    """
    """
    XMLmod = []
    _field = 5 * _space
    _disc_mass = {}

    # start item loop
    for _item in item:
        #
        XMLmod.append('{:s}<item  name="{:}">\n'.
                      format(_field, _item.name))

        XMLmod.append('{:s}<description> "{:}" </description>\n'
                      .format(2 * ' ' + _field, _item.description))
        #
        # XMLmod.append('{:s}<weight_record date="{:}">\n'
        #              .format(2 * ' ' + _field, today))      

        #
        # start Items loop
        item_out, _item_mass = print_unit(_item, _space)
        XMLmod.extend(item_out)

        # print total mass
        item_out, _mass_out = print_total_mass(_item_mass,
                                               _field,
                                               'item')
        XMLmod.extend(item_out)

        for _name, _mass in _mass_out.items():
            try:
                _disc_mass[_name].append(_mass)

            except KeyError:
                _disc_mass[_name] = [_mass]

        # record date
        # XMLmod.append('{:s}</weight_record>\n'
        #              .format(2 * ' ' + _field))

        # close item
        XMLmod.append('{:s}</item>\n'.format(_field))

    return XMLmod, _disc_mass


#
def print_unit(_item, _space):
    """
    """
    XMLmod = []
    _field = 6 * _space
    _item_mass = {}

    # start Items loop
    for _no, _unit in _item.units.items():

        XMLmod.append('{:s}<weight_unit name="{:}">\n'
                      .format(_field, _unit.name))

        XMLmod.append('{:s}<dimension dx="{:}" dy="{:}" dz="{:}"/>\n'
                      .format(2 * ' ' + _field, _unit.dimension[0],
                              _unit.dimension[1], _unit.dimension[2]))

        XMLmod.append('{:s}<description> "{:}" </description>\n'
                      .format(2 * ' ' + _field, _unit.description))

        #
        for _weight in _unit.weight.values():

            XMLmod.append('{:s}<weight_{:} mass="{:}">\n'.
                          format(2 * ' ' + _field, _weight.name,
                                 _weight.mass[0]))

            XMLmod.append('{:}<position x="{:}" y="{:}" z="{:}"/>\n'
                          .format(4 * ' ' + _field, _weight.mass[1],
                                  _weight.mass[2], _weight.mass[3]))

            try:
                _item_mass[_weight.name].append(_weight.mass)

            except KeyError:
                _item_mass[_weight.name] = [_weight.mass]

            XMLmod.append('{:s}<status> "{:}" </status>\n'.
                          format(4 * ' ' + _field, _weight.status))

            XMLmod.append('{:s}</weight_{:}>\n'.
                          format(2 * ' ' + _field, _weight.name))
        #
        #
        XMLmod.append('{:s}</weight_unit>\n'.
                      format(_field))
        #
        #
    #
    return XMLmod, _item_mass


#
def print_total_mass(_mass, field, _case):
    """
    """
    XMLmod = []
    _out_mass = {}
    _field = 2 * ' ' + field

    XMLmod.append('{:s}<weight_{:}>\n'
                  .format(_field, _case))

    # print item total mass & CoG
    for name, _mt in _mass.items():
        #
        _mass_total = [sum(x) for x in zip(*_mt)]
        #
        try:
            _out_mass[name].append(_mass_total)

        except KeyError:
            _out_mass[name] = _mass_total
            #
        XMLmod.append('{:s}<weight_{:} mass="{:}">\n'.
                      format(2 * ' ' + _field, name, _mass_total[0]))

        XMLmod.append('{:}<position x="{:}" y="{:}" z="{:}"/>\n'
                      .format(4 * ' ' + _field,
                              _mass_total[1] / len(_mt),
                              _mass_total[2] / len(_mt),
                              _mass_total[3] / len(_mt)))
        #
        XMLmod.append('{:s}</weight_{:}>\n'.
                      format(2 * ' ' + _field, name))

    XMLmod.append('{:s}</weight_{:}>\n'
                  .format(_field, _case))

    return XMLmod, _out_mass


#
def print_WCR(WCdata):
    """
    """
    today = WCdata.date
    time = datetime.datetime.now().time()
    # print(time)
    # print(time.hour, time.minute, time.second)
    XMLmod = []
    #
    XMLmod.append('<?xml version="1.0" encoding="ISO-8859-1"?> \n')
    XMLmod.append('<WC_database_protocol version = "0.1">\n')
    #
    _space = 2 * ' '
    _space_2 = 2 * _space
    #
    # Admin
    XMLmod.append('{:s}<administrative>\n'.format(_space))
    XMLmod.append('{:s}<session_info  date="{:}" time="{:}"/>\n'
                  .format(_space_2, today, time))
    XMLmod.append('{:s}</administrative>\n'.format(_space))
    #
    # units
    XMLmod.append('{:s}<units> \n'.format(_space))
    XMLmod.append('{:s}<input_unit phenomenon="Force" unit="N"/>\n'
                  .format(_space_2))
    XMLmod.append('{:s}<input_unit phenomenon="Length" unit="m"/>\n'
                  .format(_space_2))
    XMLmod.append('{:s}</units> \n'.format(_space))
    #
    # 
    XMLmod.append('{:s}<weight_report name="{:}" date="{:}">\n'
                  .format(_space, WCdata.database, today))
    #
    # start loop
    for _assembly in WCdata.assembly.values():
        # start assembly loop
        XMLmod.append('{:s}<assembly name="{:}">\n'
                      .format(_space_2, _assembly.name))

        # print description
        XMLmod.append('{:s}<description> "{:}" </description>\n'
                      .format(3 * _space, _assembly.description))

        # area section
        _area_out, _assembly_mass = print_area(_assembly.areas, _space)
        XMLmod.extend(_area_out)

        # print total mass
        _disc_out, _mass_out = print_total_mass(_assembly_mass,
                                                _space_2,
                                                'assembly')

        XMLmod.extend(_disc_out)

        # close assembly loop
        XMLmod.append('{:s}</assembly> \n'.format(_space_2))
        #
    XMLmod.append('{:s}</weight_report> \n'.format(_space))
    XMLmod.append('</WC_database_protocol> \n')
    #
    #
    return XMLmod
    #
    #
    #
    #
