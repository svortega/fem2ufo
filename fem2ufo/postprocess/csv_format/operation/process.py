# 
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import sys
import csv
import os


# package imports



#
def print_csv(item_list, file_name, row=False):
    """
    """
    #
    file_name = file_name +'.csv'
    #
    try:
        os.remove(file_name)
    except PermissionError:
        print('  *** error : Close Metocean Spreadsheet')
        ExitProg = input('      Press Enter Key To Continue')
        try:
            os.remove(file_name)
        except PermissionError:
            print('  *** error : You did not close the Metocean Spreadsheet')
            sys.exit()
    except FileNotFoundError:
        pass
    
    length = len(item_list)
    #
    if row:
        with open(file_name, 'w', newline='') as testfile:
            csv_writer = csv.writer(testfile, dialect='excel')
            for y in range(length):
                csv_writer.writerow([x[y] for x in item_list])
    else:
        with open(file_name, 'w', newline='') as testfile:
            csv_writer = csv.writer(testfile, dialect='excel')
            for y in range(length):
                csv_writer.writerow(item_list[y])        
    #
#