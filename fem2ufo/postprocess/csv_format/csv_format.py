# 
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import os

# package imports
import fem2ufo.process.control as process
import fem2ufo.postprocess.csv_format.model.loading as csv_comb
import fem2ufo.postprocess.csv_format.model.foundations as csv_soil

#
# ----------------------------------------------------------------------
# CSV format
#
class CSV_conversion:
    """Class to convert fem2ufo FE concept model to CSV format"""
    
    def __init__(self):
        """
        """
        self.units_in = None
        # Units [length, mass, time, temperature, force, pressure/stress]
        self.units_out = process.units.set_SI_units()
        self.factors = [0, 0, 0, 0, 0, 0]
    #
    def model(self, f2u_model):
        """
        **Parameters**:
           :model: fem2ufo's FE concept model
        """
        self.model = f2u_model.get_model()
        self.units_in = self.model.data['units']
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """
        try:
            path = os.path.normcase(path)
            os.chdir(path)
        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()

        retval = os.getcwd()
        print('')
        print('--- Current working directory : {:}'.format(retval))
    #
    def units(self, **kwargs):
        """
        Provide units for conversion purposes (i.e. usfos files will be printed with 
        this units, except for analysis that include usfos generated metocean loading,
        units are set in SI)
        Available units: length, mass, time, temperature, force, pressure/stress
        Units [length, mass, time, temperature, force, pressure/stress]
        
        **Parameters**:
           :length: [mandatory]  
           :force: [mandatory]  
           :temperature :  
           :gravity: [default : 9.81ms^2] 
        """
        for key, value in kwargs.items():
            _unit = process.units.find_unit_case(key)
            self.units_out = process.units.units_module(_unit, value,
                                                        self.units_out)
        #
        #print('-->')
    #
    def print_load_combination(self):
        """
        """
        # set output units
        #if self.units_out.count(None) == len(self.units_out):
        #    self.units_out = self.units_in
        #
        if not self.units_out[0] and not self.units_out[4]:
            self.units_out = self.units_in
        #
        csv_comb.print_load_combination_to_csv(self.model, self.units_out)
    #
    def print_foundation(self):
        """
        """
        if self.model.foundation:
            # set output units
            if not self.units_out[0] and not self.units_out[4]:
                self.units_out = self.units_in
            
            csv_soil.print_soil_to_csv(self.model, self.units_out)
        else:
            print('    ** Warning no foundation data found')
#
#