# 
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import datetime
from collections import OrderedDict

# package imports
import fem2ufo.process.control as process
import fem2ufo.postprocess.csv_format.operation.process as csv_process

#
#
def get_header(_name):
    """
    """
    meto = []
    meto.append(["------------------------------"])
    meto.append([_name])
    meto.append(["------------------------------"])
    
    return meto
#
#
def print_meto2csv(metocean, areas, analysis_name, 
                   analysis_number, units_in, units_out, 
                   row=False):
    """
    """
    # get date
    today = datetime.date.today()
    # get conversion factors
    factors, _grav = process.units.get_factors_and_gravity(units_in,
                                                           units_out)    
    #
    header_1 = ['Metocean Name', 'Number']
    header_2 = [analysis_name, analysis_number]
    #
    #
    meto = []
    meto.append(['Last Updated : {:}'.format(str(today))])
    meto.extend(get_header('Metocean Section'))
    meto.append(header_1)
    meto.append(header_2)
    #
    _wave, _current, _wind = get_metocean(metocean)
    _wave_data, wave_bucket = get_wave(_wave, units_out, factors)
    _curr_data, curr_bucket = get_current(_current, units_out, factors)
    _wind_data, wind_bucket = get_wind(_wind, units_out, factors)
    if areas:
        _wind_area_data = get_wind_areas(areas, units_out, factors)
        _wind_area_comb = get_wind_area_combinations(areas, _wind)
    
    _comb_data = get_seastates(metocean, wave_bucket,
                               curr_bucket, wind_bucket)
    #
    meto.extend(_comb_data)
    meto.extend(_wave_data)
    meto.extend(_curr_data)
    meto.extend(_wind_data)
    if areas:
        meto.extend(_wind_area_data)
        meto.extend(_wind_area_comb)
    #
    #
    file_name = 'Metocean_' + analysis_name
    csv_process.print_csv(meto, file_name, row)
    #
    #
    #return seastate
    #print('here')
#
def get_seastates(metocean, wave_bucket, 
                  curr_bucket, wind_bucket):
    """
    """
    #
    header_1 = ['Items', None, None,
                'Wave', None, None, None, 
                'Current', None, None, None, 
                'Wind']    
    #
    header_2 = ['Combination Name', 'Number', 'Design Load', 
                'Name', 'Direction [deg]', 'Kinematics','Buoyancy',
                'Name', 'Blockage', 'Stretching', 'Direction [deg]',
                'Name', 'Direction [deg]']
    #
    meto = []
    meto.extend(get_header('Combination Section'))
    meto.append(header_1)
    meto.append(header_2)
    _areas_total = []
    #
    for key in sorted(metocean, key = lambda name: metocean[name].number):
        _seastate = metocean[key]
        _wave = _seastate.wave
        for _bname, _group in wave_bucket.items():
            if _wave.name in _group:
                _wave_name = _bname
                break
        #
        _wave_direction = _seastate.wave_direction
        if 'calm' in _wave.theory.lower():
            _wave_direction = 0
        #
        _stretching = None
        if _seastate.current_stretching:
            _stretching = 'on'
        
        #buoy = 'off'
        #if _seastate.buoyancy == 'buOn':
        #    buoy = 'on'
        
        #elif _seastate.buoyancy == 'buOnly':
        #    _wave.theory = 'calm sea'
        #
        _areas = None
        _areas_total = False
        if _seastate.wind:
            _wind = _seastate.wind
            for _bname, _group in wind_bucket.items():
                if _wind.name in _group:
                    _wind_name = _bname
                    break
            #
            _wind_direction = _seastate.wind_direction
            
            if _seastate.wind_areas:
                _areas = [_seastate.wind.name]
                _areas_total = True
                #_areas = [_area.name for _area in _seastate.wind_areas]
                #_areas_total.append(len(_areas))
        else:
            _wind_name = None
            _wind_direction = None
        #
        if _seastate.current:
            _current = _seastate.current
            for _bname, _group in curr_bucket.items():
                if _current.name in _group:
                    _current_name = _bname
                    break
            #
            _current_direction = _seastate.current_direction
            _current_blockage = _seastate.current_blockage
            
        else:
            _current_name = None
            _current_direction = None
            _current_blockage = None
        #
        _index = len(meto)
        meto.append([_seastate.name, _seastate.wave.number, _seastate.design_load,
                     _wave_name, _wave_direction, 
                     _seastate.wave_kinematics, _seastate.buoyancy,
                     _current_name, _current_blockage, _stretching, _current_direction,
                     _wind_name, _wind_direction])
        #
        if _areas:
            meto[_index].extend(_areas)
    #
    if _areas_total:
        meto[4].extend(['Wind Area Group'])
    #if _areas_total:
    #    _areas_total = max(_areas_total)
    #    meto[4].extend(['Area Name' for x in range(_areas_total)])
    #
    return meto
#
def get_metocean(metocean):
    """
    """
    _wave = {}
    _wind = {}
    _current = {}
    for key, item in metocean.items():
        if item.wave:
            _wave[item.wave.name] = item.wave
        
        if item.wind:
            _wind[item.wind.name] = item.wind
        
        if item.current:
            _current[item.current.name] = item.current
    
    return _wave, _current, _wind
#
def get_wave(wave, units_out, factors):
    """
    """
    _wave_set = []
    _wave_flat = {}
    
    _wave_name = [key for key in sorted(wave, key = lambda name: wave[name].number)]
    
    #for _name, _wave in wave.items():
    for _name in _wave_name:
        _wave = wave[_name]
        #
        if 'calm' in _wave.theory.lower():
            _wave_flat[_wave.name] = [None, None, 
                                      _wave.theory, None, 
                                      _wave.water_depth]
        else:
            _wave_flat[_wave.name] = [_wave.height, _wave.period, 
                                      _wave.theory, _wave.order, 
                                      _wave.water_depth]
        #
        _wave_set.append(_wave.name)
    #
    #
    _wave_bucket = {}
    for _name, _prof in _wave_flat.items():
        _tb_deleted = []
        for _name2 in _wave_set:
            if _name == _name2:
                continue
            #
            _prof2 = _wave_flat[_name2]
            
            if _prof == _prof2:
                #
                try:
                    _wave_bucket[_name].append(_name2)
                except KeyError:
                    _wave_bucket[_name] = [_name, _name2]
                    _tb_deleted.append(_name)
            
                _tb_deleted.append(_name2)
        #
        for _wset_name in _tb_deleted:
            try:
                _wave_set.remove(_wset_name)
            except:
                print('what error type?')
    #
    for _name in _wave_set:
        _wave_bucket[_name] = [_name]    
    #
    header = ['Wave Name', 'Number',
              'Height [{:}]'.format(units_out[0]), 
              'Period [{:}]'.format(units_out[2]),
              'Water Depth [{:}]'.format(units_out[0]), 
              'Theory', 'Order', 'Phase [deg]']
    #
    #
    meto = get_header('Wave Section')
    #
    new_wave = {}
    #
    i = 0
    for _name, _group in sorted(_wave_bucket.items()):
        i+=1
        _wave = wave[_name]
        _wname = 'wave_' + str(i)
        new_wave[_wname] = _group
        #
        if i == 1:
            meto.append(header)
        #
        if 'calm' in _wave.theory.lower() :
            _wave.height = None
            _wave.period = None
        #
        _hw = None
        if _wave.height:
            _hw = str("{:1.3f}").format(_wave.height * factors[0])
        #
        meto.append([_wname, i, _hw,
                     _wave.period, str("{:1.3f}").format(_wave.water_depth * factors[0]),
                     _wave.theory, _wave.order])
        #        
    #
    return meto, new_wave
#
def get_current(current, units_out, factors):
    """
    """
    #
    _curr_flat = {}
    _curr_set = []
    for _name, _current in current.items():
        _elev = []
        _vel = []
        _curr_set.append(_name)
        for _prof in _current.profile:
            _elev.append(_prof[0])
            _vel.append(_prof[1] * _current.velocity)
        #
        _new_elev = []
        _new_vel = []
        for _item in sorted(_elev, reverse=True):
            _new_elev.append(_item)
            _index = _elev.index(_item)
            _new_vel.append(_vel[_index])
        #
        _curr_flat[_name] = [_new_elev, _new_vel]
    #
    _curr_flat = OrderedDict(sorted(_curr_flat.items(), key=lambda t: t[0]))
    #
    _curr_pot = {}
    for _name, _prof in _curr_flat.items():
        _tb_deleted = []
        for _name2 in _curr_set:
            
            if _name == _name2:
                continue
            #
            _prof2 = _curr_flat[_name2]
            if _prof[0] == _prof2[0] and _prof[1] == _prof2[1]:
                try:
                    _curr_pot[_name].append(_name2)
                except KeyError:
                    _curr_pot[_name] = [_name, _name2]
                    _tb_deleted.append(_name)
                
                _tb_deleted.append(_name2)
        #
        for _c_name in _tb_deleted:
            try:
                _curr_set.remove(_c_name)
            except:
                print('what error type?')
    #
    #
    for _name in _curr_set:
        _curr_pot[_name] = [_name]
    #
    _curr_pot = OrderedDict(sorted(_curr_pot.items(), key=lambda t: t[0]))
    #
    #
    meto = get_header('Current Section')
    #
    i = 0
    _curr_out = {}
    for _name, _group,  in _curr_pot.items():
        i += 1
        _cname = 'Current_' + str(i)
        _curr_out[_cname] = _group
        header = ['Current Name', 'Number', 
                  'Elevation [{:}]'.format(units_out[0]), 
                  'Velocity [{:}/{:}]'.format(units_out[0], units_out[2])]
        meto.append(header)
        _profile = _curr_flat[_name]
        _current = current[_name]
        
        meto.append([_cname, i, 
                     str("{:1.3f}").format(_profile[0][0] * factors[0]), 
                     str("{:1.3f}").format(_profile[1][0] * factors[0])])
        
        for x in range(1, len(_profile[0])):
            meto.append([None, None, 
                         str("{:1.3f}").format(_profile[0][x] * factors[0]), 
                         str("{:1.3f}").format(_profile[1][x] * factors[0])])
        #
    #
    return meto, _curr_out
#
def get_wind(wind, units_out, factors):
    """
    """
    #
    _wind_flat = {}
    _wind_set = []
    for _name, _wind in wind.items():
        _wind_flat[_name] = [_wind.velocity, _wind.height, _wind.formula]
        _wind_set.append(_name)
    #
    _wind_flat = OrderedDict(sorted(_wind_flat.items(), key=lambda t: t[0]))
    #
    _wind_pot = {}
    for _name, _prof in _wind_flat.items():
        _tb_deleted = []
        for _name2 in _wind_set:
            
            if _name == _name2:
                continue
            #
            _prof2 = _wind_flat[_name2]
            
            if _prof == _prof2:
                try:
                    _wind_pot[_name].append(_name2)
                except KeyError:
                    _wind_pot[_name] = [_name, _name2]
                    _tb_deleted.append(_name)
            
                _tb_deleted.append(_name2)
        #
        for _w_name in _tb_deleted:
            try:
                _wind_set.remove(_w_name)
            except:
                print('what error type?')
    #
    for _name in _wind_set:
        _wind_pot[_name] = [_name]    
    #
    _wind_pot = OrderedDict(sorted(_wind_pot.items(), key=lambda t: t[0]))
    #
    meto = get_header('Wind Section')
    #
    header = ['Wind Name', 'Number', 
              'Velocity [{:}/{:}]'.format(units_out[0], units_out[2]),
              'H0 [{:}]'.format(units_out[0]), 'Formula', 
              'Period Ratio', 'Height Exponent', 'Gus Factor']
    meto.append(header)
    #
    i = 0
    _wind_out = {}
    for _name, _group, in _wind_pot.items():
        i+= 1
        _wind = wind[_name]
        _wname = 'Wind_'+ str(i)
        _wind_out[_wname] = _group
        _velocity = float(_wind.velocity) * factors[0]
        _height = float(_wind.height) * factors[0]
        if _wind.formula == 'general':
            meto.append([_wname, i, 
                         str("{:1.3f}").format(_velocity), 
                         _height, 
                         _wind.formula, None,
                         _wind.power, _wind.gust_factor])
        
        elif _wind.formula == 'extreme_api21':
            meto.append([_wname, i, 
                         str("{:1.3f}").format(_velocity), 
                         _height, 
                         _wind.formula, _wind.period_ratio,
                         None, None])
        
        else:
            meto.append([_wname, i, 
                         str("{:1.3f}").format(_velocity), 
                         _height, 
                         _wind.formula, _wind.period_ratio,
                         None, None])
    #
    return meto, _wind_out
#
def get_wind_areas(areas, units_out, factors):
    """
    """
    meto = get_header('Wind Area Section')
    #
    header = ['Area Name', 'Number', 
              'Total Area [{:}2]'.format(units_out[0]), 
              'Drag Coefficient',
              'x_Centroid [{:}]'.format(units_out[0]), 
              'y_Centroid [{:}]'.format(units_out[0]), 
              'z_Centroid [{:}]'.format(units_out[0]),
              'Axis Projection']
    
    meto.append(header)
    _joint_total = []
    #
    for key in sorted(areas, key = lambda name: areas[name].number):
        _area = areas[key]
        
        if _area.wind_pressure[0]:
            _drag = _area.wind_pressure[0][0]
            _suction = _area.wind_pressure[0][1]
            _axis = 'x'
        elif _area.wind_pressure[1]:
            _drag = _area.wind_pressure[1][0]
            _suction = _area.wind_pressure[1][1]
            _axis = 'y'
        elif _area.wind_pressure[2]:
            _drag = _area.wind_pressure[2][0]
            _suction = _area.wind_pressure[2][1]
            _axis = 'z'
        #
        _joints = ['dum' + str(_joint.name) + '_int'
                   for _joint in _area.joints]
        _joint_total.append(len(_joints))
        #
        mass, x_coord, y_coord, z_coord = _area.mass
        #
        _total_area = round(_area._size[0]*_area._size[1] * factors[0]**2, 3)
        _index = len(meto)
        meto.append([_area.name, _area.number, _total_area, _drag, 
                     x_coord * factors[0], 
                     y_coord * factors[0], 
                     z_coord * factors[0], 
                     _axis])
        
        meto[_index].extend(_joints)
    #
    _joint_total = max(_joint_total)
    meto[3].extend(['Joint Name' for x in range(_joint_total)])
    #
    return meto
#
def get_wind_area_combinations(areas, wind):
    """
    """
    meto = get_header('Wind Area Group Section')
    header = ['Group Name', 'Number']
    wind_name = {_wind.name : key for key, _wind in wind.items()}
    #
    _combination = {}
    for key in sorted(areas, key = lambda name: areas[name].number):
        _area = areas[key]
        _name = _area.name
        for _interface in _area.load_interface:
            try:
                _combination[_interface].append(_name)
            except KeyError:
                _combination[_interface] = [_name]
    #
    _wind_comb = []
    _no_comb = []
    for i, (key, _comb) in enumerate(_combination.items()):
        _name = wind_name[key]
        _number = i+1 # wind[_name].number
        #_name = wind[_name_wind].name
        _wind_comb.append([_name, _number])
        _wind_comb[i].extend(list(set(_comb)))
        _no_comb.append(len(_wind_comb[i]))
    #
    _no_comb = max(_no_comb) - 2
    meto.append(header)
    meto[3].extend(['Area Name' for x in range(_no_comb)])
    meto.extend(_wind_comb)
    return meto
#