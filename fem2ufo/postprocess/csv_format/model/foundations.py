# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import datetime
import sys

# package imports
import fem2ufo.process.control as process
import fem2ufo.postprocess.csv_format.operation.process as csv_process

#
#
def get_header(_name):
    """
    """
    meto = []
    meto.append(["------------------------------"])
    meto.append([_name])
    meto.append(["------------------------------"])
    
    return meto
#
#
def get_curve(_curve, springs):
    """
    """
    for _name, _spring in springs.items():
        if _curve.name in _spring:
            return _name
    #
    print('   *** error spring name {:} not found'
          .format(_curve.name))
    sys.exit()
#
def get_layers(layers, springs, units_out, factors):
    """
    """
    output = get_header('Soil Layers Section')
    output.append(['Layer Name', 'Number', 
                   'Depth Below Mudline [{:}]'.format(units_out[0]), 
                   'Sublayers','P-Y', 'T-Z', 'Q-Z'])
    
    _rem = 0
    for key in sorted(layers, key = lambda name: layers[name].number):
        _layer = layers[key]
        #
        py_name = get_curve(_layer.curve['PY'], springs)
        tz_name = get_curve(_layer.curve['TZ'], springs)
        qz_name = get_curve(_layer.curve['QZ'], springs)
        #
        _thick = _layer.depth - _rem
        _rem = _layer.depth
        _layer.sublayers = max(1, int(_thick / _layer.diameter))
        #
        #if _thick < 0.10 * _layer.diameter:
        #    print('-->')
        #
        _name = 'Layer_'+ str(_layer.number)
        output.append([_name, _layer.number, 
                       _layer.depth * factors[0], 
                       _layer.sublayers,
                       py_name, tz_name, qz_name])
    
    return output
#
#
def get_springs(materials, units_out, factors):
    """
    """
    _spring_set = []
    _spring_flat = {}
    
    #
    #for _mat in materials.values():
    for key in sorted(materials, key = lambda name: materials[name].number):
        _mat = materials[key]
        _spring = _mat.spring
        _spring_flat[_spring.name] = [_spring.displacement, _spring.force]
        _spring_set.append(_spring.name)
    #
    _spring_bucket = {}
    for _name, _prof in sorted(_spring_flat.items()):
        _tb_deleted = []
        for _name2 in _spring_set:
            if _name == _name2:
                continue
            #
            _prof2 = _spring_flat[_name2]
            
            if _prof[0] == _prof2[0] and _prof[1] == _prof2[1]:
                #
                try:
                    _spring_bucket[_name].append(_name2)
                except KeyError:
                    _spring_bucket[_name] = [_name, _name2]
                    _tb_deleted.append(_name)
            
                _tb_deleted.append(_name2)
            #
        #
        for _set_name in _tb_deleted:
            try:
                _spring_set.remove(_set_name)
            except:
                print('what error type?')
    #
    #
    for _name in _spring_set:
        _spring_bucket[_name] = [_name]
    #
    meto = get_header('Spring Section')
    new_spring = {}
    
    subfix = 'py'
    index = 100
    meto.extend(select_spring(materials, _spring_bucket, new_spring,
                              subfix, index, units_out, factors))
    
    subfix = 'tz'
    index = 200
    meto.extend(select_spring(materials, _spring_bucket, new_spring,
                              subfix, index, units_out, factors))
    
    subfix = 'qz'
    index = 300
    meto.extend(select_spring(materials, _spring_bucket, new_spring,
                              subfix, index, units_out, factors))
    #
    return meto, new_spring        
#
def select_spring(materials, _spring_bucket, new_spring, 
                  subfix, index, units_out, factors):
    """
    """
    header = ['Spring Name', 'Number', 'Curve Data Points'] 
    _output = []
    for _name in sorted(materials, key = lambda name: materials[name].number):
        _spring = materials[_name].spring
        if not subfix in _spring.name.lower():
            continue
        
        try:
            _group = _spring_bucket[_name]
        except KeyError:
            continue
        
        index += 1
        _sname = 'spring_' + str(index)
        new_spring[_sname] = _group
        #
        _output.append(header)
        if 'qz' in subfix:
            _output.append([_sname, index, 'Force [{:}]'.format(units_out[4])])
        else:
            _output.append([_sname, index, 'Force per unit length [{:}/{:}]'.format(units_out[4], units_out[0])])
        
        row = len(_output)
        # force
        _force = [_item * factors[4] for _item in _spring.force]
        _output[row-1].extend(_force)
        _output.append([None, None, 'Displacement [{:}]'.format(units_out[0])])
        # displacement
        _disp = [_item * factors[0] for _item in _spring.displacement]
        _output[row].extend(_disp)
    #
    return _output
#
def print_soil2csv(soil, location_name, row=False):
    """
    """
    file_name = 'Foundation_' + str(location_name)
    csv_process.print_csv(soil, file_name, row)    
#
#
def get_soil_data(soil, materials):
    """
    """
    spring = {}
    diameter = []
    #
    #for key, item in soil.items():
    for key in sorted(soil, key = lambda name: soil[name].number):
        item = soil[key]    
    
        diameter.append(item.diameter)
        # Py curves
        _name = item.curve['PY'].name
        spring[_name] = materials[_name]
        # Tz curves
        _name = item.curve['TZ'].name
        spring[_name] = materials[_name]
        # Qz curves
        _name = item.curve['QZ'].name
        spring[_name] = materials[_name]        
    #
    diameter = list(set(diameter))
    if len(diameter) > 1:
        print('   *** warning, soil has more than one reference diameter')
        print('   ***          the maximum diameter is selected')
    #
    #
    return spring, max(diameter)
#
#
def print_soil_to_csv(model, units_out):
    """
    """
    # get conversion factors
    units_in = model.data['units']
    factors, _grav = process.units.get_factors_and_gravity(units_in,
                                                           units_out)
    #
    foundation = model.foundation
    today = datetime.date.today()
    header = ['Last Updated : {:}'.format(str(today))]
    #
    for _name, _soil in foundation.soil.items():
        try:
            springs, _diameter = get_soil_data(_soil.layers, 
                                               foundation.materials)
        except AttributeError:
            print('   *** warning soil {:} spring data not found'
                  .format(_name))
            continue
        #
        output = get_header('Foundation Section')
        output.append(['Soil Name', 'Number', 
                       'Pile Diameter [{:}]'.format(units_out[0]),
                       'Mudline Elevation [{:}]:'.format(units_out[0])])
        _soil_name = 'SCurve_' + str(_name)
        _diameter = _diameter * factors[0]
        _mudline = _soil.mudline * factors[0]
        output.append([_soil_name, _soil.number, 
                       _diameter, _mudline])
        #
        _spring_out, _new_springs = get_springs(springs, units_out, factors)
        #
        output.extend(get_layers(_soil.layers, _new_springs, units_out, factors))
        output.extend(_spring_out)
        output.insert(0, header)
        print_soil2csv(output, _name)
#