# 
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import datetime
import copy

# package imports
import fem2ufo.postprocess.csv_format.operation.process as csv_process
import fem2ufo.postprocess.csv_format.model.metocean as csv_meto

#
#
#
def print_lc2csv(level_1, level_2, lc_name, analysis=False,
                 basic_load=True, row=False, flag=False):
    """
    level_1 = Load combination level 1
    level_2 = Load combination level 2
    """
    #
    if analysis:
        _analysis = '_' + analysis
    else:
        _analysis = ""
    #
    today = datetime.date.today()
    # get headers
    header = ['Last Updated : {:}'.format(str(today))]    
    #
    # get level 2 load name (horizontal)
    _combination = [key for key in sorted(level_2, key = lambda name: level_2[name].number)]
    #
    # start load combination matrix
    # get level 1 load name (vertical)
    # [load name, load number, load type, load ID]
    lc_matrix = [[level_1[key].name, level_1[key].number, level_1[key].class_type, key] # key
                 for key in sorted(level_1, key = lambda name: level_1[name].number)]
    #
    #for _case in lc_matrix:
    #    _basic = level_1[_case]
    #
    #
    # start load description
    if basic_load:
        #lc_matrix = []
        #for key in sorted(level_1, key = lambda name: level_1[name].number):
        #    _basic = level_1[key]
        #    if ('basic' in _basic.class_type and not _basic.concept
        #        and not _basic.element and not _basic.node 
        #        and not _basic.gravity and not _basic.areas):
        #        print('    ** Warning basic load {:} is empty --> skip from combination'
        #              .format(_basic.name))
        #        continue
        #    lc_matrix .append([_basic.name, _basic.number, _basic.class_type, key])
        #
        lc_matrix = get_matrix(lc_matrix, level_1, level_2, 
                               _combination, 
                               'Modelled Load Cases',
                               'Adjusted Load Combinations',
                               flag)
    #
    else:
        #lc_matrix = [[level_1[key].name, level_1[key].number, level_1[key].class_type, key] # key
        #             for key in sorted(level_1, key = lambda name: level_1[name].number)]
        
        lc_matrix = get_matrix(lc_matrix, level_1, level_2, 
                               _combination, 
                               'Adjusted Load Combinations',
                               'Factored Load Combinations')
    #
    lc_matrix.insert(0, header) 
    #
    file_name = lc_name + _analysis
    csv_process.print_csv(lc_matrix, file_name, row)
    #
#
def get_factor(_name_index, lc_matrix, blc,
               _column):
    """
    """
    _row = _name_index[blc[0]]
    if lc_matrix[_row][_column]:
        lc_matrix[_row][_column] += blc[1]
    else:
        lc_matrix[_row][_column] = blc[1]
#
#
def get_matrix(lc_matrix, level_1, level_2, 
               _combination, level_1_name,
               level_2_name, flag=False):
    """
    """
    #
    description = [level_1_name, None, None, level_2_name]
    comb_number = ['Name', 'Number', 'Type', 'Number']
    #
    # find level 1 name Vs list index 
    _name_index = {}
    for x in range(len(lc_matrix)):
        #_name_index[lc_matrix[x][0]] = x
        # TODO: quick fix
        _name_index[lc_matrix[x][3]] = x
        lc_matrix[x][3] = None
    #
    for lc_name in _combination:
        # get load combinatio
        _lcomb = level_2[lc_name]
        # add load name
        description.append(_lcomb.name)
        # get load combination number
        comb_number.append(_lcomb.number)        
        # get column position of the load combination
        _column = description.index(_lcomb.name)
        # fill column
        for _lc in lc_matrix:
            _lc.append(None)
        #
        # functional
        for blc in _lcomb.functional_load:
            try:
                _basic = level_1[blc[0]]
                
                if ('basic' in _basic.class_type and not _basic.concept 
                    and not _basic.element and not _basic.node 
                    and not _basic.gravity and not _basic.areas
                    and not _basic.shell and not _basic.acceleration 
                    and not flag):
                    continue
                
                get_factor(_name_index, lc_matrix, blc, _column)
            except KeyError:
                print('    ** Warning in load combination one {:} --> load combination zero {:}'
                      .format(lc_name, blc[0]))
        #
        # wave
        for blc in _lcomb.metocean_load:
            try:
                _name_index[blc[0]]
                get_factor(_name_index, lc_matrix, blc, _column)
            except KeyError:
                continue
    #
    lc_matrix.insert(0, comb_number)
    lc_matrix.insert(0, description)
    return lc_matrix
#
#
#
def print_load_combination_to_csv(model, units_out):
    """
    """
    # get wind areas
    try:
        areas = {key: item for key, item in model.load.equipment.items() 
                 if item.type == 'wind_area'}
    except AttributeError:
        areas = {}
    #
    # FIXME : quick fix to allow genie data to bypass
    # load empty catch
    _flag = False
    if 'genie' in model.data['format'].lower():
        _flag = True
    
    _number = 0
    _analysis = model.analysis
    for _analysis_case, _comb in _analysis.case.items():
        if not _comb.number:
            _number += 1
        # get functional load
        # Remove wind area basic case (i.e. dummy basic case)
        functional = {key:_item for key, _item in model.load.functional.items()
                      if not 'metocean' in _item.class_type} 
                      #and not 'wind area group' in _item.class_type}
        # get wave for level 1
        try:
            seadata = {met.name : met 
                       for met in _comb.metocean_combination.values()}
            # renumber seastates (why?)
            #for _seastate in seadata.values():
            #    _seastate.number = _seastate.wave.number
            # print metocean data
            _units = model.data['units']
            csv_meto.print_meto2csv(seadata, areas, _analysis_case, _number,
                                    units_in=_units, units_out=units_out)
            # add metocean load
            functional.update(seadata)
        except KeyError:
            print('   *** warning no metocean data found')
            pass
        #
        if not _comb.load_combination:
            continue
        #
        # get actual load combined 
        file_name = 'Load_Combination_Actual'
        print_lc2csv(functional, 
                     _comb.load_combination[0], 
                     file_name, analysis=_analysis_case,
                     flag=_flag)
        #
        # get partial load combinations
        try:
            file_name = 'Load_Combination_Factored'
            print_lc2csv(_comb.load_combination[0], 
                         _comb.load_combination[1], 
                         file_name, analysis=_analysis_case,
                         basic_load=False)
        except KeyError:
            print('    ** Warning no Factored Load Combination data found')
    #
    #print('-->')
#
