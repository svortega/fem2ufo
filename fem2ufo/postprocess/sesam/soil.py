# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports

# package imports

# TODO : not yet implemented
# -----------------------------------------------------------------------
#                           Output Section
# ------------------------------------------------------------------------
#
def print_gensoil():
    #
    #
    # -----------------------------------------------------------------
    #  Open Output file
    #
    # FEMinput = raw_input('Enter T.fem input file:')
    temp1 = open('GENSODtoUFO.FEM', 'w')
    #    
    #
    temp1.write("' " + '________________________________________________________________________' + '\n')
    temp1.write("' " + '         Soil Property definitions in SESAM file Format' + '\n')
    temp1.write("' " + '________________________________________________________________________' + '\n')
    temp1.write("' " + ' ' + '\n')
    temp1.write("' " + '          ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ' + '\n')
    #
    j = 1
    NumSpring = 50
    #
    while j <= float(isoil):
        #
        #
        temp1.write("' " + ' ' + '\n')
        temp1.write("' " + '          ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ' + '\n')
        temp1.write("' " + '          ~~~~~~~~~~~~~~~~~  SOIL TYPE ' + str(j) + ' ~~~~~~~~~~~~~~~~~~~~' + '\n')
        temp1.write("' " + '          ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ' + '\n')
        #
        k = 1
        m = 1
        n = 1
        #
        #   PY section
        temp1.write("' " + ' ' + '\n')
        temp1.write("' " + ' ---------------------- PY data for piles ----------------------' + '\n')
        temp1.write("' " + ' ' + '\n')
        #
        while k <= (layers[str(j)]['LAYER']):
            #
            NumSpring = NumSpring + 1
            #        print 'xxxxx',NumSpring,j,k,(Player[str(j)]['Total'])
            #
            temp1.write("' " + ' P-Y ' + 'layer  ' + str(k) + '\n')
            temp1.write('MISOPL' + '  ')
            temp1.write(('%1.8E') % (NumSpring))
            temp1.write('   ' + ' 4.00000000E+00' + '   ' + ('0.00000000E+00' + '   ') * 2 + '\n')
            temp1.write('        ' + ('0.00000000E+00' + '   ') * 4 + '\n')
            #
            temp1.write('        ' + str((Pdata[str(j) + str(k)]['SprT'])) + '\n')
            #
            #
            for x in range(Pdata[str(j) + str(k)]['SprT']):
                #            print x+1,Pdata[str(j)+str(k)]
                #            print (Pdata[str(j)+str(k)]['funit'+str(x+1)])
                #
                temp1.write(('        ' + '%1.8E' + '  ' + '%1.8E' + '\n')
                            % ((Pdata[str(j) + str(k)]['funit' + str(x + 1)]),
                               (Ydata[str(j) + str(k)]['disp' + str(x + 1)])))
            k = k + 1
        #
        temp1.write("' " + ' ' + '\n')
        temp1.write("' " + ' ----------------------  TZ data for piles  ----------------------' + '\n')
        temp1.write("' " + ' ' + '\n')
        #   TZ Section
        while m <= (layers[str(j)]['LAYER']):
            #
            NumSpring = NumSpring + 1
            #        print NumSpring
            #
            temp1.write("' " + ' T-Z ' + 'layer  ' + str(m) + '\n')
            temp1.write('MISOPL' + '  ')
            temp1.write(('%1.8E') % (NumSpring))
            temp1.write('   ' + ' 4.00000000E+00' + '   ' + ('0.00000000E+00' + '   ') * 2 + '\n')
            temp1.write('        ' + ('0.00000000E+00' + '   ') * 4 + '\n')
            temp1.write('        ' + str((Tdata[str(j) + str(m)]['SprT'])) + '\n')
            #
            for x in range(Tdata[str(j) + str(m)]['SprT']):
                temp1.write(('        ' + '%1.8E' + '  ' + '%1.8E' + '\n')
                            % ((Tdata[str(j) + str(m)]['funit' + str(x + 1)]),
                               (Zdata[str(j) + str(m)]['disp' + str(x + 1)])))
            #
            m = m + 1
        #
        temp1.write("' " + ' ' + '\n')
        temp1.write("' " + ' ----------------------  QZ data for piles  ----------------------' + '\n')
        temp1.write("' " + ' ' + '\n')
        #
        #   QZ Section
        while n <= (layers[str(j)]['LAYER']):
            #
            NumSpring = NumSpring + 1
            #        print NumSpring
            #
            temp1.write("' " + ' Q-Z ' + 'layer  ' + str(n) + '\n')
            temp1.write('MISOPL' + '  ')
            temp1.write(('%1.8E') % (NumSpring))
            temp1.write('   ' + ' 2.00000000E+00' + '   ' + ('0.00000000E+00' + '   ') * 2 + '\n')
            temp1.write('        ' + ('0.00000000E+00' + '   ') * 4 + '\n')
            temp1.write('        ' + str((Qdata[str(j) + str(n)]['SprT'])) + '\n')
            #
            for x in range(Qdata[str(j) + str(n)]['SprT']):
                temp1.write(('        ' + '%1.8E' + '  ' + '%1.8E' + '\n')
                            % ((Qdata[str(j) + str(n)]['funit' + str(x + 1)]),
                               (QZdata[str(j) + str(n)]['disp' + str(x + 1)])))
            #
            n = n + 1
        #
        j = j + 1
    #
    #
    print('--- End REDSOIL module')
    #
    temp1.close()
    #
    #
    #
    #
    #  -------------------------------------------------------------------
    #
    #
