#
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import math



#
# ====================================================================
#
#
def printHeader(userData):
    import datetime
    # import time
    from time import gmtime, strftime

    TFEMmod = []
    _today = datetime.date.today()
    _time = strftime("%H:%M:%S", gmtime())

    TFEMmod.append("IDENT     1.00000000E+00  1.00000000E+00  3.00000000E+00  0.00000000E+00\n")
    TFEMmod.append("DATE      1.00000000E+00  0.00000000E+00  4.00000000E+00  7.20000000E+01\n")
    TFEMmod.append("        DATE:     {:%d-%b-%Y}".format(_today))
    TFEMmod.append("         TIME:          {:}\n".format(_time))
    TFEMmod.append("        PROGRAM:  SESAM GeniE         VERSION:       {:}\n"
                   .format(userData['TFEM'].version1))
    TFEMmod.append("        COMPUTER: X86 Windows         INSTALLATION: \n")
    TFEMmod.append("        USER:     chava               ACCOUNT: \n")
    TFEMmod.append("DATE      1.00000000E+00  0.00000000E+00  4.00000000E+00  7.20000000E+01\n")
    TFEMmod.append("        DATE:     {:%d-%b-%Y}".format(_today))
    TFEMmod.append("         TIME:          {:}\n".format(_time))
    TFEMmod.append("        PROGRAM:  SESAM Gamesha       VERSION:       {:}\n"
                   .format(userData['TFEM'].version2))
    TFEMmod.append("        COMPUTER: X86 Windows         INSTALLATION: \n")
    TFEMmod.append("        USER:     chava               ACCOUNT: \n")
    # TFEMmod.append("\n")
    return TFEMmod


#
def printNode(node):
    TFEMmod = []

    # Node number
    for _node in node.values():
        # print(_node.name)
        TFEMmod.append("GNODE     ")
        TFEMmod.append("{:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(_node.name, _node.number, _node.dof, _node.dummy))
    # Coordinate
    for _node in node.values():
        TFEMmod.append("GCOORD    ")
        TFEMmod.append("{:1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                       .format(_node.number,
                               _node.coordinate[0], _node.coordinate[1], _node.coordinate[2]))
        # Boundary
    for _node in node.values():
        try:
            NoDOF = len(_node.boundary)
            _test = 1.0 / NoDOF
            TFEMmod.append("BNBCD     ")
            TFEMmod.append("{:1.8E}  {:1.8E}".format(_node.number, NoDOF))

            printIter(TFEMmod, _node.boundary, 0, 2)
            TFEMmod.append("\n")
        except:
            pass
    # Mass
    for _node in node.values():
        try:
            NoDOF = len(_node.mass)
            _test = 1.0 / NoDOF
            TFEMmod.append("BNMASS    ")
            TFEMmod.append("{:1.8E}  {:1.8E}".format(_node.number, NoDOF))

            printIter(TFEMmod, _node.mass, 0, 2)
            TFEMmod.append("\n")
        except:
            pass

    return TFEMmod


#
def printMaterial(material):
    TFEMmod = []

    for _mat in material.values():
        TFEMmod.append("TDMATER   {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(4, _mat.number, _mat.dummy, 0))
        TFEMmod.append("        {:}\n".format(_mat.name))

    for _mat in material.values():
        TFEMmod.append("MISOSEL   {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(_mat.number, _mat.E, _mat.poisson, _mat.density))
        TFEMmod.append("          {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(0, _mat.alpha, _mat.type, _mat.Fy))

    return TFEMmod


#
def printGenSec(section):
    TFEMmod = []

    for _sec in section.values():
        try:
            _try = int(_sec.dummy)
        except:
            _ncnam = len(_sec.name)
            _nlnam = 1
            _sec.dummy = _nlnam * 100 + _ncnam

        TFEMmod.append("TDSECT    {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(4, _sec.number, _sec.dummy, 0))
        TFEMmod.append("        {:}\n".format(_sec.name))

    for _sec in section.values():
        if _sec.shape.upper() == "PLATE":
            TFEMmod.append("GELTH    {:1.8E}  {:1.8E}\n".format(_sec.number, _sec.properties.D))

    for _sec in section.values():
        if _sec.shape.upper() == "PLATE": continue

        TFEMmod.append("GBEAMG    {:1.8E}  {:1.8E}  ".format(_sec.number, 0))
        # for _prop in _sec.properties:
        TFEMmod.append("{:1.8E}  {:1.8E}\n".format(_sec.properties.Area, _sec.properties.J))
        TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                       .format(_sec.properties.Iip, _sec.properties.Iop, _sec.properties.Iyz,
                               _sec.properties.ZeJ))
        TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                       .format(_sec.properties.Zeip, _sec.properties.Zeop,
                               _sec.properties.shearAreaV, _sec.properties.shearAreaH))

        TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                       .format(_sec.properties.SCV, _sec.properties.SCH,
                               _sec.properties.SV, _sec.properties.SH))

    return TFEMmod


#
def printISec(section):
    TFEMmod = []

    for _sec in section.values():
        if 'IBEAM' == _sec.shape.upper():
            TFEMmod.append("GIORH     {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_sec.number, _sec.properties.D,
                                   _sec.properties.Tw, _sec.properties.Bft))

            TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                           .format(_sec.properties.Tft, _sec.properties.Bfb,
                                   _sec.properties.Tfb, _sec.SFV))

            TFEMmod.append("         {: 1.8E}\n".format(_sec.SFH))

    return TFEMmod


#
def printUnISec(section):
    TFEMmod = []

    for _sec in section.values():
        if 'UIBEAM' == _sec.shape.upper():
            TFEMmod.append("GUSYI     {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_sec.number, _sec.properties.D,
                                   _sec.properties.Tw, _sec.properties.Bft))

            TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                           .format(_sec.properties.B1, _sec.properties.Tft,
                                   _sec.properties.Bfb, _sec.properties.B2))

            TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E}\n"
                           .format(_sec.properties.Tfb, _sec.SFV,
                                   _sec.SFH))

    return TFEMmod


#
def printCHSec(section):
    TFEMmod = []

    for _sec in section.values():
        if 'CHANNEL' == _sec.shape.upper():
            TFEMmod.append("GCHAN     {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_sec.number, _sec.properties.D,
                                   _sec.properties.Tw, _sec.properties.Bft))

            TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                           .format(_sec.properties.Tft, _sec.SFV,
                                   _sec.SFH, 0))

            TFEMmod.append("         {: 1.8E}\n".format(_sec.properties.theta))

    return TFEMmod


#
def printBoxSec(section):
    TFEMmod = []

    for _sec in section.values():
        if 'BOX' == _sec.shape.upper():
            TFEMmod.append("GBOX      {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_sec.number, _sec.properties.D,
                                   _sec.properties.Tw, _sec.properties.Tfb))

            TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                           .format(_sec.properties.Tft, _sec.properties.Bft,
                                   _sec.SFV, _sec.SFH))

    return TFEMmod


#
def printTubSec(section):
    TFEMmod = []

    for _sec in section.values():
        if 'PIPE' == _sec.shape.upper():
            _DY = _sec.properties.D
            _T = _sec.properties.Tw
            _DI = _DY - 2 * _T
            TFEMmod.append("GPIPE     {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_sec.number, _DI, _DY, _T))

            TFEMmod.append("         {: 1.8E} {: 1.8E}\n"
                           .format(_sec.SFV, _sec.SFH))

    return TFEMmod


#
def printVector(vector):
    TFEMmod = []

    for _vec in vector.values():
        TFEMmod.append("GUNIVEC   {:1.8E}".format(_vec.number))
        # for _vec in _elem.guidepoint:
        TFEMmod.append(" {: 1.8E} {: 1.8E} {: 1.8E}\n"
                       .format(_vec.cos[0], _vec.cos[1], _vec.cos[2]))
        # TFEMmod.append("\n")
    return TFEMmod


#
def printEccen(eccen):
    TFEMmod = []

    for _ecc in eccen.values():
        TFEMmod.append("GECCEN    {:1.8E}".format(_ecc.number))
        TFEMmod.append(" {: 1.8E} {: 1.8E} {: 1.8E}\n"
                       .format(_ecc.eccentricity[0], _ecc.eccentricity[1],
                               _ecc.eccentricity[2]))

    return TFEMmod


#
def printHinge(hinge):
    TFEMmod = []

    for _hinge in hinge.values():
        TFEMmod.append("BELFIX    {:1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                       .format(_hinge.number, _hinge.option, _hinge.trano, 0))

        TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                       .format(_hinge.fix[0], _hinge.fix[1], _hinge.fix[2], _hinge.fix[3]))
        TFEMmod.append("         {: 1.8E} {: 1.8E}\n"
                       .format(_hinge.fix[4], _hinge.fix[5]))

    return TFEMmod


#
def printElemData(element, node):
    TFEMmod = []
    for _elem in element.values():
        TFEMmod.append("GELMNT1   {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(_elem.name, _elem.number, _elem.type.number, _elem.typeAd))
        TFEMmod.append("        ")
        _NoSteps = printData(TFEMmod, node, _elem.node)
        TFEMmod.append("\n")

    return TFEMmod


#
def printElemRef(element):
    TFEMmod = []
    for _elem in element.values():
        TFEMmod.append("GELREF1   {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}"
                       .format(_elem.number, _elem.material.number, _elem.addno,
                               _elem.intno))
        # TFEMmod.append("        ")
        _NoSteps = printIter(TFEMmod, _elem.stress, 0, 4)
        TFEMmod.append("\n")
        TFEMmod.append("        ")
        _NoSec = printNo(TFEMmod, _elem.section)
        _NoRel = printNo(TFEMmod, _elem.releases)
        _NoOff = printNo(TFEMmod, _elem.offsets)
        _NoGui = printNo(TFEMmod, _elem.guidepoint)
        # TFEMmod.append("\n")
        # TFEMmod.append("        ")
        _NoSteps = 4
        if _NoSec > 1:
            _NoSteps = printIter(TFEMmod, _elem.section, 0, _NoSteps)
        if _NoRel > 1:
            _NoSteps = printIter(TFEMmod, _elem.releases, 0, _NoSteps)
        if _NoOff > 1:
            _NoSteps = printIter(TFEMmod, _elem.offsets, 0, _NoSteps)
        if _NoGui > 1:
            _NoSteps = printIter(TFEMmod, _elem.guidepoint, 0, _NoSteps)
        TFEMmod.append("\n")

    return TFEMmod


#
def printData(_master, _item, _ini=0, _noRows=0):
    TFEMmod = []
    _NoParts = len(_item)

    for x in range(_ini, _NoParts):

        if _noRows == 4 and x <= _NoParts:
            TFEMmod.append("\n")
            TFEMmod.append("        ")
            _noRows = 0

        _number = _master[_item[x].name].number

        TFEMmod.append("  {:1.8E}".format(_number))
        _noRows += 1

    return _noRows, TFEMmod


#
def printNo(_item):
    TFEMmod = []
    _NoItems = len(_item)
    if _NoItems > 1:
        TFEMmod.append(" {: 1.8E}".format(-1))
    else:
        try:
            TFEMmod.append(" {: 1.8E}".format(_item[0].number))
        except:
            TFEMmod.append(" {: 1.8E}".format(0))

    return _NoItems, TFEMmod


#
#
def printSet(sets, elements):
    TFEMmod = []

    for _set in sets.values():
        TFEMmod.append("TDSETNAM  {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(4, _set.number, _set.codnam, _set.codtxt))
        TFEMmod.append("         {:}\n".format(_set.name))

    _flag = 0
    for _set in sets.values():
        _slots = 1024
        try:
            _Nitems = len(_set.items)
            _items = _Nitems
            _try = 1.0 / _Nitems
            _flag += 1
            _times = math.ceil(_Nitems / 1024.0)
            _nfield = _Nitems + (5 * _times)

            for _step in range(_times):
                # if _slots == 1024 :
                _ini = (1024 - 5) * _step
                _NoParts = min(_Nitems, (1024 - 5) * (_step + 1))
                if _flag > 1: TFEMmod.append("\n")
                # TFEMmod.append("\n")
                TFEMmod.append("GSETMEMB  {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                               .format(min(_items + 5, 1024), _set.number, (_step + 1), _set.type))
                TFEMmod.append("          {:1.8E}".format(_set.isorig))

                _items -= 1024 - 5
                # _slots = 0
                # _nfield = _nfield - 1024 - 5
                # _slots += 1
                printIter2(TFEMmod, _set.items, elements, _NoParts, _ini, 1)
                # TFEMmod.append("\n")

        except:
            pass

    return TFEMmod


#
def printIter2(_item, _elements, _NoParts, _ini=0, _noRows=0):
    TFEMmod = []
    for x in range(_ini, _NoParts):

        if _noRows == 4 and x < _NoParts:
            TFEMmod.append("\n")
            TFEMmod.append("        ")
            _noRows = 0
        _number = _elements[_item[x]].number
        TFEMmod.append("  {:1.8E}".format(_number))
        _noRows += 1
        # TFEMmod.append("\n")
    #
    return TFEMmod


#
def printLoad(load, node, element):
    TFEMmod = []
    # TFEMmod.append("\n")
    for _load in load.values():
        TFEMmod.append("\n")
        TFEMmod.append("TDLOAD    {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(4, _load.number, _load.dummy, 0))
        TFEMmod.append("        {:}".format(_load.name))

    for _load in load.values():
        try:
            _try = 1.0 / len(_load.gravity)
            TFEMmod.append("\n")
            TFEMmod.append("BGRAV     {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}"
                           .format(_load.number, 0, 0, 0))
            _NoSteps = printIter(TFEMmod, _load.gravity, 0, 4)
        except:
            pass

    for _load in load.values():
        # try:
        # _try = 1.0/len(_load.node)

        for _node in _load.node:
            # _number = node[_node.number].number

            TFEMmod.append("\n")
            TFEMmod.append("BNLOAD    {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_load.number, _node.type, _node.complex, 0))

            # TFEMmod.append("         {: 1.8E} {: 1.8E}".format(_number, _node.dof))
            TFEMmod.append("         {: 1.8E} {: 1.8E}".format(_node.number, _node.dof))
            _NoSteps = printIter3(TFEMmod, _node.point, 0, 2)
            # TFEMmod.append("\n")
            # except : pass

    for _load in load.values():
        # try:
        # _try = 1.0/len(_load.element)

        for _elem in _load.element:
            # _number = element[_elem.number].number

            TFEMmod.append("\n")
            TFEMmod.append("BELOAD1   {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_load.number, _elem.type, _elem.complex, _elem.option))
            TFEMmod.append("         {: 1.8E} {: 1.8E} {: 1.8E} {: 1.8E}\n"
                           .format(_elem.number, _elem.distance[0], _elem.distance[1], _elem.dof))
            # .format(_number, _elem.distance[0],_elem.distance[1],_elem.dof))
            TFEMmod.append("         {: 1.8E}".format(0))
            _NoSteps = printIter3(TFEMmod, _elem.distributed, 0, 1)
            # TFEMmod.append("\n")
            # except : pass
    return TFEMmod


#
def printIter3(_item, _ini=0, _noRows=0):
    TFEMmod = []
    _NoParts = len(_item)
    # _try= 1.0/_NoParts

    for x in range(_ini, _NoParts):

        if _noRows == 4 and x <= _NoParts:
            TFEMmod.append("\n")
            TFEMmod.append("        ")
            _noRows = 0

        TFEMmod.append(" {: 1.8E}".format(_item[x]))
        _noRows += 1

    # except : pass

    return _noRows, TFEMmod


#
def printEOF():
    TFEMmod = []
    TFEMmod.append("\n")
    TFEMmod.append("IEND     {:} {:1.2f} {:} {:1.2f} {:} {:1.2f} {:} {:1.2f}\n"
                   .format(10 * ' ', 0, 10 * ' ', 0, 10 * ' ', 0, 10 * ' ', 0))
    return TFEMmod


#
# ---
#
def printConText(concept):
    TFEMmod = []

    for _conc in concept.values():
        TFEMmod.append("TDSCONC   {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(4, _conc.number, _conc.codname, _conc.codtext))
        TFEMmod.append("        {:}\n"
                       .format(_conc.name))

    return TFEMmod


#
def printIter(_item, _ini=0, _noRows=0):
    TFEMmod = []
    _NoParts = len(_item)
    # _try= 1.0/_NoParts

    for x in range(_ini, _NoParts):

        if _noRows == 4 and x <= _NoParts:
            TFEMmod.append("\n")
            TFEMmod.append("        ")
            _noRows = 0

        TFEMmod.append(" {: 1.8E}".format(_item[x]))
        _noRows += 1

    # except : pass

    return _noRows, TFEMmod


#
def printConcept(concept):
    TFEMmod = []
    for _conc in concept.values():
        TFEMmod.append("SCONCEPT  {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                       .format(8, _conc.number, _conc.type, _conc.role))

        _NoJoints = len(_conc.joint)

        try:
            _NoParts = len(_conc.part)
            _try = 1.0 / _NoParts

            TFEMmod.append("          {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_conc.parent, _NoParts, _NoJoints, _conc.part[0]))
            # _NoSteps = 1

            if _NoParts > 1:
                TFEMmod.append("        ")
                _NoSteps = printIter(TFEMmod, _conc.part, 1)

            if _NoJoints > 0:
                _NoSteps = printIter(TFEMmod, _conc.joint, 0, _NoSteps)
                TFEMmod.append("\n")

            if _NoJoints == 0 and _NoParts > 1: TFEMmod.append("\n")

        except:
            try:
                _try = 1.0 / _NoJoints

                TFEMmod.append("          {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                               .format(_conc.parent, 0, _NoJoints, _conc.joint[0]))

                if _NoJoints > 1:
                    # _NoParts = 1
                    TFEMmod.append("        ")
                    _NoSteps = printIter(TFEMmod, _conc.joint, 1)
                    TFEMmod.append("\n")

            except:
                pass

    return TFEMmod


#
def printConMesh(concept):
    TFEMmod = []

    for _conc in concept.values():

        try:
            _NoJoint = len(_conc.joint)
            _try = 1.0 / _NoJoint
            _typeJoint = 1
            _numRep = 1
            # if _NoJoint > 0 : _numRep = 1
        except:
            _NoJoint = 0
            _numRep = 0

        try:
            _NoParts = len(_conc.part)
            _try = 1.0 / _NoParts
            _numRep = _numRep + 1
            _typePart = 2
            _noField = _NoParts + _NoJoint + 5

            TFEMmod.append("SCONMESH  {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_noField, _conc.number, _numRep, _typePart))
            TFEMmod.append("          {:1.8E}".format(_NoParts))
            _NoSteps = printIter(TFEMmod, _conc.part, 0, 1)

        except:
            _NoParts = 0
            _NoSteps = 1
            _noField = _NoJoint + 5
            TFEMmod.append("SCONMESH  {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
                           .format(_noField, _conc.number, _numRep, _typePart))
            TFEMmod.append("          {:1.8E}".format(_typeJoint))

        if _NoJoint > 0:
            # TFEMmod.append("  {:1.8E}".format(_typeJoint))
            _NoSteps = _NoSteps + 1
            _NoSteps = printIter(TFEMmod, _conc.joint, 0, _NoSteps)

        TFEMmod.append("\n")

        # TFEMmod.append("SCONMESH  {:1.8E}  {:1.8E}  {:1.8E}  {:1.8E}\n"
        #              .format(4,_conc.number,_NoItems, _conc.codtext))
        # TFEMmod.append("         {:}\n".format(_conc.name))
    return TFEMmod


#
# ---
# concTemp, meshSC,
def printFile(model, concept, master):
    #
    print('')
    print('------------------- MODFEM Module ------------------')
    print('--- Writing modFEM file')
    #
    # element, material, section, node,
    # vector, eccen, hinge, sets, load,
    # userData,
    #
    #
    # TFEMmod = open(fileMod,'w+')
    # -- user data
    TFEMmod = printHeader(model.data)
    # -- material
    TFEMmod.extend(printMaterial(model.materials))
    # -- section properties
    TFEMmod.extend(printGenSec(model.sections))
    # -- geometry
    TFEMmod.extend(printISec(model.sections))
    TFEMmod.extend(printUnISec(model.sections))
    TFEMmod.extend(printCHSec(model.sections))
    TFEMmod.extend(printBoxSec(model.sections))
    TFEMmod.extend(printTubSec(model.sections))
    #
    # --- Concept Section ---
    # For now, there is no point to re-write full sesam file

    # if  'sesamXX' in master['output'].keys():
    # -- Structure Concept Referable Text
    # printConText(concept, fileMod)
    # -- Structure Concept Definition
    # printConcept(concTemp, fileMod)
    # --Structure Concept Finite Element Representation
    # printConMesh(meshSC, fileMod)
    # SPROSELE -- Structure Concept Property Selector
    #
    # -- vectors
    TFEMmod.extend(printVector(model.vectors))
    # -- eccentricity
    TFEMmod.extend(printEccen(model.eccentricities))
    # -- Flexible Join/Hinge
    TFEMmod.extend(printHinge(model.hinges))
    # -- 
    # -- nodes
    TFEMmod.extend(printNode(model.nodes))
    #
    # -- Element Data
    TFEMmod.extend(printElemData(model.elements, model.nodes))
    TFEMmod.extend(printElemRef(model.elements))
    # -- Sets
    TFEMmod.extend(printSet(model.sets, model.elements))
    #
    # -- Load
    TFEMmod.extend(printLoad(model.loads, model.nodes,
                             model.elements))
    #
    TFEMmod.extend(printEOF())
    #
    print('--- End writing modFEM file')
    #
    return TFEMmod
    #
    #
