# 
# Copyright (c) 2009-2021 fem2ufo
# 


# Python stdlib imports
import cmath
#import math
import copy

# package imports
import fem2ufo.f2u_model.control as f2u
from fem2ufo.preprocess.sesam.operations.element_library import element_type
import fem2ufo.process.control as process
import fem2ufo.preprocess.sesam.operations.element_library as sesam_library
import fem2ufo.preprocess.sacs.operations.process as sacs_process

#
def dcosr(v1, v2):
    """
    calculates direction cosines of a vectors
    """
    # nodal coordinates vector 1
    x1, y1, z1 = v1.get_coordinates()
    # nodal coordinates vector 2
    x2, y2, z2 = v2.get_coordinates()
    #
    xd = x2 - x1
    yd = y2 - y1
    zd = z2 - z1
    d = (xd ** 2 + yd ** 2 + zd ** 2)**0.50
    al = xd / d
    am = yd / d
    an = zd / d
    return al, am, an
#
#
def vector_angle(beam_1, beam_2):
    """
    """
    # find direction cosines chord 1
    im1 = beam_1.node[0]
    jm1 = beam_1.node[1]
    c1l, c1m, c1n = dcosr(im1, jm1)

    # find direction cosines chord 2
    im2 = beam_2.node[0]
    jm2 = beam_2.node[1]
    c2l, c2m, c2n = dcosr(im2, jm2)

    # find angle between chord 1 & 2
    _ang_complex = cmath.acos(c1l * c2l + c1m * c2m + c1n * c2n)
    _ang = _ang_complex.real

    # print('angle', math.degrees(_ang))

    return _ang


#
#
def linear_dependency(geometry, _factors):
    """
    """
    _boundary_list = [key for key,_boundary in geometry.boundaries.items()
                      if 'master' in _boundary.type.lower()]
    #
    _dummy = {}
    for key in _boundary_list:
        _boundary = geometry.boundaries[key]
        _master = geometry.nodes[_boundary.name]
        
        _intend = []
        for _slave_name in _boundary.slave:
            _slave = geometry.nodes[_slave_name]
            _intend.append(_master.equal(_slave))
        #
        # node master/slave in the same coord
        if all(_intend):
            zero_spring(geometry, _factors)
            #print('---> short')
        # hybrid modelling
        else:
            _dummy = spokes_ufo(geometry)
            #print('---> long')
    #
    return _dummy
#
def zero_spring(geometry, _factors):
    """
    """
    nameNG = '_shim_'
    _set_spokes = {}
    
    _elem_number = [_memb.number for _memb in geometry.elements.values()]
    _elem_number = max(_elem_number)
    _elem_number -= _elem_number % -100
    
    _mat_number =  [_mat.number for _mat in geometry.materials.values()]
    _mat_number = max(_mat_number)
    _mat_number -= _mat_number % -100
    _mat_number += 5000
    
    _boundary_list = [key for key,_boundary in geometry.boundaries.items()
                      if 'master' in _boundary.type.lower()]
    
    _spkNo = 1
    for key in _boundary_list:
        _boundary = geometry.boundaries[key]
        #_spokes = []
        _master = geometry.nodes[_boundary.name]
        _element = geometry.elements[_master.elements[0]]
        #
        for _slave_name in _boundary.slave:
            try:
                _slave = geometry.nodes[_slave_name]
                _slave_boundary = geometry.boundaries[_slave.boundary[0]]
                # new material
                _mat_number += 1
                _mat_name = 'spring_' + str(_mat_number)
                geometry.materials[_mat_number] = f2u.Material(_mat_name, _mat_number)
                geometry.materials[_mat_number].type = "spring"                
            except IndexError:
                continue
            _elem_number += 1
            _elem_name  = 'spring' + str(_elem_number).zfill(4)
            #_spokes.append(_elem_name)
            geometry.elements[_elem_name] = f2u.Geometry.SpringElement(_elem_name,
                                                                       _elem_number)
            #geometry.elements[_elem_name].type.element = "shim"
            geometry.elements[_elem_name].type = sesam_library.element_type[40]
            #
            geometry.elements[_elem_name].section = [_element.section[0]]
            geometry.elements[_elem_name].material = [geometry.materials[_mat_number]]
            geometry.elements[_elem_name].node.append(geometry.nodes[_master.name])
            geometry.elements[_elem_name].node.append(geometry.nodes[_slave.name])
            # reset guidepoint
            _slave.boundary = []
            geometry.elements[_elem_name].guidepoint = []
            #
            geometry.materials[_mat_number].elements.append(_elem_name)
            #
            _mat_no = int(_mat_number) * 1000
            _unit = _factors[0]
            _temp = {}
            for _depen in _slave_boundary.dependence:
                if _depen != 0:
                    try:
                        _temp[_depen]
                    except KeyError:
                        _mat_no += 1
                        _forc = [abs(_depen) * - _factors[4], 0,
                                 abs(_depen) * _factors[4]]
                        _disp = [-1 * _unit, 0,  _unit]
                        _temp[_depen] = f2u.Geometry.SpringElement('spring_' + str(_mat_no), _mat_no)
                        _temp[_depen].displacement = _disp
                        _temp[_depen].force = _forc
                    
                    geometry.materials[_mat_number].spring.append(_temp[_depen])
                else:
                    geometry.materials[_mat_number].spring.append(0)
        #if _spokes:
        #    try:
        #        _set_spokes[nameNG].items.extend(_spokes)
        #    except:
        #        _set_spokes[nameNG] = f2u.Geometry.Groups(nameNG, _spkNo)
        #        _set_spokes[nameNG].items = _spokes
        #        _spkNo += 1
    #
    #_dummy = {}
    #
    #if _set_spokes:
    #    process.geometry.add_new_groups(geometry.sets, _set_spokes, nameNG)
    #
    # print('-->')
    #return _dummy
#
#
def get_geom_number(geometry):
    """ """
    _sec_number = [_sec.number for _sec in geometry.sections.values()]
    _sec_number = max(_sec_number)
    _sec_number -= _sec_number % -10000
    
    _elem_number = [_memb.number for _memb in geometry.elements.values()]
    _elem_number = max(_elem_number)
    _elem_number -= _elem_number % -10000
    
    _mat_number =  [_mat.number for _mat in geometry.materials.values()]
    _mat_number = max(_mat_number)
    _mat_number -= _mat_number % -10000
    
    return _sec_number, _elem_number, _mat_number
    
#
#
def spokes_ufo(geometry):
    """
    """
    nameNG = 'spokes'
    _set_spokes = {}
    
    _sec_number, _elem_number, _mat_number = get_geom_number(geometry)
    
    _boundary_list = [key for key,_boundary in geometry.boundaries.items()
                      if 'master' in _boundary.type.lower()]
    
    _spkNo = 1
    for key in _boundary_list:
    #for _boundary in geometry.boundaries.values():
        #if _boundary.type == 'master':
        _boundary = geometry.boundaries[key]
        _spokes = []
        _master = geometry.nodes[_boundary.name]
        _element = geometry.elements[_master.elements[0]]
        # new section
        _sec_number += 1
        geometry.sections[_sec_number] = copy.deepcopy(_element.section[0])
        geometry.sections[_sec_number].name = 'spoke_' + str(_sec_number).zfill(4)
        geometry.sections[_sec_number].number = _sec_number
        # new material
        _mat_number += 1
        geometry.materials[_mat_number] = copy.deepcopy(_element.material[0])
        geometry.materials[_mat_number].density = 0
        geometry.materials[_mat_number].number = _mat_number
        geometry.materials[_mat_number].name = 'spoke_'
        #
        for _slave_name in _boundary.slave:
            _slave = geometry.nodes[_slave_name]
            _elem_number += 1
            _elem_name  = 'spoke' + str(_elem_number).zfill(4)
            _spokes.append(_elem_name)
            geometry.elements[_elem_name] = copy.deepcopy(_element)
            geometry.elements[_elem_name].section = [geometry.sections[_sec_number]]
            geometry.elements[_elem_name].name = _elem_name
            geometry.elements[_elem_name].number = _elem_number
            geometry.elements[_elem_name].material = [geometry.materials[_mat_number]]
            geometry.elements[_elem_name].node[0] = geometry.nodes[_master.name]
            geometry.elements[_elem_name].node[1] = geometry.nodes[_slave.name]
            # reset guidepoint
            geometry.elements[_elem_name].guidepoint = []
            _slave.boundary = []
        #
        if _spokes:
            _set_spokes[_spkNo] = f2u.Geometry.Groups(_spkNo, nameNG)
            _set_spokes[_spkNo].items = _spokes
            _spkNo += 1
    #
    _dummy = {}
    
    if _set_spokes:
        process.geometry.add_new_groups(geometry.sets, _set_spokes, nameNG)
    #
    return _dummy
#
# joint
#
def joint_ufo(geometry):
    """
    Joint member's eccentricity is removed to avoid double tipping
    [usfos incorporates member eccentricity based on geometry]
    """
    print('    * Removing members offset at joint end')
    #
    geometry.elements, _msg = modify_joint(geometry.joints,
                                           geometry.elements)

    return _msg


#
def modify_joint(_joint, _element):
    """
    """
    check_out = []
    check_out.append("'{:} Removing brace's offset\n".format(58 * "-"))

    _offset_off = []
    _member_off = []
    #
    for _jnt in _joint.values():
        #
        for _memb_no in _jnt.brace:
            try:
                _memb = _element[_memb_no]
            
            except KeyError:
                _member_off.append(_memb_no)
                continue
            
            _node_no = 0
            if _memb.node[1].number == _jnt.node.number:
                _node_no = 1

            try:
                _element[_memb_no].offsets[_node_no] = 0

            except IndexError:
                _offset_off.append(_memb_no)
                continue
    #
    if _offset_off:
        check_out.append("'\n")
        check_out.append("'{:} ** Warning Elements have not Eccentricity\n"
                         .format(40 * " "))
        check_out.extend(process.common.printing.print_column_num(_offset_off))

    if _member_off:
        check_out.append("'\n")
        check_out.append("'{:} ** Warning Elements not Found\n".format(58 * " "))
        check_out.extend(process.common.printing.print_column_num(_member_off))

    return _element, check_out
    #


#
# FIXME : check if works
# TODO : this module should be moved to ufout.py
#
def find_duplicated_shims(key, geometry, _zero_length):
    """
    """
    _elem = geometry.elements[key]
    #if _elem.material[0].type == "shim":
    #
    # remove shimp from node's group
    _ag = {key}
    # remove shim from node group
    _bg = set(_elem.node[0].elements)
    _cg = _bg - _ag
    # checking if dupplicated shim elements
    _elem.node[0].elements = []
    for _cg2 in _cg:
        if geometry.elements[_cg2].length_node2node > 0:
            _elem.node[0].elements.append(_cg2)
        else:
            print('    ** warning duplicated shim : {:}'
                  .format(_cg2))
            _zero_length.append(_cg2)
    #
    _bg = set(_elem.node[1].elements)
    _cg = _bg - _ag
    # checking if dupplicated shim elements
    _elem.node[1].elements = []
    for _cg2 in _cg:
        if geometry.elements[_cg2].length_node2node > 0:
            _elem.node[1].elements.append(_cg2)
        else:
            print('    ** warning duplicated shim : {:}'
                  .format(_cg2))
            _zero_length.append(_cg2)
#
#
#
def get_branches(_stubs, geometry):
    """
    """
    _atol = abs(1.5 * cmath.pi / 180.0)
    #
    _joint = f2u.Geometry.Joints(name = _stubs.name,
                                 node = _stubs)
    #
    _branch = {}
    for _membNo in _stubs.elements:
        _member = geometry.elements[_membNo]
        _n0 = _member.node[0]
        _n1 = _member.node[1]
        _branch[_membNo] = [_n0, _n1]
    #
    _chord, _brace = process.joint.find_chord_brace(_joint, 
                                                    geometry.elements,
                                                    _branch,_atol)
    #
    return _chord, _brace
#
def get_vertical_element(_elem, _stubs, _chord, geometry):
    """
    """
    _elem_stub = geometry.elements[_stubs.elements[0]]
    _vector_stub = dcosr(_elem_stub.node[0],
                         _elem_stub.node[1])
    
    _elem_chord = geometry.elements[_chord.elements[0]]
    _vector_chord = dcosr(_elem_chord.node[0],
                          _elem_chord.node[1])
    
    if abs(_vector_chord[2]) > abs(_vector_stub[2]):
        _elem.node[1] = _stubs  # slave
        _elem.node[0] = _chord  # master 
    else:
        _elem.node[1] = _chord  # slave
        _elem.node[0] = _stubs  # master
#
def process_spring_ufo(geometry, _factors, _shim_gap=False):
    """
    Modify spring material behaviour
    """
    check_out = []
    check_out.append("'\n")
    check_out.append("'{:} Processing Shim Elements\n".format(57 * " "))
    _shim_stub_off = []
    _shim_off = []
    _chord_on = []
    _zero_length = []
    #
    #
    _spring_list = [_grp for _grp in geometry.sets.values()
                    if _grp.name == '_spring_']
    for _grp in _spring_list:
        for _no in _grp.items:
            _mat = geometry.materials[_no]
            _k = _mat.stiffness[0]
            _mat_no = int(_mat.number) * 1000
            for i in range(len(_k)):
                _mat.spring.append(0)
                if _k[i][i] != 0:
                    _mat_no += 1
                    _forc = [abs(_k[i][i]) * - _factors[4], 0, abs(_k[i][i]) * _factors[4]]
                    _disp = [-1.0 * _factors[0], 0, 1.0 * _factors[0]]
                    _temp = f2u.Geometry.SpringElement('spring_' + str(_mat_no), _mat_no)
                    _temp.displacement = _disp
                    _temp.force = _forc
                    _mat.spring[i] = _temp
        # empty group
        _grp.items = []
    #
    #
    _shim_list = [_grp for _grp in geometry.sets.values()
                  if _grp.name == '_shim_']    
    for _grp in _shim_list:
        print('    * Updating shim master elements')
        #
        _shim_members = [key for key, _elem in geometry.elements.items() 
                         if "shim" in _elem.material[0].type]
        for key in _shim_members:
            find_duplicated_shims(key, geometry, _zero_length)            
        #
        if _shim_gap:
            # set gap to the correct unit
            _gap = float(_shim_gap) * _factors[0]
            _unit = _factors[0]
            if _gap >= _unit:
                _unit = 2 * _gap
            #
            for _no in _grp.items:
                _mat = geometry.materials[_no]
                _mat.type = 'spring'
                _k = _mat.stiffness[0]
                _mat_no = int(_mat.number) * 1000
                _temp = {}
                for i in range(len(_k)):
                    _mat.spring.append(0)
                    if _k[i][i] != 0:
                        try:
                            _temp[_k[i][i]]
                        except KeyError:
                            _mat_no += 1
                            _forc = [abs(_k[i][i]) * - _factors[4], 0, 0, 0,
                                     abs(_k[i][i]) * _factors[4]]
                            _disp = [-1 * _unit, -1.0 * _gap, 0, _gap, _unit]
                            _temp[_k[i][i]] = f2u.Geometry.SpringElement('spring_' + str(_mat_no), _mat_no)
                            _temp[_k[i][i]].displacement = _disp
                            _temp[_k[i][i]].force = _forc
                        #
                        _mat.spring[i] = _temp[_k[i][i]]
        # normal shim 
        else:
            for key in _shim_members:
                _elem = geometry.elements[key]
                # find overlapping members
                _stubs = _elem.node[1]
                _brace_stub, _dummy_stub = get_branches(_stubs, geometry)
                #
                #
                _chord = _elem.node[0]
                _brace_chord, _dummy_chord = get_branches(_chord, geometry)
                #
                #
                #if 9641 in _stubs.elements or 9641 in _chord.elements :
                #    print('--->')                
                #
                # guessing this can be overlapping beam
                if _brace_stub and _brace_chord:
                    if not _dummy_stub and not _dummy_chord:
                        # TODO : possible issue to be look after
                        get_vertical_element(_elem, _stubs, _chord, geometry)
                    else:
                        if _dummy_stub:
                            _master = _chord
                            _slave = _stubs
                            _stubs_elements = _brace_stub
                            _elem_chord = geometry.elements[_chord.elements[0]]
                        else:
                            _master = _stubs
                            _slave = _chord
                            _stubs_elements = _brace_chord
                            _elem_chord = geometry.elements[_stubs.elements[0]]
                        #
                        _overlap = []
                        for _item in _stubs_elements:
                            _elem_stubs = geometry.elements[int(_item)]
                            _angle = vector_angle(_elem_stubs, _elem_chord)
                            if cmath.isclose(_angle, 0,  abs_tol=0.001) or cmath.isclose(_angle, cmath.pi, rel_tol=0.001):
                                _overlap.append(int(_item))
                        # check if ovelapping member
                        if _overlap:
                            _slave.elements = _overlap
                        # update master/sleve end
                        _elem.node[1] = _slave   # slave
                        _elem.node[0] = _master   # master
                # TODO : this need to be fixed in next release
                else:
                    get_vertical_element(_elem, _stubs, _chord, geometry)
        #
        # empty group
        _grp.items = []
    #
    if _chord_on:
        check_out.append("'\n")
        check_out.append('{:} ** Finding Elements attached to Shim Elements\n'
                         .format(37 * " "))
        check_out.extend(_chord_on)

    if _zero_length:
        check_out.append("'\n")
        check_out.append('{:} ** Duplicated Shim Elements eliminated\n'
                         .format(37 * " "))
        _zero_length = list(set(_zero_length))
        check_out.extend(print_column_num(_zero_length))
        #
        for _elim in _zero_length:
            del geometry.elements[_elim]
    #
    return  check_out
#
#
def find_dof(word_in):
    """
    """
    _key = {"x": r"\b(x|1)\b",
            "y": r"\b(y|2)\b",
            "z": r"\b(z|3)\b"}

    _match = process.common.find_keyword(word_in, _key)

    return _match
#
def get_dof(items, factor=1.0):
    """
    """
    _dof_name = {'x': 0, 'y': 1, 'z': 2}
    _dof = [0, 0, 0]

    if not items:
        _dof = [1, 1, 1]
        return _dof

    for _item in items:
        _dof_no = find_dof(_item)
        _no = _dof_name[_dof_no]
        _dof[_no] = factor

    return _dof
#
#
def find_hidropressure(word_in):
    """
    """
    _key = {"surface": r"\b((hi)?surf(ace)?((\_|\-)?elev(ation)?)?)\b",
            "mudline": r"\b((seabed|mudline)((\_|\-)?elev(ation)?)?)\b",
            "fluid": r"\b(((sea)?water|fluid)((\_|\-)?density)?)\b",
            "gravity": r"\b(grav(ity)?)\b",
            "air": r"\b((air)((\_|\-)?density)?)\b"}

    _match = process.common.find_keyword(word_in, _key)

    return _match
#
def print_column_num(items, max_slots=6):
    """
    """
    check_out = []
    _member_number = len(items)
    _slot_number = 0
    #i = 0
    for i, _item in enumerate(items):
        _slot_number += 1
        i += 1
        check_out.append(" {:11.0f}".format(_item))

        if _slot_number == max_slots or (i+1) == _member_number:
            check_out.append("\n")
            _slot_number = 0
    #
    return check_out
#
#
#
def get_seismic_items(kwargs):
    """
    """
    _keys = {"time_history": r"\b(time(\_)?(history|serie)(\_)?(name|id|number)?)\b",
             "dof": r"\b(d(egree)?(\_)?o(f)?(\_)?f(reedom)?(\_)?(code)?)\b",
             "factor": r"\b(factor|value)\b",
             "node" : r"\b(node(\_)?(name|id|number)?)\b",
             "pile" : r"\b(pile(\_)?(name|id|number)?)\b",
             "element": r"\b((element|member|concept)(\_)?(name|id|number)?)\b",
             "depth_below_mudline" : r"\b(depth(\_)?below(\_)?mudline)\b",
             "z_top" : r"\b(z(\_)?top)\b",
             "z_bottom" : r"\b(z(\_)?bottom)\b",
             "end" : r"\b((element|member|concept)?(\_)?end|position)\b"}
    #
    _dof = None
    _node = None
    _pile = None
    _factor = 1
    _seismic = {}
    #
    #
    for key, value in kwargs.items():
        _data = process.common.find_keyword(key, _keys)
        #
        if "time_history" in _data:
            if isinstance(value, str):
                _seismic['time_history_name'] = value
            else:
                _seismic['time_history_number'] = int(value)
        elif "node" in _data:
            if isinstance(value, str):
                _seismic['node_name'] = value
            elif isinstance(value, (list, tuple)):
                _seismic['node_name'] = []
                _seismic['node_number'] = []
                for _item in value:
                    if isinstance(_item, str):
                        _seismic['node_name'].append(_item)
                    else:
                        _seismic['node_number'].append(int(_item))
            else:
                _seismic['node_number'] = int(value)
        elif "element" in _data:
            if isinstance(value, str):
                _seismic['element_name'] = value
            elif isinstance(value, (list, tuple)):
                _seismic['element_name'] = []
                _seismic['element_number'] = []
                for _item in value:
                    if isinstance(_item, str):
                        _seismic['element_name'].append(_item)
                    else:
                        _seismic['element_number'].append(int(_item))           
            else:
                _seismic['element_number'] = int(value)
        elif "pile" in _data:
            if isinstance(value, str):
                _seismic['pile_name'] = value
            elif isinstance(value, (list, tuple)):
                _seismic['pile_name'] = []
                _seismic['pile_number'] = []
                for _item in value:
                    if isinstance(_item, str):
                        _seismic['pile_name'].append(_item)
                    else:
                        _seismic['pile_number'].append(int(_item))           
            else:
                _seismic['pile_number'] = int(value)
        elif "end" in _data:
            #_dof = find_dof(value)
            _seismic['end'] = int(value)        
        elif "dof" in _data:
            _dof = find_dof(value)
            _seismic['dof'] = _dof
        
        elif "factor" in _data:
            _factor = float(value)
            _seismic['factor'] = _factor
        
        elif "depth_below_mudline" in _data:
            _seismic['depth_below_mudline'] = float(value)
        
        elif "z_top" in _data:
            _seismic['z_top'] = float(value)
        
        elif "z_bottom" in _data:
            _seismic['z_bottom'] = float(value)
    #
    if not _dof:
        print('    *** error DOF not provided')
        sys.exit()
    #_dof = get_dof(_dof, factor=_factor)
    #_seismic['dof'] = get_dof(_dof, factor=_factor)
    #
    #if _node:
    #    print('node')
    #else:
    #    if _pile:
    #        print('pile')
    #    else:
    #        _seismic = {'time_history': th_name}
    #
    return _seismic
#
def get_elevations_seismic(time_serie_soil):
    """
    """
    _time_series = {}
    for _case, _series in time_serie_soil.items():
        # print('--->')
        _soil_depth = {}
        for key, item in _series.items():
            #if '_pile_' in key:
            #    continue
            
            _depth = item[5]
            try:
                _soil_depth[_depth][key] = item
            except KeyError:
                _soil_depth[_depth] = {}
                _soil_depth[_depth][key] = item
        #
        _z_top = 0
        for key, item in sorted(_soil_depth.items()):
            for _depth in item.values():
                if not _depth[4]:
                    _depth[4] = _z_top
            _z_top = key
        #
        _time_series[_case] = _soil_depth
    #
    return _time_series
#
#
def process_load(geometry, load):
    """ """
    sec_number, elem_number, mat_number = get_geom_number(geometry)
    equipment = load.equipment
    nodes = geometry.nodes
    #geometry.joints
    elements = geometry.elements
    sections = geometry.sections
    materials = geometry.materials
    set_piramid = {}
    set_no = 1
    lib = 15 # beam element
    #
    load_elim = []
    for key, item in load.functional.items():
        for area in item.areas:
            equip = equipment[area]
            nodeset = equip.joints
            load_elim.append(key)
            # new node
            new_node = "mass_"+ key
            try:
                nodes[new_node].mass[2] += equip.mass[0]
                #nodes[new_node].load[key].point.append(new_load)
            except KeyError:
                geometry.add_node(name=new_node)
                # new mass load
                nodes[new_node].set_coordinates(*equip.mass[1:])
                #number = nodes[new_node].number
                #new_load = [0,0, equip.mass[0]*9.8066500286389, 0,0,0]                
                nodes[new_node].mass = [0,0, equip.mass[0],0,0,0]
                #nodes[new_node].load[key] = f2u.Load.Node(new_node, number)
                #nodes[new_node].load[key].point.append(new_load)
                #item.node.append(new_node)
                #print('-->')
                # new section
                sec_number += 1
                sec_type = 'TUBULAR'
                sec_name = 'pyramid_' + str(sec_number).zfill(4)
                sections[sec_number] = f2u.Geometry.Section(sec_name, sec_number, 
                                                            'beam', 'pipe')
                sections[sec_number].properties = f2u.SecProp.SectionProperty(sec_type)
                sections[sec_number].properties.input_data(1.00, 0.25)
                #sections[sec_number].number = sec_number
                # new material
                mat_number += 1
                mat_name = 'pyramid_'+ str(mat_number).zfill(4)
                materials[mat_number] = f2u.Material(mat_name, mat_number)
                materials[mat_number].density = 0
                materials[mat_number].Fy = 345_000
                materials[mat_number].E = 2_100_000
                #materials[mat_number].poisson = 0.30
                materials[mat_number].type = 'Elastic'
                #
                piramid = []
                for node_name in nodeset:
                    elem_number += 1
                    elem_name  = 'pyramid_' + str(elem_number).zfill(4)
                    elements[elem_name] = f2u.Geometry.BeamElement(elem_name,
                                                                   elem_number)
                    elements[elem_name].section = [sections[sec_number]]
                    elements[elem_name].type = element_type[lib]
                    elements[elem_name].material = [materials[mat_number]]
                    elements[elem_name].node = [nodes[node_name], nodes[new_node]]
                    #
                    piramid.append(elem_name)
                #
                if piramid:
                    name_set = "pyramid_" + str(set_no).zfill(4)
                    set_piramid[set_no] = f2u.Geometry.Groups(set_no, name_set)
                    set_piramid[set_no].items = piramid
                    set_no += 1
                    #geometry
                    process.geometry.add_new_groups(geometry.sets, set_piramid, name_set)
            # reset node items
            item.node = [new_node]
    #
    load_elim = set(load_elim)
    for item in load_elim :
        del load.functional[item]
    #
    #print('-->')