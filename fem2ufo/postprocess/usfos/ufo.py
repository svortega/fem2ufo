# 
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import os
import sys

# package imports
import fem2ufo.f2u_model.control as f2uModel
import fem2ufo.process.control as process
#
from fem2ufo.postprocess.usfos.model.model_ufo import model_ufo
from fem2ufo.postprocess.usfos.control.control_ufo import control_ufo
import fem2ufo.postprocess.usfos.operation.process as ufo_operation


#
#
class USFOS_conversion:
    """Class to convert fem2ufo FE concept model to USFOS format"""    
    
    def __init__(self, f2u_model=None):
        """
        """
        #
        self.units_in = None
        self.units_out = None # [None, None, 'second', None, None, None]

        self.check_out = []
        self.factors = [0, 0, 0, 0, 0, 0]

        self.shim_gap = False
        self.analysis_type = 'static'

        # set ufo control output
        self.ufo_control = control_ufo()

        # print('ok')
        self.hydropressure = False
        self.hydropressure_members = []
        self.hydropressure_sets = []
        #
        self.mean_material = False
        #
        self.control_dof = [1, 1, 1]
        #
        self.jelly_member_list = []
        self.jelly_member_group_list = []
        #
        self.reinforced_member_list = []
        self.reinforced_member_group_list = []
        #
        self.time_series = {}
        self._time_history = False
        self.time_history_soil = {'displacement':{},
                                  'acceleration':{}}
        self.time_history_pile_subsidence = {'displacement':{},
                                             'acceleration':{}}        
        self.time_history_node = {'displacement':{},
                                  'acceleration':{},
                                  'velocity':{}}
        #
        #
        if f2u_model:
            USFOS_conversion.model(self, f2u_model)
        #
    #
    def model(self, f2u_model):
        """
        **Parameters**:
           :model: fem2ufo's FE concept model
        """
        self.model = f2u_model.get_model(meshing=True)
        self.process_checkout = f2u_model.check_out 
        # get input units from f2u model
        self.units_in = self.model.data['units']
        # 
        self.ufo_control.model(self.model)
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """
        try:
            path = os.path.normcase(path)
            os.chdir(path)
        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()

        retval = os.getcwd()
        print('')
        print('--- Current working directory : {:}'.format(retval))
    #
    def units(self, **kwargs):
        """
        Provide units for conversion purposes (i.e. usfos files will be printed with 
        this units, except for analysis that include usfos generated metocean loading,
        units are set in SI)
        Available units: length, mass, time, temperature, force, pressure/stress
        
        **Parameters**:
           :length: [mandatory]  
           :force: [mandatory]  
           :temperature :  
           :gravity: [default : 9.81ms^2] 
        """
        for key, value in kwargs.items():
            _unit = process.units.find_unit_case(key)
            self.units_out = process.units.units_module(_unit, value,
                                                        self.units_out)

    #
    def analysis(self, case):
        """
        Set usfos analysis type. The analysis type will define the
        control file set up and additional usfos output files.
        
        **Parameters**:
          :type_analysis: select analysis type  
                         - static (dafault)
                         - Dynamic
                         - Seismic
                         - Ship impact (static or dynamic)
        """
        _case = process.analysis.find_analysis_case(case)
        self.analysis_type = _case
    #
    def set_join_rule(self, rule, eccentricity=True):
        """
        Set joint rule. With this record, the capacity of each brace/chord
        connection at the tubular joint will be checked according to a selected
        joint capacity equation. usfos chjoint commenat is used.
        
        Note: fem2ufo will remove any inherited member's excentricities at the joint
        to avoid double excentricities (i.e. usfos introduces members offsets 
        automatically once the command chjoint is introduced)
        
        **Parameters**:
          :rule:  
                - msl (default)
                - norsok
                - nor_r3
                - iso
                - api_wsd\n
        """
        _rule = process.joint.find_join_rule(rule)

        if self.model.component.joints:
            _joints = self.model.component.joints
            self.ufo_control.joints(_joints, _rule, eccentricity)
            # remove eccentricity from members end at join for usfos purposes
            if not eccentricity:
                _msg = ufo_operation.joint_ufo(self.model.component)
                self.process_checkout.extend(_msg)
        else:
            print('    ** Warning -- > No joints found in the model')

    #
    def assign_shim_gap(self, gap):
        """
        Define a gap to SESAM's shim elements
        
        fem2ufo converts SESAM's shim elements to usfos' blindp2 command by default. 
        If a gap is required (i.e. for conductor analysis), fem2ufo will convert 
        the SESAM's shim elements to a zero lenght non-linear spring with a defined
        gap instead.
        
        **Parameters**:
          :gap: gap dimesion
        """
        self.shim_gap = float(gap)
    #
    #
    def _get_fe_elements_from_beam_concept(elements, concepts):
        """
        """
        _name, _number = process.geometry.select_items(elements)
    
        member_list = process.concept.find_fe_number_by_concept_name(concepts,
                                                                     _name, _number)
        return member_list
    #
    def element_assign_linear(self, *elements):
        """
        Specify member linear (i.e. development of plastic hinges suppressed) 
        with usfos command: lin_elem 
        
        **Parameters**:
          :elements: list with element's name or number 
        """
        group_list = []
        member_list = USFOS_conversion._get_fe_elements_from_beam_concept(elements, 
                                                                          self.model.component.concepts)
        self.ufo_control.linear(member_list, group_list)

    #
    def element_group_assign_linear(self, *groups):
        """
        Specify member linear (i.e. development of plastic hinges suppressed) 
        with usfos command: lin_elem 
        
        **Parameters**:
          :groups: list with element's group name or number 
        """
        member_list = []
        _name, _number = process.geometry.select_items(groups)

        group_list = process.concept.find_item_fe_number_by_name(self.model.component.sets,
                                                                 _name, _number)

        self.ufo_control.linear(member_list, group_list)

    #
    #
    def element_assign_nonstructural(self, *elements):
        """
        Specify non-structural beam elements (i.e. elements are excluded from global
        stiffness matrix but element loads are kept) with usfos command: nonstru 
        
        **Parameters**:
          :elements: list with element's name or number 
        """
        group_list = []
        member_list = USFOS_conversion._get_fe_elements_from_beam_concept(elements,
                                                                          self.model.component.concepts)
        self.ufo_control.nonstructural(member_list, group_list)
    #
    def element_group_assign_nonstructural(self, *groups):
        """
        Specify non-structural beam elements (i.e. elements are excluded from global
        stiffness matrix but element loads are kept) with usfos command: nonstru 
        
        **Parameters**:
          :groups: list with element's group name or number 
        """
        member_list = []
        _name, _number = process.geometry.select_items(groups)

        group_list = process.concept.find_item_fe_number_by_name(self.model.component.sets,
                                                                 _name, _number)

        self.ufo_control.nonstructural(member_list, group_list)

    #
    #
    def element_assign_fracture(self, *elements, strain=False):
        """
        Force elements to be fractured if the estimated tensile strain
        exceeds the specified limit with usfos command : userfrac
        
        **Parameters**:
          :elements: list with element's name or number
          :strain: tensile strain limit in percentage (10 default)
        """
        
        if not strain:
            _strain = 10.0 / 100.0
        else:
            _strain = float(strain) / 100.0
        
        group_list = []
        member_list = USFOS_conversion._get_fe_elements_from_beam_concept(elements,
                                                                          self.model.component.concepts)        
        self.ufo_control.fracture(_strain, member_list, group_list)

    #
    def element_group_assign_fracture(self, *groups, strain=False):
        """
        Force elements to be fractured if the estimated tensile strain
        exceeds the specified limit with usfos command : userfrac
        
        **Parameters**:
          :groups: list with element's group name or number 
          :strain: tensile strain limit in percentage (10 default)
        """
        member_list = []
        _name, _number = process.geometry.select_items(groups)

        group_list = process.concept.find_item_fe_number_by_name(self.model.component.sets,
                                                                 _name, _number)

        if not strain:
            _strain = 10.0 / 100.0

        else:
            _strain = float(strain) / 100.0

        self.ufo_control.fracture(_strain, member_list, group_list)
    #
    #
    def element_assign_jelly(self, *elements, factor=False):
        """
        Specify linear beam elements but with a reduced Elastic modulus to allow 
        elements's large deformations without carring any load.
        fem2ufo creates a new linear material with a reduced Elastic modulus and
        assigns it to the specified elements.
        
        **Parameters**:
          :elements: list with element's name or number
          :factor: Elastic module's factor (E = E/factor). factor=10 by default
        """
        if not factor:
            factor = 10
        
        group_list = []
        member_list = USFOS_conversion._get_fe_elements_from_beam_concept(elements,
                                                                          self.model.component.concepts) 
        self.jelly_member_list.append([member_list, factor])

        # self.jelly_members.extend(member_list)

    #
    def element_group_assign_jelly(self, *groups, factor=False):
        """
        Specify linear beam elements but with a reduced Elastic modulus to allow 
        elements's large deformations without carring any load.
        fem2ufo creates a new linear material with a reduced Elastic modulus and
        assigns it to the specified elements.
        
        **Parameters**:
          :groups: list with element's group name or number 
          :factor: Elastic module's factor (E = E/factor). factor=10 by default
        """

        member_list = []
        _name, _number = process.geometry.select_items(groups)

        group_list = process.concept.find_item_fe_number_by_name(self.model.component.sets,
                                                                 _name, _number)
        if not factor:
            factor = 10

        self.jelly_member_group_list.append([group_list, factor])
    #
    #
    def element_assign_reinforce(self, *elements, factor=False):
        """
        Reinforce beam elements by incresing E and Fy magnitud
        fem2ufo creates a new material with modified Elastic modulus
        and Fy and assigns it to the specified elements.
        
        **Parameters**:
          :elements: list with element's name or number
          :factor: Elastic module's factor (E = E/factor). factor=10 by default
        """
        if not factor:
            factor = 10

        group_list = []
        member_list = USFOS_conversion._get_fe_elements_from_beam_concept(elements,
                                                                          self.model.component.concepts)
        self.reinforce_member_list.append([member_list, factor])
    #    
    def element_group_assign_reinforce(self, *groups, factor=False):
        """
        Reinforce beam elements by incresing E and Fy magnitud
        fem2ufo creates a new material with modified Elastic modulus
        and Fy and assigns it to the specified elements.
        
        **Parameters**:
          :groups: list with element's group name or number 
          :factor: Elastic module's factor (E = E/factor). factor=10 by default
        """

        member_list = []
        _name, _number = process.geometry.select_items(groups)

        group_list = process.concept.find_item_fe_number_by_name(self.model.component.sets,
                                                                 _name, _number)
        if not factor:
            factor = 10

        self.reinforced_member_group_list.append([group_list, factor])
    #    
    #
    def set_mean_material(self, increment=10.0):
        """
        Set mean material by incrementing yield strenght (Fy) for all
        FE model materials
        
        **Parameters**:
          :increment: Yield strenght factor in percentage (10 by default)  
                      Fy = Fy * (1.0 + increment/100)
        """
        _inc = float(increment) / 100.0 + 1.0
        self.mean_material = _inc
    #
    def set_control_displacement(self, *dof, factor=1.0):
        """
        Set node control displacement.
        This is used to specified the control displacement of the structure.
        
        **Parameters**:
          :dof: global degree of freedom to be used in the control displacement  
                - x
                - y
                - z  
          :factor: Weight factor of the specified dof (1 default)
        """

        self.control_dof = ufo_operation.get_dof(dof, factor)
    #
    def _get_node_from_element(elements, end, components):
        """
        """
        _node_number = []
        _name, _number = process.geometry.select_items(elements)
        
        _memb_name = []
        for _no in _name:
            _memb_name.append([_no, end])

        _memb_number = []
        for _no in _number:
            _memb_number.append([_no, end])
        #
        _cnode = process.concept.find_node_in_element_end(components,
                                                      _memb_name,
                                                      _memb_number,
                                                      _node_number)
        
        return _cnode
    #
    def element_control(self, *elements, end=False):
        """
        Define node control by element's end node
        
        **Parameters**:
          :elements: list with element's name or number
          :end: element's end node  
                - 1
                - 2
        """
        if not end:
            print('    *** Warning -- > Element end not provided, ignored')
        else:
            _cnode = USFOS_conversion._get_node_from_element(elements, end, 
                                                             self.model.component)
            self.ufo_control.node_control(_cnode, self.control_dof)
    #
    def node_control(self, *nodes):
        """
        Define node control
        
        **Parameters**:
          :node: list with node's name or number
        """
        _memb_name = []
        _memb_number = []

        _name, _number = process.geometry.select_items(nodes)
        _cnode = process.concept.find_item_fe_number_by_name(self.model.component.nodes,
                                                             _name, _number)

        self.ufo_control.node_control(_cnode, self.control_dof)

    #
    # Static hydrodynamic pressure section for static analysis
    #
    def set_hydropressure(self, **kwargs):
        """
        Set the sea surface level and sea bottom position relative
        to the structure. usfos surflev command used.
        
        Note: this feature is only valid when usfos wave routines
        are not used.
        
        **Parameters**:
          :surface : Surface elevation
          :mudline : mudline elevatio
          :fluid   : fluid density
          :air     : air density
        """
        _hydro = {'surface': 0, 'mudline': 1, 'fluid': 2, 'gravity': 3, 'air': 4}
        self.hydropressure = [0, 0, 0, 0, 0]

        for key, value in kwargs.items():
            _item = ufo_operation.find_hidropressure(key)
            _no = _hydro[_item]
            self.hydropressure[_no] = float(value)

    #
    def element_assign_hydropressure(self, *elements):
        """
        Define tubular beam elements with external hydrostatic pressure
        
        **Parameters**:
          :elements: list with element's name or number
        """
        member_list = USFOS_conversion._get_fe_elements_from_beam_concept(elements,
                                                                          self.model.component.concepts)
        self.hydropressure_members.extend(member_list)
    #
    def element_group_assign_hydropressure(self, *groups):
        """
        Define tubular beam elements with external hydrostatic pressure
        
        **Parameters**:
          :groups: list with element's group name or number 
        """
        member_list = []
        _name, _number = process.geometry.select_items(groups)

        group_list = process.concept.find_item_fe_number_by_name(self.model.component.sets,
                                                                 _name, _number)

        self.hydropressure_sets.extend(group_list)
    #
    # Time history section
    #
    def time_history(self, name, number, 
                     time_serie, 
                     time_serie_name):
        """
        Set time history
        
        **Parameters**:
          :file : time histort file [time X Y Z]
          :th_type : displacement/acceleration
        """
        #
        # Units are changed to SI [m, N]
        _units_input = time_serie.units_in
        _units_output = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
        # here factors are getting for unit conversion purposes
        factors, _grav = process.units.get_factors_and_gravity(_units_input,
                                                               _units_output)
        # transfering information
        _database = time_serie.get_serie(time_serie_name)
        self.time_series[name] = f2uModel.Load.TimeSeries(name=name, 
                                                          number=number,
                                                          factors=factors,
                                                          database=_database)
    #
    # Seismic Section
    #
    def _soil_motion(self, kwargs):
        """
        """
        # lenght
        if not self.units_out:
            self.units_out = self.units_in
        # set working units
        _units_input = self.units_in
        _units_output = self.units_out
        #
        # here factors are getting for unit conversion purposes
        factors, _grav = process.units.get_factors_and_gravity(_units_input,
                                                               _units_output)
        #
        _depth = [None, None]
        _pile = []
        _dof = None
        _factor = 1.0        
        
        _data = ufo_operation.get_seismic_items(kwargs)
        
        for key, item in _data.items():
            if 'node' in key:
                print('    *** warning node item will be ignored')
            
            elif 'pile' in key:
                _pile.append(item)

            elif 'factor' in key:
                _factor = item
            
            elif 'dof' in key:
                _dof = item 
            
            elif 'time_history' in key:
                if 'name' in key:
                    _th_name = item
                    _time_serie = self.time_series[item][1]
                else:
                    _time_serie = item
            
            elif 'depth_below_mudline' in key:
                _depth[1] = float(item) * factors[0]
            
            elif 'z_top' in key:
                _depth[0] = float(item) * factors[0]
            
            elif 'z_bottom' in key:
                _depth[1] = float(item) * factors[0]
        #
        if not _dof:
            print('    *** error-- > DOF not provided, card ignored')
        else:
            if _pile:
                member_list = USFOS_conversion._get_fe_elements_from_beam_concept(_pile,
                                                                                  self.model.foundation.piles.concepts)
                _th_name = _th_name + '_pile_' + _dof
                return _th_name, [_time_serie, _dof, _factor, member_list]
            else:
                _th_name = _th_name + '_soil_' + _dof
                return _th_name, [_time_serie, _dof, _factor, None, _depth[0], _depth[1]]
    #
    def assign_soil_displacement(self, **kwargs):
        """
        Assign specific time history displacement to soil by depth or pile
        
        **Parameters**:
          :depth:
        """
        _th_name, _data = USFOS_conversion._soil_motion(self, kwargs)
        # check if pile
        if _data[3]:
            self.time_history_pile_subsidence['displacement'][_th_name] = _data
        else:
            self.time_history_soil['displacement'][_th_name] = _data
        self._time_history = True
    #
    def assign_soil_acceleration(self, **kwargs):
        """
        Assign specific time history acceleration to soil by depth or pile
        
        **Parameters**:
          :depth:
        """
        _th_name, _data = USFOS_conversion._soil_motion(self, kwargs)
        # check if pile
        if _data[3]:
            self.time_history_pile_subsidence['acceleration'][_th_name] = _data
        else:
            self.time_history_soil['acceleration'][_th_name] = _data
        self._time_history = True
    #
    def _node_motion(self, kwargs):
        """
        """
        _data = ufo_operation.get_seismic_items(kwargs)
        _nodes = []
        _elements = []
        _end = None
        _dof = None
        _factor = 1.0
        for key, item in _data.items():
            if 'node_number' in key:
                _nodes.append(item)
            
            elif 'element' in key:
                _elements.append(item)
                
            elif 'end' in key:
                _end = item
            
            elif 'factor' in key:
                _factor = item
            
            elif 'dof' in key:
                _dof = item            
            
            elif 'time_history' in key:
                if 'name' in key:
                    _th_name = item
                    _time_serie = self.time_series[item][1]
                else:
                    _time_serie = item
            
            elif 'depth_below_mudline' in key:
                _depth[1] = float(item) * factors[0]
            
            else:
                print('    *** warning -- > card {:} not recognized, ignored'.format(key))
        #
        _cnode = []
        if _nodes:
            _name, _number = process.geometry.select_items(_nodes)
            _cnode = process.concept.find_item_fe_number_by_name(self.model.component.nodes,
                                                                 _name, _number)
        #
        if _elements:
            if not _end:
                print('    *** warning -- > Element end not provided, ignored')
            else:
                _cnode_member = USFOS_conversion._get_node_from_element(_elements, _end, 
                                                                        self.model.component)
                _cnode.extend(_cnode_member)
        #
        if not _dof:
            print('    *** error-- > DOF not provided, card ignored')
        else:
            _th_name = _th_name + '_node_' + _dof
            return _th_name, [_time_serie, _dof, _factor, _cnode]
    #
    def assign_node_displacement(self, **kwargs):
        """
        Assign specific time history to a node
        
        **Parameters**:
          :node: list with node's name or number
        """
        _th_name, _data = USFOS_conversion._node_motion(self, kwargs)
        #try:
        #    self.time_history_node['displacement'][_th_name].append(_data)
        #except KeyError:
        self.time_history_node['displacement'][_th_name] = _data
        self._time_history = True
    #
    def assign_node_velocity(self, **kwargs):
        """
        Assign specific time history to a node
        
        **Parameters**:
          :node: list with node's name or number
        """
        _th_name, _data = USFOS_conversion._node_motion(self, kwargs)
        self.time_history_node['velocity'][_th_name] = _data
        self._time_history = True
    #
    def assign_node_acceleration(self, **kwargs):
        """
        Assign specific time history to a node
        
        **Parameters**:
          :node: list with node's name or number
        """
        _th_name, _data = USFOS_conversion._node_motion(self, kwargs)
        self.time_history_node['acceleration'][_th_name] = _data
        self._time_history = True    
    #
    # Pile subsidence Section
    #
    def assign_pile_subsidence(self, **kwargs):
        """
        Define pile subsidence via a displacement time history
        """
        _th_name, _data = USFOS_conversion._soil_motion(self, kwargs)
        if _data[3]:
            # FIXME : pile should be usfos number
            self.time_history_pile_subsidence['displacement'][_th_name] = _data
            self._time_history = True
    #
    # Printing Section
    #
    def print_model(self, file_name=None):
        """
        Write out FE model in ufo format
        
        **Parameters**:
           :file_name: output file name
        """
        print('')
        print('--------------- Post-Processing Model --------------')
        #
        # set output file name
        if not file_name:
            file_name = self.model.component.name
        # --------------------------------------------------------------------
        # Sorting units before continuing with the operation
        # --------------------------------------------------------------------
        # set output units
        if not self.units_out:
            self.units_out = self.units_in
        # set working units
        _units_input = self.units_in
        _units_output = self.units_out
        # if metocean then units are changed to SI [m, N]
        if self.model.metocean or self.analysis_type != 'static':
            _units_output = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
        #
        # here factors are getting for unit conversion purposes
        factors, _grav = process.units.get_factors_and_gravity(_units_input,
                                                               _units_output)
        self.factors = factors
        self.gravity = _grav
        # set mean material
        if self.mean_material:
            _mat_factor = float(self.mean_material)
            process.material.set_mean_material(self.model.component,
                                               _mat_factor)
        # --------------------------------------------------------------------
        # check if yelly elements
        # -------------------------------------------------------------------- 
        process.material.add_modified_material('jelly', 
                                               self.model.component,
                                               self.model.foundation,
                                               self.jelly_member_list,
                                               self.jelly_member_group_list)
        
        # --------------------------------------------------------------------
        # check if reinforced elements
        # -------------------------------------------------------------------- 
        process.material.add_modified_material('reinforced',
                                               self.model.component,
                                               self.model.foundation,
                                               self.reinforced_member_list,
                                               self.reinforced_member_group_list)
        
        #
        # --------------------------------------------------------------------
        # check if spring material
        # --------------------------------------------------------------------
        _shimp_gap = self.shim_gap
        _ufo_check_out = ufo_operation.process_spring_ufo(self.model.component,
                                                          factors, _shimp_gap)

        _model_check_out = process.feedback.print_model_info(self.model, factors)
        #
        # --------------------------------------------------------------------
        # check concept loading
        # --------------------------------------------------------------------
        ufo_operation.process_load(self.model.component,
                                   self.model.load)
        #
        # --------------------------------------------------------------------
        # printing check out model file
        # --------------------------------------------------------------------
        # print model        
        # printing check out file
        file_checkout = process.common.split_file_name(file_name)
        file_checkout = str(file_checkout[0]) + '_check_me.txt'
        add_out = open(file_checkout, 'w+')
        add_out.write("".join(_model_check_out))
        header = 'MODEL PROCESSING SECTION'
        add_out.write("".join(process.common.headers.print_head_line(header, subfix="'")))
        add_out.write("".join(self.process_checkout))
        header = 'USFOS MODEL PRINTING SECTION'
        add_out.write("".join(process.common.headers.print_head_line(header, subfix="'")))
        add_out.write("".join(_ufo_check_out))
        add_out.close()
        # --------------------------------------------------------------------
        # Processing ufo model geometry/foundations
        # --------------------------------------------------------------------
        # set ufo model
        self.ufo = model_ufo()

        self.ufo.model(self.model.component,
                       self.model.load.functional,
                       factors)
        #
        if self.model.metocean:
            self.ufo.metocean(self.model.metocean, factors)
        #
        # print soil's non-linear springs
        #if self.model.foundation.soil:
        if self.model.foundation:
            self.ufo.foundation(self.model.foundation, factors)
        #
        #
        print('')
        print('--------------- Printing Out Model -----------------')
        #        
        # --------------------------------------------------------------------
        # Process additional ufo files metocean/time history
        # --------------------------------------------------------------------
        #
        if self.model.analysis:
            self.ufo.combination(self.model.analysis, factors)
        #
        # time domain load
        if self.model.load.time_domain:
            _timehistory = self.model.load.time_domain
            self.ufo.time_domain(_timehistory, factors, _grav)
        #
        # --------------------------------------------------------------------
        # printing ufo & optional files 
        # --------------------------------------------------------------------
        #
        self.ufo.print_model(file_name)
        #
        # print('---------------- FINISH Postprocess ----------------')
    #
    #
    def print_control(self):
        """
        Write out USFOS control file
        """
        # --------------------------------------------------------------------
        # Start conversion
        # --------------------------------------------------------------------

        # here factors are getting for unit conversion purposes
        _grav = self.gravity
        factors = self.factors

        # --------------------------------------------------------------------
        # Output
        # --------------------------------------------------------------------
        #
        #
        # Seismic 
        if self._time_history:
            # soil motion
            _series = ufo_operation.get_elevations_seismic(self.time_history_soil)
            for _case, _soil_depth in _series.items():
                if _soil_depth:
                    self.ufo_control.seismic_soil_motion(_case, _soil_depth, self.factors)
            # pile subsidence
            for _case, _series in self.time_history_pile_subsidence.items():
                if _series:
                    self.ufo_control.pile_subsidence(_case, _series)
            # node motion
            for _case, _series in self.time_history_node.items():
                if _series:
                    self.ufo_control.seismic_node_motion(_case, _series)
        #
        self.ufo_control.analysis(self.analysis_type)

        # Combinations
        if self.model.analysis:
            for _metocean in self.model.analysis.case.values():
                if _metocean.metocean_combination:
                    self.ufo_control.metocean(_metocean.metocean_combination,
                                              self.factors)
                else:
                    print('    ** Warning -- > No metocean defined')                
        else:
            if self.hydropressure:
                _memb_list = self.hydropressure_members
                _group_list = self.hydropressure_sets
                _hisurf = self.hydropressure[0]
                _losurf = self.hydropressure[1]
                _fluid_density = self.hydropressure[2]
                self.ufo_control.hydropressure(_memb_list, _group_list,
                                               _hisurf, _losurf,
                                               _fluid_density,
                                               _grav, factors)
            else:
                print('    ** Warning -- > No hydropressure defined')

        # foundations
        if self.model.foundation:
            self.ufo_control.foundation(self.model.foundation,
                                        self.factors)
        else:
            print('    ** Warning -- > No soil defined')

        # Setting file name
        file_name = self.model.component.name
        # print control
        self.ufo_control.print_control_file(file_name)
        #       
    #
    def print_time_history(self, file_name=None, 
                           time_history_case=None):
        """
        Set time history (to be completed)
        
        **Parameters**:
          :file : file name 
          time_history_case : acceleration/displacement
        """
        #
        name = file_name
        if not file_name:
            file_checkout = process.common.split_file_name(self.model.component.name)
            name = 'TH_' + file_checkout[0]            
        #
        #
        if time_history_case:
            fe_model = model_ufo()
            fe_model.time_history(name, self.time_series,
                                  time_history_case=time_history_case)
            fe_model.print_model(name)
        else:
            time_history_case = ['acceleration', 'displacement']
            for _case in time_history_case:
                new_name = name + "_" + _case
                fe_model = model_ufo()
                fe_model.time_history(new_name, self.time_series,
                                      time_history_case=_case)
                fe_model.print_model(new_name)
        #
        self.time_series = {}
        #print('---> ')
    #
    #
