# 
# Copyright (c) 2009-2018 fem2ufo
# 


#
# Python stdlib imports
import sys

# package imports
import fem2ufo.process.control as process
#import fem2ufo.postprocess.usfos.operation.headers as common


# ----------------------------------------------------------------------
#
# FIXME :  clean this module
def clean_ufo_pile_set(_pile, option='maximum'):
    #
    import copy
    #
    print('    * Optimizing ufo foundations')
    #
    _psections = {}
    _pmaterials = {}
    #
    for _setNo, _group in sorted(_pile.sets.items()):

        if 'end_nodes__' in _group.name:
            #
            # top  coordinates
            _node = _group.nodes[0]
            _Zmax = _pile.nodes[_node].z # coordinate[2]
            #
            #_setno = int(_group.name.replace('end_nodes__', ''))
            _setno = _group.name.replace('end_nodes__', '')
            _set_pile_members = _pile.sets[_setno].items
            #
            # ------------------------
            # start with first member
            _memb = _group.items[0]
            _mat = _pile.elements[_memb].material[0].number
            _sect = _pile.elements[_memb].section[0].number
            # select residual
            _nodeTop = 0
            _nodeEnd = 1

            if _Zmax == _pile.elements[_memb].node[1]: # coordinate[2]:
                _nodeTop = 1
                _nodeEnd = 0
            #
            _item = [_memb]
            _res = _memb
            #
            # -----------------------
            #
            # Eliminate items
            #
            for _membNo in _set_pile_members:
                
                if _membNo == _memb:
                    continue
                
                # select mat & sect
                _mat2 = _pile.elements[_membNo].material[0].number
                _sect2 = _pile.elements[_membNo].section[0].number
                _Z1t = abs(_pile.elements[_membNo].node[0].z) # coordinate[2])
                _Z2t = abs(_pile.elements[_membNo].node[1].z) # coordinate[2])
                #
                # check if residual section == current section
                #
                # new section in pile
                if _sect != _sect2 or _mat != _mat2:
                    _sect = _sect2
                    _item.append(_membNo)
                    _res = _membNo
                    # find node end
                    # node 0
                    if _Z1t > _Z2t:
                        _nodeName = _pile.elements[_res].node[_nodeEnd].number
                        _pile.elements[_res].node[_nodeEnd] = _pile.elements[_membNo].node[0]
                        _nodeEnd = 0
                    # node 1
                    else:
                        _nodeName = _pile.elements[_res].node[_nodeEnd].number
                        _pile.elements[_res].node[_nodeEnd] = _pile.elements[_membNo].node[1]
                        _nodeEnd = 1
                        
                # check if residual material == current material
                #elif _mat != _mat2:
                    # new material in pile
                    #print('need to fix this : removePiles module => material')
                    # sys.exit()
                    # _mat = _mat2
                    # _pmaterials[_mat2] = copy.deepcopy(_material[_mat2])
                    #
                    #_item.append(_membNo)
                    #_res = _membNo
                    
                # continue with same section
                else:
                    # find node end
                    # node 0
                    if _Z1t > _Z2t:
                        _nodeName = _pile.elements[_res].node[_nodeEnd].number
                        _pile.elements[_res].node[_nodeEnd] = _pile.elements[_membNo].node[0]
                        _nodeEnd = 0
                    # node 1
                    else:
                        _nodeName = _pile.elements[_res].node[_nodeEnd].number
                        _pile.elements[_res].node[_nodeEnd] = _pile.elements[_membNo].node[1]
                        _nodeEnd = 1
                    #
                    # delete member & node
                    del _pile.nodes[_nodeName]
                    del _pile.elements[_membNo]
                #
                #
            #
            # feed group with list of items
            _pile.sets[_setno].items = _item
    #
    # renumber pile nodes
    #_pile.nodes = renumbering_set(_pile.nodes)

    # renumber pile members 
    #_pile.elements = renumbering_set(_pile.elements)
    #
    return _pile
    #
#
#
def renumbering_set(_item):
    #
    _number = 0
    for _key, _nod in sorted(_item.items()):
        _number += 1
        _nod.number = _number
    #
    return _item
    #
#
#
# --------------------------------------------------------------------
#
def print_foundation():
    # FEMufo = []
    # FEMufo.append("'\n")
    head = 'FOUNDATION SECTION'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    FEMufo.append("'    This record select the type of spring element used with soil modelling \n")
    FEMufo.append("'\n")
    FEMufo.append("'    Model :\n")
    FEMufo.append("'           0 = Old nonlineal spring model, step scaling \n")
    FEMufo.append("'           1 = New plasticity formulation, no step scaling, min iteration = 1 \n")
    FEMufo.append("'\n")
    FEMufo.append("'               Model\n")
    FEMufo.append("SPRI_MOD            1\n")
    FEMufo.append("'\n")
    FEMufo.append("'\n")
    FEMufo.append("'    This record is used to scale soil properties used by one or more piles \n")
    FEMufo.append("'\n")
    FEMufo.append("'    KeyWord : SoilScal\n")
    FEMufo.append("'    OptID   : Option ID\n")
    FEMufo.append("'    Type    : P-Y, T-Z, Q-Z & Assign \n")
    FEMufo.append("'    Z       : Z_coordinate relative to mudline, (Positive Z goes upwards\n")
    FEMufo.append("'    Fac     : Actual Scaling factor \n")
    FEMufo.append("'\n")
    FEMufo.append("'           KeyWord    ID    Type     Z    Fac \n")
    FEMufo.append("' PileOpt   SoilScal  100     P-Y     0    0.5 \n")
    FEMufo.append("'                                    -1    0.5 \n")
    FEMufo.append("'                                    -2    0.5 \n")
    FEMufo.append("'                                   -80    0.5 \n")
    FEMufo.append("'\n")
    FEMufo.append("'           KeyWord    ID    Type     Z    Fac \n")
    FEMufo.append("' PileOpt   SoilScal  100  Assign  1001        \n")
    FEMufo.append("'\n")

    return FEMufo


#
def print_pile(pile, soil, factors):
    # units
    _lenFactor = factors[0]
    #
    head = 'PILE MODELLING'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #    
    # --------------
    #
    FEMufo.append("'{:} Define Pile Tip Node\n".format(62 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'                  ID             x             y             z\n")
    #
    for _group in pile.sets.values():
        if 'end_nodes' in _group.name:
            FEMufo.append("NODE     {:12.0f} {: 1.6e} {: 1.6e} {: 1.6e}\n"
                          .format(pile.nodes[_group.nodes[1]].number,
                                  pile.nodes[_group.nodes[1]].x * _lenFactor,
                                  pile.nodes[_group.nodes[1]].y * _lenFactor,
                                  pile.nodes[_group.nodes[1]].z * _lenFactor))
    #
    # --------------
    #
    FEMufo.append("'\n")
    FEMufo.append("'{:} Pile Section\n".format(70 * "-"))
    FEMufo.append("'\n")
    #
    j = 0
    _mudline = []
    for _key, _set in pile.sets.items():
        j += 1
        if '_pile_' in _set.name:
            i = 0
            for _memb_no in _set.items:
                # print header
                if j == 1:
                    FEMufo.append("'{:} ID {:} Type {:} Do {:} T {:} ! pile group\n"
                                  .format(17*" ", 7*" ", 8*" ", 9*" ", 12*" "))
                    if len(_set.items) > 1:
                        FEMufo.append("'{:} ID {:} Z_mud {:} Z1 {:} Z2 {:} Do {:} T\n"
                                      .format(17*" ", 6*" ", 8*" ", 8*" ", 8*" ", 9*" "))
                    FEMufo.append("'\n")
                    j += 1

                i += 1
                _sec = pile.elements[_memb_no].section[0]
                pile.elements[_memb_no].group = _key # _set.number
                _node_0 = pile.elements[_memb_no].node[0].z # top node
                _node_1 = pile.elements[_memb_no].node[1].z # tip node
                
                # check if node exist
                try:
                    _test = 1.0 / _node_0
                    _test = 1.0 / _node_1
    
                except ZeroDivisionError:
                    print('*** Error pile top/tip node not found [{:}, {:}]'
                          .format(_node_0, _node_1))
                    print('*** program exit with an erorr')
                    sys.exit()
                
                # start with top (first) node
                if i == 1:
                    Z_mud = _node_0
                    _mudline.append(abs(Z_mud))
                    # print single section of the pile
                    FEMufo.append("PILEGEO  {:12.0f} {:12.0f} {: 1.4e} {: 1.4e} {:} ! {:}\n"
                                  .format(_set.number, 1,
                                          _sec.properties.D * _lenFactor,
                                          _sec.properties.Tw * _lenFactor,
                                          11*" ", _set.name))

                    if len(_set.items) == 1: 
                        break
                    
                    # print header for variable section of the pile
                    FEMufo.append("PILE_D-T {:12.0f}  {: 1.4e} {: 1.4e} {: 1.4e} {: 1.4e} {: 1.4e}\n"
                                  .format(_set.number, Z_mud,
                                          abs(Z_mud - _node_0) * -1, abs(Z_mud - _node_1) * -1,
                                          _sec.properties.D * _lenFactor,
                                          _sec.properties.Tw * _lenFactor))
                    continue
                # continue printing variable section of the pile
                FEMufo.append("{:} {: 1.4e} {: 1.4e} {: 1.4e} {: 1.4e}\n"
                              .format(34*" ", abs(Z_mud - _node_0) * -1, abs(Z_mud - _node_1) * -1,
                                      _sec.properties.D * _lenFactor,
                                      _sec.properties.Tw * _lenFactor))
            #
            FEMufo.append("'\n")
    #
    # --------------
    #
    FEMufo.append("'\n")
    FEMufo.append("'{:} Pile Definition\n".format(67 * "-"))
    FEMufo.append("'\n")
    #
    FEMufo.append("'\n")
    FEMufo.append("' {:} ID {:} Nodex1 {:} Nodex2 SoilID   PMat    PGeo   LoC   Imp\n"
                  .format(16*" ", 6*" ", 6*" "))
    FEMufo.append("'{:} (top) {:} (tip) {:} ! pile group\n"
                  .format(28*" ", 7*" ", 22*" "))
    _twinNode = []
    #
    # Extract soil's pile diameters and check soil elevations
    _mudline = max(_mudline)
    _soil = {}
    for _elevations in soil.values():
        _soil[round(_elevations.diameter, 2)] = _elevations.number
        try:
            _error = abs(1 - abs(_mudline) / _elevations.mudline)
            if _error > 0.10:
                _elevations.mudline = _mudline
        except ZeroDivisionError:
            _elevations.mudline = abs(_mudline)

    #
    # TODO: this section needs tidy
    for _group in pile.sets.values():
        if 'end_nodes' in _group.name:
            _node_1 = _group.nodes[1]  # tip
            _memb = _group.items[0]  # element number
            _pile = pile.elements[_memb]
            _node_0 = _pile.node[0].number # top node
            #
            _set_name = _pile.group
            _section = round(_pile.section[0].properties.D, 2)
            # one soil type
            if len(_soil) == 1:
                FEMufo.append("PILE     {:12.0f} {:13.0f} {:13.0f} {:6.0f} {:6.0f} {:7.0f}  ! {:}\n"
                              .format(_pile.number, _node_0, _node_1, 1,
                                      _pile.material[0].number,
                                      pile.sets[_set_name].number, 
                                      pile.sets[_set_name].name))
            # more than one soil type
            else:
                try:
                    _soilNo = _soil[_section]
                    FEMufo.append("PILE     {:12.0f} {:13.0f} {:13.0f} {:6.0f} {:6.0f} {:7.0f}  ! {:}\n"
                                  .format(_pile.number, _node_0, _node_1, _soilNo,
                                          _pile.material[0].number,
                                          pile.sets[_set_name].number, 
                                          pile.sets[_set_name].name))
                
                except KeyError:
                    FEMufo.append("PILE     {:12.0f} {:13.0f} {:13.0f} {:6.0f} {:6.0f} {:7.0f}  ! {:}\n"
                                  .format(_pile.number, _node_0, _node_1, 1,
                                          _pile.material[0].number,
                                          pile.sets[_set_name].number, 
                                          pile.sets[_set_name].name))

    #
    #
    if len(pile.materials) > 1:
        FEMufo.append("'\n")
        j = 0
        _mudline = []
        for _key, _set in pile.sets.items():
            j += 1
            if '_pile_' in _set.name:
                i = 0
                for _memb_no in _set.items:
                    # print header
                    if j == 1:
                        FEMufo.append("'\n")
                        j += 1
                    #
                    i += 1
                    _pile = pile.elements[_memb_no]
                    #_sec = _pile.section[0]
                    _mat = _pile.material[0]
                    #pile.elements[_memb_no].group = _set.number
                    _node_0 = _pile.node[0].z # top node
                    _node_1 = _pile.node[1].z # tip node
                    # start with top (first) node
                    if i == 1:
                        Z_mud = _node_0
                        _mudline.append(abs(Z_mud))
                        #
                        # print header for variable section of the pile
                        FEMufo.append("PILEMAT {:12.0f}  {: 1.4e} {: 1.4e} {:6.0f} ! {:}\n"
                                      .format(_pile.number,
                                              abs(Z_mud - _node_0) * -1, abs(Z_mud - _node_1) * -1,
                                              _mat.number, _mat.name))
                        continue
                    
                    # print header for variable section of the pile
                    FEMufo.append("{:}{: 1.4e} {: 1.4e} {:6.0f} ! {:}\n"
                                  .format(22*" ",
                                          abs(Z_mud - _node_0) * -1, abs(Z_mud - _node_1) * -1,
                                          _mat.number, _mat.name))                    
                #
                FEMufo.append("'\n")
    #    
    FEMufo.append("'\n")

    return FEMufo
#
#
def print_soil(foundation, factors, mudline=False):
    # units
    _lenFactor = factors[0]
    # FEMufo = []
    # FEMufo.append("'\n")
    head = 'SOIL MODELLING'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    # --------------
    #
    FEMufo.append("'{:} Soil Properties\n".format(67 * "-"))
    FEMufo.append("'\n")
    #
    # _res = 0
    # _flag = 1
    #
    for _key1, _soil in sorted(foundation.items()):
        #
        FEMufo.append("'                  ID          Type         Z_Mud         D_ref     F_fac     L_fac\n")
        FEMufo.append("SOILCHAR {:12.0f}  {:12.0f} {: 1.6e} {: 1.6e} {:9.0f} {:9.0f}\n"
                      .format(_soil.number, 1, abs(_soil.mudline) * -1 * _lenFactor,
                              _soil.diameter * _lenFactor, 1, 1))
        #
        FEMufo.append("  '\n")
        FEMufo.append("  '{:26s} Z_top        Z_bott           P-Y       T-Z       Q-Z\n".format(' '))
        _layersNo = len(_soil.layers)
        #
        _top = 0
        _minSthck = 1
        _Zcoord = 0
        for _key2, _layer in sorted(_soil.layers.items()):
            #
            try:
                _test = 1.0 / _layer.thickness
                _ratioTD = _layer.thickness / _soil.diameter

                if _ratioTD < _minSthck:
                    _minSthck = _ratioTD
                    # print('==> ', _minSthck)
                    if _minSthck <= 0.1:
                        # print('==> x ', _minSthck, _layersNo + _No)
                        FEMufo.append("'{:21s} Soil layer t/D < 0.10 --> Z_bott [{:}] is modified \n"
                                      .format(' ', _layer.depth * _lenFactor))
                        _minSthck = 0.10
                        _layer.depth += (0.10 * _soil.diameter) - _layer.thickness
            #
            except ZeroDivisionError:
                FEMufo.append("'{:21s} Soil layer t = 0 --> layer omitted\n".format(' '))
                FEMufo.append("'")
            #
            FEMufo.append("{:21s} {: 1.6e} {: 1.6e}     {:9.0f} {:9.0f} {:9.0f}\n"
                          .format(' ', _top * -1 * _lenFactor, _layer.depth * -1 * _lenFactor,
                                  _layer.curve['PY'].number, 
                                  _layer.curve['TZ'].number,
                                  _layer.curve['QZ'].number))
            #
            _top = _layer.depth

        #
        # exit with additional layer
        _Zcoord = _layer.depth + _layer.thickness  # _soil.diameter
        FEMufo.append("{:21s} {: 1.6e} {: 1.6e}     {:9.0f} {:9.0f} {:9.0f}\n"
                      .format(' ', _top * -1 * _lenFactor, _Zcoord * -1 * _lenFactor,
                              _layer.curve['PY'].number, 
                              _layer.curve['TZ'].number,
                              _layer.curve['QZ'].number))
        #
        FEMufo.append("'\n")
    #
    #       
    #
    FEMufo.append("'\n")

    return FEMufo
#