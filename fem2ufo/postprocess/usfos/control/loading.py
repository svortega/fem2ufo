# 
# Copyright (c) 2009-2018 fem2ufo
# 

#
# Python stdlib imports

# package imports
import fem2ufo.process.control as process



# --------------------------------------------------------------------
# Loading control
#
#
def print_basic_load_combination(gravity=None):
    """
    """
    #
    head = 'LOAD COMBINATION'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    FEMufo.append("'    This record defines USFOS load combinations \n")
    FEMufo.append("'    NOTE This option should be used for basic loads only \n")
    FEMufo.append("'\n")
    FEMufo.append("'    Example : Load Combination No: 3 [ Topside Load ] \n")
    FEMufo.append("'\n")
    FEMufo.append("'          Comb_Case  L_Case(i)  Factor(n) \n")
    FEMufo.append("'\n")
    if gravity:
        FEMufo.append("COMBLOAD           1         {:}       1.00      ! Gravity (Mass)\n"
                      .format(gravity))
    else:
        FEMufo.append("'COMBLOAD           1         10       1.00      ! Gravity\n")
        FEMufo.append("'\n")
        FEMufo.append("'COMBLOAD           2        600       1.00      ! Buoyancy\n")
    #
    FEMufo.append("'\n")
    FEMufo.append("'COMBLOAD           3         10       1.30      ! Topside Dead load\n")
    FEMufo.append("'                             40       1.10      ! Topside Contents load\n")
    FEMufo.append("'                             50       1.10      ! Topside Live load\n")
    FEMufo.append("'\n")
    FEMufo.append("'COMBLOAD           4         12       1.30      ! Drilling rig load\n")
    FEMufo.append("'                            100      -0.50      ! New user input Load\n")
    FEMufo.append("'                              i          n      ! (n) factor to (i) load\n")
    FEMufo.append("'\n")
    #
    return FEMufo
#
def print_control_head(_type, EndTime=20, dT=0.1,
                       text='Dynamic Simulation'):
    """
    """
    #
    head = 'LOAD CONTROL'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    if 'static' in _type :
        FEMufo.append("'    This record specifies the loading history, with load & displacement control\n")
        FEMufo.append("'    parameters \n")
        FEMufo.append("'\n")
        FEMufo.append("'         nloads    npostp   mxpstp   mxpdisp \n")
        FEMufo.append("CUSFOS       10      99999    0.001     100.0 \n")
        FEMufo.append("'\n")
        FEMufo.append("'    To get the loads to reach mxld use zero nstep \n")
        FEMufo.append("'\n")
        FEMufo.append("'    Example : Storm Condition \n")
        FEMufo.append("'\n")
        FEMufo.append("'        lcomb      lfact    mxld    nstep     minstp \n")
        FEMufo.append("'             1       0.01    1.30        0      0.001   ! SelfWeight\n")
        FEMufo.append("'             2       0.01    1.06        0      0.001   ! Buoyancy\n")
        FEMufo.append("'             3       0.01    1.30        0      0.001   ! Topside\n")
        FEMufo.append("'             4       0.01    1.30        0      0.001   ! Drilling\n")
        FEMufo.append("'             5      0.001    2.00        0      0.001   ! Wave + Current + Wind\n")
        FEMufo.append("'\n")
    # dynamic general
    else :
        FEMufo.append("'{:} Time Domain\n".format(71 * "-"))
        FEMufo.append("'\n")
        FEMufo.append("'             EndTime          dT       dTres      dTpri\n")
        #
        FEMufo.append("STATIC     {:1.4e}  {:1.4e}  {:1.4e}  {:1.4e}  ! {:}\n"
                      .format(1, 0.1, 0.1, 0.1, 'Gravity + Buoyancy'))

        FEMufo.append("DYNAMIC    {:1.4e}  {:1.4e}  {:1.4e}  {:1.4e}  ! {:}\n"
                      .format(EndTime, dT, 0.5, 0.5, text))
        #
        FEMufo.append("'\n")
    #
    FEMufo.append("'\n")
    return FEMufo
#
def print_TH_header(_gravity, buoyancy=False,
                    header='TIME HISTORY CONTROL'):
    """
    """
    FEMufo = process.common.headers.print_head_line(header, subfix="'")

    FEMufo.append("'\n")
    FEMufo.append("'{:} Time History Control\n".format(62 * "-"))
    FEMufo.append("'\n")
    #
    FEMufo.append("'  This record is used to specify the loads to be activated during a dynamic\n")
    FEMufo.append("'  analysis with all loading controlled through time\n")
    FEMufo.append("'\n")
    #
    FEMufo.append("'            ID   <type>    T1     T2     Fac   Power\n")
    if not _gravity:
        FEMufo.append("' ")
    FEMufo.append("TIMEHIST      {:}   S_Curv     0      1       1       2  ! Ramp up DeadWeight in 1 sec\n"
                  .format(_gravity))
    # 
    #if _buoyancy:
        #FEMufo.append("BUOYHIST      1      All                               ! Scale buoy according to TH 1\n")
        #FEMufo.append("'\n")
        #FEMufo.append("'\n")
        #FEMufo.append("'            ID   <type>  Dtime  Factor   Start_time\n")
        #FEMufo.append("TIMEHIST      {:}   Switch    0.0    1.0           0.0   ! Wave\n"
        #              .format(_buoyancy))
        #FEMufo.append("'\n")
    #else:
    FEMufo.append("'\n")
    FEMufo.append("'\n")
    FEMufo.append("'            ID   <type>  Dtime  Factor   Start_time\n")
    FEMufo.append("'\n")
    FEMufo.append("TIMEHIST      2   Switch    0.0     1.0          0.0   ! Wave\n")
    FEMufo.append("'\n")
    #
    if not buoyancy:
        FEMufo.append("'\n")
        FEMufo.append("'   WiD TH load example :\n")
        FEMufo.append("'            HistNo Point  Time_0 Factor_0 \n")
        FEMufo.append("'                          Time_1 Factor_1  ! start WiD load\n")
        FEMufo.append("'                          Time_2 Factor_2  ! peak  WiD load\n")
        FEMufo.append("'                          Time_3 Factor_3  ! end   WiD load\n")
        FEMufo.append("'                          Time_n Factor_n\n")
        FEMufo.append("'\n")        
        FEMufo.append("' TIMEHIST      3    Point    0 0  (1) 0  (2) 1  (3) 0  n 0   ! WiD Triangular shape\n")
        FEMufo.append("'\n")
    #
    FEMufo.append("'\n")
    FEMufo.append("'{:} Time History Parameters\n".format(59 * "-"))
    FEMufo.append("'\n")
    #
    FEMufo.append("'\n")
    FEMufo.append("'          L_Case    Hist_ID\n")
    if not _gravity:
        FEMufo.append("' ")
    FEMufo.append("LOADHIST  {:7.0f}    {:7.0f}      !  Gravity\n"
                  .format(_gravity,_gravity))
    #
    return FEMufo
#
#
#
def print_metocean_control(metocean, _type):
    """
    """
    #
    _wave_period = [_metocean.wave.period for _metocean in metocean.values()]
    _wave_period = max(_wave_period)
    #
    head = 'METOCEAN DATA'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    # Wave integration points
    FEMufo.append("'{:} Member Integration Section\n".format(56 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'    This record is used to define number of integration sections to be used in\n")
    FEMufo.append("'    connection with wave load calculations.\n")
    FEMufo.append("'\n")
    FEMufo.append("'           NIS     {Id_List}\n")
    FEMufo.append("WAVE_INT     10\n")
    FEMufo.append("'\n")
    FEMufo.append("'    NOTE ! If no elements are specified, all beam elements are using NIS\n")
    FEMufo.append("'           integration sections.\n")
    FEMufo.append("'\n")
    FEMufo.append("'{:} External Pressure Effects\n".format(57 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'    With this record USFOS find elements for external pressure\n")
    FEMufo.append("'    (Flooded elements are skipped)\n")
    FEMufo.append("'\n")
    FEMufo.append("EXTPRES     Auto\n")
    FEMufo.append("'\n")
    FEMufo.append("'{:} Wave Switches Section\n".format(61 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("' Do not store data for visualization of wave (Default on)\n")
    FEMufo.append("' SWITCHES   WaveData      NoStore\n")
    FEMufo.append("'\n")
    FEMufo.append("' Switches OFF Doppler effects (Default on)\n")
    FEMufo.append("' SWITCHES   WaveData      NoDoppler\n")
    FEMufo.append("'\n")
    FEMufo.append("' Stream Function order (Default 10)\n")
    FEMufo.append("' SWITCHES   WaveData      StreamOrd    10\n")
    FEMufo.append("'\n")
    #
    FEMufo.append("'\n")    
    FEMufo.append("'{:} Wave, Current and Wind Data\n".format(55 * "-"))
    FEMufo.append("'\n")
    # static sea state
    if 'static' in _type.lower() :
        FEMufo.append("'                  Type     LoadCase\n")
        for key in sorted(metocean, key = lambda name: metocean[name].number):
            _metocean = metocean[key]
            _wave = _metocean.wave
            try:
                _wave_name = int(_wave.name)
                _name = "'\n"
            except ValueError:
                _name = "' {:}\n".format(_wave.name)
                _wave_name = int(_wave.number)
            
            if _wave.theory != 'calm_sea':
                FEMufo.append(_name)
                FEMufo.append("' MAXWAVE     WaveLCase {:12.0f}   ! Wave Type : {:}\n".
                              format(_wave_name, _wave.theory))
                # Wave Kinematics reduction Factor:
                FEMufo.append("' WAVE_KRF    {:}\n"
                              .format(_metocean.wave_kinematics))
                # Current Blocakge
                if _metocean.current:
                    FEMufo.append("' CURRBLOCK        User   {:}\n"
                                  .format(_metocean.current_blockage))
                #
                if _metocean.wind:
                    _wind = _metocean.wind
                    try:
                        _wind_name = int(_wind.name)
                        _name = "'\n"
                    except ValueError:
                        _name = "' {:}\n".format(_wind.name)
                        _wind_name = int(_wind.number)                    
                    FEMufo.append("' MAXWAVE      AddLCase {:12.0f}   ! Add wind load case and search for max\n"
                                  .format(_wind_name))
        #
        FEMufo.append("'\n")
        FEMufo.append("'\n")
        FEMufo.append("'    With this record the user may use USFOS to identify the worst wave phase\n")
        FEMufo.append("'    to be used in a static pushover analysis\n")
        FEMufo.append("'\n")
        FEMufo.append("'                   Typ      dT      EndT    option\n")
        FEMufo.append("' MAXWAVE     BaseShear  {:6.4f}   {:6.4f}     Write\n".
                      format(0.1, _wave_period))
        FEMufo.append("' MAXWAVE      OverTurn  {:6.4f}   {:6.4f}     Write\n".
                      format(0.1, _wave_period))        
        FEMufo.append("'\n")
        FEMufo.append("'\n")
        FEMufo.append("'{:} Buoyancy\n".format(74 * "-"))
        FEMufo.append("'\n")
        FEMufo.append("'    If buoyancy is specified, buoyancy effects are accounted for.\n")
        FEMufo.append("'    Note : BUOYANCY number must be redefined in COMBLOA :\n")
        FEMufo.append("'       GRAVITY   600   0.00  0.00 0.000  ! Define dummy load (i.e. 600)\n")
        FEMufo.append("'       BUOYANCY   2   Write              ! Assign Bouyancy to new COMBLOAD (i.e. 2)\n")
        FEMufo.append("'       COMBLOAD   2     600  1.00        ! Create new COMBLOAD including dummy load\n")
        FEMufo.append("'\n")
        FEMufo.append("'\n")
        FEMufo.append("'           Lcase        Option\n")
        i = 0
        for key in sorted(metocean, key = lambda name: metocean[name].number):
        #for _wave in metocean.wave.values():
            _metocean = metocean[key]
            _wave = _metocean.wave
            if _wave.theory == 'calm_sea':
                i += 1
                if i > 1: FEMufo.append("'")
                FEMufo.append("GRAVITY  {:8.0f}   0.00000E+00  0.00000E+00 0.00000E+00\n"
                              .format(_wave.name))
                if i > 1: FEMufo.append("'")
                FEMufo.append("BUOYANCY        2         Write\n"
                              .format(_wave.name))
                FEMufo.append("'\n")
    # dynamic general
    else:
        for key in sorted(metocean, key = lambda name: metocean[name].number):
            _metocean = metocean[key]
            _wave = _metocean.wave
            
            FEMufo.append("'\n")
            FEMufo.append("'     Wave Load Case  {:}  Wave Type : {:}\n".
                          format(_wave.name, _wave.theory))            
            
            if _wave.theory != 'calm_sea':
                # Wave Kinematics reduction Factor:
                FEMufo.append("' WAVE_KRF   {:}\n"
                              .format(_metocean.wave_kinematics))

            # Current Blocakge
            if _metocean.current:
                FEMufo.append("' CURRBLOCK        User   {:}\n"
                              .format(_metocean.current_blockage))
        #
        FEMufo.append("'\n")
        FEMufo.append("'{:} Buoyancy\n".format(74 * "-"))
        FEMufo.append("'\n")
        FEMufo.append("' The buoyancy forces are added to the WAVEDATA lcase\n")
        FEMufo.append("' The buoyancy forces are updated every time wave loads are calculated,\n")
        FEMufo.append("' and the current position of the sea surface defines whether an element\n")
        FEMufo.append("' becomes buoyant or not at any time.\n")
        FEMufo.append("'\n")
        FEMufo.append("BUOYANCY                ! Account for Buoyancy\n")
        FEMufo.append("'\n")       
    #
    FEMufo.append("'\n")
    return FEMufo
#
def print_metocean_TH_parameters(metocean, buoyancy=False):
    """
    """
    FEMufo = []
    FEMufo.append("'\n")
    #
    #
    if buoyancy:
        try:
            _wave = metocean[buoyancy].wave
            FEMufo.append("LOADHIST  {:7.0f}     {:6.0f}      !  Calm Sea\n".
                          format(_wave.name, 2))
            if metocean[buoyancy].wind:
                _wind = metocean[buoyancy].wind
                _name = _wind.name
                FEMufo.append("LOADHIST  {:7.0f}     {:6.0f}      !  Wind\n".
                              format(_name, 2))
        except KeyError:
            FEMufo.append("LOADHIST  {:7.0f}     {:6.0f}      !  Calm Sea\n".
                          format(buoyancy, 2))
        FEMufo.append("'\n")
    else:
        for key in sorted(metocean, key = lambda name: metocean[name].number):
            _metocean = metocean[key]
            _wave = _metocean.wave
            
            FEMufo.append("' LOADHIST  {:7.0f}     {:6.0f}      !  Wave\n".
                          format(_wave.name, 2))
    
            if _metocean.wind:
                _wind = _metocean.wind
                _name = _wind.name
                FEMufo.append("' LOADHIST  {:7.0f}     {:6.0f}      !  Wind\n".
                              format(_name, 2))
            
            FEMufo.append("'\n")
        #    
        FEMufo.append("'\n")
        FEMufo.append("'{:} Save Dynamic Results\n".format(62 * "-"))
        FEMufo.append("' - Global Results\n")
        FEMufo.append("'\n")
        FEMufo.append("' DYNRES_GLOB      WaveLoad    ! Wave Forces\n")
        FEMufo.append("' DYNRES_GLOB      WaveOvtm    ! Wave OverTurning Moment\n")
        FEMufo.append("' DYNRES_GLOB      ReacBSH     ! Reaction, Base Shear\n")
        FEMufo.append("' DYNRES_GLOB      ReacOvtm    ! Reaction, Overturning Moment\n")
        FEMufo.append("' DYNRES_GLOB      WaveElev    ! Plot of Surface elevation\n")
        FEMufo.append("'\n")
        FEMufo.append("'                ResTyp   Elem Id    End  Dof\n")
        FEMufo.append("' DYNRES_ELEM     Force     53103      2    1\n")
        FEMufo.append("'\n")
    #
    FEMufo.append("'\n")
    FEMufo.append("'\n")
    FEMufo.append("'{:} Wave Relative Velocity\n".format(60 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("' The relative velocity between the structure and the wave particles are\n")
    FEMufo.append("' accounted for in connection with the calculation of drag forces.\n")
    FEMufo.append("' This is also the way to switch on hydro-dynamical damping.\n")
    FEMufo.append("'\n")
    FEMufo.append("' REL_VELO                ! Account for Relative Velocity\n")
    FEMufo.append("'\n")     
    FEMufo.append("'\n")
    return FEMufo
#
#
def print_seismic_header(time_serie, _gravity=False):
    """
    """
    #
    header = 'SEISMIC TIME HISTORY CONTROL'
    FEMufo = process.common.headers.print_head_line(header, subfix="'")
    FEMufo.append("'\n")
    FEMufo.append("' SWITCHES    EarthQuake  Delay  25    ! Delay the motion history with 25 seconds\n")
    FEMufo.append("'\n") 
    FEMufo.append("'\n")
    #
    if _gravity:
        FEMufo.append("'            ID  S_Curve    T1     T2  factor   power\n")
        FEMufo.append("TIMEHIST      {:}   S_Curv     0      1     1.0     2    ! Ramp up Gravity in 1 sec\n"
                      .format(_gravity))
    #      
    FEMufo.append("'\n")
    FEMufo.append("'{:} Time History Control\n".format(62 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'            HistNo Point  Time_1 Factor_1\n")
    FEMufo.append("'                          Time_2 Factor_2\n")
    FEMufo.append("'                          ...\n")
    FEMufo.append("'                          Time_n Factor_n\n")
    FEMufo.append("'\n")    
    for  _depth in time_serie:    
        FEMufo.append("'TIMEHIST     {:} point    0 0.0  25 0.0  26 1.0  100 1.0   ! Seismic Simulation\n"
                      .format(_depth))
    #
    FEMufo.append("'\n")
    FEMufo.append("'{:} Time History Parameters\n".format(59 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'          L_Case   HistID\n")
    #
    if _gravity:
        FEMufo.append("LOADHIST  {:7.0f}  {:7.0f}      !  Gravity\n"
                      .format(_gravity, _gravity))    
    #
    for  _depth, _names in time_serie.items():
        FEMufo.append("LOADHIST     {:}     {:}      ! "
                      .format(_depth, _depth))
        for _name in _names:
            FEMufo.append(" {:}".format(_name))
        FEMufo.append("\n")
    #
    FEMufo.append("'\n")
    FEMufo.append("'{:} Earthquake Parameters\n".format(61 * "-"))
    FEMufo.append("'\n")    
    return FEMufo
#
def print_seismic_soil(case, soil_depth, factors):
    """
    """
    #
    _cases = {'acceleration' : 'SOILACC',
              'displacement' : 'SOILDISP'}
    
    _dof_name = {'x': 1, 'y': 2, 'z': 3}
    
    FEMufo = []
    FEMufo.append("'\n")
    FEMufo.append("'  This record defines prescribed soil {:}\n".format(case))
    FEMufo.append("'\n")
    FEMufo.append("'          L_Case    Type       z_Top       z_Bot  Dof    Value\n")
    #
    _lenFactor = factors[0]
    for key, item in sorted(soil_depth.items()):
        for  _name, _depth in item.items():
            _dof = _dof_name[_depth[1]]
            _prescribed = _cases[case]
            _z_top = _depth[4] * _lenFactor
            _z_bottom = _depth[5] * _lenFactor
            FEMufo.append("{:}  {:7.0f}       1  -{:1.3e}  -{:1.3e}    {:}   {:1.4f}  ! {:}\n"
                          .format(_prescribed, _depth[0], _z_top, _z_bottom, _dof, _depth[2], _name))
        FEMufo.append("'\n")
    #
    FEMufo.append("'\n")
    return FEMufo
#
def print_seismic_node(case, time_serie):
    """
    """
    _cases = {'acceleration' : 'NODEACC',
              'velocity' : 'NODEVELO',
              'displacement' : 'NODEDISP'}
    
    _dof_name = {'x': 1, 'y': 2, 'z': 3}
    
    FEMufo= []
    FEMufo.append("'\n")
    FEMufo.append("'  This record defines prescribed node {:}\n".format(case))
    FEMufo.append("'\n")
    FEMufo.append("'          L_Case    Node     Dof    Value\n")
    
    for key, item in time_serie.items():
        _prescribed = _cases[case]
        _dof = _dof_name[item[1]]
        for _node in item[3]:
            FEMufo.append("{:}  {:7.0f} {:7.0f}       {:}   {:1.4f}   ! {:}\n"
                          .format(_prescribed, item[0], _node, _dof, item[2], key))
    
    return FEMufo
#
def print_pile_subsidence(case, time_serie):
    """
    """
    
    _cases = {'acceleration' : 'SOILACC',
              'displacement' : 'SOILDISP'}
    
    _dof_name = {'x': 1, 'y': 2, 'z': 3}
    
    FEMufo = []
    FEMufo.append("'\n")
    FEMufo.append("'  This record defines prescribed pile {:}\n".format(case))
    FEMufo.append("'\n")
    FEMufo.append("'          L_Case    Type    Pile      Dof    Value\n")
    
    for key, item in time_serie.items():
        _prescribed = _cases[case]
        _dof = _dof_name[item[1]]
        _pile = item[3][0]
        FEMufo.append("{:}  {:7.0f}       2 {:7.0f} {:8.0f}   {:1.4f}  ! {:}\n"
                      .format(_prescribed, item[0], _pile,  _dof, item[2], key))    
    
    return FEMufo
#
#
# ----------------------------------------------------------------------
#
def print_gravity(load_number, gravity=9.80665):
    """
    """
    UFOmod = []
    UFOmod.append("'\n")
    UFOmod.append("'{:} Gravity\n".format(75 * "-"))
    UFOmod.append("'\n")
    UFOmod.append("'         LoadCase      Acc_X    Acc_Y    Acc_Z\n")
    UFOmod.append("'\n")
    UFOmod.append(" GRAVITY  {:8.0f}    {: 6.4f}  {: 6.4f}  {: 6.4f}\n"
                  .format(load_number, 0, 0, gravity * -1.0))
    UFOmod.append("'\n")
    return UFOmod
#
#
def print_calm_sea(_nameWave, water_depth):
    """
    """
    _type = 'AIRY' # 1.0
    #UFOmod = []
    head = 'METOCEAN DATA'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    # Wave integration points
    FEMufo.append("'{:} Member Integration Section\n".format(56 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'    This record is used to define number of integration sections to be used in\n")
    FEMufo.append("'    connection with wave load calculations.\n")
    FEMufo.append("'\n")
    FEMufo.append("'           NIS     {Id_List}\n")
    FEMufo.append("WAVE_INT     10\n")
    FEMufo.append("'\n")
    FEMufo.append("'    NOTE ! If no elements are specified, all beam elements are using NIS\n")
    FEMufo.append("'           integration sections.\n")
    FEMufo.append("'\n")
    FEMufo.append("'{:} External Pressure Effects\n".format(57 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'    With this record USFOS find elements for external pressure\n")
    FEMufo.append("'    (Flooded elements are skipped)\n")
    FEMufo.append("'\n")
    FEMufo.append("EXTPRES     Auto\n")
    FEMufo.append("'\n")
    FEMufo.append("'{:} Wave Switches Section\n".format(61 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("' Do not store data for visualization of wave (Default on)\n")
    FEMufo.append("' SWITCHES   WaveData      NoStore\n")
    FEMufo.append("'\n")
    FEMufo.append("' Switches OFF Doppler effects (Default on)\n")
    FEMufo.append("' SWITCHES   WaveData      NoDoppler\n")
    FEMufo.append("'\n")
    FEMufo.append("' Stream Function order (Default 10)\n")
    FEMufo.append("' SWITCHES   WaveData      StreamOrd    10\n")
    FEMufo.append("'\n")
    FEMufo.append("'\n")    
    FEMufo.append("'{:} Calm Sea\n".format(74 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'\n")
    FEMufo.append("'           Lcase  Type  Height  Period  W_Dir   Phase   SurfLev   WDepth\n")
    FEMufo.append("WAVEDATA  {:7.0f}  {:}  {:4.4f}  {:4.4f}  {:4.4f}  {:4.4f}  {:4.4f}  {:4.4f}\n"
                  .format(_nameWave, _type, 0.01 , 4, 0, 0.0, water_depth, 0))
    #
    FEMufo.append("'\n")
    FEMufo.append("'{:} Buoyancy\n".format(74 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("' The buoyancy forces are added to the WAVEDATA lcase\n")
    FEMufo.append("' The buoyancy forces are updated every time wave loads are calculated,\n")
    FEMufo.append("' and the current position of the sea surface defines whether an element\n")
    FEMufo.append("' becomes buoyant or not at any time.\n")
    FEMufo.append("'\n")
    FEMufo.append("BUOYANCY                ! Account for Buoyancy\n")
    FEMufo.append("'\n")      
    #
    return FEMufo
#
#
#