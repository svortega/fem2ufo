# 
# Copyright (c) 2009-2018 fem2ufo
# 

#
# Python stdlib imports

# package imports
import fem2ufo.process.control as process



# --------------------------------------------------------------------
#
def print_displacement_control_header():
    """
    """
    head = 'CONTROL DISPLACEMENT'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")

    FEMufo.append("'    This record is used to specify the Control Displacement of the structure. \n")
    FEMufo.append("'\n")

    return FEMufo


#
def print_displacement_control(_cnodes, _dof):
    """
    cnodes : list of control nodes
    _dof : [x, y, z]. If dof = 0, will be ignored, factor = dofi
    """
    _dof_text = ['X', 'Y', 'Z']
    FEMufo = []

    FEMufo.append("'\n")
    FEMufo.append("'         ncnods \n")

    if _cnodes:
        _noCnodes = len(_cnodes)
        FEMufo.append("CNODES         {:}\n".format(_noCnodes))
        FEMufo.append("'\n")
        FEMufo.append("'          nodex    idof      dfact\n")

        for _nodes in _cnodes:
            FEMufo.append("'\n")
            for _no in range(3):
                try:
                    _test = 1 / _dof[_no]

                    FEMufo.append(" {:15.0f}      {:2.0f}      {:2.3f}   ! {:} displacement \n".
                                  format(_nodes, _no + 1, _dof[_no], _dof_text[_no]))

                except ZeroDivisionError:
                    continue

        FEMufo.append("'\n")

    else:
        FEMufo.append("' CNODES       1      !  External (user-specified) number of control node\n")
        FEMufo.append("'\n")
        FEMufo.append("'          nodex    idof   dfact \n")
        FEMufo.append("'\n")
        FEMufo.append("'          10200       1       1   ! X displacement \n")
        FEMufo.append("'          10200       2       1   ! Y displacement \n")
        FEMufo.append("'          10200       3       1   ! Z displacement \n")
        FEMufo.append("'\n")

    return FEMufo


#
def print_bounday(nodes):
    #
    head = 'BOUNDARY CONDITIONS'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    _iter = 0
    for _node in nodes.values():
        try:
            _node.boundary.releases
        #if _node.boundary:
            if _iter == 0:
                FEMufo.append("'    With this record the user may override the boundary conditions defined on the\n")
                FEMufo.append("'    structural file.\n")
                FEMufo.append("'\n")
                FEMufo.append("'                                  (0:Free, 1:Fixed)               (NODE/ALL) \n")
                FEMufo.append("'              ix     iy     iz    irx    iry    irz       Type    Id_List \n")
                FEMufo.append("'\n")
            FEMufo.append("CHG_BOUN  ")
            for _bound in _node.boundary.releases:
                _ir = 0
                if _bound != 0:
                    _ir = 1
                FEMufo.append((" {:6.0f}").format(_ir))
            FEMufo.append(("       Node    {:}\n").format(_node.number))
            _iter += 1
        except AttributeError:
            continue
    #
    return FEMufo


#
#
# --------------------------------------------------------------------
#
#
def print_joints(joint, _rule, eccentricity=True):
    """
    """
    head = 'JOINT MODELLING'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    #
    FEMufo.append("'\n")
    FEMufo.append("'    This record is used to reclassified according to geometry and force state at\n")
    FEMufo.append("'    specified intervals during the analysis. The joint capacities will be updated\n")
    FEMufo.append("'    according to the revised classification, P-delta curves for each joint degree\n")
    FEMufo.append("'    of freedom and the Qf factor will be updated.\n")
    FEMufo.append("'\n")
    FEMufo.append("'    interval : \n")
    FEMufo.append("'              0   = No joint classification\n")
    FEMufo.append("'              1   = Continuous joint reclasification (defaut)\n")
    FEMufo.append("'              n>1 = Joints will be reclassified at every nth step \n")
    FEMufo.append("'\n")
    FEMufo.append("'         interval\n")
    FEMufo.append("' JNTCLASS         1 \n")
    FEMufo.append("'\n")
    if eccentricity:
        FEMufo.append("Switches   Joint  EccUpdate  ON     ! Automatic eccentricity repair\n")
    else:
        FEMufo.append("Switches   Joint  EccUpdate  OFF    ! No checking\n")
    FEMufo.append("'\n")
    # try:
    FEMufo.append("'              nodex       elnox1       elnox2        geono        irule\n")
    FEMufo.append("'\n")
    for key, _joint in joint.items():

        if len(_joint.brace) >= 10:
            FEMufo.append("' joint with braces [{:}] > 10  --> ignored\n"
                          .format(len(_joint.brace)))
            FEMufo.append("'")

        elif len(_joint.brace) == 0:
            FEMufo.append("' joint has no braces  --> ignored\n"
                          .format(len(_joint.brace)))
            FEMufo.append("'")
        
        if 'jt' in _joint.name.lower() or _rule in _joint.name.lower():
            FEMufo.append("CHJOINT {:12.0f} {:12.0f} {:12.0f} {:12.0f} {:>12s}   ! {:}\n"
                          .format(_joint.node.number, int(_joint.chord[0]), int(_joint.chord[1]),
                                  0, _rule, _joint.name))

        # except : pass

    FEMufo.append("'\n")
    FEMufo.append("'\n")
    FEMufo.append("'    This record is used to include grouted joint formulation \n")
    FEMufo.append("'\n")
    FEMufo.append("'               Keyword     Joint IDs\n")
    FEMufo.append("' JNTOPTION     Grouted     100 101 102  ! The actual joint(s) uses grouted \n")
    FEMufo.append("'                                        ! capacity formulation\n")
    FEMufo.append("' JNTOPTION   NoGrouted     200 201 202  ! Grout option is switched Off \n")
    FEMufo.append("'\n")
    #
    return FEMufo


#
#
# --------------------------------------------------------------------
def print_part_data_joint(joint, _rule, _step=0):
    """
    """
    head = 'JOINT PART ATTRIBUTE DATA'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    #
    _step += 1
    FEMufo.append("PartData          {:}          0      0     100      0            1         1   ! Cans \n"
                  .format(_step))
    FEMufo.append("Name   Part       {:}       Cans \n".format(_step))
    _canNo = _step
    #
    FEMufo.append("'\n")
    #
    #
    #

    FEMufo.append("'\n")
    FEMufo.append("'{:} Cans \n".format(78 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("PartElem  {:}   ELEM \n".format(_canNo))
    _noSlot = 0
    for _joint in joint.values():
        
        if len(_joint.brace) >= 10 or len(_joint.brace) == 0:
            continue
        
        if 'jt' in _joint.name.lower() or _rule in _joint.name.lower():
            _noSlot += 1
            FEMufo.append(' {:9.0f} {:9.0f}'
                          .format(int(_joint.chord[0]), int(_joint.chord[1])))
            if 2 * _noSlot == 8:
                FEMufo.append("\n")
                _noSlot = 0
    #
    FEMufo.append("\n")
    FEMufo.append("'\n")
    return FEMufo
#
#