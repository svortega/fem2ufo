# 
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import math
import re

# package imports
import fem2ufo.process.control as process

#
#
def print_fracture_header():
    """
    """
    head = 'FRACTURE DUCTILITY CONTROL'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    FEMufo.append("'    This record is used to to force elements to be fractured \n")
    FEMufo.append("' \n")
    FEMufo.append("'    ListType  : Element, Material & Geometry\n")
    FEMufo.append("'    CritType  : Cut, Time, Lcomb, Util,  & Strain \n")
    FEMufo.append("'    Criterion : Time, Lcomb, Util,  & Strain \n")
    FEMufo.append("'    IDList    : List of IDs for which the UserFrac should be applied \n")
    FEMufo.append("' \n")
    return FEMufo
#
def print_fracture(elements, sets, _strain, _list, _type):
    """
    """
    FEMufo = []
    if 'group' in _type.lower():
        for _set in _list:
            _setNo = int(_set)
            FEMufo.append("'\n")
            FEMufo.append("'           ListTyp   CritTyp   Crit   IDList \n")
            FEMufo.append("'\n")
            #
            try:
                FEMufo.append("UserFrac    Element    Strain   {:} {:}! Group name : {:}\n"
                              .format(_strain, 7 * ' ', sets[_setNo].name))
            except KeyError:
                continue

            FEMufo.extend(print_members(sets[_setNo].items, elements))
    else:
        if _list:
            FEMufo.append("UserFrac    Element    Strain   {:}\n"
                          .format(_strain))
            FEMufo.extend(print_members(_list, elements))
    #
    FEMufo.append("'\n")
    return FEMufo
#
def print_linear_header(_head='LINEAR'):
    """
    """
    head = str(_head) + ' ELEMENTS CONTROL'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    FEMufo.append("'    This record is used to specify elements, which should be linear \n")
    FEMufo.append("' \n")
    FEMufo.append("'    Type      : For conventional beam, type = 0. (for riser beam, type=1) \n")
    FEMufo.append("'    ListType  : Element, Material, Geometry or All\n")
    FEMufo.append("'    List      : List of actual IDs \n")
    FEMufo.append("' \n")

    return FEMufo
#
def print_linear(elements, sets, _group, _memb):
    """
    """
    FEMufo = []
    #
    for _set in _group:
        _setNo = int(_set)
        FEMufo.append("\n")
        FEMufo.append("'\n")
        FEMufo.append("'           Type   ListType   IDList \n")
        FEMufo.append("'\n")
        try:
            FEMufo.append("Lin_Elem       0    Element  {:}! Group name : {:}\n"
                          .format(15 * ' ', sets[_setNo].name))

            FEMufo.extend(print_members(sets[_setNo].items, elements))
        except KeyError:
            print("    ** warning linear set : {:} no found".format(_setNo))
            continue
    #
    #
    if _memb:
        FEMufo.append("\n")
        FEMufo.append("'\n")
        FEMufo.append("'           Type   ListType   IDList \n")
        FEMufo.append("'\n")
        FEMufo.append("Lin_Elem       0    Element  {:}\n"
                      .format(15 * ' '))
        FEMufo.extend(print_members(_memb, elements))
    #
    FEMufo.append("'\n")
    return FEMufo
#
def print_members(items, elements, _title=''):
    """
    """
    FEMufo = []
    _no_members = len(items)
    _no_slots = 0
    for _memb in items:
        try:
            _elem = elements[_memb]
        except KeyError:
            continue
        #
        _no_slots += 1
        if _no_slots == 1: 
            FEMufo.append("{:}".format(_title))

        FEMufo.append(" {:11.0f}".format(_elem.number))
        if _no_slots == 6 :
            FEMufo.append(" \n")
            _no_slots = 0
    #
    FEMufo.append("\n")
    return FEMufo
#
#
def print_nonstructural_header():
    """
    """
    head = 'NONSTRUCTURAL ELEMENTS CONTROL'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    FEMufo.append("'    This record is used to specify non structural beam elements (passive elements) \n")
    FEMufo.append("' \n")
    FEMufo.append("'    ListType  : Element, Material, Geometry, Group & Visible\n")
    FEMufo.append("'    List      : List of actual IDs \n")
    FEMufo.append("' \n")
    return FEMufo
#
def print_nonstructural(elements, sets, _groupList, _membList):
    """
    """
    FEMufo = []
    _flag = 0
    for _set in _groupList:
        _flag += 1
        _setNo = int(_set)
        
        if _flag == 1:
            FEMufo.append("\n")
            FEMufo.append("'\n")
            FEMufo.append("'          ListType     IDList \n")
            FEMufo.append("'\n")
            FEMufo.append("NONSTRU     Group\n")
        
        try:
            FEMufo.append("{:} {:11.0f}   ! Name : {:}\n"
                          .format(14*" ", sets[_setNo].number, 
                                  sets[_setNo].name))
        except KeyError:
            continue
    #
    if _membList:
        FEMufo.append("\n")
        FEMufo.append("'\n")
        FEMufo.append("'          ListType     IDList \n")
        FEMufo.append("'\n")
        FEMufo.append("NONSTRU     Element  {:}\n"
                      .format(23 * ' '))
        FEMufo.extend(print_members(_membList, elements))
    #
    FEMufo.append("'\n")
    return FEMufo
#
def print_imperfection():
    """
    """
    head = 'MEMBER MODELLING'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    FEMufo.append("'{:} Initial Deformation & Damage Control \n".format(47 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("' Default definition the out of straightness of the beam element\n")
    FEMufo.append("' All beam elements will get an initial out of straightness of 0.15%\n")
    FEMufo.append("'\n")
    FEMufo.append("'        Offset   Angle\n")
    FEMufo.append("BANANA   0.0015       0\n")
    FEMufo.append("'\n")
    #FEMufo.append("'          Size/ColumnCurve Pattern LoadCase\n")
    #FEMufo.append("' CINIDEF                15       1        \n")
    #FEMufo.append("'\n")
    return FEMufo
#
def print_external_pressure(elements, sets, _grouplist, _membList,
                            _hisurf, _losurf, _fluiddensity, _gravity,
                            factors):
    """
    """
    head = 'EXTERNAL PRESSURE EFFECTS'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    _lenFactor = factors[0]
    _forceFactor = factors[4]
    _density = factors[1] / factors[0] ** 3
    # fix this 
    _gravFactor = _lenFactor
    #
    FEMufo.append("'    This record is used to identify the sea surface level and sea bottom position \n")
    FEMufo.append("'    relative to the structure, as well as properties of the surrounding fluid\n")
    FEMufo.append("'\n")
    FEMufo.append("'                hisurf          losurf         density         gravity \n")
    FEMufo.append("SURFLEV   {: 1.6e}   {: 1.6e}   {: 1.6e}   {: 1.6e} \n"
                  .format(_hisurf * _lenFactor, _losurf * _lenFactor,
                          _fluiddensity * _density, _gravity))
    FEMufo.append("'\n")
    FEMufo.append("'\n")
    FEMufo.append("'    This record is used to identify tubular beam elements with external\n")
    FEMufo.append("'    hydrostatic pressure\n")
    #
    #
    for _item in _grouplist:
        try:
            _set = sets[int(_item)]
        except KeyError:
            continue

        FEMufo.append("'\n")
        FEMufo.append("' Group number : {:} [{:}] \n"
                      .format(_set.number, _set.name))
        FEMufo.append("'\n")

        _title = "EXTPRES"
        FEMufo.extend(print_members(_set.items, elements, _title))

        FEMufo.append("'\n")
        #
    #
    if _membList:
        FEMufo.append("'\n")
        FEMufo.append("' Elements\n")
        _title = "EXTPRES"
        FEMufo.extend(print_members(_membList, elements, _title))
        #
    FEMufo.append("'\n")
    #
    return FEMufo
#
#
def print_compactness(elements, sets):
    """
    """
    head = 'D/t PART ATTRIBUTE DATA'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    _step = 0
    FEMufo.append("'\n")
    FEMufo.append("'            PartID   TranspSW      R      G      B    FringSW  SmoothSW \n")
    FEMufo.append("'\n")
    #
    for _key, _item in sets.items():
        if 'Dt_60' == _item.name:
            _step += 1
            FEMufo.append("PartData          {:}        0        0      0    255          1         1   ! D/t>60\n"
                          .format(_step))
            FEMufo.append("Name   Part       {:}      Dt_60 \n".format(_step))
            _Dt60 = _step
        elif 'Dt_80' == _item.name:
            _step += 1
            FEMufo.append("PartData          {:}        0      255      0      0          1         1   ! D/t>80\n"
                          .format(_step))
            FEMufo.append("Name   Part       {:}      Dt_80 \n".format(_step))
            _Dt80 = _step
        #
        # Not longer needed because it's included in usfos + 8.8
        #
        #elif 'slender_sections' == _item.name:
        #    _step += 1
        #    FEMufo.append(
        #        "PartData          {:}        0      139      0      0          1         1   ! slender open sections\n"
        #            .format(_step))
        #    FEMufo.append("Name   Part       {:}      slender\n".format(_step))
        #    _slender = _step

        #elif 'noncompact_sections' == _item.name:
        #    _step += 1
        #    FEMufo.append(
        #        "PartData          {:}        0       85     26    139          1         1   ! noncompact open section\n"
        #            .format(_step))
        #    FEMufo.append("Name   Part       {:}      noncompact\n".format(_step))
        #    _noncompact = _step

        # elif 'zero_Fy' == _item.name:
        #    _step += 1
        #    FEMufo.append("PartData          {:}        0      255     165     0          1         1   ! material with Fy=0\n"
        #                  .format(_step))
        #    FEMufo.append("Name   Part       {:}      zero_Fy\n".format(_step))
        #    _noncompact = _step
    #
    #
    FEMufo.append("'\n")
    for _key, _item in sets.items():
        if 'Dt_60' == _item.name:
            FEMufo.append("'\n")
            FEMufo.append("'{:} D/t>60 \n".format(76 * "-"))
            FEMufo.append("'\n")
            FEMufo.append("PartElem  {:}   ELEM \n".format(_Dt60))
            FEMufo.extend(print_members(_item.items, elements))
            #FEMufo.append("\n")
        elif 'Dt_80' == _item.name:
            FEMufo.append("'\n")
            FEMufo.append("'{:} D/t>80 \n".format(76 * "-"))
            FEMufo.append("'\n")
            FEMufo.append("PartElem  {:}   ELEM \n".format(_Dt80))
            FEMufo.extend(print_members(_item.items, elements))

            #FEMufo.append("\n")
        # 
        # Not longer needed because it's included in usfos + 8.8
        #        
        #elif 'slender_sections' == _item.name:

            #FEMufo.append("'\n")
            #FEMufo.append("'{:} slender open sections \n".format(61 * "-"))
            #FEMufo.append("'\n")
            #FEMufo.append("PartElem  {:}   ELEM \n".format(_slender))

            #FEMufo.extend(print_members(_item.items, elements))

            #FEMufo.append("\n")
        # 
        #elif 'noncompact_sections' == _item.name:

            #FEMufo.append("'\n")
            #FEMufo.append("'{:} noncompact open sections \n".format(58 * "-"))
            #FEMufo.append("'\n")
            #FEMufo.append("PartElem  {:}   ELEM \n".format(_noncompact))

            #FEMufo.extend(print_members(_item.items, elements))

            #FEMufo.append("\n")

        #
        # elif 'zero_Fy' == _item.name:
        #    FEMufo.append("'\n")
        #    FEMufo.append("'{:} material with Fy=0 \n".format(58*"-"))
        #    FEMufo.append("'\n")
        #    FEMufo.append("PartElem  {:}   ELEM \n".format(_noncompact))
        #    FEMufo.extend(print_members(_item.items, elements))
        #    FEMufo.append("\n")
        #
    #
    FEMufo.append("'\n")
    return FEMufo, _step
#