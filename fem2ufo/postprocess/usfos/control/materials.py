# 
# Copyright (c) 2009-2018 fem2ufo
# 

#
# Python stdlib imports

# package imports
import fem2ufo.process.control as process

# --------------------------------------------------------------------
#
def print_modify():
    """
    """
    #
    head = 'MATERIAL MODIFICATION SECTION'
    FEMufo = process.common.headers.print_head_line(head, subfix="'")
    #
    FEMufo.append("'    With this record the user may override the material referred to on the structural\n")
    FEMufo.append("'    file\n")
    FEMufo.append("' \n")
    FEMufo.append("'         Mat_ID     Type   Id_List (Element, Geo or Group)\n")
    FEMufo.append("' \n")
    FEMufo.append("' Chg_Mat    200  Element   101 102 103\n")
    FEMufo.append("' \n")
    #
    return FEMufo

