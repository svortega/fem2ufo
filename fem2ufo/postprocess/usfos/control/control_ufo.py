# 
# Copyright (c) 2009-2018 fem2ufo
# 

"""Class writes out usfos control file"""

#
# Python stdlib imports



# package imports
import fem2ufo.process.control as process
import fem2ufo.postprocess.usfos.control.setup as setup
import fem2ufo.postprocess.usfos.control.loading as load
import fem2ufo.postprocess.usfos.control.foundations as foundations
import fem2ufo.postprocess.usfos.control.members as members
import fem2ufo.postprocess.usfos.control.nodes as nodes
import fem2ufo.postprocess.usfos.control.materials as materials

#import fem2ufo.postprocess.usfos.operation.formatting as formatting
#import fem2ufo.postprocess.usfos.operation.headers as common
#
#
class control_ufo():
    """ usfos control file class"""
    def __init__(self):
        """
        class to create usfos control file
        """
        msg = []
        self.total_members = []
        self.ufo_fracture = []
        self.ufo_memb_linear = []
        self.ufo_memb_jelly = []
        self.ufo_memb_nonstructural = []
        self.ufo_joint = []
        self.ufo_node_control = []
        self.ufo_Dt = []
        self.ufo_hydropressure = []
        self.ufo_ship_impact = []
        self.ufo_seismic = []
        self.ufo_boundary = []
        self.ufo_metocean = []
        #
        self.step_partData = 0
        #
        Type = 'USFOS Control'
        self.ufo_title = process.common.headers.print_title(Type, subfix="'")
        #
        #self.units_in = None
        #
        # set up
        self.ufo_head = setup.print_general()
        # self.ufo_general.extend(print_analysis_control())
        self.ufo_head.extend(setup.print_numerical_parameters())
        #
        self.ufo_general = setup.print_FEM_checking()
        self.ufo_general.extend(setup.print_swithches())
        #
        # self.ufo_general.extend(materials.print_modify())
        #
        self.ufo_general.extend(members.print_imperfection())
        #
        # default load combination
        self.ufo_load_combination = load.print_basic_load_combination()
        #
        # default load control
        self.analysis_type = 'static'
        self.ufo_load_control_header = load.print_control_head(self.analysis_type)
        self.ufo_load_control = []
        self.ufo_seismic_load_control = []
        self._time_history = {}
        #
        self.gravity_load = None
    #
    # TODO : check this
    def analysis(self, type_analysis):
        """
        **Parameters**:
          type_analysis: select analysis type:
                         - static (dafault)
                         - Dynamic
                         - Seismic
                         - Ship impact (static/dynamic)
        """
        self.analysis_type = type_analysis
        self.ufo_load_control_header = load.print_control_head(self.analysis_type)
        
        if self.analysis_type != 'static':
            if not self.gravity_load:
                self.gravity_load = 1
                self.ufo_load_combination = load.print_gravity(self.gravity_load)
                # define load gravity mass combination
                self.ufo_load_combination.extend(load.print_basic_load_combination(self.gravity_load))
            else:
                # define load gravity mass combination
                self.ufo_load_combination = load.print_basic_load_combination(self.gravity_load)
            #
            #self.ufo_load_control = load.print_TH_header(self.gravity_load)
        #
    #
    def model(self, model):
        """
        Input fem2ufo concept model
        
        **Parameters**:
           :model: fem2ufo's geometry FE concept model
           :loading: fem2ufo's loading FE concept model
        """
        self.component = model.component
        data = self.component.data
        Type = 'USFOS Control'
        self.ufo_title = process.common.headers.print_title(Type, "'", data)
        self.ufo_boundary = nodes.print_bounday(self.component.nodes)
        #
        #self.units_in = model.data['units']

        # D/t data
        _Dt, self.step_partData = members.print_compactness(self.component.elements,
                                                            self.component.sets)
        self.ufo_Dt.extend(_Dt)
        #
        _gravity = None
        loading = model.load.functional
        if loading:
            for _load in loading.values():
                if _load.gravity:
                    _gravity = _load.number
                    break
        #
        self.gravity_load = _gravity

    #
    def linear(self, elements, groups):
        """
        Print linear elements 
        
        **Parameters**:
          :elements: list of element FE number 
          :groups: list of element's group FE number 
        """
        self.ufo_memb_linear.extend(members.print_linear(self.component.elements,
                                                         self.component.sets,
                                                         groups, elements))

        for _set_no in groups:
            try:
                _temp = list(_item for _item in self.component.sets[int(_set_no)].items)
                self.total_members.extend(_temp)
            except KeyError:
                print(' set {:} doesnt exist'.format(_set_no))
    #
    def jelly(self, elements, groups):
        """
        Print element with jelly properties (currently disable)
        
        **Parameters**:
          :elements: list of element FE number 
          :groups: list of element's group FE number 
        """
        self.ufo_memb_jelly.extend(members.print_linear(self.component.elements,
                                                        self.component.sets,
                                                        groups, elements))

        for _set_no in groups:
            try:
                _temp = list(_item for _item in self.component.sets[int(_set_no)].items)
                self.total_members.extend(_temp)
            except KeyError:
                print(' set {:} doesnt exist'.format(_set_no))
    #
    def nonstructural(self, elements, groups):
        """
        Print non-structural beam elements
        
        **Parameters**:
          :elements: list with element's FE number 
          :groups: list with element's group FE number 
        """
        self.ufo_memb_nonstructural.extend(members.print_nonstructural(self.component.elements,
                                                                       self.component.sets,
                                                                       groups, elements))

        for _set_no in groups:
            try:
                _temp = list(_item for _item in self.component.sets[int(_set_no)].items)
                self.total_members.extend(_temp)
            except KeyError:
                print(' set {:} doesnt exist'.format(_set_no))
    #
    def fracture(self, strain, elements, groups):
        """
        Print elements to be fractured with strain limits

        **Parameters**:
          :strain: tensile strain limit
          :elements: list with element's FE number 
          :groups: list with element's group FE number 
        """
        _type = 'group'
        self.ufo_fracture.extend(members.print_fracture(self.component.elements,
                                                        self.component.sets,
                                                        strain, groups,
                                                        _type))

        _type = 'member'
        self.ufo_fracture.extend(members.print_fracture(self.component.elements,
                                                        self.component.sets,
                                                        strain, elements,
                                                        _type))
    #
    def joints(self, joints, rule, eccentricity=True):
        """
        Print joint components (brace, chord and rule)
        
        **Parameters**:
          :joints: list of joints
          :rule: joint rule to be used 
          :eccentricity : (True/False)
        """
        self.ufo_joint = nodes.print_joints(joints, rule, eccentricity)

        self.ufo_Dt.extend(nodes.print_part_data_joint(joints, rule,
                                                       self.step_partData))
    #
    def node_control(self, node, dof=False):
        """
        Print control node
        
        **Parameters**:
          :node: list with node's number
          :dof: global degree of freedom list [x, y, z] 
                  - 1 degree of freedom on  
                  - 0 degree of freedom off  
                default [1,1,1]
        """
        if not dof:
            dof = [1, 1, 1]
        #
        self.ufo_node_control.extend(nodes.print_displacement_control(node, dof))
    #
    def hydropressure(self, elements, groups,
                      surface, mudline,
                      fluid_density, gravity,
                      factors):
        """
        Print the sea surface level and sea bottom position relative
        to the structure.
        
        **Parameters**:
          :elements: list with element's FE number 
          :groups: list with element's group FE number 
          :surface : Surface elevation (mandatory)
          :mudline : mudline elevatio (mandatory)
          :fluid_density : fluid density (dafault)
          :gravity : gravity (defaul 9.81 m/s)
          :factors : factors used for unit conversion  
                     [length, mass, time, temperature, force, pressure/stress]
        """
        self.ufo_hydropressure.extend(members.print_external_pressure(self.component.elements,
                                                                      self.component.sets,
                                                                      groups, elements,
                                                                      surface, mudline,
                                                                      fluid_density, gravity,
                                                                      factors))
    #
    def foundation(self, foundation, factors):
        """
        Print soil and piles 
        
        **Parameters**:  
          :pile: pile fem2ufo data
          :soil: soil fem2ufo data
          :factors : factors used for unit conversion  
                     [length, mass, time, temperature, force, pressure/stress]
        """
        # 
        pile = foundation.piles
        if foundation.soil:
            soil = foundation.soil
        else:
            print('    ** Warning -- > No soil defined')
            return 
        #
        # simplify pile               
        #
        # This is specific for usfos, need to be relocated
        # Eliminate redundand pile items in the group that share
        # same section & material properties
        #
        # TODO : do we need this module?
        pile = foundations.clean_ufo_pile_set(pile)
        #
        #
        # foundations & pile (pile will not be printed if soil fail)
        self.ufo_boundary = foundations.print_foundation()
        self.ufo_boundary.extend(nodes.print_bounday(self.component.nodes))
        self.ufo_boundary.extend(foundations.print_pile(pile, soil, factors))
        self.ufo_boundary.extend(foundations.print_soil(soil, factors))
        #
    #
    def metocean(self, metocean, factors=None):
        """
        Print metocean additional usfos file
        
        **Parameters**: 
          :metocean: metocean fem2ufo data
        """
        _analysis_type = self.analysis_type
        #
        if 'seismic' in _analysis_type:
            self.ufo_metocean = load.print_metocean_control(metocean, _analysis_type)
            #header = 'SEISMIC TIME HISTORY CONTROL'
            _buoyancy = None
            #
            for key in sorted(metocean, key = lambda name: metocean[name].number):
                _metocean = metocean[key]
                _wave = _metocean.wave
                _water_depth = _wave.water_depth * factors[0]
                if 'calm_sea' in _wave.theory:    
                        _buoyancy = _wave.name
                        break
            #
            if not _buoyancy:
                _buoyancy = 600
            self.ufo_metocean = load.print_calm_sea(_buoyancy, _water_depth)
            
            header = 'METOCEAN TIME HISTORY CONTROL'
            self.ufo_load_control = load.print_TH_header(self.gravity_load, 
                                                         buoyancy=True, header=header)
            self.ufo_load_control.extend(load.print_metocean_TH_parameters(metocean, _buoyancy))
        
        elif 'ship_impact' in _analysis_type:
            pass
            # static
            # else:
            # self.ufo_metocean.extend(print_load_control())
        
        else:
            self.ufo_metocean = load.print_metocean_control(metocean, 
                                                            _analysis_type)
            #
            if _analysis_type != 'static':
                header = 'METOCEAN TIME HISTORY CONTROL'
                self.ufo_load_control = load.print_TH_header(self.gravity_load,
                                                             header=header)
                self.ufo_load_control.extend(load.print_metocean_TH_parameters(metocean))
            #
    #
    def seismic_soil_motion(self, case, depth, factors):
        """
        Print foundation's seismic analysis time history 
        
        **Parameters**: 
          :case: acceleration,velocity/displacement
          :depth: soil depth time history application
          :factors : factors used for unit conversion  
                     [length, mass, time, temperature, force, pressure]
          ***
          :end_time: time history total time
          :dT: time history time step
        """
        #
        self.ufo_seismic_load_control.extend(load.print_seismic_soil(case, depth, factors))
        for key, _depth in depth.items():
            for _name, _serie in _depth.items():
                try:
                    self._time_history[_serie[0]].append(_name)
                except KeyError:
                    self._time_history[_serie[0]] = [_name]
    #
    def pile_subsidence(self, case, pile):
        """
        """
        #print('here')
        self.ufo_seismic_load_control.extend(load.print_pile_subsidence(case, pile))
        for _name, _serie in pile.items():
            try:
                self._time_history[_serie[0]].append(_name)
            except KeyError:
                self._time_history[_serie[0]] = [_name]
    #
    def seismic_node_motion(self, case, node):
        """
        Print node's seismic analysis time history
        
        **Parameters**: 
          :case: acceleration,velocity/displacement
          :node: list with node's number
          :end_time: time history total time
          :dT: time history time step
        """
        #
        self.ufo_seismic_load_control.extend(load.print_seismic_node(case, node))
        for _name, _serie in node.items():
            try:
                self._time_history[_serie[0]].append(_name)
            except KeyError:
                self._time_history[_serie[0]] = [_name]
    #
    def ship_impact(self):
        """
        Print ship impact analysis (no yet implemented)
        """
        # elif master.analysis['ship_impact']:
        # load control
        fileUFO.extend(load.print_load_combination())
        fileUFO.extend(load.print_load_control())
        # static by default
    #
    def load_combination(self):
        """
        Print load combination (no yet implemented)
        """
        pass
    #
    def print_control_file(self, file_name):
        """
        Write out usfos control file
        """
        # print('')
        print('--- Writing USFOS control file')
        # preprocessor

        # if not fracture then define group
        if not self.ufo_fracture:
            _strain = 15 / 100.0
            # _member = list(_member for _member in self.component.elements.keys())
            _member = []
            for _key, _memb in self.component.elements.items():
                if 'beam' in _memb.type.element:
                    _member.append(_key)

            _list = list(set(self.total_members))
            _total = _list
            _list = list(set(_member) - set(_total))

            _type = 'member'
            self.ufo_fracture = members.print_fracture(self.component.elements,
                                                       self.component.sets,
                                                       _strain, _list,
                                                       _type)
        #
        # print file
        #
        _name_file = process.common.split_file_name(file_name)
        file_control = 'control_' + str(_name_file[0]) + '.fem'
        _control = open(file_control, 'w+')

        # headers
        _control.write("".join(self.ufo_title))
        _control.write("".join(self.ufo_head))

        # metocean
        if self.ufo_metocean:
            _control.write("".join(self.ufo_metocean))

        # load comb
        _control.write("".join(self.ufo_load_combination))

        # load control
        _control.write("".join(self.ufo_load_control_header))
        _control.write("".join(self.ufo_load_control))
        #
        if 'seismic' in self.analysis_type:
            if self.ufo_metocean:
                _control.write("".join(load.print_seismic_header(self._time_history)))
            else:
                _control.write("".join(load.print_seismic_header(self._time_history,
                                                                 self.gravity_load)))
            _control.write("".join(self.ufo_seismic_load_control))
        #
        if not 'static' in self.analysis_type:
            _control.write("".join(setup.print_dynamic_parameters()))

        # main body
        _control.write("".join(self.ufo_general))
        # node control
        _control.write("".join(nodes.print_displacement_control_header()))
        _control.write("".join(self.ufo_node_control))
        # linear
        _control.write("".join(members.print_linear_header()))
        _control.write("".join(self.ufo_memb_linear))

        # CHECKME : see if this simplication works --> material linear
        # jelly
        # _control.write("".join(print_linear_header('JELLY')))
        # _control.write("".join(self.ufo_memb_jelly))

        # non structural
        _control.write("".join(members.print_nonstructural_header()))
        _control.write("".join(self.ufo_memb_nonstructural))
        # fracture
        _control.write("".join(members.print_fracture_header()))
        _control.write("".join(self.ufo_fracture))
        # hydro pressure
        if self.ufo_hydropressure:
            _control.write("".join(self.ufo_hydropressure))

        # joint section
        _control.write("".join(self.ufo_joint))

        # boundary contion section
        _control.write("".join(self.ufo_boundary))

        # part attribute data
        _control.write("".join(self.ufo_Dt))

        # End & close file
        _control.write("".join(process.common.headers.print_EOF(subfix="'")))
        _control.close()
        #
        print('    * File : {:}'.format(file_control))
#
#
