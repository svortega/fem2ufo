# 
# Copyright (c) 2009-2018 fem2ufo
# 


# Python stdlib imports
#


# package imports
import fem2ufo.process.control as process
import fem2ufo.postprocess.usfos.operation.process as usfos_process
import fem2ufo.postprocess.usfos.model.hydro as hydro
import fem2ufo.postprocess.usfos.model.loading as load
import fem2ufo.postprocess.usfos.model.foundations as soil_spring
import fem2ufo.postprocess.usfos.model.members as members
import fem2ufo.postprocess.usfos.model.materials as materials
import fem2ufo.postprocess.usfos.model.sections as sections
import fem2ufo.postprocess.usfos.model.nodes as nodes

#
#
class model_ufo():
    """Class writes out fem2ufo's concept FE model in ufo format and additional usfos files"""
    #
    def __init__(self):
        """
        """
        self.ufo_model = []
        self.ufo_groups = []
        self.ufo_foundation = []
        self.ufo_metocean = []
        self.ufo_hydro_parameters = []
        self.ufo_timehistory = {}
        self.spokes = {}
        self.ufo_check_out = []
    #
    def model(self, model, loading, factors,
              hinges=False):
        """
        **Parameters**:
           :model: fem2ufo's geometry FE concept model
           :loading: fem2ufo's loading FE concept model
           :factors : factors used for unit conversion  
                      [length, mass, time, temperature, force, pressure/stress]
        """
        #
        # --------------------------------------------------------------------
        # check for linear_dependency modelling
        # --------------------------------------------------------------------
        #
        self.spokes = usfos_process.linear_dependency(model, factors)
        #        
        # --------------------------------------------------------------------
        # print main model
        # --------------------------------------------------------------------
        #
        data = model.data
        Type = 'USFOS Model'
        self.ufo_model = process.common.headers.print_title(Type, subfix="'", data=data)
        self.ufo_model.extend(nodes.print_data(model.nodes, 
                                               factors[0]))

        _member, _msg = members.print_elements(model.elements)
        self.ufo_model.extend(_member)
        self.ufo_check_out.extend(_msg)

        self.ufo_model.extend(sections.print_data(model.sections, factors[0]))
        self.ufo_model.extend(members.print_vectors(model.vectors))
        self.ufo_model.extend(members.print_eccentricities(model.eccentricities, factors[0]))
        self.ufo_model.extend(materials.print_data(model.materials, factors))
        self.ufo_model.extend(load.print_mass(model.nodes, model.elements, factors))
        self.ufo_model.extend(members.print_hinges(model.elements, hinges))

        _load, _msg = load.print_loads(loading, model.nodes, model.elements, factors)
        self.ufo_model.extend(_load)
        self.ufo_check_out.extend(_msg)

        self.ufo_groups.extend(members.print_groups(model.sets, model.elements))
        #
        # print('--- End  Writing USFOS file')
        #     
        # return self.ufo_model
    #
    def foundation(self, foundation, factors):
        """
        **Parameters**:
           :foundation: fem2ufo's foundation FE concept model
           :factors : factors used for unit conversion   
                      [length, mass, time, temperature, force, pressure/stress]
        """
        try:
            _material = foundation.materials
            self.ufo_foundation.extend("'\n")
            self.ufo_foundation.extend("'\n")
            self.ufo_foundation.extend("'\n")
            #_material = foundation.materials
            self.ufo_foundation.extend(soil_spring.print_pytzqz(_material, 
                                                                factors))
        except AttributeError:
            pass
        # return self.ufo_model
    #
    def metocean(self, metocean, factors):
        """
        **Parameters**:
           :metocean: fem2ufo's metocean (hydro & aero) parameters 
           :factors : factors used for unit conversion   
                     [length, mass, time, temperature, force, pressure/stress]
        """
        for _nonhydro in self.spokes.values():
            metocean.nonhydro.append(_nonhydro)
        #
        #
        # Hydro parameters
        self.ufo_hydro_parameters.extend("'\n")
        self.ufo_hydro_parameters.extend("'\n")
        self.ufo_hydro_parameters.extend("'\n")
        self.ufo_hydro_parameters.extend(hydro.print_header_metocean())
        # 
        _coeff_no, _ufo_met = hydro.print_hydro_parameters(metocean, factors)
        self.ufo_hydro_parameters.extend(_ufo_met)
        _coeff_no, _ufo_met = hydro.print_wind_coefficients(metocean, _coeff_no)
        self.ufo_hydro_parameters.extend(_ufo_met)
    #
    def combination(self, analysis, factors):
        """
        """
        # -----------------------------------
        # Wave / wind / current data
        #data = metocean.data
        data = False
        Type = 'USFOS Metocean'
        _elevations = {}
        for _case_name, _analysis in analysis.case.items():
            #
            air_density = _analysis.condition.air_density
            # elevation.surface
            mudline = _analysis.condition.mudline
            minimum_depth = abs(mudline - _analysis.condition.surface)
            i = 0
            for wdepths in _analysis.condition.water_depth:
                i+= 1
                _name = _case_name + '_' + str(i)
                surface = wdepths - minimum_depth
                if surface == 0.0:
                    surface = _analysis.condition.surface
                #
                _elevations[_name] = process.common.headers.print_title(Type, subfix="'" , data=data)
                _elevations[_name].extend(hydro.print_header_metocean())
                _elevations[_name].extend(hydro.print_wave_current(_analysis.metocean_combination, 
                                                                   mudline, surface, factors))
                _elevations[_name].extend(hydro.print_wind(_analysis.metocean_combination, 
                                                           air_density, factors))
        #
        self.ufo_metocean = _elevations
        # -----------------------------------        
    #
    def time_history(self, analysis_name, time_series,
                     time_history_case='acceleration'):
        """
        **Parameters**:
           :database: time history database
           :factors: factors used for unit conversion  
                     [length, mass, time, temperature, force, pressure/stress]
        """
        #
        _keys = {"acceleration": r"\b(acc(eleration)?)\b",
                 "velocity": r"\b(vel(ocity)?)\b",
                 "displacement": r"\b(disp(lacement)?)\b",
                 "time" : r"\b(t(ime)?)\b"}
        #
        _case = process.common.find_keyword(time_history_case, _keys)
        #
        #
        self.ufo_timehistory[analysis_name] = []
        self.ufo_timehistory[analysis_name].extend(load.print_header_TH(analysis_name))
        #
        #th_number = 1
        for x, serie in enumerate(time_series.values()):
            #
            if "acceleration" in _case:
                _factor = serie.factors[0] # m/s**2
            elif "velocity" in _case:
                _factor = serie.factors[0] # m/s
            elif "displacement" in _case:
                _factor = serie.factors[0] # m
            else:
                print('    ** error time_history_case {:} not recognized'.format(time_history_case))
                print('       provide : acceleration, velocity or displacement')
                continue
            #
            time_history_cases = ['time']
            time_history_cases.append(_case)
            #
            th_name = serie.name
            th_number = serie.number
            #
            self.ufo_timehistory[analysis_name].extend(load.print_TH(number=th_number, 
                                                                     time_series=serie.database,
                                                                     factor=_factor, 
                                                                     series_name= th_name,
                                                                     column_names=time_history_cases))
        #
        self.ufo_timehistory[analysis_name].extend(process.common.headers.print_EOF(subfix="'"))
        #
        #
    #
    def get_model(self):
        """
        To be implemented
        """
        pass
    #
    def print_model(self, file_name):
        """
        Write out fem2ufo FE concept model in ufo format
        
        **Parameters**:
           :file_name: output file name
        """
        if self.ufo_model:
            print('--- Writing USFOS model file')
            # Read file
            _name_file = process.common.split_file_name(file_name)
            file_out = str(_name_file[0]) + '_ufo.fem'
            print_out = open(file_out, 'w+')
            print_out.write("".join(self.ufo_model))
            print_out.write("".join(self.ufo_hydro_parameters))
            print_out.write("".join(self.ufo_foundation))
            print_out.write("".join(self.ufo_groups))
            print_out.write("".join(process.common.headers.print_EOF(subfix="'")))
            print_out.close()
            print('    * File : {:}'.format(file_out))
        #
        if self.ufo_metocean:
            print('--- Writing additional hydrodynamic USFOS file')
            for _name, _elev in self.ufo_metocean.items():
                file_add = 'metocean_' + _name + '_ufo.fem'
                wave_out = open(file_add, 'w+')
                wave_out.write("".join(_elev))
                wave_out.write("".join(process.common.headers.print_EOF(subfix="'")))
                wave_out.close()
                print('    * File : {:}'.format(file_add))
        #
        if self.ufo_timehistory:
            print('--- Writing additional time history USFOS file')
            for file_out, ufo_timehistory in self.ufo_timehistory.items():
                _name_file = process.common.split_file_name(file_out)
                file_add = 'TH_' + str(_name_file[0]) + '_ufo.fem'
                # if self.ufo_metocean:
                #    UFOmod.extend(print_calm_sea(metocean, factors))
                add_out = open(file_add, 'w+')
                Type = 'USFOS Time History'
                add_out.write("".join(process.common.headers.print_title(Type, "'")))
                add_out.write("".join(ufo_timehistory))
                add_out.close()
                print('    * File : {:}'.format(file_add))
        #
        file_checkout = process.common.split_file_name(file_name)
        file_checkout = str(file_checkout[0]) + '_check_me.txt'
        add_out = open(file_checkout, 'a')
        header = 'USFOS MODEL PRINTING SECTION'
        add_out.write("".join(process.common.headers.print_head_line(header, subfix="'")))
        add_out.write("".join(self.ufo_check_out))
        add_out.close()
        #
        #
#
#
