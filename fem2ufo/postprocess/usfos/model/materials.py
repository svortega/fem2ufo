# 
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports

# package imports
import fem2ufo.process.control as process



def print_data(material, factors):
    """
    """
    header = 'MATERIAL DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")

    # units
    _forceFactor = factors[4]
    _lenFactor = factors[0]
    _stress = _forceFactor / _lenFactor** 2
    _density = factors[1] / _lenFactor** 3
    # need to fix temperature
    #
    _material_list = [key for key in sorted(material, key = lambda name: material[name].number)
                      if 'plastic' in material[key].type.lower() 
                      or 'elastic' in material[key].type.lower()]
    
    if _material_list:
        UFOmod.append("' \n")
        UFOmod.append("'         Mat_ID    Type  E-modulus    Poisson  Yield(Fy)    Density  Thermal-X\n")
        UFOmod.append("' \n")        
        
        for key in _material_list:
            _mat = material[key]
            UFOmod.append(" MATERIAL {:6.0f} {:7s} {:1.4E} {:1.4E} {:1.4E} {:1.4E} {:1.4E}  ' {:}\n"
                          .format(_mat.number, _mat.type, _mat.E * _stress,
                                  _mat.poisson, _mat.Fy * _stress,
                                  _mat.density * _density, _mat.alpha,
                                  _mat.name))
    #
    #
    _spring_ground = [key for key in sorted(material, key = lambda name: material[name].number)
                      if material[key].type == 'spring to ground']
    #
    if _spring_ground:
        UFOmod.append("' \n")
        UFOmod.append("'         Mat_ID             S P R I N G    C H A R.\n")
        UFOmod.append("' \n")        
        
        for key in _spring_ground:
            _mat = material[key]
            # this is very weak, fix this svo
            if _mat.stiffness[1][0] != 0:
                UFOmod.append(" SPRIFULL {:6.0f}\n".format(_mat.number))
                for i in range(6):
                    UFOmod.append("{:}".format(8 * " "))
                    for j in range(6):
                        UFOmod.append("  {:1.4E}".format(_mat.stiffness[i][j]))
                    UFOmod.append("\n")
            else:
                UFOmod.append(" SPRIDIAG {:6.0f}         {:1.4E} {:1.4E} {:1.4E} \n"
                              .format(_mat.number, _mat.stiffness[0][0], _mat.stiffness[1][1], _mat.stiffness[2][2]))
                UFOmod.append("  {:} {:1.4E} {:1.4E} {:1.4E}\n"
                              .format(22 * " ", _mat.stiffness[3][3], _mat.stiffness[4][4], _mat.stiffness[5][5]))
    # 
    _spring_list = [key for key in sorted(material, key = lambda name: material[name].number)
                    if material[key].type == 'spring']    
    #
    if _spring_list:
        UFOmod.append("'\n")
        UFOmod.append("'        Mat  ID                  P         Delta\n")
        UFOmod.append("'\n")
        
        for key in _spring_list:
            _mat = material[key]
            #_spring_items = {_sprg.name : _sprg for _sprg in _mat.spring if _sprg}
            _spring_name = []
            for _sprg in _mat.spring:
                _top = 0
                _spc = 21 * ' '
                if _sprg and _sprg.name not in _spring_name:
                    _spring_name.append(_sprg.name)
                    UFOmod.append(" HYPELAST {:6.0f}\n".format(_sprg.number))
                    for x in range(len(_sprg.displacement)):
                        if _top == _sprg.displacement[x]: UFOmod.append("'")

                        UFOmod.append("{:} {: 1.6e} {: 1.6e}\n"
                                      .format(_spc, _sprg.force[x] * _forceFactor / _lenFactor,
                                              _sprg.displacement[x] * _lenFactor))
                        _spc = 21 * ' '
                        _top = _sprg.displacement[x]
            #
            #UFOmod.append("'\n")
            UFOmod.append("'        Mat  ID                      S P R I N G    R E F S\n")
            #UFOmod.append("'\n")
            UFOmod.append(" MREF {:10.0f}".format(_mat.number))
            for _sprg in _mat.spring:
                if _sprg:
                    UFOmod.append("{:10.0f}".format(_sprg.number))
                else:
                    UFOmod.append("{:10.0f}".format(0))
            #
            UFOmod.append("\n")
            UFOmod.append("'\n")
            UFOmod.append("'\n")
    #
    return UFOmod
#