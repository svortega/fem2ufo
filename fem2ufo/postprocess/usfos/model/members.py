# 
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import math

# package imports
import fem2ufo.process.control as process
#
import fem2ufo.postprocess.usfos.operation.process as usfos_process
#import fem2ufo.postprocess.usfos.operation.formatting as formatting

#
#
def print_beam(elements):
    """
    """
    UFOmod = []
    check_out = []
    _guide_off = []
    _beams = [_elem for _elem in elements.values() 
              if _elem.type.element == 'beam']
    _iter = 0
    # Beam Elements
    for _elem in _beams:
    #for _elem in elements.values():
        #if _elem.type.element == 'beam':
        if _iter == 0:
            UFOmod.append("'{:} Beam element\n".format(70 * "-"))
            UFOmod.append("'\n")
            UFOmod.append("'            Elem ID      np1      np2   material   geom    lcoor    ecc1    ecc2\n")
            UFOmod.append("'\n")

        UFOmod.append((" BEAM {:14.0f}").format(_elem.number))
        for _node in _elem.node:
            UFOmod.append(" {:8.0f}".format(_node.number))
        UFOmod.append(" {:8.0f}".format(_elem.material[0].number))
        UFOmod.append(" {:8.0f}".format(_elem.section[0].number))
        #
        try:
            UFOmod.append(" {:8.0f}".format(_elem.guidepoint[0].number))
        except IndexError:
            _guide_off.append(_elem.number)
            UFOmod.append(" {:8.0f}".format(0))
        if _elem.offsets:
            for _offset in _elem.offsets:
                try:
                    UFOmod.append(" {:7.0f}".format(_offset.number))
                except AttributeError:
                    UFOmod.append(" {:7.0f}".format(0))
        #
        _iter += 1
        UFOmod.append("\n")    
    
    return UFOmod, check_out, _guide_off
#
def print_truss(elements):
    """
    """
    UFOmod = []
    check_out = []
    _guide_off = []
    _trusses = [_elem for _elem in elements.values() 
                if _elem.type.element == 'truss']
    
    _iter = 0
    for _elem in _trusses:
    #for _elem in elements.values():
        #if _elem.type.element == 'truss':
        if _iter == 0:
            UFOmod.append("'\n")
            UFOmod.append("'{:} Truss element\n".format(69 * "-"))
            UFOmod.append("'\n")
            UFOmod.append("'            Elem ID      np1      np2   material   geom    lcoor    ecc1    ecc2\n")
            UFOmod.append("'\n")

        UFOmod.append((" BEAM {:14.0f}").format(_elem.number))

        for _node in _elem.node:
            UFOmod.append(" {:8.0f}".format(_node.number))

        UFOmod.append(" {:8.0f}".format(_elem.material[0].number))
        UFOmod.append(" {:8.0f}".format(_elem.section[0].number))

        try:
            UFOmod.append(" {:8.0f}".format(_elem.guidepoint[0].number))

        except IndexError:
            _guide_off.append(_elem.number)
            UFOmod.append(" {:8.0f}".format(1))

        if _elem.offsets:
            for _offset in _elem.offsets:
                UFOmod.append(" {:7.0f}".format(_offset.number))

        _iter += 1
        UFOmod.append("\n")
    #
    return UFOmod, check_out, _guide_off
#
def print_shell4(elements):
    """
    """
    UFOmod = []
    check_out = []
    _shells = [_elem for _elem in elements.values() 
               if _elem.type.element.lower() == 'shell' 
               or _elem.type.element.lower() == 'plate']
    
    _iter = 0
    for _elem in _shells:
    #for _elem in elements.values():
        #if _elem.type.element == 'shell' or _elem.type.element == 'plate':
        if len(_elem.node) == 4:
            if _iter == 0:
                UFOmod.append("'\n")
                UFOmod.append("'{:} 4 node shell elements\n".format(61 * "-"))
                UFOmod.append("'\n")
                UFOmod.append(
                    "'            Elem ID     np1      np2      np3      np4    mater   geom      ec1    ec2    ec3    ec4\n")
                UFOmod.append("'\n")

            UFOmod.append(" QUADSHEL {:10.0f}".format(_elem.number))

            for _node in _elem.node:
                UFOmod.append(" {:8.0f}".format(_node.number))

            UFOmod.append(" {:8.0f}".format(_elem.material[0].number))

            UFOmod.append(" {:8.0f}".format(_elem.section[0].number))

            if _elem.offsets:
                for _offset in _elem.offsets:
                    UFOmod.append(" {:7.0f}".format(_offset.number))

            _iter += 1
            UFOmod.append("\n")
    #
    return UFOmod, check_out
#
def print_shell3(elements):
    """
    """
    UFOmod = []
    check_out = []
    _shells = [_elem for _elem in elements.values() 
               if _elem.type.element.lower() == 'shell' 
               or _elem.type.element.lower() == 'plate']
    
    _iter = 0
    for _elem in _shells:
    #for _elem in elements.values():
        #if _elem.type.element == 'shell' or _elem.type.element == 'plate':
        if len(_elem.node) == 3:
            if _iter == 0:
                UFOmod.append("'\n")
                UFOmod.append("'{:} 3 node shell elements\n".format(61 * "-"))
                UFOmod.append("'\n")
                UFOmod.append("'            Elem ID nod1 nod2 nod3 mat geom e1 e2 e3\n")
                UFOmod.append("'\n")

            UFOmod.append(" TRISHELL {:14.0f}".format(_elem.number))

            for _node in _elem.node:
                UFOmod.append(" {:8.0f}".format(_node.number))

            UFOmod.append(" {:8.0f}".format(_elem.material[0].number))

            UFOmod.append(" {:8.0f}".format(_elem.section[0].number))

            if _elem.offsets:
                for _offset in _elem.offsets:
                    UFOmod.append(" {:7.0f}".format(_offset.number))

            _iter += 1
            UFOmod.append("\n")    
    #
    return UFOmod, check_out    
#
def print_membrane4(elements):
    """
    """
    UFOmod = []
    check_out = []
    _membranes = [_elem for _elem in elements.values() 
                  if _elem.type.element.lower() == 'membrane']
    #
    _iter = 0
    #for _elem in elements.values():
    #    if _elem.type.element == 'membrane':
    for _elem in _membranes:
        if _iter == 0:
            UFOmod.append("'\n")
            UFOmod.append("'{:} 4 node membrane elements\n".format(58 * "-"))
            UFOmod.append("'\n")
            UFOmod.append("'            Elem ID nod1 nod2 nod3 nod4 mat geom e1 e2 e3 e4\n")
            UFOmod.append("'\n")

        if len(_elem.node) == 4:
            UFOmod.append(" MEMBRANE {:14.0f}".format(_elem.number))

            for _node in _elem.node:
                UFOmod.append(" {:8.0f}".format(_node.number))

            UFOmod.append(" {:8.0f}".format(_elem.material[0].number))
            UFOmod.append(" {:8.0f}".format(_elem.section[0].number))

            if _elem.offsets:
                for _offset in _elem.offsets:
                    UFOmod.append(" {:7.0f}".format(_offset.number))
            UFOmod.append("\n")
        else:
            print('****  membrane with 3 nodes??')
            UFOmod.append(" TRISHELL {:14.0f}".format(_elem.number))

        _iter += 1
    #UFOmod.append("\n")    
    #
    return UFOmod, check_out
#
def print_spring(elements):
    """
    """
    UFOmod = []
    check_out = []    
    _springs = [_elem for _elem in elements.values() 
                if _elem.type.element == 'spring']
    #
    if _springs:
        UFOmod.append("'\n")
        UFOmod.append("'{:} Spring elements\n".format(67 * "-"))
        UFOmod.append("'\n")        
        #
        for _elem in _springs:
            if _elem.type.description == 'General Spring / Shim Element' and _elem.material[0].type == "spring":
                UFOmod.append("'  2 node spring element\n")
                UFOmod.append("'            Elem ID      np1      np2   material   geom    lcoor    ecc1    ecc2\n")
                UFOmod.append("'\n")
                UFOmod.append((" BEAM {:14.0f}").format(_elem.number))
    
                for _node in _elem.node:
                    UFOmod.append(" {:8.0f}".format(_node.number))
                UFOmod.append(" {:8.0f} {:8.0f}".format(_elem.material[0].number, 1))
                # UFOmod.append(" {:8.0f}".format(_elem.section[0].number)) fix this svo
                # UFOmod.append(" {:8.0f}".format(_elem.guidepoint[0].number))
    
                if _elem.offsets:
                    for _offset in _elem.offsets:
                        UFOmod.append(" {:7.0f}".format(_offset.number))
    
                UFOmod.append("\n")
                if _elem.length_node2node == 0:
                    UFOmod.append(" ElmTrans   GlobNode {:8.0f}     Elem  {:16.0f}\n"
                    #UFOmod.append(" ElmTrans   MainBeam {:8.0f}  ZeroSpri  {:16.0f}\n"
                                  .format(_elem.node[0].number, _elem.number))
    
            elif _elem.type.description == 'Spring to Ground':
                UFOmod.append("'            Elem ID      np1      mat    lcoor   ecc1\n")
                UFOmod.append(" SPRNG2GR {:10.0f} {:8.0f} {:8.0f}\n"
                              .format(_elem.number, _elem.node[0].number, _elem.material[0].number))
                UFOmod.append("\n")
    #
    return UFOmod, check_out
    
#
def print_blind2p(node_slave, _elem_master, _material):
    """
    """
    #
    _stiffness = _material.stiffness[0]
    # get linear dependency dof from material
    _stiffness = [_stiffness[0][0], _stiffness[1][1], 
                  _stiffness[2][2], _stiffness[3][3], 
                  _stiffness[4][4], _stiffness[5][5]]
    _dof = [1 if _item != 0  else 0 for _item in _stiffness ]
    #
    UFOmod = []
    UFOmod.append(" BLINDP2  {:10.0f} {:8.0f}"
                  .format(node_slave.number,
                          _elem_master.number))
    #UFOmod.append("    0     1     1    0    0    0\n")
    UFOmod.append("    {:}     {:}     {:}     {:}     {:}     {:}\n".format(*_dof))
    #
    return UFOmod
#
def print_shim(elements):
    """
    """
    UFOmod = []
    check_out = []
    _shim_off = []
    _node_rep = []
    _shims = [_elem for _elem in elements.values() 
              if _elem.material[0].type == "shim"]
    #
    if _shims:
        UFOmod.append("'\n")
        UFOmod.append("'{:} Shim elements\n".format(69 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("' Master element local system\n")
        UFOmod.append("'\n")
        UFOmod.append("' Overlapping Beams (i.e. pile-in-leg, conductor's guide)\n")
        UFOmod.append("'\n")
        UFOmod.append("'               insl      iem   ix    iy    iz  irx  iry  irz\n")
        UFOmod.append("'\n")
        _stubs = []
        #
        for _elem in _shims:
            node_slave = _elem.node[1]
            node_master = _elem.node[0]
            _material = _elem.material[0]
            _elem_slave = elements[node_slave.elements[0]]
            _elem_master = elements[node_master.elements[0]]
            # get angle between the two beam elements (radians)
            _angle = usfos_process.vector_angle(_elem_master, _elem_slave)
            # if angle < than 1 degree, guessing overlapping beam
            if _angle < 0.0175 or math.isclose(_angle, math.pi, rel_tol=0.001):
                
                if _elem_master.node[0].boundary or _elem_master.node[1].boundary:
                    print('    *** warning element {:} has a boundary --> BLINDP2 may not work'
                          .format(_elem_master.name))
                    UFOmod.append("' Node with a boundary --> BLINDP2 may not work\n")
                    UFOmod.append("'")
                
                if node_slave.number in _node_rep:
                    UFOmod.append("' slave node repeated --> BLINDP2 canceled\n")
                    UFOmod.append("'")
                UFOmod.extend(print_blind2p(node_slave, _elem_master, _material))
                _node_rep.append(node_slave.number)
            else:
                _stubs.append([node_slave, _elem_master])
        #
        #
        UFOmod.append("'\n")
        UFOmod.append("' Stub-Riser beams\n")
        UFOmod.append("'\n")
        UFOmod.append("'               insl      iem   ix    iy    iz   irx   iry   irz\n")
        UFOmod.append("'\n")
        for _stub in _stubs:
            node_slave = _stub[0]
            _elem_master = _stub[1]
            
            if _elem_master.node[0].boundary or _elem_master.node[1].boundary:
                print('    *** warning element {:} has a boundary --> BLINDP2 may not work'
                      .format(_elem_master.name))
                UFOmod.append("' Node with a boundary --> BLINDP2 may not work\n")
                UFOmod.append("'")
            if node_slave.number in _node_rep:
                UFOmod.append("' slave node repeated --> BLINDP2 canceled\n")
                UFOmod.append("'")
            
            UFOmod.extend(print_blind2p(node_slave, _elem_master, _material))
            _node_rep.append(node_slave.number)            
    #
    return UFOmod, check_out, _shim_off
#
#
def print_elements(elements):
    """
    """
    header = 'ELEMENT DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")

    check_out = []
    check_out.append("'\n")
    check_out.append("'{:} Printing Elements\n".format(64 * "-"))
    _guide_off = []
    _shim_off = []
    
    # beam element
    _data, check_data, guide_data = print_beam(elements)
    UFOmod.extend(_data)
    check_out.extend(check_data)
    _guide_off.extend(guide_data)
    
    # Truss elements
    _data, check_data, guide_data = print_truss(elements)
    UFOmod.extend(_data)
    check_out.extend(check_data)
    _guide_off.extend(guide_data)

    # 4 node shell elements
    _data, check_data = print_shell4(elements)
    UFOmod.extend(_data)
    check_out.extend(check_data)     

    # 3 node shell elements
    _data, check_data = print_shell3(elements)
    UFOmod.extend(_data)
    check_out.extend(check_data)     

    # 4 node membrane elements
    _data, check_data = print_membrane4(elements)
    UFOmod.extend(_data)
    check_out.extend(check_data)    

    # spring elements
    _data, check_data = print_spring(elements)
    UFOmod.extend(_data)
    check_out.extend(check_data)     

    # shim elements
    _data, check_data, _shim_off = print_shim(elements)
    UFOmod.extend(_data)
    check_out.extend(check_data)
    #
    #
    if _guide_off:
        check_out.append("'\n")
        check_out.append("'{:} ** Warning Elements with no guidepoint\n"
                         .format(43 * " "))
        check_out.extend(process.common.printing.print_column_num(_guide_off))

    if _shim_off:
        check_out.append("'\n")
        check_out.append("'{:} ** Warning Shim elements ignored\n"
                         .format(49 * " "))
        check_out.extend(process.common.printing.print_column_num(_shim_off))
    #
    return UFOmod, check_out
#
#
#
def print_vectors(vectors):
    """
    """
    header = 'UNIT VECTOR DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    
    _vector_beam = [key for key, _vector in vectors.items()
                    if 'beam' in _vector.type.lower()]
    for key in _vector_beam:
        _vector = vectors[key]
        #if _vector.type == 'beam':
        UFOmod.append((" UNITVEC  {:14.0f}").format(_vector.number))
        UFOmod.append(" {: 14.5f} {: 14.5f} {: 14.5f}\n"
                      .format(_vector.cos[0], _vector.cos[1], _vector.cos[2]))
    #
    #
    _vector_trans = [key for key, _vector in vectors.items()
                     if 'matrix' in _vector.type.lower()]
    if _vector_trans:
        UFOmod.append("'\n") 
        UFOmod.append("' Nodal transformation matrix\n")
        UFOmod.append("'\n")        
        for key in _vector_trans:
            _vector = vectors[key]
            for _node in _vector.nodes:
                UFOmod.append((" NODTRANS {:14.0f}").format(_node.number))
                #for _cos in _vector.cos:
                UFOmod.append(" {: 14.5f} {: 14.5f} {: 14.5f}\n"
                              .format(_vector.cos[0], _vector.cos[1], _vector.cos[2]))
                UFOmod.append("{:}{: 14.5f} {: 14.5f} {: 14.5f}\n"
                              .format(25*" ", _vector.cos[3], _vector.cos[4], _vector.cos[5]))
                UFOmod.append("{:}{: 14.5f} {: 14.5f} {: 14.5f}\n"
                              .format(25*" ", _vector.cos[6], _vector.cos[7], _vector.cos[8]))
                UFOmod.append("'\n")
    #
    return UFOmod
#
def print_eccentricities(eccen, factor=1):
    """
    """
    header = 'ECCENTRICITY DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    for _ecc in eccen.values():
        UFOmod.append(" ECCENT    {:1.5E}".format(_ecc.number))
        UFOmod.append(" {: 1.5E} {: 1.5E} {: 1.5E}\n"
                      .format(_ecc.eccentricity[0] * factor,
                              _ecc.eccentricity[1] * factor,
                              _ecc.eccentricity[2] * factor))
    return UFOmod
#
def print_hinges(elements, hinges=False):
    """
    """
    header = 'INTERNAL HINGES DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    for _elem in elements.values():
        if _elem.releases:
            if not hinges and _elem.type.element != 'truss':
                UFOmod.append("' ")
            UFOmod.append(" BEAMHING")
            for _rel in _elem.releases:
                UFOmod.append("    ")
                if _rel is None:
                    UFOmod.append(" 1 1 1 1 1 1")
                    continue
                
                for _fix in _rel.fix:
                    UFOmod.append((" {:1.0f}").format(_fix))
            #
            if 'truss' in _elem.type.element :
                UFOmod.append((" {:14.0f}    ! Truss element\n").format(_elem.number))
            else:
                UFOmod.append((" {:14.0f}\n").format(_elem.number))
    #
    return UFOmod
#
#
# Groups
#
def print_groups(sets, elements):
    """
    """
    header = 'GROUP DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    #
    set_list = [key for key in sorted(sets, key=lambda name: sets[name].number)]
    #
    for _name in set_list:
        _set = sets[_name]
        try:
            _NoParts = len(_set.items)
            _test = 1.0 / _NoParts
        except ZeroDivisionError:
            continue
        # in case group is empty
        if _set.items:
            _temp = ["'\n"]
            _temp.append(" Name  Group {:5.0f} {:35s}\n".
                         format(_set.number, _set.name[:35]))
            _temp.append(" GroupDef    {:5.0f} Elem\n".
                         format(_set.number))
            _items = print_member_list(_set.items, elements)
            if not _items:
                continue
            #
            _temp.extend(_items)
            _temp.append("\n")
            UFOmod.extend(_temp)
    #
    return UFOmod

#
def print_member_list(items, elements):
    """
    """
    UFOmod = []
    _noMemb = len(items)
    _noSlot = 0
    i = 0
    for _memb in items:
        try:
            _elem = elements[_memb]
        except KeyError:
            continue

        _noSlot += 1
        i += 1
        UFOmod.append(" {:11.0f}".format(_elem.number))
        if _noSlot == 6 or i == _noMemb:
            UFOmod.append(" \n")
            _noSlot = 0
    #
    return UFOmod
#
#
#