# 
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import re
import sys

# package imports
import fem2ufo.process.control as process


#
#
def get_section_shape(sections, shape):
    """
    """
    _items = [_section for _section in sections.values()
              if re.search(shape, _section.shape, re.IGNORECASE)]
    return _items
#
#
def print_data(sections, factor=1, header=None):
    """
    """
    if not header:
        header = 'CROSS SECTION DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    #
    # Tubular cross section
    _items = get_section_shape(sections, shape=r"\btub(ular)?|pipe\b")
    if _items:
        UFOmod.append("'{:} Tubular cross section\n".format(61 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("'{:}Geom ID {:}Do {:}Thick    (Shear_y    Shear_z      Diam2 )\n"
                      .format(12*" ", 8*" ", 4*" "))
        UFOmod.append("'\n")
        
        for _section in _items:
            UFOmod.append((" PIPE {:14.0f}").format(_section.number))
            UFOmod.append((" {:10.6f}").format(_section.properties.D * factor))
            UFOmod.append((" {:10.6f}").format(_section.properties.Tw * factor))
            UFOmod.append((" {:10.3f}").format(_section.SFH))
            UFOmod.append((" {:10.3f}").format(_section.SFV))
            # cone
            UFOmod.append((" {:}").format(14*" "))
            UFOmod.append(("  ' {:}").format(_section.name))
            UFOmod.append("\n")
            
            if 'spoke_' in _section.name:
                UFOmod.append(" SURFPIPE {:10.0f} {:10.6f} {:10.6f} {:}! Spoke section\n"
                              .format(_section.number, _section.properties.D * factor * 0.01,
                                      _section.properties.Tw * factor * 0.01, 25 * " "))
            #
    #
    # I/H profile
    _items = get_section_shape(sections, shape=r"\b((u)?i(\s*beam|section)?)\b")
    if _items:
        UFOmod.append("'\n")
        UFOmod.append("'{:} I/H profile\n".format(71 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("'{:}Geom ID       H     T-web    W-top    T-top    W-bot    T-bot  Sh_y  Sh_z\n"
                      .format(12*" "))
        UFOmod.append("'\n")
        
        for _section in _items:
            UFOmod.append((" IHPROFIL {:10.0f}").format(_section.number))
            
            UFOmod.append((" {:8.4f}").format(_section.properties.D * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tw * factor))
        
            UFOmod.append((" {:8.4f}").format(_section.properties.Bft * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tft * factor))
        
            UFOmod.append((" {:8.4f}").format(_section.properties.Bfb * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tfb * factor))
        
            UFOmod.append((" {:5.2f}").format(_section.SFH))
            UFOmod.append((" {:5.2f}").format(_section.SFV))
            
            UFOmod.append(("  ' {:}").format(_section.name))
            UFOmod.append("\n")
    #
    # T profile
    _items = get_section_shape(sections, shape=r"\b(t(ee)?(\s*section)?)\b")
    if _items:
        UFOmod.append("'\n")
        UFOmod.append("'{:} T profile\n".format(73 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("'{:}Geom ID       H     T-web    W-top    T-top    W-bot    T-bot  Sh_y  Sh_z\n"
                      .format(12*" "))
        UFOmod.append("'\n")
        
        for _section in _items:
            UFOmod.append((" IHPROFIL {:10.0f} ! TEE/n").format(_section.number))

            UFOmod.append((" IHPROFIL {:10.0f}").format(_section.number))

            UFOmod.append((" {:8.4f}").format(_section.properties.D * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tw * factor))

            UFOmod.append((" {:8.4f}").format(_section.properties.Bft * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tft * factor))

            UFOmod.append((" {:8.4f}").format(_section.properties.Tw * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tw * factor))

            UFOmod.append((" {:5.2f}").format(_section.SFH))
            UFOmod.append((" {:5.2f}").format(_section.SFV))
            
            UFOmod.append(("  ' {:}").format(_section.name))
            UFOmod.append("\n")
    #
    # Rectangulat hollow section
    _items = get_section_shape(sections, shape=r"\bbox\b")
    if _items:
        UFOmod.append("'\n")
        UFOmod.append("'\n")
        UFOmod.append("'{:} Rectangulat hollow section\n".format(56 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("'{:}Geom ID        H    T-sid    T-bot    T-top   Width   Sh_y  Sh_z\n"
                      .format(11*" "))
        UFOmod.append("'\n")
        
        for _section in _items:
            UFOmod.append((" BOX {:14.0f}").format(_section.number))

            UFOmod.append((" {:8.4f}").format(_section.properties.D * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tw * factor))

            UFOmod.append((" {:8.4f}").format(_section.properties.Tfb * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tft * factor))

            _averWidth = (_section.properties.Bft + _section.properties.Bfb * factor) / 2.0
            UFOmod.append((" {:8.4f}").format(_averWidth * factor))
            
            UFOmod.append((" {:5.2f}").format(_section.SFH))
            UFOmod.append((" {:5.2f}").format(_section.SFV))            
            
            UFOmod.append(("  ' {:}").format(_section.name))
            UFOmod.append("\n")
    #
    # L open angle section
    _items = get_section_shape(sections, shape=r"\b(angle|l)(\s*beam|section)?\b")
    if _items:
        UFOmod.append("'\n")
        UFOmod.append("'{:} L open angle section\n".format(62 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("'{:}Geom ID        H   T_side    Width   T_bott   I_symm (1: L, 0: opposite L)\n"
                      .format(16*" "))
        UFOmod.append("'\n")
        
        for _section in _items:
            UFOmod.append((" LSECTION {:14.0f}").format(_section.number))
            UFOmod.append((" {:8.4f}").format(_section.properties.D * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tw * factor))
        
            UFOmod.append((" {:8.4f}").format(_section.properties.Bft * factor))
            UFOmod.append((" {:8.4f}").format(_section.properties.Tft * factor))
            
            UFOmod.append((" {:8.0f}").format(0))
            
            UFOmod.append(("  ' {:}").format(_section.name))
            UFOmod.append("\n")
    #
    # Channel section
    _items = get_section_shape(sections, shape=r"\bchannel\b")
    if _items:
        UFOmod.append("'\n")
        UFOmod.append("'{:} Channel section\n".format(67 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("'{:}GeomID        Area          It          Iy          Iz\n"
                      .format(12*" "))
        UFOmod.append("'{:}Wpx         Wpy         Wpz         Shy\n".format(26*" "))
        UFOmod.append("'{:}Shz\n".format(26*" "))
        UFOmod.append("'\n")
        
        for _section in _items:
            #sections[_section.name].properties()
            UFOmod.append(("' CHANNEL {:14.0f} [no yet implemented] \n").format(_section.number))

            UFOmod.append((" GENBEAM {:10.0f}").format(_section.number))

            UFOmod.append((" {:1.5E}").format(_section.properties.Area * factor ** 2))

            UFOmod.append((" {:1.5E}").format(_section.properties.J * factor ** 4))
            UFOmod.append((" {:1.5E}").format(_section.properties.Iip * factor ** 4))
            UFOmod.append((" {:1.5E}\n").format(_section.properties.Iop * factor ** 4))

            #UFOmod.append(("{:} {:1.5E}").format(19 * ' ', _section.properties.ZeJ * factor ** 3))
            try:
                1.0/float(_section.properties.ZeJ)
                UFOmod.append(("{:} {:1.5E}").format(19 * ' ', _section.properties.ZeJ * factor**3))
            except ZeroDivisionError:
                UFOmod.append(("{:} {:1.5E}").format(19 * ' ', _section.properties.J * factor**4))
            # fixme : value lost and set to zero?
            try:
                1.0/float(_section.properties.Zeip)
                UFOmod.append((" {:1.5E}").format(_section.properties.Zeip * factor ** 3))
            except ZeroDivisionError:
                UFOmod.append((" {:1.5E}").format(_section.properties.Iip * factor ** 4))
            #
            try:
                1.0/float(_section.properties.Zeop)
                UFOmod.append((" {:1.5E}").format(_section.properties.Zeop * factor ** 3))
            except ZeroDivisionError:
                UFOmod.append((" {:1.5E}").format(_section.properties.Iop * factor ** 4))            
            #
            UFOmod.append((" {:1.5E}\n").format(_section.properties.shearAreaV * factor ** 2))

            UFOmod.append(("{:} {:1.5E}").format(19 * ' ', _section.properties.shearAreaH * factor ** 2))
            
            UFOmod.append(("  ' {:}").format(_section.name))
            UFOmod.append("\n")
    #
    # Bar square section
    _items = get_section_shape(sections, shape=r"\b((rectangular|square)(\s*bar)?)\b")
    if _items:
        UFOmod.append("'\n")
        UFOmod.append("'{:} Bar square section\n".format(67 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("'{:}GeomID        Area          It          Iy          Iz\n"
                      .format(12*" "))
        UFOmod.append("'{:}Wpx         Wpy         Wpz         Shy\n".format(26*" "))
        UFOmod.append("'{:}Shz\n".format(26*" "))
        UFOmod.append("'\n")
        
        for _section in _items:
            UFOmod.append(("' BAR_SQUARE {:14.0f} [no yet implemented] \n").format(_section.number))

            UFOmod.append((" GENBEAM {:10.0f}").format(_section.number))

            UFOmod.append((" {:1.5E}").format(_section.properties.Area * factor ** 2))

            UFOmod.append((" {:1.5E}").format(_section.properties.J * factor ** 4))
            UFOmod.append((" {:1.5E}").format(_section.properties.Iip * factor ** 4))
            UFOmod.append((" {:1.5E}\n").format(_section.properties.Iop * factor ** 4))

            UFOmod.append(("{:} {:1.5E}").format(19 * ' ', _section.properties.ZeJ * factor ** 3))
            UFOmod.append((" {:1.5E}").format(_section.properties.Zeip * factor ** 3))
            UFOmod.append((" {:1.5E}").format(_section.properties.Zeop * factor ** 3))
            UFOmod.append((" {:1.5E}\n").format(_section.properties.shearAreaV * factor ** 2))

            UFOmod.append(("{:} {:1.5E}").format(19 * ' ', _section.properties.shearAreaH * factor ** 2))
            
            UFOmod.append(("  ' {:}").format(_section.name))
            UFOmod.append("\n")
    #
    # General cross section
    _items = get_section_shape(sections, shape=r"\b(general(\s*section)?)\b")
    if _items:
        UFOmod.append("'\n")
        UFOmod.append("'{:} General cross section\n".format(61 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("'{:}GeomID        Area          It          Iy          Iz\n"
                      .format(12*" "))
        UFOmod.append("'{:}Wpx         Wpy         Wpz         Shy\n".format(26*" "))
        UFOmod.append("'{:}Shz\n".format(26*" "))
        UFOmod.append("'\n")
        
        for _section in _items:
            UFOmod.append((" GENBEAM {:10.0f}").format(_section.number))
            UFOmod.append((" {:1.5E}").format(_section.properties.Area * factor**2))
            UFOmod.append((" {:1.5E}").format(_section.properties.J * factor**4))
            UFOmod.append((" {:1.5E}").format(_section.properties.Iip * factor**4))
            UFOmod.append((" {:1.5E}\n").format(_section.properties.Iop * factor**4))
            #
            try:
                1.0/float(_section.properties.ZeJ)
                UFOmod.append(("{:} {:1.5E}").format(19 * ' ', _section.properties.ZeJ * factor**3))
            except ZeroDivisionError:
                #raise Exception('    ** error : Section modulus missing for general section: {:}'
                #                .format(_section.name))
                UFOmod.append(("{:} {:1.5E}").format(19 * ' ', _section.properties.J * factor**4))
            #
            try:
                1.0/float(_section.properties.Zeip)
                UFOmod.append((" {:1.5E}").format(_section.properties.Zeip * factor**3))
            except ZeroDivisionError:
                #raise Exception('    ** error : Sectional modulus about y-axis missing for general section: {:}'
                #                .format(_section.name))                
                UFOmod.append((" {:1.5E}").format(_section.properties.Iip * factor**4))
            #
            try:
                1.0/float(_section.properties.Zeop)
                UFOmod.append((" {:1.5E}").format(_section.properties.Zeop * factor**3))
            except:
                #raise Exception('    ** error : Sectional modulus about z-axis missing for general section: {:}'
                #                .format(_section.name))
                UFOmod.append((" {:1.5E}").format(_section.properties.Iop * factor**4))
            #
            #
            try:
                1.0/float(_section.properties.shearAreaV )
                UFOmod.append((" {:1.5E}\n").format(_section.properties.shearAreaV * factor**2))
            except:
                raise Exception('    ** error : Shear area in direction of y-axi missing for general section: {:}'
                                .format(_section.name))
                #UFOmod.append((" {:1.5E}\n").format(1.0))
            #
            try:
                1.0 / float(_section.properties.shearAreaH )
                UFOmod.append(("{:} {:1.5E}").format(19 * ' ', _section.properties.shearAreaH * factor**2))
            except ZeroDivisionError:
                raise Exception('    ** error : Shear area in direction of z-axi missing for general section: {:}'
                                .format(_section.name))               
                #UFOmod.append(("{:} {:1.5E}").format(19 * ' ', 1.0))
            #
            UFOmod.append(("  ' {:}").format(_section.name))
            UFOmod.append("\n")
    #
    # Plate thickness
    _items = get_section_shape(sections, shape=r"\b(plate|shell|membrane)\b")
    if _items:
        UFOmod.append("'\n")
        UFOmod.append("'{:} Plate thickness\n".format(67 * "-"))
        UFOmod.append("'\n")
        UFOmod.append("'{:}Geom ID   Thickness\n".format(12*" "))
        UFOmod.append("'\n")
        
        for _section in _items:
            UFOmod.append((" PLTHICK  {:10.0f}").format(_section.number))
            UFOmod.append(("  {:10.6f}").format(_section.properties.D * factor))
            
            UFOmod.append(("  ' {:}").format(_section.name))
            UFOmod.append("\n")
    #
    return UFOmod
#