# 
# Copyright (c) 2009-2018 fem2ufo
# 


# Python stdlib imports
import math

# package imports
import fem2ufo.process.control as process

# hydro
#
def get_wave_type(lineIn):
    """
    Define wave theory
    """
    _key = {"1": r"airy_linear|airy_extrapolate",
            "1.1": r"airy_extretched",
            "2": r"stokes_5",
            "3": r"user(\s*|_)defined",
            "4": r"stream",
            "buoyancy": r"buoyancy",
            "calm_sea": r"calm_sea"}

    keyWord = process.common.find_keyword(lineIn, _key)
    return keyWord
#
#
def wave_length(T, d, grav=9.80665):
    """
    Claculate aproximate wave leghth
    
    T = Wave period
    d = Water depth
    
    alpha = Wave length
    """
    #
    Tlim = math.sqrt(2.0 * d / (grav / (2 * math.pi)))
    #
    if T < Tlim:
        alpha = grav * T ** 2 / (2 * math.pi)
    else:
        alpha = 2.0 * d * (2.0 * T / Tlim - 1.0)
    #
    # new formula
    #
    sigma = 2 * math.pi / T
    L0 = grav * T**2 / (2 * math.pi)
    alpha = L0*(math.tanh((sigma**2 * d / grav)**(3/4.)))**(2/3.)
    wave_number = 2 * math.pi / alpha
    #
    return round(alpha)
#
# Metocean
#
def print_header_metocean():
    """
    Print metocean header
    """
    header = 'METOCEAN DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    return UFOmod
#
def printCoeffs(_case, _CD, _cdcm, _noCoeff,
                _wave=False):
    """
    _case : drag, mass
    _CD[0] longitudinal
    _CD[1] transverse
    _CD[2] transverse z
    """
    UFOmod = []
    _coeffNo = _noCoeff
    #
    #  transversal coefficient
    if _CD[1] == _CD[2]:
        UFOmod.append("'\n")
        UFOmod.append("'              ID           typ    alfa           Cd\n")
        UFOmod.append("W_COEFFS {:8.0f}          {:}       0   {:1.4e}\n".
                      format(_noCoeff, _case, _CD[2]))
        #
    else:
        UFOmod.append("'\n")
        UFOmod.append("'              ID           typ    alfa           Cd\n")
        UFOmod.append("W_COEFFS {:8.0f}          {:}    -180   {:1.4e}\n".
                      format(_noCoeff, _case, _CD[2]))
        UFOmod.append("{:}      -90   {:1.4e}\n".format(30 * " ", _CD[1]))
        UFOmod.append("{:}        0   {:1.4e}\n".format(30 * " ", _CD[2]))
        UFOmod.append("{:}       90   {:1.4e}\n".format(30 * " ", _CD[1]))
        UFOmod.append("{:}      180   {:1.4e}\n".format(30 * " ", _CD[2]))
    #
    # longitudinal coefficient
    if _CD[0] != 0:
        _noCoeff += 1
        UFOmod.append("'\n")
        UFOmod.append("'              ID           typ    alfa           Cd\n")
        UFOmod.append("W_COEFFS {:8.0f}          Tang       0   {:1.4e}\n".
                      format(_noCoeff, _CD[0]))
    #
    # combine coefficients
    _coeffNo += 1
    # UFOmod.append("'              ID          Type     IDs\n")
    UFOmod.append("W_COEFFS {:8.0f}       Combine {:7.0f}               ! {:} only\n"
                  .format(_coeffNo, _noCoeff, _case))
    #
    # assing coefficent to group
    for _key, _item in _cdcm.sets.items():
        UFOmod.append("ELMCOEFF {:8.0f}         Group {:7.0f}               ! {:}\n"
                      .format(_coeffNo, _item, _key))
        #
        # if wave, swich on special card
        if _wave:
            UFOmod.append("HYDROPAR DirDepSW             1   Group     {:8.0f}  ! {:}\n"
                          .format(_item, _key))
    UFOmod.append("'\n")
    #
    _coeffNo += 1
    #
    return _coeffNo, UFOmod
    #
#
#
def print_wave_current(metocean, mudline, surface, factors):
    """
    Print wave and current cards is usfos format
    """
    UFOmod = []
    # m
    _lenFactor = factors[0]
    # fix this m/s
    _velFactor = factors[0]
    # 
    UFOmod.append("'{:} Wave and Current\n".format(66 * "-"))
    UFOmod.append("'\n")
    UFOmod.append("'\n")
    UFOmod.append("'    WAVEDATA\n")
    UFOmod.append("'    With this record, the user may specify an irregular wave to be applied \n")
    UFOmod.append("'    to the structure as hydrodynamic forces\n")
    UFOmod.append("'\n")
    UFOmod.append("'    CURRENT\n")
    UFOmod.append("'    With this record, the user may specify a current to be applied to the\n")
    UFOmod.append("'    structure as hydrodynamical forces.\n")
    UFOmod.append("'\n")
    #
    # wave
    for key in sorted(metocean, key = lambda name: metocean[name].number):
        _metocean = metocean[key]
        if _metocean.wave:
            _wave = _metocean.wave
            try:
                _wave_name = int(_wave.name)
                _name = "'\n"
            except ValueError:
                _name = "' {:}\n".format(_wave.name)
                _wave_name = int(_wave.number)
            #
            Tw = _wave.period
            dw = abs(mudline) * _lenFactor
            if  _wave.theory != 'calm_sea':
                UFOmod.append(_name)
                _type = float(get_wave_type(_wave.theory))
                UFOmod.append("'           Lcase Type   Height    Period  Direction  Phase   SurfLevel  WaterDep  N\n")
                UFOmod.append("'                          (m)       (s)     (deg)    (deg)      (m)        (m)\n")
                UFOmod.append("WAVEDATA  {:7.0f} {:1.2f} {:1.3e} {:1.3e} {:1.3e} {:1.2e} {:1.3e}  {:1.3e}"
                              .format(_wave_name, _type,
                                      _wave.height * _lenFactor, Tw,
                                      _metocean.wave_direction, _wave.phase,
                                      surface * _lenFactor, dw))
    
                UFOmod.append(" '6\n")
                WaveLength = wave_length(Tw, dw)
                UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", -2.0 * WaveLength, 0))
                UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", -1.5 * WaveLength, 0))
                UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", -1.0 * WaveLength, 1))
                UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", -0.5 * WaveLength, 1))
                UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", 0.0 * WaveLength, 0))
                UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", 0.5 * WaveLength, 0))
                UFOmod.append("'\n")
            else:
                UFOmod.append(_name)
                _type = 1
                UFOmod.append("'           Lcase Type   Height    Period  Direction  Phase   SurfLevel  WaterDep  N\n")
                UFOmod.append("'                          (m)       (s)     (deg)    (deg)      (m)        (m)\n")
                UFOmod.append("WAVEDATA  {:7.0f} {:1.2f} {:1.3e} {:1.3e} {:1.3e} {:1.2e} {:1.3e}  {:1.3e}"
                                          .format(_wave_name, _type, 0.01, 4, 0, 0.0, surface * _lenFactor, dw))
                UFOmod.append("\n")
        # current
        if _metocean.current:
                UFOmod.append(_name)
                UFOmod.append("'           Lcase       Speed   Direction SurfLevel WaterDept      [Profile]\n")
                UFOmod.append("'                       (m/s)     (deg)     (m)        (m)    Zcoord(m)   Factor\n")
                UFOmod.append("CURRENT   {:7.0f}     {:1.3e} {:1.3e} {:1.3e} {:1.3e}"
                              .format(_wave_name, _metocean.current.velocity * _velFactor,
                                      _metocean.current_direction,
                                      surface * _lenFactor, 
                                      abs(mudline) * _lenFactor))
                i = 0
                _currentProfile = reversed(_metocean.current.profile)
                for _item in _currentProfile:
                    i += 1
                    if i > 1: UFOmod.append("{:}".format(61 * " "))
                    UFOmod.append(
                        "{: 1.3e} {: 1.3e}\n".format((_item[0] - abs(mudline)) * _lenFactor, _item[1]))
    
        UFOmod.append("'\n")
    #
    UFOmod.append("'\n")
    return UFOmod
#
#
def print_wind(metocean, air_density, factors):
    """
    Print wind parameters in usfos format
    """
    #  Units
    #_lenFactor = factors[0]
    # fix this m/s
    _velFactor = factors[0]
    _density = factors[1] / factors[0] ** 3
    #
    header = 'WIND DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    UFOmod.append("'   With this record, the user may specify a windfield to be applied to the\n")
    UFOmod.append("'   structure as aerodynamical forces.\n")
    UFOmod.append("'\n")
    
    for key in sorted(metocean, key = lambda name: metocean[name].number):
        _metocean = metocean[key]
        
        if _metocean.wind:
            _wind = _metocean.wind
            try:
                _wind_name = int(_wind.name)
                _name = "'\n"
            except ValueError:
                _name = "' {:}\n".format(_wind.name)
                _wind_name = int(_wind.number)            
            #
            UFOmod.append(_name)
            _ux = _wind.velocity * _velFactor * math.cos(math.radians(_metocean.wind_direction))
            _uy = _wind.velocity * _velFactor * math.sin(math.radians(_metocean.wind_direction))
            UFOmod.append(
                "'   ux = {:1.3e}*COS({:}*PI/180),".format(_wind.velocity * _velFactor, 
                                                           _metocean.wind_direction))
            UFOmod.append(
                "    uy = {:1.3e}*SIN({:}*PI/180)\n".format(_wind.velocity * _velFactor, 
                                                            _metocean.wind_direction))
            UFOmod.append("'           Lcase   Type   ux (m/s)   uy (m/s)    uz   Z_0  Zbot Rho(kg/m3)    Power\n")
            UFOmod.append("WINDFIELD {:7.0f} Z_prof {: 1.3e} {: 1.3e} 0.000 0.000 0.000 {:1.3e} {:1.3e}\n"
                          .format(_wind_name, _ux, _uy, air_density * _density, _wind.power))
    #
    return UFOmod
#
#
def print_hydro_parameters(metocean, factors, _noCoeff=1):
    """
    Print hydrodynamic parametrs in usfos format
    """
    #  Units
    _lenFactor = factors[0]
    #
    UFOmod = []
    # Marine Growth
    UFOmod.append("'\n")
    UFOmod.append("'{:} Marine Growth\n".format(69 * "-"))
    UFOmod.append("'\n")
    # print marine growth z profile
    _no_prof = 0
    for _wgrowth in metocean.marine_growth.values():
        if _wgrowth.type == 'profile':
            _no_prof += 1
            
            if _no_prof == 1:
                UFOmod.append("'    With this record the user may define a marine growth depth profile to be\n")
                UFOmod.append("'    applied to the structural members.\n")
                UFOmod.append("'\n")
                UFOmod.append("'                   Z-coord (m)         Add Thick(m)\n")
                UFOmod.append("'\n")
            else:
                print('   ** Warning more than one marine growth profile')
                UFOmod.append("' ")
            #
            UFOmod.append("M_GROWTH  ")            
            i = 0
            for _prof in _wgrowth.profile:
                i += 1
                if i > 1: 
                    UFOmod.append("{:}".format(10 * " "))
                
                UFOmod.append("{:} {: 1.6e} {:} {:1.6e}\n"
                              .format(7 * ' ', _prof[0] * _lenFactor,
                                      7 * ' ', _prof[1] * _lenFactor))
    # print specified marine growth
    #
    for _wgrowth in metocean.marine_growth.values():
        if _wgrowth.type == 'specified':
            _no_prof += 1
            if _no_prof == 1:
                UFOmod.append("'\n")
                UFOmod.append("'    Marine Growth Parameters\n")
                UFOmod.append("'\n")
                UFOmod.append("'         KeyWord         Value    Type    {Id_List}\n")                
            #
            UFOmod.append("'\n")
            for _key, _item in _wgrowth.sets.items():
                UFOmod.append("HYDROPAR   MgrThick  {:1.6e}   Group{:}"
                              .format(_wgrowth.profile[0][1] * _lenFactor,
                                      5*" "))
                UFOmod.append("{:8.0f} ! {:}\n"
                              .format(_item, _key))
                #
                # FIXME: density and roughness need to be fixed
                # density
                #UFOmod.append("HYDROPAR   MgrDens   {:1.6e}   Group{:}"
                #              .format(_wgrowth.profile[0][1] * _lenFactor,
                #                      5*" "))
                #UFOmod.append("{:8.0f} ! {:}\n"
                #              .format(_item, _key))                
            

    # CD coefficient
    UFOmod.append("'\n")
    UFOmod.append("'{:} Hydrodynamic Coefficients\n".format(57 * "-"))
    UFOmod.append("'\n")
    #
    _no_prof = 0
    for _cdcm in metocean.cdcm.values():
        if _cdcm.type == 'profile':
            _no_prof += 1
            if _no_prof == 1:
                UFOmod.append("'    This record is used to define Drag and Mass Coefficient depth profile.\n")
                UFOmod.append("'    Between the tabulated values, the Cd is interpolated.\n")
                UFOmod.append("'    Values outside the table are extrapolated.\n")
                UFOmod.append("'\n")
                UFOmod.append("'                   Z-coord (m)         Cd\n")
                UFOmod.append("'\n")
            
            else:
                print('   ** Warning more than one Drag Coefficient profile')
                UFOmod.append("' ")
            #
            #
            UFOmod.append("Hydro_Cd  ")
            i = 0
            for _prof in _cdcm.profile['cd']:
                i += 1
                if i > 1: 
                    UFOmod.append("{:}".format(10 * " "))
                
                UFOmod.append("{:} {: 1.6e} {:} {:1.6e}\n"
                              .format(7 * ' ', _prof[0] * _lenFactor,
                                      7 * ' ', _prof[1]))
            #
            UFOmod.append("'\n")
            UFOmod.append("Hydro_Cm  ")
            i = 0
            for _prof in _cdcm.profile['cm']:
                i += 1
                if i > 1: 
                    UFOmod.append("{:}".format(10 * " "))
                
                UFOmod.append("{:} {: 1.6e} {:} {:1.6e}\n"
                              .format(7 * ' ', _prof[0] * _lenFactor,
                                      7 * ' ', _prof[1]))
    #
    UFOmod.append("'\n")
    UFOmod.append("'         KeyWord         Value    Type    {Id_List}\n")    
    # 
    for _cdcm in metocean.cdcm.values():
        # TODO : check if this section works
        if _cdcm.type == 'specified':
            # cd section
            try:
                _cd = _cdcm.coefficient[0:3]
                # print w coefficients
                if _cd[0] != 0 or _cd[1] != _cd[2]:
                    _case = 'Drag'
                    _noCoeff, _UFOmet = printCoeffs(_case, _cd, _cdcm,
                                                    _noCoeff, True)
                    UFOmod.extend(_UFOmet)
                #
                else:
                    #_value = _cd[1]
                    for _key, _item in _cdcm.sets.items():
                        UFOmod.append("'\n")
                        UFOmod.append("HYDROPAR       cd  {:1.6e}   Group     "
                                      .format(_cd[1]))
                        UFOmod.append("{:8.0f}  ! {:}\n"
                                      .format(_item, _key))
            #
            except KeyError:
                pass
            #
            #
            # cm section
            try:
                _cm = _cdcm.coefficient[3:]
            except KeyError:
                continue
            
            # print w coefficients
            if _cm[0] != 0 or _cm[1] != _cm[2]:
                _case = 'Moment'
                _noCoeff, _UFOmet = printCoeffs(_case, _cm, _cdcm,
                                                _noCoeff, True)
                UFOmod.extend(_UFOmet)
            #
            else:
                for _key, _item in _cdcm.sets.items():
                    UFOmod.append("HYDROPAR       cm  {:1.6e}   Group     "
                                  .format(_cm[1]))
                    UFOmod.append("{:8.0f}  ! {:}\n"
                                  .format(_item, _key))
        #
        #else:
        #    print(' fix here cdcm')
    #
    # Hydrodynamic diameter
    UFOmod.append("'\n")
    UFOmod.append("'{:} Hydrodynamic Diameter\n".format(61 * "-"))
    UFOmod.append("'\n")
    UFOmod.append("'         KeyWord  Diameter (m)    Type     {Id_List}\n")
    UFOmod.append("'\n")
    for _hdiam in metocean.hydro_diameter.values():
        for _key, _item in _hdiam.sets.items():
            UFOmod.append("HYDROPAR   HyDiam  {:1.6e}   Group{:}"
                          .format(_hdiam.coefficient * _lenFactor,
                                  5 * ' '))
            UFOmod.append("{:8.0f} ! {:}\n"
                          .format(_item, _key))
    # Buoyancy Diameter
    UFOmod.append("'\n")
    UFOmod.append("'{:} Buoyancy Diameter\n".format(65 * "-"))
    UFOmod.append("'\n")
    UFOmod.append("'         KeyWord         Value    Type     {Id_List}\n")
    for _bdiam in metocean.buoyancy.values():
        UFOmod.append("'\n")
        for _key, _item in _bdiam.sets.items():
            UFOmod.append("HYDROPAR   BuDiam  {:1.6e}   Group{:}"
                          .format(_bdiam.coefficient['nonflooded'] * _lenFactor,
                                  5 * ' '))
            UFOmod.append("{:8.0f} ! {:}\n"
                          .format(_item, _key))

            UFOmod.append("HYDROPAR  IntDiam  {:1.6e}   Group{:}"
                          .format(_bdiam.coefficient['steel'] * _lenFactor,
                                  5 * ' '))
            UFOmod.append("{:8.0f} ! {:}\n"
                          .format(_item, _key))
    # Flooded
    UFOmod.append("'\n")
    UFOmod.append("'{:} Flooded Members\n".format(67 * "-"))
    UFOmod.append("'\n")
    UFOmod.append("'        ListType     {Id_List}\n")
    for _flood in metocean.flooded.values():
        UFOmod.append("'\n")
        UFOmod.append("FLOODED     Group      ")
        UFOmod.append("{:8.0f} {:} ! {:}\n"
                      .format(_flood.number, 21 * ' ', _flood.name))
    # Non hydrodynamic
    UFOmod.append("'\n")
    UFOmod.append("'{:} Omit hydrodynamics\n".format(64 * "-"))
    UFOmod.append("'\n")
    UFOmod.append("'        ListType     {Id_List}\n")
    #
    for _key, _item in metocean.non_hydro.items():
        UFOmod.append("'\n")
        UFOmod.append("NONHYDRO    Group           {:} {:}   ! {:}\n"
                      .format(_item.number, 21 * ' ', _key))
    #
    #
    #
    # print('ok')
    return _noCoeff, UFOmod
    #
#
def print_wind_coefficients(metocean, _noCoeff):
    """
    Print wind coefficients in usfos format
    """
    _coeffNo = _noCoeff
    #
    header = 'WIND DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    UFOmod.append("'\n")
    UFOmod.append("'{:} Aerodynamic Coefficients\n".format(58 * "-"))
    
    for _cdcm in metocean.air_drag.values():
        if _cdcm.type == 'specified':
            # cd section
            _cd = _cdcm.coefficient[0:3]
            _case = 'Drag'
            _noCoeff, _UFOmet = printCoeffs(_case, _cd, _cdcm,
                                            _noCoeff)
            UFOmod.extend(_UFOmet)
    #
    return _coeffNo, UFOmod
    #
#