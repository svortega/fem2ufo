# 
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import math

# Python stdlib imports
import fem2ufo.process.control as process
#import fem2ufo.postprocess.usfos.operation.formatting as formatting


#
def approx_equal1(a, b, rel_tol):
    print(abs(a-b), max(abs(a), abs(b)) * rel_tol)
    #diff = abs(a-b) <= max(abs(a), abs(b)) * tol
    return abs(a-b) <= max(abs(a), abs(b)) * rel_tol
#
def approx_equal2(a, b, rel_tol):
    print(abs(a-b), (abs(a)+abs(b))/2 * rel_tol)
    diff = abs(a-b) <= (abs(a)+abs(b))/2 * rel_tol
    return diff
#
# get approx equal fuction
try:
    approx_equal = math.isclose
except AttributeError:
    approx_equal = approx_equal1
#
#
def print_iter(_item, factor=1, _ini=0, _noRows=0):
    UFOmod = []
    try:
        _NoParts = len(_item)
        _try = 1.0 / _NoParts

        for x in range(_ini, _NoParts):

            if _noRows == 5 and x <= _NoParts:
                UFOmod.append("\n")
                UFOmod.append("{:}".format(21 * " "))
                _noRows = 0

            UFOmod.append(" {: 1.5E}".format(_item[x] * factor))
            _noRows += 1
    except:
        pass

    return _noRows, UFOmod
#
#
# Loading Module
#
def print_gravity(load, _gravUnit):
    """
    """
    UFOmod = []
    _iter = 0
    for _key, _load in sorted(load.items()):
        try:
            _try = 1.0 / len(_load.gravity)

            UFOmod.append("' \n")
            UFOmod.append("' \n")
            UFOmod.append("'{:} Gravity Load\n".format(70 * "-"))
            UFOmod.append("' \n")
            UFOmod.append("'          Load Case         Acc_X        Acc_Y        Acc_Z\n")
            UFOmod.append("' \n")

            UFOmod.append(" GRAVITY    {:8.0f} ".format(_load.number))

            # TODO: need to solve this
            _NoSteps, _res = print_iter(_load.gravity, _gravUnit, 0, 2)
            UFOmod.extend(_res)
            UFOmod.append("\n")
        except ZeroDivisionError:
            pass
    
    return UFOmod
#
def print_nodal_load(load, node, _forceFactor, _dispFactor):
    """
    """
    UFOmod = []
    _node_off = []
    _iter = 0
    for load_number, _load in sorted(load.items()):

        # print header
        if _iter == 0:
            UFOmod.append("' \n")
            UFOmod.append("'{:} Nodal Load\n".format(72 * "-"))
            UFOmod.append("' \n")
            UFOmod.append("'           Load Case   Node ID        L O A D   I N T E N S I T Y\n")
            UFOmod.append("' \n")
        
        _iter += 1
        _load_iter = 0
        _nodes_list = set(_load.node)
        for _node_number in _nodes_list:
            # find node
            try:
                _node = node[_node_number]
            except KeyError:
                _node_off.append(_node_number)
                continue
            #
            _total = _node.get_sum_point_load(load_number)
            if not _total:
                continue               
            #
            if _load_iter == 0:
                UFOmod.append("'{:} Load Case Name : {:}\n".format(12 * "-", _load.name))
            #
            UFOmod.append(" NODELOAD    {:8.0f}  {:8.0f} ".format(_load.number, _node.number))
            _NoSteps, _res = iter_point_load(_total, _forceFactor, _dispFactor,
                                             0, 3)
            UFOmod.extend(_res)
            UFOmod.append("\n")
            _load_iter += 1
    #
    _node_off = list(set(_node_off))
    #
    return UFOmod, _node_off
#
def print_node_displacement(load, node, _dispFactor):
    """
    """
    UFOmod = []
    _node_off = []
    _iter = 0
    for load_number, _load in sorted(load.items()):
        
        _nodes_list = set(_load.node)
        for _node_number in _nodes_list:
            # find node
            try:
                _node = node[_node_number].load[load_number]
            except KeyError:
                _node_off.append(_node_number)
                continue
            
            # check point load only
            if not _node.displacement:
                continue
            print(' *** Note --> nodal disp load to be added')
            _iter += 1
    #
    _node_off = list(set(_node_off))
    #
    return UFOmod, _node_off
#
def print_temperature(load, element):
    """
    """
    UFOmod = []
    _member_off = []
    _iter = 0
    _temp = set()
    for load_number, _load in sorted(load.items()):
        _element_list = set(_load.element)
        
        for _member_no in _element_list:
            # check if member exist
            try:
                _elem = element[_member_no].load[load_number]
            except KeyError:
                _member_off.append(_member_no)
                continue            

            # check udl load only
            if not _elem.temperature:
                continue
            #
            if _iter == 0:
                UFOmod.append("' \n")
                UFOmod.append("'{:} Temperature Load\n".format(66 * "-"))
                UFOmod.append("' \n")
                UFOmod.append("'          ldcase    elnox    Tempx   gradTy   gradTz\n")
                UFOmod.append("' \n")

            _mean = sum(_elem.temperature) / float(len(_elem.temperature))
            # UFOmod.append("' \n")
            UFOmod.append(" BELTEMP {:8.0f} {:8.0f} {:8.0f} {:8.0f} {:8.0f}\n"
                          .format(_load.number, element[_member_no].number, _mean, 0, 0))
            _temp.add(element[_member_no].material[0].number)
            _iter += 1
        #
        if _temp:
            UFOmod.append("'\n")
            UFOmod.append("'          curSet    matno1 .. matnon\n")
            UFOmod.append(" STEELTDEP      1 ")
            for _item in _temp:
                UFOmod.append(" {:8.0f}".format(_item))
            UFOmod.append("\n")
    
    return UFOmod, _member_off
#
def print_pressure(load, element, _forceFactor):
    """
    """
    UFOmod = []
    _member_off = []
    _pressure_off = []
    _iter = 0
    
    for load_number, _load in sorted(load.items()):
        #
        # udl
        _element_list = set(_load.shell)
        for _member_no in _element_list:
            # check if member exist
            try:
                _elem = element[_member_no]
                #_elem = element[_member_no].load[load_number]
                _number = _elem.number
                #_type = _elem.type.element
                _nodeNo = len(_elem.node)
                _pressure = _elem.load[load_number].pressure
            except KeyError:
                _member_off.append(_member_no)
                continue
            
            # check pressure load only
            if not _pressure: 
                continue

            #try:
            #    _number = element[_member_no].number
            #    _type = element[_member_no].type.element
            #    _nodeNo = len(element[_member_no].node)
            #except KeyError:
            #    _member_off.append(_elem.number)
            #    continue

            #if _type == 'shell' : #and _elem.load[load_number].option == 1:

            if _iter == 0:
                UFOmod.append("' \n")
                UFOmod.append("'{:} Normal Pressure Load\n".format(62 * "-"))
                UFOmod.append("' \n")
                UFOmod.append("'          Load Case    Elem ID            p1           p2\n")
                UFOmod.append("'                                          p3           p4\n")
                UFOmod.append("' \n")
            #
            _total = _elem.get_sum_distributed_load(load_number)
            
            if not _total:
                continue
            
            #
            UFOmod.append(" PRESSURE    {:8.0f}  {:8.0f} ".format(_load.number, _number))                
            
            #
            _NoSteps, _res = iter_udl_load(_total, _forceFactor, 0, _nodeNo)
            UFOmod.extend(_res)
            UFOmod.append("\n")
            _iter += 1

        #else:
        #    _pressure_off.append(_type)
        #    print('** Error --> pressure in {:} element '.format(_type))
        #    continue
    #
    return UFOmod, _member_off, _pressure_off
#
def print_udl(load, element, udlFactor):
    """
    """
    UFOmod = []
    _member_off = []
    _iter = 0
    for load_no, _load in sorted(load.items()):
        # print header
        if _iter == 0:
            UFOmod.append("' \n")
            UFOmod.append("'{:} UDL Load\n".format(74 * "-"))
            UFOmod.append("' \n")
            UFOmod.append("'          Load Case   Elem ID        L O A D   I N T E N S I T Y\n")
            UFOmod.append("' \n")
        _iter += 1
        _load_iter = 0
        # udl elements
        _element_list = set(_load.element)
        for _member_no in _element_list:
            # check if member exist
            try:
                _element = element[_member_no]
            except KeyError:
                _member_off.append(_member_no)
                continue
            #
            if _element.type.element != 'beam':
                continue
            # get flat sum load
            _total = _element.get_sum_distributed_load(load_no)
            # check if load
            if not _total:
                continue
            #
            if _load_iter == 0:
                UFOmod.append("'{:} Load Case Name : {:}\n".format(12 * "-", _load.name))            
            #
            UFOmod.append(" BEAMLOAD   {:8.0f}  {:8.0f}  "
                          .format(_load.number, _element.number))
            #
            # usfos beam udl load only for x, y & z global dir
            _flat = _total[0][:3]
            _flat.extend(_total[1][:3])
            _NoSteps, _res = iter_udl_load(_flat, udlFactor, 0, 3)
            UFOmod.extend(_res)
            UFOmod.append("\n")
            _load_iter += 1
    _member_off = list(set(_member_off))
    return UFOmod, _member_off
#
#
def iter_udl_load(_item, factor=1, _ini=0, _noRows=0):
    """
    """
    UFOmod = []
    _NoParts = len(_item)

    for x in range(_ini, _NoParts):
        if _noRows == 6 and x <= _NoParts:
            UFOmod.append("\n")
            UFOmod.append(32 * " ")
            _noRows = 0

        UFOmod.append(" {: 1.5E}".format(_item[x] * factor))
        _noRows += 1

    return _noRows, UFOmod
#
def iter_point_load(_item, factor_force=1, factor_disp=1,
                    _ini=0, _noRows=0):
    """
    """
    UFOmod = []
    _NoParts = len(_item)

    for x in range(_ini, _NoParts):
        if _noRows == 6 and x <= _NoParts:
            UFOmod.append("\n")
            UFOmod.append(32 * " ")
            _noRows = 0
        # print force
        if x < 3:
            UFOmod.append(" {: 1.5E}".format(_item[x] * factor_force))
        else: # print moment
            UFOmod.append(" {: 1.5E}".format(_item[x] * factor_force * factor_disp))
        _noRows += 1

    return _noRows, UFOmod
#
#
def print_loads(load, nodes, elements, factors):
    """
    """
    header = 'LOAD DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")

    check_out = []
    check_out.append("'\n")
    check_out.append("'{:} Printing Loads\n".format(67 * "-"))
    _node_off = []
    _member_off = []
    #_pressure_off = []
    #
    _forceFactor = factors[4]
    # need to fix gravity units
    _gravUnit = factors[0]
    _dispFactor = factors[0]
    #
    #  Gravity
    UFOmod.extend(print_gravity(load, _gravUnit))
    #
    # node
    # 
    _dummy, _node_off = print_nodal_load(load, nodes, 
                                         _forceFactor, _dispFactor)
    UFOmod.extend(_dummy)
    # printing node warnings
    if _node_off:
        check_out.append("'\n")
        check_out.append("'{:}** Warning Load not Found on the following Nodes\n"
                         .format(34 * " "))
        check_out.extend(process.common.printing.print_column_num(_node_off))
    #
    #
    _dummy, _node_off = print_node_displacement(load, nodes, _dispFactor)
    UFOmod.extend(_dummy)
    # TODO : printing node warnings  
    #
    #
    #
    # member
    udlFactor = factors[4] / factors[0]
    # UDL
    _dummy, _memb_dummy = print_udl(load, elements, udlFactor)
    UFOmod.extend(_dummy)
    _member_off.extend(_memb_dummy)    
    #
    #
    # Temperature
    _dummy, _memb_dummy = print_temperature(load, elements)
    UFOmod.extend(_dummy)
    _member_off.extend(_memb_dummy)    
    #
    #   
    # Pressure
    _dummy, _memb_dummy, _pressure_off = print_pressure(load, elements, _forceFactor)
    UFOmod.extend(_dummy)
    _member_off.extend(_memb_dummy)
    #
    # 
    # 
    if _member_off:
        check_out.append("'\n")
        check_out.append('{:} ** Warning Load not Found in the following Elements\n'
                         .format(31 * " "))
        check_out.extend(process.common.printing.print_column_num(_member_off))

    if _pressure_off:
        check_out.append("'\n")
        check_out.append('{:} ** Warning Load pressure not Found in the following Elements\n'
                         .format(25 * " "))
        check_out.extend(process.common.printing.print_column_num(_pressure_off))
        #
    return UFOmod, check_out
#
#
# Mass Module
#
def print_node_mass(node, _massFactor):
    """
    """
    UFOmod = []
    _node_mass = [key for key, _node in node.items() if _node.mass]
    
    if _node_mass:
        UFOmod.append("' \n")
        UFOmod.append("'{:} Nodal Mass\n".format(72 * "-"))
        UFOmod.append("' \n")
        UFOmod.append("'            Node ID       M A S S\n")        
        #
        for key in _node_mass:
            _node = node[key]
            UFOmod.append(" NODEMASS {:10.0f}  ".format(_node.number))
            if any(_node.mass[:2]) or any(_node.mass[3:]):
                for _mass in _node.mass:
                    UFOmod.append("{: 1.4E}  "
                                  .format(_mass * _massFactor))
            else:
                UFOmod.append("{: 1.4E}"
                              .format(_node.mass[2] * _massFactor))                
            #
            UFOmod.append("\n")
    #
    return UFOmod
#
def print_member_mass(elements, _massFactor):
    """
    """
    UFOmod = []
    _member_mass = [key for key, _elem in elements.items()
                    if _elem.type.element == 'beam' and _elem.mass]
    #
    if _member_mass:
        UFOmod.append("' \n")
        UFOmod.append("'{:} Beam Mass\n".format(73 * "-"))
        UFOmod.append("' \n")
        UFOmod.append("'                         XtraMass      ListTyp      Elem_ID\n")
        
        for key in _member_mass:
            _elem = elements[key]
            # TODO : mass in Z-vertical only?
            try:
                _test = 1.0 / _elem.mass[2]
                UFOmod.append((" X_ELMASS             {: 1.4E}         Elem {:12.0f}\n")
                              .format(_elem.mass[2] * _massFactor, _elem.number))
            except ZeroDivisionError:
                #print('-->')
                continue
    #
    return UFOmod
#
#
def print_mass(node, elements, factors):
    #
    header = 'CONCENTRATE MASS DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    #
    _massFactor = factors[1]
    #
    # node
    UFOmod.extend(print_node_mass(node, _massFactor))
    #
    # Beam Elements
    UFOmod.extend(print_member_mass(elements, _massFactor))
    #
    return UFOmod
#
#
#
# Time history module
#
def print_header_TH(name=''):
    """
    """
    header = 'TIME HISTORY DATA {:}'.format(name)
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    return UFOmod
#
def print_TH(number, time_series, column_names=None,
             factor=1.0, series_name=None):
    """
    """
    #
    if series_name:
        series_name = '! {:}'.format(series_name)
    else:
        series_name = ''
    #
    #
    _new_number = number
    print('--- Writing time history file {:}'.format(number))
    #
    UFOmod = []
    UFOmod.append("'\n")
    UFOmod.append("'{:} Time History\n".format(70 * "-"))
    UFOmod.append("'\n")
    #
    #
    if column_names:
        _ts_names = column_names
    else:
        _ts_names = list(time_series.keys())
    #
    _ts_names.remove('time')
    #
    for x, _name in enumerate(_ts_names):
        _new_number += x
        if 'disp' in _name:
            new_name = _name + "[metre]"
        elif 'vel' in _name:
            new_name = _name + "[metre/second]"
        else:
            new_name = _name + "[metre/second^2]"
        #
        UFOmod.append("'\n")
        UFOmod.append("'           Number           Type\n")
        UFOmod.append("TIMEHIST  {:7.0f}          Point      {:}\n"
                      .format(_new_number, series_name))
        UFOmod.append("    'time [sec]{:}{:}\n".format(7*" ", new_name))
        for i in range(len(time_series[_name])):
            UFOmod.append('    {: 1.6e}    {: 1.6e}\n'
                          .format(time_series['time'][i], 
                                  time_series[_name][i] * factor))
        #
        UFOmod.append("'\n")
        UFOmod.append("'\n")
    #
    return UFOmod


#