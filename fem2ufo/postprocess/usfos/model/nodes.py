# 
# Copyright (c) 2009-2018 fem2ufo
# 


# Python stdlib imports


# package imports
import fem2ufo.process.control as process
#import fem2ufo.postprocess.usfos.operation.headers as common

#
def print_data(nodes, factor=1):
    """
    """
    header = 'NODAL DATA'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    UFOmod.append("'\n")
    UFOmod.append("'                                                                  (0:Free, 1:Fixed)\n")
    UFOmod.append("'            Node ID              X              Y              Z   Boundary code\n")
    UFOmod.append("'\n")
    
    for _node in nodes.values():
        _coordinate = _node.get_coordinates(factor)
        UFOmod.append(" NODE {:14.0f}".format(_node.number))
        UFOmod.append(" {: 14.5f} {: 14.5f} {: 14.5f}".format(*_coordinate))
        #
        if _node.boundary:
            #try:
            #boundary = boundaries[_node.boundary]
            UFOmod.append("  ")
            for _bound in _node.boundary.releases:
                _ir = 0
                if _bound != 0:
                    _ir = 1
                UFOmod.append((" {:1.0f}").format(_ir))
            
            #except AttributeError:
            #    pass
        
        UFOmod.append("\n")    
    
    return UFOmod
#