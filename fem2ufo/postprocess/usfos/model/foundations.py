# 
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports

# package imports
import fem2ufo.process.control as process


# Foundation
#
def print_pytzqz(foundation, factors):
    """
    """
    #
    header = 'SOIL MODELLING'
    UFOmod = process.common.headers.print_head_line(header, subfix="'")
    #
    # Print PY curves
    #
    UFOmod.append("'\n")
    UFOmod.append("'{:} PY spring data\n".format(68 * "-"))
    UFOmod.append("'\n")
    springtype = 'ELPLCURV'
    
    for key in sorted(foundation, key = lambda name: foundation[name].number):
        if 'py_' in key.lower():
            _soil = foundation[key]
            UFOmod.extend(print_spring_data(_soil.spring, 
                                            springtype, 
                                            factors))

    #
    # Print TZ curves
    #
    UFOmod.append("'\n")
    UFOmod.append("'{:} TZ spring data\n".format(68 * "-"))
    UFOmod.append("'\n")
    springtype = 'ELPLCURV'

    for key in sorted(foundation, key = lambda name: foundation[name].number):
        if 'tz_' in key.lower():
            _soil = foundation[key]
            UFOmod.extend(print_spring_data(_soil.spring, 
                                            springtype, 
                                            factors))
    #
    # Print QZ curves
    #
    UFOmod.append("'\n")
    UFOmod.append("'{:} QZ spring data\n".format(68 * "-"))
    UFOmod.append("'\n")
    springtype = 'HYPELAST'
    
    for key in sorted(foundation, key = lambda name: foundation[name].number):
        if 'qz_' in key.lower():
            _soil = foundation[key]
            UFOmod.extend(print_spring_data(_soil.spring, 
                                            springtype, 
                                            factors))

    #
    UFOmod.append("'\n")
    #
    return UFOmod


#
def print_spring_data(_spring, springtype, factors):
    """
    """
    UFOmod = []
    _forceFactor = factors[4]
    _lenFactor = factors[0]
    _top = 0
    # 
    UFOmod.append("'\n")
    UFOmod.append("'                  ID             P         delta\n")
    UFOmod.append("{:} {:12.0f}".format(springtype, _spring.number))
    _spc = ''
    for x in range(len(_spring.force)):

        if _top == _spring.displacement[x]: 
            UFOmod.append("'")

        UFOmod.append("{:} {: 1.6e} {: 1.6e}\n"
                      .format(_spc, _spring.force[x] * _forceFactor / _lenFactor,
                              _spring.displacement[x] * _lenFactor))
        _spc = 21 * ' '

        _top = _spring.displacement[x]

    return UFOmod


#