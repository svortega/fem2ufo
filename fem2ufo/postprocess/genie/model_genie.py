# 
# Copyright (c) 2009-2020 fem2ufo
# 


# Python stdlib imports
import os
import sys


# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.postprocess.genie.model.js_format as model_js
import fem2ufo.process.control as process
import fem2ufo.postprocess.genie.operation.process as js_operation
import fem2ufo.postprocess.csv_format.model.loading as csv_comb

#
class GeniE_conversion:
    """Class to convert fem2ufo FE concept model to GeniE js format""" 
    
    def __init__(self, Genie_format="js",
                 find_overlap=True):
        """
        """
        self.model_format = Genie_format
        #
        self.units_in = process.units.set_SI_units()
        #
        self.units_out = process.units.set_SI_units()
        #
        # set working units
        _units_input = self.units_in
        _units_output = self.units_out
        #
        # here factors are getting for unit conversion purposes
        factors, _grav = process.units.get_factors_and_gravity(_units_input,
                                                               _units_output)
        self.factors = factors
        self.gravity = _grav
        #
    #
    def model(self, f2u_model):
        """
        """
        self.model = f2u_model.get_model()
        self.input_format = f2u_model.format
        self.units_in = self.model.data['units']
    #
    def find_overlapping_beams(self, alignment=False, tol=1.5):
        """
        """
        # Find overlaping members
        #
        #if find_overlap:
        try:
            self.model.component.sets['overlap']
        except KeyError:
            grp_number = [grp.number for grp in self.model.component.sets.values()]
            _number = max(grp_number) + 1
            self.model.component.sets['overlap'] = f2u.Geometry.Groups('overlap', _number)
        #
        _concept = self.model.component.concepts
        _group = process.concept.find_overlapping_beam(_concept, alignment, tol)
        self.model.component.sets['overlap'].concepts.extend(_group)
        #
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """
        try:
            path = os.path.normcase(path)
            os.chdir(path)

        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()

        retval = os.getcwd()
        print('')
        print('--- Current working directory : {:}'.format(retval))
    #
    def print_model(self, file_name=None, rules=True,
                    subfix="", cone_auto=False,
                    renumber_load=False):
        """
        """
        print('')
        print('--------------- Post-Processing Model --------------')
        #
        if 'sesam' in self.input_format.lower():
            subfix=False
        #
        if self.model_format == "js" :
            #
            # set output file name
            if not file_name:
                file_name = self.model.name
                file_name, extension = process.common.remove_file_extension(file_name)
            #
            print('')
            print('--------------- Printing Out Model -----------------')
            
            # --------------------------------------------------------------------
            # Processing ufo model geometry/foundations
            # --------------------------------------------------------------------
            
            self.js_model = model_js.GeniE_js_format()
            
            # geometry
            self.js_model.model(self.model, 
                                self.factors, rules,
                                subfix, cone_auto)
            
            # loading
            try:
                self.js_model.loading(self.model.load,
                                      renumber_load)
            except AttributeError:
                pass
            #
            # print hydrodynamic
            try:
                self.js_model.metocean(self.model.metocean)
                #print('here')
            except AttributeError:
                pass
            #
            # print soil's non-linear springs
            try:
                self.js_model.foundation(self.model.foundation)
            except AttributeError:
                pass
            #
            #
            self.js_model.print_model(file_name)
        #
        #self.file_name = file_name
    #
    def print_load_combinations(self, file_name=None):
        """
        """
        # set output file name
        if not file_name:
            _name = self.model.component.name.split('.')
            file_name = _name[0]
        #
        if self.model_format == "js" :  
            self.js_model.print_load_combinations(self.model.analysis,
                                                  file_name)
    #
    def print_analysis(self, file_name=None):
        """
        """
        # set output file name
        if self.model_format == "js" :  
            self.js_model.print_analysis(self.model.analysis,
                                         self.model.metocean,
                                         file_name)
    #    
    #
    #
    def print_xml_model(self, file_name):
        """
        """
        self.model_format = "xml"
    #
    def update_naming_convention(self):
        """
        update model naming convetion
        """
        js_operation.rename_load(self.model['load'])
    #
    def get_buckling_factors(self, file_name=None, file_format=False):
        """
        """
        # set output file name
        if not file_name:
            file_name = self.model['geometry'].name
            file_name, extension = process.common.check_input_file(file_name)
        #
        if not file_format:
            file_format = self.model_format
        #
        if file_format == "js":
            self.js_model.print_buckling_factors(file_name)
            
        elif file_format == "xml":
            pass
    #
    def get_load_combinations(self, file_name=None, 
                              output_format='csv', factors=False):
        """
        """
        # set output file name
        if not file_name:
            file_name = self.model.name
            file_name, extension = process.common.check_input_file(file_name)        
        #
        if not factors:
            factors = self.factors        
        #
        if output_format.lower() =='csv':
            csv_comb.print_load_combination_to_csv(self.model, self.units_out)
        else:
            print('FIX js output')
            1/0
        #
        #process.print_load_combination(load)
        #fileJS = str(file_name) + '_LC.CSV'
        #JSmodel = open(fileJS, 'w+')
        #JSmodel.close()
#    