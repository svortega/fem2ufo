# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports
import sys


# package imports
import fem2ufo.process.control as process
#import fem2ufo.postprocess.genie.operation.headers as headers
from fem2ufo.process.modules.euclid import Point3

#
def print_joints(joints, factor=1, subfix=False):
    """
    """
    _prefix = ""
    if subfix:
        _prefix = str(subfix) + "_"  
    #
    header = 'JOINTS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    i = 0
    for _joint in joints.values():
        
        if (_joint.type == 'shim' or _joint.type == 'pile_head' 
            or _joint.type == 'interface'):
            continue
        i+=1
        _joint.get_name(number=i)
        JSmod.append('{:}{:} = '.format(_prefix, _joint.name))
        JSmod.append('Joint(Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m));\n'
                     .format(_joint.node.x * factor, 
                             _joint.node.y * factor, 
                             _joint.node.z * factor))
    #
    JSmod.append('// \n')
    JSmod.append('// Interface Joints\n')
    JSmod.append('// \n')
    for _joint in joints.values():
        if _joint.type != 'interface':
            continue
        
        #if '1101' in _joint.name:
        #    print('-->')
            
        i+=1
        #_joint.get_name(name='dum', number=i)
        _name = 'dum' + str(_joint.name) + '_int'
        JSmod.append('{:}{:} = '.format(_prefix, _name))
        JSmod.append('Joint(Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m));\n'
                         .format(_joint.node.x * factor, 
                                 _joint.node.y * factor, 
                                 _joint.node.z * factor))
    #
    return JSmod
#
#
def print_shim_joints_ASAS(model, factor=1, subfix=None):
    """
    """
    joints = model.joints
    
    #_prefix = 'Bm_'
    
    _prefix = ""
    if subfix:
        _prefix = str(subfix) + "_"    
    #
    header = 'POINT to POINT JOINTS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    i = 0
    JSmod.append('// \n')
    JSmod.append('// \n')    
    for _joint in joints.values():
        
        if _joint.type != 'shim':
            continue
        #
        # Conductor Bells
        try:
            _joint.overlap[0]
            #_beam = model.concepts[_joint.overlap[0]]
            #_beam2 = model.concepts[_joint.overlap[1]]
            #
            JSmod.append('// ----------\n')
            _chord_1 = model.concepts[_joint.chord[0]]
            _chord_2 = model.concepts[_joint.chord[1]]
            
            JSmod.append('{:}{:}.joinBeams({:}{:});\n'
                         .format(_prefix, _chord_1.name,
                                 _prefix, _chord_2.name))
        
            JSmod.append('{:}{:}.joinSegments(1,2);\n'
                         .format(_prefix, _chord_1.name))
            JSmod.append('// ----------\n')
            #
        # stubs
        except IndexError:
            pass
        
        #except AttributeError:
        #    pass        
        
        _beam = model.concepts[_joint.brace[0]]
        #_beam2 = False
        #
        i+=1
        _joint.get_name(name='dum', number=i)
        JSmod.append('{:}{:} = '.format(_prefix, _joint.name))
        JSmod.append('Joint(Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m));\n'
                     .format(_joint.node.x * factor, 
                             _joint.node.y * factor, 
                             _joint.node.z * factor))
        #
        JSmod.append('{:}{:}.connect();\n'.format(_prefix, _joint.name))
        #
        # ignore stubs at the moment
        #
        #if _beam2:
        JSmod.append('{:}{:}.isolate({:}{:});\n'.format(_prefix, _joint.name,
                                                        _prefix, _beam.name))            
        #JSmod.append('{:}{:}.isolate({:}{:}_{:});\n'.format(sub, _joint.name,
        #                                                    sub, _prefix, _beam2.name))
        
        JSmod.append('\n')
        
    #
    JSmod.append('// \n')
    JSmod.append('// Point-point connections\n')
    JSmod.append('// \n')
    #
    for _joint in joints.values():
        if _joint.type != 'shim':
            continue
        # bells
        try:
            _joint.overlap[0]
            _beam = model.concepts[_joint.brace[0]]
            if _beam.node[0].equal(_joint.node):
                lengh = 0
            elif _beam.node[1].equal(_joint.node):
                lengh = 1
            else:
                print('   *** error joint number not found')
                #sys.exit()
                continue
            
            JSmod.append('{:}P2P{:} = PointPointConnection('
                         .format(_prefix, _joint.name))        
            
            JSmod.append('FootprintBeamPoint({:}{:}, {:1.3f}),'
                         .format(_prefix, _beam.name, lengh))
            
            #
            _beam = model.concepts[_joint.chord[0]]
            #if _beam.node[0].name == _joint.node.name:
            lengh = 0.50
            #else:
            #    lengh = 1
            
            JSmod.append('FootprintBeamPoint({:}{:}, {:1.3f}));\n'
                         .format(_prefix, _beam.name, lengh))
            #
            JSmod.append('{:}P2P{:}.localSystem = LocalSystem(Vector3d(1 m,0 m,0 m), Vector3d(0 m,0 m,1 m));\n'
                         .format(_prefix, _joint.name))
            #
            JSmod.append('{:}P2P{:}.ConnectionType = GeneralSpringConnectionType'
                         .format(_prefix, _joint.name))
            #
            JSmod.append('({:1.4e} N/m, {:1.4e} N/m, {:1.4e} N/m, {:1.4e} N*m, {:1.4e} N*m, {:1.4e} N*m);\n'
                         .format(*_joint.spring))
        # stubs
        except IndexError:
            #_beam = model.concepts[_joint.brace[0]]
            continue
        #except AttributeError:
        #    continue        
        #
        JSmod.append('\n')
    #
    return JSmod    
#
#
def print_shim_joints(model, piles, factor=1, subfix=None):
    """
    """
    joints = model.joints
    _prefix = ""
    # fix for asas
    _prefix2 = "Bm_"
    if subfix:
        _prefix = str(subfix) + "_"    
    #
    header = 'POINT to POINT JOINTS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    i = 0
    JSmod.append('// \n')
    JSmod.append('// \n')    
    for key, _joint in joints.items():
        if _joint.type != 'shim':
            continue
        #
        #if '202' in key:
        #    print('-->')
        #
        i+=1
        _joint.get_name(name='dum', number=i)
        JSmod.append('{:}{:} = '.format(_prefix, _joint.name))
        JSmod.append('Joint(Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m));\n'
                     .format(_joint.node.x * factor, 
                             _joint.node.y * factor, 
                             _joint.node.z * factor))
        #
        JSmod.append('{:}{:}.connect();\n'.format(_prefix, _joint.name))
        JSmod.append('{:}{:}.isolate('.format(_prefix, _joint.name))
        #
        # print overlapping members
        try:
        #if _joint.overlap:
            #_joint.overlap
            #if _joint.overlap:
            for _memb_name in _joint.overlap:
                #if '201P_101P' in _memb_name:
                #    print('-->')
                try:
                    _beam = model.concepts[_memb_name]
                    JSmod.append(' {:}{:},'.format(_prefix2, _beam.name))
                except KeyError:
                    #print('--->')
                    _beam = piles.concepts[_memb_name]
                    JSmod.append(' {:}Pile_{:},'.format(_prefix, _beam.name))
            #
            JSmod.append(');\n')
            JSmod.append('{:}P2P{:} = PointPointConnection('
                                     .format(_prefix, _joint.name))
            #
            _memb_name = _joint.overlap[0]
            try:
                _beam = model.concepts[_memb_name]
                JSmod.append('FootprintBeamPoint({:}{:}, {:}.point()),'
                             .format(_prefix2, _beam.name, _joint.name))
            except KeyError:
                _beam = piles.concepts[_memb_name]
                JSmod.append('FootprintBeamPoint({:}Pile_{:}, {:}.point()),'
                             .format(_prefix, _beam.name, _joint.name))
                
            if _joint.chord:
                _beam2 = model.concepts[_joint.chord[0]]
            else:
                _beam2 = model.concepts[_joint.brace[0]]
        # print stubs
        #else:
        except AttributeError:
            #print('   ---> Dummy joints ignore stubs at the moment')
            for _memb_name in _joint.chord:
                try:
                    _beam = model.concepts[_memb_name]
                    JSmod.append(' {:}{:},'.format(_prefix2, _beam.name))
                except KeyError:
                    #print('--->')
                    _beam = piles.concepts[_memb_name]
                    JSmod.append(' {:}Pile_{:},'.format(_prefix, _beam.name))
            #
            JSmod.append(');\n')
            JSmod.append('{:}P2P{:} = PointPointConnection('
                                     .format(_prefix, _joint.name))
            
            _memb_name = _joint.chord[0]
            try:
                _beam = model.concepts[_memb_name]
                JSmod.append('FootprintBeamPoint({:}{:}, {:}.point()),'
                             .format(_prefix2, _beam.name, _joint.name))
            except KeyError:
                _beam = piles.concepts[_memb_name]
                JSmod.append('FootprintBeamPoint({:}Pile_{:}, {:}.point()),'
                             .format(_prefix, _beam.name, _joint.name))
            # fix for asas
            #try:
            _beam2 = model.concepts[_joint.brace[0]]
            #except KeyError:
            #    _beam2 = model.concepts[_joint.brace[1]]
        #
        #
        JSmod.append('FootprintBeamPoint({:}{:}, {:}.point()));\n'
                             .format(_prefix2, _beam2.name, _joint.name))
    
        JSmod.append('{:}P2P{:}.ConnectionType(GeneralSpringConnectionType'
                             .format(_prefix, _joint.name))
    
        JSmod.append('({:1.4e} N/m, {:1.4e} N/m, {:1.4e} N/m, {:1.4e} N*m, {:1.4e} N*m, {:1.4e} N*m));\n'
                             .format(*_joint.spring))
    
        try:
            _beam = model.concepts[_memb_name]
            JSmod.append('{:}P2P{:}.localSystem({:}{:}.localSystem);\n'
                                 .format(_prefix, _joint.name, _prefix2, _beam.name))
        except KeyError:
            _beam = piles.concepts[_memb_name]
            JSmod.append('{:}P2P{:}.localSystem({:}Pile_{:}.localSystem);\n'
                                 .format(_prefix, _joint.name, _prefix, _beam.name))
    
        JSmod.append('//\n')
        #
    JSmod.append('//\n')
    return JSmod 
    #
def print_shim_joints2(model, piles, factor=1, subfix=None):
    """
    """
    joints = model.joints
    _prefix = ""
    if subfix:
        _prefix = str(subfix) + "_"    
    #
    header = 'POINT to POINT JOINTS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    
    JSmod.append('// \n')
    JSmod.append('// Point-point connections\n')
    JSmod.append('// \n')
    #
    for _joint in joints.values():
        if _joint.type != 'shim':
            continue
        #
        _beam1 = model.concepts[_joint.overlap[0]]
        
        if _beam1.node[0].equal(_joint.node):
            lengh = 0
        elif _beam1.node[1].equal(_joint.node):
            lengh = 1
        else:
            print('   *** error joint number not found')
            continue
        
        # nodes member 1
        A = Point3(_beam1.node[0].x, 
                   _beam1.node[0].y, 
                   _beam1.node[0].z)
    
        B = Point3(_beam1.node[1].x, 
                   _beam1.node[1].y, 
                   _beam1.node[1].z)
        #
        _vector1 = B - A
        #
        JSmod.append('{:}P2P{:} = PointPointConnection('
                     .format(_prefix, _joint.name))        
    
        JSmod.append('FootprintBeamPoint({:}{:}, {:1.3f}),'
                     .format(_prefix, _beam1.name, lengh))
        #        
        # bells
        # what if no chord is identified?
        if _joint.chord:
            _beam = model.concepts[_joint.chord[0]]
        # stubs (find how to include stubs)
        # ignore stubs at the moment
        #except KeyError:
        else:
            #print('  stub member to be incluided')
            _beam = model.concepts[_joint.brace[0]]
            #continue
        #
        #
        # nodes member 2
        C = Point3(_beam.node[0].x, 
                   _beam.node[0].y, 
                   _beam.node[0].z)
    
        D = Point3(_beam.node[1].x, 
                   _beam.node[1].y,
                   _beam.node[1].z)
        #
        _vector2 = C - D
        #
        if _beam.node[0].equal(_joint.node):
            lengh = 0
        elif _beam.node[1].equal(_joint.node):
            lengh = 1
        else:
            print('   *** error joint number not found')
            continue
    
        JSmod.append('FootprintBeamPoint({:}{:}, {:1.3f}));\n'
                             .format(_prefix, _beam.name, lengh))
        #
        #
        V1 = _vector1.normalized()
        V2 = _vector2.normalized()            
        JSmod.append('{:}P2P{:}.localSystem = LocalSystem('
                     .format(_prefix, _joint.name))
    
        JSmod.append('Vector3d({: 1.4f} m, {: 1.4f} m, {: 1.4f} m), '.format(*V1))
        JSmod.append('Vector3d({: 1.4f} m, {: 1.4f} m, {: 1.4f} m)'.format(*V2))
        JSmod.append(');\n')
        #
        JSmod.append('{:}P2P{:}.ConnectionType = GeneralSpringConnectionType'
                     .format(_prefix, _joint.name))
        #
        JSmod.append('({:1.4e} N/m, {:1.4e} N/m, {:1.4e} N/m, {:1.4e} N*m, {:1.4e} N*m, {:1.4e} N*m);\n'
                     .format(*_joint.spring))        
        #
        JSmod.append('\n')
    #
    return JSmod    
#