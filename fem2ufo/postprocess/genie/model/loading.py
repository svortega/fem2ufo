# 
# Copyright (c) 2009-2020 fem2ufo
# 


# Python stdlib imports
import math
#import sys

# package imports
import fem2ufo.process.control as process
#import fem2ufo.postprocess.genie.operation.headers as headers
import fem2ufo.f2u_model.operations.geometry as geomop
from fem2ufo.process.modules.euclid import Point3

#
#
def find_coordinate(_memb, node_distance, end):
    """
    """
    #
    distance = node_distance[end]
    _node_end = 0
    if end != 0:
        _node_end = -1
    #
    _nodeNo3 = [0, 0, 0]
    #
    if distance <= 0.0001 :
        _nodeNo3[0] = _memb.node[_node_end].x 
        _nodeNo3[1] = _memb.node[_node_end].y
        _nodeNo3[2] = _memb.node[_node_end].z
    
    else:
        if end == 0:
            _v1 = (_memb.node[-1].x - _memb.node[0].x)
            _v2 = (_memb.node[-1].y - _memb.node[0].y)
            _v3 = (_memb.node[-1].z - _memb.node[0].z)
        else:
            _v1 = (_memb.node[0].x - _memb.node[-1].x)
            _v2 = (_memb.node[0].y - _memb.node[-1].y)
            _v3 = (_memb.node[0].z - _memb.node[-1].z)
        #
        _norm = math.sqrt(_v1**2 + _v2**2 + _v3**2)
        #
        _v1 /= _norm
        _v2 /= _norm
        _v3 /= _norm
        
        _nodeNo3[0] = (_memb.node[_node_end].x + _v1 * distance)
        _nodeNo3[1] = (_memb.node[_node_end].y + _v2 * distance)
        _nodeNo3[2] = (_memb.node[_node_end].z + _v3 * distance)
    #
    return geomop.Coordinates(*_nodeNo3) #_nodeNo3
    #
#
#
# Basic Loading
#
def print_basic_loading(load, node, concept, factors, 
                        subfix=False, renumber_load=False):
    """
    """
    # TODO : fix this
    # default force units is meter*second^-2*gram
    # to convert to meter*second^-2*kilogram : 
    _force_factor = factors[4]
    if factors[4] != 1:
        _force_factor = factors[4] / 1000.0
    #
    # need to fix gravity units
    _gravity_unit = factors[0]
    _length_factor = factors[0]
    #
    basic_load_no = 0
    loadNo = 0
    if renumber_load:
        loadNo = int(renumber_load) - 1
    #
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    
    #
    header = 'LOAD MODELLING'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    JSmod.append('// \n')
    JSmod.append('//   Basic Load Cases (BLC)\n')
    JSmod.append('// \n')
    #
    _load_name = [key for key in sorted(load, key = lambda name: load[name].number)]
    #
    _tb_remove = []
    for key in _load_name:
        _load = load[key]
        if (not _load.concept and not _load.element and not _load.node 
            and not _load.gravity and not _load.areas and not _load.shell
            and not _load.acceleration):
            _tb_remove.append(key)
            continue
        #
        loadNo += 1
        JSmod.append('\n')
        JSmod.append('{:}{:} = LoadCase();\n'.format(sub, _load.name))
        if renumber_load:
            #_load.number = loadNo
            JSmod.append('{:}{:}.setFemLoadcase({:}); \n'
                         .format(sub, _load.name, loadNo))
        else:
            JSmod.append('{:}{:}.setFemLoadcase({:}); \n'
                                 .format(sub, _load.name, _load.number))
        
        JSmod.append('{:}{:}.designCondition(lcOperating);\n'
                     .format(sub, _load.name))
    #
    # remove redundant load cases
    for key in _tb_remove:
        _load_name.remove(key)    
    #
    #
    JSmod.append('\n')
    JSmod.append('// Self Weight \n')
    for key in _load_name:
        _load = load[key]
        try:
            _test = 1.0 / len(_load.gravity)
            JSmod.append('{:}{:}.setAcceleration( Vector3d({: 1.4f} m/s^2, {: 1.4f} m/s^2, {: 1.4f} m/s^2,));\n'
                         .format(sub, _load.name,
                                 _load.gravity[0] * _gravity_unit, 
                                 _load.gravity[1] * _gravity_unit, 
                                 _load.gravity[2] * _gravity_unit))
        except:
            continue
    #
    #
    JSmod.append('\n')
    JSmod.append('// Accelerations \n')
    for key in _load_name:
        _load = load[key]
        #if 'LLMX' in key:
        #    print('-->')
        try:
            _test = 1.0 / len(_load.acceleration)
            JSmod.append('{:}{:}.setAcceleration( Vector3d({: 1.4f} m/s^2, {: 1.4f} m/s^2, {: 1.4f} m/s^2,));\n'
                         .format(sub, _load.name,
                                 _load.acceleration[0] * _gravity_unit, 
                                 _load.acceleration[1] * _gravity_unit, 
                                 _load.acceleration[2] * _gravity_unit))
        except:
            continue        
        
    #
    for key in _load_name:
        _load = load[key]
        JSmod.append(' \n')
        JSmod.append('// Loads \n')
        #
        #if _load.number == 4000:
        #    print('here')
        #
        # Self Weight
        if len(_load.gravity) == 0:
            JSmod.append('{:}{:}.excludeSelfWeight();\n'
                         .format(sub, _load.name))
        else:
            JSmod.append('{:}{:}.includeSelfWeight();\n'
                         .format(sub, _load.name))
            if _load.gravity[0] != 0 or _load.gravity[1] != 0 or _load.gravity[2] > 0:
                JSmod.append('{:}{:}.includeStructureMassWithRotationField();\n'
                                     .format(sub, _load.name))
        # accelerations
        if len(_load.acceleration) != 0:
            #if _load.gravity[0] != 0 or _load.gravity[1] != 0 or _load.gravity[2] > 0:
            JSmod.append('{:}{:}.includeStructureMassWithRotationField();\n'
                                 .format(sub, _load.name))
        
        JSmod.append('{:}{:}.meshLoadsAsMass(false);\n'
                             .format(sub, _load.name))
        JSmod.append('//\n')
        #
        # Nodal loading
        # there is an issue for repeated nodes, 
        # a quick fix using sets to eliminate repeated nodes
        _nodes_list = set(_load.node)
        for _nodeNo in sorted(_nodes_list):
            #_nodeNo = int(_nodeitem)
            _node = node[_nodeNo]
            _node_load = _node.load[key]
            #
            if not _node_load:
                continue
            # force
            for _point in _node_load.point:
                basic_load_no += 1
                JSmod.append('{:}BLC{:}_{:} = PointLoad({:}{:},'
                             .format(sub, str(basic_load_no).zfill(3), _node_load.name,
                                     sub, _load.name))
                
                x1, y1, z1 = _node.get_coordinates(_length_factor)
                JSmod.append(' Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m), '
                             .format(x1, y1, z1))
                #
                for x in range(len(_point)):
                    if x < 3:
                        JSmod.append('{:1.4e} N'.
                                     format(_point[x] * _force_factor))
                    else:
                        JSmod.append('{:1.4e} N*m'
                                     .format(_point[x] * _force_factor * _length_factor))
    
                    if x < 5: 
                        JSmod.append(', ')
    
                JSmod.append(');\n')
            # displacement
            for _point in _node_load.displacement:
                basic_load_no += 1
                JSmod.append('{:}BLC{:}_{:} = PrescribedDisplacement({:}{:}, sp_{:}, '
                             .format(sub, str(basic_load_no).zfill(3), _node_load.name,
                                     sub, _load.name, _node.name))
                #
                for x in range(len(_point)):
                    if x < 3:
                        JSmod.append('{:1.4e} m'.
                                     format(_point[x] * _length_factor))
                    else:
                        JSmod.append('{:1.4e} deg'
                                     .format(_point[x]))
    
                    if x < 5: 
                        JSmod.append(', ')
                #
                JSmod.append(');\n')                
                # print('-->')
        #
        # Beam Element loading
        _prefix = "Bm_"
        # there is an issue for repeated nodes, 
        # a quick fix using sets to eliminate repeated nodes        
        _element_list = set(_load.concept)
        for memb_name in sorted(_element_list):
            try:
                _memb = concept[memb_name]
            except KeyError:
                try:
                    cone_name = memb_name + '_cone'
                    _memb = concept[cone_name]
                except KeyError:
                    pass
            #finally:
            #    pass
            #if _memb.name == 'Bm_3693_4800':
            #    print('-->')
            _memb_load = _memb.load[key]
            #
            if _memb.offsets:
                for _point in _memb_load.point:
                    _segment = _memb.length_node2node
                    #_flexible_segment = _memb.flexible_length                
                    #
                    basic_load_no += 1
                    JSmod.append('{:}BLC{:}_{:} = PointLoad({:}{:},'
                                 .format(sub, str(basic_load_no).zfill(3), _point.name, 
                                         sub, _load.name))                    
                    #
                    JSmod.append(' FootprintBeamPoint({:}{:}{:},'.format(sub, _prefix, _memb.name))
                    _node_distance = _point.distance_from_node[0]
                    #
                    #
                    # FIXME: local case not yet implemented
                    if _memb.offsets[0].case == 2:
                        print('   *** warning --> offset local system should be checked')
                    #
                    # local case taken similar than global case
                    if math.isclose(_node_distance, 0, rel_tol=0.001):
                        _ratio = 0
                    else:
                        _ratio = _node_distance / _segment
                    JSmod.append(' {:1.4e}),'.format(_ratio))
                    #
                    JSmod.append(' PointForceMoment(')
                    # end 1
                    JSmod.append('Vector3d(')
                    for x in range(3):
                        JSmod.append('{: 1.4e} N'
                                     .format(_point.magnitude[0][x] * _force_factor))
                        if x < 2:
                            JSmod.append(', ')
                    # end 2
                    JSmod.append('), Vector3d(')
                    for x in range(3, 6):
                        JSmod.append('{: 1.4e} N*m'
                                     .format(_point.magnitude[0][x] * _force_factor * _length_factor))
                        if x < 5:
                            JSmod.append(', ')
                    #
                    JSmod.append(')));\n')                    
                #
                for _udl in _memb_load.distributed:
                    _segment = _memb.length_node2node
                    _flexible_segment = _memb.flexible_length
                    #
                    basic_load_no += 1
                    JSmod.append('{:}BLC{:}_{:} = LineLoad({:}{:},'
                                 .format(sub, str(basic_load_no).zfill(3), _udl.name,
                                         sub, _load.name))
                    #
                    JSmod.append(' FootprintBeam({:}{:}{:},'.format(sub, _prefix, _memb.name))
                    # global case
                    if _memb.offsets[0].case == 1:
                        # check if segment length almost similar than flexible length
                        if math.isclose(_segment, _flexible_segment, rel_tol=0.05):
                            # end 1
                            _node_distance = _udl.distance_from_node[0]
                            #
                            if math.isclose(_node_distance, 0, rel_tol=0.001):
                                _ratio = 0
                            else:
                                _ratio = _node_distance / _segment
                            JSmod.append(' {:1.4e},'.format(_ratio))
                            # update segment lenght
                            #_segment -= _node_distance
                            #
                            # end 2
                            _node_distance = _udl.distance_from_node[1]
                            #
                            if math.isclose(_node_distance, 0, rel_tol=0.001):
                                _ratio = 1
                            else:
                                _ratio = 1.0 - (_node_distance / _segment)
                            
                            JSmod.append(' {:1.4e}, false'.format(_ratio))
                        else:
                            # nodes member 1
                            A = Point3(_memb.node[0].x, 
                                       _memb.node[0].y, 
                                       _memb.node[0].z)
                        
                            B = Point3(_memb.node[1].x, 
                                       _memb.node[1].y, 
                                       _memb.node[1].z)
                            #
                            # nodes + eccentricity
                            C = Point3(_memb.node[0].x + _memb.offsets[0].eccentricity[0], 
                                       _memb.node[0].y + _memb.offsets[0].eccentricity[1], 
                                       _memb.node[0].z + _memb.offsets[0].eccentricity[2])
                        
                            D = Point3(_memb.node[1].x + _memb.offsets[1].eccentricity[0], 
                                       _memb.node[1].y + _memb.offsets[1].eccentricity[1], 
                                       _memb.node[1].z + _memb.offsets[1].eccentricity[2])
                            #
                            _vector = A - B
                            _vector_1 = C - A
                            _vector_2 = D - B
                            #
                            vdot1 = _vector.dot(_vector_1)
                            vdot2 = _vector.dot(_vector_2)
                            #
                            #_angle1 = abs(_vector.angle(_vector_1))
                            #_angle2 = abs(_vector.angle(_vector_2))
                            #
                            #_test = abs(_vector_1)
                            if math.isclose(vdot1, 0, rel_tol=0.01):
                                _start = 0
                                _ratio = 0
                            else:
                                _start = _vector_1.magnitude()
                                _ratio = _start / _segment
                            #
                            JSmod.append(' {:1.4e},'.format(_ratio))
                            #_ratio
                            #_segment -= _start
                            #
                            # end 2
                            if math.isclose(vdot2, 0, rel_tol=0.01):
                                _ratio = 1.0
                            else:
                                _ratio = 1.0 - (_vector_2.magnitude() / _segment)
                            #
                            JSmod.append(' {:1.4e}, false'.format(_ratio))
                    #
                    # FIXME: local case not yet implemented
                    else:
                        #print('   *** warning --> local system should be fixed')
                        if math.isclose(_segment, _flexible_segment, rel_tol=0.05):
                            # end 1
                            _node_distance = _udl.distance_from_node[0]
                            #
                            if math.isclose(_node_distance, 0, rel_tol=0.001):
                                _ratio = 0
                            else:
                                _ratio = _node_distance / _segment
                            JSmod.append(' {:1.4e},'.format(_ratio))
                            # update segment lenght
                            #_segment -= _node_distance
                            #
                            # end 2
                            _node_distance = _udl.distance_from_node[1]
                            #
                            if math.isclose(_node_distance, 0, rel_tol=0.001):
                                _ratio = 1
                            else:
                                _ratio = 1.0 - (_node_distance / _segment)
                            
                            JSmod.append(' {:1.4e}, false'.format(_ratio))
                        else:
                            # nodes member 1
                            #_node1 = _memb.node[0]
                            #A = Point3(_memb.node[0].x, 
                            #           _memb.node[0].y, 
                            #           _memb.node[0].z)
                            #_node2 = _memb.node[1]
                            #B = Point3(_memb.node[1].x, 
                            #           _memb.node[1].y, 
                            #           _memb.node[1].z)
                            #
                            #_vector = A - B
                            #_normalized = _vector.normalized()
                            
                            # end 0
                            end = 0
                            #coord = [0, 0, 0]
                            #for x in range(3):
                            #    coord[x] = _memb.node[0].coordinates[x] + _memb.offsets[end].eccentricity[x] * _normalized[x]
                            
                            #_start = _memb.offsets[end].normal()
                            _start = _memb.offsets[end].eccentricity[0]
                            #if (math.isclose(_node1.x, coord[0], abs_tol=1e-03) 
                            #    and math.isclose(_node1.y, coord[1], abs_tol=1e-03)
                            #    and math.isclose(_node1.z, coord[2], abs_tol=1e-03)):
                            #if _memb.node[0].equal(coord):
                            if math.isclose(_start, 0, abs_tol=1e-03) : # _start == 0: 
                                _ratio = 0
                            else:
                                _ratio = _start / _segment
                            
                            JSmod.append(' {:1.4e},'.format(_ratio))
                            #_ratio
                            #_segment -= _start
                            #
                            # end 1
                            end = 1
                            #for x in range(3):
                            #    coord[x] = _memb.node[1].coordinates[x] + _memb.offsets[end].eccentricity[1] * _normalized[1]
                            
                            #_start = _memb.offsets[end].normal()
                            _start = _memb.offsets[end].eccentricity[1]
                            #if (math.isclose(_node2.x, coord[0], abs_tol=1e-03) 
                            #    and math.isclose(_node2.y, coord[1], abs_tol=1e-03)
                            #    and math.isclose(_node2.z, coord[2], abs_tol=1e-03)):                        
                            if math.isclose(_start, 0, abs_tol=1e-03) : # _start == 0: 
                                _ratio = 1
                            else:
                                _ratio = 1.0 - (_start / _segment)
                            
                            JSmod.append(' {:1.4e}, false'.format(_ratio))
                    #
                    JSmod.append('), Component1dLinear(')
                    #
                    # end 1
                    JSmod.append('Vector3d(')
                    for x in range(3):
                        JSmod.append('{: 1.4e} N/m'
                                     .format(_udl.magnitude[0][x] 
                                             * (_force_factor / _length_factor)))
                        if x < 2:
                            JSmod.append(', ')
                    # end 2
                    JSmod.append('), Vector3d(')
                    for x in range(3):
                        JSmod.append('{: 1.4e} N/m'
                                     .format(_udl.magnitude[1][x] 
                                             * (_force_factor / _length_factor)))
                        if x < 5:
                            JSmod.append(', ')
                    #
                    JSmod.append(')));\n')
            #
            else:
                # nodal load
                for _point in _memb_load.point:
                    #
                    _node_distance = _point.distance_from_node[0]
                    _nodeNo3 = _memb.find_coordinate(node_end=0,
                                                     distance=_node_distance)
    
                    basic_load_no += 1
                    JSmod.append('{:}BLC{:}_{:} = PointLoad({:}{:},'
                                 .format(sub, str(basic_load_no).zfill(3), _point.name,  
                                         sub, _load.name))
                    #
                    JSmod.append(' Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m),'
                                 .format(_nodeNo3[0] * _length_factor, 
                                         _nodeNo3[1] * _length_factor, 
                                         _nodeNo3[2] * _length_factor))
    
                    for x in range(len(_point.magnitude[0])):
                        if x < 3:
                            JSmod.append('{: 1.4e} N'
                                         .format(_point.magnitude[0][x] * _force_factor))
                        else:
                            JSmod.append('{: 1.4e} N*m'
                                         .format(_point.magnitude[0][x] * _force_factor * _length_factor))
                        #
                        if x < 5:
                            JSmod.append(', ')
                    JSmod.append(');\n')
                # distributed
                for _udl in _memb_load.distributed:
                    basic_load_no += 1
                    JSmod.append('{:}BLC{:}_{:} = LineLoad({:}{:},'
                                 .format(sub, str(basic_load_no).zfill(3), _udl.name, 
                                         sub, _load.name))
                    #
                    JSmod.append(' FootprintLine(')
                    _nodeNo3 = find_coordinate(_memb,
                                               _udl.distance_from_node, end=0)
                    
                    JSmod.append(' Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m), '
                                 .format(_nodeNo3[0] * _length_factor, 
                                         _nodeNo3[1] * _length_factor, 
                                         _nodeNo3[2] * _length_factor))
                    #
                    _nodeNo4 = find_coordinate(_memb,
                                               _udl.distance_from_node, end=1)
                    JSmod.append(' Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m)'
                                 .format(_nodeNo4[0] * _length_factor, 
                                         _nodeNo4[1] * _length_factor, 
                                         _nodeNo4[2] * _length_factor))
                    JSmod.append('), Component1dLinear(')
                    # end 1
                    JSmod.append('Vector3d(')
                    for x in range(3):
                        JSmod.append('{: 1.4e} N/m'
                                     .format(_udl.magnitude[0][x] * (_force_factor / _length_factor)))
                        if x < 2:
                            JSmod.append(', ')
                    # end 2
                    JSmod.append('), Vector3d(')
                    for x in range(3):
                        JSmod.append('{: 1.4e} N/m'
                                     .format(_udl.magnitude[1][x] * (_force_factor / _length_factor)))
                        if x < 5:
                            JSmod.append(', ')
                    #
                    JSmod.append(')));\n')
        #
        # Shell Element loading
        _prefix = "Pl_"
        _element_list = set(_load.shell)
        for memb_name in sorted(_element_list):
            _memb = concept[memb_name]
            _memb_load = _memb.load[key]
            for _pressure in _memb_load.pressure:
                basic_load_no += 1
                JSmod.append('{:}BLC{:}_{:} = SurfaceLoad({:}{:},'
                             .format(sub, str(basic_load_no).zfill(3), 
                                     _memb_load.name, sub, _load.name))
                
                JSmod.append(' FootprintPlate({:}{:}{:}, psFront),'
                             .format(sub, _prefix, _memb.name))
                
                _constant = list(set(_pressure))
                if len(_constant) == 1:
                    JSmod.append('Pressure2dConstant({:} Pa));\n'
                                 .format(_constant[0]))
                else:
                    JSmod.append('Pressure2d3PointVarying(')
                    1/0
                    print('-->')
                    #pass   
    #
    JSmod.append('//\n')
    return JSmod    
#
def print_basic_equipment_loading(load, factors, sub=None):
    """
    """
    if sub:
        sub = str(sub) + "_"
    else:
        sub = ""
    #
    #_length_factor = factors[0]
    _functional = load.functional
    _equipments = load.equipment
    #
    header = 'EQUIPMENT LOAD MODELLING'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    _load_name = [key for key in sorted(_functional, key = lambda name: _functional[name].number)]
    #
    for key in _load_name:
        _load = _functional[key]
        #if 'PPDF' in key:
        #    print('stop')
        for _equipment in _load.areas:
            #_equipment = _equipments[_area_name]
            #for _node in _equipment.nodes:
            try:
                JSmod.append('{:}{:}.placeAtPoint({:},'
                             .format(sub, _load.name, _equipment.name))
            
                #x1, y1, z1 = _node.get_coordinates(_length_factor)
                mass, x1, y1, z1 = _equipment.mass
                JSmod.append(' Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m), '
                             .format(x1, y1, z1))
                # x direction
                if _equipment.wind_pressure[0]:
                    JSmod.append('LocalSystem(Vector3d(0 m,1 m,0 m), Vector3d(1 m,0 m,0 m)));\n')
                # y direction
                if _equipment.wind_pressure[1]:
                    JSmod.append('LocalSystem(Vector3d(1 m,0 m,0 m), Vector3d(0 m,-1 m,0 m)));\n')
                #
                JSmod.append('{:}{:}.constantLoad({:});\n'
                                     .format(sub, _load.name, _equipment.name))
                # x direction
                if _equipment.wind_pressure[0]:
                    JSmod.append('{:}{:}.equipment({:}).'
                                 .format(sub, _load.name, _equipment.name))
                    JSmod.append('sideInLocalSystem(Vector3d(0 m,0 m,1 m)).intensity = ')
                    JSmod.append('ConstantWindPressureLoadCase({:}, {:});\n'
                                 .format(_equipment.wind_pressure[0].drag_coefficient,
                                         _equipment.wind_pressure[0].suction_factor))
                    
                    JSmod.append('{:}{:}.equipment({:}).'
                                             .format(sub, _load.name, _equipment.name))
                    JSmod.append('sideInLocalSystem(Vector3d(0 m,0 m,-1 m)).intensity = ')
                    JSmod.append('ConstantWindPressureLoadCase({:}, {:});\n'
                                             .format(_equipment.wind_pressure[0].drag_coefficient,
                                                     _equipment.wind_pressure[0].suction_factor))
                # y direction
                if _equipment.wind_pressure[1]:
                    JSmod.append('{:}{:}.equipment({:}).'
                                 .format(sub, _load.name, _equipment.name))
                    JSmod.append('sideInLocalSystem(Vector3d(0 m,0 m,1 m)).intensity = ')
                    JSmod.append('ConstantWindPressureLoadCase({:}, {:});\n'
                                 .format(_equipment.wind_pressure[1].drag_coefficient,
                                         _equipment.wind_pressure[1].suction_factor))
                    
                    JSmod.append('{:}{:}.equipment({:}).'
                                             .format(sub, _load.name, _equipment.name))
                    JSmod.append('sideInLocalSystem(Vector3d(0 m,0 m,-1 m)).intensity = ')
                    JSmod.append('ConstantWindPressureLoadCase({:}, {:});\n'
                                             .format(_equipment.wind_pressure[1].drag_coefficient,
                                                     _equipment.wind_pressure[1].suction_factor))
            except AttributeError:
                _name = _equipment
                for key, _item in _equipments.items():
                    if _name in key:
                        JSmod.append('{:}{:}.placeAtPoint({:},'
                                     .format(sub, _load.name, _item.name))
                        #
                        mass, x1, y1, z1 = _item.mass
                        JSmod.append(' Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m), '
                                     .format(x1, y1, z1))
                        if _item.local_system[3] == 90:
                            JSmod.append('LocalSystem(Vector3d(0 m,0 m,-1 m), Vector3d(1 m,0 m,0 m)));\n')
                            JSmod.append('{:}{:}.constantLoad({:});\n'
                                         .format(sub, _load.name, _item.name))                            
                        elif _item.local_system[3] == 270:
                            JSmod.append('LocalSystem(Vector3d(0 m,0 m,1 m), Vector3d(-1 m,0 m,0 m)));\n')
                            JSmod.append('{:}{:}.constantLoad({:});\n'
                                         .format(sub, _load.name, _item.name))
                        else: # assuming default 
                            JSmod.append('LocalSystem(Vector3d(1 m,0 m,0 m), Vector3d(0 m,0 m,1 m)));\n')
                            JSmod.append('{:}{:}.constantLoad({:});\n'
                                         .format(sub, _load.name, _item.name))
                        #
            JSmod.append('\n')
    #
    JSmod.append('//\n')
    return JSmod
#
def print_equipment_wind_areas(load, seastate_name,
                               sub=None):
    """
    """
    if sub:
        sub = str(sub) + "_"
    else:
        sub = ""
    #
    seastate_name = seastate_name.strip()
    #
    header = 'EQUIPMENT WIND AREAS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    _load_name = [key for key in sorted(load, key = lambda name: load[name].number)]
    #
    for key in _load_name:
        _load = load[key]
        if not _load.wind:
            continue
        #_load = load[key]
        if _load.wind_areas:
            JSmod.append('\n')
            JSmod.append('{:}{:}.meshLoadsAsMass(false);\n'
                         .format(sub, _load.wind.name))
            
            JSmod.append('{:}{:}.setWindField(1.226 Kg/m^3, 0.010 m, 0 deg, Wind_{:}_{:});\n'
                         .format(sub, _load.wind.name, _load.wind.number, seastate_name))             

            JSmod.append('{:}{:}.excludeSelfWeight();\n'
                         .format(sub, _load.wind.name))

            JSmod.append('{:}{:}.includeStructureMassWithRotationField();\n'
                         .format(sub, _load.wind.name))
            JSmod.append('// \n')           
            JSmod.append('//\n')
    #
    JSmod.append('//\n')
    return JSmod
#
# First Level load combinations
#
def print_load_combinations(combination, functional, metocean,
                            analysis_name, sub=None, level=1):
    """
    """
    if sub:
        sub = str(sub) + "_"
    else:
        sub = ""

    header = 'LOAD COMBINATION'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    JSmod.append('//\n')
    JSmod.append('//   Load Combination Adjusted (LCA) - Level: {:} \n'.format(level))
    JSmod.append('//\n')    
    #
    _load_name = [key for key in sorted(combination, key = lambda name: combination[name].number)]
    #
    for _name in _load_name:
        _lc = combination[_name]
        #if _lc.number == 1006:
        #    print('here')
        # get design condition
        #
        if _lc.design_condition == 'storm':
            _case = 'lcStorm'

        elif _lc.design_condition == 'earthquake':
            _case ='lcEarthquake'

        elif _lc.design_condition == 'harbour':
            _case ='lcHarbour'

        elif _lc.design_condition == 'seagoing':
            _case ='lcSeagoing'

        else:
            _case ='lcOperating'
        #
        # print load combination
        #
        if not 'LCA' in _lc.name:
            _lc.name = 'LCA{:}_{:}_{:}'.format(str(_lc.number).zfill(3), 
                                               analysis_name, _lc.name)
        #
        JSmod.append('{:}{:} = LoadCombination({:});\n'
                     .format(sub, _lc.name, analysis_name))

        JSmod.append('{:}{:}.designCondition({:});\n'
                     .format(sub, _lc.name, _case))
        #
    #
    JSmod.append('//\n')
    JSmod.append('//\n')
    JSmod.append('//\n')
    _meto_name = {_sea.name : key for  key, _sea in metocean.items()}
    #
    for _name in _load_name:
        _lc = combination[_name]
        #if '_2001' in _lc.name :
        #    print('here')

        for _item in _lc.functional_load:
            try:
                _basic = functional[_item[0]]
                if (not _basic.concept and not _basic.element and not _basic.node 
                    and not _basic.gravity and not _basic.areas):
                    JSmod.append('// Warning basic load empty\n')
                    JSmod.append('//')
                    #continue
                #
                _load_name = _basic.name
                JSmod.append('{:}{:}.addCase({:}{:}, {:});\n'
                             .format(sub, _lc.name, 
                                     sub, _load_name, _item[1]))
            #
            except KeyError:
                print('    ** Warning in load combination one {:} --> load combination zero {:}'
                      .format(_lc.name, _item[0]))
        # check if metocean
        if _lc.metocean_load:
            for _item in _lc.metocean_load:
                try:
                    _seastate = metocean[_meto_name[_item[0]]]
                except KeyError:
                    _seastate = metocean[_item[0]]
                #
                _factor = _item[1]
                JSmod.append('{:}{:}.addCase({:}.WLC({:}, 1), {:});\n'
                             .format(sub, _lc.name, 
                                     analysis_name, 
                                     _seastate.number, _factor))
                # check wind areas
                #try:
                if _seastate.wind_areas:
                    _wind = _seastate.wind
                    #_new_name = 'WindArea_' + _seastate.name
                    _new_name = _wind.name
                    JSmod.append('{:}.addCase({:}, {:});\n'
                                 .format(_lc.name, 
                                         _new_name, _factor))
                #except AttributeError:
                #    pass
            #  
        #
        JSmod.append('//\n')
    #
    JSmod.append('//\n')
    #
    return JSmod
#
def print_load_combinations_seastateXX(combination, functional, 
                                       metocean,
                                     analysis_name, level=1):
    """
    """
    #
    header = 'LOAD COMBINATION'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    JSmod.append('//\n')
    JSmod.append('//   Adjusted Load Cases Including Metocean : {:} \n'
                 .format(level))
    JSmod.append('//\n')
    #
    for _name, _lc in sorted(combination.items()):
        if _lc.metocean_load:
            # get design condition
            #if _lc.number == 611:
            #    print('here')
            #
            if _lc.design_condition == 'storm':
                _case = 'lcStorm'

            elif _lc.design_condition == 'earthquake':
                _case ='lcEarthquake'

            elif _lc.design_condition == 'harbour':
                _case ='lcHarbour'

            elif _lc.design_condition == 'seagoing':
                _case ='lcSeagoing'

            else:
                _case ='lcOperating'
            #
            #
            for _item in _lc.metocean_load:
                _seastate = metocean[_item[0]]
                _wave = _seastate.wave

                if not 'MET' in _seastate.name:
                    _seastate.name = 'MET{:}_{:}'.format(str(_seastate.number).zfill(3), 
                                                         analysis_name, _seastate.name)

                JSmod.append('{:} = LoadCombination({:});\n'
                             .format(_seastate.name,  analysis_name))

                JSmod.append('{:}.designCondition({:});\n'
                             .format(_seastate.name, _case))
                #JSmod.append('//\n')
    #
    JSmod.append('//\n')
    JSmod.append('//\n')
    #
    for _name, _lc in sorted(combination.items()):
        if _lc.metocean_load:
            #if _lc.number == 611:
            #    print('here')

            for _item in _lc.metocean_load:
                _seastate = metocean[_item[0]]
                _wave = _seastate.wave # _item[0]
                _factor = _item[1]
                JSmod.append('{:}.addCase({:}.WLC({:}, 1), {:});\n'
                             .format(_seastate.name, analysis_name, 
                                     _seastate.number, _factor))
                # check wind areas
                try:
                    _wind = _seastate.wind
                    if _wind.areas:
                        #_factor = _item[1]
                        #_basic = functional[_item[0]]
                        #_basic = _item[0]
                        JSmod.append('{:}.addCase({:}, {:});\n'
                                     .format(_seastate.name, _wind.name, _factor))
                except AttributeError:
                    pass
                #
                JSmod.append('// \n')
    #                   
    #            
    JSmod.append('// \n')
    return JSmod
#
# Second Level load combinations
#
def print_top_load_combinations(combination, basic_lc, 
                                analysis_name, sub=None, level=2):
    """
    """
    if sub:
        sub = str(sub) + "_"
    else:
        sub = ""

    header = 'LOAD COMBINATIONS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    #
    JSmod.append('// \n')
    JSmod.append('//   Load Combination Factored (LCF) - Level: {:}\n'.format(level))
    JSmod.append('// \n')
    #
    _load_name = [key for key in sorted(combination, key = lambda name: combination[name].number)]
    #
    for _name in _load_name:
        _lc = combination[_name]
        #if _lc.number == 506:
        #   print('here')
        
        if _lc.design_condition == 'storm':
            _case = 'lcStorm'
        
        elif _lc.design_condition == 'earthquake':
            _case ='lcEarthquake'
        
        elif _lc.design_condition == 'harbour':
            _case ='lcHarbour'
        
        elif _lc.design_condition == 'seagoing':
            _case ='lcSeagoing'
        
        else:
            _case ='lcOperating'
        #
        #
        if not 'LCF' in _lc.name:
            _lc.name = 'LCF{:}_{:}_{:}'.format(str(_lc.number).zfill(3), 
                                               analysis_name, _lc.name)
        #
        JSmod.append('{:} = LoadCombination({:});\n'
                     .format(_lc.name, analysis_name)) 
        
        JSmod.append('{:}.designCondition({:});\n'
                     .format(_lc.name, _case))
    #
    JSmod.append('// \n')
    JSmod.append('// \n')
    JSmod.append('// \n')
    #
    for _name in _load_name:
        _lc = combination[_name]
        #if _lc.number == 506:
        #    print('here')
        for _item in _lc.functional_load:
            _factor = _item[1]
            try:
                _load_number = basic_lc[_item[0]].number
                _load_name = basic_lc[_item[0]].name
                JSmod.append('{:}.addCase({:}{:}, {:});\n'
                             .format(_lc.name,  
                                     sub, _load_name, _factor))
            except KeyError:
                print('    *** warning basic load case {:} not found'.format(_item[0]))
        #
        JSmod.append('//\n')
    #
    JSmod.append('// \n')
    #
    return JSmod
#
def print_top_combinations_seastateXX(combination, basic_lc, metocean,
                                    analysis_name, sub=None, level=2):
    """
    """
    #
    header = 'LOAD COMBINATIONS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    #
    JSmod.append('// \n')
    JSmod.append('//   Factored Load Combination Including Metocean - Level: {:}\n'.format(level))
    JSmod.append('// \n')    
    JSmod.append('// \n')
    #
    for key, _lc in sorted(combination.items()):
        for _item in _lc.functional_load:
            _factor = _item[1]
            _blc = basic_lc[_item[0]]
            if _blc.metocean_load:
                for _item in _blc.metocean_load:
                    _seastate = metocean[_item[0]] # _item[0]
                    JSmod.append('{:}.addCase({:}, {:});\n'
                                 .format(_lc.name, 
                                         _seastate.name, _factor))
                    JSmod.append('\n')
            #
        #
    #
    JSmod.append('// \n')
    #
    return JSmod
#
# Equipment loading
#
#
def get_small_foot_print_x(skid_beams, skid_length, skid_width):
    """
    set footprint for 1 and 2 skid beams
    """
    _list_items = []
    if skid_beams == 1:
        #print('--> 1')
        _length = skid_length / 6.0
        _list_items.append([_length, _length * -1.0,
                        - skid_width/2.0, skid_width/2.0])
    elif skid_beams == 2:
        _length = skid_length / 10
        _list_items.append([_length * 5, _length * 3,
                        - skid_width/2.0, skid_width/2.0])
    elif skid_beams == 3:
        _length = skid_length / 14
        _list_items.append([_length * 7, _length * 5,
                        - skid_width/2.0, skid_width/2.0])
        _list_items.append([_length, _length * -1,
                            - skid_width/2.0, skid_width/2.0])
    #
    return _list_items
#
def get_small_foot_print_y(skid_beams, skid_width):
    """
    set footprint for 1 and 2 skid beams
    """
    _list_items = []
    if skid_beams == 1:
        _width = skid_width / 6.0
        _list_items.append([_width, _width * -1.0])
    elif skid_beams == 2:
        _width = skid_width / 10
        _list_items.append([_width * 5, _width * 3])
    elif skid_beams == 3:
        _width = skid_width / 14
        _list_items.append([_width * 7, _width * 5])
        _list_items.append([_width, _width * -1])
    #
    return _list_items
#
#
def print_equipments(equipments,
                     factors, subfix=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    #
    # convert from gram to kilogram
    _mass_factor = factors[1] * 1000.0     
    #
    header = 'EQUIPMENTS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")    
    JSmod.append('// \n')
    JSmod.append('//Equipments\n')
    JSmod.append('// \n')

    for key, _equipment in sorted(equipments.items()):
        #if '1910SHPU_EQWM' in key:
        #    print('-->')
        #
        JSmod.append('{:}{:} = PrismEquipment('.format(sub, _equipment.name))
        length, width, height = _equipment.size
        _mass = 0
        if _equipment.mass:
            _mass, CoGx, CoGy, CoGz = _equipment.mass

        JSmod.append('{: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {:1.4e} kg);\n'
                     .format(length, width, height, _mass * _mass_factor))
        if sum(_equipment.local_system[:3]) != 0:
            JSmod.append('{:}{:}.centreOfGravity(Vector3d({: 1.3f} m, {: 1.3f} m, {: 1.3f} m));\n'
                         .format(sub, _equipment.name, *_equipment.local_system[:3]))
        #
        if _equipment.foot_print:
            JSmod.append('{:}{:}.clearFootprint();\n'.format(sub, _equipment.name))            
            # 0: long skid beam (parallel to the length x)
            # 1: transv skid beam (parallel to width y)
            x, y = _equipment.foot_print
            _step_x = x + (x - 1)
            _step_y = y + (y - 1)
            #
            _length = length / (2.0 * _step_x)
            _width = width / (2.0 * _step_y)
            #
            #
            JSmod.append('// transversal skid beam (parallel to width y)\n')
            _list_x = []
            if x <= 3:
                _list_x = get_small_foot_print_x(skid_beams = x, 
                                                 skid_length = length, 
                                                 skid_width = width)
                for _item in _list_x:
                    JSmod.append('{:}{:}.addToFootprint({: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {: 1.3f} m);\n'
                                 .format(sub, _equipment.name, *_item))
            #               
            else:
                for _stepx  in range(_step_x, 0, -4):
                    _list_x.append([_length * _stepx, _length * max(_stepx-2, -_stepx),
                                    - width/2.0, width/2.0])
                    JSmod.append('{:}{:}.addToFootprint({: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {: 1.3f} m);\n'
                                 .format(sub, _equipment.name, *_list_x[-1]))
            #
            for _item in reversed(_list_x):
                if sum(_item[:2]) == 0:
                    continue
                _list_x.append([-_item[1], - _item[0], -_item[2], - _item[3]])
                JSmod.append('{:}{:}.addToFootprint({: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {: 1.3f} m);\n'
                             .format(sub, _equipment.name, *_list_x[-1]))
            #
            #
            JSmod.append('// longitudinal skid beam (parallel to the length x)\n')
            _list_y = []
            if not _list_x:
                if y <= 3:
                    _temp = get_small_foot_print_y(skid_beams = y, skid_width=width)
                    for _item in _temp:
                        _list_y.append([-length/2.0, length/2.0, _item[0], _item[1]])
                        JSmod.append('{:}{:}.addToFootprint({: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {: 1.3f} m);\n'
                                     .format(sub, _equipment.name, *_list_y[-1]))
                else:
                    for _stepy  in range(_step_y, 0, -4):
                        _list_y.append([-length/2.0, length/2.0 ,
                                        _width * _stepy, _width * max(_stepy-2, -_stepy)])
                        JSmod.append('{:}{:}.addToFootprint({: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {: 1.3f} m);\n'
                                     .format(sub, _equipment.name, *_list_y[-1]))
            else:
                for x in range(len(_list_x)-1):
                    _x1 = _list_x[x][1] - math.copysign(0.001, _list_x[x][1])
                    if _list_x[x][1] < 0:
                        _x1 = _list_x[x][1] + math.copysign(0.001, _list_x[x][1])                    
                    #
                    _x2 = _list_x[x+1][0] + math.copysign(0.001, _list_x[x+1][0])
                    if _list_x[x+1][0] < 0:
                        _x2 = _list_x[x+1][0] - math.copysign(0.001, _list_x[x+1][0])
                    #
                    if y <= 3:
                        _temp = get_small_foot_print_y(skid_beams = y, skid_width=width)
                        for _item in _temp:
                            _list_y.append([_x1, _x2, _item[0], _item[1]])
                            JSmod.append('{:}{:}.addToFootprint({: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {: 1.3f} m);\n'
                                         .format(sub, _equipment.name, *_list_y[-1]))
                    else:
                        for _stepy  in range(_step_y, 0, -4):
                            _y1 = _width * _stepy
                            _y2 = _width * max(_stepy-2, -_stepy)
                            _list_y.append([_x1, _x2, _y1, _y2])
                            JSmod.append('{:}{:}.addToFootprint({: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {: 1.3f} m);\n'
                                         .format(sub, _equipment.name, *_list_y[-1]))
            #
            for _item in reversed(_list_y):
                if sum(_item[2:]) == 0:
                    continue
                JSmod.append('{:}{:}.addToFootprint({: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {: 1.3f} m);\n'
                             .format(sub, _equipment.name, -_item[0], - _item[1], -_item[2], - _item[3]))                     
        #
        JSmod.append('\n')
    #
    JSmod.append('// \n')
    return JSmod
#
def print_load_interface(equipments, # joints,
                         factors, subfix=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    #
    #_length_factor = factors[0]
    #
    header = 'LOAD INTERFACES'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    JSmod.append('// \n')
    JSmod.append('// Load interfaces\n')
    JSmod.append('// \n')

    for key, _equipment in sorted(equipments.items()):
        JSmod.append('{:}{:}_LI = LoadInterface();\n'
                     .format(sub, _equipment.name))

        _load_interface = list(set(_equipment.load_interface))
        for _basic in sorted(_load_interface):
            JSmod.append('{:}{:}_LI.addEquipment({:}, {:});\n'
                         .format(sub, _equipment.name, 
                                 _equipment.name, _basic))

        for _joint in _equipment.joints:
            _name = 'dum' + str(_joint.name) + '_int'
            JSmod.append('{:}{:}_LI.add({:});\n'
                         .format(sub, _equipment.name, 
                                 _name))

        JSmod.append('// \n')
    #
    JSmod.append('// \n')
    return JSmod

#
#
def print_mass_point(nodes, factors, subfix=False):
    """
    """
    # convert from gram to kilogram
    _mass_factor = factors[1] * 1000.0 
    
    sub = ""
    if subfix:
        sub = str(subfix) + "_"

    header = 'MASS POINTS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    JSmod.append('// \n')

    for _node in nodes.values():
        try:
            1.0 / len(_node.mass)
            _total = sum(_node.mass)
            JSmod.append('{:}mass_{:} = PointMass('.format(sub, _node.name))
            x1, y1, z1 = _node.get_coordinates(factors[0])
            JSmod.append('Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m), {:1.4e} kg);\n'
                         .format(x1, y1, z1, _total * _mass_factor))
        except ZeroDivisionError:
            pass

    return JSmod
#
#