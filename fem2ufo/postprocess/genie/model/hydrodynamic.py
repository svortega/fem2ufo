# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import math

# package imports
import fem2ufo.process.control as process



#
#
#
def print_hydro_parameters(metocean, factors, _noCoeff=1):
    """
    Print hydrodynamic parametrs in usfos format
    """
    #  Units
    _length_factor = factors[0]
    #
    header = 'Hydro Properties' 
    JSmod = process.common.headers.print_head_line(header, subfix="//")    
    #    
    # Morison coefficients
    JSmod.append("//\n")
    JSmod.append("//{:} Morison Coefficients\n".format(62 * "-"))
    JSmod.append("//\n")
    try:
        for _cdcm in  metocean.cdcm.values():
            if _cdcm.type == 'diameter_function' :
                JSmod.append("MorisonDiameter_{:} = MorisonDiameterFunction();\n"
                             .format(_cdcm.name))
                
                for _diameter, _coeff in sorted(_cdcm.diameter.items()):
                    JSmod.append("MorisonDiameter_{:}.add({:1.4f} m, {:1.4f}, {:1.4f},"
                                 .format(_cdcm.name, _diameter, _coeff[0][1], _coeff[0][4]))
                    JSmod.append(" {:1.4f}, {:1.4f});\n".format(_coeff[1][1], _coeff[1][4]))
        #
        JSmod.append("//\n")
        #
        #for _cdcm in  metocean.cdcm.values(): 
        for key in sorted(metocean.cdcm, key = lambda name: metocean.cdcm[name].number):
            _cdcm = metocean.cdcm[key]
            #
            if 'specified' in _cdcm.type :
                JSmod.append("MorisonConstant_{:} = MorisonCoefficients("
                             .format(_cdcm.name))
                
                JSmod.append("{:1.4f}, {:1.4f}, {:1.4f}, {:1.4f}, {:1.4f}, {:1.4f});\n"
                             .format(*_cdcm.coefficient))
    except AttributeError:
        JSmod.append("//\n")
    #
    # Aerodynamic properties
    JSmod.append("//\n")
    JSmod.append("//\n")
    JSmod.append("//{:} Aerodynamics\n".format(70 * "-"))
    JSmod.append("//\n")
    JSmod.append("//\n")
    try:
        for key in sorted(metocean.air_drag, key = lambda name: metocean.air_drag[name].number):
            air_drag = metocean.air_drag[key]
            
            if 'specified' in air_drag.type :
                JSmod.append("AirDragConstant_{:} = AirDragConstant("
                             .format(air_drag.name))
                JSmod.append("{:1.4f}, {:1.4f});\n".format(air_drag.coefficient[1],
                                                           air_drag.coefficient[2]))
            else:
                print('fix air drag type')
        
    except AttributeError:
        JSmod.append("//\n")    
    #
    JSmod.append("//\n")
    #
    # 
    # Marine Growth
    try:
        JSmod.append("//\n")
        JSmod.append("//{:} Marine Growth\n".format(69 * "-"))
        JSmod.append("//\n")
        #
        for _mgw in  metocean.marine_growth.values():
            if _mgw.type == 'profile':
                JSmod.append("//\n")
                JSmod.append("MG_ZLevel_{:} = MarineGrowthZLevelFunction();\n"
                             .format(_mgw.name))
                
                for _layer in _mgw.profile:
                    JSmod.append("MG_ZLevel_{:}.add"
                                 .format(_mgw.name))
                    
                    JSmod.append("({: 1.3f} m, {: 1.4f} m, {: 1.4f} m, {:1.3f});\n"
                                 .format(_layer[0] * _length_factor,  # z-level
                                         _layer[1] * _length_factor,  # thickness
                                         _layer[2] * _length_factor,  # roughness
                                         _mgw.density/_mgw.sea_water_density)) # density_factors
                #
                # calculate inertia force
                if _mgw.inertia:
                    JSmod.append("MG_ZLevel_{:}.useInForceCalculations(true);\n"
                                 .format(_mgw.name))
                else:
                    JSmod.append("MG_ZLevel_{:}.useInForceCalculations(false);\n"
                                 .format(_mgw.name))                    
                #
                # absolute elevations
                if _mgw.absolute_elevations:
                    JSmod.append("MG_ZLevel_{:}.setAbsoluteElevations(true);\n"
                                 .format(_mgw.name))
                else:
                    JSmod.append("MG_ZLevel_{:}.setAbsoluteElevations(false);\n"
                                 .format(_mgw.name))
                #
                JSmod.append("MG_ZLevel_{:}.setDefault();\n"
                             .format(_mgw.name))
            # specified
            else:
                # TODO : fix this
                JSmod.append("//\n")
                #
                JSmod.append("MG_constant_{:} = MarineGrowthConstant"
                             .format(_mgw.name))
                
                JSmod.append("({:} m, {:} m, {:});\n"
                             .format(_mgw.thickness  * _length_factor, # _mgw.profile[0][1]
                                     _mgw.roughness * _length_factor, 
                                     _mgw.density_factor))
                #
                # calculate inertia force
                if _mgw.inertia:
                    JSmod.append("MG_constant_{:}.useInForceCalculations(true);\n"
                                 .format(_mgw.name))
                else:
                    JSmod.append("MG_constant_{:}.useInForceCalculations(false);\n"
                                 .format(_mgw.name))
                #
                #JSmod.append("MG_ZLevel_{:}.setDefault();\n"
                #             .format(_mgw.name))
            #
    except AttributeError:
        JSmod.append("//\n")   
    #
    # hydrodynamic diameter
    try:
        JSmod.append("//\n")
        JSmod.append("//{:} Hydrodynamic diameter\n".format(61 * "-"))
        JSmod.append("//\n")
        for key, _diameter in  metocean.hydro_diameter.items():
            JSmod.append("HydroDiameter_{:} = HydroDynamicDiameter({:1.4e} m);\n"
                         .format(_diameter.name, _diameter.coefficient))
    except AttributeError:
        JSmod.append("//\n")
    #
    #
    # Buoyancy Area
    try:
        JSmod.append("//\n")
        JSmod.append("//{:} Buoyancy Area\n".format(69 * "-"))
        JSmod.append("//\n")
        
        for key, _items in  metocean.buoyancy_area.items():
            JSmod.append("BuoyancyArea_{:} = HydroBuoyancyArea("
                         .format(_items.name))
            # non-flooded
            if not _items.coefficient[0]:
                JSmod.append("buoyancyAreaFromStructure, ")
            else:
                JSmod.append("{:1.4f} m^2, ".format(_items.coefficient[0]))
            #
            # flooded
            if not _items.coefficient[1]:
                JSmod.append("buoyancyAreaFromStructure);\n")
            else:
                JSmod.append("{:1.4f} m^2);\n".format(_items.coefficient[1]))
    except AttributeError:
        JSmod.append("//\n")
    #
    #
    # set flood meembers
    JSmod.append("//\n")
    JSmod.append("//{:} Flooded Members\n".format(67 * "-"))
    JSmod.append("//\n")    
    JSmod.append("flooding_on = Flooding(1);\n")
    JSmod.append("//\n")
    #
    return JSmod
#
#
#
#