# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports
#import os
#import sys
import math

# package imports
import fem2ufo.process.control as process
#import fem2ufo.postprocess.genie.operation.headers as headers
import fem2ufo.postprocess.genie.operation.process as js_process
import fem2ufo.postprocess.genie.model.loading as loading
import fem2ufo.postprocess.genie.model.members as member
import fem2ufo.postprocess.genie.model.sections as sections
import fem2ufo.postprocess.genie.model.materials as materials
import fem2ufo.postprocess.genie.model.joints as joints
import fem2ufo.postprocess.genie.model.rules as genie_rules
import fem2ufo.postprocess.genie.model.foundations as soil_spring
import fem2ufo.postprocess.genie.model.hydrodynamic as hydro
import fem2ufo.postprocess.genie.model.seastate as seastate
#
#
#
# ----------------------
#
#
def write_combinations(file_name, analysis, functional,
                       analysis_name, subfix):
    """
    """
    #
    sea_state_js = []
    top_load_combinations = {}
    #
    # ------ Load combinations ----
    #
    combination = analysis.load_combination
    metocean = analysis.metocean_combination
    #                 
    #if self.loads.combination:
    # load combinations level 1
    sea_state_js.extend(loading.print_load_combinations(combination[0], 
                                                        functional, 
                                                        metocean,
                                                        analysis_name,
                                                        sub=subfix, 
                                                        level=1))
    #
    #
    #sea_state_js.extend(loading.print_load_combinations_seastate(combination[0], 
    #                                                             functional, 
    #                                                             metocean,
    #                                                             analysis_name, 
    #                                                             level=0))
    #
    # Print load combinations level 1
    #
    fileJS = str(file_name) + '_LCA_' + analysis_name + '.js'
    JSmodel = open(fileJS, 'w+')
    JSmodel.write("".join(sea_state_js))
    JSmodel.write("".join(process.common.headers.print_EOF(subfix="//")))
    JSmodel.close()
    print('    * File : {:}'.format(fileJS))
    #
    #
    _total = len(combination)
    #
    if _total > 1:
        for x in range(1, _total):
            # load combinations design (top) level
            try:
                top_load_combinations[x].extend(loading.print_top_load_combinations(combination[x],
                                                                                    combination[x-1],
                                                                                    analysis_name, 
                                                                                    sub=subfix, level=x))
            except KeyError:
                top_load_combinations[x] = loading.print_top_load_combinations(combination[x],
                                                                               combination[x-1],
                                                                               analysis_name,
                                                                               sub=subfix, level=x)                            
            # combination to include seastates
            #top_load_combinations[x].extend(loading.print_top_combinations_seastate(combination[x], 
            #                                                                        combination[x-1], 
            #                                                                        metocean,
            #                                                                        analysis_name,
            #                                                                        sub=subfix, level=x))
            #
            # Print out top level load combinations
            #
            fileJS = str(file_name) + '_LCF_' + analysis_name + '.js'
            JSmodel = open(fileJS, 'w+')
            #
            #
            JSmodel.write("".join(top_load_combinations[x]))
            #
            JSmodel.write("".join(process.common.headers.print_EOF(subfix="//")))
            JSmodel.close()
            print('    * File : {:}'.format(fileJS))                            
            #
    #
    #                       
#
#
# ----------------------
#
class GeniE_js_format:
    """Class writes out fem2ufo's concept FE model in GeniE js format"""
    
    def __init__(self):
        """
        """
        self.js_model = []
        self.js_properties = []
        self.js_geometry = []
        self.js_loading = []
        self.js_foundation = []
        self.js_enviroment = []
        self.js_hydro_parameters = []
        self.js_metocean = []
        self.js_joints = []
        #
        #self.seastate = False
        self.soil = False
        self.sub =""
        self.loads = {}
        #
    #
    def model(self, model, factors, 
              rules=True, subfix=True, 
              cone_auto=False):
        """
        **Parameters**:
           :geometry: fem2ufo's geometry FE concept model
           :load : fem2ufo's loading FE concept model
           :factors : factors used for unit conversion  
                      [length, mass, time, temperature, force, pressure/stress]
        """
        
        geometry = model.component
        try:
            piles = model.foundation.piles
        except AttributeError:
            piles = {}
        
        sub = False
        if subfix:
            sub = subfix
            #sub, extension = process.common.remove_file_extension(geometry.name)
            self.sub = sub
        #
        self.js_model = process.common.headers.print_title(Type='GeniE Model', subfix="//")
        self.js_model.extend(genie_rules.print_units())
        #
        self.factors = factors
        #
        # RULES
        if rules:
            self.js_model.extend(genie_rules.print_rules())
        #
        # PROPERTIES
        self.js_properties.append('//\n')
        self.js_properties.append('//***** STRUCTURE *****//\n')
        self.js_properties.append('// Structure, level 0\n')
        self.js_properties.append('//\n')
        
        self.js_properties.extend(sections.print_properties(geometry.sections,
                                                            factors[0], sub))
        
        self.js_properties.extend(materials.print_material(geometry.materials, 
                                                           factors, sub))
        
        self.js_properties.extend(member.print_hinges(geometry.hinges, sub))
        
        # STRUCTURE
        # beam concept element
        self.js_geometry.extend(member.print_beam(geometry.concepts, 
                                                  geometry.sections,
                                                  factors[0], sub,
                                                  cone_auto))
        # plates 
        self.js_geometry.extend(member.print_shell(geometry.concepts, 
                                                   factors[0], sub))
        
        # membrane
        self.js_geometry.extend(member.print_membrane(geometry.concepts, 
                                                   factors[0], sub))
        
        #if geometry.joints:
        
        try:
            #self.js_joints = []
            self.js_joints.extend(joints.print_joints(geometry.joints, 
                                                      factors[0], sub))
            #try:
            self.js_joints.extend(joints.print_shim_joints(geometry, piles,
                                                           factors[0], sub))
            #except UnboundLocalError:
            #    pass
            #self.js_joints.extend(joints.print_shim_joints_ASAS(geometry, factors[0], sub))
        except AttributeError:
            pass
        #
        #
        self.js_geometry.extend(loading.print_mass_point(geometry.nodes, 
                                                         factors, sub))
        
        # Structure, level 1
        self.js_geometry.extend(member.print_support(geometry,
                                                     factors[0], sub))
        #
        # ENVIRONMENT
        # EQUIPMENTS
        #
        # SETS
        self.js_geometry.extend(member.print_sets(geometry.sets, 
                                                  geometry.concepts,
                                                  sub))
        #
        self.geometry = geometry
        #
    #
    def loading(self, load, 
                renumber_load=False):
        """
        Print 
        functional load cases
        Actual Load combinations
        Factored Load combinations
        """
        # LOAD MODELLING AND ANALYSIS
        self.js_loading.extend(loading.print_basic_loading(load.functional, 
                                                           self.geometry.nodes, 
                                                           self.geometry.concepts, 
                                                           self.factors, 
                                                           self.sub, renumber_load))
        #
        if load.equipment:
            self.js_loading.extend(loading.print_equipments(load.equipment,
                                                            self.factors, self.sub))
            
            self.js_loading.extend(loading.print_basic_equipment_loading(load,
                                                                         self.factors, self.sub))
            
            self.js_loading.extend(loading.print_load_interface(load.equipment, 
                                                                #self.geometry.joints,
                                                                self.factors, self.sub))
        #
        self.loads = load
        #
    #
    def foundation(self, foundation, factors=False):
        """
        **Parameters**:
           :foundation: fem2ufo's foundation FE concept model
           :factors : factors used for unit conversion   
                      [length, mass, time, temperature, force, pressure/stress]
        """
        if not factors:
            factors = self.factors
        #
        self.js_foundation.extend(soil_spring.print_piles(foundation.piles, 
                                                          factors,
                                                          self.sub))        
        #
        self.js_foundation.extend(soil_spring.print_pytzqz(foundation.soil, 
                                                           factors))
        #
        #self.js_enviroment.extend(location.print_enviroment(foundation.soil, 
        #                                                    factors))
        #
        _zcoord = []
        for _name, _item in sorted(foundation.piles.sets.items()):
            if 'end_nodes' in _item.name:
                _node_number = _item.nodes[0]
                _coord = foundation.piles.nodes[_node_number].z
                _zcoord.append(abs(_coord)) # coordinate[2]
        #
        _zcoord = max(_zcoord)
        self.pile_head = math.copysign(_zcoord, _coord)
        #
        self.soil = foundation.soil
    #
    def metocean(self, metocean, factors=False):
        """
        **Parameters**:
           :metocean: fem2ufo's metocean data 
           :factors : factors used for unit conversion   
                     [length, mass, time, temperature, force, pressure/stress]
        """
        #
        if not factors:
            factors = self.factors
        #
        self.js_hydro_parameters.extend(hydro.print_hydro_parameters(metocean, factors))
        #
        #self.seastate = metocean
        #
    #
    #
    def print_model(self, file_name):
        """
        Write out fem2ufo FE concept model in GeniE js format
        
        **Parameters**:
           :file_name: output file name
        """
        #
        print('--- Writing GeniE model files')
        #
        #
        # print model
        #
        fileJS = str(file_name) + '.js'
        #
        JSmodel = open(fileJS, 'w+')
        #
        JSmodel.write("".join(self.js_model))
        #
        JSmodel.write("".join(self.js_properties))
        #
        JSmodel.write("".join(self.js_hydro_parameters))
        #
        JSmodel.write("".join(self.js_geometry))
        JSmodel.write("".join(self.js_foundation))
        JSmodel.write("".join(self.js_joints))
        #
        JSmodel.write("".join(self.js_loading))
        #
        JSmodel.write("".join(self.js_metocean))
        JSmodel.write("".join(self.js_enviroment))
        #
        #JSmodel.write("".join(self.js_conditions))
        #
        #
        JSmodel.write("".join(js_process.print_simplify_model()))
        JSmodel.write("".join(process.common.headers.print_EOF(subfix="//")))
        JSmodel.close()
        #
        print('    * File : {:}'.format(fileJS))
        #        
        #
    #
    def print_analysis(self, analysis, metocean,
                       file_name=False, factors=False):
        """
        """
        #
        if not file_name:
            file_name = analysis.name
            file_name = file_name.split('.')
            file_name = file_name[0] + '_'
        else:
            file_name += '_'
        #
        try:
            non_hydro = metocean.non_hydro.name
        except AttributeError:
            non_hydro = None
        #
        if not factors:
            factors = self.factors        
        #
        # print locations + sea state load combination
        #
        self.top_load_combinations = {}
        
        #if self.seastate:
        for _name, _analysis in analysis.case.items():
            #
            js_conditions = []
            #sea_state_js = []
            #           
            #
            #_combination = _conditions.combination
            #
            if _analysis.metocean_combination:
                #
                # TODO: quick units fix
                water_depth = [round(_depth * factors[0], 3)  for _depth in _analysis.condition.water_depth]
                mudline = round(_analysis.condition.mudline * factors[0], 3)
                surface = round(_analysis.condition.surface * factors[0], 3)                
                #
                #
                # wind 
                try:
                    js_conditions.extend(seastate.print_wind_profiles(_analysis.metocean_combination,
                                                                      factors,
                                                                      seastate_name=_name))
                    #
                    # Areas - Equipment
                    #
                    #if metocean.wind:
                    js_conditions.extend(loading.print_equipment_wind_areas(_analysis.metocean_combination, # self.loads.functional,
                                                                            seastate_name=_name))
                    #js_conditions.extend(loading.print_load_interface(self.loads.equipment, 
                    #                                                  self.geometry.joints,
                    #                                                  factors))
                except AttributeError:
                    pass
                # current
                try:
                    js_conditions.extend(seastate.print_current_profiles(_analysis.metocean_combination,
                                                                         factors,
                                                                         seastate_name=_name))
                except AttributeError:
                    pass
                #
                # wave sets
                try:
                    js_conditions.extend(seastate.print_wave_sets(_analysis.metocean_combination,
                                                                  factors, 
                                                                  seastate_name=_name))
                except AttributeError:
                    pass                    
                #
                # ENVIRONMENT
                #
                try:
                    _analysis.condition
                    
                    if self.soil:
                        js_conditions.extend(seastate.print_enviroment_soil(surface, 
                                                                            mudline,  # self.pile_head,
                                                                            self.soil, 
                                                                            factors,
                                                                            seastate_name=_name))
                    else:
                        js_conditions.extend(seastate.print_enviroment_no_soil(surface,
                                                                               mudline,  # self.pile_head, 
                                                                               factors,
                                                                               seastate_name=_name))
                #
                except AttributeError:
                    pass
                #
                # CONDITIONS
                #
                js_conditions.extend(seastate.print_conditions(_analysis.metocean_combination,
                                                               seastate_name=_name))
                #
                # ANALYSIS
                #
                pile_boundary=False
                if self.soil:
                    pile_boundary=True
                #
                js_conditions.extend(seastate.print_analysis_metocean(_analysis.metocean_combination, 
                                                                      water_depth, mudline, surface,
                                                                      seastate_name=_name,
                                                                      non_hydro = non_hydro,
                                                                      pile_boundary=pile_boundary))

            #
            # No metocean data
            else:
                if self.soil:
                    mudline = _analysis.condition.mudline
                    js_conditions.extend(seastate.print_no_conditions(_name))
                    js_conditions.extend(seastate.print_soil_no_enviroment(self.soil, 
                                                                           mudline, 
                                                                           self.factors,
                                                                           seastate_name=_name))
                else:
                    js_conditions.extend(seastate.print_analysis_no_metocean(_name))
            #
            #
            analysis_name = _name.strip() 
            #
            # Print analysis
            #
            fileJS = file_name + 'metocean_' + analysis_name  + '.js'
            JSmodel = open(fileJS, 'w+')
            JSmodel.write("".join(js_conditions))
            JSmodel.write("".join(process.common.headers.print_EOF(subfix="//")))
            JSmodel.close()
            print('    * File : {:}'.format(fileJS))
            #
            #
        #
        #
        #
        #
        #else:
            #
            #js_conditions = []
            #
            #analysis_name = 'Analysis_1'
            #
            #if self.soil:
                
                #js_conditions.extend(seastate.print_soil_no_enviroment(self.soil, 
                #                                                       self.pile_head, 
                #                                                       self.factors))
                ##js_conditions.extend(seastate.print_no_conditions())
                #
                #fileJS = str(file_name) + '_seastate.js' # + analysis_name + '.js'
                #JSmodel = open(fileJS, 'w+')
                #JSmodel.write("".join(js_conditions))
                #JSmodel.write("".join(process.common.headers.print_EOF(subfix="//")))
                #JSmodel.close()
                #print('    * File : {:}'.format(fileJS))
                #                
            #
            #else:
            #    print(' fix here no soil')            
            #
        #
    #
    def print_load_combinations(self, analysis, file_name=False):
        """
        """
        #
        if not file_name:
            file_name = self.sub        
        #
        # ------ Load combinations ----
        #        
        for _name, _analysis in analysis.case.items():
            analysis_name = _name.strip() 
            if _analysis.load_combination:
                write_combinations(file_name, 
                                   _analysis, # .load_combination,
                                   self.loads.functional,
                                   analysis_name, self.sub)
        #        
    #
    def print_buckling_factors(self, file_name):
        """
        """
        # print model
        #
        fileJS = str(file_name) + '_buckling_factors.js'
        #
        JSmodel = open(fileJS, 'w+')
        #
        _prefix='Bm'
        # normal beam
        for _beam in self.model.elements.values():
    
            if _beam.type != 'beam':
                continue
            
            JSmodel.write("".join(member.print_buckling_factors(_beam, self.factors[0],
                                                                _prefix, self.sub)))
        #
        JSmodel.write("".join(process.common.headers.print_EOF(subfix="//")))
        JSmodel.close()
    #
#