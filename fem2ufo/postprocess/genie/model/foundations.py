# 
# Copyright (c) 2009-2015 fem2ufo
# 
import math

# Python stdlib imports


# package imports
import fem2ufo.process.control as process
import fem2ufo.postprocess.genie.model.members as member
import fem2ufo.postprocess.genie.model.sections as sections
import fem2ufo.postprocess.genie.model.materials as materials

# Foundation
#
def print_pytzqz(foundation, factors):
    #
    header = 'Soil Data and Soil Curves'
    JSmod = process.common.headers.print_head_line(header, subfix="//")    
    #     
    JSmod.append("//\n")
    JSmod.append("//\n")
    JSmod.append("Soil_default = Sand(false, 1, 2000 Kg/m^3, 30 deg);\n")
    JSmod.append("Soil_default.frictionRatio = 0;\n")
    JSmod.append("Soil_default.zDisplacement = 1 m;\n")
    JSmod.append("SoilClay = Clay(false, 1, 1927.77 Kg/m^3, 0.5, 200000 Pa, 0 m, 200000 Pa, -100 m);\n")
    JSmod.append("//\n")
    JSmod.append("//\n")
    #
    #
    for _soil in foundation.values():
        #
        JSmod.append("//\n")
        JSmod.append("// Soil Name : {:}\n".format(_soil.name))
        JSmod.append("//\n")
        #
        for key in sorted(_soil.layers, key = lambda name: _soil.layers[name].depth):
            _layer = _soil.layers[key]
            JSmod.append("//\n")
            JSmod.append("SCurve_{:}_{:} = SoilCurves(pyManual, tzManual, qzManual);\n"
                         .format(_soil.name, _layer.number))             
            
            JSmod.extend(print_spring_data(_soil.name, _layer.number, 
                                           _soil.diameter, _layer.curve, 
                                           factors))
        #
        JSmod.append("//\n")
    
    JSmod.append("//\n")
    JSmod.append("SoilData_1 = SoilData(0 Pa, 0.5, 0 Pa, 0 Pa, 0, 0 Pa, 0);\n")
    JSmod.append("//\n")
    return JSmod
#
def get_spring(_spring, unit_stress_factor=1,
               length_factor=1):
    """
    """
    _force = _spring.force
    _disp = _spring.displacement    
    _steps = len(_force)
    #if _steps > 12:
    #    print('here')
    #
    # displacement
    #
    JSmod = []
    JSmod.append("Array(")
    
    if "qz" in _spring.name.lower():
        JSmod.append("{:1.4e} m, "
                     .format(-1.0))
        JSmod.append("{:1.4e} m, ".format(0))
    else:
        JSmod.append("{:1.4e} m, "
                     .format(0))
    #
    _top = 0
    for x in range(_steps - 1):
        if _force[x] <= 0.0 and _disp[x] <= 0.0 :
            continue
        
        if _top == _disp[x]:
            continue
        
        JSmod.append("{:1.4e} m, "
                     .format(_disp[x] * length_factor))
        
        _top = _disp[x]
    # end
    if _top != _disp[_steps-1]:
        JSmod.append("{:1.4e} m), "
                     .format(_disp[_steps-1] * length_factor))
    else:
        JSmod.append("), ")
    #
    # Stress
    #
    JSmod.append("Array(")
    JSmod.append("{:1.4e} Pa, ".format(0))
    if "qz" in _spring.name.lower():
        JSmod.append("{:1.4e} Pa, ".format(0))
    #
    _top = 0
    for x in range(_steps-1):
        if _force[x] <= 0.0 and _disp[x] <= 0.0 :
            continue
        
        if _top == _disp[x]:
            continue        
        
        JSmod.append("{:1.4e} Pa, "
                     .format(_force[x] * unit_stress_factor))
        
        _top = _disp[x]
    # end
    if _top != _disp[_steps-1]:
        JSmod.append("{:1.4e} Pa));\n"
                     .format(_force[_steps-1] * unit_stress_factor))
    else:
        JSmod.append("));\n")    
    #
    return JSmod
#
#
def get_springTZ(_spring, unit_stress_factor=1,
                 length_factor=1):
    """
    """
    
    _force = _spring.force
    _disp = _spring.displacement    
    _steps = len(_force)
    #
    # displacement
    #
    JSmod = []
    JSmod.append("Array(")
    #
    _top = 0
    for x in range(_steps - 1):
        if _top == _disp[x]:
            continue
        JSmod.append("{:1.4e} m, "
                     .format(_disp[x] * length_factor))
        _top = _disp[x]
    # end
    if _top != _disp[_steps-1]:
        JSmod.append("{:1.4e} m), "
                     .format(_disp[_steps-1] * length_factor))
    else:
        JSmod.append("), ")
    #
    # Stress
    #
    JSmod.append("Array(")
    #
    _top = 0
    for x in range(_steps-1):
        if _top == _disp[x]:
            continue        
        JSmod.append("{:1.4e} Pa, "
                     .format(_force[x] * unit_stress_factor))
        _top = _disp[x]
    # end
    if _top != _disp[_steps-1]:
        JSmod.append("{:1.4e} Pa));\n"
                     .format(_force[_steps-1] * unit_stress_factor))
    else:
        JSmod.append("));\n")    
    #
    return JSmod
#
#
def print_spring_data(soil_name, layer_no, _diameter, _layer, factors):
    """
    """
    # default force units is meter*second^-2*gram
    # to convert to meter*second^-2*kilogram : 
    if factors[4] == 1:
        _force_factor = 1
    else:
        _force_factor = factors[4]  / 1000.0
    # need to fix gravity units
    _gravity_unit = factors[0]
    _length_factor = factors[0]
    #
    JSmod = []
    #
    # PY
    #
    _spring = _layer['PY']
    unit_stress_factor = _force_factor / (_diameter * _length_factor)
    
    JSmod.append("SCurve_{:}_{:}.addManualPY({:1.3f} m, "
                 .format(soil_name, layer_no, 
                         _diameter * _length_factor))
    #
    JSmod.extend(get_spring(_spring, 
                            unit_stress_factor,
                            _length_factor))
    #
    # TZ
    #
    _spring = _layer['TZ']
    unit_stress_factor = _force_factor / (_diameter * _length_factor * math.pi)
    
    JSmod.append("SCurve_{:}_{:}.addManualTZ({:1.3f} m, "
                 .format(soil_name, layer_no, 
                         _diameter * _length_factor))
    #
    JSmod.extend(get_springTZ(_spring, 
                              unit_stress_factor,
                              _length_factor))
    #
    # QZ
    #
    try:
        _spring = _layer['QZ']
        unit_stress_factor = (4.0 * _force_factor) / ((_diameter * _length_factor)**2 * math.pi)
        
        JSmod.append("SCurve_{:}_{:}.addManualQZ({:1.3f} m, "
                     .format(soil_name, layer_no, 
                             _diameter * _length_factor))
        #
        JSmod.extend(get_spring(_spring, 
                                unit_stress_factor,
                                _length_factor))
    except KeyError:
        JSmod.append("SCurve_{:}_{:}.addManualQZ({:1.3f} m, Array(0 m, 0.01 m), Array(0 Pa, 0 Pa));\n"
                     .format(soil_name, layer_no, 
                             _diameter * _length_factor))
    #
    return JSmod


#
def print_piles(piles, factors, subfix):
    """
    """
    #
    sub = ""
    sub2 = "Pile"    
    if subfix:
        sub = str(subfix) + "_"
        sub2 = str(subfix) + "_Pile"
    
    #
    header = 'FOUNDATIONS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    JSmod.append("//\n")
    JSmod.append("//Pile Characteristics\n")       
    JSmod.append("//\n")
    JSmod.append("pile_characteristics_default = PileCharacteristics(0 Kg/m^3, tcFree);\n")
    JSmod.append("\n")
    #
    JSmod.extend(sections.print_properties(piles.sections, factors[0], sub2))
    JSmod.extend(materials.print_material(piles.materials, factors, sub2))
    #
    header = 'beam'
    JSmod.extend(process.common.headers.print_head_line(header, subfix="//"))
    for _pile in piles.concepts.values():
        JSmod.extend(member.get_beam(sub, _pile, factor=factors[0],
                                     cone_auto=False))
    #
    JSmod.append("//\n")
    return JSmod
#
#
    
