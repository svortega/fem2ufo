# 
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import re
from math import floor, ceil

# package imports
import fem2ufo.process.control as process

def roundUp(n, d=8):
    d = int('1' + ('0' * d))
    return ceil(n * d) / d
#
def roundDown(n, d=8):
    d = int('1' + ('0' * d))
    return floor(n * d) / d
#
def print_properties(section, factor=1, subfix=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    #
    header = 'PROPERTIES'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    #
    JSmod.extend(print_tubular(section, factor, sub))
    JSmod.extend(print_ibeam(section, factor, sub))
    JSmod.extend(print_box(section, factor, sub))
    JSmod.extend(print_channel(section, factor, sub))
    JSmod.extend(print_angle(section, factor, sub))
    JSmod.extend(print_bar(section, factor, sub))
    JSmod.extend(print_general(section, factor, sub))
    JSmod.extend(print_cone(section, factor, sub))
    JSmod.extend(print_shell(section, factor, sub))
    #
    return JSmod
    #
#
def print_tubular(section, factor, sub):
    """
    """
    JSmod = []
    JSmod.append('// \n')
    JSmod.append('// Tubular Section \n')
    JSmod.append('// \n')    
    for key, _sec in sorted(section.items()):
        if re.search(r"\btub(ular)?|pipe\b", _sec.shape, re.IGNORECASE):
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            JSmod.append('PipeSection({:1.4f} m, {:1.4f} m); // {:}\n'
                         .format(_sec.properties.D * factor, 
                                 _sec.properties.Tw * factor,
                                 key))
    #
    return JSmod
#
def print_ibeam(section, factor, sub):
    """
    """
    JSmod = []
    JSmod.append('// \n')
    JSmod.append('// I Section \n')
    JSmod.append('// \n')
    for key, _sec in sorted(section.items()):
        if re.search(r"\bi(\s*beam|section)?\b", _sec.shape, re.IGNORECASE):
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            if _sec.properties.Bfb != _sec.properties.Bft \
               or _sec.properties.Tfb != _sec.properties.Tft:
                JSmod.append('UnsymISection({:1.4f} m, {:1.4f} m, {:1.4f} m, {:1.5f} m, {:1.4f} m, {:1.4f} m, {:1.5f} m, {:1.4f} m); // {:}\n'
                             .format(_sec.properties.D * factor,
                                     _sec.properties.Tw * factor,
                                     _sec.properties.Bft * factor,
                                     roundUp(_sec.properties.Bft * factor, 4) * 0.50, 
                                     _sec.properties.Tft * factor,
                                     _sec.properties.Bfb * factor,
                                     roundUp(_sec.properties.Bfb * factor, 4) * 0.50, 
                                     _sec.properties.Tfb * factor,
                                     key))
            else:
                JSmod.append('ISection({:1.4f} m, {:1.4f} m, {:1.4f} m, {:1.4f} m); // {:}\n'
                             .format(_sec.properties.D * factor, 
                                     _sec.properties.Bft * factor,
                                     _sec.properties.Tw * factor, 
                                     _sec.properties.Tft * factor,
                                     key))
        
        elif re.search(r"\b(unsymetric(\s*i\s*beam|section)?|open\s*thin\s*walled)\b", _sec.shape, re.IGNORECASE):
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            _flange_width_top = max(_sec.properties.Bft, _sec.properties.Tw*0.50)
            _flange_width_bottom = max(_sec.properties.Bfb, _sec.properties.Tw*0.50)
            JSmod.append('UnsymISection({:1.6f} m, {:1.6f} m, {:1.6f} m, {:1.6f} m, {:1.6f} m, {:1.6f} m, {:1.6f} m, {:1.6f} m); // {:}\n'
                    .format(_sec.properties.D * factor, 
                            _sec.properties.Tw * factor,
                            _flange_width_top* factor, 
                            (_flange_width_top / 2.0) * factor, 
                            _sec.properties.Tft * factor,
                            _flange_width_bottom * factor, 
                            (_flange_width_bottom / 2.0) * factor, 
                            _sec.properties.Tfb * factor,
                            key))
        
        elif re.search(r"\b(t(ee)?(\s*section)?)\b", _sec.shape, re.IGNORECASE):
            JSmod.append('// Tee section \n')
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            _flange_width_top = max(_sec.properties.Bft, _sec.properties.Tw*0.50)
            JSmod.append(
                'UnsymISection({:} m, {:1.6f} m, {:1.6f} m, {:1.6f} m, {:1.6f} m, {:1.6f} m, {:1.6f} m, {:1.6f} m);\n'
                    .format(_sec.properties.D * factor, 
                            _sec.properties.Tw * factor,
                            _flange_width_top * factor, 
                            (_flange_width_top / 2.0) * factor, 
                            _sec.properties.Tft * factor,
                            _sec.properties.Tw * factor, 
                            (_sec.properties.Tw / 2.0) * factor, 
                            _sec.properties.Tw * factor))        
    #
    return JSmod
#
def print_shell(section, factor, sub):
    """
    """
    JSmod = []
    JSmod.append('// \n')
    JSmod.append('// Shell & Membrane \n')
    JSmod.append('// \n')    
    for key, _sec in sorted(section.items()):
        if re.search(r"\b(plate|shell)\b", _sec.shape, re.IGNORECASE):
            JSmod.append('// Shell {:}\n'.format(key))
            JSmod.append('{:}{:} = Thickness({:});\n'
                         .format(sub, _sec.name, _sec.properties.D* factor))
        elif re.search(r"\b(membrane)\b", _sec.shape, re.IGNORECASE):
            JSmod.append('// Membrane {:}\n'.format(key))
            JSmod.append('{:}{:} = Thickness({:});\n'
                         .format(sub, _sec.name, _sec.properties.D* factor))        
    #
    return JSmod
#
def print_box(section, factor, sub):
    """
    """
    JSmod = []
    JSmod.append('// \n')
    JSmod.append('// Box Section \n')
    JSmod.append('// \n')    
    for key, _sec in sorted(section.items()):
        if re.search(r"\bbox\b", _sec.shape, re.IGNORECASE):
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            JSmod.append('BoxSection({:1.4f} m, {:1.4f} m, {:1.4f} m, {:1.4f} m); // {:}\n'
                         .format(_sec.properties.D * factor, 
                                 _sec.properties.Bft * factor,
                                 _sec.properties.Tw * factor, 
                                 _sec.properties.Tft * factor,
                                 key))
    #
    return JSmod
#
def print_channel(section, factor, sub):
    """
    """
    JSmod = []
    JSmod.append('// \n')
    JSmod.append('// Channel Section \n')
    JSmod.append('// \n')    
    for key, _sec in sorted(section.items()):
        if re.search(r"\bchannel\b", _sec.shape, re.IGNORECASE):
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            JSmod.append('ChannelSection({:1.4f} m, {:1.4f} m, {:1.4f} m, {:1.4f} m); // {:}\n'
                         .format(_sec.properties.D * factor, 
                                 _sec.properties.Bft * factor,
                                 _sec.properties.Tw * factor, 
                                 _sec.properties.Tft* factor,
                                 key))
    #
    return JSmod
#
def print_angle(section, factor, sub):
    """
    """
    JSmod = []
    JSmod.append('// \n')
    JSmod.append('// Angle Section \n')
    JSmod.append('// \n')    
    for key, _sec in sorted(section.items()):
        if re.search(r"\b(angle|l)(\s*beam|section)?\b", _sec.shape, re.IGNORECASE):
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            JSmod.append('LSection({:1.4f} m, {:1.4f} m, {:1.4f} m, {:1.4f} m); // {:}\n'
                         .format(_sec.properties.D * factor, 
                                 _sec.properties.Bft * factor,
                                 _sec.properties.Tw * factor, 
                                 _sec.properties.Tft * factor,
                                 key))
    #
    return JSmod
#
def print_bar(section, factor, sub):
    """
    """
    JSmod = []
    JSmod.append('// \n')
    JSmod.append('// Rectangular Bar Section \n')
    JSmod.append('// \n')    
    for key, _sec in sorted(section.items()):
        if re.search(r"\b(rectangular|square\s*bar)\b", _sec.shape, re.IGNORECASE):
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            JSmod.append('BarSection({:1.4f} m, {:1.4f} m);\n'
                         .format(_sec.properties.D * factor, 
                                 _sec.properties.Tw * factor))
    #
    return JSmod
#
def print_general(section, factor, sub):
    """
    """
    JSmod = []
    JSmod.append('// \n')
    JSmod.append('// General Beam Section \n')
    JSmod.append('// \n')    
    for key, _sec in sorted(section.items()):
        if re.search(r"\b(general\s*section)\b", _sec.shape, re.IGNORECASE):
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            JSmod.append(
                'GeneralSection({:1.4e} m^2, {:1.4e} m^4, {:1.4e} m^4, {:1.4e} m^4, {:1.4e} m^4, {:1.4e} m^3, {:1.4e} m^3, {:1.4e} m^3,'
                    .format(_sec.properties.Area * factor**2,
                            _sec.properties.J * factor**4, 
                            _sec.properties.Iip * factor**4, 
                            _sec.properties.Iop * factor**4,
                            _sec.properties.Iyz * factor**4,
                            _sec.properties.Zeip * factor**3, 
                            _sec.properties.Zeop * factor**3, 
                            _sec.properties.ZeJ * factor**3))

            JSmod.append(' {:} m^2, {:1.4e} m^2, {:1.4e} m, {:1.4e} m, {:1.4e} m^3, {:1.4e} m^3); \n'
                         .format(_sec.properties.shearAreaV * factor**2, 
                                 _sec.properties.shearAreaH * factor**2,
                                 _sec.properties.SCV * factor, 
                                 _sec.properties.SCH * factor,
                                 _sec.properties.SV * factor**3, 
                                 _sec.properties.SH * factor**3))
    #
    return JSmod
#
def print_cone(section, factor, sub):
    """
    """
    JSmod = []
    JSmod.append('// \n')
    JSmod.append('// Cone Section \n')
    JSmod.append('// \n')    
    for key, _sec in sorted(section.items()):
        if re.search(r"\b(cone)\b", _sec.shape, re.IGNORECASE):
            JSmod.append('// Cone section to tubular \n')
            JSmod.append('{:}{:} = '.format(sub, _sec.name))
            JSmod.append('PipeSection({:1.4f} m, {:1.4f} m); // {:}\n'
                         .format(_sec.properties.Bft * factor, 
                                 _sec.properties.Tft * factor,
                                 key))
    #
    return JSmod
#