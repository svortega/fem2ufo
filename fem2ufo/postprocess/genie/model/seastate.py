# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import math
import re

# package imports

#
def select(lineIn, key, keyWord="N/A", count=1):
    """
    match
    """
    #
    lineOut = lineIn
    _match = False
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.match(lineIn)

        if keys:
            keyWord = _key
            lineOut = re.sub(keys.group(), " ", lineIn, count)
            _match = True

    lineOut = lineOut.strip()
    return keyWord, lineOut, _match
#
def get_wave_theory(lineIn):
    """
    """
    _key = {"Stokes5": r"\b(stoke(s)?(\s*(\_)?5((\s')?th)?|fifth\s*(order)?)?)\b",
            "Airy": r"\b(airy(\s*(\_)?linear|extrapolated|extretched)?)\b",
            "Cnoidal": r"\b(cnoidal)\b",
            "CalmSea": r"\b(calm\s*(\_)?sea)\b",
            "StreamFunction": r"\b(stream(\s*function)?(\s*theory)?(\_8)?)\b",
            "user_defined": r"\b(user(\s*defined)?)\b"}

    keyWord, lineOut, _match = select(lineIn, _key)
    #
    return keyWord
#
def get_buoyancy(lineIn):
    """
    """
    _key = {"buOff": r"\b((bu\s*)?off)|0\b",
            "buOn": r"\b((bu\s*)?on)|1\b",
            "buOnly": r"\b((bu\s*)?only)\b",
            "mgWeight": r"\b((mg\s*)?weight)\b"}

    keyWord, lineOut, _match = select(str(lineIn), _key)
    #
    return keyWord
#
#
def print_enviroment_soil(sea_surface, mudline, foundation,
                          factors, seastate_name=None,
                          tolerance=0.1):
    """
    """
    if  seastate_name:
        seastate_name = seastate_name.strip()
    else:
        seastate_name = "1"
    
    _length_factor = factors[0]
    
    JSmod = []
    JSmod.append("//\n")
    JSmod.append("//***** ENVIRONMENT *****//\n")
    JSmod.append("//\n")
    JSmod.append("//Locations\n")
    JSmod.append("//\n")
    
    _step = 0
    for _name in sorted(foundation, key = lambda name: foundation[name].number):
    #for _number, _soil in foundation.items():
        _soil = foundation[_name]
        # FIXME : quick fix
        _step += 1
        if _step > 1:
            continue
        #
        # water surface & mudline
        JSmod.append("Location_{:} = Location({: 1.3f} m, {: 1.3f} m);\n"
                     .format(seastate_name, sea_surface * _length_factor, 
                             mudline * _length_factor))
                    #.format(seastate_name, sea_surface * _length_factor, 
                    #        math.copysign(_soil.mudline, mudline) * _length_factor))
        
        JSmod.append("Location_{:}.gravity = 9.80665 m/s^2;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.air().density = 1.226 Kg/m^3;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.air().kinematicViscosity = 1.462e-005 m^2/s;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.water().density = 1030 Kg/m^3;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.water().kinematicViscosity = 1.899e-006 m^2/s;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.seabed().normaldirection = Vector3d(0 m,0 m,1 m);\n"
                     .format(seastate_name))
        #
        #
        _depth = {(abs(mudline) + _layer.depth) : key
                  for key, _layer in sorted(_soil.layers.items())}
        #
        # find soil borders
        _borders = [_key for _key in _depth.keys()]
        #
        _borders = sorted(_borders)
        _borders.insert(0, abs(mudline))
        #
        # remove soil thikcness < tolerance
        #_tb_removed = []
        #for x in range(1, len(_borders)):
        #    _thickness = abs(_borders[x] - _borders[x-1])
        #    if _thickness <= tolerance:
        #        _tb_removed.append(_borders[x])
        #
        #for _item in _tb_removed:
        #    _borders.remove(_item)
        #
        #
        JSmod.append("//\n")
        for _border in _borders[1:]:
            JSmod.append("Location_{:}.insertSoilBorder({: 1.3f} m);\n"
                         .format(seastate_name,
                                 math.copysign(_border, mudline) * _length_factor))
        #
        #       
        JSmod.append("//\n")
        #
        _step = 0
        _diam = _soil.diameter
        for x in range(1, len(_borders)):
            _thickness = abs(_borders[x] - _borders[x-1])
            _sublayer = max(int(_thickness/_diam), 1)
            _name_layer = _depth[_borders[x]]
            _layer = _soil.layers[_name_layer]
            _step += 1
            JSmod.append("Location_{:}.soil({:}).soilCurves = SCurve_{:}_{:};\n"
                         .format(seastate_name, _step, _soil.name, _layer.number))
            JSmod.append("Location_{:}.soil({:}).soilData = SoilData_1;\n"
                         .format(seastate_name, _step))
            JSmod.append("Location_{:}.soil({:}).soilType = Soil_default;\n"
                         .format(seastate_name, _step))
            JSmod.append("Location_{:}.soil({:}).numberOfSublayers = {:};\n"
                         .format(seastate_name, _step, _sublayer))
        #
        #
    #
    JSmod.append("//\n")
    JSmod.append("//\n")
    #
    return JSmod
#
#
def print_enviroment_no_soil(sea_surface, mudline,
                             factors, seastate_name=None):
    """
    """
    #
    if  seastate_name:
        seastate_name = seastate_name.strip()
    else:
        seastate_name = "1"   
    #
    _length_factor = factors[0]
    
    JSmod = []
    JSmod.append("//\n")
    JSmod.append("//***** ENVIRONMENT *****//\n")
    JSmod.append("//\n")
    JSmod.append("//Locations\n")
    JSmod.append("//\n")    
    #
    JSmod.append("Location_{:} = Location({: 1.3f} m, {: 1.3f} m);\n"
                 .format(seastate_name, sea_surface * _length_factor,
                         mudline * _length_factor))
    JSmod.append("Location_{:}.gravity = 9.80665 m/s^2;\n"
                 .format(seastate_name))
    JSmod.append("Location_{:}.air.density = 1.226 Kg/m^3;\n"
                 .format(seastate_name))
    JSmod.append("Location_{:}.air.kinematicViscosity = 1.462e-005 m^2/s;\n"
                 .format(seastate_name))
    JSmod.append("Location_{:}.water.density = 1025 Kg/m^3;\n"
                 .format(seastate_name))
    JSmod.append("Location_{:}.water.kinematicViscosity = 1.19e-006 m^2/s;\n"
                 .format(seastate_name))
    JSmod.append("Location_{:}.seabed.normaldirection = Vector3d(0 m,0 m,1 m);\n"
                 .format(seastate_name))
    JSmod.append("Location_{:}.relativeSoilLayers = false;\n"
                 .format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("//\n")
    #
    return JSmod
#
#
def print_soil_no_enviroment(foundation, mudline, factors,
                             seastate_name=None):
    """
    """
    #
    if  seastate_name:
        seastate_name = seastate_name.strip()
    else:
        seastate_name = "1"  
    #
    _length_factor = factors[0]
    set_no = 1
    
    JSmod = []
    JSmod.append("//\n")
    JSmod.append("//***** ENVIRONMENT *****//\n")
    JSmod.append("//\n")
    JSmod.append("//Locations\n")
    JSmod.append("//\n")
    #
    _step = 0
    for _number in sorted(foundation, key = lambda name: foundation[name].number):
    #for _number, _soil in foundation.items():
        _soil = foundation[_number]
        # FIXME: quick fix
        _step += 1
        if _step > 1:
            continue
            
        _sign = math.copysign(0.01, mudline)
        sea_surface = round(mudline + (mudline * _sign))
        
        JSmod.append("Location_{:} = Location({: 1.3f} m, {: 1.3f} m);\n"
                     .format(seastate_name, 
                             sea_surface  * _length_factor, 
                             mudline  * _length_factor))
        JSmod.append("Location_{:}.gravity = 9.80665 m/s^2;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.air.density = 1.226 Kg/m^3;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.air.kinematicViscosity = 1.462e-005 m^2/s;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.water.density = 1025 Kg/m^3;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.water.kinematicViscosity = 1.19e-006 m^2/s;\n"
                     .format(seastate_name))
        JSmod.append("Location_{:}.seabed.normaldirection = Vector3d(0 m,0 m,1 m);\n"
                     .format(seastate_name))
        #
        #
        _borders = [(abs(mudline) + _layer.depth)
                    for _layer in _soil.layers.values()]
        _borders = sorted(_borders)        
        
        JSmod.append("//\n")
        
        for _border in _borders:
            JSmod.append("Location_{:}.insertSoilBorder({: 1.3f} m);\n"
                         .format(seastate_name,
                                 math.copysign(_border, mudline) * _length_factor))
        
        JSmod.append("//\n")
        #
        _step = 0
        for _step in range(1, len(_borders)+1):
            
            JSmod.append("Location_{:}.soil({:}).soilCurves = SoilCurves_{:};\n"
                         .format(seastate_name, _step, _step))
            JSmod.append("Location_{:}.soil({:}).soilData = SoilData_1;\n"
                         .format(seastate_name, _step))
            JSmod.append("Location_{:}.soil({:}).soilType = Soil_default;\n"
                         .format(seastate_name, _step))
            JSmod.append("Location_{:}.soil({:}).numberOfSublayers = 1;\n"
                         .format(seastate_name, _step))        
    #
    #
    JSmod.append("//\n")
    JSmod.append("//\n")
    #
    return JSmod
#
#
def print_conditions(_metocean, seastate_name):
    """
    """
    #
    seastate_name = seastate_name.strip() 
    #
    JSmod = []
    #
    JSmod.append("//\n")
    JSmod.append("//Conditions\n")
    JSmod.append("//\n")
    #
    JSmod.append("Condition_{:} = DeterministicTime(Location_{:});\n"
                 .format(seastate_name, seastate_name))      
    #
    JSmod.append("Condition_{:}.waterSurface().regularWaveSet = Wave_{:};\n"
                 .format(seastate_name, seastate_name))
    
    JSmod.append("Condition_{:}.water().setNoCurrent();\n"
                 .format(seastate_name))
    
    JSmod.append("Condition_{:}.populate();\n"
                 .format(seastate_name))
    JSmod.append("//\n")
    #
    #
    #
    #for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        #_seastate = _metocean[key]
        #_wave = _seastate.wave
        ##
        #if 'calm' in _wave.theory.lower():            
            #JSmod.append("Condition_{:}.addCalmSea();\n"
            #             .format(seastate_name))
    #
    #
    i = 0
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        _wave = _seastate.wave
        _waveTheory = get_wave_theory(_wave.theory)
        
        if 'calm' in _waveTheory.lower():
            continue        
        #
        i += 1
        #
        # wave
        if 'stream' in _waveTheory.lower():
            JSmod.append("Condition_{:}.component({:}).waterSurface.waveModel({:}({:}));\n"
                         .format(seastate_name, i, _waveTheory, int(_wave.order)))
        else:
            JSmod.append("Condition_{:}.component({:}).waterSurface.waveModel({:}());\n"
                         .format(seastate_name, i, _waveTheory))
        #
        # current
        if _seastate.current:
            JSmod.append("Condition_{:}.component({:}).water().current = Current_{:}_{:};\n"
                         .format(seastate_name, i, _seastate.number, seastate_name))
            JSmod.append("Condition_{:}.component({:}).water().current(Current_{:}_{:});\n"
                         .format(seastate_name, i, _seastate.number, seastate_name))
        else:
            JSmod.append("Condition_{:}.component({:}).water().setNoCurrent();\n"
                         .format(seastate_name, i))
        #
        # wind
        if _seastate.wind:
            JSmod.append("Condition_{:}.component({:}).air.windProfile(Wind_{:}_{:});\n"
                         .format(seastate_name, i, _seastate.wind.number, seastate_name))
    #
    #
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        _wave = _seastate.wave
        _waveTheory = get_wave_theory(_wave.theory)
        #
        # wave
        if 'calm' in _waveTheory.lower():
            i += 1
            JSmod.append("Condition_{:}.addCalmSea();\n"
                                 .format(seastate_name))            
            #
            # current
            if _seastate.current:
                JSmod.append("Condition_{:}.component({:}).water().current = Current_{:}_{:};\n"
                             .format(seastate_name, i, _seastate.number, seastate_name))
                JSmod.append("Condition_{:}.component({:}).water().current(Current_{:}_{:});\n"
                             .format(seastate_name, i, _seastate.number, seastate_name))
            else:
                JSmod.append("Condition_{:}.component({:}).water().setNoCurrent();\n"
                             .format(seastate_name, i))
            #
            #
            # wind
            if _seastate.wind:
                JSmod.append("Condition_{:}.component({:}).air.windProfile(Wind_{:}_{:});\n"
                             .format(seastate_name, i, _seastate.wind.number, seastate_name))
        #
    #    
    #
    JSmod.append("//\n")
    JSmod.append("//\n")    
    #
    return JSmod
#
#
def print_no_conditions(seastate_name=None):
    """
    """
    #
    if  seastate_name:
        seastate_name = seastate_name.strip()
    else:
        seastate_name = "1"  
    #
    JSmod = []
    JSmod.append("//\n")
    JSmod.append("//\n")
    JSmod.append("//Conditions\n")
    JSmod.append("//\n")
    JSmod.append("Condition_{:} = DeterministicTime(Location_{:});\n"
                 .format(seastate_name, seastate_name))     
    JSmod.append("Condition_{:}.addCalmSea();\n"
                 .format(seastate_name))
    JSmod.append("Condition_{:}.water().setNoCurrent();\n"
                 .format(seastate_name))
    JSmod.append("//\n")
    JSmod.append("//\n")
    #
    return JSmod
#
#
def print_analysis_metocean(_metocean, water_depths, 
                            mudline, surface, seastate_name,
                            non_hydro, pile_boundary=True):
    """
    """
    #
    seastate_name = seastate_name.strip()
    # FIXME : check this value
    minimum_depth = round(abs(mudline - surface),3)
    #
    JSmod = []
    #
    JSmod.append("//\n")
    JSmod.append("//Analysis\n")
    JSmod.append("//\n")
    #
    JSmod.append("{:} = Analysis(true);\n".format(seastate_name))
    JSmod.append("{:}.add(MeshActivity());\n".format(seastate_name))
    JSmod.append("{:}.step(1).beamsAsMembers = true;\n".format(seastate_name))
    JSmod.append("{:}.step(1).smartLoadCombinations = true;\n".format(seastate_name))
    JSmod.append("{:}.step(1).usePartialMesher = false;\n".format(seastate_name))
    JSmod.append("{:}.step(1).lockMeshedConcepts = true;\n".format(seastate_name))
    
    if pile_boundary:
        JSmod.append("{:}.step(1).pileBoundaryCondition = pmPileSoilInteraction;\n".format(seastate_name))
        JSmod.append("{:}.step(1).superElementTypeTop = 21;\n".format(seastate_name))
        JSmod.append("{:}.step(1).superElementType = 1;\n".format(seastate_name))        
    else:
        JSmod.append("{:}.step(1).pileBoundaryCondition = pmFixed;\n".format(seastate_name))
        JSmod.append("{:}.step(1).setNoSuperElementType();\n".format(seastate_name))
    #
    JSmod.append("{:}.step(1).nodeNumberFromJointName = false;\n".format(seastate_name))
    JSmod.append("{:}.step(1).elementNumberFromBeamName = false;\n".format(seastate_name))
    JSmod.append("{:}.step(1).regenerateMeshOption = anConditionalRegenerateMesh;\n".format(seastate_name))
    #
    JSmod.append("//\n")
    #
    JSmod.append("{:}.add(WaveLoadActivity(Condition_{:}));\n"
                 .format(seastate_name, seastate_name))
    JSmod.append("{:}.step(2).loadCalculation(true);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    #
    JSmod.append("{:}.step(2).specialOptions().useMacCamyFuchs(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).specialOptions().inPhase(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).specialOptions().useOnStructure(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).specialOptions().excludeEccentricities(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).specialOptions().absoluteWaterDepths(false);\n".format(seastate_name))
    #
    if non_hydro:
        JSmod.append("{:}.step(2).specialOptions().useEliminatedStructure(true);\n".format(seastate_name))
        JSmod.append("{:}.step(2).specialOptions().eliminatedStructure = {:};\n"
                     .format(seastate_name, non_hydro))
    else:
        JSmod.append("{:}.step(2).specialOptions().useEliminatedStructure(false);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    #
    for wdepths in water_depths:
        _level = round(wdepths, 3) - minimum_depth
        if _level == 0.0:
            continue
        JSmod.append("{:}.step(2).specialOptions().addLevel({: 1.3f} m);\n".format(seastate_name, _level))
    
    JSmod.append("{:}.step(2).deterministicSeastates().setStepType(PhaseStepping);\n".format(seastate_name))
    JSmod.append("{:}.step(2).deterministicSeastates().gustWind(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).deterministicSeastates().populate();\n".format(seastate_name))  
    #
    # print wave only
    wave_number = []
    i = 0
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        _wave = _seastate.wave        
        #wave_number.append(_seastate.number)
        wave_number.append(_wave.number)
        
        if 'calm' in _wave.theory.lower():
            continue
        
        i += 1
        JSmod.append("{:}.step(2).deterministicSeastates().seastate"
                     .format(seastate_name))
        
        stretching = 'NoStretching'
        if _seastate.current_stretching:
            stretching = 'WheelerStretching'
        #
        buoyancy = get_buoyancy(_seastate.buoyancy)
        #
        _depth = minimum_depth
        if _wave.water_depth:
            _depth = round(_wave.water_depth, 3)
        #
        JSmod.append("({:}).dataPhase({:}, 5 deg, 72, {:}, {:}, {:}, {:}, {: 1.3f} m);\n"
                     .format(i, stretching, buoyancy, 
                             _seastate.design_load,
                             _seastate.current_blockage, 
                             _seastate.wave_kinematics, _depth))
        #
        # TODO : check why this change in name, numer
        #_seastate.name = seastate_name + '_' + str(_seastate.name) 
        _seastate.number = i
        #
    #
    # print calm sea only
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        _wave = _seastate.wave         
        #_wave = _metocean[key]
        
        if 'calm' in _wave.theory.lower():
            i += 1
            # check if foundation exist
            if pile_boundary:
                _depth = minimum_depth
                if _wave.water_depth:
                    _depth = round(_wave.water_depth, 3)
            else:
                if i == 1: # check if no detrministic wave print special option
                    JSmod.append("{:}.step(2).specialOptions().addLevel({: 1.3f} m);\n".format(seastate_name, _level))
                _depth = ""  # no water depth is needed
            #
            JSmod.append("{:}.step(2).deterministicSeastates().seastate"
                         .format(seastate_name))
            #
            buoyancy = get_buoyancy(_seastate.buoyancy)
            
            JSmod.append("({:}).dataPhase(NoStretching, 0 deg, 1, {:}, {:}, 1, 1, {:});\n"
                         .format(i, buoyancy, _seastate.design_load, _depth))
            #
            #
            # TODO : check why this change in name, numer            
            #_seastate.name = seastate_name + '_' + str(_seastate.name) 
            _seastate.number = i
            #
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).addedMassAndDamping().calculateAddedMass(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().includeInternalWater(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().useCm(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().includeLongitudinalAddedMass(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().includeLongitudinalHydrodynamicMass(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().calculateDamping(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().includeLongitudinalDamping(false);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).rules().setRuleType(wrNone);\n".format(seastate_name))
    JSmod.append("{:}.step(2).rules().includeDoppler(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).rules().applyWakeType(wrNoApply);\n".format(seastate_name))
    JSmod.append("{:}.step(2).rules().maxAngle(0 deg);\n".format(seastate_name))
    JSmod.append("{:}.step(2).rules().adjustForCurrent(false);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).buoyancy().horisontalFreeSurface(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).buoyancy().steelAreaBuoyancy(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).buoyancy().includeEndForces(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).buoyancy().beamsAtMudlineBuoyancy(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).buoyancy().includeWeightOfMarineGrowth(true);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).morison().normalToElement(true);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).output().globalResults().momentRefPoint(Point(0 m,0 m,{: 1.3f} m));\n"
                 .format(seastate_name, mudline))
    JSmod.append("{:}.step(2).output().globalResults().generateSIFFile(false);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).output().loadsInterfaceFile().generateLFile(true);\n".format(seastate_name))
    #if water_depths:
    #    JSmod.append("{:}.step(2).output().loadsInterfaceFile().formatted(false);\n".format(seastate_name))
    #else:
    JSmod.append("{:}.step(2).output().loadsInterfaceFile().formatted(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).output().loadsInterfaceFile().loadCaseNumbering(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).output().loadsInterfaceFile().firstLoadCaseNumber({:});\n"
                 .format(seastate_name, min(wave_number)))
    #
    #
    if pile_boundary:
        JSmod.append("//\n")
        JSmod.append("{:}.add(PileSoilAnalysis(Condition_{:}));\n"
                     .format(seastate_name, seastate_name))
        JSmod.append("{:}.step(3).soil.materialCoeffs.tanPhiCoeff = 1;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.materialCoeffs.shearStrengthCoeff = 1;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.materialCoeffs.skinFrictionCoeff = 1;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.materialCoeffs.pileTipResistanceCoeff = 1;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.curveGeneration.lowestShearStiff = 100 Pa;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.curveGeneration.lowestLevelWithCyclicPY = 0 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.curveGeneration.zoneOfInfluenceTZ = 10;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.curveGeneration.curveShapeFactorTZ = 0.9;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.groupEffects.averagePoissonRatio = 0.5;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.groupEffects.modulusOfElasticity(10000 Pa, 0 m, 10000 Pa, -100 m);\n"
                     .format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.verticalStressAtSurface = 0 KPa;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.verticalStressUnderEmbankment = 0 Pa;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.widthOfEmbankmentSlopingPart = 0 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.distancePileToEmbankmentToe = 0 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.verticalStressUnderCircularLoadedArea = 0 Pa;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.radiusOfCircularLoadedArea = 0 m;\n".format(seastate_name))
        #
        JSmod.append("//\n")
        #
        JSmod.append("{:}.step(3).splice.solver.maxIterations = 20;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.solver.convergenceCriterion = 0.001 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.solver.divergenceCheck = false;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.solver.simplifiedGroupEffects = false;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.solver.revisionV = false;\n".format(seastate_name))
        
        JSmod.append("{:}.step(3).splice.groupEffects.interactionCode = anOff;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.groupEffects.interactionDistance = 0 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.linearisedSprings.computeLinearisedSprings = false;\n".format(seastate_name))
        #
        JSmod.append("//\n")
        #
        JSmod.append("{:}.step(3).sestra.warpCorrection = true;\n".format(seastate_name))
        JSmod.append("{:}.step(3).sestra.continueOnError = false;\n".format(seastate_name))
        JSmod.append("//\n")
    else:
        JSmod.append("//\n")
        JSmod.append("{:}.add(LinearAnalysis());\n".format(seastate_name))
    #
    #
    JSmod.append("{:}.add(LoadResultsActivity());\n".format(seastate_name))
    return JSmod
#
#
def print_analysis_no_metocean(analysis_name):
    """
    """
    JSmod = []
    #
    JSmod.append("//\n")
    JSmod.append("//Analysis\n")
    JSmod.append("//\n")
    #
    JSmod.append("{:} = Analysis(true);\n".format(analysis_name))
    JSmod.append("{:}.add(MeshActivity());\n".format(analysis_name))
    JSmod.append("{:}.step(1).beamsAsMembers = true;\n".format(analysis_name))
    JSmod.append("{:}.step(1).smartLoadCombinations = true;\n".format(analysis_name))
    JSmod.append("{:}.step(1).usePartialMesher = false;\n".format(analysis_name))
    JSmod.append("{:}.step(1).lockMeshedConcepts = true;\n".format(analysis_name))
    JSmod.append("{:}.step(1).pileBoundaryCondition = pmFixed;\n".format(analysis_name))
    JSmod.append("{:}.step(1).superElementTypeTop = 21;\n".format(analysis_name))
    JSmod.append("{:}.step(1).superElementType = 1;\n".format(analysis_name))
    JSmod.append("{:}.step(1).nodeNumberFromJointName = false;\n".format(analysis_name))
    JSmod.append("{:}.step(1).elementNumberFromBeamName = false;\n".format(analysis_name))
    JSmod.append("{:}.step(1).regenerateMeshOption = anConditionalRegenerateMesh;\n".format(analysis_name))
    #
    #JSmod.append("//\n")
    #JSmod.append("{:}.step(2).warpCorrection = true;\n".format(analysis_name))
    #JSmod.append("{:}.step(2).continueOnError = false;\n".format(analysis_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.add(LinearAnalysis());\n".format(analysis_name)) 
    JSmod.append("{:}.add(LoadResultsActivity());\n".format(analysis_name))
    JSmod.append("//\n")
    return JSmod
#
#
def print_wave_sets(_metocean, factors, seastate_name):
    """
    """
    #
    seastate_name = seastate_name.strip()
    #
    #  Units
    _length_factor = factors[0]
    #
    JSmod = []
    JSmod.append("//\n")
    JSmod.append("// Regular Wave Sets\n")
    JSmod.append("//\n")
    JSmod.append("\n")
    JSmod.append("Wave_{:} = RegularWaveSet();\n"
                         .format(seastate_name))    
    #
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        _wave = _seastate.wave
        #
        if 'calm' in _wave.theory.lower():
            continue
        #
        JSmod.append("// Load Name : {:} Number : {:}\n"
                     .format(_wave.name, _wave.number))
        #
        JSmod.append("Wave_{:}.add(RegularWave"
                     .format(seastate_name))
        
        JSmod.append("({:} deg, {:1.3f} m, WavePeriod({:} s), 0 deg));\n"
                     .format(_seastate.wave_direction,
                             _wave.height * _length_factor,
                             _wave.period))
    #
    JSmod.append("//\n")
    return JSmod
#
def print_current_profiles(_metocean, factors, seastate_name):
    """
    """
    #
    seastate_name = seastate_name.strip()
    #    
    #  Units
    _length_factor = factors[0]
    #
    JSmod = []
    JSmod.append("//\n")
    JSmod.append("// Regular Current Sets\n")
    JSmod.append("//\n")
    #
    _list_current = []
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        if _seastate.current:
            _curr = _seastate.current
            
            if _curr.name in _list_current:
                continue
            else:
                _list_current.append(_curr.name)
        
        else:
            continue
        #
        # Elevations
        #
        JSmod.append("Current_{:}_{:}_elevations = Array("
                     .format(_curr.number, seastate_name))
        
        _range = len(_curr.profile)
        i = 0
        for _elev in _curr.profile:
            i += 1
            JSmod.append("{: 1.3f} m".format(_elev[0] * _length_factor))
            if i < _range:
                JSmod.append(",")
        #
        JSmod.append(");\n")
        #       
        #
        # Elevations
        #
        JSmod.append("Current_{:}_{:}_velocities = Array("
                     .format(_curr.number, seastate_name))
        
        i = 0
        for _elev in _curr.profile:
            i += 1
            JSmod.append("{:1.3f} m/s".format(_elev[1] * _curr.velocity))
            if i < _range:
                JSmod.append(",")
        #
        JSmod.append(");\n")
    #
    JSmod.append("\n")
    JSmod.append("\n")
    #
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        if _seastate.current:
            _curr = _seastate.current
        else:
            continue
        #
        # Angles
        #
        JSmod.append("Current_{:}_{:}_directions = Array("
                     .format(_seastate.number, seastate_name))        
        i = 0
        for _elev in _curr.profile:
            i += 1
            JSmod.append("{:} deg".format(_seastate.current_direction))
            if i < _range:
                JSmod.append(",")
        #
        JSmod.append(");\n") 
        #
        #
        JSmod.append("Current_{:}_{:} = CurrentProfileRelDir("
                     .format(_seastate.number, seastate_name))
        JSmod.append("Current_{:}_{:}_elevations, "
                     .format(_curr.number, seastate_name))
        JSmod.append("Current_{:}_{:}_directions, "
                     .format(_seastate.number, seastate_name))
        
        _rel = 'false'
        if _curr.absolute_elevation:
            _rel = 'true'
        JSmod.append("Current_{:}_{:}_velocities, dtRelativeX, {:});\n"
                     .format(_curr.number, seastate_name, _rel))
        
        JSmod.append("\n")
    #
    JSmod.append("//\n")
    #
    return JSmod
#
def get_heading(alignment):
    """
    """
    if alignment == 'wave':
        return "dtRelativeHeading, "
    
    elif alignment == 'along':
        return "dtAlongHeading, "
    
    else:
        return "dtRelativeX, "
#
def print_wind_profiles(_metocean, factors, seastate_name):
    """
    """
    #
    seastate_name = seastate_name.strip()
    #    
    #  Units
    _length_factor = factors[0]
    #
    JSmod = []
    JSmod.append("//\n")
    JSmod.append("// Wind Profiles\n")
    JSmod.append("//\n")
    #
    #for _wind in wind.values():
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        if _seastate.wind:
            _wind = _seastate.wind
        else:
            continue
        
        JSmod.append("// load case : {:} \n".format(_wind.name))
        
        JSmod.append("Wind_{:}_{:} = WindProfileRelDir("
                     .format(_wind.number, seastate_name))
        
        JSmod.append("{:1.4e} m/s, {:1.3f} m, "
                     .format(_wind.velocity * _length_factor,
                             _wind.height* _length_factor))
        #
        #
        if _wind.formula == 'general':
            JSmod.append("{:}, ".format(_wind.power))
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("wpGeneral, {:} deg, {:}"
                         .format(_seastate.wind_direction, 
                                 _wind.gust_factor))
        
        elif _wind.formula == 'normal':
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("wpNormal, {:} deg, {:}"
                         .format(_seastate.wind_direction, 
                                 _wind.period_ratio))
        
        elif _wind.formula == 'abs':
            JSmod.append("{:}, ".format(_wind.power))
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("wpABS, {:} deg, {:}"
                         .format(_seastate.wind_direction, 
                                 _wind.period_ratio))
        
        elif _wind.formula == 'extreme':
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("wpExtreme, {:} deg, {:}"
                         .format(_seastate.wind_direction, 
                                 _wind.period_ratio))
        
        elif _wind.formula == 'extreme_api21':
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("wpExtremeAPI21, {:} deg, {:}"
                         .format(_seastate.wind_direction, 
                                 _wind.period_ratio))
        
        if _seastate.wind_areas:
            JSmod.append(", wfProjectedPressure);\n")
        else:
            JSmod.append(");\n")
    #
    JSmod.append("//\n")
    #
    return JSmod    
#
#