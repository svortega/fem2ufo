# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports


# package imports
import fem2ufo.process.control as process

#
#
def print_material(material, factors, subfix=False):
    """
    """
    # ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
    # default force units is meter*second^-2*gram
    # to convert to meter*second^-2*kilogram :     
    # units
    # FIXME : this doesn't look good
    if factors[4] == 1.0:
        _forceFactor = factors[4]
    else:
        _forceFactor = factors[4] / 1000.0
    #
    _lenFactor = factors[0]
    _stress = _forceFactor / _lenFactor**2
    _density = factors[1] / _lenFactor**3
    # need to fix temperature
    sub = ""
    if subfix:
        sub = str(subfix) + "_"  
    #
    header = 'MATERIALS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    #
    JSmod.append('//  MaterialLinear(Fy  Density  E, Poisson, Alpha, Damping)\n')
    for _mat in material.values():

        if 'shim' in _mat.type.lower():
            continue
        
        elif 'spring' in _mat.type.lower():
            continue

        JSmod.append('{:}{:} = '.format(sub, _mat.name))
        try:
            JSmod.append(
                'MaterialLinear({:1.4e} Pa, {:1.4e} Kg/m^3, {:1.4e} Pa, {:1.4e}, {:1.4e} delC^-1, {:1.4e} N*s/m, {:1.4e} Pa); \n'
                    .format(_mat.Fy * _stress,
                            _mat.density * _density,
                            _mat.E * _stress,
                            _mat.poisson,
                            _mat.alpha,
                            _mat.damping,
                            _mat.Fu * _stress))
        except AttributeError:
            JSmod.append(
                'MaterialLinear({:1.4e} Pa, {:1.4e} Kg/m^3, {:1.4e} Pa, {:1.4e}, {:1.4e} delC^-1, {:1.4e} N*s/m); \n'
                    .format(_mat.Fy * _stress,
                            _mat.density * _density,
                            _mat.E * _stress,
                            _mat.poisson,
                            _mat.alpha,
                            _mat.damping))
    #
    return JSmod
#