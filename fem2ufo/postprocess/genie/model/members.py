# 
# Copyright (c) 2009-2020 fem2ufo
# 


# Python stdlib imports
import math
import sys

# package imports
import fem2ufo.process.control as process


#
def print_hinges(hinges, subfix=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"  
    #
    header = 'HINGES'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    #
    for _hinge in hinges.values():
        JSmod.append('{:}{:} ='.format(sub, _hinge.name))
        JSmod.append(' Hinge(')
        #
        for i in range(6):
            JSmod.append('{:}'.format(_hinge.fix[i]))
            if i == 5:
                JSmod.append(');\n')
            else:
                JSmod.append(', ')
    # 
    return JSmod
#
#
#
def print_hydro_properties(_beam, _prefix='Bm'):
    """
    """
    JSmod = []
    #
    # cdcm 
    try:
        _cdcm = _beam.properties.cdcm
        if 'diameter' in _cdcm.type:
            JSmod.append('{:}{:}.morison = MorisonDiameter_{:};\n'
                         .format(_prefix, _beam.name,
                                 _cdcm.name))
        else:
            JSmod.append('{:}{:}.morison = MorisonConstant_{:};\n'
                         .format(_prefix, _beam.name,
                                 _cdcm.name))
    except AttributeError:
        pass
    #
    # aerodynamic
    try:
        air_drag = _beam.properties.air_drag
        if air_drag.type == 'specified':
            JSmod.append('{:}{:}.airDrag = AirDragConstant_{:};\n'
                         .format(_prefix, _beam.name,
                                 air_drag.name))
    except AttributeError:
        pass
    #    
    # marine growth
    try:
        _mgrowth = _beam.properties.marine_growth
        if _mgrowth.type == 'profile':
            JSmod.append('{:}{:}.marineGrowth = MG_ZLevel_{:};\n'
                         .format(_prefix, _beam.name, _mgrowth.name))
        else:
            JSmod.append('{:}{:}.marineGrowth = MG_constant_{:};\n'
                         .format(_prefix, _beam.name, _mgrowth.name))
    except AttributeError:
        pass        
    #
    # flooded
    if _beam.properties.flooded:
        JSmod.append('{:}{:}.flooding = flooding_on;\n'
                     .format(_prefix, _beam.name))
    #
    JSmod.append('{:}{:}.elementRefinement = element_refinement_default;\n'
                 .format(_prefix, _beam.name))
    #
    # hydrodynamic diameter
    try:
        _hdiam =  _beam.properties.hydrodynamic_diameter
        JSmod.append('{:}{:}.hydrodynamicDiameter = HydroDiameter_{:};\n'
                     .format(_prefix, _beam.name, _hdiam.name))
    except AttributeError:
        pass     
    #
    # buoyancy area
    try:
        _area = _beam.properties.buoyancy_area
        JSmod.append('{:}{:}.buoyancyArea = BuoyancyArea_{:};\n'
                     .format(_prefix, _beam.name, _area.name))
    except AttributeError:
        pass
    #
    #
    return JSmod
#
def print_buckling_factors(_beam, factor, _prefix='Bm'):
    """
    """
    JSmod = []
    try:
        item1, item2, item3 = _beam.properties.L
        
        JSmod.append('{:}{:}.buckling = BucklingFactor('
                     .format(_prefix, _beam.name))
        
        #item1, item2, item3 = _beam.properties.L
        try:
            item2 = item2 * factor
        except TypeError:
            pass
        
        try:
            item3 = item3 * factor
        except TypeError:
            pass        
        
        try:
            JSmod.append('{:1.3f}, {:1.3f},'
                         .format(item2, item3))
        except ValueError:
            JSmod.append('{:}, {:},'
                         .format(item2, item3))
        
        item1, item2, item3 = _beam.properties.K
        JSmod.append(' {:1.3f}, {:1.3f},'
                     .format(item2, item3))
        
        item1, item2, item3 = _beam.properties.Cm
        JSmod.append(' {:1.3f}, {: 1.3f});\n'
                     .format(item2, item3))
    except AttributeError:
        pass
    
    return JSmod
#
def print_local_system(_beam, _prefix='Bm'):
    """
    """
    JSmod = []
    try:
        _angle = _beam.properties.local_system
        JSmod.append('{:}{:}.rotateLocalX({:} deg);\n'
                     .format(_prefix, _beam.name, _angle))
    except AttributeError:
        pass    
    
    return JSmod
#
def print_offsets(_beam, factor, _prefix='Bm'):
    """
    """
    JSmod = []
    #
    # Global system end 1
    if _beam.offsets[0].case == 1:
        JSmod.append('{:}{:}.setEndOffset(1, Vector3d'
                     .format(_prefix, _beam.name))
        
        x1, y1, z1 = _beam.offsets[0].eccentricity
        JSmod.append('({: 1.4f} m, {: 1.4f} m, {: 1.4f} m));\n'
                     .format(x1 * factor, y1 * factor, z1 * factor))
    # Local system
    else:
        # end 1
        JSmod.append('{:}{:}.CurveOffset = LinearVaryingCurveOffset('
                     .format(_prefix, _beam.name))
        
        x1, y1, z1 = _beam.offsets[0].eccentricity
        JSmod.append('Vector3d({: 1.4f} m, {: 1.4f} m, {: 1.4f} m), '
                     .format(x1 * factor, y1 * factor, z1 * factor))
        #
        # end 2
        try:
            x1, y1, z1 = _beam.offsets[-1].eccentricity
            JSmod.append('Vector3d({: 1.4f} m, {: 1.4f} m, {: 1.4f} m), true);\n'
                         .format(x1 * factor, y1 * factor, z1 * factor))
        except AttributeError:
            JSmod.append('Vector3d({: 1.4f} m, {: 1.4f} m, {: 1.4f} m), true);\n'
                         .format(0, 0, 0))
        #
        return JSmod
    # 
    # Global system end 2
    #if _beam.offsets[1].case == 1:
    JSmod.append('{:}{:}.setEndOffset(2, Vector3d'
                 .format(_prefix, _beam.name))
    
    x1, y1, z1 = _beam.offsets[-1].eccentricity
    JSmod.append('({: 1.4f} m, {: 1.4f} m, {: 1.4f} m));\n'
                 .format(x1 * factor, y1 * factor, z1 * factor))
    #
    return JSmod
#
def print_steps(_beam, _prefix, _prefix2,
                cone_auto=False):
    """
    """
    #
    #if '4034_4029' in _beam.name :
    #    print('here')
    #
    JSmod = []
    _segment = _beam.length_node2node
    #_segment2 = _beam.length_node2node
    #   
    _steps = 0            
    JSmod.append('//  segmented beam\n')
    _total_steps = len(_beam.step)
    #
    # asas specific
    _cone = False
    _cone2 = False
    if cone_auto:  
        if (_total_steps > 2 and 'tubular' in _beam.section[0].shape and 'Bm' in _prefix):
            # from end 1
            #try:
            if _beam.section[0].properties.D > _beam.section[1].properties.D > _beam.section[2].properties.D:
                _cone = True
            # from end 2
            if _beam.section[-1].properties.D > _beam.section[-2].properties.D > _beam.section[-3].properties.D:
                _cone2 = True
            #except IndexError: # probably a grouted steeped section 
            #    print('-->') 
    #
    for x in range(_total_steps):
        _start = float(_beam.step[x])
        _ratio = _start / _segment
        #
        # TODO : This shortcut fix was defined for piles sections
        if _ratio > 1:
            _ratio = 1
            _segment = 0
        else:
            _segment -= _start
        #
        _steps += 1
        #
        if round(_segment, 3) < 0:
            JSmod.append('//\n')
            JSmod.append('{:}{:}.setSegmentMaterial({:}, {:}{:});\n'
                         .format(_prefix, _beam.name, _steps, 
                                 _prefix2, _beam.material[0].name))
            #try:
            JSmod.append('{:}{:}.SetSegmentSection({:}, {:}{:});\n'
                         .format(_prefix, _beam.name, _steps, 
                                 _prefix2, _beam.section[0].name))
            break
        #
        try:
            #_ratio = round(_start / _segment, 3)
            _test = 1.0 / (1.0 - round(_ratio, 3))
            #
            JSmod.append('{:}{:}.divideSegmentAt({:}, {:1.4f});\n'
                         .format(_prefix, _beam.name, _steps, _ratio))
        except ZeroDivisionError:
            pass
        #
        #
        JSmod.append('{:}{:}.setSegmentMaterial({:}, {:}{:});\n'
                     .format(_prefix, _beam.name, _steps, 
                             _prefix2, _beam.material[x].name))
        
        if _beam.section[x].shape == 'cone':
            JSmod.append('{:}{:}.SetSegmentSection({:}, {:});\n'
                         .format(_prefix, _beam.name, _steps, 
                                 'cone_medium')) 
        
        else:
            JSmod.append('{:}{:}.SetSegmentSection({:}, {:}{:});\n'
                         .format(_prefix, _beam.name, _steps, 
                                 _prefix2, _beam.section[x].name))
    #
    # asas specific
    if _cone:
        JSmod.append('// add cone\n')
        JSmod.append('{:}{:}.SetSegmentSection(2, cone_medium);\n'
                     .format(_prefix, _beam.name))
        JSmod.append('//\n')
    #
    if _cone2:
        JSmod.append('// add cone\n')
        JSmod.append('{:}{:}.SetSegmentSection({:}, cone_medium);\n'
                     .format(_prefix, _beam.name, _total_steps-1))
        JSmod.append('//\n')
    #
    return JSmod
#
#
def get_beam(subfix, _beam, factor, cone_auto):
    """
    """
    _prefix = "Bm_"
    _prefix2 = ""
    
    if subfix:
        _prefix = str(subfix) + 'Bm_'
        _prefix2 = subfix

    if 'pile' in _beam.type.lower():
        _prefix = 'Pile_'
        _prefix2 = 'Pile_'
        
        if subfix:
            _prefix = str(subfix) + 'Pile_'
            _prefix2 = str(subfix) + 'Pile_'
    #
    JSmod = []
    JSmod.append('\n')
    #
    JSmod.append('{:}{:}.setDefault();\n'.format(_prefix2, 
                                                 _beam.section[0].name))
    JSmod.append('{:}{:}.setDefault();\n'.format(_prefix2, 
                                                 _beam.material[0].name))
    JSmod.append('{:}{:} = '.format(_prefix, _beam.name))
    
    if _beam.type != 'pile':
        JSmod.append('{:}('.format("beam"))
    else:
        JSmod.append('{:}('.format(_beam.type))
    
    #if _beam.name == '689_5C1X' :
    #    print('--->')
    
    _beam.get_end_nodes()
    x1, y1, z1 = _beam.node[0].get_coordinates(factor)
    JSmod.append('Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m), '
                 .format(x1, y1, z1))

    x2, y2, z2 = _beam.node[-1].get_coordinates(factor)
    JSmod.append('Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m)'
                 .format(x2, y2, z2))
    #
    #
    #if _beam.overlap:
    if 'pile' in _beam.type.lower():
        JSmod.append(');\n')
    else:
        JSmod.append(', geAllowOverlap')
        JSmod.append(');\n')
    #
    #
    if 'disconnected' in _beam.type:
        JSmod.append('{:}{:}.beamType = pile_in_leg;\n'
                     .format(_prefix, _beam.name))
    
    elif 'nonstructural' in _beam.type:
        JSmod.append('{:}{:}.beamType = non_structural;\n'
                     .format(_prefix, _beam.name))
    
    elif 'grouted' in _beam.type:
        JSmod.append('{:}{:}.beamType = grouted;\n'
                     .format(_prefix, _beam.name))    
    #
    #
    if 'pile' in _beam.type.lower():
        JSmod.append('{:}{:}.pileCharacteristics = pile_characteristics_default;\n'
                     .format(_prefix, _beam.name))    
    #
    #
    # check if segmented beam 
    if len(_beam.step) > 1:
        JSmod.extend(print_steps(_beam, _prefix, _prefix2, cone_auto))
    #
    #
    # beam offsets
    # place beam offset first and see if lengh ok
    # TODO : do we need offset before segmented?
    if _beam.offsets:
        JSmod.extend(print_offsets(_beam, factor, _prefix))    
    # hinges
    #
    _end = 0
    for _hinge in _beam.releases:
        _end += 1
        if _hinge :
            JSmod.append('{:}{:}.setEndHinge('
                         .format(_prefix, _beam.name))
            JSmod.append('{:}, {:}{:});\n'
                         .format(_end, _prefix2, _hinge.name))
    #
    # Properties
    #
    if _beam.properties:
        # hydro parameters
        JSmod.extend(print_hydro_properties(_beam, _prefix))
        #
        # beam code check parameters
        JSmod.extend(print_buckling_factors(_beam, factor, _prefix))
        #
        # beam local system
        JSmod.extend(print_local_system(_beam, _prefix))
        #
    #
    #
    #
    return JSmod
#
def get_plate(sub, _plate, factor, membrane=False):
    """
    """
    _prefix = "Pl_"
    _prefix2 = ""
    if sub:
        _prefix = str(sub) + 'Pl_'
        _prefix2 = str(sub)
    #
    #if '4401' in _plate.section[0].name:
    #    print('-->')
    #
    JSmod = []
    JSmod.append('\n')
    JSmod.append('{:}{:}.setDefault();\n'.format(_prefix2, 
                                                 _plate.section[0].name))
    JSmod.append('{:}{:}.setDefault();\n'.format(_prefix2, 
                                                 _plate.material[0].name))
    JSmod.append('{:}{:} = Plate('.format(_prefix, _plate.name))
    #
    _tot = len(_plate.node)
    if _tot < 3 or _tot > 5:
        print('   *** error plate {:} with {:} nodes'
              .format(_plate.name, _tot))
        sys.exit()
    
    for x in range(_tot - 1):
        x1, y1, z1 = _plate.node[x].get_coordinates(factor)
        JSmod.append('Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m), '
                     .format(x1, y1, z1))
    #
    x1, y1, z1 = _plate.node[_tot-1].get_coordinates(factor)
    JSmod.append('Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m)'
                 .format(x1, y1, z1))
    
    JSmod.append(');\n')
    if membrane:
        JSmod.append('{:}{:}.plateType = membrane;\n'
                     .format(_prefix, _plate.name))
    
    JSmod.append('{:}{:}.meshDensity = mesh_density_default;\n'.format(_prefix, _plate.name))
    #
    return JSmod
#
def get_shim(sub, _beam, factor):
    """
    """
    _prefix = 'Bm'
    if sub:
        _prefix = sub + '_Bm'
    
    JSmod = []
    JSmod.append('\n')
    JSmod.append('{:}{:}.setDefault();\n'.format(_prefix, 
                                                    _beam.section[0].name))
    JSmod.append('{:}{:}.setDefault();\n'.format(s_prefix, 
                                                    _beam.material[0].name))
    JSmod.append('{:}{:} = '.format(_prefix, _beam.name))
    JSmod.append('{:}('.format('Beam'))
    
    #if _beam.name == '301_401' :
    #    print('--->')
    
    _beam.get_end_nodes()

    x1, y1, z1 = _beam.node[0].get_coordinates(factor)
    JSmod.append('Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m), '
                 .format(x1, y1, z1))

    x2, y2, z2 = _beam.node[-1].get_coordinates(factor)
    JSmod.append('Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m)'
                 .format(x2, y2, z2))
    #
    if _beam.overlap:
        JSmod.append(', geAllowOverlap')    
    JSmod.append(');\n')
    #
    JSmod.append('{:}{:}.beamType = Shim1;\n'
                 .format(_prefix, _beam.name))
    #
    #
    # hydro
    #
    if _beam.properties:
        # hydro parameters
        JSmod.extend(print_hydro_properties(_beam, _prefix, sub))
        #
        # beam code check parameters
        JSmod.extend(print_buckling_factors(_beam, factor, _prefix, sub))
    #
    #
    return JSmod    
#
#
def print_beam(element, sections, factor, 
               subfix=False, cone_auto=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"  
    #
    header = 'beam' 
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    #
    _beams = [key for key in sorted(element, key=lambda name: element[name].number)
              if 'beam' in element[key].type  or 'truss' in element[key].type]
    
    # normal beam
    for key in _beams:
        _beam = element[key]
        #if '108_140' in key:
        #    print('-->')
        JSmod.extend(get_beam(sub, _beam, factor, cone_auto))
        
    # overlpping beam
    #for key in _beams:
    #    _beam = element[key]
    #    if _beam.overlap:
    #        JSmod.extend(get_beam(sub, _beam, factor, cone_auto))
 
    # shim elements
    #for _beam in  element.values():
    #    if _beam.type == 'shim' :
    #        JSmod.extend(get_shim(sub, _beam, factor)) 
    #        JSmod.append('{:}shim_{:} = '.format(sub, _beam.name))
    #        JSmod.append('BeamTypeShim()\n')
    #
    #
    return JSmod
#
def print_shell(element, factor, subfix=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    #
    header = 'Shell' 
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    #
    # normal plate
    for _plate in element.values():
        if _plate.type != 'shell':
            continue
        JSmod.extend(get_plate(sub, _plate, factor))
    #
    return JSmod
#
def print_membrane(element, factor, subfix=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    #
    header = 'Membrane' 
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    #
    # normal plate
    for _plate in element.values():
        if _plate.type != 'membrane':
            continue
        JSmod.extend(get_plate(sub, _plate, factor,
                               membrane=True))
    #
    return JSmod
#
def print_sets(sets, element, subfix=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    #
    header = 'SETS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")
    JSmod.append('// \n')
    
    set_list = [key for key in sorted(sets, key=lambda name: sets[name].number)]

    for _name in set_list:
        _set = sets[_name]
        JSmod.append('{:}{:} = Set();\n'.format(sub, _set.name))
    #
    #
    JSmod.append('//\n')
    JSmod.append('//***** SETS ( Fill ) *****//\n')
    JSmod.append('// Sets \n')
    JSmod.append('//\n')
    
    for _name in set_list: 
        _set = sets[_name]
        #if _set.number == 7999:
        #    print('-->')
        #
        for _item in _set.concepts:
            try:
                _memb = element[_item]
            except KeyError:
                continue
            
            #_prefix = ""
            #if subfix:
            if _memb.type == 'beam':
                _prefix = 'Bm_'
            elif _memb.type == 'shell' or _memb.type == 'membrane':
                _prefix = 'Pl_'
            else:
                continue
            
            JSmod.append('{:}{:}.add('.format(sub, _set.name))
            JSmod.append('{:}{:}{:});\n'.format(sub, _prefix, _memb.name))
        JSmod.append('// \n')

    return JSmod
#
#
def print_support(geometry, 
                  factor=1, subfix=False):
    """
    """
    boundaries = geometry.boundaries
    nodes = geometry.nodes
    element = geometry.concepts
    #
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    #
    header = 'SUPPORTS'
    JSmod = process.common.headers.print_head_line(header, subfix="//")

    JSmod.append('//\n')
    #try:
    for _support in boundaries.values():
        if _support.releases:
            for _node_name in _support.nodes:
                try:
                    _node = nodes[_node_name]
                except KeyError:
                    #print('   ** warning boundary node {:} not found'
                    #      .format(_node_name))
                    continue
                #_test = 1.0 / len(_node.boundary)
                
                JSmod.append('{:}sp_{:} = SupportPoint('.format(sub, _node.name))
                x1, y1, z1 = _node.get_coordinates(factor)
                JSmod.append('Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m));\n'
                             .format(x1, y1, z1))
    
                JSmod.append('{:}sp_{:}.boundary = BoundaryCondition('.format(sub, _node.name))
                _No = 0
                for _fix in _node.boundary.releases:
                    _No += 1
    
                    if _fix == 0:
                        JSmod.append('Free')
    
                    else:
                        JSmod.append('Fixed')
    
                    if _No < 6:
                        JSmod.append(', ')
                    else:
                        JSmod.append(');\n')
    
                JSmod.append('\n')
    #except AttributeError:
    #    pass
    #
    _beams = [key for key in sorted(element, key=lambda name: element[name].number)
              if 'Spring to Ground' in element[key].type]
    
    for key in _beams:
        _beam = element[key]
        _node = _beam.node[0]
        JSmod.append('{:}sp_{:} = SupportPoint('.format(sub, _node.name))
        x1, y1, z1 = _node.get_coordinates(factor)
        JSmod.append('Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m));\n'
                                     .format(x1, y1, z1))
        
        JSmod.append('{:}sp_{:}.boundary = BoundaryCondition('.format(sub, _node.name))
        _No = 0
        for _fix in _beam.material[0].stiffness[0]:
            _No += 1
            if isinstance(_fix, str):
                JSmod.append('{:}'.format(_fix))
                if _No < 6:
                    JSmod.append(', ')
            else:
                JSmod.append('Stiffness({:1.4e}'.format(_fix))
                
                if _No == 6:
                    JSmod.append(' N*m)')
                elif _No > 3:
                    JSmod.append(' N*m), ')
                else:
                    JSmod.append(' N/m), ')
                    
            if _No == 6:
                JSmod.append(');\n')

        JSmod.append('\n')        
    # 
    #
    return JSmod
#
#