# Copyright (c) 2009-2019 fem2ufo
#
#
#
from fem2ufo.interface.fem2ufo import (SESAM_model, 
                                       ASAS_model, 
                                       GENIE_model,
                                       SACS_model,
                                       USFOS_conversion, 
                                       GeniE_conversion,
                                       CSV_conversion,
                                       XML_conversion,
                                       WEIGHT_database,
                                       TimeSeries,
                                       master_input)


__all__ = "interface"


# constants

__major__ = 0.  # for major interface/format changes
__minor__ = 40  # for minor interface/format changes
__release__ = 00  # revision for tweaks, bug-fixes, or development

__version__ = '%d.%d.%d' % (__major__, __minor__, __release__)

__author__ = 'Salvador Vargas-Ortega'
__license__ = 'MIT'
__author_email__ = 'svortega@gmail.com'
__maintainer_email__ = 'fem2ufo_users@googlegroups.com'
__url__ = 'http://fem2ufo.readthedocs.org'
__downloadUrl__ = "http://bitbucket.org/svortega/fem2ufo/downloads"

# header
print('{:}'.format(52*'-'))
print('{:}   fem2ufo'.format(18*' '))
print('{:}   Version {:}'
      .format(14*' ',__version__))