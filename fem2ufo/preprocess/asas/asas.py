# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import sys
import os
import math
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.asas.operations.process as asas_process
import fem2ufo.preprocess.asas.model.prebeamst as asas_code_check
import fem2ufo.preprocess.asas.model.wave as wave
from fem2ufo.preprocess.asas.model.geometry import read_asas_geometry
from fem2ufo.preprocess.asas.model.load import read_asas_load
from fem2ufo.preprocess.asas.model.loco import read_asas_load_combination
from fem2ufo.preprocess.common.foundation import read_foundation
#
#
#
#
#
class ASAS_model:
    """Class reads ASAS files and place data in fem2ufo model concept"""
    #
    def __init__(self, tol=False):
        """
        **Parameters**:
           :tol: set tolerance to identied supernode at pile head 
        """
        #
        self.component = {}
        self.format = 'asas'
        self.tolerance = tol
        #
        # asas model converted to SI
        self.units_in = None
        self.units_out = process.units.set_SI_units()
        self.gravity = False
        #
        self.Fu_update = {}
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """
        try:
            path = os.path.normcase(path)
            os.chdir(path)
        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()

        retval = os.getcwd()
        print('')
        print('--- Current working directory :')
        print('--- {:}'.format(retval))
    #
    def global_units(self, **kwargs):
        """
        Global units must be defined:
        Units [length, mass, time, temperature, force, pressure/stress]/n
        
        **Parameters**:
          :length : [mandatory]\n
          :force: [mandatory]\n
          :mass : [mandatory]\n
          :temperature: \n
          :gravity: [default : 9.81ms^2]\n
        """
        for key, value in kwargs.items():
            _unit = process.units.find_unit_case(key)
            self.units_in = process.units.units_module(_unit, value,
                                                       self.units_in)
    #
    def substructure(self, topology, 
                     super_element=False, 
                     conductor_bell=False,
                     general_section=False):
        """
        **Parameters**:
           :topology: asas.dat file file
           :loading: associated loading.dat file (optional)
           :super_element : if True, Master nodes are converted to element hinges
           :conductor_bell : if True, conductor bells are identified and a joint introduced
           :general_section : if True, general sections takes priority
        """
        #self.gemetry_file = topology
        #self.loading_file = loading

        # set master node tolerance
        if not self.tolerance:
            try:
                self.tolerance = process.units.get_tolerance(self.units_in[0])
            except SyntaxError:
                print('  **  error global units not provided')
                print('      process terminated')
                sys.exit()

        # read geometry
        self.model = f2u.F2U_model(topology, 1)
        
        self.model.component = read_asas_geometry(file_name = topology, 
                                                  global_units = self.units_in,
                                                  superelement=super_element,
                                                  conductor_bell=conductor_bell,
                                                  general_section=general_section)
        
        #self.model = preprocess.read_geometry(_format = self.format,
        #                                      _geometry = topology,
        #                                      #_load = self.loading_file,
        #                                      _tol = self.tolerance,
        #                                      _units = self.units_in)
        self.model.data['format'] = 'asas'
    #
    def loading(self, load_file):
        """
        **Parameters**:
           :load_file: asas loading file
        """        
        read_asas_load(model=self.model, 
                       file_name=load_file,
                       global_units=self.units_in,
                       load_flag=True)
    #
    def foundation(self, _foundation,
                   file_format=False, foundation_units=False):
        """
        **Parameters**:
           :foundation: splinter.dat file  
           :file_format: asas (default) 
           :units : [length, mass, time, temperature, force, pressure/stress]
        """
        if not file_format:
            file_format = self.format

        if not foundation_units:
            foundation_units = self.units_in

        if not self.units_in[0] and self.units_in[4]:
            print('  **  error global units not provided')
            print('      process terminated')
            sys.exit()

        read_foundation(self.model,
                        _foundation,
                        file_format,
                        foundation_units,
                        self.units_out)
        #
        self.model.data.update(self.model.foundation.data)
        #
        #print('ok soil')
    #
    def piles(self, geometry_file):
        """
        """
        import fem2ufo.preprocess.asas.model.splinter as splinter
        #
        try:
            _mudline = [ _soil_temp.mudline for _soil_temp in self.model.foundation.soil.values()]
            _mudline = max(_mudline)
        except KeyError:
            print('  **  error foundation data not provided')
            print('      process terminated')
            sys.exit()            
        #
        if not self.model.foundation:
            self.model.set_foundation()
            
        self.model.foundation.piles, _mudline = splinter.get_piles(geometry_file, 
                                                                   self.units_in, 
                                                                   _mudline)
        #
        _lev = []
        for _soil_temp in self.model.foundation.soil.values():
            _lev.append(_soil_temp.mudline)
            _soil_temp.mudline =  math.copysign(_soil_temp.mudline, _mudline)
        #
        #_lev = []
        #for _soil in self.model.foundation.soil.values():
        #    _lev.append(_soil.mudline)
        #    _soil.mudline = abs(_soil.mudline)
        #
        _mudline = max(_lev)
        #
        for _setNo, _item in sorted(self.model.foundation.piles.sets.items()):
            if 'end_nodes__' in _item.name:
                _node0 = self.model.foundation.piles.nodes[_item.nodes[0]]
                continue
                #
                # FIXME : what is this?
                #
                if abs(_node0.z) > _mudline:
                    # FIXME : What is this?
                    fix = 1.0/0.0
                    _setNo = _item.name.replace('end_nodes__', '')
                    _membNo = self.model.foundation.piles.sets[int(_setNo)].items[0]
                    _memb = self.model.foundation.piles.concepts[_membNo]
                    _matNo = _memb.material[0].number
                    _memb.node[0].z = _mudline
                    #
        #
        process.foundation.renumber_soil(self.model.foundation.materials,
                                         self.model.component.materials)         
        #print('ok piles')
        #
    #
    def load_combination(self, data_file, design_condition,
                         combination_name, super_element=False):
        """
        Define load combination
        data_file :
        design_condition : operating/storm
        combination_name : load combination ID
        super_element : False if load combination from component
                        True if load combination from superelement assembly
        """
        #
        if not self.model.load:
            print('  **  error model load data not provided')
            print('      process terminated')
            sys.exit()
        #
        read_asas_load_combination(data_file, design_condition, self.model,
                                   combination_name, super_element)
        #
        #self.model
    #
    def metocean(self, data_file, combination_name):
        """
        **Parameters**:
          :metocean : metocean.dat
        """
        if not self.model:
            print('  **  error model data not provided')
            print('      process terminated')
            sys.exit()
        
        if not self.units_in[0] and self.units_in[4]:
            print('  **  error global units not provided')
            print('      process terminated')
            sys.exit()

        
        wave.read_metocean(metocean_file = data_file,
                           model = self.model,
                           combination_name = combination_name,
                           units = self.units_in)
        
        #self.model.data.update(self.model.metocean.data)
    #
    def assign_Fu(self, material_number, material_grade,
                  tmin, tmax, Fu):
        """
        """
        self.Fu_update[material_number] = {'number' : material_number,
                                           'grade' : material_grade,
                                           'tmin': tmin, 'tmax' : tmax,
                                           'Fu':Fu}
        #print('-->')
    #
    def code_check(self, data_file):
        """
        **Parameters**:
           :Code check parameter
        """
        #
        if not self.model:
            print('  **  error model data not provided')
            print('      process terminated')
            sys.exit()
        # 
        asas_code_check.read_prebeamst_file(data_file, 
                                            self.model.component,
                                            self.units_in)

    #
    def joints(self, data_file, prefix=False):
        """
        **Parameters**:
           :Joint input data
        """
        
        if not prefix:
            prefix = 'msl'
        #
        if not self.model.component:
            print('  **  error model data not provided')
            print('      process terminated')
            sys.exit()
        # 
        asas_code_check.read_joints(data_file, 
                                    self.model.component,
                                    prefix)
        
    #
    def Fy(self, data_file):
        """        
        **Parameters**:
           :Yield (Fy) input data
        """
        #
        if not self.model.component:
            print('  **  error model data not provided')
            print('      process terminated')
            sys.exit()
        # 
        asas_code_check.read_yield(data_file, 
                                   self.model.component,
                                   self.units_in)
    #
    def get_model(self):
        """
        Processing model --> mandatory
        """       
        # 
        print('')
        print('--------------- Updating ASAS Model ----------------')
        #
        #
        # update material's Fu
        if self.Fu_update:
            process.material.update_Fu(self.model.component, 
                                       self.model.foundation, 
                                       self.Fu_update)
        # update materials
        process.material.simplify_material(self.model.component.materials,
                                           self.model.component.concepts)
        process.material.update_material_name(self.model.component.materials,
                                              self.units_in, renumber=True)
        # Rename material pile concepts
        try:
            process.material.update_material_name(self.model.foundation.piles.materials,
                                                  self.units_in)
        except AttributeError:
            pass
        #
        # update sets
        #
        process.geometry.update_sets_name(self.model.component.sets,
                                          renumber=False)
        #
        # simplify sections
        #  geometry
        process.sections.simplify_sections(self.model.component.sections,
                                           self.model.component.concepts,
                                           self.units_in)
        # piles
        try:
            process.sections.simplify_sections(self.model.foundation.piles.sections,
                                               self.model.foundation.piles.concepts,
                                               self.units_in)
        except AttributeError:
            pass
        #        
        #
        # simplify hinges
        # probably not a good idea coz it won't provide control
        process.geometry.simplify_hinges(self.model.component.hinges, 
                                         self.model.component.concepts)
        #
        #
        # update load combinations
        #
        try:
            self.model.analysis
            asas_process.update_load_combinations(self.model)
        except AttributeError:
            pass      
        #
        # check units
        #
        #if self.units_out.count(None) == len(self.units_out):
        #    self.units_out = self.units_in
        #
        # self.model.data['units'] = f2u.Units(self.units_out)
        self.model.data['units'] = self.units_out
        #
        # clean model
        #
        #self.model = process.geometry.clean_model(self.model)
        #
        print('')
        print('----------------- END ASAS Module ------------------')
        #
        return self.model
