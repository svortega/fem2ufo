# 
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import sys
import re


# package imports
import fem2ufo.f2u_model.feclass.geometry as femData
#import fem2ufo.preprocess.asas.operations.io as asas_io
import fem2ufo.preprocess.asas.operations.common as asas_common
#

# ----------------------
# Master word modules
#
def master_word(lineIn, keyWord, ind=0):
    """
    """
    _key = {"joint": r"joint|chord",
            "Fy": r"yiel",
            'cmy' : r"CMY",
            'cmz' : r"CMZ",
            'K' : r"EFFE",
            'Lyz' : r"UNBR",
            'Lb' : r"ULCF",
            'end': r"end|stop"}
    #
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
#
def joint_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"to": r"to",
            "all": r"all",
            "element": r"elem(ent)?"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
def fy_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"material": r"mate(ial)?",
            "all": r"all",
            "step": r"step"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
#
def to_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"to": r"to",
            "all": r"all"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
def Lyz_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {#"to": r"to",
            'factor': r"fact(or)?",
            'lenght' : r"leng(ht)?"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    else:
        keyWord[ind] = 'lenght'
    #
    return lineOut, keyWord
#