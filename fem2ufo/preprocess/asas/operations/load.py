# 
# Copyright (c) 2009-2017 fem2ufo
# 


# Python stdlib imports
#import math
import re
#import sys


#
#
def load_title(lineIn):
    """
    """
    lineOut = lineIn
    lineOut = re.sub(r"\(|\)|\[|\]|\{|\}|\,", "_", lineOut)
    lineOut = re.sub(r"\-|\+|\%|\s|\=", "_", lineOut)
    lineOut = re.sub(r"\&", "and", lineOut)
    lineOut = re.sub(r"\/|\'|\;", "", lineOut)
    lineOut = re.sub(r"\.", "dot", lineOut)
    lineOut = re.sub(r"\_+", "_", lineOut)
    lineOut = re.sub(r"\?", "Qmark", lineOut)
    lineOut = re.sub(r"\@", "at", lineOut)
    lineOut = re.sub(r'\"', "in_", lineOut)
    lineOut = re.sub(r"\'", "ft_", lineOut)
    lineOut = lineOut.strip()
    lineOut = lineOut.strip("_")
    return lineOut.lower()
#
#