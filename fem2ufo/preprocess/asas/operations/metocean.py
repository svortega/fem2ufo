# 
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
#import re

# package imports
import fem2ufo.preprocess.asas.operations.common as asas_common


# ----------------------
# Master word modules
#
def master_word(lineIn, keyWord, ind=0):
    """
    """
    _key = {"elev": r"elev",
            "grow": r"grow",
            "cd": r"drag",
            "cm": r"mass",
            "end": r"end|stop",
            "execute": r"exec",
            "flooded": r"free|nobo",
            "buoy": r"buoy",
            "wave": r"wave",
            "current": r"pcur",
            "curr_profile": r"curr",
            "tide": r"tide",
            "spread": r"spre",
            "kinematic": r"kine",
            "cblockage": r"bloc\s*factor",
            "phase": r"phas",
            "wind": r"wind",
            "gravity": r"grav",
            "link": r"\@",
            "reset": r"rese",
            "elim_wave": r"nowl",
            "elim_wind": r"nowi",
            "elim_selfweight": r"nosw",
            "zone": r"zone",
            "load": r"load",
            "case": r"case",
            "output": r"outp",
            "design_load": r"maxm",
            'extra_mass': r"xmas"}
    #
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
#
#
#
def grow_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"zone": r"zone",
            "table": r"tabl"}
    #
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
#
def number_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"to": r"to"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
def wave_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"airy": r"airy",
            "cnoidal": r"cnoidal",
            "stokes5": r"stoke5",
            "grid": r"grid",
            "streamFunction": r"stream",
            "newwave": r"neww",
            "irregular": r"irre",
            "load": r"loa(d)?"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
def elev_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"xmin": r"xmin",
            "xmax": r"xmax",
            "ymin": r"ymin",
            "ymax": r"ymax",
            "zmin": r"zmin",
            "zmax": r"zmax"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
#