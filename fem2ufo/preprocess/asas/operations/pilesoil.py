# 
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import re

# package imports
#import fem2ufo.process.control as process
import fem2ufo.preprocess.asas.operations.common as asas_common


#
#  TODO : This is in fem.py file
def sort_group_top_node(_group, _node_toptip, geometry,
                        _coord='z'):
    """
    """
    _cno = str(_coord).lower()
    _coordinate = {'x': 0, 'y': 1, 'z': 2}

    _dummy = {}
    for _no, _node in sorted(_node_toptip.items()):

        _no1 = geometry.nodes[_node[0]].get_coordinates()
        _no2 = geometry.nodes[_node[1]].get_coordinates()

        if abs(_no1[_coordinate[_cno]]) > abs(_no2[_coordinate[_cno]]):
            _node_toptip[_no] = [_node[1], _node[0]]
        #
        _members = {i: {geometry.concepts[i].node[0].name,
                        geometry.concepts[i].node[1].name}
                    for i in _group[_no]}
        #
        _dummy[_no] = []
        _len = len(_members)
        _nodebase = _node_toptip[_no]
        _noderesidual = {_node_toptip[_no][0]}
        #
        while _len:
            for _memb_no, _memb in sorted(_members.items()):
                if _nodebase[0] in _memb:
                    _dummy[_no].append(_memb_no)
                    #
                    _noderesidual = _memb - _noderesidual
                    _nodebase = list(_noderesidual)
                    del _members[_memb_no]
            # calculate new length
            _len = len(_members)
    # 
    # print('ok')
    return _dummy, _node_toptip


#
#
# --------------------------------------------------------------------
#                         Splinter Auxiliary
# --------------------------------------------------------------------
# 
#
def section_data(lineIn):
    #
    lineOut = ''
    _data = []
    for x in lineIn.split():
        try:
            _data.append(float(x))
        except:
            _text = re.compile(x)
            _index = _text.search(lineIn, re.I)
            _ind = _index.start()
            #
            lineOut = lineIn[_ind:]
            lineOut = lineOut.strip()
            break
    #
    return lineOut, _data
#
# ----------------------
# Master word 
#
def spt_master_word(lineIn, keyWord, ind=0):
    #     
    _key = {  # "nodes": r"coor",
              "end": r"end",
              # "sectionproperty": r"sect",
              # "section" : r"geom",
              # "material" : r"mate",
              # "element": r"elem",
              # "hinges" : r"rele",
              # "constraint" : r"cons",
              # "fixity" : r"supp",
              # "masternode" : r"topo",
              # "load" : "load",
              # "singlepile" : r"pile",
              # "grouppile" : r"pgrp",
              "soil": r"soil"}
    #
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
#
# ----------------------
# soil modules
#
def soil_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"mudline": r"mudd",
            "pfactor": r"pfac",
            "yfactor": r"yfac",
            "tfactor": r"tfac",
            "zfactor": r"zfac",
            "PY": r"p\s*\-\s*y",
            "TZ": r"t\s*\-\s*z",
            # "force" : r"t|p",
            #"mudslide" : r"slid",
            # "step": r"\:",
            "end": r"end"}

    # _remained = keyWord[ind]
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
        # if keyWord[ind] == 'step' : keyWord[ind] = _remained
    #
    return lineOut, keyWord
#
#
def soil_word2(line_in, keyWord, ind=1):
    """
    """
    _key = {"mudmodY": r"mapy",
            "mudmodZ": r"matz",
            "PY": r"p\s*\-\s*y",
            "TZ": r"t\s*\-\s*z",
            "mudslide" : r"slid",
            "QZ": r"endb"}

    keyWord[ind], lineOut, _match = asas_common.select2(line_in, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
def soil_word3(lineIn, keyWord, ind=1):
    _key = {"force": r"t|p",
            "displacement": r"z|y",
            "step": r"\:"}

    _remained = keyWord[ind]
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
        if keyWord[ind] == 'step': keyWord[ind] = _remained
    #
    return lineOut, keyWord


#     
#
#
# --------------------------------------------------------------------
#                             Sub
# --------------------------------------------------------------------
#
#
def inser_soil_layer(soil, _depth):
    #
    import copy
    #
    for _key1, _soil in sorted(soil.items()):
        for _key2, _layer in sorted(_soil.layers.items()):
            if _depth >= abs(_layer.depth):
                _resDepth = (_layer.depth, _key2)
                _endDepth = (_layer.depth, _key2)
            else:
                _endDepth = (_layer.depth, _key2)
                break
    #
    # print(_depth / _resDepth[0], _depth / _endDepth[0])
    # print(_resDepth[0]/_depth, _endDepth[0]/_depth)
    # print(_depth, _resDepth, _endDepth)

    # find suitable layer, no need for inserting new layer
    if _resDepth[0] / _depth <= 0.10:
        _depth = _resDepth[1]
    #
    elif _endDepth[0] / _depth <= 0.10:
        _depth = _endDepth[1]
    # insert new layer in soil
    else:
        # print('need to insert new layer in soil')
        _soil.layers[_depth] = copy.copy(_soil.layers[_resDepth[1]])
        _soil.layers[_depth].name = _depth
        _soil.layers[_depth].depth = _depth
        _soil.layers[_depth].bottom += _depth

        # renumber soil layers
        for _key1, _soil in sorted(soil.items()):
            i = 0
            for _key2, _layer in sorted(_soil.layers.items()):
                i += 1
                _layer.number = i
                #
                #
    #
    return soil, _depth
    #
#
#
def process_insert_soil_layer(_foundation, _depth):
    """
    """
    _foundation.soil, _depth = inser_soil_layer(_foundation.soil, _depth)
    return _foundation, _depth
#
#
def process_seismic_soil(_soil, _soil_depth):
    _depthTemp = []
    for _depth in _soil_depth:
        _soil, _temp = process_insert_soil_layer(_soil, _depth)
        _depthTemp.append(_temp)
    #
    #
    return _soil, _depthTemp
#
#