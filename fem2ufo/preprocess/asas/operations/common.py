# 
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import re

# package imports
import fem2ufo.process.control as process

#
# ----------------------
# Master word modules
#
def master_word(lineIn, keyWord, ind=0):
    """
    """
    _key = {"nodes": r"coor",
            "end": r"end",
            "sectionproperty": r"sect",
            "section": r"geom",
            "material": r"mate",
            "element": r"elem|pile",
            "hinges": r"rele",
            "constraint": r"cons",
            "fixity": r"supp",
            "masternode": r"link",
            "assembly": r"topo",
            "load": "load"}
    #
    keyWord[ind], lineOut, _match = select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
#
def freedom_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"0": r"x",
            "1": r"y",
            "2": r"z",
            "3": r"rx",
            "4": r"ry",
            "5": r"rz",
            "all": r"all",
            "repeat": r"(r)?rp"}

    keyWord[ind], lineOut, _match = select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
        # keyWord[3] = 'N/A'
    #
    return lineOut, keyWord
#
# ----------------------
# units modules
#
def units_word(lineIn, _unit):
    """
    """
    _key = {"metre": r"m(et[r]?e[r]?)?[s]?",
            "centimetre": r"c(enti)?m(et[r]?e[r]?)?[s]?",
            "millimetre": r"m(illi)?m((et[r]?e[r]?)?[s]?)?",
            "micrometre": r"mic(ro)?m(et[r]?e[r]?)?[s]?",
            "nanometre": r"nan[o]?m(et[r]?e[r]?)?[s]?",
            "inch": r"in(ch)?(es)?",
            "foot": r"f(oo|ee)?t"}

    _item, lineOut, _match = select(lineIn, _key, _unit)

    if _match:
        _case = 'length'
        _unit = process.units.find_length_unit(_item)
        _conv = process.units.Number(1.0, dims=_unit)
        _factor = _conv.value
        return lineOut, _case, _factor, _unit
    #
    _key = {"newton": r"n(ewton)?[s]?",
            "kilonewton": r"k(ilo)?n(ewton)?[s]?",
            "meganewton": r"m(ega)?n(ewton)?[s]?",
            "tonneforce": r"t(on)?nef(orce)?[s]?",
            "poundal": r"p(oun)?d[a]?l[s]?",
            "poundforce": r"p(ound)?f(orce)?[s]?|lbf[s]?",
            "kip": r"ki(lo)?p(ound)?[s]?",
            "tonforce": r"tonf(orce)?[s]?",
            "kilogramforce": r"k(ilo)?g(ram)?[s]?f(orce)?[s]?"}

    _item, lineOut, _match = select(lineIn, _key, _unit)

    if _match:
        _case = 'force'
        _unit = process.units.find_force_unit(_item)
        _conv = process.units.Number(1.0, dims=_unit)
        # N/m^2  without conversion result is: g/(m*s^2)
        _factor = _conv.value
        #
        return lineOut, _case, _factor, _unit
    #
    _key = {"centigrade": r"c(entigrade)?[s]?",
            "fahrenheit": r"f(ahrenheit)?"}
    #
    _item, lineOut, _match = select(lineIn, _key, _unit)

    if _match:
        _case = 'temperature'
        _unit = find_temp_unit(_item)
        _conv = Number(1.0, dims=_unit)
        _factor = _conv.value
        return lineOut, _case, _factor, _unit
    #
    print('error unit {:} no found'.format(_unit))
    #


#
def unit_factor(line, _length, _force):
    """
    """
    line = line.strip()
    line, item = word_pop(line)
    #
    _length_factor, _force_factor = process.units.convert_units_SI(_length, 
                                                                   _force)    
    _items = str(line).split()

    for _item in _items:
        line, _case, _factor, _unit = units_word(line, _item)

        if _case == 'length':
            _length_factor = _factor

        elif _case == 'force':
            # N/m^2  without conversion result is: g/(m*s^2)
            _force_factor = _factor/1000.0

        elif _case == 'temperature':
            _tempFactor = _factor
    #
    line = ''
    return line, _length_factor, _force_factor
#
#
#
#
# ----------------------
# TODO : relocate this in process module
def word_pop(lineIn, count=1):
    """
    """
    lineIn = lineIn.replace('+', '')
    _word = str(lineIn).split()
    _temp = re.compile(_word[0])
    lineOut = re.sub(_temp, " ", lineIn, count)
    lineOut = str(lineOut).strip()
    wordOut = str(_word[0]).strip()
    #
    return lineOut, wordOut


#
#
def remove_comment(_lineIn):
    """
    """
    pattern = r"\*"
    reobj = re.compile(pattern)
    _index = []
    for match in reobj.finditer(_lineIn):
        _index.append(match.start())
        # print(_index)
    # print(_lineIn[0:_index])
    _lineOut = _lineIn[0:_index[0]]
    _lineOut = _lineOut.strip()

    return _lineOut
#
#
def get_comment(_lineIn):
    """
    """
    pattern = r"\*"
    reobj = re.compile(pattern)
    _index = []
    for match in reobj.finditer(_lineIn):
        _index.append(match.start())
    #
    _lineOut = _lineIn[_index[1]:]
    _lineOut = _lineOut.replace("*", "")
    _lineOut = _lineOut.strip()

    return _lineOut
#
#
#
def split_line(line, flag, rigth=False):
    """
    """
    extension = line.split(flag)
    
    if rigth:
        line_out = extension[1].strip()
    else:
        line_out = extension[0].strip()
    
    return line_out
#
#
#
def select(lineIn, key, keyWord="N/A", count=1):
    """
    match
    """
    #
    lineOut = lineIn
    _match = False
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.match(lineIn)

        if keys:
            keyWord = _key
            lineOut = re.sub(keys.group(), " ", lineIn, count)
            _match = True

    lineOut = lineOut.strip()
    return keyWord, lineOut, _match
#
#
#
def select2(lineIn, key, keyWord="N/A", count=1):
    """
    search
    """
    #
    lineOut = lineIn
    _match = False
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        # keys = rgx.match(lineIn)
        keys = rgx.search(lineIn)

        if keys:
            keyWord = _key
            lineOut = re.sub(keys.group(), " ", lineIn, count)
            _match = True

    lineOut = lineOut.strip()
    return keyWord, lineOut, _match
#
#
def FE_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"element": r"elem(ent)?",
            "group": r"grou(p)?",
            "property": r"prop(erty)?",
            "all": r"all"}

    keyWord[ind], lineOut, _match = select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
def assign_property(item, remainder, _keyWord, geometry, _property, _argument):
    """
    """
    _factor = 1
    
    if _keyWord[2] == 'element':
        if _keyWord[3] == 'to':
            _total = int(item) - memb_no
            for x in range(_total):
                memb_no += 1
                _property(geometry, memb_no, _argument, _factor)
        #
        elif _keyWord[3] == 'all':
            for memb_no, _memb in geometry.concepts.items():
                _property(geometry, memb_no, _argument, _factor)
        #
        else:
            memb_no = int(item)
            _property(geometry, memb_no, _argument, _factor)
    #
    elif _keyWord[2] == 'group':
        if _keyWord[3] == 'to':
            _total = int(item) - remainder
            for x in range(_total):
                remainder += 1
                group = geometry.sets[str(remainder)]
                for memb_no in group.concepts:
                    _property(geometry, memb_no, _argument, _factor)
        elif _keyWord[3] == 'all':
            for group in geometry.sets:
                for memb_no in group.concepts:
                    _property(geometry, memb_no, _argument, _factor)
        else:
            remainder = int(item)
            group = geometry.sets[str(remainder)]
            for memb_no in group.concepts:
                _property(geometry, memb_no, _argument, _factor)
    #
    elif _keyWord[2] == 'property':
        print('fix property', item)
    #
    else:
        print('  *** error line data not recognised {:}', _keyWord[2])
    
    return remainder


