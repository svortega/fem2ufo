# 
# Copyright (c) 2009-2020 fem2ufo
# 

# Python stdlib imports
#import copy
import cmath
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.process.operations.joint as joint




# -----------------------------
#
def update_load_combinations(_model):
    """
    """
    #
    print('    * Updating Load Combinations')
    #
    for _name, _comb in _model.analysis.case.items():
        try:
            _load_case = get_basic_level(_comb.load_combination[0], 
                                         _model.load.functional)
        except KeyError:
            _load_case = None          
        
        if _load_case:
            try:
                get_metocean(_comb.load_combination[0], 
                             _load_case,
                             _comb.metocean_combination)
            except KeyError:
                print('   *** warning metocean condition {:} not found'
                      .format(_name))
                pass
        #
        #
        # if total > 1 then second load combination
        _total = len(_comb.load_combination)
        #
        if _total > 1:
            for x in range(1, _total):
                _load_case = get_top_level(_comb.load_combination[x], 
                                           _comb.load_combination[x-1])
                
                if _load_case:
                    print('   ** warning check here --> load combination')
    #
#
def get_top_level(combination, basic_load):
    """
    """
    _load_dummy = {}
    for _lcomb in combination.values():
        _new_list = []
        for x in range(len(_lcomb.functional_load)):
            _number = _lcomb.functional_load[x][0]
    
            try:
                basic_load[_number]
                _new_list.append(_lcomb.functional_load[x])
            except KeyError:
                try:
                    _load_dummy[_number].append([_lcomb.number, _lcomb.functional_load[x]])
                except KeyError:
                    _load_dummy[_number] = [[_lcomb.number, _lcomb.functional_load[x]]]
        #
        _lcomb.functional_load = _new_list
    #
    return _load_dummy
#
def get_basic_level(combination, functional):
    """
    """
    _load_dummy = {}
    _load_name = [key for key in sorted(combination, key = lambda name: combination[name].number)]
    
    for _name in _load_name:
        _lcomb = combination[_name]    
        _new_list = []
        for x in range(len(_lcomb.functional_load)):
            _number = _lcomb.functional_load[x][0]
            try:
                functional[_number]
                _new_list.append(_lcomb.functional_load[x])
            except KeyError:
                try:
                    _load_dummy[_number].append([_lcomb.number, _lcomb.functional_load[x]])
                except KeyError:
                    _load_dummy[_number] = [[_lcomb.number, _lcomb.functional_load[x]]]
        #
        _lcomb.functional_load = _new_list        
    #
    return _load_dummy
#
def get_metocean(combination, _load_case, metocean):
    """
    """
    # metocean load case
    try:
        # Metocean combination (wave, current & wind)
        for _name, _metocean in sorted(metocean.items()):
            #if _wave.number == 3:
            #    print('--->')
            try:
                for _lcomb in _load_case[_name]:
                    #_comb = [_name, _lcomb[1][1]]           # for js
                    _comb = [_metocean.name, _lcomb[1][1]]  # for csv
                    combination[str(_lcomb[0])].metocean_load.append(_comb)
            except KeyError:
                continue
    except KeyError:
        return    
#
# -----------------------------
#
def get_bells(_concept, _tubular_joint):
    """
    """
    # Find if conductor bells
    del_joint = []
    _atol = 4 * cmath.pi / 180.0
    for key, _jnt in _tubular_joint.items():
        _branch = {}
        #if key == '933':
        #    print('-->')
        
        for _membNo in _jnt.node.elements:
            _member = _concept[_membNo]
            _n0 = _member.node[0]
            _n1 = _member.node[1]
            _branch[_member.name] = [_n0, _n1] 
        
        _chord, _overlapping = chord(_jnt, _concept, _branch, _atol)
        _overlapping = list(set(_overlapping))
        _jnt.chord = _chord
        _brace = list(set(_jnt.brace) - set(_chord))
        if _brace:
            _jnt.brace = _brace
        else:
            #print('????')
            _branch = set(key for key in _branch.keys())
            _brace = list(_branch - set(_chord))
            _jnt.brace = _brace            
        #
        #
        for _item in _brace:
            _jnt.node.elements.remove(_item)
        
        if _overlapping:
            _jnt.overlap = _overlapping
            _new_node = [0,0]
            
            for _item in _chord:
                _memb = _concept[_item]
                
                #if _item == '3575':
                #    print('here')                
                
                if _jnt.node.equal(_memb.node[0]):
                    if _jnt.node.name != _memb.node[0].name:
                        _memb.node[0] = _jnt.node
                
                elif _jnt.node.equal(_memb.node[1]):
                    if _jnt.node.name != _memb.node[1].name:
                        _memb.node[1] = _jnt.node
                #
                else:
                    print('    *** error joint {:} has no colinear nodes'.format(key))
                    1/0
                    del_joint.append(key)
                    break                    
                #
                if _jnt.node.name != _memb.node[0].name:
                    
                    if _jnt.node.z < _memb.node[0].z:
                        _new_node[0] = _memb.node[0]
                    
                    else:
                        _new_node[1] = _memb.node[0]                    
                    
                else:
                    if _jnt.node.z < _memb.node[1].z:
                        _new_node[0] = _memb.node[1]
                    
                    else:
                        _new_node[1] = _memb.node[1] 
            #
            #
            for _item in _overlapping:
                _memb = _concept[_item]
                
                #if _item == '3575':
                #    print('here')                 
                
                if _jnt.node.equal(_memb.node[0]):
                    if _jnt.node.name != _memb.node[0].name:
                        _memb.node[0] = _jnt.node
                
                elif _jnt.node.equal(_memb.node[1]):
                    if _jnt.node.name != _memb.node[1].name:
                        _memb.node[1] = _jnt.node
                #
                else:
                    print('    *** error joint {:} has no colinear nodes'.format(key))
                    1/0
                    del_joint.append(key)
                    break
                #
                if _jnt.node.name == _memb.node[0].name:
                    if _jnt.node.z < _memb.node[1].z:
                        _memb.node[0] = _new_node[0]
                    else:
                        _memb.node[0] = _new_node[1]
                else:
                    if _jnt.node.z < _memb.node[0].z:
                        _memb.node[1] = _new_node[0]
                    else:
                        _memb.node[1] = _new_node[1]
        #
        else:
            # assume this is a conductor suported by the topside
            if not _chord:
                _jnt.chord = _jnt.brace
                _jnt.brace = list(set(_jnt.node.elements) - set(_jnt.brace))
            #else:
                #print('??? fix joint')
                #1/0
    #
    for key in del_joint:
        del _tubular_joint[key]
    #
    return _tubular_joint
    #
    #
#
def find_overlapping_beam(nodes, concept, tol=1.5):
    """
    """
    _dummy_joint = {}
    #
    # create dummy joints for joints with > 1 elements
    for _node in nodes.values():
        if len(_node.elements) > 1:
            _dummy_joint[_node.number] = f2u.Geometry.Joints(_node.number, 
                                                              _node)
    #
    # tolerance in dregrees
    _atol = tol * cmath.pi / 180.0
    #
    for _jnt in _dummy_joint.values(): 
        _branch = {}
        for _membNo in _jnt.node.elements:
            _member = concept[_membNo]
            _n0 = _member.node[0]
            _n1 = _member.node[1]
            _branch[_member.name] = [_n0, _n1] 
        
        _chord, _overlapping = chord(_jnt, concept, _branch, _atol)
        
        if _overlapping:
            for _membNo in _overlapping:
                concept[_membNo].overlap = True           
    #
#
def chord(_joint, _element, _branch, _atol):
    """
    Find chord and braces members
    """
    #
    _brace = []
    _toremove = []
    _chrd = []
    _branch_new = dict(_branch)

    # find chords of branch based on tolerance (tol)
    for m1, [im1, jm1] in _branch.items():
        if im1.name != _joint.node.name:
            jm1 = im1
            im1 = _joint.node
        # find direction cosines chord 1
        try:
            c1l, c1m, c1n = joint.dcosr(im1, jm1)
        except ZeroDivisionError:
            _memb_1 = _element[m1]
            im1 = _element[m1].node[0]
            jm1 = _element[m1].node[1]
            #print('---->')
            c1l, c1m, c1n = joint.dcosr(im1, jm1)
        # delete current branch
        del _branch_new[m1]
        # iterate with remaining branchs
        for m2, [im2, jm2] in _branch_new.items():
            if im2.name != _joint.node.name:
                jm2 = im2
                im2 = _joint.node
            # find direction cosines chord 2
            try:
                c2l, c2m, c2n = joint.dcosr(im2, jm2)
            except ZeroDivisionError:
                _memb_2 = _element[m2]
                im2 = _element[m2].node[0]
                jm2 = _element[m2].node[1]
                #print('---->')
                #try:
                c2l, c2m, c2n = joint.dcosr(im2, jm2)
                #except ZeroDivisionError:
                #    xxx = _element[m2]
                #    print('????')

            # find angle between chord 1 & 2
            _ang_complex = cmath.acos(c1l * c2l + c1m * c2m + c1n * c2n)
            _ang = _ang_complex.real
            # in angle << tolerance, probably an overlaping member
            if _ang <= _atol:
                # member with smallest D, t or Fy will be eliminated
                # assuming nopt = 1 (to be fixed)
                ts, tr, _msg = joint.selec(m1, m2, _element)
                _toremove.append(tr)
                continue
            # check if member is a brace
            _ang2 = abs(_ang - cmath.pi)
            if _ang2 > _atol:
                _brace.append(m2)
                continue
            # then these 2 members form the chord
            _chrd.append([m1, m2])
    #
    # delete elements probably overlaping existing members
    _chord = []
    for _items in _chrd:
        if set(_items) & set(_toremove):
            continue
        _chord.extend(_items)
    #
    _brace = set(_brace)    
    #
    if len(_chord) > 2:
        from operator import itemgetter
        _diam = {}
        for _name in _chord:
            _memb_1 = _element[_name]
            _diam[_name] = _memb_1.section[0].properties.D
        
        _chord = [key for key, value in sorted(_diam.items(), key=itemgetter(1), reverse=True)]
        #print('--->')
    #
    return _chord, _toremove
    #
#
def get_joints(_concept, nodes, _tubular_joint):
    """
    """
    # Find joints 
    #_memb_no = [_memb.number for _memb in _concept.values()]
    #_memb_no = max(_memb_no)
    #del_joint = []
    _atol = 4 * cmath.pi / 180.0
    for key, _jnt in _tubular_joint.items():
        _branch = {}
        #if key == '931':
        #    print('-->')
        #
        _flag = False
        for _membNo in _jnt.node.elements:
            _member = _concept[_membNo]
            _n0 = _member.node[0]
            _n1 = _member.node[1]
            
            if _jnt.node.equal(_n0) or _jnt.node.equal(_n1):
                _branch[_member.name] = [_n0, _n1]
                _newname = _member.name
            else:
                print('    *** warning joint {:} has no colinear nodes'.format(key))
                _jnt.type = 'interface'
                _flag = True
                break
                #
                #_memb_name = key + '_' + _jnt.name
                #try:
                    #_concept[_memb_name]
                    #print('    *** joint {:} deleted'.format(key))
                    #_jnt.type = 'interface'
                    ##del_joint.append(key)
                    #_flag = True
                    #break
                #except KeyError:
                    #print('    *** new element {:} created'.format(_memb_name))
                    #_memb2 = _concept[_newname]
                    #_memb_no =+ 1
                    #_concept[_memb_name] = f2u.Concept(_memb_name, _memb_no)
                    #_concept[_memb_name].type = 'beam'
                    #_concept[_memb_name].material.append(_memb2.material[0])
                    #_memb2.material[0].concepts.append(_memb_name)
                    #
                    #_node1 = _jnt.node.name
                    #_concept[_memb_name].node.append(nodes[_node1])
                    #nodes[_node1].elements.append(_memb_name)
                    #
                    #_node2 = _jnt.name
                    #_concept[_memb_name].node.append(nodes[_node2])
                    #nodes[_node2].elements.append(_memb_name)
                    #
                    #_concept[_memb_name].section.append(_memb2.section[0])
                    #_memb2.section[0].elements.append(_memb_name)                    
                    #
                    #_jnt.node = nodes[_node2]
                    #del _branch[_newname]
                #
        #
        #continue
        if _flag:
            #_memb_name = key + '_' + _jnt.name
            #_memb2 = _concept[_memb_name]
            #try:
            #    _memb2.material[0].concepts.remove(_memb_name)
            #    _memb2.section[0].elements.remove(_memb_name)
            #except ValueError:
            #    pass
            #del _concept[_memb_name]
            continue
        # define joint
        _chord, _overlapping = chord(_jnt, _concept, _branch, _atol)
        _overlapping = list(set(_overlapping))
        
        # get chord
        if _chord:
            _jnt.chord = _chord
        else:
            _atol = 25 * cmath.pi / 180.0
            _chord, _overlapping = chord(_jnt, _concept, _branch, _atol)
            _atol = 4 * cmath.pi / 180.0
            
            if _chord:
                _jnt.chord = _chord
            else:
                print('    *** joint {:} deleted'.format(key))
                _jnt.type = 'interface'
                #_jnt.node = nodes[key]
                #_memb_name = key + '_' + _jnt.name
                #_memb2 = _concept[_memb_name]
                #try:
                #    _memb2.material[0].concepts.remove(_memb_name)
                #    _memb2.section[0].elements.remove(_memb_name)
                #except ValueError:
                #    pass                
                #del _concept[_memb_name]
                ##del_joint.append(key)
                continue
                #print('-->')
        # get braces
        _brace = list(set(_jnt.brace) - set(_chord))
        if _brace:
            _jnt.brace = _brace
        else:
            _branch = [key for key in _branch.keys()]
            _brace = list(set(_branch) - set(_chord))
            _jnt.brace = _brace
        
    #
    #for key in del_joint:
    #    del _tubular_joint[key]
    #
    return _tubular_joint    
#