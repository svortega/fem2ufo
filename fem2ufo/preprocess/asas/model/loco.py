# 
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import sys
import re
import os
from itertools import islice

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
#
import fem2ufo.preprocess.asas.operations.common as asas_common
import fem2ufo.preprocess.asas.operations.load as load_operation

#
#
def get_line(fin):
    """
    """
    line2 = list(islice(fin, 1))
    line2 = ' '.join(map(str, line2))
    #
    # find key words and clean lines from comments
    if re.search(r"\*", line2):
        line2 = asas_common.remove_comment(line2)
    #
    line2 = line2.strip()
    #line2 = line2.upper()
    return line2
#
#
def word_pop(line_in, count=1):
    """
    """
    _word = str(line_in).split()
    _temp = re.compile(_word[0])
    line_out = re.sub(_temp, " ", line_in, count)
    line_out = str(line_out).strip()
    word_out = str(_word[0]).strip()
    #
    return line_out, word_out
#
# ----------------
#
def master_load(lineIn, keyWord, ind=0):
    """
    """
    _key = {"load": r"load",
            "component": r"topo"}
    #
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
#
def topo_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"mirrow": r"mirr",
            "rotate": r"dcos",
            "translated": r"orig",
            "end" : r"end"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
    #
#
#
def load_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"link": r"\@",
            "group": r"grou[p]?",
            "loadcase": r"case",
            "loadnode": r"nodal\s*lo(ad)?",
            "distributed": r"distribu(te)?[d]?",
            "gravity": r"body\s*for(ce)?",
            "temperature": r"temperat(ure)?",
            "pressure": r"pressure",
            "prescribed": r"prescrib(ed)?",
            "centrifugal": r"centrifu(gal)?",
            "angular": r"ang\s*acce(leration)?",
            "component": r"comp\s*loa[d]?",
            "mass": r"dire",
            "addedmass": r"lump",
            "consistent": r"cons(istent)?",
            "end": r"end"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
# ----------------
#
def load_word_loco(lineIn, keyWord, ind=1):
    """
    """   
    _key = {"case": r"case",
            "load_id": r"sele",
            "structure" : r"structure",
            "component" : r"component",
            "new_load" : r"newcase",
            "end" : r"end"}
    #
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
#
def master_load_loco(lineIn, keyWord, ind=0):
    """
    """
    _key = {"combination": r"COMB",
            "stop" : r"stop"}
    #
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
# ----------------
#
#
#
def read_asas_load_combination(file_name, design_condition, model,
                               combination_name, super_element=False):
    """
    """
    print('')
    print('-------------- ASAS Load Combination Module --------------')
    #
    if not model.analysis:
        model.set_analysis(combination_name)
    #
    if super_element:
        level = len(model.analysis.case)
        _component_name = model.component.name
        _component_name = _component_name.split('.')
        _component_name = _component_name[0].lower()
        
        _component = {}
        _component[_component_name] = []
        
        _load_comb, _link = read_file_global(file_name, _component,
                                             design_condition, level)
        
        for _number, _items in _link.items():
            try:
                _load_comb[_number]
            except KeyError:
                _load_comb[_number] = f2u.Load.Combination(_number, 
                                                           int(_number), 
                                                           level)
                _load_comb[_number].design_condition = design_condition
            
            for _file in _items:
                if not _load_comb[_number].name:
                    _load_comb[_number].name = _file[1]
                    
                read_file_local(_file[0], _component, _load_comb[_number],
                                design_condition, level)
        #
        #try:
        model.analysis.case[combination_name].load_combination[level] = _load_comb
        #model['load'].combination_level[level] = _load_comb
        #
    #
    else:
        # check if combination
        try:
            _load_comb = read_file_component(file_name, model, design_condition,
                                             model.analysis.case[combination_name].load_combination[0])
        except KeyError:
            _load_comb = read_file_component(file_name, model, design_condition)
        # Update load combination
        try:
            model.analysis.case[combination_name].load_combination[0].update(_load_comb)
        except KeyError:
            model.analysis.case[combination_name] = f2u.Analysis.Case(combination_name)
            model.analysis.case[combination_name].load_combination[0] = _load_comb
        #
        #model['load'].combination[0] = f2u.CombinationLevel(0, 'loading')
        #model['load'].combination[0].level = _load_comb
        #
    #
    return model
    
#
# TODO : Why a second load pass here?
def read_file_componentX(file_name, model, design_condition,
                        _load_comb=None):
    """
    """
    #
    asas_file = process.common.check_input_file(file_name)
    if not asas_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    #
    if not _load_comb:
        _load_comb = {}
    #
    print('')
    print('--- Reading load combination data from {:} input file'.format(file_name))
    print('')
    print('    * First pass')
    
    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    dat_temp = open('CheckMe_'+ asas_file,'w+')
    #
    #
    with open(asas_file) as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            #
            # clean lines from comments
            if re.search(r"\*", line):
                line = asas_common.remove_comment(line)
            #
            line = line.strip()
            if not line:
                continue
            #
            line, _keyWord = master_load_loco(line, _keyWord)
            #
            if _keyWord[0] == 'stop':
                break            
            #
            elif _keyWord[0] == 'combination':
                #
                if not line:
                    continue
                #
                line, _keyWord = load_word_loco(line, _keyWord, 1)
                #
                if _keyWord[1] == 'end':
                    _keyWord[1] = 'N/A'
                #
                elif  _keyWord[1] == 'load_id':
                    line, _load_no = word_pop(line)
                    #_load_no = int(_load_no)
                    _load_name = load_operation.load_title(line)
                    _load_name = 'LCA' + str(_load_no).zfill(3) + '_' + _load_name
                    #
                    if int(_load_no) == 999:
                        print('???')                    
                    #
                    _load_comb[_load_no] = f2u.Load.Combination(_load_name,
                                                                int(_load_no))
                    _load_comb[_load_no].design_condition = design_condition
                #
                elif _keyWord[1] == 'case':
                    line, _load_number = word_pop(line)
                    line, _factor = word_pop(line)
                    _load_comb[_load_no].functional_load.append([_load_number, float(_factor)])
                    #_load_comb[_load_no].factor.append(float(_factor))
                    #print('-->')
                #
                elif _keyWord[1] == 'new_load':
                    line, _load_new = word_pop(line)
                    line, _factor = word_pop(line)
                    #
                    try:
                        _load_comb[_load_no]
                    except KeyError:
                        _load_comb[_load_no] = f2u.Load.Combination(_load_name, 
                                                                        int(_load_no))
                        _load_comb[_load_no].design_condition = design_condition
                    #
                    try:
                        _new_list = []
                        for _items in _load_comb[_load_new].functional_load:
                            _new_list.append([_items[0], _items[1] * float(_factor)])
                        #
                        _load_comb[_load_no].functional_load.extend(_new_list)
                    except KeyError:
                        print('   *** warining load combination {:} not found'
                              .format(_load_new))                    
                    #continue
                
                else:
                    print('   *** error line {:}'.format(line))
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    #
    print('    * Second pass')
    #
    with open(asas_file) as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            # clean lines from comments
            if re.search(r"\*", line):
                line = asas_common.remove_comment(line)
            #
            line = line.strip()
            if not line:
                continue
            #
            line, _keyWord = master_load_loco(line, _keyWord)
            #
            if _keyWord[0] == 'stop':
                break            
            #
            elif _keyWord[0] == 'combination':
                #
                if not line:
                    continue
                #
                line, _keyWord = load_word_loco(line, _keyWord, 1)
                #
                if _keyWord[1] == 'end':
                    _keyWord[1] = 'N/A'
                #
                elif  _keyWord[1] == 'load_id':
                    line, _load_no = word_pop(line)
                    _load_name = load_operation.load_title(line)
                    _load_name = 'LCA' + str(_load_no).zfill(3) + '_' + _load_name
                    if int(_load_no) == 999:
                        print('here')                    
                #
                elif _keyWord[1] == 'new_load':
                    line, _load_new = word_pop(line)
                    line, _factor = word_pop(line)
                    #
                    try:
                        _load_comb[_load_no]
                    except KeyError:
                        _load_comb[_load_no] = f2u.Load.Combination(_load_name, 
                                                                    int(_load_no))
                        _load_comb[_load_no].design_condition = design_condition
                    #
                    try:
                        _new_list = []
                        for _items in _load_comb[_load_new].functional_load:
                            _new_list.append([_items[0], _items[1] * float(_factor)])
                        #
                        _load_comb[_load_no].functional_load.extend(_new_list)
                    except KeyError:
                        print('   *** warining load combination {:} not found'
                              .format(_load_new))
                    #
    #
    #
    print('--- End reading {:} file'.format(file_name)) 
    #
    #    
    return _load_comb
#
def read_file_component(file_name, model, design_condition,
                        _load_comb=None):
    """
    """
    #
    asas_file = process.common.check_input_file(file_name)
    if not asas_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    #
    if not _load_comb:
        _load_comb = {}
    #
    print('')
    print('--- Reading load combination data from {:} input file'.format(file_name))
    print('')
    print('    * First pass')

    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    dat_temp = open('CheckMe_'+ asas_file,'w+')
    #
    #
    with open(asas_file) as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            #
            # clean lines from comments
            if re.search(r"\*", line):
                line = asas_common.remove_comment(line)
            #
            line = line.strip()
            if not line:
                continue
            #
            line, _keyWord = master_load_loco(line, _keyWord)
            #
            if _keyWord[0] == 'stop':
                break            
            #
            elif _keyWord[0] == 'combination':
                #
                if not line:
                    continue
                #
                line, _keyWord = load_word_loco(line, _keyWord, 1)
                #
                if _keyWord[1] == 'end':
                    _keyWord[1] = 'N/A'
                #
                elif  _keyWord[1] == 'load_id':
                    line, _load_no = word_pop(line)
                    #_load_no = int(_load_no)
                    _load_name = load_operation.load_title(line)
                    _load_name = 'LCA' + str(_load_no).zfill(3) + '_' + _load_name
                    #
                    #if int(_load_no) == 999:
                    #    print('???')                    
                    #
                    _load_comb[_load_no] = f2u.Load.Combination(_load_name,
                                                                int(_load_no))
                    _load_comb[_load_no].design_condition = design_condition
                #
                elif _keyWord[1] == 'case':
                    line, _load_number = word_pop(line)
                    line, _factor = word_pop(line)
                    _load_comb[_load_no].functional_load.append([_load_number, float(_factor)])
                    #_load_comb[_load_no].factor.append(float(_factor))
                    #print('-->')
                #
                elif _keyWord[1] == 'new_load':
                    line, _load_new = word_pop(line)
                    line, _factor = word_pop(line)
                    #
                    try:
                        _load_comb[_load_no]
                    except KeyError:
                        _load_comb[_load_no] = f2u.Load.Combination(_load_name, 
                                                                    int(_load_no))
                        _load_comb[_load_no].design_condition = design_condition
                    #
                    try:
                        _new_list = []
                        for _items in _load_comb[_load_new].functional_load:
                            _new_list.append([_items[0], _items[1] * float(_factor)])
                        #
                        _load_comb[_load_no].functional_load.extend(_new_list)
                    except KeyError:
                        print('   *** warining load combination {:} not found'
                              .format(_load_new))                    
                    #continue

                else:
                    print('   *** error line {:}'.format(line))
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    print('--- End reading {:} file'.format(file_name)) 
    #
    #    
    return _load_comb    
    #
#
# ----------------
#
# ----------------
#
#
def read_file_global(file_name, _component, 
                     design_condition, level):
    """
    """
    #
    asas_file = process.common.check_input_file(file_name)
    if not asas_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    #
    print('')
    print('--- Reading load combination data from {:} input file'
          .format(file_name))
    print('')

    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    #
    _component_name = list(_component.keys())
    _component_name = _component_name[0]
    #
    #_functional = {}
    #
    _load_comb = {}
    #
    print('    * First pass')
    #
    dat_temp = open('datTemp.dat','w+')
    #
    # find component identification
    with open(asas_file) as fin:
        for line2 in fin: 
            # Jump commented lines
            if re.match(r"\*", line2):
                continue
            # find key words and clean lines from comments
            if re.search(r"\*", line2):
                line2 = asas_common.remove_comment(line2)
            #
            line, _keyWord = master_load(line2, _keyWord)
            if not line:
                continue
            #
            if _keyWord[0] == 'component':
                line, _keyWord = topo_word(line, _keyWord, 1)
                #
                if not line:
                    continue
                #
                elif _keyWord[1] == 'end':
                    _keyWord = ['load', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
                #
                elif _keyWord[1] == 'mirrow':
                    print('  --> fix mirrow')
                #
                elif _keyWord[1] == 'rotate':
                    print('  --> fix rotate')
                #
                elif _keyWord[1] == 'translated':
                    print('  --> fix translated')
                #
                else:
                    if _component_name in line.lower():
                        _data = line.split()
                        _component[_component_name] = _data[1]
                        #print('here')
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    #
    #
    if not _component[_component_name]:
        print('   *** warning no component found')
        os.remove('datTemp.dat')
        return None, None
    #
    print('    * Second pass')
    #
    _link = {}
    #
    dat_temp = open('CheckMe_'+ asas_file,'w+')
    #
    with open('datTemp.dat') as fin:
        for line in fin: 
            #
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            #
            # find key words and clean lines from comments
            if re.search(r"\*", line):
                line = asas_common.remove_comment(line)
            #
            if not line:
                continue
            #
            line, _keyWord = master_load(line, _keyWord)
            #
            #
            if _keyWord[0] == 'stop':
                break             
            #
            if _keyWord[0] == 'load':
                line, _keyWord = load_word(line, _keyWord)
                
                if re.match(r"\/", line):
                    continue
                #
                if not line:
                    continue
                #
                elif _keyWord[1] == 'loadcase':
                    line, _load_no = asas_common.word_pop(line)
                    _load_no = _load_no
                    _load_name = load_operation.load_title(line)
                    _load_name = 'LCA' + str(_load_no).zfill(3) + '_' + _load_name
                    #
                    #if int(_load_no) == 611:
                    #    print('???')
                    #
                    while True:
                        line2 = get_line(fin)
                        if not line2:
                            continue
                        #
                        elif 'end' in line2.lower():
                            _keyWord[1] = 'N/A'
                            break
                        #
                        elif 'comp' in line2.lower():
                            continue
                        #
                        elif  '@' in line2:
                            #_file = line2.lower()
                            _file = line2.replace('@', '')
                            try:
                                _link[_load_no].appen([_file.strip(), _load_name])
                            except KeyError:
                                _link[_load_no] = [[_file.strip(), _load_name]]
                        #
                        elif str(_component[_component_name]).lower() in line2.lower():
                            #print('here')
                            line2, _comp = word_pop(line2)
                            line2, _load_number = word_pop(line2)
                            line2, _factor = word_pop(line2)
                            try:
                                _load_comb[_load_no].functional_load.append([_load_number, 
                                                                             float(_factor)])
                            except KeyError:
                                _load_comb[_load_no] = f2u.Load.Combination(_load_name,
                                                                                int(_load_no), 
                                                                                level)
                                _load_comb[_load_no].design_condition = design_condition
                                _load_comb[_load_no].functional_load.append([_load_number, float(_factor)])
                        #
                        #
                        else:
                            dat_temp.write("\n")
                            dat_temp.write(line2)
                #                
                elif _keyWord[1] == 'end':
                    _keyWord = ['load', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
                #
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    os.remove('datTemp.dat')
    #
    #
    #
    #
    print('--- End reading {:} file'.format(file_name))
    #    
    return _load_comb, _link
#
def read_file_local(file_name, _component,  _load_comb,
                    design_condition, level):
    """
    """
    asas_file = process.common.check_input_file(file_name)
    if not asas_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    #
    #print('')
    print('--- Reading local load comb data from @{:} input file'
          .format(file_name))
    #print('')
    #
    _component_name = list(_component.keys())
    _component_name = _component_name[0]
    
    #_load_comb = {}
    #_load_no = _load_comb.number
    #_load_name = _load_comb.name
    
    dat_temp = open('CheckMe_'+ asas_file,'w+')
    #
    with open(asas_file) as fin:
        for line2 in fin: 
            # Jump commented lines
            if re.match(r"\*", line2):
                continue
            #
            # find key words and clean lines from comments
            if re.search(r"\*", line2):
                line2 = asas_common.remove_comment(line2)
            #
            if not line2:
                continue
            #
            elif str(_component[_component_name]).lower() in line2.lower():
                #print('here')
                line2, _comp = word_pop(line2)
                line2, _load_number = word_pop(line2)
                line2, _factor = word_pop(line2)
                _load_comb.functional_load.append([_load_number, 
                                                   float(_factor)])
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line2)
    #
    dat_temp.close()
    #
    #return _load_comb
#