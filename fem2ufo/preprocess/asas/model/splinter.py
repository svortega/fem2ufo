# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports
import math
import re
import sys


# package imports
import fem2ufo.f2u_model.control as f2u
#import fem2ufo.f2u_model.feclass.common as userdata
#import fem2ufo.f2u_model.feclass.soil as soil_FE
import fem2ufo.process.control as process
import fem2ufo.preprocess.asas.model.geometry as geometry
import fem2ufo.preprocess.asas.operations.common as asas_common
import fem2ufo.preprocess.asas.operations.pilesoil as pilesoil
from fem2ufo.preprocess.common.foundation import get_spring_material

# --------------------------------------------------------------------
#
#
def get_piles(SPLTinp, global_units,
              mudline=False):
    """
    """
    print('    * Pile Geometry')
    #
    _pile = geometry.read_asas_geometry(SPLTinp, 
                                        global_units)
    # Pile model 
    # create new pile group
    _pgroup = f2u.Geometry.Groups('_pile', 1)

    # add pile member's name to the pile group
    for _name in _pile.concepts.keys():
        _pgroup.items.append(_name)

    # Create pile model

    # split pile group in subgroups with members sharing node ends
    _pgroup, _node_toptip = process.geometry.split_sets(_pgroup.items, 
                                                        _pile.concepts, 
                                                        _pgroup.name)
    # need to add here finding top & bottom node
    _pgroup, _node_toptip = pilesoil.sort_group_top_node(_pgroup, _node_toptip, _pile)
    #
    #
    #
    _pile.sets = {}
    _setnumber = 0

    # element sets
    for _name, _set in sorted(_pgroup.items()):
        _setnumber = int(_name.replace('_pile_', ''))
        _pile.sets[_setnumber] = f2u.Geometry.Groups(_name, _setnumber)
        _pile.sets[_setnumber].items = _set
    #
    # end nodes group
    _setnumber = len(_pile.sets)
    for _name, _set in sorted(_node_toptip.items()):
        _namenumber = int(_name.replace('_pile_', ''))
        _setnumber2 = _setnumber + _namenumber
        _name2 = 'end_nodes__' + str(_namenumber)
        _pile.sets[_setnumber2] = f2u.Geometry.Groups(_name2, _setnumber2)
        _pile.sets[_setnumber2].nodes = _set
    #
    if mudline:
        for _node_no in _node_toptip.values():
            _node = _pile.nodes[_node_no[0]]
            mudline = math.copysign(mudline, _node.z)
            _node.coordinates._replace(z = mudline)
    #
    #
    return _pile, mudline
#
#
# FIXME : ugly code
def read_splinter(splt_file, global_units):
    """
    """
    #
    SPLTinp = process.common.check_input_file(splt_file)
    if not SPLTinp:
        print('    ** I/O error : splinter file {:} not found'
              .format(splt_file))
        sys.exit()    
    #
    soil = {}
    _PY = {}
    _TZ = {}
    _QZ = {}
    _soilNo = 0
    #
    #   Define Constans
    #
    i = 0
    nolayer = 0
    UnitFactF = 1
    UnitFactD = 1
    UnitFactQF = 1
    #
    print('')
    print('------------------- REDSOIL Module -----------------')
    print('--- Reading SPLINTER file')
    #
    #
    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    _node = {}
    _section = {}
    _material = {}
    _element = {}
    _group = {}
    _load = {}
    #
    _user_data = {}
    _user_data['GENSOD'] = f2u.InputUserData()
    _user_data['GENSOD'].file_name = SPLTinp
    #
    _nolayer = 0
    #
    # Units [length, mass, time, temperature, force, pressure/stress]
    global_length = global_units[0]
    global_force = global_units[4]
    
    _length_factor, _force_factor = process.units.convert_units_SI(global_length,
                                                            global_force)
    #
    # here factors are getting for unit conversion purposes
    # units_output = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
    # factors, _grav = process.get_factors_and_gravity(_units_input,
    #                                                 _units_output)    
    #
    print('    * Soil')
    #
    _isoil = 0
    _pyNo = 0
    _tzNo = 0
    _qzNo = 0
    _springNo = 0
    #
    _pfactor = 1.0
    _yfactor = 1.0
    _tfactor = 1.0
    _zfactor = 1.0
    #
    for line in open(SPLTinp):
        # Segment line
        line = line.strip()
        if not line: 
            continue
        # Jump commented lines
        if re.match(r"\*", line): 
            continue
        #if 'slid' in line.lower():
        #    print('-->', line)
        # remove commented sections
        if re.search(r"\*", line): 
            line = asas_common.remove_comment(line)
        
        keyword = line.split()
        line, _keyWord = pilesoil.spt_master_word(line, _keyWord)
        #
        # --------------------------------------------------------------------
        #                             Read Soil Data
        # --------------------------------------------------------------------
        #
        #
        if 'UNITS' in line.upper() and _keyWord[0] == 'N/A':
            if re.search(r"disp|stre", line, re.IGNORECASE):
                continue
            line, _length_factor, _force_factor= asas_common.unit_factor(line, 
                                                                         global_length,
                                                                         global_force)
        #
        elif _keyWord[0] == 'soil':
            if not line:
                continue
            # find units
            if 'UNITS' in line.upper():
                line, _length_factor, _force_factor= asas_common.unit_factor(line, 
                                                                             global_length,
                                                                             global_force)
            # find slide
            #elif 'slid' in line.lower():
            #    _data = line.split()
            #    try:
            #        _isoil = int(_data[0])
            #        
            #    except ValueError:
            #        print('-->')
            # find soil data
            elif re.match(r"\d", line):
                line, _keyWord = pilesoil.soil_word2(line, _keyWord, 1)
                #
                if 'END' in line:
                    _length_factor, _force_factor = process.units.convert_units_SI(global_length, 
                                                                                   global_force)
                    _pfactor = 1.0
                    _yfactor = 1.0
                    _tfactor = 1.0
                    _zfactor = 1.0
                    line = ''
                # find PY soil data
                elif _keyWord[1] == 'PY':
                    line, _soilNo = asas_common.word_pop(line)
                    line, _layerdepth = asas_common.word_pop(line)
                    _layerdepth = float(_layerdepth)
                    _isoil = _soilNo
                    _springNo += 1
                    _pyNo = _springNo
                    _PY[_pyNo] = f2u.Geometry.SpringElement('Py', _pyNo)

                    try:
                        soil[_isoil].layers[_layerdepth].curve['PY'] = _PY[_pyNo]
                    except KeyError:
                        _nolayer = 1
                        _zbottom = float(_mudline) - float(_layerdepth)
                        soil[_isoil] = f2u.Foundation.Soil(int(_soilNo), _isoil)
                        soil[_isoil].layers[_layerdepth] = f2u.Foundation.Layer(_layerdepth, _nolayer)
                        soil[_isoil].layers[_layerdepth].depth = float(_layerdepth)
                        #soil[_isoil].layers[_layerdepth].bottom = _zbottom
                        soil[_isoil].mudline = float(_mudline)
                        soil[_isoil].diameter = 1.0
                        soil[_isoil].layers[_layerdepth].curve['PY'] = _PY[_pyNo]
                        soil[_isoil].diameter = 1
                        # print(' P-Y ==>', line)
                # find TZ soil data
                elif _keyWord[1] == 'TZ':
                    line, _soilNo = asas_common.word_pop(line)
                    line, _layerdepth = asas_common.word_pop(line)
                    _layerdepth = float(_layerdepth)
                    _isoil = _soilNo
                    _springNo += 1
                    _tzNo = _springNo
                    _TZ[_tzNo] = f2u.Geometry.SpringElement('Tz', _tzNo)

                    try:
                        soil[_isoil].layers[_layerdepth].curve['TZ'] = _TZ[_tzNo]
                    except KeyError:
                        _nolayer = 1
                        _zbottom = float(_mudline) - float(_layerdepth)
                        soil[_isoil] = f2u.Foundation.Soil(int(_soilNo), _isoil)
                        soil[_isoil].layers[_layerdepth] = f2u.Foundation.Layer(_layerdepth, _nolayer)
                        soil[_isoil].layers[_layerdepth].depth = float(_layerdepth)
                        #soil[_isoil].layers[_layerdepth].bottom = _zbottom
                        soil[_isoil].mudline = float(_mudline)
                        
                        soil[_isoil].layers[_layerdepth].curve['TZ'] = _TZ[_tzNo]
                        soil[_isoil].diameter = 1

                # find TZ soil data
                elif _keyWord[1] == 'QZ':
                    line, _soilNo = asas_common.word_pop(line)
                    _isoil = _soilNo
                    _springNo += 1
                    _qzNo = _springNo
                    _QZ[_qzNo] = f2u.Geometry.SpringElement('Qz', _qzNo)

                    try:
                        soil[_isoil].layers[_layerdepth].curve['QZ'] = _QZ[_qzNo]
                    except KeyError:
                        _nolayer = 1
                        _zbottom = float(_mudline) - float(_layerdepth)
                        soil[_isoil] = f2u.Foundation.Soil(int(_soilNo), _isoil)
                        soil[_isoil].layers[_layerdepth] = f2u.Foundation.Layer(_layerdepth, _nolayer)
                        soil[_isoil].layers[_layerdepth].depth = float(_layerdepth)
                        #soil[_isoil].layers[_layerdepth].bottom = _zbottom
                        soil[_isoil].mudline = float(_mudline)
                        soil[_isoil].layers[_layerdepth].curve['QZ'] = _QZ[_qzNo]
                        soil[_isoil].diameter = 1
                #
                elif _keyWord[1] == 'mudmodY':
                    # line, _mudmodY = word_pop(line)
                    line, _data = pilesoil.section_data(line)
                    #print('mud mod Y', line, _data)
                    # line = ''
                #
                elif _keyWord[1] == 'mudmodZ':
                    # line, _mudmodY = word_pop(line)
                    line, _data = pilesoil.section_data(line)
                    #print('mud mod Z', line, _data)
                #
                elif _keyWord[1] == 'mudslide':
                    line, _isoil = asas_common.word_pop(line)
                    _data = line.split()
                    _data = [float(_item) for _item in _data]
                    
                    try:
                        soil[_isoil].displacement.append(_data)
                    except KeyError:
                        soil[_isoil] = f2u.Foundation.Soil(int(_soilNo), _isoil)
                        soil[_isoil].displacement.append(_data)
                #
                else:
                    print('?? soilWord2 ', line)
                    line = ''
                    #
            #
            else:
                line, _keyWord = pilesoil.soil_word(line, _keyWord, 1)
                line, _keyWord = pilesoil.soil_word3(line, _keyWord, 2)
                #
                if _keyWord[1] == 'end':
                    _keyWord = ['soil', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
                    _lengthFactor, _forceFactor = process.units.convert_units_SI(global_length, 
                                                                                 global_force)
                    _nolayer = 0
                #
                elif _keyWord[1] == 'mudline':
                    line, _mudline = asas_common.word_pop(line)
                    #print('mudline', line)
                #
                elif _keyWord[1] == 'pfactor':
                    line, _pfactor = asas_common.word_pop(line)
                    _pfactor = float(_pfactor)
                    #print('p factor', line)
                #
                elif _keyWord[1] == 'yfactor':
                    line, _yfactor = asas_common.word_pop(line)
                    _yfactor = float(_yfactor)
                    #print('y factor', line)
                #
                elif _keyWord[1] == 'tfactor':
                    line, _tfactor = asas_common.word_pop(line)
                    _tfactor = float(_tfactor)
                    #print(line)
                #
                elif _keyWord[1] == 'zfactor':
                    line, _zfactor = asas_common.word_pop(line)
                    _zfactor = float(_zfactor)
                    #print('z factor', line)
                #
                elif _keyWord[1] == 'mudslide':
                    line, _dummy = asas_common.word_pop(line)
                    _data = line.split()
                    _data = [float(_item) for _item in _data]
                    soil[_isoil].displacement.append(_data)
                #
                elif _keyWord[1] == 'PY' and _keyWord[2] == 'N/A':
                    line, _layerdepth = asas_common.word_pop(line)
                    _layerdepth = float(_layerdepth)

                    _springNo += 1
                    _pyNo = _springNo
                    _PY[_pyNo] = f2u.Geometry.SpringElement('Py', _pyNo)

                    try:
                        soil[_isoil].layers[_layerdepth].curve['PY'] = _PY[_pyNo]
                    except KeyError:
                        _nolayer += 1
                        _zbottom = soil[_isoil].mudline - float(_layerdepth)

                        soil[_isoil].layers[_layerdepth] = f2u.Foundation.Layer(_layerdepth, _nolayer)
                        soil[_isoil].layers[_layerdepth].depth = float(_layerdepth)
                        #soil[_isoil].layers[_layerdepth].bottom = _zbottom
                        soil[_isoil].layers[_layerdepth].curve['PY'] = _PY[_pyNo]
                #
                elif _keyWord[1] == 'TZ' and _keyWord[2] == 'N/A':
                    line, _layerdepth = asas_common.word_pop(line)
                    _layerdepth = float(_layerdepth)

                    _springNo += 1
                    _tzNo = _springNo
                    _TZ[_tzNo] = f2u.Geometry.SpringElement('Tz', _tzNo)

                    try:
                        soil[_isoil].layers[_layerdepth].curve['TZ'] = _TZ[_tzNo]
                    except KeyError:
                        _nolayer = 1
                        _zbottom = float(_mudline) - float(_layerdepth)

                        soil[_isoil].layers[_layerdepth] = f2u.Foundation.Layer(_layerdepth, _nolayer)
                        soil[_isoil].layers[_layerdepth].depth = float(_layerdepth)
                        #soil[_isoil].layers[_layerdepth].bottom = _zbottom

                        soil[_isoil].layers[_layerdepth].curve['TZ'] = _TZ[_tzNo]
                #
                else:
                    while line: 
                        if _keyWord[2] == 'force':
                            line, _item = asas_common.word_pop(line)
                            _item = float(_item)

                            if _keyWord[1] == "PY":
                                _PY[_pyNo].force.append(_item * _pfactor * _force_factor)
                            #
                            elif _keyWord[1] == "TZ":
                                _TZ[_tzNo].force.append(_item * _tfactor * _force_factor)
                            #
                            elif _keyWord[1] == "QZ":
                                _QZ[_qzNo].force.append(_item * _tfactor * _force_factor)
                            #
                            else:
                                print(' *** error -- > force spring not recognized : {:}'
                                      .format(line))
                                # soil[_isoil].layers[_layerdepth].curve[_keyWord[1]].force.append(_item)
                        #
                        elif _keyWord[2] == 'displacement':
                            line, _item = asas_common.word_pop(line)
                            _item = float(_item)

                            if _keyWord[1] == "PY":
                                _PY[_pyNo].displacement.append(_item * _yfactor * _length_factor)
                            #
                            elif _keyWord[1] == "TZ":
                                _TZ[_tzNo].displacement.append(_item * _zfactor * _length_factor)
                            #
                            elif _keyWord[1] == "QZ":
                                _QZ[_qzNo].displacement.append(_item * _zfactor * _length_factor)
                            #
                            else:
                                print(' *** error -- > force spring not recognized : {:}'
                                      .format(line))

                                # soil[_isoil].layers[_layerdepth].curve[_keyWord[1]].displacement.append(_item)
                        #
                        else:
                            print('?? ', line)
                            line = ''
                            #
    #
    #
    #
    #
    # check if pile data for diameter prurposes
    _pile = geometry.read_asas_geometry(SPLTinp, 
                                        global_units)
    #
    _list = []
    for _sect in _pile.sections.values():
        _list.append(_sect.properties.D)
    #
    if _list:
        _diam = max(_list)
    else:
        print('   *** error pile diamter not found')
        sys.exit()
    #
    # import operator
    _qz = {}
    for key, _Qz in sorted(_QZ.items()):
        _qz = _Qz
        break
    _py = {}
    _tz = {}
    
    for _soil in soil.values():
        _layerdepth = 0
        _soil.mudline = abs(_soil.mudline)
        _layer_flag = []
        _soil.diameter = _diam
        _layer_number = 0
        for _no, _layers in sorted(_soil.layers.items()):
            try:
                _layers.curve['PY']
            except KeyError:
                _layers.curve['PY'] = _py
            _py = _layers.curve['PY']
            #
            try:
                _layers.curve['TZ']
            except KeyError:
                _layers.curve['TZ'] = _tz
            _tz = _layers.curve['TZ']
            #
            try:
                _layers.curve['QZ']
            except KeyError:
                _layers.curve['QZ'] = _qz
            _qz = _layers.curve['QZ']
            
            if _no - _layerdepth < 0.1 * _length_factor:
                _layer_flag.append(_no)
                continue
            #
            _layerdepth = _no            
            _layer_number += 1
            _layers.number = _layer_number
            _layers.diameter = _diam
        #
        for _layer_no in _layer_flag:
            del _soil.layers[_layer_no]
    #
    for _spring in _PY.values():
        process.foundation.fill_spring(_spring)
    
    for _spring in _TZ.values():
        process.foundation.fill_spring(_spring)
    
    #for _spring in _QZ.values():
    #    process.foundation.fill_spring(_spring)
    #
    spring = [_PY, _TZ, _QZ]
    #
    print('--- End reading {:} file'.format(SPLTinp))
    #
    _material_spring = get_spring_material(spring)
    #  
    return soil, _material_spring, _user_data
#
#
