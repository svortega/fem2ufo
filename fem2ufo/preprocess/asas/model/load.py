# 
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import re
import sys

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.asas.operations.common as asas_common
import fem2ufo.preprocess.asas.operations.load as load_operation

# ----------------------
# load modules
#
def master_load(lineIn, keyWord, ind=0):
    """
    """ 
    _key = {"load": r"load"}
    #
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
#
def load_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"geometry": r"\@",
            "group": r"grou[p]?",
            "loadcase": r"case",
            "loadnode": r"nodal\s*lo(a)?(d)?",
            "distributed": r"distribu(te)?[d]?",
            "gravity": r"body\s*for(ce)?",
            "temperature": r"temperat(ure)?",
            "pressure": r"pressure",
            "prescribed": r"prescrib(ed)?",
            "centrifugal": r"centrifu(gal)?",
            "angular": r"ang\s*acce(leration)?",
            "component": r"comp\s*loa[d]?",
            "mass": r"dire",
            "addedmass": r"lump",
            "consistent": r"cons(istent)?",
            "end": r"end"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
def gravity_word(lineIn, keyWord, ind=2):
    """
    """   
    _key = {"group": r"GROUP",
            "element": r"ELEM",
            "repeat": r"RP|RRP"}
    #
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'

    return lineOut, keyWord
#
#
def udl_word(lineIn, keyWord, ind=3):
    """
    """
    _key = {"udl_global_full_a": r"gl1",
            "udl_global_full_b": r"gp1",
            "udl_local_full": r"bl(1|2)",
            "udl_local_full_step": r"bl3",

            "udl_local_partial_step1": r"bl4",
            "udl_global_partial_step1_a": r"gl4",
            "udl_global_partial_step1_b": r"gp4",

            "udl_global_partial_step2_a": r"gl6",
            "udl_global_partial_step2_b": r"gp6",

            "udl_global_partial_step3_a": r"gl7",
            "udl_global_partial_step3_b": r"gp7",
            "udl_local_partial": r"bl(6|7)|cb1",

            "node_global": r"gl5",
            "node_local": r"bl5",
            "node_local_ends": r"bl8",

            "panel_1": r"ml1",
            "panel_2": r"ml2",
            "panel_3": r"ml3",
            "panel_4": r"tb1"}
    #

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
#
#
def find_member(element, node1, node2):
    """
    """
    _elem = [_item for _item in element.values()
             if _item.node[0].name == node1
             and _item.node[1].name == node2
             or _item.node[0].name == node2
             and _item.node[1].name == node1]

    try:
        return _elem[0]
    except:
        print("    **** error loading, member {:} {:} no found".format(node1, node2))
        return False
#
#
def udl_fill(_udl, _type, _force, _DOF, _L,
             _option=1, _complex=0):
    """
    """
    #
    _udl.type = _type
    _udl.complex = _complex
    _udl.option = _option
    _udl.distance = _L
    #
    _loadlist = [0, 0, 0, 0, 0, 0]
    _loadlist[_DOF] = float(_force[0])
    _udl.distributed = _loadlist
    #
    _loadlist = [0, 0, 0, 0, 0, 0]
    _loadlist[_DOF] = float(_force[1])
    _udl.distributed.extend(_loadlist)
    #
    return _udl
#
#
def udl_fill_load(_load_case_no, _type, _force, _DOF,
                  _L, line, _element, _load):
    """
    """
    udl = {}
    if re.match(r"elem", line, re.I):
        line, _elem = asas_common.word_pop(line)
        _elemList = [int(x) for x in line.split()]

        for _elementNo in _elemList:
            _memb_name = str(_elementNo)
            #if _memb_name == '7448':
            #    print('-->')
            try:
                _mem = _element[_memb_name]
                udl = f2u.Load.BeamUDL(_memb_name, _load_case_no)
                udl.beam_length = _mem.flexible_length
                udl.type = _type
                # here change L1 length from node 0 to node 1
                if _L[1] != 0:
                    _L[1] = abs(round(udl.beam_length - _L[1], 3))
                #
                _loadlist = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
                _loadlist[0][_DOF] = float(_force[0])
                _loadlist[1][_DOF] = float(_force[1])
                udl.input_udl(_loadlist, _L)
                #
                try:
                    _mem.load[_load_case_no]
                except KeyError:
                    _mem.load[_load_case_no] = f2u.Load.Element(_memb_name, _elementNo)
                    #_mem.load[_load_case_no].type = _type
                #
                if _type == 2:
                    _mem.load[_load_case_no].point.append(udl)
                else:
                    _mem.load[_load_case_no].distributed.append(udl)             
                #
                _load.functional[_load_case_no].concept.append(_memb_name)
            #
            except:
                print('***** udl fill load')
                print("error member {:} no found".format(_memb_name))
    #
    else:
        _endNode = line.split()
        # TODO : check if using int is safe
        _mem = find_member(_element, _endNode[0], _endNode[1])
        if not _mem:
            return
        _memb_name = _mem.name
        #
        udl = f2u.Load.BeamUDL(_memb_name, _load_case_no)
        udl.beam_length = _mem.flexible_length
        udl.type = _type
        #
        if _L[1] != 0:
            _L[1] = abs(round(udl.beam_length - _L[1], 3))
        #
        _loadlist = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
        _loadlist[0][_DOF] = float(_force[0])
        _loadlist[1][_DOF] = float(_force[1])
        udl.input_udl(_loadlist, _L)
        #
        try:
            _mem.load[_load_case_no]
        except KeyError:
            _mem.load[_load_case_no] = f2u.Load.Element(_memb_name, _memb_name)
            #_mem.load[_load_case_no].type = _type
        #
        if _type == 2:
            _mem.load[_load_case_no].point.append(udl)
        else:
            _mem.load[_load_case_no].distributed.append(udl)
        #
        _load.functional[_load_case_no].concept.append(_memb_name)
    #
    #
#
#
def read_asas_load(file_name, model, global_units,
                   load_flag=False):
    """
    fem2ufo module reads ASAS load files
    """
    #
    #global _load
    # check input data
    if not model.load:
        model.set_load('loading')
    
    # read ASAS input file
    asas_file = process.common.check_input_file(file_name)
    if not asas_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())        
    #
    #
    print(' ')
    print('--- Reading loading from {:} input file'.format(file_name))
    print(' ')
    #
    # Units [length, mass, time, temperature, force, pressure/stress]
    global_length = global_units[0]
    global_force = global_units[4]   
    #
    if load_flag:
        _keyWord = ['load', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    else:
        _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    
    _length_factor, _force_factor = process.units.convert_units_SI(global_length, global_force)
    #      
    #
    print('    * First pass')
    #
    dat_temp = open('CheckMe_'+ asas_file,'w+')
    #
    with open(asas_file) as fin:
        for line2 in fin:
            # Jump commented lines
            if re.match(r"\*", line2):
                continue
            # find key words and clean lines from comments
            if re.search(r"\*", line2):
                line2 = asas_common.remove_comment(line2)
            #
            line, _keyWord = master_load(line2, _keyWord)
            if not line:
                continue
            #
            if _keyWord[0] == 'load':
                line, _keyWord = load_word(line, _keyWord)
                
                if re.match(r"\/", line):
                    continue
                
                if not line:
                    continue
                #
                elif 'UNITS' in line.upper():
                    line = line.replace('UNITS', '')
                    line, _length_factor, _force_factor = asas_common.unit_factor(line, 
                                                                             global_length,
                                                                             global_force)
                #
                elif _keyWord[1] == 'loadcase':
                    line, _loadCaseNo = asas_common.word_pop(line)
                    #
                    #if '2825' in _loadCaseNo:
                    #    print('-->')
                    #
                    _title = load_operation.load_title(line)
                    if _title:
                        _title = '_' + str(_title)
                    _loadTitle = 'BLC' + str(_loadCaseNo).zfill(3) + _title
                    #
                    try:
                        model.load.functional[_loadCaseNo].name = _loadTitle
                    except KeyError:
                        model.load.functional[_loadCaseNo] = f2u.Load.Basic(_loadCaseNo, _loadCaseNo)
                        model.load.functional[_loadCaseNo].name = _loadTitle
                #
                elif _keyWord[1] == 'end':
                    _keyWord = ['load', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
                    _length_factor, _force_factor = process.units.convert_units_SI(global_length, global_force)
                #
                elif _keyWord[1] == 'geometry':
                    # line, item = wordPop(line)
                    print('--- ', line)
                    # line = ''
                    # break
                #
                elif _keyWord[1] == 'gravity':
                    line, item = gravity_word(line, _keyWord, 2)
                    if _keyWord[2] == "N/A":
                        _grav = [float(x) for x in line.split()]
                        # print('gravity', line, _grav)
                        model.load.functional[_loadCaseNo].gravity = _grav
                #
                elif _keyWord[1] == 'group':
                    #
                    #print('   *** warning group from loading need to be fixed', line)
                    dat_temp.write("\n")
                    dat_temp.write("   *** warning group from loading need to be fixed in line {:}"
                                   .format(line2))                    
                #
                elif _keyWord[1] == 'distributed':
                    line, _keyWord = asas_common.freedom_word(line, _keyWord, 2)
                    line, _keyWord = udl_word(line, _keyWord)
                    #
                    if _keyWord[3] == 'udl_global_full_a':
                        #
                        _type = 1  # True distributed force, conservative load
                        _DOF = int(_keyWord[2])
                        #
                        line, _udl_1 = asas_common.word_pop(line)
                        _udl_1 = float(_udl_1) * _force_factor / _length_factor
    
                        line, _udl_2 = asas_common.word_pop(line)
                        _udl_2 = float(_udl_2) * _force_factor / _length_factor
    
                        _force = [_udl_1, _udl_2]
                        _L = [0, 0]
                    #
                    elif _keyWord[3] == 'udl_global_partial_step1_a':
                        #
                        # print('== >',line)
                        #
                        _type = 1  # True distributed force, conservative load
                        _DOF = int(_keyWord[2])
                        #
                        line, _udl_1 = asas_common.word_pop(line)
                        _udl_1 = float(_udl_1) * _force_factor / _length_factor
    
                        _force = [_udl_1, _udl_1]
    
                        line, _distance_1 = asas_common.word_pop(line)
                        _distance_1 = float(_distance_1) * _length_factor
    
                        line, _distance_2 = asas_common.word_pop(line)
                        _distance_2 = float(_distance_2) * _length_factor
    
                        _L = [_distance_1, _distance_2]
                        #
                        # print('+++++++++++++')
                    #
                    elif _keyWord[3] == 'udl_global_partial_step1_b':
                        print('Error --> udl_global_partial_step1_b no implemented')
                        #
                    elif _keyWord[3] == 'udl_global_partial_step2_a':
                        #
                        # print('== >',line)
                        #
                        _type = 1  # True distributed force, conservative load
                        _DOF = int(_keyWord[2])
                        #
                        line, _udl_1 = asas_common.word_pop(line)
                        _udl_1 = float(_udl_1) * _force_factor / _length_factor
    
                        line, _udl_2 = asas_common.word_pop(line)
                        _udl_2 = float(_udl_2) * _force_factor / _length_factor
    
                        _force = [_udl_1, _udl_2]
    
                        line, _distance_1 = asas_common.word_pop(line)
                        _distance_1 = float(_distance_1) * _length_factor
    
                        line, _distance_2 = asas_common.word_pop(line)
                        _distance_2 = float(_distance_2) * _length_factor
    
                        _L = [_distance_1, _distance_2]
                        # print('== >',_distance_1, _distance_2, _L)
                    #
                    elif _keyWord[3] == 'udl_global_partial_step2_b':
                        print('Error --> udl_global_partial_step2_b no implemented')
                    #
                    elif _keyWord[3] == 'udl_global_partial_step3_a':
                        print('Error --> udl_global_partial_step3_a no implemented')
                    #
                    elif _keyWord[3] == 'udl_global_partial_step3_b':
                        print('Error --> udl_global_partial_step3_b no implemented')
                        #
                    elif _keyWord[3] == 'udl_global_partial':
                        print('Error --> udl_global_partial no implemented')
                        #
                    elif _keyWord[3] == 'node_global':
                        #
                        _type =  2  # Simulated concentrated force, conservative load
                        _DOF = int(_keyWord[2])
                        #
                        line, _udl_1 = asas_common.word_pop(line)
                        _udl_1 = float(_udl_1) * _force_factor
    
                        line, _distance = asas_common.word_pop(line)
                        _distance = float(_distance) * _length_factor
    
                        _force = [_udl_1, _udl_1]
                        _L = [_distance, 0]
                    #
                    udl_fill_load(_loadCaseNo,
                                  _type, _force, _DOF, _L, line, 
                                  model.component.concepts,
                                  model.load)
                #
                elif _keyWord[1] == 'loadnode':
                    _nodeL = {}
                    line, _keyWord = asas_common.freedom_word(line, _keyWord, 2)
                    _DoFno = int(_keyWord[2])
                    line, _nodeLoad = asas_common.word_pop(line)
                    _nodeList = line.split()
    
                    for _nodenumber in _nodeList:
                        try:
                            _node = model.component.nodes[_nodenumber]
                        except KeyError:
                            #print('   ** warning node {:} not found'
                            #      .format(_nodenumber))
                            dat_temp.write("\n")
                            dat_temp.write("   *** warning node {:} not found in line {:}"
                                           .format(_nodenumber, line2))
                            continue
                        
                        _loadlist = [0, 0, 0, 0, 0, 0]
                        _loadlist[_DoFno] = float(_nodeLoad) * _force_factor
    
                        try:
                            #_nodeL[_nodenumber].point = _loadlist
                            _node.load[_loadCaseNo]
                        except KeyError:
                            #_nodeL[_nodenumber] = f2u.NodeLoad(_nodenumber)
                            #_nodeL[_nodenumber].point = _loadlist
                            _node.load[_loadCaseNo] = f2u.Load.Node(_nodenumber, _nodenumber)
                            #_node.load[_loadCaseNo].type = _type                     
                        #
                        _node.load[_loadCaseNo].point.append(_loadlist)
                        model.load.functional[_loadCaseNo].node.append(_nodenumber)
                #
                elif _keyWord[1] == 'prescribed':
                    print('prescribed to be fixed', line)
                    line = ''
                #
                else:
                    line, item = asas_common.word_pop(line)
                    print('fail ==> ', item, '-', line)
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    #
    print('--- End reading {:} file'.format(file_name))
    #
    #return _load
#
