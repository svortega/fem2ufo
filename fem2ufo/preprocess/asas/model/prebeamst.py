# 
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import sys
import re


# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
#
import fem2ufo.preprocess.asas.operations.codecheck as codecheck
import fem2ufo.preprocess.asas.operations.common as asas_common

#
#
def assign_Cm_member(geometry, memb_no, _Cm, _index):
    """
    """
    try:
        memb_name = str(memb_no)
        memb = geometry.concepts[memb_name]
    except KeyError:
        #print('   *** warning element {:} not found'
        #      .format(memb_no))
        return
    
    try:
        memb.properties.Cm[_index] = float(_Cm)
    except AttributeError:
        memb.properties = f2u.Geometry.BeamElementProperty('code_check')
        memb.properties.Cm[_index] = float(_Cm)
#
#
def assign_K_member(geometry, memb_no, _Ky, _Kz):
    """
    """
    #
    _Ky =  float(_Ky)
    if not _Kz:
        _Kz = _Ky
    else:
        _Kz =  float(_Kz)
    #
    try:
        memb_name = str(memb_no)
        memb = geometry.concepts[memb_name]
    except KeyError:
        print('   *** warning assign K --> element {:} not found'
              .format(memb_no))
        return
    
    try:
        memb.properties.K = [False, _Kz, _Ky]
        #memb.properties.K = [False, _Ky, _Kz]
    except AttributeError:
        memb.properties = f2u.Geometry.BeamElementProperty('code_check')
        memb.properties.K = [False, _Kz, _Ky]
        #memb.properties.K = [False, _Ky, _Kz]
#
#
def assign_Lyz_member(geometry, memb_no, _Ly, _Lz):
    """
    """
    #
    _Ly =  float(_Ly)
    if not _Lz:
        _Lz = _Ly
    else:
        _Lz =  float(_Lz)
    #
    try:
        memb_name = str(memb_no)
        memb = geometry.concepts[memb_name]

    except KeyError:
        print('   *** warning Lyz element {:} not found'
              .format(memb_no))
        return
    
    try:
        memb.properties.L = [None, _Lz, _Ly]
        #memb.properties.L = [None, _Ly, _Lz] 
    except AttributeError:
        memb.properties = f2u.Geometry.BeamElementProperty('code_check')
        memb.properties.L = [None, _Lz, _Ly]
        #memb.properties.L = [None, _Ly, _Lz]
#
#
def assign_Lb_member(geometry, memb_no, _Lby, _Lbz):
    """
    """
    #
    _Lby =  float(_Lby)
    if not _Lbz:
        _Lbz = _Lby
    else:
        _Lbz =  float(_Lbz)
    #
    try:
        memb_name = str(memb_no)
        memb = geometry.concepts[memb_name]

    except KeyError:
        print('   *** warning Lb element {:} not found'
              .format(memb_no))
        return
    
    try:
        memb.code_check.Lb = [None, _Lbz, _Lby] 
        #memb.code_check.Lb = [None, _Lby, _Lbz] 
    except AttributeError:
        memb.code_check = f2u.CodeCheck()
        memb.code_check.Lb = [None, _Lbz, _Lby]
        #memb.code_check.Lb = [None, _Lby, _Lbz] 
#
#
def assign_FY_member(geometry, memb_no, _Fy, _factor=1, _step=None):
    """
    """
    Fy_step = _Fy * _factor
    
    try:
        memb_name = str(memb_no)
        memb = geometry.concepts[memb_name]
    except KeyError:
        print('    *** warning Fy element {:} not found'
              .format(memb_no))
        return
    #
    #
    try:
        _step = int(_step)
        try:
            if memb.material[_step].Fy != Fy_step:
                update_material(geometry, memb, _Fy, _factor, _step)
        except IndexError:
            print('    *** warning element {:} has not step members'.format(memb_no))
    except TypeError:
        for _step in range(len(memb.material)):
            try:
                if memb.material[_step].Fy != Fy_step:
                    update_material(geometry, memb, _Fy, _factor, _step)
            except IndexError:
                print('    *** warning element {:} has not step members'.format(memb_no))
    #
#
#
def update_material(geometry, memb, _Fy, _factor, _step):
    """
    """
    # check if material already exist
    _matNo = []
    for _mat in geometry.materials.values():
        #if 'new' in _mat.name:
        #    print('--->')
        _matNo.append(_mat.number)
        if _mat.equal(memb.material[_step], grade=_Fy):
            # remove material from group if applicable
            if len(memb.material) == 1 or _step == 0:
                try:
                    memb.material[_step].concepts.remove(memb.name)
                except ValueError:
                    pass
            memb.material[_step] = _mat
            _mat.concepts.append(memb.name)
            return
    #
    # no existing material found
    # create a new one
    _matNo = max(_matNo)
    while True:
        try:
            _matNo += 1
            geometry.materials[_matNo]
        except KeyError:
            break
    #
    _matName = str(memb.material[_step].name) + "_new" # + str(_matNo)
    #
    geometry.materials[_matNo] = f2u.Material(_matName, _matNo)
    geometry.materials[_matNo].E = memb.material[_step].E
    geometry.materials[_matNo].poisson = memb.material[_step].poisson
    geometry.materials[_matNo].alpha = memb.material[_step].alpha
    geometry.materials[_matNo].density = memb.material[_step].density
    geometry.materials[_matNo].Fy = _Fy * _factor
    geometry.materials[_matNo].grade = _Fy
    geometry.materials[_matNo].type = 'Plastic'
    geometry.materials[_matNo].concepts.append(memb.name)
    #
    if len(memb.material) == 1 or _step == 0:
        try:
            memb.material[_step].concepts.remove(memb.name)
        except ValueError:
            pass    
    
    memb.material[_step] = geometry.materials[_matNo]
#
#
#
#
#
def read_prebeamst_file(fileName, geometry, global_units):
    """
    read code check parameters from prebeamst asas file
    """
    #
    # read ASAS input file
    asas_file = process.common.check_input_file(fileName)
    if not asas_file:
        print('   *** error file {:} not found'.format(fileName))
        print("      try again", sys.exit())
    #
    print('-------------- ASAS CODE CHECK Module --------------')
    print('')
    print('--- Reading code check data from {:} input file'.format(fileName))
    print('')

    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    # Units [length, mass, time, temperature, force, pressure/stress]
    global_length = global_units[0]
    global_force = global_units[4]
    _length_factor, _force_factor = process.units.convert_units_SI(global_length, 
                                                                   global_force)
    #
    print('    * First pass')
    dat_temp = open('CheckMe_'+ asas_file,'w+')
    #
    with open(asas_file) as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            # find key words and clean lines from comments
            if re.search(r"\*", line):
                line = asas_common.remove_comment(line)
            #
            line = line.strip()
            if not line:
                continue
            #
            line, _keyWord = codecheck.master_word(line, _keyWord)
            #
            if 'UNITS' in line:
                
                if re.search(r"disp|stre", line, re.IGNORECASE):
                    continue
    
                line, _length_factor, _force_factor= asas_common.unit_factor(line, 
                                                                        global_length,
                                                                        global_force)
            #
            elif  'cm' in _keyWord[0]:
                #line, _Cm_item = asas_common.word_pop(line)
                line, _Cm = asas_common.word_pop(line)
                line, _keyWord = asas_common.FE_word(line, _keyWord, 1)
                
                _index = 1
                if 'z' in _keyWord[0]:
                    _index = 2
                
                while line:
                    line, _keyWord = codecheck.to_word(line, _keyWord, 2)
                    
                    try:
                        line, item = asas_common.word_pop(line)
                    except IndexError:
                        item = []                    
                    
                    if _keyWord[1] == 'element':
                        if _keyWord[2] == 'to':
                            _total = int(item) - memb_no
                            for x in range(_total):
                                memb_no += 1
                                assign_Cm_member(geometry, memb_no, _Cm, _index)
                        #
                        elif _keyWord[2] == 'all':
                            for memb_no, _memb in geometry.concepts.items():
                                assign_Cm_member(geometry, memb_no, _Cm, _index)
                        #
                        else:
                            memb_no = int(item)
                            assign_Cm_member(geometry, memb_no, _Cm, _index)
                    #
                    elif _keyWord[1] == 'group':
                        if _keyWord[2] == 'to':
                            _total = int(item) - group_no
                            for x in range(_total):
                                group_no += 1
                                group = geometry.sets[str(group_no)]
                                for memb_no in group.concepts:
                                    assign_Cm_member(geometry, memb_no, _Cm, _index)                            
                        #
                        elif _keyWord[2] == 'all':
                            for _group in geometry.sets.values():
                                for memb_no in _group.items:
                                    assign_Cm_member(geometry, memb_no, _Cm, _index)
                        #
                        else:
                            group_no = int(item)
                            group = geometry.sets[str(group_no)]
                            for memb_no in group.concepts:
                                assign_Cm_member(geometry, memb_no, _Cm, _index)
                    #
                    else:
                        print('   *** error line not read in Cm : {:}'
                              .format(line))
            # EFFE
            elif   _keyWord[0] == 'K':
                #
                line, _Ky = asas_common.word_pop(line)
                _Kz = False
                line, _keyWord = asas_common.FE_word(line, _keyWord, 1)
                
                if _keyWord[1] == 'N/A':
                    line, _Kz = asas_common.word_pop(line)
                    line, _keyWord = asas_common.FE_word(line, _keyWord, 1)
                #
                while line:
                    line, _keyWord = codecheck.to_word(line, _keyWord, 2)
                    
                    try:
                        line, item = asas_common.word_pop(line)
                    except IndexError:
                        item = []
                    
                    if _keyWord[1] == 'element':
                        if _keyWord[2] == 'to':
                            _total = int(item) - memb_no
                            for x in range(_total):
                                memb_no += 1
                                assign_K_member(geometry, memb_no, _Ky, _Kz)
                        #
                        elif _keyWord[2] == 'all':
                            for memb_no, _memb in geometry.concepts.items():
                                assign_K_member(geometry, memb_no, _Ky, _Kz)
                            #print('K?')
                        #
                        else:
                            memb_no = int(item)
                            assign_K_member(geometry, memb_no, _Ky, _Kz)                    
                    #
                    elif _keyWord[1] == 'group':
                        if _keyWord[2] == 'to':
                            _total = int(item) - group_no
                            for x in range(_total):
                                group_no += 1
                                group = geometry.sets[str(group_no)]
                                
                                for memb_no in group.concepts:
                                    assign_K_member(geometry, memb_no, _Ky, _Kz)                            
                        #
                        elif _keyWord[2] == 'all':
                            for _group in geometry.sets.values():
                                for memb_no in _group.items:
                                    assign_K_member(geometry, memb_no, _Ky, _Kz)
                        #
                        else:
                            group_no = int(item)
                            group = geometry.sets[str(group_no)]
                            
                            for memb_no in group.concepts:
                                assign_K_member(geometry, memb_no, _Ky, _Kz)                
                    #
                    else:
                        print('   *** error K factor not identified : {:}'.
                              format(line))
                        line=''
            # UNBR
            elif _keyWord[0] == 'Lyz':
                #
                line, _keyWord = codecheck.Lyz_word(line, _keyWord, 1)
                _Lz = False
                line, _Ly = asas_common.word_pop(line)
                line, _keyWord = asas_common.FE_word(line, _keyWord, 2)
                
                if _keyWord[2] == 'N/A':
                    line, _Lz = asas_common.word_pop(line)
                    line, _keyWord = asas_common.FE_word(line, _keyWord, 2)
                #
                while line:
                    line, _keyWord = codecheck.to_word(line, _keyWord, 3)
                    try:
                        line, item = asas_common.word_pop(line)
                    except IndexError:
                        item = []
                    
                    if _keyWord[2] == 'element':
                        if _keyWord[3] == 'to':
                            _total = int(item) - memb_no
                            for x in range(_total):
                                memb_no += 1
                                assign_Lyz_member(geometry, memb_no, _Ly, _Lz)                     
                        #
                        elif _keyWord[3] == 'all':
                            for memb_no, _memb in geometry.concepts.items():
                                assign_Lyz_member(geometry, memb_no, _Ly, _Lz)                            
                        #
                        else:
                            memb_no = int(item)
                            assign_Lyz_member(geometry, memb_no, _Ly, _Lz)                    
                    #
                    elif _keyWord[2] == 'group':
                        if _keyWord[3] == 'to':
                            _total = int(item) - group_no
                            for x in range(_total):
                                group_no += 1
                                group = geometry.sets[str(group_no)]
                                
                                for memb_no in group.concepts:
                                    assign_Lyz_member(geometry, memb_no, _Ly, _Lz)                            
                                #print('here')
                        #
                        elif _keyWord[2] == 'all':
                            for _group in geometry.sets.values():
                                for memb_no in _group.items:
                                    assign_Lyz_member(geometry, memb_no, _Ly, _Lz) 
                        #
                        else:
                            group_no = int(item)
                            group = geometry.sets[str(group_no)]
                            for memb_no in group.concepts:
                                assign_Lyz_member(geometry, memb_no, _Ly, _Lz)                
                    #
                    else:
                        print('   *** error Lyz factor not identified : {:}'.
                              format(line))
                        line=''
            #
            elif _keyWord[0] == 'Lb':
                #if '12445' in line:
                #    print('??')
                #
                line, _keyWord = codecheck.Lyz_word(line, _keyWord, 1)
                _Lz = False
                line, _Ly = asas_common.word_pop(line)
                
                line, _keyWord = asas_common.FE_word(line, _keyWord, 2)
                
                if _keyWord[2] == 'N/A':
                    line, _Lz = asas_common.word_pop(line)
                    line, _keyWord = asas_common.FE_word(line, _keyWord, 2)
                #
                while line:
                    line, _keyWord = codecheck.to_word(line, _keyWord, 3)
                    try:
                        line, item = asas_common.word_pop(line)
                    except IndexError:
                        item = []                    
                    
                    if _keyWord[2] == 'element':
                        if _keyWord[3] == 'to':
                            _total = int(item) - memb_no
                            for x in range(_total):
                                memb_no += 1
                                assign_Lb_member(geometry, memb_no, _Ly, _Lz)                     
                        #
                        elif _keyWord[3] == 'all':
                            for memb_no, _memb in geometry.concepts.items():
                                assign_Lb_member(geometry, memb_no, _Ly, _Lz)                            
                        #
                        else:
                            memb_no = int(item)
                            assign_Lb_member(geometry, memb_no, _Ly, _Lz)                    
                    #
                    elif _keyWord[2] == 'group':
                        if _keyWord[3] == 'to':
                            _total = int(item) - group_no
        
                            for x in range(_total):
                                group_no += 1
                                group = geometry.sets[str(group_no)]
                                
                                for memb_no in group.concepts:
                                    assign_Lb_member(geometry, memb_no, _Ly, _Lz)                            
                                #print('here')
                        #
                        elif _keyWord[2] == 'all':
                            for _group in geometry.sets.values():
                                for memb_no in _group.items:
                                    assign_Lb_member(geometry, memb_no, _Ly, _Lz) 
                        #
                        else:
                            group_no = int(item)
                            group = geometry.sets[str(group_no)]
                            
                            for memb_no in group.concepts:
                                assign_Lb_member(geometry, memb_no, _Ly, _Lz)                
                    #
                    else:
                        print('   *** error Lb factor not identified : {:}'.
                              format(line))
                        line=''
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()      
    #
    print('--- End reading {:} file'.format(fileName))
    #
    #return geometry
#
#
def read_yield(fileName, geometry, global_units):
    """
    read Fy data from prebeamst asas file
    Units [length, mass, time, temperature, force, pressure/stress]
    """
    #
    # read ASAS input file
    asas_file = process.common.check_input_file(fileName)
    if not asas_file:
        print('   *** error file {:} not found'.format(fileName))
        print("      try again", sys.exit())
    #    print('')
    #
    print('-------------- ASAS CODE CHECK Module --------------')
    print('')
    print('--- Reading Fy data from {:} input file'.format(fileName))
    print('')

    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    
    # 
    global_length = global_units[0]
    global_force = global_units[4]

    _length_factor, _force_factor = process.units.convert_units_SI(global_length, 
                                                                   global_force)

    dat_temp = open('CheckMeFy_'+ asas_file,'w+')
    print('    * First pass')
    
    with open(asas_file) as fin:
        for line in fin:    
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            # find key words and clean lines from comments
            if re.search(r"\*", line):
                line = asas_common.remove_comment(line)
            #
            line = line.strip()
            if not line:
                continue
            #
            line, _keyWord = codecheck.master_word(line, _keyWord)
            #
            if 'UNITS' in line:
                if re.search(r"disp|stre", line, re.IGNORECASE):
                    continue
                line, _length_factor, _force_factor= asas_common.unit_factor(line, 
                                                                             global_length,
                                                                             global_force)
            #
            elif _keyWord[0] == 'Fy':
                line, item = asas_common.word_pop(line)
                # need to identify grade
                _Fy = float(item)
                _factor = (_force_factor / _length_factor**2)
    
                while line:
                    line, _keyWord = codecheck.fy_word(line, _keyWord)
                    
                    if _keyWord[1] == 'all':
                        print(' fix all material')
                        #dat_temp.write("\n")
                        #dat_temp.write(line)
                        #line = ''
                        for _material in geometry.materials.values():
                            _material.Fy = _Fy *  _factor
                            _material.type = 'Plastic'
    
                    elif _keyWord[1] == 'material':
                        line, item = asas_common.word_pop(line)
                        _mat_name = item
                        try:
                            geometry.materials[_mat_name].Fy = _Fy * _factor
                            geometry.materials[_mat_name].grade = _Fy
                            geometry.materials[_mat_name].type = 'Plastic'
                        except KeyError:
                            dat_temp.write("\n")
                            dat_temp.write('   *** warning material {:} not found'
                                           .format(_mat_name))
                            dat_temp.write("\n")
                            dat_temp.write(line)
                            line = ''
    
                    elif _keyWord[1] == 'step':
                        line, item = asas_common.word_pop(line)
                        _step = int(item) - 1
                        line, _keyWord = asas_common.FE_word(line, _keyWord, ind=2)
                        
                        while line:
                            line, _keyWord = codecheck.to_word(line, _keyWord, 3)
                            
                            try:
                                line, item = asas_common.word_pop(line)
                            except IndexError:
                                if _keyWord[3] != 'all':
                                    continue
        
                            if _keyWord[2] == 'element':
                                if _keyWord[3] == 'to':
                                    _total = int(item) - memb_no
                                    
                                    for x in range(_total):
                                        memb_no += 1
                                        assign_FY_member(geometry, memb_no, _Fy, _factor, _step)
                                
                                elif _keyWord[3] == 'all':
                                    for memb_no, _memb in geometry.concepts.items():
                                        assign_FY_member(geometry, memb_no, _Fy, _factor, _step)
                                
                                else:
                                    memb_no = int(item)
                                    #if memb_no == 118:
                                    #    print('here')
                                    assign_FY_member(geometry, memb_no, _Fy, _factor, _step)
        
                            elif _keyWord[2] == 'group':
                                print('fix Fy group step')
                                dat_temp.write("\n")
                                dat_temp.write(line)
                                line = ''
        
                            else:
                                print('  *** error line not recognised', line)
                                dat_temp.write("\n")
                                dat_temp.write(line)
                                line = ''
    
                    else:
                        line, _keyWord = asas_common.FE_word(line, _keyWord, ind=2)
                        while line:
                            line, _keyWord = codecheck.to_word(line, _keyWord, 3)
                            #
                            try:
                                line, item = asas_common.word_pop(line)
                            except IndexError:
                                if _keyWord[3] != 'all':
                                    continue
                        
                            if _keyWord[2] == 'element':
                                if _keyWord[3] == 'to':
                                    _total = int(item) - memb_no
                                    for x in range(_total):
                                        memb_no += 1
                                        assign_FY_member(geometry, memb_no, _Fy, _factor)
                                #
                                elif _keyWord[3] == 'all':
                                    for _material in geometry.materials.values():
                                        _material.Fy = _Fy *  _factor
                                        _material.type = 'Plastic'
                                    for memb_no, _memb in geometry.concepts.items():
                                        assign_FY_member(geometry, memb_no, _Fy, _factor)
                                #
                                else:
                                    #if "118" in item:
                                    #    print('-->')
                                    memb_no = int(item)
                                    assign_FY_member(geometry, memb_no, _Fy, _factor)
                            #
                            elif _keyWord[2] == 'group':
                                if _keyWord[3] == 'to':
                                    _total = int(item) - group_no
                                    for x in range(_total):
                                        group_no += 1
                                        group = geometry.sets[str(group_no)]
                                        for memb_no in group.concepts:
                                            assign_FY_member(geometry, memb_no, _Fy, _factor)
                                elif _keyWord[3] == 'all':
                                    for group in geometry.sets:
                                        for memb_no in group.concepts:
                                            assign_FY_member(geometry, memb_no, _Fy, _factor)
                                else:
                                    group_no = int(item)
                                    group = geometry.sets[str(group_no)]
                                    for memb_no in group.concepts:
                                        assign_FY_member(geometry, memb_no, _Fy, _factor)
                            #
                            else:
                                print('  *** error line not recognised', line)
                                dat_temp.write("\n")
                                dat_temp.write(line)
                                line = ''
    # 
    dat_temp.close()      
    #
    print('--- End reading {:} file'.format(fileName))
    #
    #return geometry
    
#
def read_joints(fileName, geometry, prefix):
    """
    read join data from prebeamst asas file
    """
    # read ASAS input file
    asas_file = process.common.check_input_file(fileName)
    if not asas_file:
        print('   *** error file {:} not found'.format(fileName))
        print("      try again", sys.exit())
    #
    print('-------------- ASAS CODE CHECK Module --------------')
    print('')    
    print('')
    print('--- Reading joint data from {:} input file'.format(fileName))
    print('')
    print('    * First pass')
    #
    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    _joint_number = 0
    #
    dat_temp = open('CheckMejnt_'+ asas_file,'w+')
    #
    with open(asas_file) as fin:
        
        for line in fin:    
            #
            # Jump commented lines
            if re.match(r"\*", line):
                continue
                #
            # find key words and clean lines from comments
            if re.search(r"\*", line):
                line = asas_common.remove_comment(line)
            #
            line = line.strip()
            #
            if not line:
                continue
            #
            line, _keyWord = codecheck.master_word(line, _keyWord)
            #
            #
            if _keyWord[0] == 'joint':
                while line:
                    line, item = asas_common.word_pop(line)
                    line, _keyWord = codecheck.joint_word(line, _keyWord)
    
                    if _keyWord[1] == 'to':
                        line, item = asas_common.word_pop(line)
                        _total = int(item) - _node_number 
    
                        for x in range(_total):
                            _node_number += 1
                            _joint_number += 1
                            try:
                                geometry.nodes[_node_number]
                            except KeyError:
                                print('   *** warning node {:} does not exist'.format(_node_number))
                                continue                            
                            
                            _jnt_name = str(_joint_number)
                            try:
                                geometry.joints[_jnt_name]
                            except KeyError:
                                geometry.add_joint(_jnt_name, _node_number)
                            
                            geometry.joints[_jnt_name].name = prefix
                    #
                    elif _keyWord[1] == 'all':
                        print('fix joint all')
                        dat_temp.write("\n")
                        dat_temp.write(line)                        
                        line = ''
                    #
                    elif _keyWord[1] == 'element':
                        print('fix joint elment')
                        dat_temp.write("\n")
                        dat_temp.write(line)                         
                        line = ''
                    #
                    else:
                        _node_number = item
                        _joint_number += 1
                        try:
                            geometry.nodes[_node_number]
                        except KeyError:
                            print('   *** warning node {:} does not exist'.format(_node_number))
                            continue
                        
                        _jnt_name = str(_joint_number)
                        try:
                            geometry.joints[_jnt_name]
                        except KeyError:
                            geometry.add_joint(_jnt_name, _node_number)
                        
                        geometry.joints[_jnt_name].name = prefix
            # 
            #else:
            #    dat_temp.write("\n")
            #    dat_temp.write(line)
            #
        #
    #
    dat_temp.close()      
    #
    print('--- End reading joints from {:} file'.format(fileName))
    #
    #return geometry
#
