# 
# Copyright (c) 2009-2021 fem2ufo
# 

# Python stdlib imports
import re
import copy
import os
from itertools import islice
import cmath
import sys

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process

import fem2ufo.preprocess.asas.operations.process as asas_process
import fem2ufo.preprocess.asas.operations.common as asas_common

#
#
#
#
#
def get_line(fin):
    """
    """
    line2 = list(islice(fin, 1))
    line2 = ' '.join(map(str, line2))
    #
    # find key words and clean lines from comments
    if re.search(r"\*", line2):
        line2 = asas_common.remove_comment(line2)
    #
    line2 = line2.strip()
    line2 = line2.upper()
    return line2
#
#
def tub_section_check(line, _name, _Word, _section, _property,
                      _sec_dummy, _length_factor):
    """
    """
    _datax = line.split()
    #
    _sec_dummy[_name] = _datax[:2]
    #
    try:
        _property[_datax[0]]
        _section[_name].shape = _Word
        _section[_name].properties = _property[_datax[0]]
        line = line.replace(_datax[0], '')
        line = line.strip()
    except KeyError:
        _property[_name] = f2u.SecProp.SectionProperty(_Word)
        line, _property = shape_data(line, _property, _name, _Word,
                                     _length_factor)                        
        #
        _section[_name].shape = _Word
        _section[_name].properties = _property[_name]
    
    return line
#
# ----------------------
# node modules
#
def node_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"cartesian": r"cart",
            "cylindrical": r"cyli",
            "spherical": r"sphe"}

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
        # keyWord[3] = 'N/A'
    #
    return lineOut, keyWord
#
# ----------------------
# section modules
#
def section_word(lineIn):
    """
    """
    _key = {"ibeam": r"wf",
            "ibeamfab": r"fbi",
            "tubular": r"tub",
            "box": r"box",
            "rhs": r"rhs",
            "channel": r"chan",
            "angle": r"angl",
            "tee": r"tee",
            "general": r"pri",
            "circular bar" : r"fla",
            "membrane" : r"qum",
            "plate" : r"qus"}

    keyWord, lineOut, _match = asas_common.select(lineIn, _key)
    return lineOut, keyWord
#
#
def property_word(lineIn):
    """
    """
    _key = {"area": r"ax",
            "Iip": r"iy",
            "Iop": r"iz",
            "J": r"j",
            "shearAreaV": r"ay",
            "shearAreaH": r"az"}

    keyWord, lineOut, _match = asas_common.select(lineIn, _key)
    return lineOut, keyWord
#
#
def freedom_word(lineIn):
    """
    """
    # swap axis
    _key = {"0": r"x",
            "2": r"y",
            "1": r"z",
            "3": r"rx",
            "5": r"ry",
            "4": r"rz",
            "all": r"all"}

    keyWord, lineOut, _match = asas_common.select(lineIn, _key)
    return lineOut, keyWord
#
#
def local_freedom_word(lineIn):
    """
    """
    # swap axis
    _key = {"1": r"x",
            "2": r"y",
            "0": r"z",
            "4": r"rx",
            "5": r"ry",
            "3": r"rz",
            "all": r"all"}
    keyWord, lineOut, _match = asas_common.select(lineIn, _key)
    return lineOut, keyWord
#
def section_data(lineIn):
    """
    """
    lineOut = ''
    _data = []
    for x in lineIn.split():
        try:
            _data.append(float(x))
        except:
            _text = re.compile(x)
            _index = _text.search(lineIn, re.I)
            #
            _ind = _index.start()
            lineOut = lineIn[_ind:]
            lineOut = lineOut.strip()
            break
    #
    return lineOut, _data
#
#
def geometry_word(lineIn):
    """
    """
    _key = {"tubular": r"tube",
            "beam": r"beam",
            "beam2d": r"bm2d",
            "beam3d": r"bm3d",
            "step": r"[\:]?\s*step",
            "length": r"slen?",
            "shearareas": r"shar?",
            "offsetlocal": r"[\:]?\s*offs",
            "offsetglobal": r"[\:]?\s*offg",
            "offsetskew": r"[\:]?\s*ofsk",
            "node": r"node",
            "alpha": r"gpos",
            "angle": r"beta"}

    keyWord, lineOut, _match = asas_common.select(lineIn, _key)
    return lineOut, keyWord
#
#
def step_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"length": r"slen?"}
    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])
    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    #
    return lineOut, keyWord
#
#
def shape_data(line, _property, _name, _shape, _factor):
    """
    """
    _shape = f2u.SecProp.find_section(_shape)
    line, _item = section_data(line)
    _data = [x * _factor for x in _item]
    #
    if _shape == 'tubular':
        _property[_name].input_data(_data[0], _data[1])
        #
        if len(_data) > 2:
            line = " ".join(str(x) for x in _data[2:])
    #
    elif _shape == 'i section':
        try:
            _property[_name].input_data(_data[0], _data[3],
                                         _data[1], _data[2],
                                         _data[4], _data[5])
        except:
            _property[_name].input_data(_data[0], _data[3],
                                         _data[1], _data[2],
                                         _data[1], _data[2])
    #
    elif _shape == 'box':
        _D = _data[0]
        _B = _data[1]
        try:
            _tf = (_data[4] + _data[2]) / 2.0
            _tw = _data[3]
        except:
            _tf = _data[2]
            # fix this, it may be catching real data
            try:
                _tw = _data[3]
            except:
                _tw = _data[2]

        _property[_name].input_data(_D, _tw, _B, _tf)
    #
    elif _shape == 'channel':
        # line, _data = sectData(line)
        _property[_name].input_data(_data[0], _data[3],
                                     _data[1], _data[2])
        # print(_data)
    #
    elif _shape == 'angle':
        # line, _data = sectData(line)
        _property[_name].input_data(_data[0], _data[3],
                                     _data[1], _data[2])
        # print(_data)
    #
    elif _shape == 'tee':
        # line, _data = sectData(line)
        _property[_name].input_data(_data[0], _data[3],
                                     _data[1], _data[2])
        # print(_data)
    #
    elif 'shell' in _shape or 'membrane' in _shape:
        _data = sum(_data)/len(_data)
        _property[_name].input_data(_data)
    #
    elif _shape == 'circular bar':
        #print('--->')
        _data = (sum(_data)/len(_data))**0.50
        _property[_name].input_data(_data)
    #
    elif _shape == 'rectangular bar':
        _data = (sum(_data)/len(_data))**0.50
        _property[_name].input_data(_data, _data)    
    #
    #elif _shape == 'shell':
    #    _data = sum(_data)/len(_data)
    #    _property[_name].input_data(_data)    
    #
    else:
        print('+++ section shape error here', line)
    #
    return line, _property
#
#
# ----------------------
# material modules
#
def material_word(lineIn, keyWord, ind=1):
    """
    """
    _key = {"isotropic": r"iso",
            "orthotropic": r"orth",
            "woven": r"wove",
            "anisotropic": r"aiso",
            "hyperelastic": r"hype",
            "laminated": r"lami",
            "user": r"umat",
            "general": r"fiel",
            "piezoresistive": r"pier",
            "convection": r"cvec",
            "heat": r"radi",
            "plastic": r"plas",
            "creep": r"crep",
            "failuredata": r"fail", }

    keyWord[ind], lineOut, _match = asas_common.select(lineIn, _key, keyWord[ind])

    if _match:
        for x in range(ind + 1, len(keyWord)): keyWord[x] = 'N/A'
    return lineOut, keyWord
#
# ----------------------
# element modules
#
def element_word(lineIn):
    """
    """
    _key = {"group": r"grou[p]?",
            "material": r"matp",
            "beam": r"beam",
            "tubular": r"tube",
            "beam2d": r"bm2d",
            "beam3d": r"bm3d",
            "membrane": r"qum4",
            "soil": r"solp"}

    keyWord, lineOut, _match = asas_common.select(lineIn, _key)
    return lineOut, keyWord
#
# ----------------------
# hinge modules
#
#
def read_asas_geometry(file_name, global_units, 
                       superelement=False, 
                       conductor_bell=False,
                       general_section=False):
    """
    fem2ufo module reads ASAS .DAT files
    
    **Parameters**:  
      :fileName:  ASAS .DAT file (geometry and functional loading)
      :global_units : 
      :superelement : True = Master nodes are converted to element hinges
    **Returns**:
      fem2ufo FE concept model
    
    model
        |_ data
        |_ nodes
        |_ sections
        |_ elements
        |_ materials
        |_ vectors
        |_ eccentricities
        |_ hinges
        |_ sets
        |_ joints
        |_ boundaries
        |
        |_ concepts
        |_ cdcm
    
    """
    #
    print('')
    print('------------------ READASAS Module -----------------')    
    #
    _prop_dummy = {}
    _property = {}
    _section = {}
    _material = {}
    _concept = {}
    _group = {}
    _hinge = {}
    _tubular_joint = {}
    _step = {}
    _eccentricity = {}
    _link_joints = {}
    #
    model = f2u.F2U_model(file_name)
    model.set_component()    
    #
    _load = {}
    #
    _fix_no = 0
    _group_no = 0
    #
    factors = [0, 0, 0, 0, 0, 0]
    #
    # read ASAS input file
    asas_file = process.common.check_input_file(file_name)
    if not asas_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    #
    print('')
    print('--- Reading geometry from {:} input file'.format(file_name))
    print('')
    #
    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']

    # Units [length, mass, time, temperature, force, pressure/stress]
    global_length = global_units[0]
    global_force = global_units[4]
    _length_factor, _force_factor = process.units.convert_units_SI(global_length, 
                                                                   global_force)

    #    
    _mat_flag = True
    _soil_no = ''
    dat_temp = open('datTemp.dat','w+')
    #
    print('    * First pass')
    #
    with open(asas_file) as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            # find key words and clean lines from comments
            if re.search(r"\*", line):
                line = asas_common.remove_comment(line)
            #
            line = line.strip()
            if not line:
                continue
            #
            #line, _keyWord = asas_common.master_word(line, _keyWord)
            #
            if 'JOB' in line:
                continue
            #
            elif 'FILES' in line:
                continue
            # UNITS card
            elif 'UNITS' in line and _mat_flag:
                if re.search(r"disp|stre", line, re.IGNORECASE):
                    continue
                line, _length_factor, _force_factor= asas_common.unit_factor(line, 
                                                                             global_length,
                                                                             global_force)
                
            #
            elif 'GEOM' in line.upper():
                _mat_flag = False
                dat_temp.write("\n")
                dat_temp.write(line)            
            #
            # COOR Card
            elif 'COOR' in line:
                while True:
                    line2 = get_line(fin)
                    # Jump commented lines
                    if re.match(r"\*", line2):
                        continue
                    
                    if not line2:
                        continue                    
                    
                    if 'END' in line2:
                        _length_factor, _force_factor = process.units.convert_units_SI(global_length, global_force)
                        break
                    
                    _line2 = line2.split()
                    
                    #line2, _keyWord = node_word(line2, _keyWord)
                    #
                    if 'UNITS' in line2:
                        line2, _length_factor, _force_factor = asas_common.unit_factor(line2, 
                                                                                       global_length,
                                                                                       global_force)
                        #print('node factors : length ', _length_factor, ' force ', _force_factor)
                    #
                    #
                    elif 'CART' in line2:
                        _system = 'cartesian'
                    #
                    elif 'CYLI' in  line2:
                        _system = 'cylindrical'
                    #
                    elif 'SPHE' in line2:
                        _system = 'spherical'
                    #
                    elif type(eval(_line2[0])) == int:
                        _nodeNo = _line2[0]
                        _x = float(_line2[1]) * _length_factor
                        _y = float(_line2[2]) * _length_factor
                        _z = float(_line2[3]) * _length_factor
                        #_coord = [_x, _y, _z]
                        #_coord = {'x':_x, 'y':_y, 'z':_z}
                        
                        #_node[_nodeNo] = f2u.Geometry.Node(_nodeNo, _nodeNo)
                        #_node[_nodeNo].coordinates(_x, _y, _z)
                        #_node[_nodeNo].coordinate_system = _system
                        #
                        model.component.add_node(name=_nodeNo, 
                                                 number=int(_nodeNo),
                                                 coordinates=[_x, _y, _z])
                        
                        #model.component.nodes[_nodeNo].set_coordinates(_x, _y, _z)
                        model.component.nodes[_nodeNo].coordinate_system = _system
                        #
                        try:
                            _name_system = _line2[4]
                            print(' warning node system name')
                        except IndexError:
                            pass
                    #
                    else:
                        print('error nodes here ==> ', line)
                        #
            #
            # SECT Card
            elif 'SECT' in line:
                while True:
                    line2 = get_line(fin)
                    # Jump commented lines
                    if re.match(r"\*", line2):
                        continue
                    
                    if not line2:
                        continue
                    
                    if 'END' in line2:
                        _length_factor, _force_factor = process.units.convert_units_SI(global_length, 
                                                                                       global_force)
                        break
                    
                    _line2 = line2.split()
                    if 'UNITS' in line2:
                        line2, _length_factor, _force_factor = asas_common.unit_factor(line2, 
                                                                                       global_length,
                                                                                       global_force)
                        #print('sect prop factors : length ', _length_factor, ' force ', _force_factor)
                    #
                    elif re.search(r"wf|fbi|tub|rhs|box|chan|angl|tee|pri", line2, re.I):
                        line2, _name = asas_common.word_pop(line2)
                        line2, _key1 = section_word(line2)
                        _property[_name] = f2u.SecProp.SectionProperty(_key1)
                        #
                        if _key1 == 'general':
                            _property[_name].general_section_data(0, 0, 0, 0, 0, 0,
                                                                  0, 0, 1, 1, 0, 0)
                        # XSEC
                        if re.search(r"xsec", line2, re.I):
                            line2, item = asas_common.word_pop(line2)
                            line2, _property = shape_data(line2, _property, _name, _key1,
                                                          _length_factor)
                    #
                    elif re.search(r"fab", line2, re.I):
                        line2, _fabname = asas_common.word_pop(line2)
                        print('fab sec name', _fabname)
                    #
                    # FLEX
                    elif 'FLEX' in line2:
                        line2 = line2.replace(':', '')
                        line2 = line2.replace('FLEX', '')
                        line2 = line2.strip()
                        # line, item = wordPop(line)
                        try:
                            line2, _data = section_data(line2)
                            Area, Iop, Iip, J = _data[:4]
                            _property[_name].general_section_data(Area * _length_factor**2,
                                                                  Iip * _length_factor**4,
                                                                  Iop * _length_factor**4,
                                                                  J * _length_factor**4,
                                                                  0, 0, 0, 0, 1, 1, 0, 0)
        
                            try:
                                _property[_name].shearAreaV = _data[5] * _length_factor**2
                                _property[_name].shearAreaH = _data[6] * _length_factor**2
                            except:
                                pass
                        #
                        except:
                            line2, _key1 = property_word(line2)
                            # 
                            if _key1 == 'area':
                                line2, _data = section_data(line2)
                                _property[_name].Area = _data[0] * _length_factor**2
        
                            elif _key1 == 'J':
                                line2, _data = section_data(line2)
                                _property[_name].J = _data[0] * _length_factor**4
        
                            elif _key1 == 'Iip':
                                line2, _data = section_data(line2)
                                _property[_name].Iip = _data[0] * _length_factor**4
        
                            elif _key1 == 'Iop':
                                line2, _data = section_data(line2)
                                _property[_name].Iop = _data[0] * _length_factor**4
        
                            elif _key1 == 'shearAreaV':
                                line2, _data = section_data(line2)
                                _property[_name].shearAreaV = _data[0] * _length_factor**2
        
                            elif _key1 == 'shearAreaH':
                                line2, _data = section_data(line2)
                                _property[_name].shearAreaH = _data[0] * _length_factor**2
        
                            else:
                                print('*** section properties error here', line2)
                        #
                        if general_section:
                            _property[_name].shape = 'general section'
                    #
                    else:
                        try:
                            line2, item = asas_common.word_pop(line2)
                        except:
                            pass
                        print('error section shape here ==> ', item, '-', line2)
            #
            elif 'MATE' in line:
                while True:
                    line2 = get_line(fin)
                    if not line2:
                        continue
                    # Jump commented lines
                    if re.match(r"\*", line2):
                        continue
                    
                    if 'END' in line2:
                        _length_factor, _force_factor = process.units.convert_units_SI(global_length, 
                                                                                global_force)
                        break
                    
                    _line2 = line2.split()                 
    
                    if 'UNITS' in line2:
                        line2, _length_factor, _force_factor = asas_common.unit_factor(line2, 
                                                                                       global_length,
                                                                                       global_force)
                    #
                    elif re.match(r"\d", line2):
                        line2, _item = asas_common.word_pop(line2)
                        _mat_name = _item
                        _material[_mat_name] = f2u.Material(_mat_name, int(_mat_name))
                        _material[_mat_name].type = 'Plastic'
                        #
                        if  'ISO' in line2:
                            line2, _name = asas_common.word_pop(line2)
                            line2, _data = section_data(line2)
                            #
                            _material[_mat_name].E = float(_data[0] * _force_factor / _length_factor**2)
                            _material[_mat_name].poisson = float(_data[1])
                            #_material[_mat_name].type = 'Elastic'
        
                            try:
                                _material[_mat_name].alpha = float(_data[2])
                                _material[_mat_name].density = float(_data[3]) * _force_factor
                            except:
                                print('error material No {:} no found'.format(_mat_name))
                                pass
                        #
                        elif 'ORTH' in line2:
                            print("orthotropic", line)
                            break
                        #
                        elif 'PLAS' in line2:
                            print("plastic", line)
                            break
                        #
                        else:
                            # consider material isotropic by default
                            #_keyWord[1] = 'isotropic'
                            line2, _data = section_data(line2)
                            #
                            _material[_mat_name].E = float(_data[0] * _force_factor / _length_factor**2)
                            _material[_mat_name].poisson = float(_data[1])
        
                            try:
                                _material[_mat_name].alpha = float(_data[2])
                                _material[_mat_name].density = float(_data[3]) * _force_factor
                            except:
                                print('error material No {:} no found'.format(_mat_name))
                                pass
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
            #
    #
    dat_temp.close()
    #
    _sec_dummy = {}
    #
    print('    * Second pass')
    #
    _NoFix = 0
    dat_temp = open('datTemp2.dat','w+')
    #
    with open('datTemp.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue
            #
            elif 'GEOM' in line.upper():
                while True:
                    line2 = get_line(fin)
                    # Jump commented lines
                    if re.match(r"\*", line2):
                        continue
                    #
                    if not line2:
                        continue
                    #
                    if 'END' in line2:
                        _length_factor, _force_factor = process.units.convert_units_SI(global_length,
                                                                                       global_force)
                        break
                    #
                    #_line2 = line2.split()
                    #
                    if 'UNITS' in line2:
                        line2, _length_factor, _force_factor = asas_common.unit_factor(line2, 
                                                                                       global_length,
                                                                                       global_force)
                    #
                    elif re.match(r"\d", line2):
                        line2, _sect_name = asas_common.word_pop(line2)
                        try:
                            _sect_name = str(int(_sect_name))
                        except ValueError:
                            pass
                        _section[_sect_name] = f2u.Geometry.Section( _sect_name, int(_sect_name),
                                                                     'BEAM', 'GENERAL_BEAM')
                        _geomNo = 0
                        #
                        #if _sect_name == '304':
                        #    print('here')
                        #
                        if 'TUBE' in line2:
                            line2, _key1 = geometry_word(line2)
                            line2 = tub_section_check(line2, _sect_name, _key1, _section,
                                                      _property, _sec_dummy, 
                                                      _length_factor)
                            #
                            # FIXME: weak way to identify pile length data
                            if line2:
                                # it is assumed a pile section
                                _items = line2.split()
                                try:
                                    float(_items[0])
                                    _section[_sect_name].length = float(_items[3]) * _length_factor
                                except IndexError :
                                    if len(_items) == 1:
                                        _section[_sect_name].length = float(_items[0]) * _length_factor
                                        #line2 = 'SLEN'
                                    elif len(_items) == 2:
                                        _section[_sect_name].length = float(_items[1]) * _length_factor
                                        #line2 = 'SLEN'
                                    else:
                                        print('  error section line : {:}'.format(line2))
                                except ValueError:
                                    pass
                        #
                        elif 'BEAM' in line2 or 'BM3D' in line2:
                            line2, _item = asas_common.word_pop(line2)
                            line2, _pname = asas_common.word_pop(line2)
                            _secName = f2u.SecProp.find_section(_property[_pname].shape)
                            _section[_sect_name].shape = _secName
                            _section[_sect_name].properties = _property[_pname]
                        #
                        elif 'FLA2' in line2:
                            line2, _name = asas_common.word_pop(line2)
                            _name = 'FLA2_' + _sect_name
                            _key1 = 'rectangular bar'
                            _property[_name] = f2u.SecProp.SectionProperty(_key1)
                            line, _property = shape_data(line2, _property, _name, _key1,
                                                         _length_factor)
                            #
                            _secName = f2u.SecProp.find_section(_key1)
                            _section[_sect_name].shape = _secName
                            _section[_sect_name].properties = _property[_name]                            
                        #
                        elif 'FLA3' in line2:
                            print('fix fla3')
                            #'circular bar'
                        
                        elif 'QUM4' in line2:
                            line2, _name = asas_common.word_pop(line2)
                            # 'membrane'
                            _name = 'QUM4_' + _sect_name
                            _key1 = 'membrane'
                            _property[_name] = f2u.SecProp.SectionProperty(_key1, _name)
                            
                            line, _property = shape_data(line2, _property, _name, _key1,
                                                         _length_factor)
                            
                            _secName = f2u.SecProp.find_section(_key1)
                            _section[_sect_name].shape = _secName
                            _section[_sect_name].properties = _property[_name]
                            _section[_sect_name].type = 'membrane'
                            #_section[_sect_name].name = 'PLT_'+_sect_name
                            # check
                            _prop_dummy[_sect_name] = _section[_sect_name]                            
                        
                        elif 'QUS4' in line2:
                            #print('fix QUS4')
                            line2, _name = asas_common.word_pop(line2)
                            # 'plate'
                            _name = 'QUS4_' + _sect_name
                            _key1 = 'shell'
                            _property[_name] = f2u.SecProp.SectionProperty(_key1, _name)
                        
                            line, _property = shape_data(line2, _property, _name, _key1,
                                                         _length_factor)
                            
                            _secName = f2u.SecProp.find_section(_key1)
                            _section[_sect_name].shape = _secName
                            _section[_sect_name].properties = _property[_name]
                            _section[_sect_name].type = 'shell'
                            #_section[_sect_name].name = 'PLT_'+_sect_name
                            # check
                            _prop_dummy[_sect_name] = _section[_sect_name]
                        
                        else:
                            print('    *** error geometry not recognised {:}'.format(line2))
                            1/0.0
                        #
                        if 'SLEN' in line2:
                            line2 = line2.replace('SLEN', '')
                            line2, item = asas_common.word_pop(line2)
                            _section[_sect_name].length = float(item) * _length_factor
                    #
                    elif 'STEP' in line2:
                        _geomNo += 1
                        _nameStep = str(_section[_sect_name].name) + '_step_' + str(_geomNo)
                        _shape = _section[_sect_name].shape
                        try:
                            _section[_sect_name].step.append(_nameStep)
                        except AttributeError:
                            _section[_sect_name].step = [_nameStep]
                        #
                        _section[_nameStep] = f2u.Geometry.Section(_nameStep, _nameStep,
                                                               'beam', _shape)
                        #
                        line2 = line2.replace(':', '')
                        line2 = line2.replace('STEP', '')
                        line2 = line2.strip()
                        _data = line2.split()
                        #
                        try:
                            _section[_nameStep].properties = _property[_data[0]]
                            try:
                                _section[_nameStep].length = float(_data[1]) * _length_factor
                            except IndexError:
                                pass                        
                        except KeyError:
                            if _shape == 'tubular':
                                line2 = tub_section_check(line2, _nameStep, _shape, _section,
                                                          _property, _sec_dummy, _length_factor)                             
                                #                            
                                try:
                                    _section[_nameStep].length = float(_data[2]) * _length_factor
                                except IndexError:
                                    #_section[_nameStep].length = False
                                    pass
                                line2 = ''
                            else:
                                print ("Fix this step section: ", _section[_sect_name].shape)
                        #
                    #
                    elif 'OFFS' in line2:
                        #
                        #if _sect_name == '3519':
                        #    print('-->')
                        #
                        line2 = line2.replace(':', '')
                        line2 = line2.replace('OFFS', '')
                        line2 = line2.strip()                        
                        #_eccentnumber = _section[_sect_name].number
                        line2, _data = section_data(line2)
                        
                        _data = [x * _length_factor for x in _data]
                        
                        _eccentricity[_sect_name] = f2u.Geometry.Eccentricities(_sect_name,
                                                                                int(_sect_name))
                        _eccentricity[_sect_name].case = 2
                        # TODO : check sing of z
                        try:
                            _data_swap = [_data[0], _data[2], -1*_data[1], -1*_data[3], _data[5], -1*_data[4]]
                            _eccentricity[_sect_name].eccentricity.extend(_data_swap)
                        except IndexError:
                            _data = [0, 0 , _data[0]]
                            _eccentricity[_sect_name].eccentricity.extend(_data)
                            #print('    *** Warning plate offset ignored')
                            #continue
                        #
                    #
                    elif 'OFFG' in line2:
                        #
                        #if _sect_name == '3519':
                        #    print('-->')                        
                        #
                        line2 = line2.replace(':', '')
                        line2 = line2.replace('OFFG', '')
                        line2 = line2.strip()
                        #_eccentnumber = _section[_sect_name].number
                        line2, _data = section_data(line2)
                        _data = [x * _length_factor for x in _data]
                        # swap local axis
                        _data_swap = _data
                        _data_swap = [_data[0], _data[1], _data[2], _data[3], _data[4], _data[5]]
                        #
                        _eccentricity[_sect_name] = f2u.Geometry.Eccentricities(_sect_name,
                                                                            int(_sect_name))
                        _eccentricity[_sect_name].case = 1
                        _eccentricity[_sect_name].eccentricity.extend(_data_swap)
                        #
                    #
                    elif 'NODE' in line2:
                        print('  *** warning node in section module needs to be fixed : ', line2)
                        if _section[_sect_name].shape == 'tubular':
                            print('section :tubular --> omit')
                        else:
                            print('section :', _section[_sect_name].shape, line2)
                        line2 = ''
                    #
                    elif 'GPOS' in line2:
                        if _section[_sect_name].shape != 'tubular':
                            print('  *** warining alpha in section modules needs fix : ', line2)
                            print('      section :', _section[_sect_name].shape)
                        line2 = ''
                    #
                    elif 'BETA' in line2:
                        print('  *** warning angle in section modules needs fix: ', line2)
                        if _section[_sect_name].shape == 'tubular':
                            print('tubular')
                        line2 = ''
                    #
                    else:
                        line, item = asas_common.word_pop(line2)
                        # print('** ',item, '-',line)
                        print('error section here ==> ', line2)
                        line2 = ''
            #
            elif 'SUPP' in line.upper():
                while True:
                    line2 = get_line(fin)
                    # Jump commented lines
                    if re.match(r"\*", line2):
                        continue
                    
                    if not line2:
                        continue                    
                    
                    if 'END' in line2:
                        _length_factor, _force_factor = process.units.convert_units_SI(global_length,
                                                                                global_force)
                        break
                    #
                    if 'ALL' in line2:
                        line2 = line2.replace('ALL', '')
                        line2 = line2.split()
                        for _nodeNo in line2:
                            #line2, _nodeNo = asas_common.word_pop(line2)
                            try:
                                model.component.nodes[_nodeNo]
                                model.component.add_boundary(name=_nodeNo,
                                                             node_name=_nodeNo)
                                model.component.boundaries[_nodeNo].fixed
                                model.component.nodes[_nodeNo].boundary.type = 'fixed'
                            except KeyError:
                                print('   *** warning node {:} support not found'.format(_nodeNo))
                    else:
                        print(line2)
                        line2, item = asas_common.word_pop(line2)
                        print('** ', item, '-', line2)
                        print('error fixity here ==> ', line2)
            #            
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    os.remove('datTemp.dat')
    #
    print('    * Third pass')
    #
    dat_temp = open('CheckMe_' + asas_file,'w+')
    _memb_no = 0
    with open('datTemp2.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue
            #
            elif 'ELEM' in line.upper() or 'PILE' in line.upper():
                _type = line.lower()
                while True:
                    line2 = get_line(fin)
                    # Jump commented lines
                    if re.match(r"\*", line2):
                        continue
                    
                    if not line2:
                        continue
                    
                    if 'END' in line2:
                        _length_factor, _force_factor = process.units.convert_units_SI(global_length,
                                                                                       global_force)
                        break
                    #
                    #line, _keyWord = element_word(line, _keyWord)
                    #
                    elif 'GROU' in line2:
                        # can be group
                        line3 = line2.split()
                        line2 = line3[1]
                        line2, _set_number = asas_common.word_pop(line2)
                        try:
                            _group[_set_number]
                        except KeyError:
                            #_group_no += 1
                            _group[_set_number] = f2u.Geometry.Groups(_set_number, 
                                                                  int(_set_number))
                            #_group[_set_number].type = 2
                    #
                    elif 'MATP' in line2:
                        line2 = line2.replace('MATP', '')
                        line2, _mat_name = asas_common.word_pop(line2)
                        #_matNo = int(_matNo)
                    #
                    elif 'SOLP' in line2:
                        line2 = line2.replace('SOLP', '')
                        line2, _soil_no = asas_common.word_pop(line2)
                    #
                    elif 'TUBE' in line2:
                        line2 = line2.replace('TUBE', '')
                        line2, _data = section_data(line2)
                        try:
                            _memb_name = str(int(_data[3]))
                            _memb_no = int(_data[3])
                        except IndexError:
                            _memb_name = str(int(_data[2]))
                            _memb_no += 1
                        
                        #if _memb_name == '304':
                        #    print('ok')
                        #
                        try:
                            _concept[_memb_name]
                            _memb_name = str(int(_data[0])) + '_' + str(int(_data[1]))
                        except KeyError:
                            pass
                        #
                        _concept[_memb_name] = f2u.Concept(_memb_name, _memb_no)
                        _concept[_memb_name].type = 'beam'
                        if _type == 'pile':
                            _concept[_memb_name].type = 'pile'
                        #
                        _concept[_memb_name].material.append(_material[_mat_name])
                        _material[_mat_name].concepts.append(_memb_name)
                        # offset
                        #
                        _node1 = str(int(_data[0]))
                        _concept[_memb_name].node.append(model.component.nodes[_node1])
                        model.component.nodes[_node1].elements.append(_memb_name)
        
                        _node2 = str(int(_data[1]))
                        _concept[_memb_name].node.append(model.component.nodes[_node2])
                        model.component.nodes[_node2].elements.append(_memb_name)
                        
                        _sect_name = str(int(_data[2]))
                        _concept[_memb_name].section.append(_section[_sect_name])
                        _section[_sect_name].elements.append(_memb_name)
                        #
                        #
                        try:
                            # FIXME : what does _step do?
                            _concept[_memb_name].section.extend(_step[_data[2]])
                            # is this instead?
                            #_concept[_memb_name].section.append(_section[_sect_name])
                            _test = _concept[_memb_name]
                            _section[_sect_name].elements.append(_memb_name)
                        except KeyError:
                            pass
                        #
                        try:
                            _group[_set_number].concepts.append(_memb_name)
                        except KeyError:
                            pass
                        except UnboundLocalError:
                            pass
                    #
                    elif 'QUM4' in line2 or 'QUS4' in line2:
                        line2, _name = asas_common.word_pop(line2)
                        line2, _data = section_data(line2)
                        _memb_no = int(_data.pop())
                        _memb_name = str(_memb_no)
                        _concept[_memb_name] = f2u.Geometry.ShellElement(_memb_name, _memb_no)
                        if 'QUS4' in _name:
                            _concept[_memb_name].type = 'shell'
                        else:
                            _concept[_memb_name].type = 'membrane'
                        #
                        _sect_name = str(int(_data.pop()))
                        _concept[_memb_name].section.append(_section[_sect_name])
                        _section[_sect_name].elements.append(_memb_name)
                        _concept[_memb_name].material.append(_material[_mat_name])
                        _material[_mat_name].concepts.append(_memb_name)
                        #
                        for _node in _data:
                            _node_name = str(int(_node))
                            try:
                                _concept[_memb_name].node.append(model.component.nodes[_node_name])
                                model.component.nodes[_node_name].elements.append(_memb_name)
                            except KeyError:
                                print('   *** error Node {:} not found'
                                      .format(_node_name))
                                sys.exit()
                        #
                        try:
                            _group[_set_number].concepts.append(_memb_name)
                        except UnboundLocalError:
                            print('   **** warning group option missing in geometry  : {:}'.format(_set_number))
                    #
                    elif 'FLA2' in line2:
                        line2, _name = asas_common.word_pop(line2)
                        line2, _data = section_data(line2)
                        _memb_no = int(_data.pop())
                        _memb_name = str(_memb_no)
                        
                        _concept[_memb_name] = f2u.Concept(_memb_name, _memb_no)
                        _concept[_memb_name].type = 'beam'
                        _sect_name = str(int(_data.pop()))
                        _concept[_memb_name].section.append(_section[_sect_name])
                        _section[_sect_name].elements.append(_memb_name)
                        
                        _node1 = str(int(_data[0]))
                        _concept[_memb_name].node.append(model.component.nodes[_node1])
                        model.component.nodes[_node1].elements.append(_memb_name)
                        
                        _concept[_memb_name].material.append(_material[_mat_name])
                        _material[_mat_name].concepts.append(_memb_name)
                    
                        _node2 = str(int(_data[1]))
                        _concept[_memb_name].node.append(model.component.nodes[_node2])
                        model.component.nodes[_node2].elements.append(_memb_name)
                        #
                        try:
                            _group[_set_number].concepts.append(_memb_name)
                        except UnboundLocalError:
                            _set_number =  "set_" + _mat_name
                            print('   **** warning group option missing in geometry : {:}'.format(_set_number))                       
                    #
                    elif 'FLA3' in line2:
                        print('fix fla3')
                        line2 = ''
                    #
                    else:
                        line2, _key1 = element_word(line2)
                        if not 'beam' in _key1:
                            print('????')
                            1/0
                        #line2 = line2.replace('GROU', '')
                        line2, _data = section_data(line2)
                        _memb_name = str(int(_data[3]))
                        _memb_no = int(_data[3])
                        
                        #if _memb_name == '3575':
                        #    print('here')                        
        
                        #_concept[_memb_name] = f2u.BeamElement(_memb_name, _memb_no)
                        _concept[_memb_name] = f2u.Concept(_memb_name, _memb_no)
                        _concept[_memb_name].type = 'beam'
                        #_concept[_memb_name].typeAd = _soil_no
                        _concept[_memb_name].material.append(_material[_mat_name])
                        _material[_mat_name].concepts.append(_memb_name)
                        
                        # offset
                        #_concept[_memb_name].offsets.append(_eccentricity[_mat_name])
                        #                        
                        _node1 = str(int(_data[0]))
                        _concept[_memb_name].node.append(model.component.nodes[_node1])
                        model.component.nodes[_node1].elements.append(_memb_name)
                        
                        _node2 = str(int(_data[1]))
                        _concept[_memb_name].node.append(model.component.nodes[_node2])
                        model.component.nodes[_node2].elements.append(_memb_name)
                        
                        _sect_name = str(int(_data[2]))
                        _concept[_memb_name].section.append(_section[_sect_name])
                        _section[_sect_name].elements.append(_memb_name)
                        
                        try:
                            _group[_set_number].concepts.append(_memb_name)
                        except UnboundLocalError:
                            _set_number =  "set_" + _mat_name
                            print('   **** warning group option missing in geometry --> new created : {:}'.format(_set_number))
                            try:
                                _group[_set_number]
                            except KeyError:
                                _group[_set_number] = f2u.Geometry.Groups(_set_number, 
                                                                      int(_mat_name))
                            
                            _group[_set_number].concepts.append(_memb_name)
            #
            elif 'LINK' in line.upper() and superelement:
                # Master nodes will be converted to element hinges
                while True:
                    line2 = get_line(fin)
                    
                    if re.match(r"\*", line2):
                        continue
                    
                    if not line2:
                        continue                    
                    
                    if 'END' in line2:
                        _length_factor, _force_factor = process.units.convert_units_SI(global_length,
                                                                                       global_force)
                        break
                    #
                    elif 'RP' in line2:
                        print('  ***error fix repeat in link')
                        sys.exit()
                    #
                    elif 'RRP' in line2:
                        print('  ***error data generation in link')
                        sys.exit()
                    #
                    else:
                        _hinge_temp = [0, 0, 0, 0, 0, 0]
                        for x in line2:
                            line2, _key1 = freedom_word(line2)
                        
                            if _key1 == 'all':
                                line2 = ''
                                break
                            #
                            elif _key1 == 'N/A':
                                _nodeNo = model.component.nodes[line2]
                                _link_joints[_nodeNo.name] = f2u.Geometry.Joints(name='link', 
                                                                                 node=_nodeNo)
                                #_link_joints[_nodeNo.name].name = 'link'
                                #_link_joints[_nodeNo.name].node = _nodeNo
                                _link_joints[_nodeNo.name].type = None
                                
                                #
                                # switch off allocation of hinges
                                #
                                #for _memb_name in _nodeNo.elements:
                                    ##_memb_name =_nodeNo.elements[0]
                                    #_node_no = _nodeNo.name
                                    #_memb = _concept[_memb_name]
                                    #
                                    #_node_end = 0
                                    #if _node_no == _memb.node[1].number:
                                    #    _node_end = 1
                                    #
                                    #if _memb.slope > 0.1:
                                    #    _hinge_temp = [_hinge_temp[1], _hinge_temp[2], _hinge_temp[0],
                                    #                   _hinge_temp[4], _hinge_temp[5], _hinge_temp[3]]                                    
                                    #
                                    #_fix_no += 1
                                    #_fix_name = str(_memb_name) +'_'+ str(_node_end)
                                    ##_fix_name = str(_memb_name) +'_link_'+ str(_node_end)
                                    #_hinge[_fix_name] = f2u.Hinges(_fix_no, _fix_name)
                                    #_hinge[_fix_name].fix = _hinge_temp                                
                                    #
                                    #try:
                                    #    _memb.releases[_node_end] = _hinge[_fix_name]
                                    #except IndexError:
                                    #    _memb.releases.append(None)
                                    #    _memb.releases.append(None)
                                    #    _memb.releases[_node_end] = _hinge[_fix_name]
                            #
                            else:
                                _hinge_temp[int(_key1)] = 1
                        #
            #
            elif 'RELE' in line.upper():
                while True:
                    line2 = get_line(fin)
                    # Jump commented lines
                    if re.match(r"\*", line2):
                        continue
                    
                    if not line2:
                        continue                    
                    
                    if 'END' in line2:
                        _length_factor, _force_factor = process.units.convert_units_SI(global_length,
                                                                                global_force)
                        break
                    #    
                    # print('    *** Warning : hinges module needs to be fixed : ',line)
                    #
                    elif 'RP' in line2:
                        print('  ***error fix repeat in hinges')
                        sys.exit()
                    
                    elif 'RRP' in line2:
                        print('  ***error data generation in hinges')
                        sys.exit()
    
                    else:
                        line2, _key1 = freedom_word(line2)
                        line2 = str(line2).split()
                        _prefix = ''
                        if len(line2) > 2:
                            _prefix = line2.pop(0)
                            _prefix = '_' + _prefix
                        #
                        _hinge_no = _key1
                        _memb_name = str(int(line2[0]))
                        _node_no = int(line2[1])
                        _fix_no += 1
                        line2 = ''
                        #
                        _memb = _concept[_memb_name]
                        _node_end = 0
                        if _node_no == _memb.node[1].name:
                            _node_end = 1
                        #
                        _fix_name = str(_memb_name) +'_'+ str(_node_end) + _prefix
                        #
                        try:
                            if _hinge_no == 'all':
                                _hinge[_fix_name].fix = [0, 0, 0, 0, 0, 0]
                            else:
                                _hinge_no = int(_hinge_no)
                                _hinge[_fix_name].fix[_hinge_no] = 0
                        except KeyError:
                            _hinge[_fix_name] = f2u.Geometry.Hinges(_fix_name,
                                                                _fix_no)
                            _hinge[_fix_name].fix = [1, 1, 1, 1, 1, 1]
    
                            if _hinge_no == 'all':
                                _hinge[_fix_name].fix = [0, 0, 0, 0, 0, 0]
                            else:
                                _hinge_no = int(_hinge_no)
                                _hinge[_fix_name].fix[_hinge_no] = 0
                        #
                        try:
                            _memb.releases[_node_end] = _hinge[_fix_name]
                        except IndexError:
                            _memb.releases.append(None)
                            _memb.releases.append(None)
                            _memb.releases[_node_end] = _hinge[_fix_name]
                    #
            #
            elif 'CONS' in line.upper():
                # constrain will be converted to sesam shim elements
                while True:
                    line2 = get_line(fin)
                    # Jump commented lines
                    if re.match(r"\*", line2):
                        continue
                    
                    if not line2:
                        continue
                    
                    if 'END' in line2:
                        _length_factor, _force_factor = process.units.convert_units_SI(global_length,
                                                                                global_force)
                        break
                    #
                    elif 'RP' in line2:
                        print('  ***error fix repeat in constrains')
                        sys.exit()
                    
                    elif 'RRP' in line2:
                        print('  ***error data generation in constrains')
                        sys.exit()
    
                    else:
                        dddof = [0 for x in range(6)]
                        _dummy = 1.00000000E+10
                        line2, _key1 = freedom_word(line2)
                        _dof = int(_key1)
                        
                        line2, _item = asas_common.word_pop(line2)
                        _node_slave = _item
                        
                        line2, _item = asas_common.word_pop(line2)
                        _factor = float(_item )
                        
                        line2, _key1 = freedom_word(line2)
                        _dof = int(_key1)                        
                        
                        #
                        dddof[1] = _dummy * abs(_factor)
                        dddof[2] = _dummy * abs(_factor)
                        dddof[3] = _dummy * abs(_factor)
                        
                        line2, _item = asas_common.word_pop(line2)
                        _node_master = _item
                        #_memb_name = str(_node_slave) + str(_node_master)
                        try:
                            if re.match(r"\+|\-", line2):
                                _factor = line2.split()
                                _factor = _factor[0]
                                line2 = line2.replace(_factor, '')
                                line2 = line2.strip()
                                _factor = float(_factor)
                            else:
                                line2, _item = asas_common.word_pop(line2)
                                _factor = float(_item)
                            
                            line2, _key1 = freedom_word(line2)
                            _dof = int(_key1)
                            dddof[_dof] = _dummy * abs(_factor)
                        except IndexError:
                            pass
                        #
                        #
                        # create dummy joint
                        # note joint number set to master node --> this is use to
                        # identify it
                        _tubular_joint[_node_slave] = f2u.Geometry.Joints(name = _node_master,
                                                                          node = model.component.nodes[_node_slave])
                        #_tubular_joint[_node_slave].node = model.component.nodes[int(_node_slave)]
                        #_tubular_joint[_node_slave].name = 'dum'
                        _tubular_joint[_node_slave].type = 'shim'
                        _tubular_joint[_node_slave].set_spring(dddof)
                        _tubular_joint[_node_slave].brace = model.component.nodes[_node_master].elements
                        #
                        #
                        if not conductor_bell:
                            #model.component.nodes[_node_slave].elements.extend(model.component.nodes[_node_master].elements)
                        #else:
                            _tubular_joint[_node_slave].chord = copy.copy(model.component.nodes[_node_slave].elements)
                        #
                        model.component.nodes[_node_slave].elements.extend(model.component.nodes[_node_master].elements)
                        #
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
        #
    #
    #
    dat_temp.close()
    os.remove('datTemp2.dat')
    #
    # ------------
    # find offsets
    #
    process.geometry.update_offsets(_eccentricity)
    #  
    #
    #for memb in _concept.values():
    #    memb
    #
    #memb = _concept['9315']
    #off = _eccentricity[9100]
    # ---------------------
    # find stepped sections
    #
    _tapered = {}
    _geometry_items = process.concept.find_step_section(_concept, 
                                                        _section,
                                                        _material,
                                                        _eccentricity,
                                                        _tapered,
                                                        asas=True)
    #
    # ---------------------------
    # create superstructure group
    #
    _set_number = [int(_set.number)  for _set in _group.values()]
    if _set_number:
        _set_number = str(max(_set_number)+10)
    else:
        _set_number = '1'
    #
    _set_name = 'sub_structure'
    #
    _group[_set_number] = f2u.Geometry.Groups(_set_name, int(_set_number))
    #_group[_set_number].type = 2
    _group[_set_number].concepts = _geometry_items
    #
    #
    # ----------------------------------------------------------------------
    #                       Find Overlapping Members
    # ----------------------------------------------------------------------
    # this is probably redundant
    #asas_process.find_overlapping_beam(model.component.nodes, 
    #                                   _concept, tol=2)    
    #
    #
    #
    # -------------------
    # create model object
    #
    #model = f2u.F2U_model(file_name)
    #model.set_component()
    #model.component.nodes = _node
    model.component.sections = _section
    model.component.concepts = _concept
    model.component.materials = _material
    # model.component.vectors = _unit_vector
    model.component.eccentricities = _eccentricity
    model.component.hinges = _hinge
    model.component.sets = _group
    model.component.data = {}  # _user_data
    # model.component.boundaries = _boundary
    # model.component.cdcm = _hydro
    # model.component.concepts = _concept
    #
    #
    # ----------------------------------------------------------------------
    #                             LOAD SECTION
    # ----------------------------------------------------------------------
    #
    #model['load'] = f2u.LoadType(no+1, 'loading')
    #model['load'].functional = {}
    #
    #load.read_asas_load(file_name, model, global_units)
    #
    #
    # ----------------------------------------------------------------------
    #                 Find Overlapping Members Constrain flag
    # ----------------------------------------------------------------------
    #
    if _tubular_joint:
        if conductor_bell:
            model.component.joints = asas_process.get_bells(model.component.concepts, 
                                                            _tubular_joint)
        else:
            #model.component.joints = _tubular_joint
            model.component.joints = asas_process.get_joints(model.component.concepts, 
                                                             model.component.nodes,
                                                            _tubular_joint)
    #
    #
    if _link_joints:
        try:
            model.component.joints.update(_link_joints)
        except AttributeError:
            model.component.joints = _link_joints
    #
    print('--- End reading {:} file'.format(file_name))
    #
    #    
    #
    return model.component
#