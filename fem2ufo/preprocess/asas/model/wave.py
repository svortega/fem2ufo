# 
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import re
import os
import sys
import copy
import math
#
from itertools import islice

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.asas.operations.common as asas_common
import fem2ufo.preprocess.asas.operations.metocean as metocean
import fem2ufo.preprocess.asas.operations.load as load_operation

#
#
def set_cdcm(memb, _cdcm):
    """
    """
    try:
        memb.properties.cdcm = _cdcm
    except AttributeError:
        memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
        memb.properties.cdcm =_cdcm
#
def get_cdcm_item(model, memb_items, _cdcm,
                  zmin=False, zmax=False):
    """
    """
    for membNo in memb_items:
        memb_name = str(membNo)
        try:
            memb = model.component.concepts[memb_name]
            if memb.type != 'beam':
                try:
                    memb_items.remove(memb_name)
                except ValueError:
                    pass
                continue                        
        except KeyError:
            print('   *** warning element {:} not found'
                  .format(memb_name))
            try:
                memb_items.remove(memb_name)
            except ValueError:
                pass
            continue
        #
        if zmin:
            if get_zone(memb, zmin, zmax):
                set_cdcm(memb, _cdcm)
        else:
            set_cdcm(memb, _cdcm)
#
def get_member_mass(geometry, memb_no, mass, _factor):
    """
    """
    try:
        memb_name = str(memb_no)
        member = geometry.concepts[memb_name]
    except KeyError:
        print('    *** warning element {:} not found'
              .format(memb_no))
        return
    #
    _lenght = member.flexible_length
    _point_mass = mass * _lenght / 2.0
    #
    member.node[0].mass.append(_point_mass)
    member.node[-1].mass.append(_point_mass)
    #print('here')
#
def get_flooded_item(_concepts, _flooded_items, model):
    """
    """
    for _memb_no in _concepts:
        try:
            memb_name = str(_memb_no)
            _memb = model.component.concepts[memb_name]
            if not 'beam' in _memb.type:
                continue
            _memb.properties.flooded = True
            _flooded_items.append(memb_name)
        except KeyError:
            print('    *** Warning, element {:} not found'.format(memb_name))
    
#
#
def get_zone(memb, zmin, zmax):
    """
    """
    _flag = False
    _node0 = memb.node[0].z
    if _node0 > zmin and _node0 < zmax:
        _node1 = memb.node[1].z
        if _node1 > zmin and _node1 < zmax:
            _flag = True
    #
    return _flag
#
def get_drag(_flag, _subject_cd, _subject_id, _flag_cdcm, model,
             _coeff, _items, _zone):
    """
    """
    if _flag == 'all':
        _subject_id = 'all'
        if _zone :
            _subject_id = _subject_id + '_' + _zone
        #
        try:
            _subject_cd[_subject_id]
        except KeyError:
            _no_wind = len(_subject_cd)
            _no_wind += 1
            _subject_cd[_subject_id] = f2u.Metocean.Parameters(_subject_id, _no_wind)
            #_subject_cd[_subject_id] = f2u.Metocean.DragMassCoefficient(_subject_id, _no_wind)
            _subject_cd[_subject_id].type = 'specified'
        #
        _subject_cd[_subject_id].items = ['all']
    #
    else:
        try:
            _subject_cd[_subject_id]
        except KeyError:
            _no_wind = len(_subject_cd)
            _no_wind += 1
            _subject_cd[_subject_id] = f2u.Metocean.Parameters(_subject_id, _no_wind)
            #_subject_cd[_subject_id] = f2u.Metocean.DragMassCoefficient(_subject_id, _no_wind)
            _subject_cd[_subject_id].type = 'specified'
        #
        if _flag == 'element':
            _subject_cd[_subject_id].items.extend(_items)
            
        # FIXME : need to fix group as dic
        elif _flag == 'group':
            for _grp in _items:
                try:
                    model.sets[str(_grp)]
                    _subject_cd[_subject_id].sets.update({str(_grp): _grp})
                except KeyError:
                    print('   *** warinig group {:} not found'.format(_grp))
        #
        elif _flag == 'property':
            for _prop in _items:
                try:
                    _subject_cd[_subject_id].items.extend(model.sections[str(_prop)].elements)
                except KeyError:
                    print('   *** warinig property {:} not found'.format(_prop))
        #
        else:
            print('  *** error item not identified : {:}'.format(_flag))
    #
    _subject_cd[_subject_id].coefficient[_flag_cdcm] = _coeff
    if _zone :
        #print(' cdcm by zone')
        _subject_cd[_subject_id].zone = _zone
    #
#
def get_mgrow(_flag, _subject, _subject_id, model, 
              _items, _zone, _thick, _density):
    """
    """
    if _flag == 'all':
        _subject_id = 'all'
        if _zone :
            _subject_id = _subject_id + '_' + _zone.name
        #
        try:
            _subject[_subject_id].name
        except KeyError:
            _subject_no = len(_subject)
            _subject_no += 1
            _subject[_subject_id] = f2u.Metocean.MarineGrowth(_subject_id, _subject_no)
            _roughness = 0
            _subject[_subject_id].profile.append([_zone.zmin, _thick, _roughness])
            _subject[_subject_id].profile.append([_zone.zmax, _thick, _roughness])
            _subject[_subject_id].density = _density             
        #
        _subject[_subject_id].items = ['all']
        _subject[_subject_id].type = 'profile'
    #
    else:
        try:
            _subject[_subject_id].name
        except KeyError:
            _subject_no = len(_subject)
            _subject_no += 1
            _subject[_subject_id] = f2u.Metocean.MarineGrowth(_subject_id, _subject_no)
            _roughness = 0
            _subject[_subject_id].constant(_thick, _roughness, _density)
        #
        if _flag == 'element':
            _subject[_subject_id].items.extend(_items)
        #
        elif _flag == 'group':
            for _grp in _items:
                try:
                    model.sets[str(_grp)]
                    _subject[_subject_id].sets.update({str(_grp): _grp})
                except KeyError:
                    print('   *** warinig group {:} not found'.format(_grp))
        #
        elif _flag == 'property':
            for _prop in _items:
                try:
                    _subject[_subject_id].items.extend(model.sections[str(_prop)].elements)
                except KeyError:
                    print('   *** warinig property {:} not found'.format(_prop))
        #
        _subject[_subject_id].type = 'specified'
        #_subject[_subject_id].zone = _zone
    #return _subject
    #
#
#
def read_metocean(metocean_file, model, combination_name,
                  units):
    """
    """
    #
    #geometry = model.component
    #
    #
    if not units:
        print('  **  error global units not provided in ASAS model')
        print('      process terminated')
        sys.exit()

    # read wave file
    links = {}
    read_wave(metocean_file, model, 
              combination_name,
              units, links)
    
    if links:
        #links = list(set(links))
        #_addlink = []
        for _file in links.keys():
            read_wave(_file, model, 
                      combination_name,
                      units, links)
            #_addlink.extend(_link)
            #print('here')
    #
    metomodel = model.metocean
    # add elim group members to model's sets
    try:
        metomodel.non_hydro
        if metomodel:
            for _no_hydro in metomodel.non_hydro.values():
                metomodel.non_hydro[1].items.extend(_no_hydro.items)
                #print('ok')
        else:
            _new_set_name = '_elim'
            process.geometry.add_new_groups(model.sets,
                                       metomodel.non_hydro,
                                       _new_set_name)
    except AttributeError:
        pass
    #
    # add cdcm group members to model's sets
    try:
        #if _meto.cdcm:
        _new_set_name = '_cdcm'
        process.geometry.add_new_groups(model.sets,
                                   metomodel.cdcm,
                                   _new_set_name)
    except AttributeError:
        pass        
    #
    # add flooded member to model's sets
    try:
        metomodel.flooded
        if metomodel:
            for _flod in metomodel.flooded.values():
                metomodel.flooded[1].items.extend(_flod.items)
                #metomodel.flooded[1].sets.update(_meto.flooded.sets)
            #print('ok')
        else:            
            _new_set_name = '_flood'
            process.geometry.add_new_groups(model.sets,
                                       metomodel.flooded,
                                       _new_set_name)
    except AttributeError:
        pass
    #
    #return metomodel
    #
#
#
def read_wave(file_name, model, 
              combination_name, global_units,
              links, _load_case=1):
    """
    """
    print('')
    print('----------------- REDMETOCEAN Module ---------------')

    asas_file = process.common.check_input_file(file_name)
    if not asas_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit(1))

    #
    print('')
    print('--- Reading metocean data from {:} input file'.format(file_name))
    print('')
    #
    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    geometry = model.component

    # Units [length, mass, time, temperature, force, pressure/stress]
    global_length = global_units[0]
    global_force = global_units[4]
    _length_factor, _force_factor = process.units.convert_units_SI(global_length, 
                                                                   global_force)
    _mass_factor = 1
    
    # here factors are getting for unit conversion purposes
    # units_output = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
    # factors, _grav = process.get_factors_and_gravity(_units_input,
    #                                                 _units_output)    

    _elevation = {}
    _no_elev = 0

    _non_hydro = {}
    _nohydro_no = 0

    _wind_cd = {}
    _no_wind = 0

    _flooded = {}
    _no_floo = 0
    global_flooded = False

    _buoy = {}
    _no_buoy = 0

    _wave = {}
    _wave_ini = {}
    isea = 0

    _k_factor = 1.0
    _kin = {}
    _phase_angle = 1.0
    _phase = {}
    _block_factor = 1.0
    _blockage = {}
    _current = {}
    _mgrowth = {}
    _mgrowth_no = 0
    #_cdcm = {}
    _wind = {}
    _seastate = {}
    _design = {}

    _grav = 9.81  #
    _gravity = {}

    _zone_no = 0
    _zone = {}
    load_name = {}
    _wind_items = []
    #
    #link = []
    #
    dat_temp = open('datTemp.dat','w+')
    #
    print('    * First pass')
    #
    _sea_number = 1
    with open(asas_file) as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            # find key words and clean lines from comments
            if re.search(r"\+", line):
                line = re.sub(r"\+", "", line)
            #
            line = line.strip()
            if not line:
                continue
            #
            # UNITS card
            if 'UNITS' in line:
                line, _length_factor, _force_factor= asas_common.unit_factor(line, 
                                                                        global_length,
                                                                        global_force)
                continue
            #
            if 'EXEC' in line:
                _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
                _length_factor, _force_factor = process.units.convert_units_SI(global_length,
                                                                   global_force)
                #
                l_name = asas_common.get_comment(line)
                l_name = load_operation.load_title(l_name)
                load_name[str(_sea_number)] = l_name
                dat_temp.write("\n")
                dat_temp.write('EXEC')
                dat_temp.write("\n****")
                _sea_number += 1
                continue
            #
            elif re.search(r"\*", line):
                line = asas_common.remove_comment(line)
            #
            if 'RESE' in line:
                _case = line.split()
                #_cases_on = [0, 2]
                if int(_case[1]) in [0, 2]:
                    dat_temp.write("\n")
                    dat_temp.write('RESE {:}'.format(_sea_number))
                continue
                #_phase[_sea_number] = _phase_angle
                #_kin[_sea_number] = _k_factor
                #_blockage[_sea_number] = _block_factor
            #
            elif 'LOAD' in line:
                _case = line.split()
                data = list(islice(fin, 2))
                line, _keyWord = metocean.master_word(data[0], _keyWord)
                l_name, _no = asas_common.word_pop(line)
                l_name = load_operation.load_title(l_name)
                load_name[_no] = l_name
                _sea_number = int(_no)
                #
                dat_temp.write("\n")
                dat_temp.write('RESE {:}'.format(_sea_number))
                #
                continue
            #
            elif 'GRAV' in line:
                _line = str(line).split()
                _grav = float(_line[2]) * _length_factor
            #
            elif 'ELEV' in line:
                _line = str(line).split()
                _lev_name = str(float(_line[1])) + '_' + str(float(_line[2]))
                
                try:
                    _elevation[_lev_name]
                except KeyError:
                    _no_elev += 1
                    _elevation[_lev_name] = f2u.Metocean.Condition(_lev_name, _no_elev)
                    _elevation[_lev_name].surface = float(_line[1]) * _length_factor
                    _elevation[_lev_name].mudline = float(_line[2]) * _length_factor
                    _elevation[_lev_name].water_density = float(_line[3]) * _mass_factor / _length_factor**3
    
                try:
                    _elevation[_lev_name].air_density = float(_line[4])
                except IndexError:
                    _elevation[_lev_name].air_density = 1.23  # kg/m^3
                #
                dat_temp.write("\n")
                dat_temp.write('ELEV {:}'.format(_lev_name))
                continue
            #
            # The following won't need to be traslated
            #
            elif 'OUTP' in line:
                # The OUTP command controls the amount of printed output for each wave (EXEC)
                # ignored for conversion purposes
                continue
            #
            elif '@' in line:
                # ignored for conversion purposes i.e. hydro data constant
                #print('   *** Warning -->  @ file not read', line)
                _file = asas_common.split_line(line, '@', rigth=True)
                _file = asas_common.split_line(_file, '\\', rigth=True)
                #
                try:
                    links[_file].append(str(_sea_number))
                except KeyError:
                    links[_file] = [str(_sea_number)]
                #link.append(_file)
                continue
            #
            elif 'PHAS' in line:
                # specific not longer required because now the wave is
                # ran for all phases
                #continue
                dat_temp.write("\n")
                dat_temp.write('{:}'.format(line))                
                #
                #_line = str(line).split()
                #if float(_line[2]) == 0:
                #    _phase_angle = 360 / float(_line[0])
                #else:
                #    _phase_angle = float(_line[2])
                #
                #_phase[_no_elev] = _phase_angle
            #
            #
            #elif 'MAXM' in line:
                #
                # specific not longer required because wave maximum
                # is calculated for both BS & OTM
                #
                #continue
            #
            # write no read cards
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    #
    # Metocean Combinations
    #
    _level_flag = False
    dat_temp = open('datTemp2.dat','w+')
    print('    * Second pass')
    #
    with open('datTemp.dat') as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            #
            if re.search(r"\+", line):
                line = re.sub(r"\+", "", line)
            #
            line = line.strip()
            if not line:
                continue
            #
            #
            line2, _keyWord = metocean.master_word(line, _keyWord)
            #
            #
            if _keyWord[0] == 'reset':
                _sea_number = line2.strip()
                _level_flag = False
                _meto_name = combination_name + '_' + str(_sea_number)
                _sea_name = _sea_number # 'MET'+str(_sea_number).zfill(3)
                _seastate[_sea_name] = f2u.Metocean.Combination(_sea_name, int(_sea_number),)
                #
                dat_temp.write("\n")
                dat_temp.write('RESE {:}'.format(_sea_number))
                continue
            #
            if _keyWord[0] == 'elev':
                _level_flag = True
                _lev_name = line2.strip()
                dat_temp.write("\n")
                dat_temp.write('ELEV {:}'.format(_lev_name))
                #_elevation[_lev_name].wave.append(_sea_number)
                #print('-->')
            #
            elif _keyWord[0] == 'zone':
                line2, _name = asas_common.word_pop(line2)
                try:
                    _zone[_name]
                except KeyError:
                    _zone_no += 1
                    _zone[_name] = f2u.Metocean.Zone(_name, _zone_no)
    
                while line2:
                    line2, _keyWord = metocean.elev_word(line2, _keyWord, 1)
                    line2, _level = asas_common.word_pop(line2)
                    
                    if _keyWord[1] == 'zmin':
                        _zone[_name].zmin = float(_level) * _length_factor
                    
                    elif _keyWord[1] == 'zmax':
                        _zone[_name].zmax = float(_level) * _length_factor
                    
                    else:
                        print('   *** warning zone line not read : {:}'.
                              format(line2))
            #
            elif _keyWord[0] == 'kinematic':
                line, _factor = asas_common.word_pop(line2)
                _k_factor = float(_factor)
                _kin[_meto_name] = _k_factor
            #
            elif _keyWord[0] == 'spread':
                line2, _power = asas_common.word_pop(line2)
                _k_factor = ((float(_power) + 1.0) / (float(_power) + 2.0))**0.5
                _kin[_sea_number] = _k_factor
            #
            elif _keyWord[0] == 'cblockage':
                _line = str(line2).split()
                _block_factor = float(_line[0])
                _blockage[_meto_name] = _block_factor
            #
            elif _keyWord[0] == 'cd' or _keyWord[0] == 'cm':
                _flag_cdcm = 'cd'
                if _keyWord[0] == 'cm':
                    _flag_cdcm = 'cm'
    
                line2, _keyWord = metocean.grow_word(line2, _keyWord, 1)
    
                if _keyWord[1] == 'N/A':
                    line2, _cdmx = asas_common.word_pop(line2)
                    line2, _cdmy = asas_common.word_pop(line2)
                    line2, _cdmz = asas_common.word_pop(line2)
                    # local axis in asas converted
                    _name = _flag_cdcm +'_'+ str(_cdmz) + '_' + str(_cdmy)
                    _cdmx, _cdmy, _cdmz = float(_cdmx), float(_cdmy), float(_cdmz)
                    _coeff = [_cdmx, _cdmz, _cdmy]
    
                else:
                    line2, _table_name = asas_common.word_pop(line2)
                    print(' fix table in drag')
                
                _subject_cd = _wind_cd
                if _coeff[0] == _coeff[1] == _coeff[2] == 0:
                    _name = 'drag_'+ str(_name)
                    _subject_cd = _non_hydro
    
                line2, _keyWord = metocean.grow_word(line2, _keyWord, 2)
                
                _zone_flag = False
    
                if _keyWord[2] == 'zone':
                    line2, _zone_name = asas_common.word_pop(line2)
                    _zone_flag = _zone_name
    
                line2, _keyWord = asas_common.FE_word(line2, _keyWord, 3)
                
                # check if all
                if _keyWord[3] == 'all':
                    _items = []
                    get_drag(_keyWord[3], _subject_cd, _name, 
                             _flag_cdcm, model.component,
                             _coeff, _items, _zone_flag)
                    continue
                #
                while line2:
                    line2, _start = asas_common.word_pop(line2)
                    line2, _keyWord = metocean.number_word(line2, _keyWord, 4)
    
                    if _keyWord[4] == 'to':
                        line2, _end = asas_common.word_pop(line2)
                        _total = abs(int(_end) - int(_start)) + 1
                        _items = []
                        for x in range(_total):
                            _items.append(int(_start) + x)
                    else:
                        _items = [int(_start)]
                    #
                    get_drag(_keyWord[3], _subject_cd, _name, 
                             _flag_cdcm, model.component,
                             _coeff, _items, _zone_flag)
                    #print('here')
                #
            #
            elif _keyWord[0] == 'current' or _keyWord[0] == 'tide':
                _line = str(line2).split()
                if _keyWord[0] == 'current':
                    _z = float(_line[2]) * _length_factor
                    _v = float(_line[0]) * _length_factor
    
                    try:
                        _current[_meto_name].profile.append([_z, _v])
                    except KeyError:
                        _current[_meto_name] = f2u.Metocean.Current(_meto_name, int(_sea_number))
                        _current[_meto_name].profile.append([_z, _v])
                        #
                        _seastate[_sea_name].current = _current[_meto_name]
                        _seastate[_sea_name].current_direction = float(_line[1])
                else:
                    print('  fix tide-current data')
            #
            elif _keyWord[0] == 'curr_profile':
                """
                """
                _line = str(line2).split()
                _profile = int(_line[0])
                _dir = float(_line[4])
                _vs = float(_line[1]) * _length_factor
                _vb = float(_line[2]) * _length_factor
                #
                #_zaw = float(_line[3]) * _length_factor
                #_v = _vs
                #
                _z = _elevation[_lev_name]
                #
                _current[_meto_name] = f2u.Metocean.Current(_meto_name, int(_sea_number))
                _current[_meto_name].profile.append([_z.mudline, _vb])
                _current[_meto_name].profile.append([0, _vs])
                _current[_meto_name].profile.append([_z.surface, _vs])
                #
                _seastate[_sea_name].current = _current[_meto_name]
                _seastate[_sea_name].current_direction = _dir               
            #
            elif _keyWord[0] == 'design_load':
                _line = str(line2).split()
                if int(_line[0]) == 0:
                    _design[_meto_name] = 'MaxBaseShear'
                else:
                    _design[_meto_name] = 'MaxOMoment'
            #
            elif _keyWord[0] == 'flooded':
                line2, _keyWord = asas_common.FE_word(line2, _keyWord, 2)
    
                if _keyWord[2] == 'N/A':
                    line2, _elev = asas_common.word_pop(line2)
                    line2, _keyWord = asas_common.FE_word(line2, _keyWord, 2)
    
                if _keyWord[2] == 'all':
                    #_wave_ini[_wave_name].buoyancy = None
                    dat_temp.write("\n")
                    dat_temp.write('NOBO ALL {:}')
                    global_flooded = True
                    #continue
    
                else:
                    while line2:
                        line2, _start = asas_common.word_pop(line2)
                        line2, _keyWord = metocean.number_word(line2, _keyWord, 3)
    
                        if _keyWord[3] == 'to':
                            line2, _end = asas_common.word_pop(line2)
                            _total = abs(int(_end) - int(_start)) + 1
                            _items = []
                            for x in range(_total):
                                _items.append(int(_start) + x)
                        else:
                            _items = [int(_start)]
    
                        try:
                            _flooded[_keyWord[2]].extend(_items)
                            #_test = _flooded[_no_floo]
                        except KeyError:
                            _flooded[_keyWord[2]] = _items
                            #_no_floo += 1
                            #_flooded[_no_floo] = f2u.Metocean.Parameters(_no_floo, _no_floo)
                            #_flooded[_no_floo].type = 'flooded_members'
                            #_flooded[_no_floo].flooded = True
                        #
                        #if _keyWord[2] == 'element':
                        #    _flooded[_no_floo].items.extend(_items)
                        # FIXME : need to fix group as dic
                        #elif _keyWord[2] == 'group':
                        #    for _grp in _items:
                        #        try:
                        #            model.component.sets[str(_grp)]
                        #            _flooded[_no_floo].sets.update({str(_grp): _grp})
                        #        except KeyError:
                        #            print('   *** warinig group {:} not found'.format(_grp))
    
                        #elif _keyWord[2] == 'property':
                        #    for _prop in _items:
                        #        try:
                        #            _flooded[_no_floo].items.extend(model.component.sections[str(_prop)].elements)
                        #        except KeyError:
                        #            print('   *** warinig property {:} not found'.format(_prop))
                        #else:
                        #    print('  *** error item not identified : {:}'.format(_keyWord[3]))
            #
            elif _keyWord[0] == 'buoy':
                line2, _keyWord = asas_common.FE_word(line2, _keyWord, 1)
    
                if _keyWord[1] == 'N/A':
                    line2, _density = asas_common.word_pop(line2)
                    line2, _flood = asas_common.word_pop(line2)
                    _name = str(_density) + '_' + str(_flood)
    
                    line2, _keyWord = asas_common.FE_word(line2, _keyWord, 1)
                else:
                    _name = '1'
    
                if _keyWord[1] == 'all':
                    for memb in model.component.concepts.values():
                        try:
                            memb.properties.flooded = False
                        except AttributeError:
                            memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
                            memb.properties.flooded = False
                    
                    #print(' fix all in buoy')
                    #continue
                else:
                    while line2:
                        line2, _start = asas_common.word_pop(line2)
                        line2, _keyWord = metocean.number_word(line2, _keyWord, 2)
    
                        if _keyWord[2] == 'to':
                            line2, _end = asas_common.word_pop(line2)
                            _total = abs(int(_end) - int(_start)) + 1
                            _items = []
                            for x in range(_total):
                                _items.append(int(_start) + x)
                        else:
                            _items = [int(_start)]
    
                        try:
                            _test = _buoy[_name]
                        except KeyError:
                            _no_buoy += 1
                            _buoy[_name] = f2u.Metocean.Parameters(_no_buoy, _no_buoy)
                            _buoy[_name].type = 'buoyancy'
                        #
                        if _keyWord[1] == 'element':
                            _buoy[_name].items.extend(int(_items))
                        # FIXME : need to fix group as dic
                        elif _keyWord[1] == 'group':
                            for _grp in _items:
                                try:
                                    model.component.sets[str(_grp)]
                                    _buoy[_name].sets.update({str(_grp): _grp})
    
                                except KeyError:
                                    print('   *** warinig group {:} not found'.format(_grp))
    
                        elif _keyWord[1] == 'property':
                            # _properties_cd.extend(_items)
                            for _prop in _items:
                                try:
                                    _buoy[_name].items.extend(model.component.sections[str(_prop)].concepts)
    
                                except KeyError:
                                    print('   *** warinig property {:} not found'.format(_prop))
    
                        else:
                            print('  *** error item not identified : {:}'.format(_keyWord[3]))
                #
                # check if link file
                if links:
                    _seastate = model.analysis.case[combination_name].metocean_combination
                    for _sea_name2 in links[file_name]:
                        _seastate[_sea_name2].buoyancy = 'buOn'
                        #print('-->')
            #
            elif _keyWord[0] == 'phase':
                dat_temp.write("\n")
                dat_temp.write("PHAS {:}".format(load_operation.load_title(load_name[_sea_number])))                
            #
            elif _keyWord[0] == 'wave':
                line2, _keyWord = metocean.wave_word(line2, _keyWord, 1)
                _line = str(line2).split()
                #
                #_wave_name = load_operation.load_title(load_name[_sea_number])
                #_meto_name = combination_name + '_' + str(_sea_number)
                #
                if _keyWord[1] == 'N/A':
                    _wave[_meto_name] = f2u.Metocean.Wave(_meto_name, int(_sea_number))
                    #_theory = _line[0]
                    _wave[_meto_name].theory = 'stream'
                    _wave[_meto_name].order = int(_line[0])
                    _wave[_meto_name].height = float(_line[1]) * _length_factor
                    _wave[_meto_name].period = float(_line[2])
                    #
                    _seastate[_sea_name].wave = _wave[_meto_name]
                    _seastate[_sea_name].wave_direction = float(_line[3])
                    #
                    water_depth = abs(_elevation[_lev_name].mudline - _elevation[_lev_name].surface)
                    _wave[_meto_name].water_depth = water_depth                    
                    
                elif _keyWord[1] != 'new_wave' or _keyWord[1] != 'irregular':
                    _wave[_meto_name] = f2u.Metocean.Wave(_meto_name, int(_sea_number))
                    _wave[_meto_name].theory = _keyWord[1]
                    _wave[_meto_name].height = float(_line[0]) * _length_factor
                    _wave[_meto_name].period = float(_line[1])
                    #
                    _seastate[_sea_name].wave = _wave[_meto_name]
                    _seastate[_sea_name].wave_direction = float(_line[2])
                    #
                    water_depth = abs(_elevation[_lev_name].mudline - _elevation[_lev_name].surface)
                    _wave[_meto_name].water_depth = water_depth
                else:
                    print('  irregular wave needs to be fixed')  
                #
                #
                #
            #
            elif _keyWord[0] == 'wind':
                _wind[_meto_name] = f2u.Metocean.Wind(_meto_name, int(_sea_number))
                _line = str(line2).split()
                _wind[_meto_name].velocity = float(_line[0]) * _length_factor
                _wind[_meto_name].alignment = 'x axis'
                _wind[_meto_name].density = float(_line[2]) * _mass_factor / _length_factor ** 3
                _wind[_meto_name].height = 10
                _wind[_meto_name].period_ratio = 1
                _wind[_meto_name].formula = 'normal'
                #
                _seastate[_sea_name].wind = _wind[_meto_name]
                #_seastate[_sea_name].wind_direction = 'x axis'
                _seastate[_sea_name].wind_direction = float(_line[1])
                #
            #
            elif _keyWord[0] == 'extra_mass':
                line2, item = asas_common.word_pop(line2)
                _mass = float(item)/_length_factor
                if math.isclose(_mass, 0):
                    print('    *** warning extra mass is zero --> ignored')
                    continue
                # find
                _remainder = None
                while line2:
                    line2, _keyWord = asas_common.FE_word(line2, _keyWord, 2)
                    try:
                        line2, item = asas_common.word_pop(line2)
                    except IndexError:
                        continue
                    _remainder = asas_common.assign_property(item, _remainder, _keyWord, 
                                                             geometry, get_member_mass, _mass)
                
            #
            elif _keyWord[0] == 'execute':
                dat_temp.write("\n")
                dat_temp.write('EXEC')
                dat_temp.write("\n****")
                continue
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    #
    dat_temp.close()
    os.remove('datTemp.dat')
    #
    print('    * Third pass')
    dat_temp = open('CheckMe_' + file_name ,'w+')
    #
    #
    with open('datTemp2.dat') as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*", line):
                continue
            #
            if re.search(r"\+", line):
                line = re.sub(r"\+", "", line)
            #
            line = line.strip()
            if not line:
                continue
            #
            line2, _keyWord = metocean.master_word(line, _keyWord)
            #
            if _keyWord[0] == 'reset':
                _sea_number = line2.strip()
                _level_flag = False
                _sea_name = _sea_number # 'MET'+str(_sea_number).zfill(3)
                #_seastate[_sea_number] = f2u.MetoceanCombination( "N/A", int(_sea_number),)
            #
            elif _keyWord[0] == 'elev':
                _level_flag = True
                _lev_name = line2.strip()
                _seastate[_sea_name].buoyancy = "buOn"
                _meto_name = combination_name + '_' + str(_sea_number)
                try:
                    _wave[_meto_name]
                except KeyError:
                    #_wave_name = load_operation.load_title(load_name[_sea_number])
                    _wave[_meto_name] = f2u.Metocean.Wave(_meto_name, int(_sea_number))
                    #_wave[_meto_name].theory = "calm_sea" # next update to be agreed
                    _wave[_meto_name].theory = "Airy"
                    _wave[_meto_name].height = 0.01
                    _wave[_meto_name].period = 6.0
                    #_wave[_meto_name].name =  load_operation.load_title(load_name[_sea_number])
                    #
                    water_depth = abs(_elevation[_lev_name].mudline - _elevation[_lev_name].surface)
                    _wave[_meto_name].water_depth = water_depth
                    _seastate[_sea_name].wave = _wave[_meto_name]
                    _seastate[_sea_name].wave_direction = 0
                    _seastate[_sea_name].design_load = "MaxBaseShear" 
                    #_seastate[_sea_name].name = load_operation.load_title(load_name[_sea_number])
            #
            elif _keyWord[0] == 'grow':
                _hydro = True
                _zone_flag = False
                _flag_cdcm = False
                
                line2, _thick = asas_common.word_pop(line2)
                _thick = float(_thick)
                line2, _density = asas_common.word_pop(line2)
                _density = float(_density)
                _mgrow_id = str(_thick) + '_' + str(_density)
                
                _subject = _mgrowth
                if _thick == _density == 0.0:
                    _hydro = False
                    _mgrow_id = 'mgrow_'+str(_mgrow_id)
                #
                line2, _keyWord = metocean.grow_word(line2, _keyWord, 1)
                #
                if _keyWord[1] == 'zone':
                    line2, _name_zone = asas_common.word_pop(line2)
                    _zone_flag = _zone[_name_zone]
                else:
                    _zone_no = len(_zone) + 1
                    _name_zone = 'mgrow_' + str(_zone_no)
                    _zone[_name_zone] = f2u.Metocean.Zone(_name_zone, _zone_no)
                    line2, _upper = asas_common.word_pop(line2)
                    line2, _lower = asas_common.word_pop(line2)
                    _zone[_name_zone].zmin = float(_lower) * _length_factor
                    _zone[_name_zone].zmax = float(_upper) * _length_factor
                    _zone_flag = _zone[_name_zone]
                #
                line2, _keyWord = asas_common.FE_word(line2, _keyWord, 2)
                #
                if _keyWord[2] == 'N/A':
                    line2, _cd = asas_common.word_pop(line2)
                    line2, _cm = asas_common.word_pop(line2)
                    _name = str(_cd) + '_' + str(_cm)
                    _cd = float(_cd)
                    _cm = float(_cm)
                    line2, _keyWord = asas_common.FE_word(line2, _keyWord, 2)
                    # check if members omited from hydro calcs
                    if _cd == _cm == 0.0:
                        _hydro = False
                        _subject = _non_hydro
                    else:
                        print('fix here cd & cm')
                        _subject_cd = _wind_cd
                        _flag_cdcm = True
                        #
                #
                elif _keyWord[2] == 'all':
                    _items = []
                    get_mgrow(_keyWord[2], _subject, _mgrow_id,
                              model.component, _items,
                              _zone_flag, _thick, _density)                 
                    continue
                #
                # non hydro
                while line2:
                    line2, _start = asas_common.word_pop(line2)
                    line2, _keyWord = metocean.number_word(line2, _keyWord, 3)
    
                    if _keyWord[3] == 'to':
                        line2, _end = asas_common.word_pop(line2)
                        _total = abs(int(_end) - int(_start)) + 1
                        _items = []
                        for x in range(_total):
                            _items.append(int(_start) + x)
                    else:
                        _items = [int(_start)]
                #
                get_mgrow(_keyWord[2], _subject, _mgrow_id,
                          model.component, _items,
                          _zone_flag, _thick, _density)                
                #
                if _flag_cdcm:
                    _name_cd = 'cd_'+ str(_cd) + '_' + str(_cd)
                    _coeff_cd = [0, _cd, _cd]
                    get_drag(_keyWord[2], _subject_cd, _name_cd, 'cd',
                             model.component,
                             _coeff_cd, _items, _zone_flag)
                    #
                    _name_cm = 'cm_'+ str(_cm) + '_' + str(_cm)
                    _coeff_cm = [0, _cm, _cm]
                    get_drag(_keyWord[2], _subject_cd, _name_cm, 'cm',
                             model.component,
                             _coeff_cm, _items, _zone_flag)
            # FIXME : need to check how this card works
            elif _keyWord[0] == 'elim_wind':
                1/0
                line2, _keyWord = asas_common.FE_word(line2, _keyWord, 1)
                # if not level then this was used for wind only
                if _level_flag:
                    while line:
                        line, _start = asas_common.word_pop(line)
                        line, _keyWord = metocean.number_word(line, _keyWord, 2)
    
                        if _keyWord[2] == 'to':
                            line, _end = asas_common.word_pop(line)
                            _total = abs(int(_end) - int(_start)) + 1
                            _items = []
                            for x in range(_total):
                                _items.append(str(int(_start) + x))
                        else:
                            _items = [str(int(_start))]
    
                        try:
                            _test = _wind_cd[_name].type
                        except KeyError:
                            _no_wind += 1
                            _wind_cd[_name] = f2u.Metocean.Parameters(_no_wind, _no_wind)
                            _wind_cd[_name].type = 'wind'
                            _wind_cd[_name].coefficient.append([str(_flag_cdcm) + 'y', _cdmy])
                            _wind_cd[_name].coefficient.append([str(_flag_cdcm) + 'z', _cdmz])
    
                        if _keyWord[1] == 'element':
                            _wind_cd[_name].items.extend(int(_items))
                        # FIXME : need to fix group as dic
                        elif _keyWord[1] == 'group':
                            for _grp in _items:
                                try:
                                    model.component.sets[str(_grp)]
                                    _wind_cd[_name].sets.update({str(_grp): _grp})
                                except KeyError:
                                    print('   *** warinig group {:} not found'.format(_grp))
    
                        elif _keyWord[1] == 'property':
                            # _properties_cd.extend(_items)
                            for _prop in _items:
                                try:
                                    _wind_cd[_name].items.extend(model.component.sections[str(_prop)].concepts)
                                except KeyError:
                                    print('   *** warinig property {:} not found'.format(_prop))
    
                        else:
                            print('  *** error item not identified : {:}'.format(_keyWord[1]))
                #
                else:
                    if _keyWord[1] == 'all':
                        _meto_name = combination_name + '_' + str(_sea_number)
                        _wave[_meto_name].height = 0.01
                        continue
            #
            elif _keyWord[0] == 'elim_wave':
                line2, _keyWord = asas_common.FE_word(line2, _keyWord, 1)
                if _keyWord[1] == 'all':
                    try:
                        if _seastate[_sea_name].wave:
                            _meto_name = _seastate[_sea_name].wave.name
                            #_wave[_meto_name].theory = "calm_sea"
                        else:
                            _meto_name = combination_name + '_' + str(_seastate[_sea_name].number)
                            _wave[_meto_name] = f2u.Metocean.Wave(_meto_name, _seastate[_sea_name].number)                            
                            #print('-->')
                        #
                        _wave[_meto_name].theory = "Airy"
                        _wave[_meto_name].height = 0.01
                        _wave[_meto_name].period = 6.0
                        _seastate[_sea_name].design_load = "MaxBaseShear"
                    except KeyError:
                        print('    *** error seastate {:} not found'.format(_sea_name))
                        1/0
                else:
                    line, _keyWord = metocean.number_word(line, _keyWord, 2)
                    1/0
            #
            elif _keyWord[0] == 'elim_selfweight':
                print('    *** error eliminate selfweight card needs fix')
                continue
            #
            elif _keyWord[0] == 'phase':
                _name = line2.strip()
                _name = 'MET' + str(_sea_number).zfill(3) + '_' +_name
                #_wave[_meto_name].name = _wave_name
                _meto_name = combination_name + '_' + str(_sea_number)
                #
                try:
                    _seastate[_sea_name].name = _name
                    
                    try:
                        _seastate[_sea_name].wave_kinematics = _kin[_meto_name]
                    except KeyError:
                        pass
                    #
                    try:
                        _seastate[_sea_name].current_blockage = _blockage[_meto_name]
                    except KeyError:
                        pass
                    #
                    try:
                        #_seastate[_sea_name].current = _current[_meto_name]
                        _seastate[_sea_name].current_stretching = True
                        
                    except KeyError:
                        pass                    
                    #
                    try:
                        _wind[_meto_name].name = load_name[_sea_number]
                    except KeyError:
                        pass
                    #
                    try:
                        _seastate[_sea_name].design_load = _design[_meto_name]
                    except KeyError:
                        pass
                    #
                    #
                    #_elevation[_lev_name].wave.append(_sea_number)
                except KeyError:
                    print('   ***error seastate {:} not identified'.format(_sea_name))
                    dat_temp.write('RESE {:}'.format(_sea_number))
                    dat_temp.write("\n")
                    dat_temp.write(line)
                #
                #
                #else:
                #    print(line)
            #
            elif _keyWord[0] == 'flooded':
                line2, _keyWord = asas_common.FE_word(line2, _keyWord, 2)
                if _keyWord[2] == 'all':
                    _seastate[_sea_name].buoyancy = "buOff"
            #
            elif _keyWord[0] == 'end':
                _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
                _length_factor, _force_factor = process.units.convert_units_SI(global_length, 
                                                                               global_force)                
                continue
            #
            elif _keyWord[0] == 'execute':
                continue
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
            #               
    #
    dat_temp.close()
    os.remove('datTemp2.dat')
    #
    #
    print('--- End Reading {:} file'.format(file_name))
    #
    #
    if model.metocean:
        _metocean = model.metocean
    else:      
        model.set_metocean('metocean')
        _metocean = model.metocean
    #
    #
    if not model.analysis:
        model.set_analysis(combination_name)
    #
    #
    _lev = {}
    if _elevation:
        min_water_depth = min([ abs(_elev.mudline - _elev.surface) 
                                for _elev in _elevation.values()])
        water_depth = [min_water_depth]
        #
        for _lev_name, _elev in  _elevation.items():
            if min_water_depth == abs(_elev.mudline - _elev.surface):
                _lev = _elev
                _lev.number = 1 # i
                _name_meto = asas_file.split('.')
                _lev.name = _name_meto[0]
            else:
                water_depth.append(abs(_elev.mudline - _elev.surface))
        #
        _lev.water_depth = water_depth
        #
    #
    if _lev:
        try:
            model.analysis.case[combination_name].condition = _lev
        except KeyError:
            model.analysis.case[combination_name] = f2u.Analysis.Case(combination_name)
            model.analysis.case[combination_name].condition = _lev
    #    
    #
    if _seastate:
        for key, _sea in _seastate.items():
            if _sea.wave:
                continue
            # add calm sea dummy wave
            _meto_name = combination_name + '_' + str(_sea.number)
            _wave[_meto_name] = f2u.Metocean.Wave(_meto_name, _sea.number)
            #_wave[_meto_name].theory = "calm_sea"
            _wave[_meto_name].theory = "Airy"
            _wave[_meto_name].height = 0.01
            _wave[_meto_name].period = 6.0            
            #
            _sea.wave = _wave[_meto_name]
            _sea.wave.water_depth = min_water_depth
            _sea.wave_direction = 0
            _sea.design_load = "MaxBaseShear" 
        #
        try:
            model.analysis.case[combination_name].metocean_combination.update(_seastate)
        except KeyError:
            model.analysis.case[combination_name] = f2u.Analysis.Case(combination_name)
            model.analysis.case[combination_name].metocean_combination = _seastate
    #
    #
    if _current:
        # normalize velocity profile
        for _curr in _current.values():
            _vel = [_profile[1] for _profile in _curr.profile]
            _curr.velocity = max(_vel)
            _curr.profile = [[_profile[0], _profile[1]/_curr.velocity] 
                             for _profile in _curr.profile]
            #
            #_curr.profile.insert(0, [_curr.profile[0][0] * 100, 1.0])
            #print('hh')        
        
        
        #for _curr in _current.values():
        #    #increase current profile above water
        #    _curr.profile[0][0] = _curr.profile[0][0] * 100

        try:
            _metocean.current.update(_current)

        except AttributeError:
            _metocean.current = _current
    #
    #
    if _wave:
        try:
            _metocean.wave.update(_wave)
        except AttributeError:
            _metocean.wave = _wave
    #
    # 
    if _wind:
        try:
            _metocean.wind.update(_wind)
        except AttributeError:
            _metocean.wind = _wind

        if _wind_items:
            _metocean.wind.items.extend(_wind_items)
    #
    # 
    #
    # ---------------------------
    #
    memb_beam_items = [memb for memb in model.component.concepts.keys()]
    #memb_beam_items = [memb.number for memb in model.component.concepts.values()]
    #
    MG = {}
    if _mgrowth:
        i = 0
        for _name_mgw, _mgw in _mgrowth.items():
            i += 1
            MG[i] = _mgw
            MG[i].number = i
            MG[i].name = i
            
            #if _mgw.zone:
            #zmax = _zone[_mgw.zone].zmax
            #zmin = _zone[_mgw.zone].zmax
            #MG[i].profile[0][0] = _zone[_mgw.zone].zmax
            #MG[i].profile[1][0] = _zone[_mgw.zone].zmin
            #print('ok')
            
            if 'all' in _name_mgw:
                for membNo in memb_beam_items:
                    memb_name = str(membNo)
                    try:
                        memb = model.component.concepts[memb_name]
                        if memb.type != 'beam':
                            try:
                                memb_beam_items.remove(memb_name)
                            except ValueError:
                                pass
                            continue
                    except KeyError:
                        print('   *** warning element {:} not found'
                              .format(memb_name))
                        try:
                            memb_beam_items.remove(memb_name)
                        except ValueError:
                            pass
                        continue
                    #
                    try:
                        memb.properties.marine_growth = MG[i]
                    except AttributeError:
                        memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
                        memb.properties.marine_growth = MG[i]
                #
                MG[i].items.extend(memb_beam_items)
                continue
            #
            for _groupNo in _mgw.sets:
                #MG[i].items.extend(model.component.sets[_groupNo].items)
                #print('ok')
                for membNo in model.component.sets[_groupNo].items:
                    memb_name = str(membNo)
                    try:
                        memb = model.component.concepts[memb_name]
                    except KeyError:
                        print('   *** warning element {:} not found'
                              .format(memb_name))
                        #memb_items.remove(memb_name)
                        continue                        
                    
                    if get_zone(memb, zmin, zmax):
                        try:
                            memb.properties.marine_growth = MG[i]
                        except AttributeError:
                            memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
                            memb.properties.marine_growth = MG[i]                
        #
    #
    if MG :
        try:
            _metocean.marine_growth.update(MG)
        except AttributeError:
            _metocean.marine_growth = MG
    #    
    #
    _elim_sets = []
    _elim_items = []
    #
    #
    if _non_hydro:
        _elim = {}
        _nohydro_no = 0
        for _hydro_item in _non_hydro.values():
            try:
                _elim[_nohydro_no]
            except KeyError:
                _nohydro_no += 1
                _elim[_nohydro_no] = f2u.Metocean.Parameters(_nohydro_no, _nohydro_no)
                _elim[_nohydro_no].type = 'non_hydrodynamic'
                _elim[_nohydro_no].sets['ELIM'] = []
            #
            if _hydro_item.items:
                _elim[_nohydro_no].items.extend(_hydro_item.items)
            
            if _hydro_item.sets:
                _elim[_nohydro_no].sets['ELIM'].extend(_hydro_item.sets)
        #
        _elim_sets = list(set(_elim[_nohydro_no].sets['ELIM']))
        _elim[_nohydro_no].sets['ELIM'] = _elim_sets
        #
        _elim_items = list(set(_elim[_nohydro_no].items))
        _elim[_nohydro_no].items = _elim_items
        #
        try:
            _metocean.non_hydro.update(_elim)
        except AttributeError:
            _metocean.non_hydro = _elim    
    #
    # ---------------------------
    #
    _cdcm = {}
    if _wind_cd:
        # select cd/cm by set/element
        _cd_set = {}
        _cd_item = {}
        _cm_set = {}
        _cm_item = {}
        for _drag in _wind_cd.values():
            if 'cd' in  _drag.name:
                if _drag.items:
                    _cd_item[_drag.name] = set(_drag.items)
                if _drag.sets:
                    _cd_set[_drag.name] = set(_drag.sets)
            elif 'cm' in  _drag.name:
                if _drag.items:
                    _cm_item[_drag.name] = set(_drag.items)
                if _drag.sets:
                    _cm_set[_drag.name] = set(_drag.sets)
            #
        #
        # find cd/cm match elements
        _deletecm = []
        for _namecd, _cd in _cd_item.items():
            for _namecm, _cm in _cm_item.items():
                _benn = _cd - _cm
                if not _benn:
                    _wind_cd[_namecd].coefficient.update(_wind_cd[_namecm].coefficient)
                    _deletecm.append(_namecm)
                    break
        #
        # find cd/cm match sets
        for _namecd, _cd in _cd_set.items():
            for _namecm, _cm in _cm_set.items():
                _benn = _cd - _cm
                if not _benn:
                    _cd1 = _wind_cd[_namecd]
                    _cd2 = _wind_cd[_namecm]
                    
                    if _cd1.zone == _cd2.zone:
                        _wind_cd[_namecd].coefficient.update(_wind_cd[_namecm].coefficient)
                        _deletecm.append(_namecm)
                        break    
                        
                    
                    elif not _cd1.zone and  not _cd2.zone:
                        _wind_cd[_namecd].coefficient.update(_wind_cd[_namecm].coefficient)                
                        _deletecm.append(_namecm)
                        break
        #
        # delete cd 
        for _namecm in _deletecm:
            try:
                del _wind_cd[_namecm]
            except KeyError:
                continue
        #
        #
        _del_wind = []
        #memb_items = [memb.number for memb in model.concepts.values()]
        #
        for _groups in _elim_sets:
            try:
                _group = model.component.sets[_groups]
                _elim_items.extend(_group.items)
            except KeyError:
                continue
        #
        memb_items = list(set(memb_beam_items) - set(_elim_items))
        #
        i = 0
        for _name_drag, _drag in _wind_cd.items():
            if 'all' in _name_drag:
                if _drag.zone:
                    continue
                #
                i += 1
                _cdcm[i] = f2u.Metocean.DragMassCoefficient(i, i)
                _cdcm_items = _drag.coefficient['cd']
                _cdcm_items.extend(_drag.coefficient['cm'])
                _cdcm[i].set_cdcm(_cdcm_items)
                
                #_cdcm[i] = _drag
                #_cdcm[i].number = i
                #_cdcm[i].name = i
                #
                get_cdcm_item(model, memb_items, _cdcm[i])
                _del_wind.append(_name_drag)
                #print('ok no zone')
        #
        for _name_drag, _drag in _wind_cd.items():
            if 'all' in _name_drag:
                if _drag.zone:
                    try:
                        zmin = _zone[_drag.zone].zmin
                        zmax = _zone[_drag.zone].zmax
                    except KeyError:
                        print('   *** warning zone {:} not found'
                              .format(_drag.zone))
                        continue
                    #
                    i += 1
                    _cdcm[i] = f2u.Metocean.DragMassCoefficient(i, i)
                    try:
                        _cdcm_items = _drag.coefficient['cd']
                    except KeyError:
                        print(' fix cd')
                        _cdcm_items = [0,0,0]
                    
                    try:
                        _cdcm_items.extend(_drag.coefficient['cm'])
                    except KeyError:
                        print(' fix cm')
                        _cdcm_items.extend([0,0,0])
                    
                    _cdcm[i].set_cdcm(_cdcm_items)
                    #
                    #_cdcm[i] = _drag
                    #_cdcm[i].number = i
                    #_cdcm[i].name = i
                    #
                    get_cdcm_item(model, memb_items, _cdcm[i], zmin, zmax)
                    _del_wind.append(_name_drag)
        #
        for _name_drag in _del_wind:
            del _wind_cd[_name_drag]
        #
        # organize new set cdcm
        for _drag in _wind_cd.values():
            i += 1
            #
            _cdcm[i] = f2u.Metocean.DragMassCoefficient(i, i)
            try:
                _cdcm_items = _drag.coefficient['cd']
            except KeyError:
                print(' fix cd')
                _cdcm_items = [0,0,0]
            #
            try:
                _cdcm_items.extend(_drag.coefficient['cm'])
            except KeyError:
                print(' fix wind cm')
                _cdcm_items.extend([0,0,0])
            
            _cdcm[i].set_cdcm(_cdcm_items)            
            #
            #_cdcm[i] = _drag
            #_cdcm[i].number = i
            #_cdcm[i].name = i
            #
            if _drag.zone:
                try:
                    zmin = _zone[_drag.zone].zmin
                    zmax = _zone[_drag.zone].zmax
                except KeyError:
                    print('   *** warning zone {:} not found'
                          .format(_drag.zone))
                    continue
                
                # update members
                for membNo in _drag.items:
                    memb_name = str(membNo)
                    if get_zone(memb, zmin, zmax):
                        memb = model.component.concepts[memb_name]
                        try:
                            memb.properties.cdcm = _cdcm[i]
                        except AttributeError:
                            memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
                            memb.properties.cdcm =_cdcm[i]
                # update sets
                for _groupNo in _drag.sets:
                    try:
                        _group = model.component.sets[_groupNo]
                        for membNo in _group.items:
                            memb_name = str(membNo)
                            memb = model.component.concepts[memb_name]
                            if get_zone(memb, zmin, zmax):
                                try:
                                    memb.properties.cdcm.append(_cdcm[i])
                                except AttributeError:
                                    memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
                                    memb.properties.cdcm = _cdcm[i]
                    except KeyError:
                        continue
            #
            else:
                # update members
                for membNo in _drag.items:
                    memb_name = str(membNo)
                    memb = model.component.concepts[memb_name]
                    try:
                        memb.properties.cdcm = _cdcm[i]
                    except AttributeError:
                        memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
                        memb.properties.cdcm =_cdcm[i]
                # update sets
                for _groupNo in _drag.sets:
                    try:
                        _group = model.component.sets[_groupNo]
                        for membNo in _group.items:
                            memb_name = str(membNo)
                            memb = model.component.concepts[memb_name]
                            try:
                                memb.properties.cdcm.append(_cdcm[i])
                            except AttributeError:
                                memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
                                memb.properties.cdcm = _cdcm[i]
                    except KeyError:
                        continue
    #
    # ---------------------------
    #
    if _cdcm:
        try:
            _metocean.cdcm.update(_cdcm)
        except AttributeError:
            _metocean.cdcm = _cdcm 
    #
    #
    if _flooded:
        _flooded_items = []
        for key, _flod in _flooded.items():
            if 'element' in key:
                get_flooded_item(_flod, _flooded_items, model)
            
            elif 'group' in key:
                #print('group')
                for _item in _flod:
                    try:
                        _set = model.component.sets[str(_item)]
                        get_flooded_item(_set.concepts, _flooded_items, model)
                    except KeyError:
                        print('   *** warinig flooded group {:} not found'.format(_item))                
            
            elif 'property' in key:
                #print('property')
                for _item in _flod:
                    try:
                        _prop = model.component.sections[str(_item)]
                        get_flooded_item(_prop.elements, _flooded_items, model)
                    except KeyError:
                        print('   *** warinig flooded property {:} not found'.format(_item))
            
            else:
                print('  *** error flooded item not identified : {:}'
                      .format(key))
            
        #
        _flooded_items = list(set(_flooded_items))
        try:
            model.component.sets['_flooded_']
        except KeyError:
            _set_number = [int(_set.number)  for _set in model.component.sets.values()]
            _set_number = max(_set_number) + 10
            model.component.sets['_flooded_'] = f2u.Geometry.Groups('_flooded_', _set_number)
        
        model.component.sets['_flooded_'].concepts.extend(_flooded_items)
        
        
        #try:
        #    _metocean.flooded.update(_flooded_items)
        #except AttributeError:
        #    _metocean.flooded = _flooded_items
    #
    if _buoy:
        # i = 0
        # _buoyancy = {}
        # for _by in _buoy.values():
        #    i += 1
        #    _buoyancy[i] = _by
        #    _buoyancy[i].number = i
        #    _buoyancy[i].name = i
        try:
            _metocean.buoyancy.update(_buoy)
        except AttributeError:
            _metocean.buoyancy = _buoy
    #
    #
    #if _seastate:
        #_name_meto = asas_file.split('.')
        #_name_meto = load_operation.load_title(_name_meto[0])
        #_meto = {combination_name : _seastate}
        #try:
        #    _metocean.combination.update(_meto)

        #except AttributeError:
        #    _metocean.combination = _meto   
    #
    #
    #return link
#
#