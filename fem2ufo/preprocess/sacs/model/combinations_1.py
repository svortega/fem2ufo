# 
# Copyright (c) 2009-2016 fem2ufo
#

# Python stdlib imports
import os
import re
import copy
#from operator import itemgetter
from itertools import islice, chain
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process

#
def comb_of_comb(comb_item, _functional, _combinations,
                 _replace, res_factor=1.0):
    """
    """
    
    def basic_load(_replace, _item, _functional, factor=1.0):
        """
        """
        _basic = _functional[_item[0]]
        _replace.append([_item[0], _item[1] * factor])
    #
    def combined_load(_replace, _item, _functional, factor=1.0):
        """
        """
        _combination = _combinations[_item[0]]
        
        for _comb_functional in _combination.functional_load:
            try:
                basic_load(_replace, _comb_functional, _functional,
                           factor=factor*_item[1])
            except KeyError:
                #print('--->')
                comb_of_comb(_comb_functional, _functional, _combinations,
                             _replace, res_factor=factor)
    #
    _basic_comb = _combinations[comb_item[0]]
    
    for _item in _basic_comb.functional_load:
        try:
            basic_load(_replace, _item, _functional, factor=comb_item[1]*res_factor)
        except KeyError:
            #if 'TCOX' in comb_item[0]:
            #    print('-->')
            combined_load(_replace, _item, _functional, factor=comb_item[1]*res_factor)
    #
#
#
def add_combination(name, model, _combination, index, _comb_name):
    """
    """
    try:
        model.analysis.case[_comb_name].load_combination[index][name] = _combination
    except KeyError:
        try:
            model.analysis.case[_comb_name].load_combination[index] = {name : _combination} 
        except KeyError:
            model.analysis.case[_comb_name] = f2u.Analysis.Case(_comb_name)
            model.analysis.case[_comb_name].load_combination[index] = {name : _combination}
    #except:
    #    print('-->', name)
    #
    #
#
#
def get_metocean(load_name, _comb_functional, seastate, _analysis_case,
                 _functional, _basic_no):
    """
    """
    #
    try:
        _seastate = seastate[_comb_functional[0]]
        # print('seastate', _seastate.name)
        #
        _name_seatate = 'MET'+ str(_seastate.number).zfill(3) +'_'+ str(_comb_functional[0])
        #
        # check if metocean combination exist
        try:
            _analysis_case.metocean_combination[_name_seatate]
        except KeyError:
            _analysis_case.metocean_combination[_name_seatate] = _seastate
            _analysis_case.metocean_combination[_name_seatate].name = _name_seatate        
        #
        # check if wind_area
        try:
            _wind_area = _seastate.wind_areas
            _new_name = 'comb_wind_area_' + str(_seastate.number).zfill(3)
            _seastate.wind.name = _new_name
            for _area in _wind_area:
                _area.load_interface.append(_seastate.wind.name)
            #
            _name = _comb_functional[0]
            try:
                _basic = _functional[_name]
                if _basic.number == _seastate.number:
                    _basic_no += 1
                    _basic.number = _basic_no
            except KeyError:
                _basic_no += 1
                #_number = _seastate.wind.number
                # new functional wind load
                _functional[_name] = f2u.Load.Basic(_name, _basic_no)
                _basic = _functional[_name]
            # 
            _basic.class_type = "wind area group"
            _basic.areas.extend(_wind_area)
            _basic.name = _new_name
        except AttributeError:
            pass
        #
        # add metocean case
        _new_combination = _analysis_case.load_combination[0][load_name].metocean_load
        _new_combination.append([_name_seatate, _comb_functional[1]])
        # report load to be delete
        _del_combination = _comb_functional[0]
    except KeyError:
        _del_combination = None
        #print('no seastate', _comb_functional)
    #
    #print('--> end meto')
    return _del_combination, _basic_no
#
def del_comb_functional(_meto_update, _item_combination):
    """
    """
    _index = []
    for _update in  _meto_update:
        for x, _item in enumerate(_item_combination):
            if _update == _item[0]:
                _index.append(x)
    # delete redundant basic cases (replaced by meto case)
    for x in sorted(_index, reverse=True):
        del _item_combination[x]    
#
def window(seq, n=2):
    "Returns a sliding window (of width n) over data from the iterable"
    "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result    
    for elem in it:
        result = result[1:] + (elem,)
        yield result
#
def missing_elements(L):
    missing = chain.from_iterable(range(x + 1, y) for x, y in window(L) if (y - x) > 1)
    return list(missing)
#
#
def get_load_number(_functional, seastate, _combinations):
    """
    """
    # find basic load number
    _basic_no_list = [_basic.number for key, _basic in _functional.items()]
    _basic_no = max(_basic_no_list)
    _basic_no -= _basic_no % -100
    _basic_no = int(round(_basic_no, -2))
    if seastate:
        _sea_no = max([_basic.number for key, _basic in seastate.items()])
        _sea_no -= _sea_no % -100
        _basic_no = max(_basic_no, _sea_no)
    #
    # find comb load number
    _comb_no_list = [_comb.number for _comb in _combinations.values()]
    _div = len(_comb_no_list) 
    _div -= _div % -100
    _comb_no = min(_comb_no_list)
    _comb_no -= _div
    _comb_no = int(round(_comb_no, -2))
    #
    # check basic vs comb load number
    if abs(_basic_no - int(_comb_no)) <= _div:
        #_basic_no = max(_basic_no - 100, max(_basic_no_list))
        #_basic_no = int(round(_basic_no, -1))
        _comb_no = missing_elements(_comb_no_list)
        if len(_comb_no) < _div:
            _total = _div - len(_comb_no)
            try:
                _max_comb = max(_comb_no) + 1
                _add = [x for x in range(_max_comb, _max_comb + _total)]
                _comb_no.extend(_add)
            except ValueError:
                _max_comb = max(_comb_no_list) + 1
                _comb_no = [x for x in range(_max_comb, _max_comb + _div)]
    else:
        _comb_no = [x for x in range(_comb_no, _comb_no + _div)]
    #
    return _basic_no, _comb_no
#
def read_sacs_load_combinations(file_name, model, design_condition, 
                                seastate=False, elevation=False):
    """
    """
    print('')
    print('-------------- SACS Load Combination Module --------------')
    #
    if not model.analysis:
        model.set_analysis(model.name)    
    #
    _combinations = read_file_component(file_name, model,
                                        design_condition)
    _functional = model.load.functional
    
    _comb_name_list = [key for key in sorted(_combinations, key=lambda 
                                             name: _combinations[name].number)]
    #
    _basic_no, _comb_no = get_load_number(_functional, seastate, _combinations)
    #
    for key in _comb_name_list:
        #if '2001' in key:
        #    print('-->')
        _flag = False
        _combination = _combinations[key]
        _design_condition = _combination.design_condition
        #
        _name = key
        _new_level_0 = []
        _new_level_1 = []
        _new_basic = []
        _index = 0
        for _comb_functional in _combination.functional_load:
            # check if basic
            try:
                _basic = _functional[_comb_functional[0]]
                _new_basic.append(_comb_functional)
            except KeyError:
                # check first level (in demand)
                try:
                    _comb_level_0 = model.analysis.case[_design_condition].load_combination[0]
                    _new_level = _comb_level_0[_comb_functional[0]]
                    _new_level_0.append(_comb_functional)
                    _flag = True
                except KeyError:
                    # check second level
                    try:
                        _comb_level_1 = model.analysis.case[_design_condition].load_combination[1]
                        _new_level = {_item.name : _key 
                                         for _key, _item in _comb_level_1.items()}
                        _new_level = _new_level[_comb_functional[0]]
                        _new_level = _comb_level_1[_new_level]
                        _new_level_0.extend(_new_level.functional_load)
                        _flag = True
                    except KeyError:
                        # check metocean
                        try:
                            _meto = seastate[_comb_functional[0]]
                            _new_basic.append(_comb_functional)
                        except KeyError:
                            # check combinations
                            _replace = []
                            comb_of_comb(_comb_functional, _functional, _combinations, _replace)
                            if _replace:
                                _new_basic.extend(_replace)
                                _flag = True
                            else:
                                print('    ** Warning {:} not found'.format(_comb_functional[0]))
                                continue
        #
        if _flag:
            # create Firs level combination
            if _new_basic:
                _new_number = _comb_no.pop(0)
                _name1 = 'LCA'+ str(_new_number).zfill(3) + '_' + str(_name)
                _index = 0
                _new_combination = copy.deepcopy(_combination)
                _new_combination.functional_load = copy.copy(_new_basic)
                _new_combination.name = _name1
                _new_combination.number = _new_number
                add_combination(_name1, model, _new_combination, _index, _design_condition)
                #
                _analysis_case = model.analysis.case[_design_condition]
                _meto_update = []
                for _item in _new_basic:
                    _update_item, _basic_no = get_metocean(_name1, _item, seastate, _analysis_case,
                                                           _functional, _basic_no)
                    if _update_item:
                        _meto_update.append(_update_item)
                #
                if _meto_update:
                    _item_combination = _analysis_case.load_combination[0][_name1].functional_load
                    del_comb_functional(_meto_update, _item_combination)
                    #print('--> meto')
            #
            # create Second level combination
            if _new_level_0 or _new_basic:
                _number = _combination.number
                _name2 = 'LCF'+ str(_number).zfill(3) + '_' + str(_name)
                _index = 1
                if _new_level_0:
                    _combination.functional_load = _new_level_0
                if _new_basic:
                    _combination.functional_load.append([_name1, 1.0])
                #_combination.name = _name2
                add_combination(_name2, model, _combination, _index, _design_condition)
        else:
            #_comb_no += 1
            #_name = 'LCA'+ str(_comb_no).zfill(3) + '_' + str(_name)
            #_combination.number = _comb_no
            _combination.name = 'LCA'+ str(_combination.number).zfill(3) + '_' + str(_name)
            add_combination(_name, model, _combination, _index, _design_condition)
            #
            _analysis_case = model.analysis.case[_design_condition]
            _meto_update = []
            for _item in _new_basic:
                _update_item, _basic_no = get_metocean(_name, _item, seastate, _analysis_case, 
                                                       _functional, _basic_no)
                if _update_item:
                    _meto_update.append(_update_item)                
            #
            if _meto_update:
                _item_combination = _analysis_case.load_combination[0][_name].functional_load
                del_comb_functional(_meto_update, _item_combination)                
        #print('--> meto')
        # update elevations
        if elevation:
            model.analysis.case[_design_condition].condition = elevation        
    #
    #print('here level 0')
#
#
def read_file_component(file_name, model, design_condition):
    """
    """  
    #        
    #
    # read SACS input file
    sacs_file = process.common.check_input_file(file_name)
    if not sacs_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    #
    print('')
    print('--- Reading Load Combinations from {:} input file'.format(file_name))
    #
    #
    #if not model.analysis:
    #    model.set_analysis(_comb_name)    
    #
    _combinations = {}
    try:
        _combination = model.analysis.case[design_condition]
        _max_lcom = [_lc.number for _lc in _combination.load_combination[0].values()]
        #print('-->')
    except KeyError:
        _max_lcom = [_lc.number for _lc in model.load.functional.values()]
    
    _max_lcom = max(_max_lcom)
    _max_lcom -= _max_lcom % -100     
    _load_no = _max_lcom
    #
    print('    * First pass')
    #_checkme_file = 'CheckMe_' + sacs_file.replace('CheckMe_', '')
    #dat_temp = open(_checkme_file,'w+')
    dat_temp = open('combTemp.dat','w+')
    #
    with open(sacs_file) as fin:
        for line in fin:
            #
            # Jump commented lines
            if re.match(r"\*|\-", line):
                continue
            #
            line = line.strip()
            if not line:
                continue
            #
            # Segment line
            keyword = line.split()
            # Flag word
            if line[0]:
                flag1 = keyword[0]            
            #
            #
            if 'LCSEL' in flag1.upper():
                _function = line[6:8].strip()
                _cases = line[16:75].split()
                if not _cases:
                    continue
                #
                for _load_name in _cases:
                    try:
                        _combinations[_load_name]
                    except KeyError:
                        try:
                            _number = int(_load_name)
                            _new_name = 'LCA' + str(_number).zfill(3) +'_'+ design_condition +'_'+ _load_name
                        except ValueError:
                            _load_no += 1
                            _number = _load_no
                            _new_name = 'LCA' + str(_number).zfill(3) +'_'+ design_condition +'_'+ _load_name
                        _new_name = _load_name
                        _combinations[_load_name] = f2u.Load.Combination(_new_name, _number)
                        _combinations[_load_name].design_condition = design_condition
            #
            elif 'LCFAC' in flag1.upper():
                _function = line[6:8].strip()
                _factor = float(line[10:16])
                _cases = line[16:75].split()
                print(' fix lcfact')
                1/0.0
            # before if here
            elif 'LDCASE' in flag1.upper():
                _cases = line[7:75].split()
                if not _cases:
                    continue
                
                for _load_name in _cases:
                    try:
                        _combinations[_load_name]
                    except KeyError:
                        try:
                            _number = int(_load_name)
                            _new_name = 'LCA' + str(_number).zfill(3) +'_'+ design_condition +'_'+ _load_name
                        except ValueError:
                            _load_no += 1
                            _number = _load_no
                            _new_name = 'LCA' + str(_number).zfill(3) +'_'+ design_condition +'_'+ _load_name
                        _new_name = _load_name
                        _combinations[_load_name] = f2u.Load.Combination(_new_name, _number)
                        _combinations[_load_name].design_condition = design_condition                
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line) 
    #
    dat_temp.close()
    #
    print('    * Second pass')
    dat_temp = open('combTemp2.dat','w+')
    #
    with open('combTemp.dat') as fin:
        for line in fin:
            #
            # Jump commented lines
            if re.match(r"\*|\-", line):
                continue
            #
            line = line.strip()
            if not line:
                continue
            #
            # Segment line
            keyword = line.split()
            # Flag word
            if line[0]:
                flag1 = keyword[0]            
            #
            #
            if 'AMOD' in flag1.upper():
                _code = line[4:].strip()
                if not _code:
                    continue
                #_code = _code.split()
                #_design_condition = 'storm'
                _step = 2
                for x in range(0, 7):
                    _load_name = _code[10*x: 10*x+4].strip()
                    if not _load_name:
                        continue
                    #_factor = float(_code[10*x+4: 10*x+6])
                    #if _factor > 1.0:
                    try:
                        _combinations[_load_name].design_condition = design_condition #'storm'
                    except KeyError:
                        pass
            #
            elif 'LCOMB' in flag1.upper():
                _load_name = line[6:10].strip()
                if not _load_name:
                    continue
                
                _step = 0
                for x in range(6):
                    try:
                        _case = line[11 + _step : 15 + _step].strip()
                        _factor = float(line[15 + _step : 21 + _step])
                        _step += 10
                        try:
                            _combinations[_load_name].functional_load.append([_case, _factor])
                        except KeyError:
                            try:
                                _number = int(_load_name)
                                _new_name = 'LCA' + str(_number).zfill(3) + '_' + design_condition +'_'+ _load_name
                            except ValueError:
                                _load_no += 1
                                _number = _load_no
                                _new_name = 'LCA' + str(_number).zfill(3) +'_'+ design_condition +'_'+ _load_name
                            _new_name = _load_name
                            _combinations[_load_name] = f2u.Load.Combination(_new_name, _number)
                            _combinations[_load_name].functional_load.append([_case, _factor])
                            _combinations[_load_name].design_condition = design_condition
                            #
                    #
                    except ValueError:
                        break
            #
            elif 'LDCOMB' in flag1.upper():
                _load_name = line[6:10].strip()
                if not _load_name:
                    continue
                _step = 0
                for x in range(6):
                    try:
                        _factor = float(line[10 + _step : 18 + _step])/100.0
                        _case = line[18 + _step : 20 + _step].strip()
                        _step += 10
                        try:
                            _combinations[_load_name].functional_load.append([_case, _factor])
                        except KeyError:
                            try:
                                _number = int(_load_name)
                                _new_name = 'LCA' + str(_number).zfill(3) +'_'+ design_condition +'_'+ _load_name
                            except ValueError:
                                _load_no += 1
                                _number = _load_no
                                _new_name = 'LCA' + str(_number).zfill(3) +'_'+ design_condition +'_'+ _load_name
                            _new_name = _load_name
                            _combinations[_load_name] = f2u.Load.Combination(_new_name, _number)
                            _combinations[_load_name].functional_load.append([_case, _factor])
                            _combinations[_load_name].design_condition = design_condition
                    #
                    except ValueError:
                        break
                #print('???')
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    os.remove('combTemp.dat')
    #
    #
    try:
        _checkme_file = 'CheckMe_' + sacs_file.replace('CheckMe_', '')
        os.remove(sacs_file)
        os.rename('combTemp2.dat', _checkme_file)
    except FileExistsError:
        pass
    #print('here combination')
    #
    return _combinations