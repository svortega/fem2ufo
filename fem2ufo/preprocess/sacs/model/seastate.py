# 
# Copyright (c) 2009-2021 fem2ufo
# 


# Python stdlib imports
from collections import Counter
import sys
import re
import os
#import operator
from math import copysign, isclose
from statistics import mean
import copy
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.sacs.operations.common as sacs_common
import fem2ufo.preprocess.sacs.operations.process as sacs_process
import fem2ufo.preprocess.sacs.operations.metocean as sacs_meto
from fem2ufo.preprocess.sacs.model.combinations import read_sacs_load_combinations

#
def polygon_area(vertices) -> float:
    """Find area of polygon via Exterior algebra"""
    n = len(vertices) # of corners
    a = 0.0
    for i in range(n):
        j = (i + 1) % n
        a += (vertices[i].coordinates.x * vertices[j].coordinates.y -
              vertices[j].coordinates.x * vertices[i].coordinates.y)
    result = abs(a / 2.0)
    return result
#
def load_title(lineIn):
    """
    """
    lineOut = lineIn
    lineOut = re.sub(r"\(|\)|\[|\]|\{|\}|\,", "_", lineOut)
    lineOut = re.sub(r"\-|\+|\%|\s|\=", "_", lineOut)
    lineOut = re.sub(r"\&", "and", lineOut)
    lineOut = re.sub(r"\/|\'", "", lineOut)
    lineOut = re.sub(r"\.", "dot", lineOut)
    lineOut = re.sub(r"\_+", "_", lineOut)
    lineOut = re.sub(r"\?", "Qmark", lineOut)
    lineOut = re.sub(r"\@", "at", lineOut)
    lineOut = re.sub(r"\*", "x", lineOut)
    lineOut = re.sub(r"\#", "no", lineOut)
    lineOut = lineOut.strip()
    lineOut = lineOut.strip("_")

    try:
        int(lineOut[0])
        lineOut = 'L_' + lineOut
    except IndexError:
        return None
    except ValueError:
        pass

    return lineOut
#
def get_coordinate(coord, _length_factor):
    """
    """
    try:
        return round(float(coord)*_length_factor, 3)
    except ValueError:
        return 0
#
def get_gravity(_location, _grav=False):
    """
    """
    if not _grav:
        _grav = [0, 0, 0]
    
    _sing = 1.0
    if '-' in _location.lower():
        _sing = -1.0
    
    if 'x' in _location.lower():
        _grav[0] = 9.81 * _sing 
        
    elif 'y' in _location.lower():
        _grav[1] = 9.81 * _sing
        
    elif 'z' in _location.lower():
        _grav[2] = 9.81 * _sing 
    
    return _grav
#
#
def get_design_type(line_in):
    """
    """
    _key = {"MaxBaseShear": r"\b(ms|mu)\b",
            "MaxOMoment": r"\b(mm)\b",
            "MinBaseShear": r"\b(ns|md)\b",
            "MinOMoment": r"\b(nm)\b",
            "BothLoads": r"\b(al)\b"} #,
            #"MinBothLoads": r"\b(min(imum)?\s*both\s*(loads)?)\b",
            #"NoDesignLoads": r"\b(no\s*design\s*loads)\b"}

    key_word = process.common.find_keyword(line_in, _key)
    #
    return key_word
#
def wind_theory(line_in):
    """
    """
    _key = {"general": r"general",
            "normal": r"normal",
            "abs": r"abs",
            "extreme": r"ap",
            "extreme_api21": r"21ap",
            "constant": r"cons"}
    key_word = process.common.find_keyword(line_in, _key)
    return key_word
#
def wave_theory(line_in):
    """
    """
    _key = {"airy_linear": r"airc",
            "airy_extrapolated": r"airy",
            #"airy_extretched": r"airy",
            "stokes_5": r"stok",
            "stream": r"stre|strn",
            #"stream_no_current": r"strn",
            "cnoidal": r"cnoi",
            "user_defined": r"line",
            "solitary": r"soli",
            "repeat": r"rept"}
    key_word = process.common.find_keyword(line_in, _key)
    return key_word
#
def get_wind_units(_units_in, _w_units, _length_factor):
    """
    """
    if "EN" in _units_in.upper():
        units4 = "kilolbf"
        _factorW = 1
        if not _w_units:
            _w_length = 0.514444
        else:
            if 'm' in _w_units.lower():
                units0 = 'mile'
                _factorW = 1.0 / 3600
            elif 'f' in _w_units.lower():
                units0 = "foot"
            else:
                print('    *** error unit : {:} not recognized'.format(_w_units))
                1/0

            _w_length, _w_force = process.units.convert_units_SI(units0, units4)
            _w_length = _w_length * _factorW
    else:
        _w_length = _length_factor
    return _w_length
#
#
def point_beam(P:float, L:float, L1:float):
    """ """
    L2 = L - L1
    R1 = P * L2 / L
    R2 = P * L1 / L
    return R1, R2
#
def trapz_beam(w1:float, w2:float, L:float, 
               L1:float, L2:float):
    """ """
    w3 = int(w2 - w1)
    L3 = L - (L1 + L2)
    Lp = L1 + L3/2.0    
    try:
        1/w3
        w4 = min(w2, w1)
        if w4 == w1:
            Lt = L1 + 2*L3/3.0
        else:
            Lt = L1 + L3/3.0
        #
        Pw = w4 * L3
        Pt = w3 * L3 / 2.0
        Rt1, Rt2 = point_beam(P=Pt, L=L, L1=Lt)
    except ZeroDivisionError:
        Pw = w1 * L3
        Rt1, Rt2 = 0, 0
    Ru1, Ru2 = point_beam(P=Pw, L=L, L1=Lp)
    return Ru1+Rt1, Ru2+Rt2
#
def updte_mass_node(i, node, mass_load,  groups, group_id, group_no):
    """ """
    try:
        node.mass[i] += mass_load
    except IndexError:
        joint_id = node.name
        node.mass = [0,0,0,0,0,0]
        node.mass[i] += mass_load
        try:
            groups[group_id].nodes
        except KeyError:
            group_no += 1
            groups[group_id] = f2u.Geometry.Groups(group_id, group_no)
        groups[group_id].nodes.append(joint_id)
    return group_no
#
#
def read_seastate(file_name, model, design_condition): 
                    # , combination_name):
    """
    Module to read sacs seastate input file
    """
    print('')
    print('---------------- READ SEASTATE Module --------------')
    # read T fem file
    sea_file = process.common.check_input_file(file_name)

    if not sea_file :
        print('    ** I/O error : seastate file {:} not found'
              .format(file_name))
        sys.exit()
    #
    #
    # ---------------------------
    #    
    #if not combination_name:
    #    combination_name = sea_file.replace('.', '_')
    #
    # check if metocean module exist
    if model.metocean:
        _metocean = model.metocean
    else:      
        model.set_metocean('metocean')
        _metocean = model.metocean
        _metocean.flooded = {}
    #
    #model.component.sets['nohydro'] = f2u.Geometry.Groups('nohydro')
    #
    #
    # check if analysis already set up
    #if not model.analysis:
    #    model.set_analysis(combination_name)
    #
    # ---------------------------
    #
    _groups = model.component.sets
    _nodes = model.component.nodes
    _members = model.component.concepts
    _functional = model.load.functional
    #
    _node_no = len(_nodes)
    _node_stats = [_item.z for _item in _nodes.values()]
    _node_stats = Counter(_node_stats)
    _node_stats = {key:_item/_node_no for key, _item in _node_stats.items()}
    #
    _load_no = [_load.number for _load in _functional.values()]
    _group_no = max([item.number for item in _groups.values()])
    #
    _elevation = {}
    _seastate = {}
    _wave = {}
    _wind = {}
    _current = {}
    _flooded = {}
    _temp_cdcm = {}
    _mgrowth = {}
    _wind_shield = []
    #
    _hydro_diameter = {}
    _hydro_buoy_area = {}
    _grwth_option = {}
    _dummy = {}
    _item_nonstr = []
    _tubular_joint = {}
    #
    _no_elev = 0
    #_sea_number = 0
    _meto_number = 0
    _mgrowth_no = 0
    #_no_floo = 0
    _wave_repeat = None
    global_flooded = False
    #
    _areas = {}
    _area_no = 0
    _surfwt = {}
    _wgt_sets = {}
    _density_sets = {}
    #
    print('    * First pass')
    dat_temp = open('seaTemp.dat','w+')
    #
    with open(sea_file) as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*|\-", line):
                continue
            #
            line = line.strip()
            if not line:
                continue
            #
            if 'LDOPT' in  line.upper():
                # get factors
                _units_in = line[52:54]
                _length_factor, _force_factor, _mass_factor = sacs_common.get_units(_units_in)
                _area_factor = _length_factor
                # set units
                units = ["foot", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
                if 'M' in _units_in.upper():
                    units = ["metre", "1.0*kilogram", "second", 'centigrade', "newton", "pascal"]
                #
                _length = sacs_common.get_length_factor(units)
                #
                # ---------------------------------------------
                #
                vert_coord = line[14:16].strip()
                if vert_coord:
                    if '-' in vert_coord:
                        print('negative coordinate, now what?')

                    elif 'x' in vert_coord.lower():
                        print('x coordinate up, now what?')

                    elif 'y' in vert_coord.lower():
                        print('y coordinate up, now what?')

                    else:
                        vert_coord = 1
                # dafault +z
                else:
                    vert_coord = 1
                #
                _no_elev += 1
                _lev_name = sea_file # combination_name #'Elev_' + str(_no_elev)
                _elevation = f2u.Metocean.Condition(_lev_name, _no_elev)

                try:
                    _mudline = float(line[32:40]) * _length_factor
                except ValueError:
                    _mudline = 0

                _elevation.mudline = _mudline 

                try:
                    _water_depth = float(line[40:48])  * _length_factor
                except ValueError:
                    _water_depth = 0.01

                _elevation.water_depth.append(_water_depth)
                _elevation.surface = (_water_depth - abs(_mudline))
                _elevation.water_density = float(line[16:24]) * _mass_factor / _length_factor**3
                # elevation
                # set marine growth
                #mg_mudline = _elevation.mudline
                #
                # by default all members non flooded
                #global_flooded = False
                if 'FL' in line[12:14].strip():
                    global_flooded = True
                    #print('global flooded really?')
                #
                #dat_temp.write("\n")
                #dat_temp.write(line)
            #
            elif 'OPTION' in line.upper():
                # get factors
                _units_in = line[13:15].strip()
                _area_factor, _force_factor, _mass_factor = sacs_common.get_units(_units_in)
                #_length_factor, _force_factor, _mass_factor = sacs_common.get_units(_units_in)
                #_area_factor = _length_factor                
                # set units
                units = ["foot", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
                if 'M' in _units_in.upper():
                    units = ["metre", "1.0*kilogram", "second", 'centigrade', "newton", "pascal"]
                #
                _length2 = sacs_common.get_length_factor(units)
                _force2 = _force_factor
                if 'EN' in _units_in:
                    _force2 = _force_factor / 1000.00
                #            
            #
            elif 'LCSEL' in  line.upper():
                #_sea_number = line[5:].split()
                #
                dat_temp.write("\n")
                dat_temp.write(line)                
            #
            elif 'LOADCN' in  line.upper():
                """
                """
                if not "number:" in line:
                    _meto_name = line[6:10].strip()
                    #
                    try:
                        _meto_number = int(_meto_name)
                    except ValueError:
                        _meto_number += 1
                    #
                    if not _meto_name:
                        _meto_name = str(_meto_number)
                    #
                    # user factor
                    try:
                        _user_factor = float(line[32:39])
                    except ValueError:
                        _user_factor = 1
                    #
                    # buoyancy factor
                    try:
                        _buoy_factor = float(line[39:46])
                    except ValueError:
                        _buoy_factor = 1
                    #
                    #
                    dat_temp.write("\n")
                    dat_temp.write('LOADCN number: {:} name: {:} user_factor: {:}, buoyancy_factor: {:}'
                                   .format(_meto_number, _meto_name, _user_factor, _buoy_factor))
                else:
                    _data = line[6:].split()
                    _meto_name = _data[3].strip()
                    _meto_number = int(_data[1])
                    # keep load numbering
                    dat_temp.write("\n")
                    dat_temp.write(line)
                
                _load_no.append(_meto_number)
                #_seastate[_meto_name] = f2u.Metocean.Combination(_meto_name, _number)
            #
            elif 'CDM' in line.upper():
                if not line[4:].strip():
                    continue                
                #
                _option = line[4:6].strip()
                if _option:
                    print('fix cdcm option here')
                else:
                    try:
                        _diameter = float(line[6:12]) * _length_factor
                    except ValueError:
                        _diameter = None

                    sacs_meto.get_cdcm(_diameter, _temp_cdcm, line, _length)

                    #_zone[_name_zone] = f2u.Metocean.Zone(_name_zone, _zone_no)
                    #_zone[_name_zone]['zmin'] = _lower * _length_factor
                    #_zone[_name_zone]['zmax'] = _upper * _length_factor
                    #print('here')
            #
            elif 'FILE' in line.upper():
                # print('fix this FILE option')
                dat_temp.write("\n")
                dat_temp.write(line)                
            #
            elif line[0:5] == 'DUMMY' :
                _set_name = line[6:14].strip()
                _dummy[_set_name] = {'boundary':None, 'nonstructural':None,
                                     'description': None}
                _description = line[14:].strip()
                if _description:
                    _dummy[_set_name]['description'] = _description
            #
            elif line[0:4] == 'KEEP' :
                _keep = line[7:].split()
                _dummy[_set_name]['boundary'] = _keep
            #
            elif line[0:6] == 'DELETE' :
                _nonstr = line[7:].split()
                _dummy[_set_name]['nonstructural'] = _nonstr
            #
            #
            elif 'DELDRP' in line.upper():
                print('fix this delete group option')
                dat_temp.write("\n")
                dat_temp.write(line)                
            #
            elif 'DELMEM' in line.upper():
                _joint_data = line[8:72].strip()
                _joint_data = _joint_data.split()
                for x in range(0, 12, 2):
                    try:
                        _memb_name = _joint_data[x] + '_' +_joint_data[x+1]
                        _memb = _members[_memb_name]
                        _memb.type = _memb.type + '_nonstructural'
                        _item_nonstr.append(_memb_name)
                    except IndexError:
                        continue
                    except KeyError:
                        print('    ** Warning meber {:} not found'
                              .format(_memb_name))
            #
            elif 'DELJNT' in line.upper():
                print('fix this delete joint option')
                dat_temp.write("\n")
                dat_temp.write(line)                
            #
            # surface section
            #
            elif 'AREA' in line.upper():
                _option = line[78:79].strip()
                if not _option:
                    continue

                if _option in ['F', 'R', 'A']:
                    _type = 'wind_area'
                elif 'D' in _option:
                    _type = 'drag_area'
                elif 'I' in _option:
                    _type = 'inertia_area'
                else:
                    print('   ** error area type {:} not identified'
                          .format(_option))
                #
                _area_no += 1
                _area_id = line[4:6].strip()
                if not _area_id:
                    _area_id = 'default'
                #
                _area_id = _area_id + '_' + str(_area_no)
                #
                _coord_x = get_coordinate(line[24:31], _area_factor)
                _coord_y = get_coordinate(line[31:39], _area_factor)
                _coord_z = get_coordinate(line[39:45], _area_factor)

                _centre = [_coord_x, _coord_y, _coord_z]

                try:
                    _shape_factor = float(line[45:50])
                except ValueError:
                    _shape_factor = 1.0

                _list = line[50:78].strip()
                _step = 4
                _node_list = []
                for x in range(0, 7):
                    _node_list.append(_list[x*_step: _step + x*_step].strip())
                _node_list = [x for x in _node_list if x]

                _proyection = [line[6:12].strip(),
                               line[12:18].strip(),
                               line[18:24].strip()]

                _proyection = [float(x) * _area_factor**2 
                               if x else None for x in _proyection]

                _size = [x**0.5 for x in _proyection if x]
                _size = round(sum(_size)/len(_size), 2)
                _areas[_area_id] = f2u.Load.Equipment(name=_type + '_' +_area_id,
                                                      number=_area_no)
                #_areas[_area_id].name = 'wind_area_'+_area_name
                _areas[_area_id].type = _type
                _areas[_area_id].size = _size, _size, 0.10
                _areas[_area_id].mass = 0, _centre
                #
                #_coord = []
                for _node_name in _node_list:
                    try:
                        _node = _nodes[_node_name]
                        model.component.joints[_node_name] = f2u.Geometry.Joints(name = _node_name,
                                                                                 node = _node)
                        model.component.joints[_node_name].type = 'interface'

                        _areas[_area_id].joints.append(model.component.joints[_node_name])
                    except KeyError:
                        print('    ** Warning node {:} not found'
                              .format(_node_name))
                #
                if _proyection[0]:
                    _areas[_area_id].wind_pressure_x(_shape_factor, 0)

                if _proyection[1]:
                    _areas[_area_id].wind_pressure_y(_shape_factor, 0)

                if _proyection[2]:
                    _areas[_area_id].wind_pressure_z(_shape_factor, 0)
                #
                #
                #try:
                #    _areas[_area_id].append(_area)
                #except:
                #    _areas[_area_id] = [_area]
                #
                #_areas[_area_id] = {'proyection':_proyection,
                #                    'coordinate':_coord,
                #                    'shape_factor':_shape_factor,
                #                    'nodes' : _node_list,
                #                    'option':_option}               
                #print('---> fix area')
            #
            elif 'SURFID' in line.upper():
                """THE SURFACE IDENTIFIER LINE ALLOWS THE DESCRIPTION OF A BOUNDED DEFINED AREA
                   WHERE LOAD AND MASS IS DISTRIBUTED TO MEMBERS WITHIN THIS AREA"""
                #
                _type = 'equipment'
                _area_no += 1
                _area_id = line[7:14].strip()
                #_area_name = sacs_common.get_name(_area_id, prefix=_type)
                _areas[_area_id] = f2u.Load.Equipment(name=_type + '_' +_area_id,
                                                      number=_area_no)
                _areas[_area_id].type = _type
                #
                # ORIGIN JOINT NAME
                _coord_master = line[18:22].strip()
                _coord_master = _nodes[_coord_master]
                _coord = [_coord_master.x, _coord_master.y, _coord_master.z]
                _areas[_area_id].mass = 0, _coord
                #
                # LOAD DISTRIBUTION TYPE
                if 'LX' in line[15:17].strip():
                    #print('lx')
                    _angle = 0
                else:
                    _angle = 90
                    #print('ly')                
                #
                # LOCAL X JOINT NAME
                _coord_1 = line[23:27].strip()
                _coord_1 = _nodes[_coord_1]
                #
                # LOCAL Y JOINT NAME
                _coord_2 = line[28:32].strip()
                _coord_2 = _nodes[_coord_2]
                #
                #_sign_x = _coord_master.x - _coord_1.x
                #_sign_y = (_coord_master.y - _coord_2.y) * -1.0
                #
                # LOCAL COORDINATES DEFINITION (swap axis?)
                #_areas[_area_id].local_system = [_coord_master.number, _coord_1.number,
                #                                 _coord_2.number, _angle]
                _areas[_area_id].local_system = [0, 0, 0, _angle]                
                #
                # OUT-OF-PLANE TOLERANCE
                _tol= float(line[33:40].strip()) * _length2
                _areas[_area_id].tolerance[2] = _tol
                #
            #
            elif 'SURFDR' in line.upper():
                """THE SURFACE DEFINITION RECORD ALLOWS THE DEFINITION OF THE BOUNDARY OF THE SURFACE AREA"""
                # BOUNDARY JOINT NAMES
                _list = line[7:76].strip()
                _step = 5
                _node_list = []
                for x in range(14):
                    _node_list.append(_list[x*_step: _step + x*_step].strip())
                _node_list = [x for x in _node_list if x]
                #
                _xdir = []
                _ydir = []
                _zdir = []
                _points = []
                for _node_name in _node_list:
                    try:
                        _node = _nodes[_node_name]
                        _xdir.append(_node.x)
                        _ydir.append(_node.y)
                        _zdir.append(_node.z)
                        #_areas[_area_id].foot_print.append(_node_name)
                        _points.append(_node)
                    except KeyError:
                        print('    ** Warning node {:} not found'
                              .format(_node_name))
                #
                #_points.append(_points[0])
                _area = polygon_area(_points)
                #
                _xdir = sorted(_xdir)
                _ydir = sorted(_ydir)
                #
                _size_x = _xdir[-1] - _xdir[0]
                _size_y = _ydir[-1] - _ydir[0]
                _areas[_area_id].size = abs(_size_x), abs(_size_y), 1.0
                #
                #_local = _areas[_area_id].local_system
                #
                _coord_master = _areas[_area_id].mass
                #
                _sign_x = mean(_xdir) - _coord_master.x
                _sign_y = mean(_ydir) - _coord_master.y               
                #
                #_coord_x = _coord_master[1] + copysign(_size_x, _sign_x) / 2.0 
                #_coord_y = _coord_master[2] + copysign(_size_y, _sign_y) / 2.0
                #
                # x dir ---------------------------
                test1 = _xdir[0] - _coord_master[1]
                test2 = _xdir[-1] - _coord_master[1]
                #
                if copysign(_size_x, _sign_x) != copysign(_size_x, test1):
                    _coord_x = _xdir[0] + copysign(_size_x, _sign_x) / 2.0
                elif copysign(_size_x, _sign_x) != copysign(_size_x, test2):
                    _coord_x = _xdir[-1] + copysign(_size_x, _sign_x) / 2.0
                else:
                    _coord_x = _coord_master[1] + copysign(_size_x, _sign_x) / 2.0
                #
                # y dir ----------------------------
                test1 = _ydir[0] - _coord_master[2]
                test2 = _ydir[-1] - _coord_master[2]
                #
                if copysign(_size_y, _sign_y) != copysign(_size_y, test1):
                    _coord_y = _ydir[0] + copysign(_size_y, _sign_y) / 2.0
                elif copysign(_size_y, _sign_y) != copysign(_size_y, test2):
                    _coord_y = _ydir[-1] + copysign(_size_y, _sign_y) / 2.0
                else:
                    _coord_y = _coord_master[2] + copysign(_size_y, _sign_y) / 2.0               
                #
                _coord = [_coord_x, _coord_y, mean(_zdir)]
                #_coord = [mean(_xdir), mean(_ydir), mean(_zdir)]
                _areas[_area_id].mass = _area, _coord
            #
            elif 'SURFWT' in line.upper():
                """THE SURFACE WEIGHT RECORD ALLOWS THE DESCRIPTION OF NON-STRUCTURAL WEIGHTS DEFINED
                   BY PRESSURE LOADING ON DEFINED SURFACE AREAS"""
                #print('sufwt')
                _surf_name = line[6:10].strip() + '_WGT'
                try:
                    _surfwt[_surf_name]
                except KeyError:
                    _surfwt[_surf_name] = {'areas': [],
                                           'pressure':[],
                                           'weight_id':[],
                                           'density':[],
                                           'weigh_factors':[]}
                #
                _pressure = float(line[10:17].strip()) * (_force2/_area_factor**2)
                _weight_id = line[17:25].strip()
                try:
                    _density = float(line[25:32].strip()) * (_force2/_area_factor**3)
                except ValueError:
                    _density = None

                _weigh_factors = [line[32:36].strip(), line[36:40].strip(), line[40:44].strip()]
                _weigh_factors = [float(_item) if _item else 1 for _item in _weigh_factors]
                #
                _surf_areas = line[44:79].strip() 
                _step = 7
                _area_list = []
                for x in range(5):
                    _area_list.append(_surf_areas[x*_step: _step + x*_step].strip())
                _area_list = [x for x in _area_list if x]                

                for _item in _area_list:
                    _surfwt[_surf_name]['areas'].append(_item)
                    _surfwt[_surf_name]['pressure'].append(_pressure)
                    _surfwt[_surf_name]['weight_id'].append(_weight_id)
                    _surfwt[_surf_name]['density'].append(_density)
                    _surfwt[_surf_name]['weigh_factors'].append(_weigh_factors)
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    # 
    dat_temp.close()
    #
    # check if flood all members
    if global_flooded:
        for _memb in model.component.concepts.values():
            if not 'beam' in _memb.type:
                continue
            _memb.properties.flooded = True
    #
    # get non strucutral elements
    if _dummy:
        for key, _set in _dummy.items():
            for _node_name in _set['nonstructural']:
                _node = _nodes[_node_name]
                for _memb_name in _node.elements:
                    _memb = _members[_memb_name]
                    _memb.type = _memb.type + '_nonstructural'
                    _item_nonstr.append(_memb_name)
        #
        if _item_nonstr:
            _group = model.component.sets
            _group_name = '_nonstructural_'
            try: 
                _group[_group_name]
            except KeyError:
                _group_no = [_set.number for _set in _group.values()]
                _group_no = max(_group_no) + 1
                _group[_group_name] = f2u.Geometry.Groups(_group_name, 
                                                          _group_no)
            #
            _item_nonstr = list(set(_item_nonstr))
            _group[_group_name].concepts.extend(_item_nonstr)
            #print('-->')
    #
    # ---------------------------
    # update equipment
    _load_no = max(_load_no)
    if _areas:
        _area_del = []
        for _group_id,_items in _surfwt.items():
            #_load = _functional[_key]
            #if 'LLC1' in _group_id:
            #    print('-->')            
            for x, _item in enumerate(_items['areas']):
                _area_del.append(_item)
                _type = 'equipment'
                _area_no += 1
                _area_id = sacs_common.get_name(_item, prefix=_type)
                _area_id = _area_id # 'SFW_'+  
                _area_name = sacs_common.get_name(_items['weight_id'][x], prefix=_type)
                _area_name = load_title(_area_name)
                _area_id = _area_id +'_'+ _group_id +'_'+ _area_name
                _areas[_area_id] = f2u.Load.Equipment(name=_type + '_' + _area_id, 
                                                      number=_area_no)
                _areas[_area_id].type = _type
                #
                _coord_master = _areas[_item].mass
                _coord = [_coord_master.x, _coord_master.y, _coord_master.z]
                #
                _angle = _areas[_item].local_system[3]
                if _angle == 90:
                    _factor = _items['weigh_factors'][x][1]
                else:
                    _factor = _items['weigh_factors'][x][0]
                #
                _areas[_area_id].mass = (_coord_master.mass * _items['pressure'][x] * _factor) / 9810, _coord
                #
                _areas[_area_id].local_system = copy.deepcopy(_areas[_item].local_system)
                _areas[_area_id].tolerance[2] = copy.deepcopy(_areas[_item].tolerance[2])
                #_areas[_area_id].foot_print = copy.deepcopy(_areas[_item].foot_print)
                _areas[_area_id].size = copy.deepcopy(_areas[_item].size)
                #print('-->')
                try:
                    _functional[_group_id].areas.append(_area_id)
                except KeyError:
                    #try:
                    #    _number = int(_group_id)
                    #except ValueError:
                    #    _load_no += 1
                    #    _number = _load_no
                    _load_no += 1
                    _new_load_name = load_title(_group_id)
                    _functional[_group_id] = f2u.Load.Basic(_new_load_name, _load_no)
                    _functional[_group_id].areas.append(_area_id)
            #
            # remove duplicate main load
            #try:
            #    _functional[_group_id].areas.remove(_group_id)
            #except ValueError:
            #    pass
        #
        _area_del = list(set(_area_del))
        for _item in _area_del:
            del _areas[_item]
        #
        model.load.equipment.update(_areas)
    _areas = model.load.equipment
    #
    # check if data found, otherwise exit
    try:
        _length_factor
    except UnboundLocalError:
        os.remove('seaTemp.dat')
        print('    *** Warning : LDOPT card missing or file not found')
        return
    #
    # check defaults
    #
    _cdcm = sacs_meto.cdcm_default(_temp_cdcm)
    #_no_cdcm = 1
    #
    _sections_cdcm  = sacs_meto.set_cdcm(_cdcm, model)
    #
    _temp_cdcm ={}
    _mg_lev = []
    #_override_group = {}
    _mat_density = {}
    #
    print('    * Second pass')
    dat_temp = open('seaTemp2.dat','w+')
    #
    with open('seaTemp.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue            
            #
            if 'LOADCN' in  line.upper():
                """
                """
                _data = line[6:].split()
                _meto_name = _data[3].strip()
                _meto_number = int(_data[1])
                #
                dat_temp.write("\n")
                dat_temp.write(line)
            #
            elif line[0:6] == 'LOADLB':
            #    _load_name = line[6:10].strip()
            #    #model.load.functional[_load_name].name = load_title(line[10:])
            #    try:
            #        _seastate[_meto_name].name = load_title(line[10:])
            #    except KeyError:
                dat_temp.write("\n")
                dat_temp.write(line)
            #
            # TODO : this card needs more work
            elif 'MGROV' in line.upper():

                if not line[5:].strip():
                    continue
                #
                mg_factor = 0.0254
                if 'metre' in units:
                    mg_factor = 0.01
                # 
                # get marine growth mudline
                try:
                    mg_mudline = float(line[32:40]) * _length_factor
                except:
                    pass
                #
                # roughness
                try:
                    mw_roughness = float(line[40:48]) * mg_factor
                except ValueError:
                    mw_roughness = 0
                #
                # mg thickness
                try:
                    hmgrw_bottom = round(float(line[24:32].strip()) * mg_factor, 4)
                except ValueError:
                    test = line[24:32].strip()
                    test = re.sub(r'(\-|\+)', r'E\1', test)
                    hmgrw_bottom = round(float(test) * mg_factor, 4)

                # mg dry density
                try:
                    _density = round(float(line[48:56]) * _mass_factor / _length_factor**3, 3)
                except ValueError:
                    _density = 0
                #
                # get bottom of zone
                try:
                    _bottom = float(line[8:17].strip()) * _length_factor
                except ValueError:
                    #_elev =_elevation.mudline
                    _bottom = 0
                #
                # get top zone
                try:
                    _top = float(line[17:24].strip()) * _length_factor
                    hmgrw_top = hmgrw_bottom 
                except ValueError:
                    #print('   *** error : marine growth top zone missing')
                    #sys.exit()
                    _top = None
                    #hmgrw_top = 0.001
                #
                #_mg_lev.append([_top, hmgrw_top, _density])
                #_mg_lev.append([_bottom, hmgrw_bottom, _density])
                #
                _mgrowth_no = 1
                #
                #if not _top:
                    #_mgrowth[_mgrowth_no] = f2u.Metocean.MarineGrowth(_mgrowth_no, 
                    #                                                  _mgrowth_no)
                    #_mgrowth[_mgrowth_no].type = 'profile'
                    #_mgrowth[_mgrowth_no].absolute_elevations = True
                    #
                    #_mgrowth[_mgrowth_no].profile.append([_bottom, hmgrw_bottom, mw_roughness])
                    #_mgrowth[_mgrowth_no].profile.append([0, hmgrw_top, mw_roughness])
                    #
                    #_mgrowth[_mgrowth_no].density = _density
                #else:
                try:
                    _mgrowth[_mgrowth_no].profile
                    if _top:
                        _mgrowth[_mgrowth_no].profile.append([_bottom + 0.10, hmgrw_bottom, mw_roughness])
                        _mgrowth[_mgrowth_no].profile.append([_top, hmgrw_top, mw_roughness])
                        _mg_lev.append(_top)
                    else:
                        _mgrowth[_mgrowth_no].profile.append([_bottom, hmgrw_bottom, mw_roughness])
                        _mg_lev.append(_bottom)
                except KeyError:
                    _mgrowth[_mgrowth_no] = f2u.Metocean.MarineGrowth(_mgrowth_no, 
                                                                      _mgrowth_no)
                    _mgrowth[_mgrowth_no].type = 'profile'
                    _mgrowth[_mgrowth_no].absolute_elevations = True
                    #_mgrowth[_mgrowth_no].profile.append([_elev, hmgrw])
                    _mgrowth[_mgrowth_no].density = _density
                    _mgrowth[_mgrowth_no].profile.append([_bottom, hmgrw_bottom, mw_roughness])
                    _mg_lev.append(_bottom)
                    if _top:
                        _mgrowth[_mgrowth_no].profile.append([_top, hmgrw_top, mw_roughness])
                        _mg_lev.append(_top)
            #
            elif 'WAVE' in  line.upper():
                if not line[4:].strip():
                    continue
                _wave_type = wave_theory(line[8:12].strip())
                try:
                    _seastate[_meto_name]
                except KeyError:
                    _seastate[_meto_name] = f2u.Metocean.Combination(_meto_name, _meto_number)
                #
                if 'user_defined' in _wave_type:
                    print('---> fix user defined wave')
                else:
                    if 'repeat' in _wave_type :
                        _wave[_meto_name] = copy.deepcopy(_wave[_wave_repeat])
                    else:
                        _wave[_meto_name] = f2u.Metocean.Wave(_meto_name, 
                                                              _meto_number)
                        _wave[_meto_name].theory = _wave_type
                        _wave[_meto_name].phase = 0
                        try:
                            _seastate[_meto_name].wave_kinematics = float(line[4:8])
                        except ValueError:
                            _seastate[_meto_name].wave_kinematics = 1.0
                        #
                        _wave[_meto_name].height = float(line[12:18]) * _length_factor
                        #
                        # wave period
                        try:
                            _wave[_meto_name].period = float(line[24:30])
                        except ValueError:
                            # or wave length
                            _wave[_meto_name].length = float(line[30:38]) * _length_factor
                        #
                        # mudline elevation
                        try:
                            _water_depth = float(line[18:24]) * _length_factor
                            _wave[_meto_name].water_depth = _water_depth

                            if not _water_depth in _elevation.water_depth:
                                _elevation.water_depth.append(_water_depth)
                        except ValueError:
                            _wave[_meto_name].water_depth = _elevation.water_depth[0]
                        #
                        _wave_repeat = _meto_name
                    #
                    # wave order
                    if 'stream' in _wave_type:
                        try:
                            _wave[_meto_name].order = int(line[76:78])
                        except:
                            _wave[_meto_name].order = 8
                    #
                    # design load
                    _design_load = get_design_type(line[68:70])
                    if not _design_load:
                        _design_load = "BothLoads"
                    #
                    # update seasate
                    _seastate[_meto_name].wave = _wave[_meto_name]
                    try:
                        _seastate[_meto_name].wave_direction = float(line[38:44])
                    except ValueError:
                        _seastate[_meto_name].wave_direction = 0
                    _seastate[_meto_name].buoyancy = 'on'
                    _seastate[_meto_name].design_load = _design_load
                    #
            #
            elif 'GRPOV' in  line.upper():
                _group = line[15:18].strip()
                if not _group:
                    continue
                #
                try:
                    _groups[_group]
                except KeyError:
                    continue
                    #_override_group[_group]
                #
                # marine grwth options
                _grwth = line[18:19].strip()
                if _grwth:
                    if 'n' in _grwth.lower():
                        _grwth_option[_group] = _grwth
                    else:
                        print('fix marine grwth options')
                #
                # flooded options
                _flood_flag = line[19:20].strip()
                if _flood_flag:
                    #print('fix flooded options')
                    #try:
                    #    _flooded[_group]
                    #except KeyError:
                    #_group_no += 1
                    _flooded[_group] = _flood_flag 
                #
                #
                # material weight density
                _density = line[20:26].strip()
                if _density:
                    _density = round(float(_density) * _mass_factor / _length_factor**3, 4)
                    _mat_density[_group] = _density

                    #try:
                    #    _mat_density[_density].append(_group)
                    #except KeyError:
                    #    _mat_density[_density] = [_group]
                    #print('fix material weight density')
                #
                # non flooded buoyancy/disp area
                if line[33:40].strip():
                    _hydro_buoy_area[_group] = [float(line[33:40]) * _length**2, None]

                # cross section area flooded
                if line[26:33].strip():
                    try:
                        _hydro_buoy_area[_group][1] = float(line[33:40]) * _length**2
                    except KeyError:
                        _hydro_buoy_area[_group] = [None, float(line[33:40]) * _length**2]
                #
                #
                # dimension for forces
                # local y direction
                _dhydro = []
                if line[40:46].strip():
                    _dim = float(line[40:46]) * _length
                    _dhydro = [_dim, _dim]
                #
                # local z direction
                if line[46:52].strip():
                    _dim = float(line[46:52]) * _length
                    try:
                        _dhydro[1] = _dim
                    except KeyError:
                        _dhydro = [_dim, _dim]
                #
                if _dhydro:
                    _hydro_diameter[_group] = _dhydro
                #
                #
                # cdcm section
                #
                _data = line[52:].strip()
                if _data:
                    _name = line[52:76].strip()
                    _name = _name.replace(" ", "_")
                    try:
                        _temp_cdcm[_name]['sets'].append(_group)
                    except KeyError:
                        sacs_meto.get_cdcm_overrides(_name, _temp_cdcm, line)
                        _temp_cdcm[_name]['sets'].append(_group)
                    #
                    #print('here')                
                #print('here')
            #
            elif 'WINSHL' in  line.upper():
                _step = 8
                _shiel = line[10:74]
                try:
                    for x in range(0, 8, 2):
                        _botom = float(_shiel[x*_step: 8 + x*_step]) * _length_factor
                        _top = float(_shiel[8 + x*_step: 16 + x*_step]) * _length_factor
                        _wind_shield.append([_botom, _top])
                except ValueError:
                    continue
                #
                #print('---> winshield')
            #
            # mass section groups
            #
            elif 'WGTJT' in line.upper():
                """
                The added joint weight record allows the description of
                nonstructural weights attached to a single joint 
                """
                _group_id = line[6:10].strip() + '_WGTJ'
                #
                #
                #try:
                #    _wgt_sets[_group_id]
                #except KeyError:
                #    _wgt_sets[_group_id] = {'node':[],
                #                            'concept':[],
                #                            'areas':[]}
                #
                #if 'HLDK' in _group_id:
                #    print('-->')
                _weight = float(line[10:17].strip()) * _mass_factor
                _weight_id = line[17:25].strip()
                _joint_id = line[25:29].strip()
                # Radio of gyration
                _mass = [0, 0, 0, 0, 0, 0]
                try:
                    _mass[3] = float(line[36:42].strip()) * _mass_factor
                    _mass[4] = float(line[42:48].strip()) * _mass_factor
                    _mass[5] = float(line[48:54].strip()) * _mass_factor
                    #print(' include rad of gyration in WGTJT')
                except ValueError:
                    pass
                # weigth factors
                _mass[0] = float(line[54:59].strip()) * _weight
                _mass[1] = float(line[59:64].strip()) * _weight
                _mass[2] = float(line[64:69].strip()) * _weight * -1.0
                #
                _node = _nodes[_joint_id]
                try:
                    _node = [_node.mass[i] + _mass[i] for i in range(6)]
                    #_node.mass[2] += _mass[2]
                except IndexError:
                    _node.mass = _mass
                    try:
                        _groups[_group_id].nodes
                    except KeyError:
                        _group_no += 1
                        _groups[_group_id] = f2u.Geometry.Groups(_group_id, 2)
                    _groups[_group_id].nodes.append(_joint_id)
            #
            elif 'WGTMEM' in line.upper():
                """
                The distributed member weight record allows the description
                of member distributed weights
                """
                try:
                    _memb_name = line[10:14].strip() + '_' + line[14:18].strip()
                    _mem = _members[_memb_name]
                    _end_1 = 0
                    _end_2 = 1
                except KeyError:
                    try:
                        _memb_name = line[14:18].strip() + '_' + line[10:14].strip()
                        _mem = _members[_memb_name]
                        _end_1 = 1
                        _end_2 = 0
                    except KeyError:
                        print("    ** Warining member {:} no found".format(_memb_name))
                        continue
                #
                _group_id = line[6:10].strip() + '_WGTM'
                _weight_id = line[73:80].strip()
                #
                _comment = line[72:80].strip()
                if _comment:
                    _comment = '_' + _comment
                #else:
                _comment =  _group_id + _comment + '_' + _weight_id                
                #
                _L = [0, 0]
                #_loadlist = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
                #
                # weigth factors
                _mass = [0, 0, 0]
                _mass[0] = float(line[46:50].strip()) #* _weight
                _mass[1] = float(line[50:54].strip()) #* _weight
                _mass[2] = float(line[54:58].strip()) * -1.0 #*_weight           
                #
                Lt = _mem.length_node2node
                _weight_type = line[62:66].strip()
                if 'CONC' in _weight_type.upper():
                    _weight = float(line[26:32].strip()) * _mass_factor
                    #print('contrated')
                    _distance = float(line[18:25].strip()) *  _length_factor
                    if _end_1 == 0:
                        _L[0] =  _distance
                    else:
                        _L[0] = _mem.length_node2node - _distance
                    #
                    mass_load = []
                    for factor in _mass:
                        mass_load.append(point_beam(P=factor*_weight, L=Lt, L1=_L[0]))
                else:
                    _weight1 = float(line[26:32].strip()) * _mass_factor
                    _weight2 = float(line[40:46].strip()) * _mass_factor
                    #print('uniform')
                    # end 1
                    try:
                        _L[_end_1] = float(line[18:25].strip()) *  _length_factor
                    except ValueError:
                        pass
                    #
                    # end 2
                    try:
                        _distance = float(line[32:39].strip()) *  _length_factor
                        #_length2node = (_mem.length_node2node - _L[_end_1] - _distance)
                        _L[_end_2] = _distance
                    except ValueError:
                        pass
                    #
                    mass_load = []
                    for factor in _mass:
                        mass_load.append(trapz_beam(w1=_weight1*factor, w2=_weight2*factor,
                                                    L=Lt, L1=_L[0], L2=_L[1]))                
                #
                #m1, m2
                for i in range(len(mass_load)):
                    #_mem.node[0].mass[i] += mass_load[i][0]
                    _group_no = updte_mass_node(i=i, node=_mem.node[0], 
                                                mass_load=mass_load[i][0],
                                                groups=_groups,
                                                group_id=_group_id,
                                                group_no=_group_no)
                    #
                    _group_no = updte_mass_node(i=i, node=_mem.node[1], 
                                                mass_load=mass_load[i][1],
                                                groups=_groups,
                                                group_id=_group_id,
                                                group_no=_group_no)
                # various
                _system = 'GLOB'
                try:
                    _system = float(line[58:62].strip())
                except ValueError:
                    pass
                
                if not 'GLOB' in _system.upper():
                    print(' include local system in WGTMEM')
                
                try:
                    _density = float(line[66:72].strip())
                    try:
                        _density_sets[_density].append(_memb_name)
                    except KeyError:
                        _density_sets[_density] = [_memb_name]
                    #print(' include density in WGTMEM')
                except ValueError:
                    pass                
            #
            elif 'WGTFP' in line.upper():
                """
                WGTFP:
                The footprint weight record allows the description of nonstructural weights
                where the load and mass is distributed to members coinciding with the foot
                print location.
                
                WGTFP2:
                The additional footprint weight record allows the description of weight
                factors, radii of gyration, density and orientation of the preceding 
                footprint weight line.
                """
                if 'WGTFP2' in line.upper():
                    _mass = _areas[_weight_id].mass
                    # weigth factors
                    _weigh_factors = [1, 1, 1]
                    try:
                        _weigh_factors[0] = float(line[10:14].strip()) * _mass[0]
                    except ValueError:
                        pass
                    try:
                        _weigh_factors[1] = float(line[14:18].strip()) * _mass[0]
                    except ValueError:
                        pass
                    try:
                        _weigh_factors[2] = float(line[18:22].strip()) * _mass[0]
                    except ValueError:
                        pass                    
                    _areas[_weight_id].mass = _weigh_factors[2], _mass[1:]
                    # tolerance
                    try:
                        _tol= float(line[22:26].strip()) * _area_factor
                        _areas[_weight_id].tolerance[2] = _tol
                        #
                        _coord_z = _centre_footprint[2]
                        _range = {key: _item for key, _item in _node_stats.items() 
                                  if isclose(key, _coord_z, abs_tol=_tol)}
                        _coord_z = max(_range, key=_range.get)
                        _centre_footprint[2] = _coord_z
                        nodes_out = sacs_process.get_nodesXarea(_nodes, _foot_area,
                                                                _centre_footprint)
                        _areas[_weight_id].joints = nodes_out
                    except ValueError:
                        pass                    
                    # radii of gyration
                    _rad_gyration = [0, 0, 0]
                    try:
                        _rad_gyration[0] = float(line[27:32].strip())
                        _rad_gyration[1] = float(line[32:37].strip())
                        _rad_gyration[2] = float(line[37:42].strip())
                        print(' include rad of gyration in WGTFP2')
                    except ValueError:
                        pass
                    # various
                    try:
                        _density = float(line[42:48].strip())
                        print(' include density in WGTFP2')
                    except ValueError:
                        pass
                    
                    try:
                        _angle = float(line[48:54].strip())
                        print(' include orientation angle in WGTFP2')
                        _areas[_weight_id].local_system = [0, 0, 0, _angle] 
                    except ValueError:
                        pass                    
                else: # WGTFP
                    _group_id = line[6:10].strip() + '_WGTFP'
                    _weight = float(line[10:17].strip()) * _force_factor / 9810
                    _weight_id = line[17:25].strip()
                    #
                    #try:
                    #    _wgt_sets[_group_id]
                    #except KeyError:
                    #    _wgt_sets[_group_id] = {'node':[],
                    #                            'concept':[],
                    #                            'areas':[]}
                    #
                    #if 'UDK-PIPG' in _weight_id:
                    #    print('stop')
                    #
                    # Footprint centre
                    _footprint_id = line[25:29].strip()
                    _coord_x = get_coordinate(line[29:35], _area_factor)
                    _coord_y = get_coordinate(line[35:41], _area_factor)
                    _coord_z = get_coordinate(line[41:47], _area_factor)
                    #
                    _range = {key: _item for key, _item in _node_stats.items() 
                              if isclose(key, _coord_z, abs_tol=0.1524)}
                    _coord_z = max(_range, key=_range.get)
                    #
                    _centre_footprint = [_coord_x, _coord_y, _coord_z]
                    # footprint size
                    _length = get_coordinate(line[66:71], _area_factor)
                    _width =  get_coordinate(line[71:76], _area_factor)
                    _foot_area = [_length, _width]
                    nodes_out = sacs_process.get_nodesXarea(_nodes, _foot_area,
                                                            _centre_footprint)
                    #
                    # Weight coordinates
                    _option_centre = line[47:48].strip()
                    _coord_x = get_coordinate(line[48:54], _area_factor)
                    _coord_y = get_coordinate(line[54:60], _area_factor)
                    _coord_z = get_coordinate(line[60:66], _area_factor)
                    _centre_weight = [_coord_x, _coord_y, _coord_z]
                    _CoG = [_centre_weight[x] + _centre_footprint[x]
                            for x in range(3)]
                    #
                    if 'R' in _option_centre.upper():
                        _centre_weight = _CoG
                    #
                    _height = round(2 * abs(_CoG[2]), 2)
                    if _height < 1.0: 
                        _height = 1.0
                    #
                    _skid_beams = [0, 0]
                    try:
                        _skid_beams[0] = int(line[76:78].strip())
                    except ValueError:
                        pass
                    
                    try:
                        _skid_beams[1] = int(line[78:80].strip())
                    except ValueError:
                        pass
                    #
                    #
                    _type = 'equipment'
                    _area_no += 1
                    _area_id = load_title(_group_id)
                    if not _weight_id:
                        _weight_id = 'area_' + str(_area_no)
                    _weight_id = _weight_id + '_' + _area_id # 'SFW_'+  
                    _area_name = load_title(_weight_id)
                    _areas[_weight_id] = f2u.Load.Equipment(name=_type + '_' + _area_name, # 
                                                            number=_area_no)
                    #_areas[_group_id].name = 'wind_area_'+_area_name
                    _areas[_weight_id].type = _type
                    _areas[_weight_id].size = _length, _width, _height
                    _areas[_weight_id].mass = _weight, _centre_weight
                    if sum(_skid_beams) != 0:
                        _areas[_weight_id].foot_print = _skid_beams
                    _areas[_weight_id].joints = nodes_out
                    _areas[_weight_id].local_system = [_CoG[0], _CoG[1], _CoG[2], 0]                    
                    #
                    model.load.equipment[_weight_id] = _areas[_weight_id]
                    #
                    #_wgt_sets[_group_id]['areas'].append(_weight_id)
                    #
                    try:
                        _functional[_group_id].areas.append(_weight_id)
                    except KeyError:
                        _load_no += 1
                        _new_load_name = load_title(_group_id)
                        _functional[_group_id] = f2u.Load.Basic(_new_load_name, _load_no)
                        _functional[_group_id].areas.append(_weight_id)
                    #_functional[_group_id].node.extend(nodes_out)
            #
            elif 'DEAD' in line[0:4]:
                _location = line[10:12].strip()
                if not _location:
                    continue
                
                _grav = get_gravity(_location)
                
                try:
                    _functional[_meto_name]
                except KeyError:
                    _load_no += 1
                    model.load.functional[_meto_name] = f2u.Load.Basic('gravity', _load_no)
                
                model.load.functional[_meto_name].gravity.extend(_grav)             
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    os.remove('seaTemp.dat')
    #
    # ---------------------------
    # update hydro diameter
    #
    i = 0
    for key, _set in  _hydro_diameter.items():
        _mean = mean(_set)
        try:
            _metocean.hydro_diameter[_mean]
        except KeyError:
            i += 1
            _metocean.hydro_diameter[_mean] = f2u.Metocean.Parameters(i, i)
            _metocean.hydro_diameter[_mean].coefficient = _mean
            _metocean.hydro_diameter[_mean].type = 'hydrodynamic_diameter'
        #
        _metocean.hydro_diameter[_mean].items.extend(_groups[key].concepts)
        #
        for _item in _groups[key].concepts:
            try:
                _memb = model.component.concepts[_item]
                _memb.properties.hydrodynamic_diameter = _metocean.hydro_diameter[_mean]
            except KeyError:
                _groups[key].concepts.remove(_item)
    #
    #
    # ---------------------------
    # update hydro buoyancy area

    i = len(_metocean.buoyancy_area)
    for key, _set in _hydro_buoy_area.items():
        try:
            _mean = mean(_set)
        except TypeError:
            if _set[0]:
                _mean = _set[0]
            else:
                _mean = _set[1]

        try:
            _metocean.buoyancy_area[_mean]
        except KeyError:
            i += 1
            _metocean.buoyancy_area[_mean] = f2u.Metocean.Parameters(i, i)
            _metocean.buoyancy_area[_mean].coefficient = _set
            _metocean.buoyancy_area[_mean].type = 'buoyancy_area'
        #
        _metocean.buoyancy_area[_mean].items.extend(_groups[key].concepts)
        #
        for _item in _groups[key].concepts:
            _memb = model.component.concepts[_item]
            if not 'beam' in _memb.type:
                continue
            _memb.properties.buoyancy_area = _metocean.buoyancy_area[_mean]
    #
    # ---------------------------
    # update material density    
    if _mat_density:
        _material = model.component.materials
        for _set_name, _density in _mat_density.items():
            _group = model.component.sets[_set_name]             

            for _memb_name in _group.concepts:
                _memb = model.component.concepts[_memb_name]
                for x in range(len(_memb.material)):
                    _mat_name = _memb.material[x].name
                    _index = _mat_name.rfind('x')
                    _new_name = str(_density).replace('.', '_')
                    _new_name = _mat_name[:_index] + 'x' + _new_name.zfill(4)
                    try:
                        _material[_new_name]
                    except KeyError:
                        _material[_new_name] = copy.deepcopy(_material[_mat_name])
                        _material[_new_name].density = _density

                    _memb.material[x] = _material[_new_name]
    #
    # ---------------------------
    # update marine growth
    try:
        _metocean.marine_growth['no_marine_growth']
    except KeyError:
        _metocean.marine_growth['no_marine_growth'] = f2u.Metocean.MarineGrowth('no_marine_growth', 0)
        _metocean.marine_growth['no_marine_growth'].type = 'specified'
        _metocean.marine_growth['no_marine_growth'].inertia = False
        _metocean.marine_growth['no_marine_growth'].constant(thickness=0, roughness=0)
    #
    for key, _set in _grwth_option.items():
        if 'n' in _set.lower():
            _mgrowth_name = 'no_marine_growth'
        else:
            print(' --> marine growth option??')
            1/0

        for _item in _groups[key].concepts:
            _memb = model.component.concepts[_item]
            if not 'beam' in _memb.type:
                continue            
            _memb.properties.marine_growth = _metocean.marine_growth[_mgrowth_name]
    #
    # ---------------------------
    # update flooded 
    #
    for key, _set in _flooded.items():
        if 'f' in _set.lower():
            try:
                model.component.sets['_flooded_']
            except KeyError:
                model.component.sets['_flooded_'] = f2u.Geometry.Groups('_flooded_')

            model.component.sets['_flooded_'].concepts.extend(_groups[key].concepts)

            for _item in _groups[key].concepts:
                _memb = model.component.concepts[_item]
                if not 'beam' in _memb.type:
                    continue                
                _memb.properties.flooded = True
        else:
            for _item in _groups[key].concepts:
                _memb = model.component.concepts[_item]
                if not 'beam' in _memb.type:
                    continue                
                _memb.properties.flooded = False            
    #
    #
    _wind_drag = ''
    _curr_dir = 0
    _wind_dir = 0
    #
    print('    * Third pass')
    dat_temp = open('seaTemp3.dat','w+')
    #
    with open('seaTemp2.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue            
            #
            if 'LOADCN' in  line.upper():
                """
                """
                _data = line[6:].split()
                _meto_name = _data[3].strip()
                _meto_number = int(_data[1])
                #
                dat_temp.write("\n")
                dat_temp.write(line)               
            #
            elif 'MEMOV' in  line.upper():
                _data = line[5:].strip()
                if not _data:
                    continue

                node1 = (line[7:11].strip())
                node2 = (line[11:15].strip())
                _memb = sacs_process.find_member(model.component.concepts, 
                                                 node1, node2)

                if not _memb:
                    continue
                #
                # marine grwth options
                _grwth = line[18:19].strip()
                if _grwth:
                    if 'n' in _grwth.lower():
                        _memb.properties.marine_growth = _metocean.marine_growth['no_marine_growth']
                    else:
                        print('fix marine grwth options')
                #
                # flooded options
                _flood_flag = line[19:20].strip()
                if _flood_flag :
                    if 'f' in _flood_flag.lower():
                        _memb.properties.flooded = True
                    else:
                        _memb.properties.flooded = False
                    #print('fix flooded options')
                #
                # material weight density
                if line[20:26].strip():
                    print('fix material weight density')
                #
                # cross section area
                if line[26:33].strip():
                    print('fix cross section area')
                #
                # non flooded buoyancy/disp area
                if line[33:40].strip():
                    print('fix non flooded buoyancy')
                #
                #
                # dimension for forces
                # local y direction
                _dhydro = []
                if line[40:46].strip():
                    _dim = float(line[40:46]) * _length
                    _dhydro = [_dim, _dim]
                #
                # local z direction
                if line[46:52].strip():
                    _dim = float(line[46:52]) * _length
                    try:
                        _dhydro[1] = _dim
                    except KeyError:
                        _dhydro = [_dim, _dim]
                # need to solve this
                if _dhydro:
                    _new_diameter = (_dhydro[0]**2 + _dhydro[1]**2)**0.50
                    _memb.properties.hydrodynamic_diameter = _new_diameter
                #                
                # cdcm section
                #
                _data = line[52:].strip()
                if _data:
                    _name = line[52:76].strip()
                    _name = _name.replace(" ", "_")
                    try:
                        _temp_cdcm[_name]['items'].append(_memb.name)
                    except KeyError:
                        sacs_meto.get_cdcm_overrides(_name, _temp_cdcm, line)
                        _temp_cdcm[_name]['items'].append(_memb.name)
                    #
                    #print('here')
            #
            elif line[0:4] == 'CURR' :
                _data = line[4:].strip()
                if not _data:
                    continue
                #
                #if "601" in _meto_name:
                #    print("-->")
                #
                try:
                    _seastate[_meto_name]
                except KeyError:
                    _seastate[_meto_name] = f2u.Metocean.Combination(_meto_name, _meto_number)
                #
                # assume only current direction defined on this line
                if not line[8:16].strip() and not line[16:24].strip() : # and line[24:32].strip()
                    _current[_meto_name] = f2u.Metocean.Current(_meto_name, 
                                                                _meto_number)
                                       
                    _seastate[_meto_name].current = _current[_meto_name]

                    try:
                        _curr_dir = float(line[24:32])
                    except ValueError:
                        pass
                    _seastate[_meto_name].current_direction = _curr_dir

                    try:
                        _seastate[_meto_name].current_blockage = float(line[40:48])
                    except ValueError:
                        _seastate[_meto_name].current_blockage = 1.0

                    if line[59:61].strip() != 'CN':
                        _seastate[_meto_name].current_stretching = True

                    continue
                #
                #
                #_cfactor = 0.514444
                #if "M" in _units_in.upper():
                #    _cfactor = 1
                try:
                    _z = float(line[8:16]) * _length_factor
                except ValueError:
                    _z = 0.0

                try:
                    _v = float(line[16:24]) * _length_factor
                except ValueError:
                    try:
                        _v = _current[_meto_name].profile[-1][1]
                    except IndexError:
                        _v = 0
                    except KeyError:
                        _v = 0

                try:
                    _current[_meto_name].profile.append([_z, _v])
                except KeyError:
                    _current[_meto_name] = f2u.Metocean.Current(_meto_name, 
                                                                _meto_number)
                    _current[_meto_name].profile.append([_z, _v])
                    #
                    _seastate[_meto_name].current = _current[_meto_name]

                    try:
                        _curr_dir = float(line[24:32])
                    except ValueError:
                        pass
                    _seastate[_meto_name].current_direction = _curr_dir

                    try:
                        _seastate[_meto_name].current_blockage = float(line[41:48])
                    except ValueError:
                        _seastate[_meto_name].current_blockage = 1.0

                    if line[59:61].strip() != 'CN':
                        _seastate[_meto_name].current_stretching = True
                    #
                #
                #
                try:
                    mudline = abs(float(line[32:40]) * _length_factor)
                    if not mudline in _elevation.water_depth:
                        _elevation.water_depth.append(mudline)                    
                    #print('Fix current mudline')
                except ValueError:
                    mudline = _elevation.water_depth[0]
                #
                if not _seastate[_meto_name].wave:
                    # add calm sea dummy wave
                    _wave[_meto_name] = f2u.Metocean.Wave(_meto_name, _meto_number)
                    _wave[_meto_name].theory = 'calm_sea'
                    _wave[_meto_name].water_depth = mudline
                    _seastate[_meto_name].wave = _wave[_meto_name]
            #
            elif line[0:4] == 'WIND' :
                _vel = line[8:61]
                if not _vel:
                    continue
                #
                _wind_drag = line[6:7].strip()
                _w_units = line[7:8].strip()
                _w_length = get_wind_units(_units_in, _w_units,
                                           _length_factor)
                #
                _w_type = 'extreme'
                if line[40:44].strip():
                    _w_type = wind_theory(line[40:44].strip())

                try:
                    _wind[_meto_name]
                except KeyError:
                    _wind[_meto_name] = f2u.Metocean.Wind(_meto_name, _meto_number)
                #
                _wind[_meto_name].formula = _w_type
                #
                # get velocity, height, pressure or gus factor
                try:
                    _wind[_meto_name].velocity = float(line[8:16]) * _w_length
                    if line[16:24].strip():
                        if 'api21' in _w_type:
                            #_wind[_meto_name].duration = float(line[16:24])
                            _wind[_meto_name].period_ratio = float(line[16:24])
                        elif 'abs' in _w_type:
                            _wind[_meto_name].power = float(line[16:24])
                        else:
                            _wind[_meto_name].height = float(line[16:24]) * _length_factor
                except ValueError:
                    _wind[_meto_name].pressure = (float(line[16:24]) * _force_factor 
                                                  / (_length_factor**2 * 1000.00))
                #
                try:
                    _seastate[_meto_name]
                except KeyError:
                    _seastate[_meto_name] = f2u.Metocean.Combination(_meto_name, _meto_number)                
                #
                _seastate[_meto_name].wind = _wind[_meto_name]
                #_seastate[_meto_name].wind_direction = 'x axis'
                try:
                    _wind_dir = float(line[24:32])
                except ValueError:
                    _wind_dir = 0
                _seastate[_meto_name].wind_direction = _wind_dir
                #
                # TODO : fix this
                #_seastate[_meto_name].wind.period_ratio = 1
                #
                # -------
                #
                try:
                    _water_depth = float(line[32:40]) * _length_factor
                    if not _water_depth in _elevation.water_depth:
                        _elevation.water_depth.append(_water_depth)
                except ValueError:
                    _water_depth = _elevation.water_depth[0]
                #
                # -------
                # wind areas
                _area_wind_flag = False
                _area_data = line[44:80].strip()
                if _area_data and _areas:
                    _area_list = []
                    _step = 2
                    for x in range(0, 36, _step):
                        _area_list.append(_area_data[x: _step + x].strip())

                    _area_list = [x for x in _area_list if x]
                    for _item in _area_list:
                        for _area_name, _area in _areas.items():
                            _area_sacs = _area_name.split('_')[0]
                            if _item == _area_sacs:
                            #try:
                                if _area.type == 'wind_area':
                                    _seastate[_meto_name].wind_areas.append(_area)
                                    _area_wind_flag = True
                                else:
                                    print('    ** Warning wind area type: {:} not implemented'
                                          .format(_area.type))
                            #except KeyError:
                            #    print('    ** Warning wind area {:} not found'.format(_item))

                elif _areas and not _area_data:
                    for _area_name, _area in _areas.items():
                        if 'default' in _area_name and _area.type == 'wind_area':
                            _seastate[_meto_name].wind_areas.append(_area)
                            _area_wind_flag = True
                #
                if _area_wind_flag :
                    try:
                        _functional[_meto_name]
                    except KeyError:
                        _load_no += 1
                        _new_load_name = load_title(_meto_name)
                        _functional[_meto_name] = f2u.Load.Basic(_new_load_name, _load_no)
                        _functional[_meto_name].class_type = "metocean"
                #
                if _seastate[_meto_name].wave:
                    #print('fix here wind wave')
                    continue
                else:
                    # add calm sea dummy wave
                    _wave[_meto_name] = f2u.Metocean.Wave(_meto_name, _meto_number)
                    _wave[_meto_name].theory = 'calm_sea'
                    _wave[_meto_name].water_depth = _water_depth
                    #
                    _seastate[_meto_name].wave = _wave[_meto_name]
                    _seastate[_meto_name].design_load = "MaxBaseShear"
            #
            elif 'DRAG' in  line.upper():
                print(' --> fix drag')
                _area_data = line[44:80].strip()
                if _area_data and _areas:
                    _area_list = []
                    _step = 2
                    for x in range(0, 18, 2):
                        _area_list.append(_area_data[x*_step: _step + x*_step].strip())

                    _area_list = [x for x in _node_list if x]
                    for _item in _area_list:
                        for _area_name, _area in _areas.items():
                            _area_sacs = _area_name.split('_')[0]
                            if _item == _area_sacs:
                                if _area.type != 'wind_area':
                                    print(' ---> ?', _item)
                                else:
                                    print('    ** Warning wind area type: {:} not implemented'
                                          .format(_area.type))

                elif _areas and not _area_data:
                    for key, _area in _areas.items():
                        if 'default' in key and _area.type != 'wind_area':
                            print(' ---> ?', _item)                   

            #
            elif 'INCWGT' in  line.upper():
                """THE WEIGHT SELECTION RECORD ALLOWS THE SELECTION OF WEIGHT GROUPS TO BE
                   INCLUDED IN SPECIFIC LOAD CASES"""              
                #
                #if 'LLC1' in _meto_name:
                #    print('-->')
                _list = line[8:80].strip()
                _step = 4
                _node_list = []
                for x in range(18):
                    _node_list.append(_list[x*_step: _step + x*_step].strip())
                _node_list = [x for x in _node_list if x]                
                #
                #_functional[_meto_name].areas = _node_list
                _comb_list = []
                for _set in _node_list:
                    try:
                        _wgt_set = _wgt_sets[_set + "_WGT"]
                        #
                        _new_name = _meto_name + "_WGT"
                        #
                        try:
                            _functional[_new_name]
                        except KeyError:
                            _load_no += 1
                            _new_load_name = load_title(_new_name)
                            _functional[_new_name] = f2u.Load.Basic(_new_load_name, _load_no)                         
                        #
                        for _item in _wgt_set['node']:
                            _functional[_new_name].node.append(_item)
                        for _item in _wgt_set['concept']:
                            _functional[_new_name].concept.append(_item)
                        for _item in _wgt_set['areas']:
                            _functional[_new_name].areas.append(_item)
                    except KeyError:
                        _comb_list.append(_set)
                # probably a combination
                if _comb_list:
                    dat_temp.write("\n")
                    dat_temp.write("INCWGT {:}".format(_meto_name))                     
                    for _item in _comb_list:                       
                        dat_temp.write(" {:} 1.".format(_item))
                #dat_temp.write(line)                
            #
            elif 'ACCEL' in  line.upper():
                """THIS LINE IS USED TO SPECIFY THE ANGULAR AND TRANSLATIONAL COMPONENTS OF THE MODEL'S
                   ACCELERATION ABOUT THE GLOBAL AXES. THE ANGULAR VELOCITIES ARE USED IN CALCULATING
                   THE CENTRIPETAL ACCELERATIONS"""
                #
                _list = line[9:72].strip()
                _acc_trans = [line[9:16].strip(), line[16:23].strip(), line[23:30].strip()]
                _acc_trans = [float(_item) * -9.810 if _item else 0 for _item in _acc_trans]
                #
                _acc_rot = [line[30:37].strip(), line[37:44].strip(), line[44:51].strip()]
                _acc_rot = [float(_item) if _item else 0 for _item in _acc_rot]
                #
                _vel_rot = [line[51:58].strip(), line[58:65].strip(), line[65:72].strip()]
                _vel_rot = [float(_item) if _item else 0 for _item in _vel_rot]
                #
                if sum(_acc_trans) != 0 :
                    if line[74:75].strip() != 'N' and _acc_trans[2] == 0:
                        _acc_trans[2] = -9.810
                    
                    try:
                        _functional[_meto_name]
                    except KeyError:
                        _load_no += 1
                        model.load.functional[_meto_name] = f2u.Load.Basic('rotation_field', _load_no)
                    
                    if any(_acc_trans):
                        model.load.functional[_meto_name].acceleration.extend(_acc_trans)
                #
                if any(_acc_rot) or any(_vel_rot):
                    print(' fix rotations field')
            else:
                dat_temp.write("\n")
                dat_temp.write(line)            
    #
    dat_temp.close()
    os.remove('seaTemp2.dat')
    #
    _checkme_file = 'CheckMe_' + sea_file.replace('CheckMe_', '')
    dat_temp = open(_checkme_file,'w+')
    print('    * Fourth pass')
    with open('seaTemp3.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue
            
            elif line[0:6] == 'LOADLB':
                _load_name = line[6:10].strip()
                try:
                    _seastate[_load_name].name = load_title(line[10:])
                except KeyError:
                    model.load.functional[_load_name].name = load_title(line[10:]) +'_'+ str(_load_name)
                
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    os.remove('seaTemp3.dat')    
    #
    print('--- End Reading {:} file'.format(file_name))
    #
    #
    #
    # update current
    if _current:
        # normalize velocity profile
        for _curr in _current.values():
            _vel = [_profile[1] for _profile in _curr.profile]
            _curr.velocity = max(_vel)
            _curr.profile = [[(_profile[0] + _elevation.mudline),
                              _profile[1]/_curr.velocity] 
                             for _profile in reversed(_curr.profile)]
            #
            _curr.profile.insert(0, [2*abs(_elevation.mudline), 1.0])
            #print('hh')
        #
        try:
            _metocean.current.update(_current)
        except AttributeError:
            _metocean.current = _current
    #
    # update wave
    if _wave:
        try:
            _metocean.wave.update(_wave)
        except AttributeError:
            _metocean.wave = _wave
    #
    # update wind
    if _wind:
        try:
            _metocean.wind.update(_wind)
        except AttributeError:
            _metocean.wind = _wind

        #if _wind_items:
        #    _metocean.wind.items.extend(_wind_items)
    #
    #
    # ---------------------------
    # update cd & cm
    #
    #
    if _mg_lev:
        _mg_lev = max(_mg_lev)
        _surface = round(_elevation.mudline + _mg_lev, 2)
    else:
        _surface = _elevation.surface

    _air_drag = {}
    if _temp_cdcm:
        sacs_meto.update_cdcm(model, _surface, _temp_cdcm, _sections_cdcm, 
                              _cdcm, _wind_drag, _air_drag, _wind_shield)
    #
    # TODO : complete this section
    # update member hydro properties
    for key, _memb in model.component.concepts.items():
        if not 'beam' in _memb.type:
            continue
        # check if beam element
        try:
            _memb.properties.cdcm
        except AttributeError:
            _memb.properties.cdcm = _cdcm['default']
    #
    # update air drag
    if _wind and _elevation and _wind_drag.lower() != 'i':
        _surface = _elevation.surface
        _wind_cd = sacs_meto.update_air_drag(model, _surface, _sections_cdcm,
                                             _wind_drag, _air_drag, _wind_shield)
        #
        try:
            _metocean.air_drag.update(_wind_cd)
        except AttributeError:
            _metocean.air_drag = _wind_cd
    #
    #
    #if _cdcm:
    try:
        _metocean.cdcm.update(_cdcm)
    except AttributeError:
        _metocean.cdcm = _cdcm
    #
    # update marine g
    #MG = {}
    if _mgrowth :
        try:
            _metocean.marine_growth.update(_mgrowth)
        except AttributeError:
            _metocean.marine_growth = _mgrowth
    #
    # 
    #if model.component.sets['nohydro'].concepts:
    try:
        _metocean.non_hydro = model.component.sets['nohydro']
    except KeyError:
        _metocean.non_hydro = {}
    #
    #
    try:
        #if model.component.sets['_flooded_'].concepts:
        _metocean.flooded = {'_flooded' : model.component.sets['_flooded_']}
    except KeyError:
        _metocean.flooded = {}
    #
    # ---------------------------
    # update flooded members
    #
    #sacs_meto.update_flooded_members(_metocean, model)
    #
    _materials = model.component.materials
    _mat_no = len(_materials)
    for key, _items in _density_sets.items():
        for _memb_name in _items:
            _member = _members[_memb_name]
            _stress_factor, _density_factor = sacs_common.material_units(_units_in)
            _density = float(key) * _density_factor
            #
            #_new_mat = []
            for x, _material in enumerate(_member.material):
                #_material = _member.material[0]
                _Emod = _material.E
                _Gmod = _material.G
                _Fy = _material.Fy
                _density_in = _material.density + _density
                _units_test = 'SI'
                _mat_name = sacs_common.get_material(_Emod, _Gmod, _Fy, _density_in, 
                                                     _units_test, _materials, _mat_no)
                #_new_mat.append(_mat_name)
                _member.material[x] = _materials[_mat_name]
    #
    #for key, _wgt_set in _wgt_sets.items():
    #    try:
    #        _functional[key]
    #    except KeyError:
    #        _load_no += 1
    #        _new_load_name = load_title(key)
    #        _functional[key] = f2u.Load.Basic(_new_load_name, _load_no)
    #        #
    #        #for _set in _node_list:
    #            #_wgt_set = _wgt_sets[_set]
    #        for _item in _wgt_set['node']:
    #            _functional[key].node.append(_item)
    #        for _item in _wgt_set['concept']:
    #            _functional[key].concept.append(_item)
    #        for _item in _wgt_set['areas']:
    #            _functional[key].areas.append(_item)
    #
    #
    _tobedeleted = []
    for key, _sea in _seastate.items():
        if _sea.wind or _sea.wave or _sea.current:
            try:
                _basic = _functional[key]
                if (_basic.concept or _basic.element or _basic.node 
                    or _basic.gravity or _basic.areas):
                    pass
                else:
                    _basic.class_type = "metocean"
            except KeyError:
                pass
            continue
        _tobedeleted.append(key)
    #
    for _sea in _tobedeleted:
        del _seastate[_sea]    
    #
    read_sacs_load_combinations(_checkme_file, model,
                                design_condition,
                                seastate= _seastate,
                                elevation = _elevation)
                                #combination_name=combination_name)
                                #combination_type='metocean')
    #
    # ---------------------------
    #
    # update analysis metocean combination
    #if _seastate:
        #try:
        #    model.analysis.case[combination_name].metocean_combination.update(_seastate)
        #except KeyError:
            #model.analysis.case[combination_name] = f2u.Analysis.Case(combination_name)
            #model.analysis.case[combination_name].metocean_combination = _seastate
    #
    # ---------------------------
    #
    #if _elevation:
    #model.analysis.case[combination_name].condition = _elevation
    #  
    if _tubular_joint:
        model.component.joints.update(_tubular_joint)    
    #
    #print('end seastate')