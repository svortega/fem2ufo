# 
# Copyright (c) 2009-2020 fem2ufo
# 


# Python stdlib imports
import re
import sys
import os
import cmath
from math import degrees
import copy
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
from fem2ufo.preprocess.sacs.model.load import read_sacs_load
import fem2ufo.preprocess.sacs.operations.common as sacs_common
import fem2ufo.preprocess.sacs.operations.process as sacs_process
#from fem2ufo.preprocess.sacs.model.combinations import read_combinations_level_0
from fem2ufo.process.modules.euclid import Point3, Plane
#import fem2ufo.preprocess.sacs.operations.concept as sacs_concept
#
#
def get_coordinate(line, units):
    """
    """
    x_1 = process.units.Number(float(line[11:18]),dims = units[0])
    y_1 = process.units.Number(float(line[18:25]),dims = units[0])
    z_1 = process.units.Number(float(line[25:32]),dims = units[0])
    #
    unit2 = "inch"
    if units[0] == "metre":
        unit2 = "centimetre"
    #
    try :
        #x2m = line[32:39]
        x_2 = process.units.Number(float(line[32:39]),dims = unit2)
        x = x_1 + x_2
    except ValueError:
        x = x_1
    #
    try:
        #y2m = line[39:46]
        y_2 = process.units.Number(float(line[39:46]),dims = unit2)
        y = y_1 + y_2
    except ValueError:
        y = y_1
    #
    try:
        #z2m = line[46:53]
        z_2 = process.units.Number(float(line[46:53]),dims = unit2)
        z = z_1 + z_2
    except ValueError:
        z = z_1
    
    # convert units to metre
    return [x.convert('metre').value, 
            y.convert('metre').value, 
            z.convert('metre').value]
#
def buoyancy_area(do, to, di, ti, mat_density):
    """
    """
    #
    R1 = (do - 2 * to) * 0.50
    R2 = (di - 2 * ti ) * 0.50
    non_flooded_area = round(cmath.pi * R1**2 , 3)
    _thickness = R1 - R2
    flooded_area = round(cmath.pi * (2 * R1 * _thickness), 3)
    #
    R3 = di * 0.50 
    _anulus = round(cmath.pi * (R1**2 - R3**2), 3)
    _inner = round(cmath.pi * (di**2 - (di - 2*ti)**2) / 4.0, 3)
    #
    _total = (_anulus * 2400) + (mat_density * _inner)
    _density = _total / _inner
    
    return [non_flooded_area, flooded_area] , _density
#
#
def get_member(_node_1, _node_2, _concepts):
    """
    """
    try:
        _memb_name = _node_1 + '_' + _node_2
        _memb = _concepts[_memb_name]
        #_end_1 = 0
        #_end_2 = 1
    except KeyError:
        try:
            _memb_name =   _node_2 + '_' + _node_1
            _memb = _concepts[_memb_name]
            #_end_1 = 1
            #_end_2 = 0
        except KeyError:
            print("    ** Warining member {:} no found".format(_memb_name))
            return None
    
    return _memb
#
def get_xjoint_int(node_id, member):
    """
    """
    _items = []
    for _item in member.node[0].elements:
        if node_id in _item:
            _items.append(_item)
    
    if len(_items) >= 4:
        return True
    
    return False
#
def plate_type(lineIn):
    """
    """
    _key = {"shell": r"i|s",
            "membrane": r"m",
            #"plate": r"s",
            "ortho_x": r"x",
            "ortho_x": r"y"}

    keyWord = process.common.find_keyword(lineIn, _key)
    return keyWord
#
def shape_data(_data, _property, _name, _shape, _factor):
    """
    """
    #
    if _data[18:49].strip():
        #_shape = 'general section'
        #
        Area = float(_data[18:24])
        try:
            J = float(_data[24:32])
        except ValueError:
            J = 1.0e6
        Iip = float(_data[32:40])
        Iop = float(_data[40:48])
        
        _property[_name].general_section_data(Area * _factor**2,
                                              Iip * _factor**4,
                                              Iop * _factor**4,
                                              J * _factor**4,
                                              0, 0, 0, 0, 1, 1, 0, 0)
        #_property[_name].shape = 'general section'
        #
        _zdim = None
        try:
            _zdim = float(_data[49:55]) * _factor
            _property[_name].Bft = _zdim
        except ValueError:
            pass
        try:
            _property[_name].D = float(_data[60:66]) * _factor
            
            if not _zdim:
                _property[_name].Bft = float(_data[60:66]) * _factor
        except ValueError:
            if _zdim:
                _property[_name].D = _zdim
            pass
    #else:
    _shape = f2u.SecProp.find_section(_shape)
    #
    if _shape == 'tubular':
        _property[_name].input_data(float(_data[49:55]) * _factor, 
                                    float(_data[55:60]) * _factor)
        #
        if _data[66:71].strip():
            _name = 'grouted_' + _name
            _property[_name] = f2u.SecProp.SectionProperty(_shape, _name)
            _property[_name].input_data(float(_data[60:66]) * _factor, 
                                        float(_data[66:71]) * _factor)
    #
    elif _shape == 'i section':
        #
        #_fillet_rad = float(_data[71:76]) # not currently used
        #        
        # boxed section
        try:
            _tw = float(_data[76:81]) * _factor
            _D = float(_data[60:66]) * _factor
            _B = float(_data[49:55]) * _factor
            _tf = float(_data[55:60]) * _factor
            # i section web ignored
            _property[_name].shape = 'box'
            _property[_name].input_data(_D, _tw, _B, _tf, _B, _tf)
        except:
            _bf = float(_data[49:55])
            _bft = float(_data[55:60])
            _property[_name].input_data(float(_data[60:66]) * _factor, 
                                        float(_data[66:71]) * _factor,
                                        float(_data[49:55]) * _factor, 
                                        float(_data[55:60]) * _factor,
                                        _bf * _factor, 
                                        _bft * _factor)
    #
    elif _shape == 'unsymmetric':
        # check if bottom flange data provided
        try:
            _bft = float(_data[76:80])
            _bf = float(_data[71:76])
        except ValueError:
            _bf = float(_data[49:55])
            _bft = float(_data[55:60])
        #
        #_fillet_rad = float(_data[71:76]) # not currently used
        #
        _property[_name].input_data(float(_data[60:66]) * _factor, 
                                    float(_data[66:71]) * _factor,
                                    float(_data[49:55]) * _factor, 
                                    float(_data[55:60]) * _factor,
                                    _bf * _factor, 
                                    _bft * _factor)        
    #
    elif _shape == 'box':
        _D = float(_data[49:55]) * _factor
        _tw = float(_data[55:60]) * _factor
        _B = float(_data[60:66]) * _factor
        try:
            _tf = float(_data[66:71]) * _factor
        except ValueError:
            _tf = _tw
        # D, Tw=0, Bft=0, Tft=0, Bfb=0, Tfb=0
        _property[_name].input_data(_D, _tw, _B, _tf, _B, _tf)
    # 
    elif _shape == 'general section':
        try:
            Area = float(_data[18:24])
            
            try:
                J = float(_data[24:32])
            except ValueError:
                J = 1.0e6
            Iip = float(_data[32:40])
            Iop = float(_data[40:48])
            
            _property[_name].general_section_data(Area * _factor**2,
                                                  Iip * _factor**4,
                                                  Iop * _factor**4,
                                                  J * _factor**4,
                                                  0, 0, 0, 0, 1, 1, 0, 0)
        except ValueError:
            _property[_name].general_section_data(1.0, 0.10, 0.10, 0.10,
                                                  0, 0, 0, 0, 1, 1, 0, 0)
        #
        _zdim = None
        try:
            _zdim = float(_data[49:55]) * _factor
            _property[_name].Bft = _zdim
        except ValueError:
            pass
        
        try:
            _property[_name].D = float(_data[60:66]) * _factor
            
            if not _zdim:
                _property[_name].Bft = float(_data[60:66]) * _factor
        except ValueError:
            if _zdim:
                _property[_name].D = _zdim
            pass        
    #
    elif _shape == 'channel':
        _property[_name].input_data(float(_data[49:55]) * _factor,
                                    float(_data[60:66]) * _factor,
                                    float(_data[55:60]) * _factor,
                                    float(_data[66:71]) * _factor)
        #print(_shape)
    #
    elif _shape == 'angle':
        try:
            _flange_thk = float(_data[66:71])
        except ValueError:
            _flange_thk = float(_data[55:60])
        
        _property[_name].input_data(float(_data[49:55]) * _factor,
                                    float(_data[60:66]) * _factor,
                                    float(_data[55:60]) * _factor,
                                    _flange_thk * _factor)
    #
    elif _shape == 'tee':
        _property[_name].input_data(float(_data[49:55]) * _factor,
                                    float(_data[60:66]) * _factor,
                                    float(_data[55:60]) * _factor,
                                    float(_data[66:71]) * _factor)
    #
    elif _shape == 'plate':
        print('plate section needs to be fixed')
        pass
    #
    elif _shape == 'cone':
        #
        D1 = float(_data[49:55]) * _factor
        D2 = float(_data[60:66]) * _factor
        WT = float(_data[55:60]) * _factor
        D = (D1 + D2) / 2.0
        #
        try:
            Tw = float(_data[71:76]) * _factor
        except ValueError:
            Tw = WT
        
        try:
            thk = float(_data[66:71]) * _factor
        except ValueError:
            thk = float(_data[60:66]) * _factor
        #
        _property[_name].input_data(D, Tw, D1, WT, D2, thk)
    #
    else:
        print('+++ section shape error here', _shape)
    #
    #
    #_property[_name].sacs_common.get_name(_length_unit)
    #
    #return _property
    #
#
#
def get_value(item,  factor):
    """ """
    try:
        return float(item) * factor
    except ValueError:
        try:
            item = re.sub(r'(\-|\+)', r'E\1', item)
            return float(item) * factor
        except ValueError:
            return 0
#
def get_offset(line, case, factor):
    """
    """
    x1 = get_value(line[35:41],  factor)
    y1 = get_value(line[41:47],  factor)
    z1 = get_value(line[47:53],  factor)
    #
    x2 = get_value(line[53:59],  factor)
    y2 = get_value(line[59:65],  factor)
    z2 = get_value(line[65:71],  factor)
    # local
    if case != 1:
        #_data_swap = [x1, z1, y1, x2, z2, y2]
        _JA = [x1, y1*-1.0 , z1]
        _JB = [x2, y2*-1.0, z2]
    # global
    else:
        _JA = [x1, y1, z1]
        _JB = [x2, y2, z2]
    return _JA, _JB
#
#
def hinge_value(line):
    """ """
    try:
        hinge = float(line)
        if hinge == 1.0:
            return 0
        else:
            return 1
    except ValueError:
        return 1    
#
def get_hinge(line, step):
    """
    """
    x1 = hinge_value(line[step:step+1])
    y1 = hinge_value(line[step+1:step+2])
    z1 = hinge_value(line[step+2:step+3])
    #
    x2 = hinge_value(line[step+3:step+4])
    y2 = hinge_value(line[step+4:step+5])
    z2 = hinge_value(line[step+5:step+6])
    # local
    _data_swap = [int(x1) , int(y1) , int(z1), int(x2), int(y2), int(z2)]
    _name = ("H_" + str(_data_swap[0]) + str(_data_swap[1]) + str(_data_swap[2]) 
             + str(_data_swap[3]) + str(_data_swap[4]) + str(_data_swap[5]))
    return _data_swap, _name
#
#
def get_spring2ground(line, _mat_name, _material, _mat_no,
                      _length_factor, _force_factor):
    """
    :param _units_in:
    :param _material:
    :param _mat_no:
    :return:
    """

    try:
        _material[_mat_name]
    except KeyError:
        _mat_no += 1
        _material[_mat_name] = f2u.Material(_mat_name, _mat_no)
        _material[_mat_name].type = 'spring to ground'
        #
        # create new material
        #
        _tdof = 6
        _material[_mat_name].stiffness = process.mathmet.zeros(_tdof, _tdof)

        # Translational spring rates
        sx = line[11:18].strip()
        sy = line[18:25].strip()
        sz = line[25:32].strip()
        # Rotational spring rates
        smx = line[32:39].strip()
        smy = line[39:46].strip()
        smz = line[46:53].strip()

        data = [sx.replace('+', 'E+'), sy.replace('+', 'E+'), 
                sz.replace('+', 'E+'), smx.replace('+', 'E+'),
                smy.replace('+', 'E+'), smz.replace('+', 'E+')]
        i, j = 0, 0
        _factor = _force_factor / _length_factor
        for x in range(len(data)):
            if x > 2:
                _factor = _force_factor * _length_factor

            try:
                _k = float(data[x]) * _factor
            except ValueError:
                _k = 0

            if j == _tdof:
                i += 1
                j = i
            
            _material[_mat_name].stiffness[j][i] = _k
            _material[_mat_name].stiffness[i][j] = _k
            j += 1
    return _mat_no
#
#
def get_stiffener(_prop_name, _spacing, _dir, _location, line, _units_in):
    """
    """
    if "EN" in _units_in:
        _units = ["inch", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
    else:
        _units = ["centimetre", "1.0*kilogram", "second", 'centigrade', "newton", "pascal"]
    #
    _length_factor, _force_factor= process.units.convert_units_SI(_units[0],  _units[4]) 
    
    _spacing *= _length_factor
    
    if not _dir:
        _dir = 'X'
    
    if not _location:
        _location = "T"
    #
    return [_prop_name, _spacing, _dir, _location]
#
#
def read_sacs_geometry(file_name, 
                       design_condition, library,
                       whisbone_name=None):
    """
    fem2ufo module reads SACS inp files

    **Parameters**:  
      :fileName:  SACS file (geometry and functional loading)
      :global_units : 
      :superelement : True = Master nodes are converted to element hinges
    **Returns**:
      fem2ufo FE concept model

    model
        |_ data
        |_ nodes
        |_ sections
        |_ elements
        |_ materials
        |_ vectors
        |_ eccentricities
        |_ hinges
        |_ sets
        |_ joints
        |_ boundaries
        |
        |_ concepts
        |_ cdcm

    """
    #
    print('')
    print('----------------- READSACS Module ------------------')    
    #
    whisbone_id = ['w.b', 'wb']
    if whisbone_name:
        whisbone_id.append(whisbone_name)
    #
    # read SACS input file
    sacs_file = process.common.check_input_file(file_name)
    if not sacs_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    #
    print('')
    print('--- Reading geometry from {:} input file'.format(file_name))
    #print('')
    #
    #_keyWord = [None, None, None, None, None, None]
    # units = [length, mass, time, temperature, force, pressure/stress]
    #
    _user_data = {}
    _user_data['SACSinp'] = f2u.InputUserData()
    _user_data['SACSinp'].program = 'SACS'
    _user_data['SACSinp'].file_name = file_name
    #
    #_bound_type = ['free', 'fixed', 'prescribed', 'dependent', 'link']
    #
    #_element = {}
    #_node = {}
    _property = {}
    _hinge = {}
    _group = {}
    _unit_vector = {}
    _section = {}
    _material = {}
    _concept = {}
    _eccentricity = {}
    _flooded = {}
    _tubular_joint = {}
    _tapered = {}
    _grouted = {}
    _stiffeners = {}
    #
    _mat_no = 0
    _sect_no = 0
    _plt_no = 0
    #_pst_no = 0
    _memb_no = 0
    _ecc_no = 0
    _node_no = 0
    _fix_no = 0
    _units_in = None
    #
    _group_no = 2
    _group['non_structural'] = f2u.Geometry.Groups('non_structural', 1)
    _group['overlap'] = f2u.Geometry.Groups('overlap', 2)
    #
    _dented_group = []
    _cone_group = []
    #
    model = f2u.F2U_model(sacs_file, 1)
    model.set_component()    
    #
    print('    * First pass')
    dat_temp = open('inpTemp.dat','w+')
    #
    with open(sacs_file) as fin:
        for line in fin:
            #
            # Jump commented lines
            if re.match(r"\*|\-", line):
                continue
            #
            # find key words and clean lines from comments
            #if re.search(r"\*", line):
            #    line = sacs_common.remove_comment(line)
            #
            line = line.strip()
            if not line:
                continue
            #
            # Segment line
            keyword = line.split()
            # Flag word
            if line[0]:
                flag1 = keyword[0]
            #
            #
            if 'PROJECT' in flag1.upper():
                _user_data['SACSinp'].project = line.replace("PROJECT:", "")
                
            #
            # read options
            #
            if 'OPTION' in flag1.upper():
                # get factors
                _units_in = line[13:15].strip()
                _length_factor, _force_factor, _mass_factor = sacs_common.get_units(_units_in)
                # set units
                units = ["foot", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
                if 'M' in _units_in.upper():
                    units = ["metre", "1.0*kilogram", "second", 'centigrade', "newton", "pascal"]
                #
                _length2 = sacs_common.get_length_factor(units)
                _force2 = _force_factor
                if 'EN' in _units_in:
                    _force2 = _force_factor / 1000.00
                #
                _user_data['SACSinp'].units = _units_in
                #
                dat_temp.write("\n")
                dat_temp.write(line)
            #
            # Read Nodes
            #
            elif flag1.upper() == 'JOINT':
                _node_name = line[6:10].strip()
                if not _node_name:
                    continue
                #if '1102' in _node_name:
                #    print('-->')
                if "PERSET" in line[54:61].strip():
                    #print(' *** warning load displacement no yet implemented')
                    dat_temp.write("\n")
                    dat_temp.write(line)                    
                    #continue
                elif "ELASTI" in line[54:61].strip():
                    # spring to ground
                    model.component.nodes[_node_name]
                    # Create new member
                    _memb_no += 1
                    _memb_name = 'spr_' + _node_name
                    _concept[_memb_name] = f2u.Concept(_memb_name, _memb_no)
                    _concept[_memb_name].type = 'Spring to Ground'
                    _concept[_memb_name].node.append(model.component.nodes[_node_name])
                    #
                    _mat_name = 'spr_' + _node_name
                    _mat_no = get_spring2ground(line, _mat_name, _material, _mat_no,
                                                _length2, _force2)
                    _concept[_memb_name].material.append(_material[_mat_name])
                    _material[_mat_name].concepts.append(_memb_name)
                    #
                    del model.component.boundaries[_node_name]
                else:
                    # spring to ground
                    try:
                        model.component.nodes[_node_name]
                        #print('fix here node {:}', line)
                        # Create new member
                        _memb_no += 1
                        _memb_name = 'spr_' + _node_name
                        _concept[_memb_name] = f2u.Concept(_memb_name, _memb_no)
                        _concept[_memb_name].type = 'Spring to Ground'
                        _concept[_memb_name].node.append(model.component.nodes[_node_name])
                        #model.component.nodes[_node_name].elements.append(_memb_name)
                        _mat_name = 'spr_' + _node_name
                        _mat_no = get_spring2ground(line, _mat_name, _material, _mat_no,
                                                    _length2, _force2)
                        _concept[_memb_name].material.append(_material[_mat_name])
                        _material[_mat_name].concepts.append(_memb_name)
                        #
                        del model.component.boundaries[_node_name]
                    except KeyError:
                        _node_no += 1
                        model.component.add_node(_node_name, _node_no)
                        #
                        #if _node_name == "0087":
                        #    print('here')
                        #
                        coord = get_coordinate(line, units)
                        model.component.nodes[_node_name].set_coordinates(coord[0],
                                                                          coord[1],
                                                                          coord[2])
                        #
                        # boundary
                        #
                        _fixity = line[54:60].strip()
                        if _fixity:
                            if 'PILEHD' in _fixity:
                                model.component.add_boundary(name=_node_name,
                                                             node_name=_node_name)
                                model.component.boundaries[_node_name].set_releases([1, 1, 1, 1, 1, 1])
                                model.component.nodes[_node_name].boundary.type = 'link'
                                #
                                # define joint --> this clashes with p2p joints
                                #model.component.add_joint(_node_name, _node_name)
                                #model.component.joints[_node_name].name = 'pile_head'
                                #model.component.joints[_node_name].type = 'pile_head'
                                #                                

                            elif 'FIXED' in _fixity:
                                model.component.add_boundary(name=_node_name,
                                                             node_name=_node_name)
                                model.component.boundaries[_node_name].fixed
                                model.component.nodes[_node_name].boundary.type = 'fixed'

                            elif 'PINNED' in _fixity:
                                model.component.add_boundary(name=_node_name,
                                                             node_name=_node_name)
                                model.component.boundaries[_node_name].pinned
                                model.component.nodes[_node_name].boundary.type = 'pinned'

                            else:
                                #print('constrain')
                                if 'F' in _fixity:
                                    print('   *** warning spring or deflected boundary condition no available')
                                    #model.component.boundaries[_node_name].set_releases([1, 1, 1, 1, 1, 1])
                                    continue
                                # (0 or 2 --> free)
                                _fixity = [0 if int(c) == 2 else int(c) for c in _fixity]
                                # the following fixity will be ignored
                                if sum(_fixity) == 0 :
                                    continue
                                #
                                model.component.add_boundary(name=_node_name,
                                                             node_name=_node_name)
                                #
                                #
                                if len(_fixity) < 6:
                                    _dummy = []
                                    for x in range(6):
                                        try:
                                            _dummy.append(_fixity[x])
                                        except IndexError:
                                            _dummy.append(1)
                                    model.component.boundaries[_node_name].set_releases(_dummy)
                                else:
                                    model.component.boundaries[_node_name].set_releases(_fixity)
                                    model.component.nodes[_node_name].boundary.type == 'fixed'
                                #
            #
            # Read sections
            #
            elif flag1.upper() == 'SECT':
                _sect_name = line[5:12].strip()
                if not _sect_name:
                    continue              
                #
                _shape = sacs_common.section_word(line[15:18])
                _prop_name = sacs_common.get_name(_sect_name, prefix='Sect_')

                if 'dented' in _shape :
                    _shape = 'tubular'
                    _dented_group.append(_prop_name)
                elif 'cone' in _shape :
                    _cone_group.append(_prop_name)
                    #print('---> cone')
                #
                #if _shape == 'box':
                #    print('--')
                _property[_sect_name] = f2u.SecProp.SectionProperty(_shape, 
                                                                    _prop_name)
                
                _factor =  sacs_common.get_length_factor(units)
                shape_data(line, _property, _sect_name, _shape, _factor)
                #_property[_sect_name]
            #
            elif line[0:5] == 'PSTIF':
                print('    *** error PSTIF not yet included')
                continue             
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    #
    print('    * Second pass')
    dat_temp = open('inpTemp2.dat','w+')
    #
    with open('inpTemp.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue
            #
            # Segment line
            keyword = line.split()
            # Flag word
            if line[0]:
                flag1 = keyword[0]
            # Read Groups/material/sections
            # beam
            if flag1.upper() == 'GRUP':
                _group_name = line[5:8].strip()
                if not _group_name:
                    continue
                # property
                _prop_name = line[9:16].strip()
                _prop_new_name = None
                #
                #if 'V18' in _group_name:
                #    print('-->', _mat_no)
                #
                if _prop_name:
                    try:
                        _shape = _property[_prop_name].shape
                    except KeyError:
                        _shape = process.common.get_section_library(_prop_name, _property, library)
                        _prop_new_name = _prop_name
                        # guessing uk sections
                        if _prop_new_name[0].isdigit():
                            if 'b' in _prop_new_name.lower():
                                _prop_new_name = _prop_new_name.replace('B', 'x')
                                _prop_new_name = 'UB' + _prop_new_name
                            elif 'c' in _prop_new_name.lower():
                                _prop_new_name = _prop_new_name.replace('C', 'x')
                                _prop_new_name = 'UC' + _prop_new_name
                # new tubular section
                else:
                    _factor = sacs_common.get_length_factor(units)
                    _shape = 'tubular'
                    _diam = line[17:23].strip() 
                    _thk  = line[23:29].strip()
                    _prop_name = _diam + "_" +_thk
                    _sect_name = sacs_common.get_name(_prop_name, prefix='TS_')
                    #if _sect_name == 'Z01':
                    #    print('here')
                    try:
                        _property[_prop_name]
                    except KeyError:
                        _property[_prop_name] = f2u.SecProp.SectionProperty(_shape, 
                                                                            _sect_name)
                        _property[_prop_name].input_data(float(_diam) * _factor, 
                                                         float(_thk) * _factor)
                #
                # material
                #
                _Emod = float(line[30:35].strip())
                _Gmod = float(line[35:40].strip())
                _Fy = float(line[40:45].strip())
                _density = line[70:76].strip()
                try:
                    _density = float(_density)
                except ValueError:
                    _density = re.sub(r'(\-|\+)', r'E\1', _density)
                    _density = float(_density)
                #
                _mat_name, _mat_no = sacs_common.get_material(_Emod, _Gmod, _Fy, _density, 
                                                              _units_in, _material, _mat_no)
                #
                # section
                #
                try:
                    _section[_group_name]
                    _geomNo += 1
                    _nameStep = _section[_group_name].name
                    if not _nameStep:
                        _nameStep = _group_name
                    _nameStep = str(_nameStep) + '_step_' + str(_geomNo)
                    #_prop_new_name = sacs_common.get_name(_prop_name, prefix='Sect_')
                    #_nameStep = _prop_new_name
                    # 
                    #
                    try:
                        _section[_group_name].step.append(_nameStep)
                    except AttributeError:
                        _section[_group_name].step = [_nameStep]
                    #
                    _sect_no += 1
                    _section[_nameStep] = f2u.Geometry.Section(_prop_new_name, 
                                                               _sect_no, 'beam', _shape)
                    #
                    # update section
                    _section[_nameStep].properties = _property[_prop_name]
                    _section[_nameStep].material = _material[_mat_name]
                    #if not _section[_nameStep].name:
                    #    _section[_nameStep].get_name('metre')
                    #
                    # tappered section'
                    if line[8:9].strip():
                        try:
                            _tapered[_group_name].append([_nameStep, line[8:9].strip()])
                        except KeyError:
                            pass 
                    #
                    #
                    # segmented length
                    try:
                        _sect = float(line[76:80]) * _length_factor
                        if cmath.isclose(_sect, 0, rel_tol=0.01):
                            continue
                        _section[_nameStep].length = _sect
                    except ValueError:
                        continue
                    #
                except KeyError:
                    _geomNo = 0
                    _sect_no += 1
                    #_prop_new_name = sacs_common.get_name(_group_name, prefix='Sect_')
                    _section[_group_name] = f2u.Geometry.Section(_prop_new_name,
                                                                 _sect_no, 'beam', _shape)
                    #
                    # update section
                    _section[_group_name].properties = _property[_prop_name]
                    _section[_group_name].material = _material[_mat_name]
                    
                    if _group_name.lower() in whisbone_id:
                        _section[_group_name].type = 'beam_wishbone'
                    #
                    # _name = 'grouted_' + _name
                    try:
                        _property['grouted_' + _prop_name]
                        _section[_group_name].type = 'beam_grouted'
                        _grouted[_group_name] = []
                        # new
                        _new_sect_name = _group_name + '_grouted' 
                        _sect_no += 1
                        _section[_new_sect_name] = f2u.Geometry.Section(_prop_new_name,
                                                                        _sect_no, 'beam', _shape)
                        _section[_new_sect_name].properties = _property['grouted_' + _prop_name]
                        _section[_new_sect_name].material = _material[_mat_name]                        
                    except KeyError:
                        pass
                    #
                    _group_no += 1
                    _prefix = 'GS'+ str(_group_no).zfill(3)
                    _new_group_name = sacs_common.get_name(_group_name, prefix=_prefix)
                    _new_group_name =  _prefix +'_'+_new_group_name
                    _group[_group_name] = f2u.Geometry.Groups(_new_group_name, 
                                                              _group_no)
                    #
                    # segmented length
                    try:
                        _sect = float(line[76:80]) * _length_factor
                        if cmath.isclose(_sect, 0, rel_tol=0.01):
                            continue
                        _section[_group_name].length = _sect
                    except ValueError:
                        pass
                    #
                    # tappered section'
                    if line[8:9].strip():
                        _tapered[_group_name] = []
                        _tapered[_group_name].append([_group_name, line[8:9].strip()])
                #
                #
                # flooded
                _flood = line[69:70].strip()
                if 'F' in _flood.upper():
                    _flooded[_group_name] = _group_name
                #
                #
                #
            # plates
            elif flag1.upper() == 'PGRUP':
                _group_name = line[5:9].strip()
                if not _group_name:
                    continue
                #
                try:
                    float(_group_name)
                    _name = 'PLT_' + _group_name
                except ValueError:
                    _name = _group_name
                #
                _group_name = 'PLT_' + _group_name
                #
                _factor =  sacs_common.get_length_factor(units)
                _plt_no += 1
                _th = float(line[10:16].strip())
                _sect_type = line[16].strip()
                
                if not _sect_type:
                    _sect_type = 'shell'
                    _shape = 'shell'
                else:
                    _sect_type = plate_type(_sect_type)
                    #
                    if _sect_type != 'shell' and _sect_type != 'membrane' and _sect_type != 'plate':
                        print('    *** error, plate element type {:} no yet defined'.format(_sect_type))
                        _sect_type = 'shell'
                        _shape = 'shell'
                        #sys.exit()
                    else:
                        _shape = 'shell'
                #
                _section[_group_name] = f2u.Geometry.Section(_name, _plt_no, 
                                                             _sect_type, _shape)
                #
                _prop_name = 'PLT_' + str(_th)
                _sect_name = sacs_common.get_name(_prop_name, prefix='Sect_')
                #_sect_name = 'Plt_' + _sect_name
                #if _sect_name == 'Z01':
                #    print('here')
                
                _section[_group_name].properties = f2u.SecProp.SectionProperty(_sect_type, _sect_name)
                _section[_group_name].properties.input_data(_th * _factor)
                #
                # material
                #
                _Emod = float(line[17:23].strip())
                _Gmod = float(line[23:29].strip())
                _Fy = float(line[29:35].strip())
                _density = line[72:80].strip()
                try:
                    _density = float(_density)
                except ValueError:
                    _density = re.sub(r'(\-|\+)', r'E\1', _density)
                    try:
                        _density = float(_density)
                    except ValueError:
                        _density = 0
                #
                _mat_name, _mat_no = sacs_common.get_material(_Emod, _Gmod, _Fy, _density, 
                                                              _units_in, _material, _mat_no) #,
                                                     #prefix='MTP')
                #
                #
                _section[_group_name].material = _material[_mat_name]
                #
                _group_no += 1
                _prefix = 'GS' + str(_group_no).zfill(3)
                _new_group_name = sacs_common.get_name(_group_name, prefix=_prefix)
                _new_group_name =  _prefix +'_'+_new_group_name
                _group[_group_name] = f2u.Geometry.Groups(_new_group_name, 
                                                          _group_no)
                #
                # First stiffener
                _prop_name = line[41:48].strip()
                if _prop_name:
                    _spacing = float(line[48:54].strip())
                    _dir = line[54].strip()
                    _location = line[55].strip()
                    _flag = True
                    try:
                        _shape = _property[_prop_name].shape
                        #_name = _section[_group_name].name
                        #_stiffeners[_group_name] = []
                        #_stiffeners[_group_name].append(get_stiffener(_prop_name, _spacing, _dir, 
                        #                                              _location, line, _units_in))
                    except KeyError:
                        try:
                            _shape = process.common.get_section_library(_prop_name, _property, library)
                            #_stiffeners[_group_name] = []
                            #_stiffeners[_group_name].append(get_stiffener(_prop_name, _spacing, _dir, 
                            #                                              _location, line, _units_in))
                        except IOError:
                            _flag = False
                            print('    *** warning section {:} not found'.format(_prop_name))
                    #
                    if _flag:
                        _stiffeners[_group_name] = []
                        _stiffeners[_group_name].append(get_stiffener(_prop_name, _spacing, _dir, 
                                                                      _location, line, _units_in))                    
                        #
                        try:
                            _section[_prop_name]
                        except KeyError:
                            _sect_no += 1
                            _section[_prop_name] = f2u.Geometry.Section(_prop_name,
                                                                         _sect_no, 'beam', _shape)
                            # update section
                            _section[_prop_name].properties = _property[_prop_name]
                            _section[_prop_name].material = _material[_mat_name]                    
                #
                #
                # Seconf stiffener
                _prop_name = line[57:64].strip()
                if _prop_name:
                    _spacing = float(line[64:70].strip())
                    _dir = line[70].strip()
                    _location = line[71].strip()
                    _flag = True
                    try:
                        _shape = _property[_prop_name].shape
                        #_stiffeners[_group_name].append(get_stiffener(_prop_name, _spacing, _dir, 
                        #                                              _location, line, _units_in))
                    except KeyError:
                        try:
                            _shape = process.common.get_section_library(_prop_name, _property, library)
                            #_stiffeners[_group_name] = get_stiffener(_prop_name, _spacing, _dir, 
                            #                                         _location, line, _units_in)
                        except IOError:
                            _flag = False
                            print('    *** warning section {:} not found'.format(_prop_name))
                    #
                    if _flag:
                        _stiffeners[_group_name].append(get_stiffener(_prop_name, _spacing, _dir, 
                                                                      _location, line, _units_in))                    
                        #
                        try:
                            _section[_prop_name]
                        except KeyError:
                            _sect_no += 1
                            _section[_prop_name] = f2u.Geometry.Section(_prop_name,
                                                                         _sect_no, 'beam', _shape)
                            # update section
                            _section[_prop_name].properties = _property[_prop_name]
                            _section[_prop_name].material = _material[_mat_name]                     
            #
            elif flag1.upper() == 'END':
                pass
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    os.remove('inpTemp.dat')
    #
    _fix_no += 1
    _hinge['H_111111'] = f2u.Geometry.Hinges('H_111111', _fix_no)
    _hinge['H_111111'].fix = [1,1,1,1,1,1]
    #
    print('    * Third pass')
    dat_temp = open('inpTemp3.dat', 'w+')
    with open('inpTemp2.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue
            # Segment line
            keyword = line.split()
            #    Flag word
            if line[0]:
                flag1 = keyword[0]
            #
            if line[0:6] == 'MEMBER':
                _offset = False
                _group_name = line[16:19].strip()
                #if 'V18' in _group_name:
                #    print('-->')
                # check if member or member offset section
                if not _group_name:
                    _offset = line[7:15].strip()
                    if not _offset:
                        continue
                else:
                    _node_1 = line[7:11].strip()
                    _node_2 = line[11:15].strip()
                    try:
                        _offset_case = int(line[6:7])
                        #print('-->',_offset_case)
                    except ValueError:
                        _offset_case = None
                #
                # member offset card only
                if _offset:
                    _ecc_name = _group_name_res
                    _factor =  sacs_common.get_length_factor(units)
                    _JA, _JB = get_offset(line, _offset_case, _factor)
                    #
                    #if _memb_name == '2544_H106':
                    #    print('here')                    
                    #
                    _ecc_no += 1
                    _eccentricity[_ecc_name] = f2u.Geometry.Eccentricities(_ecc_name,
                                                                           _ecc_no)
                    _eccentricity[_ecc_name].eccentricity.extend(_JA)
                    _eccentricity[_ecc_name].case = _offset_case
                    
                    _concept[_memb_name].offsets.append(_eccentricity[_ecc_name])
                    #
                    #
                    _ecc_no += 1
                    _ecc_name = _ecc_name + "_1"
                    _eccentricity[_ecc_name] = f2u.Geometry.Eccentricities(_ecc_name,
                                                                           _ecc_no)
                    _eccentricity[_ecc_name].eccentricity.extend(_JB)
                    _eccentricity[_ecc_name].case = _offset_case
                    #
                    _concept[_memb_name].offsets.append(_eccentricity[_ecc_name])
                    #
                    #_concept[_memb_name].offsets.append(_eccentricity[_sec_no])
                    #
                    # no more information after this
                    continue
                # member cards
                else:
                    #_group_name_res = _group_name
                    _memb_name = str(_node_1) + "_" + str(_node_2)
                    _memb_no += 1
                    _group_name_res = _memb_name
                    #
                    #if _memb_name == '0151_0037':
                    #    print('here')  
                    #
                    try:
                        _concept[_memb_name]
                        _memb_name = _memb_name + "_" + str(_memb_no)
                        _concept[_memb_name] = f2u.Concept(_memb_name, _memb_no)
                        _concept[_memb_name].overlap = True
                        try:
                            _non_structural = line[19:21].strip()
                            _group['non_structural'].concepts.append(_memb_name)
                        except ValueError:
                            _group['overlap'].concepts.append(_memb_name)
                    except KeyError:
                        _concept[_memb_name] = f2u.Concept(_memb_name, _memb_no)
                    #
                    _concept[_memb_name].type = 'beam'
                    _concept[_memb_name].properties = f2u.Geometry.BeamElementProperty('hydrodynamic')                   
                    #
                    _concept[_memb_name].node.append(model.component.nodes[_node_1])
                    model.component.nodes[_node_1].elements.append(_memb_name)
                
                    _concept[_memb_name].node.append(model.component.nodes[_node_2])
                    model.component.nodes[_node_2].elements.append(_memb_name)
                    #
                    # hinges
                    #
                    if line[23:28].strip() or line[28:34].strip():
                        # joint A
                        if line[23:28].strip() :
                            _JA, _fix_name = get_hinge(line, step=22)
                            try:
                                _hinge[_fix_name]
                            except KeyError:
                                _fix_no +=1
                                _hinge[_fix_name] = f2u.Geometry.Hinges(_fix_name,
                                                                        _fix_no)
                                _hinge[_fix_name].fix = _JA
                            
                            _concept[_memb_name].releases.append(_hinge[_fix_name])
                        else:
                            _concept[_memb_name].releases.append(_hinge['H_111111'])
                        # joint B
                        if line[28:34].strip():
                            _JB, _fix_name = get_hinge(line, step=22+6)
                            try:
                                _hinge[_fix_name]
                            except KeyError:
                                _fix_no +=1
                                _hinge[_fix_name] = f2u.Geometry.Hinges(_fix_name,
                                                                        _fix_no)
                                _hinge[_fix_name].fix = _JB
                            
                            _concept[_memb_name].releases.append(_hinge[_fix_name])
                        else:
                            _concept[_memb_name].releases.append(_hinge['H_111111'])
                    #
                    # member propertities allocation
                    try:
                        _concept[_memb_name].section.append(_section[_group_name])
                        # density
                        try:
                            _mat_density = float(line[64:70])
                            _material_section = _section[_group_name].material
                            #print(' -- > fix mat density')
                            _stress_factor, _density_factor = sacs_common.material_units(_units_in)
                            _mat_density *= _density_factor
                            #
                            _mat_name, _mat_no = sacs_common.get_material(_material_section.E, 
                                                                          _material_section.G, 
                                                                          _material_section.Fy, 
                                                                          _mat_density, 'SI', 
                                                                          _material, _mat_no)
                        except ValueError:
                            _mat_name = _section[_group_name].material.name
                        #
                        _material_section = _material[_mat_name]
                        _material_section.concepts.append(_memb_name)
                        #
                        _concept[_memb_name].material.append(_material_section)
                        _section[_group_name].elements.append(_memb_name)
                        _group[_group_name].concepts.append(_memb_name)
                        # check if group flooded
                        try:
                            _flooded[_group_name]
                            _concept[_memb_name].properties.flooded = True
                            try:
                                _group['_flooded_'].concepts.append(_memb_name)
                            except KeyError:
                                _group_no += 1
                                _group['_flooded_'] = f2u.Geometry.Groups('_flooded_', _group_no)
                                _group['_flooded_'].concepts.append(_memb_name)
                        except KeyError:
                            pass
                        #
                        # check if tapered
                        try:
                            _concept[_memb_name].properties.tapered = _tapered[_group_name]
                        except KeyError:
                            pass
                        #
                        # check if grouted                       
                        try:
                            _grouted[_group_name].append(_memb_name)
                        except KeyError:
                            pass
                        #
                    except KeyError:
                        print(' error section missing {:}'.format(_group_name))
                        continue
                #
                # Rotation
                try:
                    _angle = float(line[35:41])
                    _concept[_memb_name].properties.local_system = _angle
                except ValueError:
                    pass
                #
                # local axis
                _node_3 = line[41:45].strip()
                if _node_3:
                    #print(' -- > fix local axis')
                    _node1 = model.component.nodes[_node_1]
                    _node2 = model.component.nodes[_node_2]
                    _node3 = model.component.nodes[_node_3]
                    
                    # nodes member 1
                    A = Point3(_node1.x, _node1.y, _node1.z)
                    B = Point3(_node2.x, _node2.y, _node2.z)
                    C = Point3(_node3.x, _node3.y, _node3.z)
                    
                    _planeABC = Plane(A, B, C)
                    _vAB = B - C
                    _vuni = _vAB.normalized()
                    try:
                        _angle = degrees(_vuni.angle(_planeABC.n))
                        _concept[_memb_name].properties.local_system = _angle
                    except ZeroDivisionError:
                        pass
                    #print(' -- > fix local axis', _angle)
                #
                #
                # flooded
                #
                _flood = line[45:46].strip()
                if 'F' in _flood.upper():
                    _concept[_memb_name].properties.flooded = True
                    #
                    try:
                        _group['_flooded_'].concepts.append(_memb_name)
                    except KeyError:
                        _group_no += 1
                        _group['_flooded_'] = f2u.Geometry.Groups('_flooded_', _group_no)
                        _group['_flooded_'].concepts.append(_memb_name)
                
                elif 'N' in _flood.upper():
                    _concept[_memb_name].properties.flooded = False
                #
                # KL option
                _KLoption = line[46:47].strip()
                #
                # KL factors It seems sacs has similar convention as sesam (i.e. about local system)
                # KLy
                try:
                    _KLy = float(line[51:55])
                    if 'L' in _KLoption.upper():
                        _concept[_memb_name].properties.L[1] = _KLy * _length_factor
                    else:
                        _concept[_memb_name].properties.K[1] = _KLy
                except ValueError:
                    pass
                # KLz
                try:
                    _KLz = float(line[55:59])
                    if 'L' in _KLoption.upper():
                        _concept[_memb_name].properties.L[2] = _KLz * _length_factor
                    else:
                        _concept[_memb_name].properties.K[2] = _KLz
                except ValueError:
                    pass
                #
                # unbraced lenght or shear modulus factor
                try:
                    _concept[_memb_name]
                    _LbSF = float(line[59:64])
                    #if 'i section' in _concept[_memb_name].section[0].shape:
                    #    _concept[_memb_name].properties.Lb = _LbSF * _length_factor 
                    if 'tubular' in _concept[_memb_name].section[0].shape:
                        print(' -- > fix tubular LbSF')
                    else:
                        _concept[_memb_name].properties.Lb = _LbSF * _length_factor 
                        #print(' -- > fix other LbSF')
                except ValueError:
                    pass
                #
                # added mass diameter
                #
            #
            elif line[0:5] == 'PLATE':
                _offset = False
                _group_name = line[27:30].strip()
                #
                if not _group_name:
                    _offset = line[7:14].strip()
                    if not _offset:
                        continue
                else:
                    _PLnodes = [line[11:15].strip()]
                    _PLnodes.append(line[15:19].strip())
                    _PLnodes.append(line[19:23].strip())
                    _node_4 = line[23:27].strip()
                    
                    if _node_4:
                        _PLnodes.append(_node_4)
                    
                    try:
                        _offset_case = int(line[42])
                    except ValueError:
                        _offset_case = None                
                #
                if _offset:
                    #print('fix plate offsets')
                    _ecc_name = _group_name_res
                    
                    _factor =  sacs_common.get_length_factor(units)
                    _JA, _JB = get_offset(line, _offset_case, _factor)
                    #
                    #
                    _ecc_no += 1
                    _ecc_name = _ecc_name + "_" + str(_ecc_no)
                    _eccentricity[_ecc_name] = f2u.Geometry.Eccentricities(_ecc_name,
                                                                           _ecc_no)
                    _eccentricity[_ecc_name].eccentricity.extend(_JA)
                    _eccentricity[_ecc_name].case = _offset_case
                    
                    _concept[_memb_name].offsets.append(_eccentricity[_ecc_name])
                    #
                    #
                    _ecc_no += 1
                    _ecc_name = _ecc_name + "_" + str(_ecc_no)
                    _eccentricity[_ecc_name] = f2u.Geometry.Eccentricities(_ecc_name,
                                                                           _ecc_no)
                    _eccentricity[_ecc_name].eccentricity.extend(_JB)
                    _eccentricity[_ecc_name].case = _offset_case
                    
                    _concept[_memb_name].offsets.append(_eccentricity[_ecc_name])
                else:
                    _memb_name  = line[6:10].strip()
                    #_no = 0
                    #for _node_name in _PLnodes:
                    #    if _no > 0:
                    #        _memb_name += "_" 
                    #    _no += 1
                    #    _memb_name += _node_name
                    #
                    _memb_no += 1
                    _group_name_res = _memb_name
                    #
                    #print('here')
                    #
                    _concept[_memb_name] = f2u.Geometry.ShellElement(_memb_name,
                                                                     _memb_no)
                    _concept[_memb_name].type = 'shell'
                    #
                    for _node_name in _PLnodes:
                        try:
                            _concept[_memb_name].node.append(model.component.nodes[_node_name])
                            model.component.nodes[_node_name].elements.append(_memb_name)
                        except KeyError:
                            print('    *** error Node {:} not found'
                                  .format(_node_name))
                            sys.exit()
                    #
                    try:
                        _group_name = 'PLT_' + _group_name
                        _concept[_memb_name].section.append(_section[_group_name])
                        _material_section = _section[_group_name].material
                        _material_section.concepts.append(_memb_name)
                        _concept[_memb_name].material.append(_material_section)
                        _section[_group_name].elements.append(_memb_name)
                        _group[_group_name].concepts.append(_memb_name)
                    except KeyError:
                        print('    *** error section missing {:}'.format(_group_name))
                        continue
            #
            #
            elif line[0:5] == 'SHELL':
                #
                print(' error SHELL not yet included')
                continue
            #
            elif line[0:5] == 'SOLID':
                #
                print(' error SOLID not yet included')
                continue
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    os.remove('inpTemp2.dat')
    #
    print('    * Fourth pass')
    _checkme_file = 'CheckMe_' + sacs_file
    dat_temp = open(_checkme_file,'w+')
    #
    with open('inpTemp3.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue
            #
            # Segment line
            keyword = line.split()
            #    Flag word
            if line[0]:
                flag1 = keyword[0]
            #
            if line[0:5] == 'MEMB2':
                #_axis = {'X':0, 'Y':1, 'Z':2}
                if line[7:14].strip():
                    print(' fix tension member')
                #
                _joint_flag = True
                _join_type = line[14:15].strip()
                _plane = line[15:16].strip()
                if not _plane:
                    _plane = 'Z'
                #
                _node_1 = line[16:20].strip()
                _node_2 = line[20:24].strip()
                _memb1 = get_member(_node_1, _node_2, _concept)
                if not _memb1:
                    continue
                #
                _node_3 = line[24:28].strip()
                _node_4 = line[28:32].strip()
                _memb2 = get_member(_node_3, _node_4, _concept)
                #
                #
                if _memb2:
                    set_1 = set([_node_1, _node_2])
                    set_2 = set([_node_3, _node_4])
                    _node_name = list(set_1.intersection(set_2))
                    try:
                        _node_name = _node_name[0]
                    except IndexError:
                        print('    ** Warning element {:} forming {:}-joint has no intersection points'
                              .format(_memb2.name, _join_type))
                        _joint_flag = False
                else:
                    _joint_flag = get_xjoint_int(_node_1, _memb1)
                    if _joint_flag:
                        _node_name = _node_1
                    else:
                        _joint_flag = get_xjoint_int(_node_2, _memb1)
                        if _joint_flag:
                            _node_name = _node_2
                        else:
                            print('    ** Warning element {:} forming {:}-joint has no intersection points'
                                  .format(_memb1.name, _join_type))
                    #
                    #print('node --> joint', _node_name)
                #
                # K factor
                try:
                    _Kyz = float(line[32:38])
                except ValueError:
                    _Kyz = 0.90
                    if _join_type.lower() == 'k':
                        _Kyz = 0.80
                
                _memb1.properties.K[1] = _Kyz
                _memb1.properties.K[2] = _Kyz
                
                if _memb2:
                    _memb2.properties.K[1] = _Kyz
                    _memb2.properties.K[2] = _Kyz
                #
                # Effective Length
                try:
                    _Lyz = float(line[38:45]) * _length_factor
                    _memb1.properties.L[1] = _Lyz
                    _memb1.properties.L[2] = _Lyz
                    
                    if _memb2:
                        _memb2.properties.L[1] = _Lyz
                        _memb2.properties.L[2] = _Lyz
                except ValueError:
                    pass
                #
                # define joint
                if _joint_flag:
                    model.component.add_joint(_node_name, _node_name)
                    model.component.joints[_node_name].name = 'msl'
                    model.component.joints[_node_name].type = 'msl'
                #
                # define ring spacing
                try:
                    _ring = float(line[45:50])
                    print('ring spacing')
                except ValueError:
                    pass
                #
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    os.remove('inpTemp3.dat') 
    #
    # ---------------------
    # find stepped sections
    #
    _geometry_items = process.concept.find_step_section(_concept, _section,
                                                        _material, _eccentricity,
                                                        _tapered)
    #
    # ----------------------------------------------------------------------
    #                     Find whisbones
    # ----------------------------------------------------------------------    
    #
    _shim = []
    for key, memb in _concept.items():
        if memb.type != 'beam' :
            continue
        elif not memb.releases:
            continue
        #
        if not 'tubular' in memb.section[0].shape:
            continue        
        #
        #if memb.name == '8503_8502':
        #    print('here')
        
        # assume max tolerance 6 inches
        if 'wishbone' in memb.section[0].type or cmath.isclose(memb.length_node2node, 0, abs_tol=0.155): 
            # assuming the norm for wishbones are not fully fix in both ends
            if (sum(memb.releases[0].fix) == 6 and sum(memb.releases[1].fix) == 6):
                continue
            # assuming the norm for wishbones are fully fix in one end
            if (sum(memb.releases[0].fix) < 6 and sum(memb.releases[1].fix) < 6):
                #print('-->')
                continue
            #
            _shim.append(key)
            #
            # Probably an overlaping memeber (i.e. pile in leg)
            #if memb.offsets:
            # end fix is offset 6', then pick up the end
            if sum(memb.releases[0].fix) == 6:
                _node_1 = memb.node[0]
                _node_2 = memb.node[1]
                _rel = memb.releases[1].fix
                #_index = 0
                #_offset = memb.offsets[0]
            else:
                _node_1 = memb.node[1]
                _node_2 = memb.node[0]
                _rel = memb.releases[0].fix
                #_index = 1
                #_offset = memb.offsets[1]
            #
            #
            #else:
                # Probaly a conductor/stub member
                # case when no offsets were given i.e immortelle
                # try if this can be used for a general selection of the shim end
                #if len(memb.node[0].elements) < len(memb.node[1].elements):
                #    _node_1 = memb.node[1]
                #    _node_2 = memb.node[0]
                #    _rel = memb.releases[1].fix
                #else:
                #    _node_1 = memb.node[0]
                #    _node_2 = memb.node[1]
                #    _rel = memb.releases[0].fix
            #
            # updating slave node coordinates
            _node_2.coordinates = _node_1.coordinates
            #
            dddof = [0, 0, 0, 0, 0, 0]
            for x in range(6):
                if _rel[x] == 0:
                    continue
                dddof[x] = 1.0E+12
            #
            # create dummy joint
            # note joint number set to master node --> this is use to
            # identify it
            #
            _node_master = _node_1.name
            _node_slave = _node_2.name
            #
            # remove shim from items try node1_node2
            try:
                _name_shim = _node_master +"_"+_node_slave
                _node_1.elements.remove(_name_shim)
                _node_2.elements.remove(_name_shim)
            except ValueError:
                # remove shim from items try node2_node3
                try:
                    _name_shim = _node_slave +"_"+ _node_master
                    _node_1.elements.remove(_name_shim)
                    _node_2.elements.remove(_name_shim)
                except ValueError:
                    print('    *** error wishbone {:}'.format(key))
                    continue
            #
            #_node_name = 'p2p_' + _node_slave
            _node_name = _node_slave
            #
            #if _node_name == '9902':
            #    print('here')            
            #
            # define dummy joint
            _tubular_joint[_node_name] = f2u.Geometry.Joints(name = _node_master,
                                                             node = model.component.nodes[_node_slave])
            _tubular_joint[_node_name].type = 'shim'
            _tubular_joint[_node_name].set_spring(dddof)
            #_tubular_joint[_node_name].brace = model.component.nodes[_node_master].elements
            #
            _master_memb = model.component.nodes[_node_master].elements
            _slave_memb = model.component.nodes[_node_slave].elements           
            # Find overlaping members (i.e. pile in leg)
            _dum_memb = copy.copy(_master_memb)
            _dum_memb.extend(copy.copy(_slave_memb))
            _chord, _overlapping = sacs_process.get_chord(_dum_memb,
                                                          _tubular_joint[_node_name], _concept)            
            #
            if _overlapping:
                _tubular_joint[_node_name].chord = _chord
                if _overlapping[0] in _slave_memb:
                    _tubular_joint[_node_name].overlap = _slave_memb
                    _braces = list(set(_master_memb) - set(_chord))
                else:
                    _tubular_joint[_node_name].overlap =  _master_memb
                    _braces = list(set(_slave_memb) - set(_chord))
                _tubular_joint[_node_name].brace = _braces
                #
                for _memb_name in _tubular_joint[_node_name].overlap:
                    _member = _concept[_memb_name]
                    if _member.type != 'beam' :
                        continue
                    _member.type = 'beam_disconnected'
                    _member.overlap = True
                    _group['overlap'].concepts.append(_memb_name)                
            else:
                # find chord master
                _master_chord, _master_dummy = sacs_process.get_chord(_master_memb,
                                                                      _tubular_joint[_node_name], _concept)
                # find chord slave
                _slave_chord, _slave_dummy = sacs_process.get_chord(_slave_memb,
                                                                    _tubular_joint[_node_name], _concept)
                
                if _master_chord and _slave_chord:
                    _tubular_joint[_node_name].chord = _slave_memb 
                    _tubular_joint[_node_name].brace = _master_chord
                elif _master_chord:
                    #print('---> 1')
                    _tubular_joint[_node_name].chord = _master_chord
                    _tubular_joint[_node_name].brace = _slave_memb                    
                elif _slave_chord:
                    _tubular_joint[_node_name].chord = _slave_chord
                    _tubular_joint[_node_name].brace = _master_memb
                    # print('---> 2')
                else:
                    _tubular_joint[_node_name].chord = _slave_memb 
                    _tubular_joint[_node_name].brace = _master_memb                    
                    #print('---> fix shim here')
                    #1/0
    #
    for _memb_name in _shim:
        del _concept[_memb_name]
    #
    _secprop = {}
    for key, _sect in _section.items():
        try:
            _secprop[_sect.properties.name].append(key)
        except KeyError:
            _secprop[_sect.properties.name] = [key]
    #
    # get dented
    #_secprop = {_sect.properties.name : key 
    #            for key, _sect in _section.items()}
    #
    for _sect_name in _dented_group:
        try:
            for _item in _secprop[_sect_name]:
                #_sect = _section[_item]
                _group_no += 1
                _name = 'dented_' + _sect_name
                _group[_name] = f2u.Geometry.Groups(_name, _group_no)
                for _memb_name in _section[_item].elements:
                    _group[_name].concepts.append(_memb_name)
        except KeyError:
            print('    ** Warning section {:} not in use'
                  .format(_sect_name))        
    #
    if _cone_group:
        _group_no += 1
        _name = 'cones_new_set'
        _group[_name] = f2u.Geometry.Groups(_name, _group_no)
        for _sect_name in _cone_group:
            try:
                for _item in _secprop[_sect_name]:
                    #_sect = _section[_item]
                    for _memb_name in _section[_item].elements:
                        _group[_name].concepts.append(_memb_name)
            except KeyError:
                print('    ** Warning section {:} not in use'
                      .format(_sect_name))
    #
    # get grouted
    if _grouted:
        from statistics import mean
        _group_no += 1
        _name = 'grouted_pile_in_leg'
        _group[_name] = f2u.Geometry.Groups(_name, _group_no)
        #
        # check if metocean module exist
        if model.metocean:
            _metocean = model.metocean
        else:      
            model.set_metocean('metocean')
            _metocean = model.metocean
            _metocean.flooded = {}
        #
        _metocean.hydro_diameter['Zero'] = f2u.Metocean.Parameters('Zero', 0)
        _metocean.hydro_diameter['Zero'].coefficient = 1e-005
        _metocean.hydro_diameter['Zero'].type = 'hydrodynamic_diameter'
        #
        _metocean.marine_growth = {}
        _metocean.marine_growth['no_marine_growth'] = f2u.Metocean.MarineGrowth('no_marine_growth', 0)
        _metocean.marine_growth['no_marine_growth'].type = 'specified'
        _metocean.marine_growth['no_marine_growth'].inertia = False
        _metocean.marine_growth['no_marine_growth'].constant(thickness=0, roughness=0)
        #
        _metocean.cdcm = {}
        _metocean.cdcm['Zero'] = f2u.Metocean.DragMassCoefficient('Zero', 0)
        _metocean.cdcm['Zero'].set_cdcm([0.0,0.0,0.0,0.0,0.0,0.0])
        #
        ba_i = 0
        #
        for _sect, _items in _grouted.items():
            _sect_name = _sect + '_grouted'
            _sect = _section[_sect_name]
            for _memb_name in _items:
                _memb = _concept[_memb_name]
                _mat = _memb.material[0]
                _new_name = _memb.name + '_Inner'
                _memb_no += 1
                _concept[_new_name] = copy.deepcopy(_concept[_memb_name])
                _concept[_new_name].type = 'beam_grouted'
                _concept[_new_name].name = _new_name
                _concept[_new_name].number = _memb_no
                _concept[_new_name].section = [_sect]
                # TODO: check if step is required
                #if _memb.step:
                #    print('---> step')                
                _concept[_new_name].step = []
                _concept[_new_name].overlap = True
                # hydro parameters
                _concept[_new_name].properties.hydrodynamic_diameter = _metocean.hydro_diameter['Zero']
                _concept[_new_name].properties.marine_growth = _metocean.marine_growth['no_marine_growth']
                _concept[_new_name].properties.cdcm = _metocean.cdcm['Zero']
                # Section
                _sect.elements.append(_new_name)
                # get buoyancy areas
                _set, mat_density = buoyancy_area(_memb.section[0].properties.D, 
                                                  _memb.section[0].properties.Tw, 
                                                  _sect.properties.D, _sect.properties.Tw,
                                                  _mat.density)
                _mean = mean(_set)
                try:
                    _metocean.buoyancy_area[_mean]
                except KeyError:
                    ba_i += 1
                    _metocean.buoyancy_area[_mean] = f2u.Metocean.Parameters(ba_i, ba_i)
                    _metocean.buoyancy_area[_mean].coefficient = _set
                    _metocean.buoyancy_area[_mean].type = 'buoyancy_area'
                _concept[_new_name].properties.buoyancy_area = _metocean.buoyancy_area[_mean]
                # materials
                _new_mat_name = _mat.name + '_grouted'
                try:
                    _material[_new_mat_name]
                except KeyError:
                    _material[_new_mat_name] = copy.deepcopy(_mat)
                    _material[_new_mat_name].name = _new_mat_name
                    _material[_new_mat_name].number = len(_material) + 1
                    _material[_new_mat_name].density = mat_density
                _concept[_new_name].material = [_material[_new_mat_name]]
                _material[_new_mat_name].concepts.append(_new_name)
                # sets 
                _group['overlap'].concepts.append(_new_name)
                _group[_name].concepts.append(_new_name)
                #print('-->')
    #
    #
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #                                   POSTPROCESSING
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #    
    # Populate concept object
    #nodes = model.component.nodes
    #_elements = sacs_concept.update_concepts(_concept, nodes, _group)
    #    
    #
    # --------------------------------------------------------------------------------------
    #                                     END READ FILE
    # --------------------------------------------------------------------------------------
    #
    model.component.data = _user_data
    #model.component.elements = _elements
    model.component.sections = _section
    model.component.concepts = _concept
    model.component.materials = _material
    model.component.eccentricities = _eccentricity
    model.component.hinges = _hinge
    model.component.sets = _group
    model.component.vectors = _unit_vector
    #
    if _tubular_joint:
        model.component.joints.update(_tubular_joint)
    #
    #beam = model.component.concepts['8102_8101']
    #
    sacs_process.find_short_members(model, tol=0.010)    
    #
    # ----------------------------------------------------------------------
    # Find Overlapping Plates & add stiffeners
    #
    sacs_process.find_overlapping_shell(model, _stiffeners)
    #  
    # ----------------------------------------------------------------------
    #
    # clean groups
    _set_delete = []
    for key, _set in model.component.sets.items():
        _new_items = []
        for _item in _set.concepts:
            try:
                _concept[_item]
                _new_items.append(_item)
            except KeyError:
                continue
        #
        if not _new_items:
            _set_delete.append(key)
        else:
            _set.concepts = _new_items
    #
    for _set in _set_delete:
        del model.component.sets[_set]
    #
    #
    for key, sect in model.component.sections.items():
        if not 'beam' in sect.type:
            continue
        #
        if sect.name:
            continue
        # rename section
        sect.get_name(length_unit='metre')
    #  
    # ----------------------------------------------------------------------
    #                             LOAD SECTION
    # ----------------------------------------------------------------------
    #
    model.set_load()
    # ---------------------------
    # update equipment
    #if _areas:
    #    model.load.equipment.update(_areas)    
    #    
    read_sacs_load(_checkme_file, model,
                   _length_factor, _force_factor)
    #
    #read_combinations_level_0(_checkme_file, model,
    #                          design_condition)
    #
    print('--- End readSACS module')
    return model