# 
# Copyright (c) 2009-2020 fem2ufo
# 
# Python stdlib imports
import copy
import math 
import re
#import sys
#import os
from itertools import islice

#
# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.sacs.operations.common as sacs_common
#from fem2ufo.preprocess.sacs.operations.piles import read_piles

#
#
def get_spring(line, points, spring_factor, shift,
               _length_factor, _force_factor, 
               global_spring_factor=1.0, 
               displacement_factor=1.0):
    """
    """
    if points <= 5:
        _steps = points
        points = 0
    else:
        _steps = 5
        points -= 5
    #
    _force = []
    _disp = []
    _start = 17
    for _step in range(_steps):
        try:
            _force.append(float(line[_start:_start+6]) * spring_factor
                          * (_force_factor/_length_factor**2) * global_spring_factor)
        except ValueError:
            _force.append(0)
        
        try:
            _disp.append((float(line[_start+6:_start+12]) + shift) 
                         * _length_factor * displacement_factor)
        except ValueError:
            _disp.append(0)
        
        _start += 12
    #
    return _force, _disp, points
#
def get_curve(curve, _curve_points, fin, 
              _length, _force_factor, 
              _spring_factor, _shift=0,
              _global_spring_factor=1.0,
              _displacement_factor=1.0):
    """
    """
    line1 = list(islice(fin, 1))
    _test = _force_factor/_length**2
    #_curve_points
    _force, _disp,  _curve_points = get_spring(line1[0], _curve_points, 
                                               _spring_factor, _shift,
                                               _length, _force_factor,
                                               global_spring_factor = _global_spring_factor,
                                               displacement_factor=_displacement_factor)
    #
    curve.force = _force
    curve.displacement = _disp
    #
    while _curve_points:
        line2 = list(islice(fin, 1))
        _force, _disp,  _curve_points = get_spring(line2[0], _curve_points, 
                                                   _spring_factor, _shift,
                                                   _length, _force_factor,
                                                   global_spring_factor=_global_spring_factor,
                                                   displacement_factor=_displacement_factor)

        curve.force.extend(_force)
        curve.displacement.extend(_disp)
    #
    if curve.force[0] == 0.0 and curve.displacement[0] == 0.0:
        process.foundation.fill_spring(curve)
        

    #
#
def get_templayer(_layers, soil_name, line, _length_factor):
    """
    """
    try:
        _name = float(line[24:30].strip())
    except ValueError:
        _name = 0
    
    try:
        _layers[soil_name][_name]
    except KeyError:
        _layers[soil_name][_name] = {'top':None, 'bottom':None,
                                     'diametre':None, 'name':None,
                                     'PY':None, 'TZ':None, 'QZ':None}
        
        _layers[soil_name][_name]['top'] = _name * _length_factor
        
        try:
            _layers[soil_name][_name]['bottom'] = float(line[30:36]) * _length_factor
        except ValueError:
            pass
    
    return _name
#
def update_templayer(soil, spring, _layers, prefix, 
                     _length_factor, _spring_number,
                     _material, _soil_name):
    """
    """
    _temp_layer = [key for key in sorted(_layers.keys())]
    _layer_no = 0
    #
    for _index, _key in enumerate(_temp_layer):
        _layer = _layers[_key]
        try:
            _spr = copy.deepcopy(spring[_layer[prefix]])
            #_soil_name = _spr.name
            # check data
            if len(_spr.force) < 3:
                _spr.force = [0, 0, 0]
                _spr.displacement = [-1, 0, 1]
        except KeyError:
            #print('???')
            continue
        #
        try:
            _name = _temp_layer[_index+1]
            _layer['bottom'] = _layers[_name]['top']
        except IndexError:
            if _index == (len(_temp_layer) - 1):
                _name = _temp_layer[_index]
                _layer['bottom'] = _layers[_name]['bottom']
            else:
                continue
        #
        _layerdepth = round(_layer['top'], 3)
        try:
            _layer_thickness = abs(round(_layer['top'] - _layer['bottom'], 3))
            if _layer_thickness < 0.1:
                _layer['bottom'] = _layer['top'] + 0.1
                _layer_thickness = 0.1
                #print('here')
        except TypeError:
            print('   ** Warning soil {:} layer {:} --> bottom elevation missing'
                  .format(prefix, _key))
            continue
        #
        try:
            soil[_soil_name].layers[_layerdepth]
        except KeyError:
            _layer_no += 1 
            soil[_soil_name].layers[_layerdepth] = f2u.Foundation.Layer(_layerdepth, _layer_no)
            soil[_soil_name].layers[_layerdepth].depth = round(_layer['bottom'], 3)
            soil[_soil_name].layers[_layerdepth].thickness = _layer_thickness
            #
            if _layer['diametre']: 
                soil[_soil_name].diameter = _layer['diametre']
        #
        # adjusting curves from N/mm to force
        if 'PY' in prefix:
            _spr.force = [(point  * _length_factor) for point in _spr.force]
        # adjusting curves from N/mm to force
        elif 'TZ' in prefix:
            _spr.force = [point * (soil[_soil_name].diameter * math.pi)
                          for point in _spr.force]
        # adjusting curves from N/mm2 to force
        elif 'QZ' in prefix:
            _spr.force = [point * (soil[_soil_name].diameter**2 * math.pi / 4.0) if point > 0 else 0
                          for point in _spr.force]
        #
        _spring_number += 1
        _spr.name = prefix + "_" + _spr.name + "_" + str(_spring_number)
        _spr.number = _spring_number
        #
        _mat_name = _spr.name
        _material[_mat_name] = f2u.Material(_mat_name, _spring_number)
        _material[_mat_name].spring = _spr
        _material[_mat_name].type = 'spring'
        #
        soil[_soil_name].layers[_layerdepth].curve[prefix] = _spr       
#
def update_layers(soil, spring, _layers, prefix):
    """
    """
    _index = [_layers[key][prefix] for key in sorted(_layers.keys())]
    _new_list = []
    for x, _item in enumerate(_index):
        if _item:
            _new_list.append(_item)
        else:
            try:
                _index2 = next(s for s in _index[x:] if s)
            except StopIteration:
                _index2 = next(s for s in reversed(_index[:x]) if s)
            _new_list.append(_index2)
    #
    x = 0
    for key in sorted(_layers.keys()):
        _layers[key][prefix] = _new_list[x]
        x += 1
    #_layers
#
def fill_curve_gap(prefix, index, _layer_name, _layer, _soil):
    """
    """
    try:
        _spring = _layer.curve[prefix]
        _spring
        _force = _spring.force
        _steps = len(_force)
        
        if _steps > 25:
            _disp = _spring.displacement
            _rem = _steps//2
            if sum(_force[_rem:]) == 0:
                _spring.force = _force[_rem-11:_rem+12]
                _spring.displacement = _disp[_rem-11:_rem+12]
                #_steps = 12
            else:
                _redundant = []
                _max_neg = min(_force)
                _index_neg = _force.index(_max_neg)
                if _index_neg > 2: 
                    for x in range(1, _index_neg-1):
                        _redundant.append(x)
                    #
                #
                if len(_force[_index_neg:_rem]) > 9:
                    _diff = len(_force[_index_neg:_rem]) - 8
                    for x in range(_index_neg+1, _index_neg+_diff):
                        _redundant.append(x)
                    #print('here')
                #
                _max_pos = max(_force)
                _index_pos = _force.index(_max_pos)
                if (_steps - _index_pos) > 3:
                    for x in range(_max_pos+1, _steps-1):
                        _redundant.append(x)
                #
                if len(_force[_rem:_index_pos]) > 9:
                    _diff = len(_force[_rem+1:_index_pos]) - 8
                    for x in range(_rem+9, _rem+9+_diff):
                        _redundant.append(x)               
                #
                for _item in reversed(_redundant):
                    _spring.force.pop(_item)
                    _spring.displacement.pop(_item)
                #print('here')
    except KeyError:
        # bring spring from layer on top
        try:
            _index = index-1
            if _index < 0:
                get_next_layer(prefix, _layer, _layer_name, _soil, index)
            else:
                _name = _layer_name[_index]
                _layer.curve[prefix] = _soil.layers[_name].curve[prefix]
        # otherwise get spring from the next layer down (ie Qz)
        except KeyError:
            get_next_layer(prefix, _layer, _layer_name, _soil, index)
            #
            #for x in range(index, len(_layer_name)):
            #    _name = _layer_name[x]
            #    try:
            #        _layer.curve[prefix] = _soil.layers[_name].curve[prefix]
            #        break
            #    except KeyError:
            #        continue
#
#
def get_next_layer(prefix, _layer, _layer_name, _soil, index):
    """
    """
    for x in range(index, len(_layer_name)):
        _name = _layer_name[x]
        try:
            _layer.curve[prefix] = _soil.layers[_name].curve[prefix]
            break
        except KeyError:
            continue    
#
#
def read_psi(psi_file):
    """
    """
    # read psi file
    PSIinp = process.common.check_input_file(psi_file)
    if not PSIinp:
        raise IOError('    ** I/O error : psi file {:} not found'
              .format(psi_file))
        #sys.exit()
    #
    print('')
    print('------------------- REDSOIL Module -----------------')
    print('--- Reading PSI file')
    #
    _user_data = {}
    _user_data['PSI'] = f2u.InputUserData()
    _user_data['PSI'].file_name = PSIinp    
    #
    soil = {}
    _PY = {}
    _TZ = {}
    _QZ = {}
    _layers = {}
    _layer_item = {'top':None, 'botton':None,
                   'diametre':None}
    #
    _py_factor = 1.0
    _py_global_factor = 1.0
    _tz_factor = 1.0
    _z_global_factor = 1.0
    _tz_global_factor = 1.0
    _qz_factor = 1.0 
    #_qz_global_factor=1.0
    _zq_global_factor=1.0
    _precision_factor = 1.0
    #
    _isoil = 0
    _spring_number = 0
    #_layer_no = 0
    _flag = None
    
    with open(PSIinp) as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*|\-", line):
                continue
            #
            line = line.strip()
            if not line:
                continue
            #
            # read options
            if 'PSIOPT' in  line.upper():
                _units_in = line[9:12]
                _length_factor, _force_factor, _mass_factor = sacs_common.get_units(_units_in)
                # set units
                units = ["foot", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
                if 'MN' in _units_in.upper():
                    units = ["metre", "1.0*kilogram", "second", 'centigrade', "newton", "pascal"]
                #
                _length = sacs_common.get_length_factor(units)
                # check material density
                #mat_density = float(line[72:80])
                # check global system
                _vertical = line[7:9]
            #
            #elif 'PLGRUP' in line.upper():
            #    print(' fix PLGROUP')
            #
            elif 'TZAXIAL' in  line.upper():
                _soil_name = line[40:44]
                _flag = 'TZAXIAL'
                try:
                    soil[_soil_name]
                except KeyError:
                    _isoil += 1
                    soil[_soil_name] = f2u.Foundation.Soil(_soil_name, _isoil)
                #
                try:
                    _layers[_soil_name]
                except KeyError:
                    _layers[_soil_name] = {}
                #
                # t global factor
                _factor = line[27:33].strip()
                try:
                    _tz_global_factor = float(_factor)
                except ValueError:
                    try:
                        _factor = re.sub(r'(\-|\+)', r'E\1', _factor)
                        _tz_global_factor = float(_factor)
                    except ValueError:
                        pass
                #
                # z factor
                _factor = line[33:40].strip()
                try:
                    _z_global_factor = float(_factor)
                except ValueError:
                    try:
                        _factor = re.sub(r'(\-|\+)', r'E\1', _factor)
                        _z_global_factor = float(_factor)
                    except ValueError:
                        pass
            #
            elif 'SLOC' in  line.upper() and _flag == 'TZAXIAL':
                _curve_points = int(line[21:23])
                # t local factor
                _factor = line[38:44].strip()
                try:
                    _tz_factor = float(_factor)
                except ValueError:
                    try:
                        _factor = re.sub(r'(\-|\+)', r'E\1', _factor)
                        _tz_factor = float(_factor)
                    except ValueError:
                        pass
                #
                #soil[_soil_name].layers[_depth].depth = _depth
                #
                _spring_number += 1
                _tzNo = _spring_number
                _tz_name = _soil_name
                #
                _name = get_templayer(_layers, _soil_name, line, _length_factor)
                #
                #if _curve_points >= 15:
                #    print('-->')
                #
                _TZ[_tzNo] = f2u.Geometry.SpringElement(_tz_name, _name)
                get_curve(_TZ[_tzNo], _curve_points, fin, 
                          _length, _force_factor, 
                          _spring_factor=_tz_factor,
                          _global_spring_factor=_tz_global_factor,
                          _displacement_factor=_z_global_factor)
                #
                _layers[_soil_name][_name]['TZ'] = _tzNo # _TZ[_tzNo]
            #
            elif 'BEARING' in  line.upper():
                _soil_name = line[40:44]
                _flag = 'BEARING'
                try:
                    soil[_soil_name]
                except KeyError:
                    _isoil += 1
                    soil[_soil_name] = f2u.Foundation.Soil(_soil_name, _isoil)
                #
                try:
                    _layers[_soil_name]
                except KeyError:
                    _layers[_soil_name] = {}
                #
                # z global factor
                _factor = line[33:40].strip()
                try:
                    _zq_global_factor = float(_factor)
                except ValueError:
                    try:
                        _factor = re.sub(r'(\-|\+)', r'E\1', _factor)
                        _zq_global_factor = float(_factor)
                    except ValueError:
                        pass               
            #
            elif 'SLOC' in  line.upper() and _flag == 'BEARING':
                _curve_points = int(line[21:23])
                # local q factor
                _factor = line[38:44].strip()
                try:
                    _qz_factor = float(_factor)
                except ValueError:
                    try:
                        _factor = re.sub(r'(\-|\+)', r'E\1', _factor)
                        _qz_factor = float(_factor)
                    except ValueError:
                        pass
                #
                _spring_number += 1
                _qzNo = _spring_number
                _qz_name = _soil_name 
                
                _name = get_templayer(_layers, _soil_name, line, _length_factor)
                
                _QZ[_qzNo] = f2u.Geometry.SpringElement(_qz_name, _name)
                get_curve(_QZ[_qzNo], _curve_points, fin, 
                          _length, _force_factor, 
                          _spring_factor=_qz_factor,
                          _displacement_factor=_zq_global_factor)
                
                _layers[_soil_name][_name]['QZ'] = _qzNo # _QZ[_qzNo]
            #
            elif 'LATERAL' in  line.upper():
                _soil_name = line[40:44]
                _diam_ref = float(line[27:33])
                _flag = 'LATERAL'
                try:
                    soil[_soil_name]
                except KeyError:
                    _isoil += 1
                    soil[_soil_name] = f2u.Foundation.Soil(_soil_name, _isoil)
                #
                try:
                    _layers[_soil_name]
                except KeyError:
                    _layers[_soil_name] = {}
                # 
                # global p factor
                _factor = line[33:40].strip()
                try:
                    _py_global_factor = float(_factor)
                except ValueError:
                    try:
                        _factor = re.sub(r'(\-|\+)', r'E\1', _factor)
                        _py_global_factor = float(_factor)
                    except ValueError:
                        pass
            #
            elif 'SLOCSM' in  line.upper() and _flag == 'LATERAL':
                _curve_points = int(line[21:23])
                # local p factor
                _factor = line[36:40].strip()
                try:
                    _py_factor = float(_factor)
                except ValueError:
                    try:
                        _factor = re.sub(r'(\-|\+)', r'E\1', _factor)
                        _py_factor = float(_factor)
                    except ValueError:
                        pass
                # shift
                try:
                    _shift = float(line[40:44])
                except ValueError:
                    _shift = 0
                # high precision factor
                try:
                    _precision_factor = float(line[69:76])
                except ValueError:
                    pass
                #
                # final p factor
                _py_factor *= _precision_factor
                #
                #
                _spring_number += 1
                _pyNo = _spring_number
                _py_name = _soil_name
                
                _name = get_templayer(_layers, _soil_name, line, _length_factor)
                _layers[_soil_name][_name]['diametre'] = _diam_ref * _length
                
                _PY[_pyNo] = f2u.Geometry.SpringElement(_py_name, _name)
                get_curve(_PY[_pyNo], _curve_points, fin, 
                          _length, _force_factor, 
                          _spring_factor=_py_factor, 
                          _shift=_shift,
                          _global_spring_factor=_py_global_factor)
                
                _layers[_soil_name][_name]['PY'] = _pyNo # _PY[_pyNo]
            #print('tzaxial', line)
    #
    _material_spring = {}
    for key, _layer in _layers.items():
        #if 'SL50' in key:
        #    print('-->')
        _spring_number = 0
        update_templayer(soil, _PY, _layer, 'PY', _length, 
                         _spring_number, _material_spring, key)
        update_templayer(soil, _TZ, _layer, 'TZ', _length, 
                         _spring_number, _material_spring, key)
        update_layers(soil, _QZ, _layer, 'QZ')
        update_templayer(soil, _QZ, _layer, 'QZ', _length, 
                         _spring_number, _material_spring, key)
    #
    #
    for _soil in soil.values():
        _layer_name = [key for key in sorted(_soil.layers.keys())]
        for _index, _key in enumerate(_layer_name):
            _layer = _soil.layers[_key]
            fill_curve_gap('PY', _index, _layer_name, _layer, _soil)
            fill_curve_gap('TZ', _index, _layer_name, _layer, _soil)
            fill_curve_gap('QZ', _index, _layer_name, _layer, _soil)
            #
            _layer.diameter = _soil.diameter
            _layer.number = _index + 1
    #
    print('--- End READ SOIL module')
    return soil, _material_spring, _user_data