# 
# Copyright (c) 2009-2018 fem2ufo
# 


# Python stdlib imports
import re
import sys
import os
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.sacs.operations.common as sacs_common


#
def load_title(lineIn):
    """
    """
    lineOut = lineIn
    lineOut = re.sub(r"\(|\)|\[|\]|\{|\}|\,|\;", "_", lineOut)
    lineOut = re.sub(r"\-|\+|\%|\s|\=", "_", lineOut)
    lineOut = re.sub(r"\&", "and", lineOut)
    lineOut = re.sub(r"\/|\'", "", lineOut)
    lineOut = re.sub(r'\"', '', lineOut)
    lineOut = re.sub(r"\.", "dot", lineOut)
    lineOut = re.sub(r"\_+", "_", lineOut)
    lineOut = re.sub(r"\?", "Qmark", lineOut)
    lineOut = re.sub(r"\@", "at", lineOut)
    lineOut = re.sub(r"\*", "x", lineOut)
    lineOut = re.sub(r"\#", "no", lineOut)
    lineOut = re.sub(r"\°", "deg", lineOut)
    lineOut = lineOut.strip()
    lineOut = lineOut.strip("_")

    try:
        int(lineOut[0])
        lineOut = 'BL_' + lineOut
    except IndexError:
        return None
    except ValueError:
        pass

    return lineOut
#
def get_load_type(lineIn):
    """
    """
    _key = {"member_distributed": r"unif",
            "member_moment": r"dmom",
            "member_point": r"conc|momt",
            "temperature": r"temp",
            "node": r"join",
            "plate_pressure": r"pres",
            "plate_temperature": r"ptem",
            "shell_pressure": r"spg",
            "shell_distributed": r"spc",
            "shell_temperature": r"stc|stm|stt|stb"}

    keyWord = process.common.find_keyword(lineIn, _key)
    #
    return keyWord
#
#
def get_value(item,  factor):
    """ """
    try:
        return float(item) * factor
    except ValueError:
        try:
            item = re.sub(r'(\-|\+)', r'E\1', item)
            return float(item) * factor
        except ValueError:
            return 0
#
def get_nodal_load(line, length_moment, force_factor):
    """
    """
    loadlist = [0, 0, 0, 0, 0, 0]
    # x
    item = line[16:23].strip()
    loadlist[0] = get_value(item, force_factor)
    # y
    item = line[23:30].strip()
    loadlist[1] = get_value(item, force_factor)
    # z
    item = line[30:37].strip()
    loadlist[2] = get_value(item, force_factor)
    # mx
    item = line[37:44].strip()
    loadlist[3] = get_value(item, force_factor * length_moment)
    # my
    item = line[44:52].strip()
    loadlist[4] = get_value(item, force_factor * length_moment)
    # mz
    item = line[52:59].strip()
    loadlist[5] = get_value(item, force_factor * length_moment)
    #
    return loadlist
#
#
def get_nodal_displacement(line, length_factor):
    """
    """
    loadlist = [0, 0, 0, 0, 0, 0]
    # x
    item = line[11:18].strip()
    loadlist[0] = get_value(item, length_factor)
    # y
    item = line[18:25].strip()
    loadlist[1] = get_value(item, length_factor)
    # z
    item = line[25:32].strip()
    loadlist[2] = get_value(item, length_factor)
    # mx
    item = line[32:39].strip()
    loadlist[3] = get_value(item, 1.0)
    # my
    item = line[39:46].strip()
    loadlist[4] = get_value(item, 1.0)
    # mz
    item = line[46:53].strip()
    loadlist[5] = get_value(item, 1.0)
    #
    return loadlist
#
def read_sacs_load(file_name, model,
                   _length_factor=False, 
                   _force_factor=False):
    """
    """
    # read SACS input file
    sacs_file = process.common.check_input_file(file_name)
    if not sacs_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())        
    #
    print(' ')
    print('--- Reading loading from {:} input file'.format(file_name))
    print(' ')
    #
    print('    * First pass')
    dat_temp = open('loadTemp.dat','w+')
    _load_number = 0
    _DOFu = {'x':0, 'y':1, 'z':2}
    _DOFm = {'x':3, 'y':4, 'z':5}
    _flag = None
    #
    with open(sacs_file) as fin:
        for line in fin:
            #
            # Jump commented lines
            if re.match(r"\*|\-", line):
                continue
            #
            line = line.strip()
            if not line:
                continue
            #
            # Segment line
            keyword = line.split()
            # Flag word
            if line[0]:
                _flag = keyword[0]
            #
            if _flag.upper() == 'OPTIONS':
                _units_in = line[13:15]
                _length_factor, _force_factor, _mass_factor = sacs_common.get_units(_units_in)
                
                units = ["inch", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
                if  "MN" in _units_in:
                    units = ["metre", "1.0*kilogram", "second", 'centigrade', "kilonewton", "pascal"]
                elif  "ME" in _units_in:
                    units = ["centimetre", "1.0*kilogram", "second", 'centigrade', "newton", "pascal"]
                #
                _length_2, _force_factor2 = process.units.convert_units_SI(units[0],  units[4])
                #_length = sacs_common.get_length_factor(_units_in)
            #
            if line[0:6] == 'LOADCN':
                """
                """
                if not "number:" in line:
                    _load_name = line[6:10].strip()
                    #
                    try:
                        _load_number = int(_load_name)
                    except ValueError:
                        _load_number += 1
                    #
                    if not _load_name:
                        _load_name = str(_load_number)                  
                    #
                    #
                    dat_temp.write("\n")
                    dat_temp.write('LOADCN number: {:} name: {:}'
                                   .format(_load_number, _load_name))
                else:
                    #print('herre')
                    _load_name = keyword[3].strip()
                    _load_number = int(keyword[1])
                    #
                    dat_temp.write("\n")
                    dat_temp.write(line)                    
                #                
                #
                # keep load numbering
                #dat_temp.write("\n")
                #dat_temp.write(line)
            #
            #elif _flag.upper() == 'DEAD':
            #    _location = line[10:12].strip()
            #    if not _location:
            #        continue
            #    
            #    _grav = get_gravity(_location)
            #    
            #    try:
            #        model.load.functional[_load_name]
            #        #_grav = get_gravity(_location)
            #    except KeyError:
            #        #_grav = get_gravity(_location)
            #        #_load_no += 1
            #        model.load.functional[_load_name] = f2u.Load.Basic('gravity', _load_no)
            #    
            #    model.load.functional[_load_name].gravity.extend(_grav)
            #
            elif _flag.upper() == 'LOAD':
                _type = line[65:69].strip()
                if not _type:
                    continue
                #
                _global = line[60:64].strip()
                _type = get_load_type(_type)
                _comment = load_title(line[72:80].strip())
                #if 'CR_E_MM4' in _comment :
                #    print('--->')
                #
                # create basic load if not found
                try:
                    model.load.functional[_load_name]
                    #print('   ** warning load {:} already exist'.format(_load_name))
                except KeyError:
                    _new_load_name = load_title(_load_name)
                    model.load.functional[_load_name] = f2u.Load.Basic(_new_load_name, _load_number)                
                #
                #
                if _global.lower() != 'glob':
                    #_type = get_load_type(_global)
                    #if not _type:
                    _global = 'local'
                    print(' ---> correct here : local system')
                    #1.0/0
                #
                if _type == 'node':
                    _node_name = line[7:12].strip()
                    try:
                        _node = model.component.nodes[_node_name]
                    except KeyError:
                        dat_temp.write("\n")
                        dat_temp.write("   *** warning node {:} not found in line {:}"
                                       .format(_node_name, line))
                        continue
                    
                    _loadlist = get_nodal_load(line, _length_2, _force_factor)
                    
                    try:
                        _node.load[_load_name].point.append(_loadlist)
                    except KeyError:
                        if not _comment:
                            _comment = _node_name
                        else:
                            _comment = _comment + '_' + _node_name
                        #
                        _node.load[_load_name] = f2u.Load.Node(_comment, _node_name)
                        _node.load[_load_name].point.append(_loadlist)
                        #
                        model.load.functional[_load_name].node.append(_node_name)
                    
                    #_loadTitle = line[72:80].strip()
                    #if _loadTitle:
                    #    model.load.functional[_loadCaseNo].name = _loadTitle
                #
                elif 'member' in _type :
                    try:
                        _memb_name = line[7:11].strip() + '_' + line[11:15].strip()
                        _mem = model.component.concepts[_memb_name]
                        _end_1 = 0
                        _end_2 = 1
                    except KeyError:
                        try:
                            _memb_name = line[11:15].strip() + '_' + line[7:11].strip()
                            _mem = model.component.concepts[_memb_name]
                            _end_1 = 1
                            _end_2 = 0
                        except KeyError:
                            print("    ** Warining member {:} no found".format(_memb_name))
                            continue
                    #
                    #if _memb_name == '1012_9021':
                    #    print('here')
                    #
                    _dir_load = line[5].lower()
                    if not _comment:
                        _comment = _memb_name
                    else:
                        _comment = _comment +'_'+ _memb_name
                    #
                    udl = f2u.Load.BeamUDL(_comment, _load_name)
                    udl.beam_length = _mem.flexible_length
                    #
                    _L = [0, 0]
                    _loadlist = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
                    
                    if _type == 'member_point':
                        udl.type = 2  # Simulated concentrated force, conservative load
                        _flag = line[65:69].strip()
                        #
                        if 'conc' in _flag.lower():
                            try:
                                _distance = float(line[16:23].strip()) *  _length_factor
                                if _end_1 == 0:
                                    _L[0] =  _distance
                                else:
                                    _L[0] = _mem.length_node2node - _distance
                                #
                                try:
                                    _udl_1 = float(line[23:30].strip()) * _force_factor
                                    _udl_2 = 0
                                    _force = [_udl_1, _udl_2]
                                    _loadlist[0][_DOFu[_dir_load]] = float(_force[0])
                                    _loadlist[1][_DOFu[_dir_load]] = 0
                                except ValueError:
                                    print('    *** warning no load defined in ', _comment)
                            # TODO : check if this is ok
                            # load appears at the member end, so it is converted to nodal load instead
                            except ValueError:
                                _node = _mem.node[_end_1]
                                _load_node = [0, 0, 0, 0, 0, 0]
                                _load_node[_DOFu[_dir_load]] = float(line[23:30].strip()) * _force_factor
                                
                                try:
                                    _node.load[_load_name].point.append(_load_node)
                                except KeyError:
                                    #if not _comment:
                                    #    _comment = _node.name
                                    #else:
                                    #    _comment = _comment +'_'+ _node.name
                                    #
                                    _node.load[_load_name] = f2u.Load.Node(_comment, _node.name)
                                    _node.load[_load_name].point.append(_load_node)
                                    #
                                    model.load.functional[_load_name].node.append(_node.name)
                                #
                                continue
                        else:
                            try:
                                _distance = float(line[45:52].strip()) *  _length_factor
                                #_L[_end_1] = _distance *  _length_factor
                                if _end_1 == 0:
                                    _L[0] =  _distance 
                                else:
                                    _L[0] = _mem.length_node2node - _distance
                                
                                _udl_1 = float(line[52:59].strip()) * _force_factor * _length_2
                                _udl_2 = 0
                                _force = [_udl_1, _udl_2]
                                _loadlist[0][_DOFm[_dir_load]] = float(_force[0])
                                _loadlist[1][_DOFm[_dir_load]] = 0
                            # TODO : check if this is ok
                            # load appears at the member end, so it is converted to nodal load instead
                            except ValueError:
                                _node = _mem.node[_end_1]
                                _load_node = [0, 0, 0, 0, 0, 0]
                                _load_node[_DOFm[_dir_load]] = float(line[52:59].strip()) * _force_factor * _length_2
                                
                                try:
                                    _node.load[_load_name].point.append(_load_node)
                                except KeyError:
                                    #if not _comment:
                                    #    _comment = _node.name
                                    #else:
                                    #    _comment = _comment +'_'+ _node.name
                                    #
                                    _node.load[_load_name] = f2u.Load.Node(_comment, _node.name)
                                    _node.load[_load_name].point.append(_load_node)
                                    #
                                    model.load.functional[_load_name].node.append(_node.name)
                                #
                                continue
                        #
                    # distributed
                    else:
                        # begining lod
                        try:
                            _udl_1 = float(line[23:30].strip())
                        except ValueError:
                            _udl_1 = 0
                        # final load
                        try:
                            _udl_2 = float(line[37:44].strip())
                        except ValueError:
                            _udl_2 = 0
                        #
                        _force = [_udl_1, _udl_2]
                        #
                        # end 1
                        try:
                            _L[_end_1] = float(line[16:23].strip()) *  _length_factor
                        except ValueError:
                            pass
                        #
                        # end 2
                        try:
                            _distance = float(line[30:37].strip()) *  _length_factor
                            _length2node = (_mem.length_node2node - _L[_end_1] - _distance)
                            _L[_end_2] = _length2node
                        except ValueError:
                            pass
                        #
                        if _type == 'member_distributed':
                            udl.type = 1 # True distributed force, conservative load
                            _loadlist[_end_1][_DOFu[_dir_load]] = float(_force[_end_1]) * _force_factor / _length_factor
                            _loadlist[_end_2][_DOFu[_dir_load]] = float(_force[_end_2]) * _force_factor / _length_factor
                        # moment uniform
                        else:
                            udl.type = 1 # True distributed force, conservative load
                            _loadlist[_end_1][_DOFm[_dir_load]] = float(_force[_end_1]) * _force_factor * _length_2 / _length_factor
                            _loadlist[_end_2][_DOFm[_dir_load]] = float(_force[_end_2]) * _force_factor * _length_2 / _length_factor
                    #
                    #
                    udl.input_udl(_loadlist, _L)                    
                    #
                    try:
                        _mem.load[_load_name]
                    except KeyError:
                        #if not _comment:
                        #    _comment = _memb_name
                        #else:
                        #    _comment = _comment +'_'+ _memb_name
                        #
                        _mem.load[_load_name] = f2u.Load.Element(_comment, _memb_name)
                    
                    # Simulated concentrated force
                    if udl.type == 2:
                        _mem.load[_load_name].point.append(udl)
                    else:
                        _mem.load[_load_name].distributed.append(udl)
                    
                    model.load.functional[_load_name].concept.append(_memb_name)
                #
                elif _type == 'temperature':
                    print(' --> fix temperature load')
                
                elif 'plate' in _type:
                    #print(' --> fix plate load')
                    _memb_name = line[7:11].strip()
                    try:
                        _mem = model.component.concepts[_memb_name]
                    except KeyError:
                        print("    ** Warining member {:} no found".format(_memb_name))
                        continue
                    #
                    _direction = 1
                    if '-' in line[5:6].strip():
                        _direction = -1
                    
                    _pressurelist = [float(line[16:23].strip()) * _direction * _force_factor / _length_factor]
                    
                    try:
                        _pressurelist.append(float(line[23:30].strip()) * _direction * _force_factor / _length_factor)
                        _pressurelist.append(float(line[30:37].strip()) * _direction * _force_factor / _length_factor)
                        try:
                            _pressurelist.append(float(line[37:44].strip()) * _direction * _force_factor / _length_factor)
                        except ValueError:
                            print(' 3 node plate?')
                            pass
                    except ValueError:
                        for _step in range(1, len(_mem.node)):
                            _pressurelist.append(_pressurelist[0])
                    
                    try:
                        _mem.load[_load_name].pressure.append(_pressurelist)
                    except KeyError:
                        _mem.load[_load_name] = f2u.Load.Element(_comment, _memb_name)
                        _mem.load[_load_name].pressure.append(_pressurelist)
                    
                    model.load.functional[_load_name].shell.append(_memb_name)                    
                
                elif 'shell' in _type:
                    print(' --> fix shell load')
                
                else:
                    print('   *** error load not identified: {:}'.format(line))                    
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    #
    dat_temp.close()
    #
    print('    * Second pass')
    _checkme_file = 'CheckMe_' + sacs_file.replace('CheckMe_', '')
    os.remove(sacs_file)
    dat_temp = open(_checkme_file,'w+')
    #dat_temp = open('inpTemp2.dat','w+')
    #
    with open('loadTemp.dat') as fin:
        for line in fin:
            #
            line = line.strip()
            if not line:
                continue
            #
            # Segment line
            keyword = line.split()
            # Flag word
            if line[0]:
                _flag = keyword[0]
            #
            if _flag.upper() == 'JOINT':
                _node_name = line[6:10].strip()
                _load_name = line[68:72].strip()
                _comment = load_title(line[61:68].strip())
                
                try:
                    _node = model.component.nodes[_node_name]
                except KeyError:
                    dat_temp.write("\n")
                    dat_temp.write("   *** warning node {:} not found in line {:}"
                                   .format(_node_name, line))
                    continue
                
                if "PERSET" in line[54:61].strip():
                    if "EN" in _units_in:
                        _units = ["inch", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
                    else:
                        _units = ["centimetre", "1.0*kilogram", "second", 'centigrade', "newton", "pascal"]
                    #
                    _length_perset, _force_perset = process.units.convert_units_SI(_units[0],  _units[4])                    
                    #print(' *** warning load displacement no yet implemented')
                    _displist = get_nodal_displacement(line, _length_perset)
                    #_displist
                    try:
                        _node.load[_load_name].displacement.append(_displist)
                    except KeyError:
                        if not _comment:
                            _comment = 'disp_node_' + _node_name
                        else:
                            _comment = _comment + '_disp_node_' + _node_name
                        #
                        _node.load[_load_name] = f2u.Load.Node(_comment, _node_name)
                        _node.load[_load_name].displacement.append(_displist)
                        #
                        model.load.functional[_load_name].node.append(_node_name)
                        #
                        _memb_name = 'spr_' + _node_name
                        _mem = model.component.concepts[_memb_name]
                        #_boundary = model.component.boundaries[_node_name]
                        for x, _degree in enumerate(_displist):
                            if _degree != 0.0:
                                _mem.material[0].stiffness[0][x] = 'Prescribed'
                    #continue
                #elif "ELASTI" in line[54:61].strip():
                #    print(' *** warning spring support no yet implemented')
            #
            elif line[0:6] == 'LOADLB':
                _load_name = line[6:10].strip()
                _comment = load_title(line[10:])
                try:
                    model.load.functional[_load_name].name =  _comment +'_'+ str(_load_name)
                except KeyError:
                    dat_temp.write("\n")
                    dat_temp.write(line)
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)            
    #
    # updating load number
    _load_no = [_item.number for _item in model.load.functional.values()]
    _load_no = max(_load_no)
    for _item in model.load.functional.values():
        if _item.number == 0:
            _load_no += 1
            _item.number = _load_no
    #
    #
    dat_temp.close()
    os.remove('loadTemp.dat')
  