# 
# Copyright (c) 2009-2020 fem2ufo
# 


# Python stdlib imports
import sys
import os
#

# package imports
#import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
from fem2ufo.preprocess.sacs.model.geometry import read_sacs_geometry
from fem2ufo.preprocess.sacs.model.load import read_sacs_load
from fem2ufo.preprocess.common.foundation import read_foundation
from fem2ufo.preprocess.sacs.operations.piles import read_piles
from fem2ufo.preprocess.sacs.model.seastate import read_seastate
# from fem2ufo.preprocess.sacs.model.combinations import read_sacs_load_combinations
import fem2ufo.preprocess.sacs.operations.process as sacs_process
import fem2ufo.preprocess.sacs.operations.concept as sacs_concept
#
#
#
class SACS_model:
    """Class reads SACS files and place data in fem2ufo model concept"""
    #
    def __init__(self, tol=False):
        """
        **Parameters**:
           :tol: set tolerance to identied supernode at pile head 
        """
        #
        self.component = {}
        self.format = 'sacs'
        self.tolerance = tol
        self.model = {}
        self.check_out = []
        # sacs models are converted to SI units
        self.units_in = process.units.set_SI_units()
        self.units_out = process.units.set_SI_units()
        self.gravity = False
        #
        self.model = {}
        self.gemetry_file = False
        self.loading_file = False
        self.foundation_file = False
        self.metocean_file = False
        #
        self.concept_name = True
        self.design_condition = 'operating'
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """
        try:
            path = os.path.normcase(path)
            os.chdir(path)
        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()

        retval = os.getcwd()
        print('')
        print('--- Current working directory :')
        print('--- {:}'.format(retval))
    #
    def units(self, **kwargs):
        """
        Global units must be defined:
        Units [length, mass, time, temperature, force, pressure/stress]/n
        
        **Parameters**:
          :length : [mandatory]\n
          :force: [mandatory]\n
          :mass : [mandatory]\n
          :temperature: \n
          :gravity: [default : 9.81ms^2]\n
        """

        for key, value in kwargs.items():
            _unit = process.find_unit_case(key)
            self.units_in = process.units_module(_unit, value,
                                                 self.units_in)

    #
    def geometry(self, file_name, 
                 design_condition=None,
                 library=None):
        """
        **Parameters**:  
           :file_name : sacs geometry file
        """
        self.gemetry_file = file_name
        if design_condition:
            self.design_condition = design_condition
        # read data 
        self.model = read_sacs_geometry(self.gemetry_file, 
                                        self.design_condition,
                                        library)
        #
        # get metocean data and load combination
        _checkme_file = 'CheckMe_' + file_name
        read_seastate(_checkme_file, self.model, 
                      self.design_condition)
        #
        self.model.data['format'] = 'sacs'
        #
    #
    def loading(self, loading):
        """
        **Parameters**:
           :load: L1.FEM file
        """
        self.loading_file = loading

        if not self.model.component:
            print('  error --> geometry not found')
            sys.exit()
        read_sacs_load(self.loading_file, self.model)    
    #
    def foundation(self, foundation_file,
                   file_format=False, foundation_units=False):
        """
        **Parameters**:  
           :foundation: GENSOD.lis file  
           :Format: sesam (default) 
           :units : [length, mass, time, temperature, force, pressure/stress]
        """
        #
        self.foundation_file = foundation_file

        if not file_format:
            file_format = self.format

        if not foundation_units:
            foundation_units = self.units_in
        #
        if not self.units_in[0] and self.units_in[4]:
            print('  **  error input units not provided')
            print('      process terminated')
            sys.exit()        
        #
        # read foundatios
        read_foundation(self.model,
                        foundation_file,
                        file_format, 
                        foundation_units,
                        self.units_out)

        self.model.component.data.update(self.model.foundation.data)
        #
        # TODO : quick fix to get mudline set at pilehead elevation
        #
        #
        _lev = []
        #for _jnt in self.model.component.joints.values():
        #    if _jnt.type == 'pile_head':
        #        _lev.append(_jnt.node.coordinates.z)
        #
        for key, _node in self.model.component.nodes.items():
            try:
                if _node.boundary.type == 'link':
                    _lev.append(_node.coordinates.z)
                    #_bound_node[key] = _node.boundary
            except AttributeError:
                continue        
        #
        #
        # FIXME : find permament fix
        #self.model.analysis.case[self.gemetry_file].condition = f2u.Metocean.Condition(self.gemetry_file, 1)
        #self.model.analysis.case[self.gemetry_file].condition.mudline = max(_lev)
        #
        #        
        # find piles if sacs file provide
        if 'sacs' in file_format.lower():
            self.model.foundation.piles = read_piles(foundation_file,
                                                     self.model.component.nodes)
            #
            # FIXME : quick fix for foundation mudline
            for _soil in self.model.foundation.soil.values():
                _soil.mudline = round(sum(_lev)/len(_lev), 4)
            #
            #
            #
            # set master node tolerance
            if not self.tolerance:
                try:
                    self.tolerance = process.units.get_tolerance(self.units_in[0])
                except SyntaxError:
                    print('  **  error input units not provided')
                    print('      process terminated')
                    sys.exit()            
            # Find  twin node for pile head
            process.geometry.find_super_node_New(self.model.component, 
                                                 self.model.foundation.piles,
                                                 self.tolerance)
        #
        #print('here soil')
    #
    def piles(self, foundation_file):
        """
        """
        # read piles
        piles = read_piles(foundation_file,
                           self.model.component.nodes)
    #
    def metocean(self, metocean, 
                 design_condition=None):
                 #analysis_name=False):
        """
        **Parameters**:
           :metocean : seastate.inp
        """
        self.metocean_file = metocean
        if design_condition:
            self.design_condition = design_condition        

        if not self.model.component:
            print('  error --> geometry not found')
            sys.exit()

        read_seastate(metocean, self.model, self.design_condition) #,
                      #, analysis_name)
                      #self.format,
        #
        #self.model.component.data.update(self.model.metocean.data)
        #
        #print('here meto')
    #
    def get_model(self, meshing=False):
        """
        Processing model --> mandatory
        """       
        # 
        print('')
        print('--------------- Updating SACS Model ----------------')    
        #
        # update materials
        #
        # Rename material model concepts
        process.material.update_material_name(self.model.component.materials,
                                              self.units_in)        
        #
        # Rename material pile concepts
        try:
            process.material.update_material_name(self.model.foundation.piles.materials,
                                                  self.units_in)
        except AttributeError:
            pass
        #
        #
        # common_process.update_sets(self.model.component.sets)
        #
        # simplify sections (Probably not required)
        process.sections.simplify_sections(self.model.component.sections,
                                           self.model.component.concepts,
                                           self.units_in)
        #
        #
        #for key, _member in self.model.component.concepts.items():
            #_name = _member.number
            #if self.concept_name:
            #    _name = key
            #
            #if _member.type == 'plate':
            #    _member.name = 'Plt_' + str(_name)
            #
            #elif _member.type == 'pile':
            #    _member.name = 'Pile_' + str(_name)
            #
            #else:
            #    _member.name = 'Bm_' + str(_name)
        #
        # find cones
        try:
            _cone_group = self.model.component.sets['cones_new_set']
            sacs_process.get_cones(_cone_group, self.model)
        except KeyError:
            pass
        #        
        #
        # check units
        #
        if self.units_out.count(None) == len(self.units_out):
            self.units_out = self.units_in
        #
        self.model.data['units'] = self.units_out
        #
        #
        # clean model
        #
        #self.model = process.clean_model(self.model)
        #
        # updating load name
        try:
            #self.model.analysis        
            prefix_level = {0:'LCA', 1:'LCF'}
            for key, _analysis in self.model.analysis.case.items():
                for _level, _combinations in _analysis.load_combination.items():
                    for _key, _combination in _combinations.items():
                        if prefix_level[_level] in _combination.name:
                            continue
                        elif prefix_level[_level] in _key:
                            _combination.name = _key
                        else:
                            _combination.name = prefix_level[_level] + str(_combination.number).zfill(3) + '_' + str(_combination.name)
        except AttributeError:
            pass
        #
        # Meshing
        if meshing:
            concepts = self.model.component.concepts
            nodes = self.model.component.nodes
            group = self.model.component.sets
            elements, vectors = sacs_concept.mesh_concepts(concepts, nodes, group)        
            #
            loading = self.model.load
            sacs_concept.update_load(concepts, elements, nodes, loading)
            #
            self.model.component.elements = elements
            self.model.component.vectors = vectors
        #
        print('')
        print('----------------- END SACS Module -----------------')         
        #
        return self.model