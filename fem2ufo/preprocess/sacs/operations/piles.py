# 
# Copyright (c) 2009-2016 fem2ufo
# 
# Python stdlib imports
import re
#import sys
import os
from math import isclose
#
# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.sacs.operations.common as sacs_common
from fem2ufo.process.modules.euclid import Point3
import copy
#
#
#
#
def read_piles(psi_file, nodes):
    """
    """
    # read psi file
    PSIinp = process.common.check_input_file(psi_file)
    
    if not PSIinp:
        print('    ** I/O error : T.FEM file {:} not found'
              .format(psi_file))
        sys.exit()
    #
    #
    _property = {}
    _section = {}
    _group = {}
    _concept = {}
    _material = {}
    _eccentricity = {}
    #
    mat_density = 0
    _mat_no = 0
    _sect_no = 0
    _group_no = 0
    _memb_no = 0
    _node_no = 0
    #
    # Create pile model
    model = f2u.F2U_model('piles', 3)
    model.set_component()    
    #
    print('    * Pile Geometry')
    dat_temp = open('pileTemp.dat','w+')
    #
    with open(PSIinp) as fin:
        for line in fin:
            # Jump commented lines
            if re.match(r"\*|\-", line):
                continue
            #
            line = line.strip()
            if not line:
                continue
            #
            # read options
            if 'PSIOPT' in  line.upper():
                _units_in = line[9:12]
                _length_factor, _force_factor, _mass_factor = sacs_common.get_units(_units_in)
                # set units
                units = ["foot", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
                if 'M' in _units_in.upper():
                    units = ["metre", "1.0*kilogram", "second", 'centigrade', "newton", "pascal"] 
                #
                _length = sacs_common.get_length_factor(units)
                # check material density
                mat_density = float(line[72:80])
                # check global system
                _vertical = line[7:9]
                #
                dat_temp.write("\n")
                dat_temp.write(line)                
            #
            # Read sections
            elif 'PLSEC' in line.upper():
                _sect_name = line[7:14].strip()
                if not _sect_name:
                    continue
                #
                _shape = sacs_common.section_word(line[15:18])
                _prop_name = sacs_common.get_name(_sect_name, prefix='S_')

                _property[_sect_name] = f2u.SecProp.SectionProperty(_shape, 
                                                                    _prop_name)
                
                _factor =  sacs_common.get_length_factor(units)
                shape_data(line, _property, _sect_name, _shape,
                           _factor)
            #          
            else:
                dat_temp.write("\n")
                dat_temp.write(line)            
    #
    dat_temp.close()
    #
    #
    print('    * Second pass')
    dat_temp = open('pileTemp2.dat','w+')
    #
    with open('pileTemp.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue
            #
            # Read Groups/material/sections
            # Pile
            if 'PLGRUP' in line.upper():
                _group_name = line[7:10].strip()
                if not _group_name:
                    continue
                #
                # property
                _prop_name = line[11:18].strip()
                #
                if _prop_name:
                    try:
                        _shape = _property[_prop_name].shape
                    except KeyError:
                        _shape = process.common.get_section_library(_prop_name, _property, library)
                # new tubular section
                else:
                    _factor = sacs_common.get_length_factor(units)
                    _shape = 'tubular'
                    _diam = line[19:25].strip() 
                    _thk  = line[25:31].strip()
                    _prop_name = _diam + "P" +_thk
                    _sect_name = sacs_common.get_name(_prop_name, prefix='S_')
                    #
                    _property[_prop_name] = f2u.SecProp.SectionProperty(_shape, 
                                                                        _sect_name)
                    _property[_prop_name].input_data(float(_diam) * _factor, 
                                                     float(_thk) * _factor)
                #
                # material
                try:
                    _Emod = float(line[31:37].strip())
                except ValueError:
                    _Emod = 29
                    if 'KN' in _units_in:
                        _Emod *= 0.689
                    elif 'KG'  in _units_in:
                        _Emod *= 689.476
                    print('    *** warning: E value missing, default used ({:})'.format(_Emod))
                
                try:
                    _Gmod = float(line[37:43].strip())
                except ValueError:
                    _Gmod = 11.6
                    if 'KN' in _units_in:
                        _Gmod *= 0.689
                    elif 'KG'  in _units_in:
                        _Gmod *= 689.476                    
                    print('    *** warning: G value missing, default used ({:})'.format(_Gmod))
                
                try:
                    _Fy = float(line[43:49].strip())
                except ValueError:
                    _Fy = 36
                    if 'KN' in _units_in:
                        _Fy *= 0.689
                    elif 'KG'  in _units_in:
                        _Fy *= 689.476                     
                    print('    *** warning: Fy value missing, default used ({:})'.format(_Fy))
                #
                try:
                    _density = float(mat_density)
                except ValueError:
                    _density = float(mat_density.replace('-', 'E-'))
                #
                _mat_no += 1
                _mat_name = sacs_common.get_material(_Emod, _Gmod, _Fy, _density, 
                                                     _units_in, _material, _mat_no)
                #
                # section
                #
                try:
                    _section[_group_name]
                    _geomNo += 1
                    _nameStep = str(_section[_group_name].name) + '_step_' + str(_geomNo)
                    _prop_new_name = sacs_common.get_name(_prop_name, prefix='S_')
                    # 
                    try:
                        _section[_group_name].step.append(_nameStep)
                    except AttributeError:
                        _section[_group_name].step = [_nameStep]
                    #
                    _sect_no += 1
                    _section[_nameStep] = f2u.Geometry.Section(_prop_new_name, 
                                                               _sect_no, 'beam', _shape)
                    # update section
                    _section[_nameStep].properties = _property[_prop_name]
                    _section[_nameStep].material = _material[_mat_name]
                    #
                    # segmented length
                    try:
                        _sect = float(line[49:57]) * _length_factor
                        if isclose(_sect, 0, rel_tol=0.01):
                            continue
                        
                        _section[_nameStep].length = _sect
                    except ValueError:
                        continue
                #
                except KeyError:
                    _geomNo = 0
                    _sect_no += 1
                    _prop_new_name = sacs_common.get_name(_group_name, prefix='S_')
                    _section[_group_name] = f2u.Geometry.Section(_prop_new_name,
                                                                 _sect_no, 'beam', _shape)
                    #
                    # update section
                    _section[_group_name].properties = _property[_prop_name]
                    _section[_group_name].material = _material[_mat_name]
                    #
                    #
                    _group_no += 1
                    _new_group_name = sacs_common.get_name(_group_name, prefix='G_')
                    _group[_group_name] = f2u.Geometry.Groups(_new_group_name, 
                                                              _group_no)
                    #
                    # segmented length
                    try:
                        _sect = float(line[49:57]) * _length_factor
                        if isclose(_sect, 0, rel_tol=0.01):
                            continue
                        _section[_group_name].length = _sect
                    except ValueError:
                        continue
                    #
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)
    # 
    dat_temp.close()
    os.remove('pileTemp.dat')
    #
    #
    print('    * Third pass')
    dat_temp = open('soil_input.inp', 'w+')
    #
    with open('pileTemp2.dat') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue
            #
            if 'pile' in line[0:4].lower():
                _group_name = line[15:19].strip()
                if not _group_name:
                    continue
                
                _node_1 = line[6:10].strip()
                _node_2 = line[10:15].strip()
                #
                _memb_name = str(_node_1) + "x" + str(_node_2)
                _memb_no += 1
                _group_name_res = _memb_name
                #
                _concept[_memb_name] = f2u.Concept(_memb_name, _memb_no)
                #
                # node 1 pile head
                #
                _node_no += 1
                _node_name = _memb_name + '_1'
                model.component.add_node(_node_name, _node_no)
                coord = nodes[_node_1].coordinates
                model.component.nodes[_node_name].set_coordinates(coord[0],
                                                                  coord[1],
                                                                  coord[2])
                #
                # add boundary to define node is a link (pile head)
                model.component.add_boundary(name=_node_name,
                                             node_name=_node_name)                
                model.component.boundaries[_node_name].set_releases([1, 1, 1, 1, 1, 1])
                model.component.boundaries[_node_name].type = 'link'
                #
                _concept[_memb_name].type = 'pile'
                _concept[_memb_name].node.append(model.component.nodes[_node_name])
                model.component.nodes[_node_name].elements.append(_memb_name)
                #
                # node 'dummy' 2 (define batter)
                #
                _node_no += 1
                _node_name = _memb_name + '_2'
                model.component.add_node(_node_name, _node_no)
                coord = nodes[_node_2].coordinates
                model.component.nodes[_node_name].set_coordinates(coord[0],
                                                                  coord[1],
                                                                  coord[2])                 
                _concept[_memb_name].node.append(model.component.nodes[_node_name])
                model.component.nodes[_node_name].elements.append(_memb_name)
                #
                try:
                    _concept[_memb_name].section.append(_section[_group_name])
                    _concept[_memb_name].material.append(_section[_group_name].material)
                    _section[_group_name].elements.append(_memb_name)
                    # 
                    _group[_group_name].concepts.append(_memb_name)
                except KeyError:
                    print(' error section missing {:}'.format(_group_name))
                    continue
                #
                # Soil table identifier
                _soil_group = line[68:72].strip()
                try:
                    _group[_soil_group].concepts.append(_memb_name)
                except KeyError:
                    _group_no += 1
                    _group[_soil_group] = f2u.Geometry.Groups(_soil_group, 
                                                              _group_no)
                    _group[_soil_group].concepts.append(_memb_name)
                #
            #
            else:
                dat_temp.write("\n")
                dat_temp.write(line)            
    #
    #
    dat_temp.close()
    os.remove('pileTemp2.dat')
    #
    #
    # ---------------------
    # find stepped sections
    #
    _tapered = {}
    _geometry_items = process.concept.find_step_section(_concept, 
                                                        _section,
                                                        _material,
                                                        _eccentricity,
                                                        _tapered)
    #
    # ---------------------
    # find tip/bottom nodes
    #
    for key, memb in _concept.items():
        #
        #if key == '9305_9911':
        #    print('here')        
        # nodes member 1
        A = Point3(memb.node[0].x,
                   memb.node[0].y,
                   memb.node[0].z)
    
        B = Point3(memb.node[1].x, 
                   memb.node[1].y, 
                   memb.node[1].z)
        #
        _vector = B - A
        _normalized = _vector.normalized()
        #
        _total_length = 0
        for _step in memb.section:
            _total_length += _step.length
        #
        coord = [0, 0, 0]
        for x in range(3):
            coord[x] = memb.node[0].coordinates[x] - _total_length * _normalized[x]
        #
        # update bottom node coordinates
        _node_name = key + '_2'
        memb.node[1].set_coordinates(coord[0],
                                     coord[1],
                                     coord[2])
        #
        _group_no += 1
        _group_name = 'end_nodes__' + key
        _group[key] = f2u.Geometry.Groups(_group_name, 
                                          _group_no)
        _group[key].nodes.append(memb.node[0].name)
        _group[key].nodes.append(memb.node[1].name)
        _group[key].items.append(key)
    #    
    #
    #
    #
    # Create pile model
    #model = f2u.F2U_model('piles', 3)
    #model.set_component()
    #model.component.nodes = _pnode
    #model.component.elements = _pelement
    model.component.sections = _section
    model.component.materials = _material
    model.component.concepts =  _concept
    model.component.sets = _group
    #
    #
    #
    return model.component
    #
#