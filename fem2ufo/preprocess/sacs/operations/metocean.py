# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import sys
import re
from bisect import bisect_left
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process

#
#
class Interpolate(object):
    """
    """
    def __init__(self, x_list, y_list):
        
        if any(y - x <= 0 for x, y in zip(x_list, x_list[1:])):
            raise ValueError("x_list must be in strictly ascending order!")
        
        x_list = self.x_list = list(map(float, x_list))
        y_list = self.y_list = list(map(float, y_list))
        
        intervals = zip(x_list, x_list[1:], y_list, y_list[1:])
        self.slopes = [(y2 - y1)/(x2 - x1) for x1, x2, y1, y2 in intervals]
        
    #def __getitem__(self, x):
    def __call__(self, x):
        #i = bisect_left(self.x_list, x) - 1
        #return self.y_list[i] + self.slopes[i] * (x - self.x_list[i])
        #
        if x <= self.x_list[0]:
            return self.y_list[0]
        
        elif x >= self.x_list[-1]:
            return self.y_list[-1]
        
        else:
            i = bisect_left(self.x_list, x) - 1
            return self.y_list[i] + self.slopes[i] * (x - self.x_list[i])        
#
#
#
def cdcm_default(_temp_cdcm):
    """
    """
    _cdcm = {}
    _no_cdcm = 1
    _name = 'default'
    _cdcm[_name] = f2u.Metocean.DragMassCoefficient(_name, _no_cdcm)
    _diam_list=[]
    
    if _temp_cdcm:
        for _item, _value in _temp_cdcm.items():
            if 'rough_default' in _item or 'smooth_default' in _item :
                rough = _temp_cdcm['rough_default']
                smooth = _temp_cdcm['smooth_default']
                
                _cdcm[_name].diameter_function(0.01, rough, smooth)
                _cdcm[_name].diameter_function(0.3048, rough, smooth)
                _cdcm[_name].diameter_function(3.0480, rough, smooth)
                _cdcm[_name].diameter_function(25.4, rough, smooth)
                return _cdcm
            else:
                if 'rough_' in _item:
                    _diameter = _item.replace('rough_', '')
                    rough = _temp_cdcm[_item]
                    smooth = _temp_cdcm['smooth_'+_diameter]
                else:
                    _diameter = _item.replace('smooth_', '')
                    smooth = _temp_cdcm[_item]
                    rough = _temp_cdcm['rough_'+_diameter]
                
                if _diameter in _diam_list:
                    continue
                
                _diam_list.append(_diameter)
                _cdcm[_name].diameter_function(float(_diameter), rough, smooth)
    # set sacs default
    else:
        _cdcm[_name].diameter_function(diameter=0.01, 
                                       rough=[0, 0.517, 0.517, 0, 1.38, 1.38],
                                       smooth=[0, 0.517, 0.517, 0, 1.38, 1.38])
        #
        _cdcm[_name].diameter_function(diameter=0.3048, 
                                       rough=[0, 0.610, 0.610, 0, 1.39, 1.39],
                                       smooth=[0, 0.610, 0.610, 0, 1.39, 1.39])
        #
        _cdcm[_name].diameter_function(diameter=0.6096, 
                                       rough=[0, 0.665, 0.665, 0, 1.40, 1.40],
                                       smooth=[0, 0.665, 0.665, 0, 1.40, 1.40])
        #
        _cdcm[_name].diameter_function(diameter=1.2192, 
                                       rough=[0, 0.720, 0.720, 0, 1.45, 1.45],
                                       smooth=[0, 0.720, 0.720, 0, 1.45, 1.45])
        #
        _cdcm[_name].diameter_function(diameter=1.8288, 
                                       rough=[0, 0.756, 0.756, 0, 1.67, 1.67],
                                       smooth=[0, 0.756, 0.756, 0, 1.67, 1.67])
        #
        _cdcm[_name].diameter_function(diameter=2.4384, 
                                       rough=[0, 0.781, 0.781, 0, 1.67, 1.67],
                                       smooth=[0, 0.781, 0.781, 0, 1.67, 1.67])
        #
        _cdcm[_name].diameter_function(diameter=3.0480, 
                                       rough=[0, 0.799, 0.799, 0, 1.71, 1.71],
                                       smooth=[0, 0.799, 0.799, 0, 1.71, 1.71])
        #
        _cdcm[_name].diameter_function(diameter=10.16, 
                                       rough=[0, 0.897, 0.897, 0, 2.176, 2.176],
                                       smooth=[0, 0.897, 0.897, 0, 2.176, 2.176])
        #
        _cdcm[_name].diameter_function(diameter=25.4, 
                                       rough=[0, 0.973, 0.973, 0, 3.176, 3.176],
                                       smooth=[0, 0.973, 0.973, 0, 3.176, 3.176])        
    #
    return _cdcm
#
#
def set_cdcm(_cdcm, model):
    """
    """
    #
    _temp_cdcm = {}
    #
    _diam_x = [_diam for _diam in sorted(_cdcm['default'].diameter.keys())]
    # 
    _cdm_rg = []
    _cdm_sm = []
    for x in range(len(_diam_x)):
        _diam = _diam_x[x]
        _cdm_rg.append([])
        _cdm_sm.append([]) 
        for y in range(6):
            _cdm_rg[x].append(_cdcm['default'].diameter[_diam][0][y])
            _cdm_sm[x].append(_cdcm['default'].diameter[_diam][1][y])
    
    for key, _section in model.component.sections.items():
        #
        if re.search(r"\b(tub(ular)?|pipe)\b", _section.shape, re.IGNORECASE):
            memb_diam = _section.properties.D
        #
        elif re.search(r"\b(box|rectangular\s*bar|channel|angle|l|t(ee)?|i(\s*beam|section)?)\b", _section.shape, re.IGNORECASE):
            _diam = [_section.properties.D]
            _diam.append((_section.properties.D**2 + _section.properties.Bft**2)**0.5)
            memb_diam = max(_diam)
        #
        elif re.search(r"\b(unsymetric(\s*i\s*beam|section)?|open\s*thin\s*walled)\b", _section.shape, re.IGNORECASE):
                _diam = [_section.properties.D]
                _diam.append((_section.properties.D**2 + max(_section.properties.Bft, _section.properties.Bfb)**2)**0.5)
                memb_diam = max(_diam)
        #
        elif re.search(r"\b(general\s*section)\b", _section.shape, re.IGNORECASE):
            memb_diam = (_section.properties.D**2 + _section.properties.Bft**2)**0.5
            #1/0.0
        #
        elif re.search(r"\b(cone)\b", _section.shape, re.IGNORECASE):
            memb_diam = (_section.properties.Bft + _section.properties.Bfb) / 2.0
        #
        else:
            memb_diam = _section.properties.D
        #
        cdm_rg = [[dum[x] for dum in _cdm_rg]
                  for x in range(6)]
        #
        cdm_sm = [[dum[x] for dum in _cdm_sm]
                  for x in range(6)]            
        #
        _cdcm_new_rg = []
        _cdcm_new_sm = []
        _name = _section.name
        #if not _section.name:
        #    print('-->')
        for x in range(6):
            interpolate = Interpolate(_diam_x, cdm_rg[x])
            _new_y = round(interpolate(memb_diam), 3)
            _cdcm_new_rg.append(_new_y)
            #
            interpolate = Interpolate(_diam_x, cdm_sm[x])
            _new_y = round(interpolate(memb_diam), 3)
            _cdcm_new_sm.append(_new_y)
        #
        try:
            _temp_cdcm[_name]
        except KeyError:
            #_no_cdcm += 1
            _temp_cdcm[_name] = {}
            _temp_cdcm[_name]['rough'] = _cdcm_new_rg
            _temp_cdcm[_name]['smooth']= _cdcm_new_sm
            _temp_cdcm[_name]['items'] = []
        #
        _temp_cdcm[_name]['items'].extend(_section.elements)
    #  
    return _temp_cdcm
#
#
def get_cdcm(_diameter, _cdcm, line, _length):
    """
    """
    #
    if _diameter:
        _cdcm_id = _diameter * _length
        _name = 'smooth_' + str(_cdcm_id)
    else:
        _name = 'smooth_default'
    #
    # Clean memebers
    # cd
    _cdy = float(line[12:18])
    try:
        _cdx = float(line[18:24])
    except ValueError:
        _cdx = 0
    # cm
    _cmy = float(line[24:30])
    try:
        _cmx = float(line[30:36])
    except ValueError:
        _cmx = 0
    #
    _cdcm[_name] = {} 
    _cdcm[_name] = [_cdx, _cdy, _cdy, _cmx, _cmy, _cmy]
    #
    # Fouled members
    #
    if _diameter:
        _name = 'rough_' + str(_cdcm_id)
    else:
        _name = 'rough_default'
    # cd
    _cdy = float(line[36:42])
    try:
        _cdx = float(line[42:48])
    except ValueError:
        _cdx = 0
    # cm
    _cmy = float(line[48:54])
    try:
        _cmx = float(line[54:60])
    except ValueError:
        _cmx = 0
    #
    _cdcm[_name] = [_cdx, _cdy, _cdy, _cmx, _cmy, _cmy]
#
def get_cdcm_overrides(_name, _cdcm, line):
    """
    """
    # cd
    try:
        _cdy = float(line[52:56])
    except ValueError:
        _cdy = 0
    
    try:
        _cdz = float(line[56:60])
    except ValueError:
        _cdz = 0
    # cm
    try:
        _cmy = float(line[60:64])
    except ValueError:
        _cmy = 0
    
    try:
        _cmz = float(line[64:68])
    except ValueError:
        _cmz = 0
    #
    # Tangential
    # cd
    try:
        _cdx = float(line[68:72])
    except ValueError:
        _cdx = 0
    #
    # cm
    try:
        _cmx = float(line[72:76])
    except ValueError:
        _cmx = 0
    #
    # factor
    _factor = False
    if line[76:77].strip():
        _factor = True
    #
    _cdcm[_name] = {}
    _cdcm[_name]['cdcm'] = [_cdx, _cdy, _cdz, _cmx, _cmy, _cmz]
    _cdcm[_name]['items'] = []
    _cdcm[_name]['sets'] = []
    _cdcm[_name]['factor'] = _factor
#
#
#
#
def assign_cdcm(items, _coeff, model):
    """
    """
    for _memb_no in items:
        _memb = model.component.concepts[_memb_no]            
        try:
            _memb.properties.cdcm = _coeff
        except AttributeError:
            _memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
            _memb.properties.cdcm = _coeff
    #
    number = _coeff.number
    name = '_cdcm_' + str(number)
    new_set_name, new_set_no = process.geometry.add_group(model.component.sets, 
                                                          items, name)
    #
    return new_set_name, new_set_no
#
def get_cdcm_name(coeff):
    """
    """
    name = ''
    for item in coeff:
        if item == 0.0:
            name += '0'
            continue
        name += str(item)
    #
    name = name.replace('.', '_')
    return name
#
def update_cdcm(model, _surface, _temp_cdcm, _sections_cdcm, 
                _cdcm, _wind_drag, _air_drag, _wind_shield):
    """
    """
    try:
        _no_cdcm = len(model.metocean.cdcm)
    except AttributeError:
        _no_cdcm = len(_cdcm)
    
    _wind = False
    if 'w' in _wind_drag.lower():
        _wind = True
    
    try:
        _no_air = len(model.metocean.air_drag)
    except AttributeError:
        _no_air = len(_air_drag)
    #
    # 2 - set group global over-ride
    for key, _item in _temp_cdcm.items():
        #
        if _item['sets']:
            # overrides are factors
            if _item['factor']:
                for _group_no in _item['sets']:
                    for _memb_no in model.component.sets[_group_no].concepts:
                        _no_cdcm = set_factor_CDCM(_memb_no, _no_cdcm, _cdcm, _air_drag, _wind_shield,
                                                   _sections_cdcm, _surface, _item, _wind, _no_air, model)
                        #
            else:
                _name = 'cdcm_rough_' + get_cdcm_name(_item['cdcm'])
                try:
                    _coeff = _cdcm[_name]
                except KeyError:
                    _no_cdcm += 1
                    _cdcm[_name] = f2u.Metocean.DragMassCoefficient(_no_cdcm, _no_cdcm)
                    _cdcm[_name].set_cdcm(_item['cdcm'])
                    _coeff = _cdcm[_name]
                #
                for _group_name in _item['sets']:
                    try:
                        _group = model.component.sets[_group_name]
                    except KeyError:
                        print('   *** warning group {:} not found'.format(_group_name))
                        continue
                    
                    _newgroup_name, newgroup_number = assign_cdcm(_group.concepts, 
                                                                  _coeff, model)
                    _coeff.sets[_newgroup_name] = newgroup_number
    #
    # 3 - set member global over-ride
    for key, _item in _temp_cdcm.items():
        if _item['items']:
            
            if _item['factor']:
                for _memb_no in _item['items']:
                    _no_cdcm = set_factor_CDCM(_memb_no, _no_cdcm, _cdcm, _air_drag, _wind_shield,
                                               _sections_cdcm, _surface, _item, _wind, _no_air, model)
                    #print('fix factor here')
            else:
                _no_cdcm += 1
                _name = 'cdcm_' + get_cdcm_name(_item['cdcm'])
                _cdcm[_name] = f2u.Metocean.DragMassCoefficient(_no_cdcm, _no_cdcm)
                _cdcm[_name].set_cdcm(_item['cdcm'])
                _cdcm[_name].items = _item['items']
                #
                # Update members
                _newgroup_name, newgroup_number = assign_cdcm(_item['items'], 
                                                              _cdcm[_name], model)
                
                _cdcm[_name].sets[_newgroup_name] = newgroup_number
#
#
def set_factor_CDCM(_memb_no, _no_cdcm, _cdcm, _air_drag, _wind_shield,
                    _sections_cdcm, _surface, _item, _wind, _no_air, model):
    """
    """
    _memb = model.component.concepts[_memb_no]
    _section_name = _memb.section[0].name
    _default = _sections_cdcm[_section_name]
    #
    # find if both node z coord above surface (asumming Z up)
    _temp = [round(a*b, 4) for a, b in zip(_item['cdcm'], _default['rough'])]
    _name = 'cdcm_rough_' + get_cdcm_name(_temp)
    _memb.get_end_nodes()
    if _memb.node[0].z > _surface and _memb.node[1].z > _surface:
        _temp = [round(a*b, 4) for a, b in zip(_item['cdcm'], _default['smooth'])]
        _name = 'cdcm_smooth_' +  get_cdcm_name(_temp)
    #
    # check if _cdcm already exist
    try:
        _coeff = _cdcm[_name]
    except KeyError:
        _no_cdcm += 1
        _cdcm[_name] = f2u.Metocean.DragMassCoefficient(_no_cdcm, _no_cdcm)
        if '_smooth_' in _name:
            #_temp = [round(a*b, 4) for a, b in zip(_item['cdcm'], _default['smooth'])]
            _cdcm[_name].set_cdcm(_temp)
            #
            if _wind:
                # Also set wind drag for smooth sections
                _no_air += 1
                _air_drag[_name] = f2u.Metocean.DragMassCoefficient(_no_air, _no_air)
                _air_drag[_name].set_cdcm(_temp)
        else:
            #_temp = [round(a*b, 4) for a, b in zip(_item['cdcm'], _default['rough'])]
            _cdcm[_name].set_cdcm(_temp)
        #
        _coeff = _cdcm[_name]
    #
    # assign cdcm to concept
    try:
        _memb.properties.cdcm = _coeff
    except AttributeError:
        _memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
        _memb.properties.cdcm = _coeff
    #
    # check if air drag
    try:
        _air_drag[_name]
        _flag = True
        for _interval in _wind_shield:
            if _memb.node[0].z >= _interval[0] and _memb.node[1].z >= _interval[0]:
                if _memb.node[0].z <= _interval[1] and _memb.node[1].z <= _interval[1]:
                    _flag = False
                    try:
                        model.component.sets['nohydro'].concepts.append(_memb_no)
                    except KeyError:
                        grp_number = [grp.number for grp in model.component.sets.values()]
                        _number = max(grp_number) + 1
                        model.component.sets['nohydro'] = f2u.Geometry.Groups('nohydro', _number)
                        model.component.sets['nohydro'].concepts.append(_memb_no)
                    break
        #
        #
        if _flag:
            _memb.properties.air_drag = _air_drag[_name]
    except KeyError:
        pass
    #
    # check if global group exist
    #
    try:
        model.component.sets[_name].concepts.append(_memb_no)
    except KeyError:
        _newgroup_name, newgroup_number = process.geometry.add_group(model.component.sets, 
                                                                     _memb_no, _name,
                                                                     group_type=3)
        # cdcm group[name] = number
        _coeff.sets[_newgroup_name] = newgroup_number
        #
        # check air drag group
        try:
            _air_drag[_name].sets[_newgroup_name] = newgroup_number
        except KeyError:
            pass
        #
    #
    return _no_cdcm
#
def update_air_drag(model, _surface, _sections_cdcm, 
                    _wind_drag, _air_drag, _wind_shield):
    """
    """
    try:
        _no_air = len(model.metocean.air_drag)
    except AttributeError:
        _no_air = len(_air_drag)
    #
    if not _wind_drag:
        _no_air += 1
        _name = 'air_drag_default_tubular'
        _air_drag[_name] = f2u.Metocean.DragMassCoefficient(_no_air, _no_air)
        _air_drag[_name].set_cdcm([0, 0.50, 0.50, 0, 0, 0])
        #
        _no_air += 1
        _name = 'air_drag_default_notubular'
        _air_drag[_name] = f2u.Metocean.DragMassCoefficient(_no_air, _no_air)
        _air_drag[_name].set_cdcm([0, 1.50, 1.50, 0, 0, 0])
        #
        _no_air += 1
        _name = 'zero_air_drag'
        _air_drag[_name] = f2u.Metocean.DragMassCoefficient(_no_air, _no_air)
        _air_drag[_name].set_cdcm([0, 0, 0, 0, 0, 0])         
        
    
    for _memb_no, _memb in model.component.concepts.items():
        #if '9011_9015' in _memb.name:
        #    print('-->')
        if _memb.type != 'beam':
            continue
        #
        # check if air drag already allocated
        try:
            _memb.properties.air_drag
            continue
        except AttributeError:
            pass
        #
        # dismiss members below surface
        _memb.get_end_nodes()
        if _memb.node[0].z < _surface and _memb.node[1].z < _surface:
            continue
        
        if 'w' in _wind_drag.lower():
            _section_name = _memb.section[0].name
            _default = _sections_cdcm[_section_name]
            #
            # check if air_drag already exist
            _name = 'air_drag_' + get_cdcm_name(_default['smooth'])
            try:
                _air_drag[_name]
            except KeyError:
                _no_air += 1
                _air_drag[_name] = f2u.Metocean.DragMassCoefficient(_no_air, _no_air)
                _air_drag[_name].set_cdcm(_default['smooth'])
            #
        else:
            if 'tubular' in _memb.section[0].shape:
                _name = 'air_drag_default_tubular'
                #print('----?')
            else:
                _name = 'air_drag_default_notubular'
        #
        #_flag = True
        for _interval in _wind_shield:
            if _memb.node[0].z >= _interval[0] and _memb.node[1].z >= _interval[0]:
                if _memb.node[0].z <= _interval[1] and _memb.node[1].z <= _interval[1]:
                    #_flag = False
                    #
                    _name = 'zero_air_drag'
                    try:
                        _air_drag[_name]
                    except KeyError:
                        _no_air += 1
                        _name = 'zero_air_drag'
                        _air_drag[_name] = f2u.Metocean.DragMassCoefficient(_no_air, _no_air)
                        _air_drag[_name].set_cdcm([0, 0, 0, 0, 0, 0])
                    #
                    try:
                        model.component.sets['nohydro'].concepts.append(_memb_no)
                    except KeyError:
                        grp_number = [grp.number for grp in model.component.sets.values()]
                        _number = max(grp_number) + 1
                        model.component.sets['nohydro'] = f2u.Geometry.Groups('nohydro', _number)
                        model.component.sets['nohydro'].concepts.append(_memb_no)
                    break
        #
        #if _flag:
        _memb.properties.air_drag = _air_drag[_name]
        #
        # check if global group exist
        try:
            model.component.sets[_name].concepts.append(_memb_no)
        except KeyError:
            _newgroup_name, newgroup_number = process.geometry.add_group(model.component.sets, 
                                                                         _memb_no, _name,
                                                                         group_type=3)
            
            _air_drag[_name].sets[_newgroup_name] = newgroup_number
    #
    return _air_drag
#
#
def update_flooded_members(_metocean, model):
    """
    """
    for key, _set in model.component.sets.items():
        if '_flooded_' in key:
            try:
                _metocean.flooded[key]
            except KeyError:
                _metocean.flooded[key] = _set
            #
            continue
        #
        # find group from main model
        try:
            #_flooded[key]
            _metocean.flooded[key]
            #
            # update concepts
            for _memb_no in _set.concepts:
                _memb = model.component.concepts[_memb_no]            
                try:
                    _memb.properties.flooded = True
                except AttributeError:
                    _memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
                    _memb.properties.flooded = True
            #
            _metocean.flooded[key] = _set
            
        except KeyError:
            continue
    #
    #print('here')
#