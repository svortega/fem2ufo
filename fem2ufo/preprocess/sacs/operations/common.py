# 
# Copyright (c) 2009-2021 fem2ufo
# 


# Python stdlib imports
import re
#import sys
#
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process

#
#
def select(lineIn, key, keyWord=None, count=1):
    """
    match
    """
    #
    lineOut = lineIn
    _match = False
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.match(lineIn)

        if keys:
            keyWord = _key
            lineOut = re.sub(keys.group(), " ", lineIn, count)
            _match = True

    lineOut = lineOut.strip()
    return keyWord, lineOut, _match
#
#
#
def match_line(word_in, key):
    """
    search key word at the begining of the string
    """
    word_out = False

    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.match(word_in)

        if keys:
            word_out = _key

    return word_out
#
def search_line(lineIn, key, keyWord=None, count=1):
    """
    search key word anywere in the the string
    """
    lineOut = lineIn
    _match = False
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.search(lineIn)

        if keys:
            keyWord = _key
            lineOut = re.sub(keys.group(), " ", lineIn, count)
            _match = True

    lineOut = lineOut.strip()
    return keyWord, lineOut, _match
#
#
def get_name(lineIn, prefix):
    """
    """
    # keep original section name
    try:
        num = float(lineIn[:1])
        lineOut = prefix + lineIn
    except ValueError:
        lineOut = lineIn
    #
    lineOut = re.sub(r"\(|\)|\[|\]|\{|\}|\,", "_", lineOut)
    lineOut = re.sub(r"\-|\+|\%|\s|\=", "_", lineOut)
    lineOut = re.sub(r"\&", "and", lineOut)
    lineOut = re.sub(r"\/|\'|\*", "", lineOut)
    lineOut = re.sub(r"\.", "_", lineOut)
    lineOut = re.sub(r"\_|\-+", "_", lineOut)
    lineOut = lineOut.strip()
    return lineOut
#
# ----------------------
# section modules
#
def section_word(lineIn):
    """
    """
    _key = {"ibeam": r"wf(c)?",
            "ibeamfab": r"plg|pgu|pgb",
            "tubular": r"tub",
            "dented": r"dtb",
            "box": r"box|rtb",
            "cone": r"con",
            "channel": r"ch(an)?(l)?",
            "angle": r"ang(l)?",
            "tee": r"tee",
            "general": r"pri"}

    keyWord = match_line(lineIn, _key)
    return keyWord
#
#
def get_length_factor(_units):
    """
    """
    if "foot" in _units[0]:
        _factor, _force_factor = process.units.convert_units_SI('inch', 
                                                                _units[4])
    else:
        _factor, _force_factor = process.units.convert_units_SI('centimetre', 
                                                                _units[4])
    
    return _factor
#
def material_units(_units_in):
    """
    """
    if "EN" in _units_in:
        unit2 = 'kilopsi'
        _stress = process.units.Number(1.0, dims=unit2)
        _density = process.units.Number(1.0, dims='pound/foot^3')
    elif "MN" in _units_in:
        unit2 = 'kilonewton/centimetre^2'
        _stress = process.units.Number(1.0, dims=unit2)
        _density = process.units.Number(1.0, dims='megagram/metre^3')
    elif "ME" in _units_in:
        unit2 = 'newton/centimetre^2' # kilogram
        _stress = process.units.Number(1.0, dims=unit2) * 9.80665 # kilogram to newton
        _density = process.units.Number(1.0, dims='megagram/metre^3')
    elif "SI" in _units_in:
        unit2 = 'newton/metre^2'
        _stress = process.units.Number(1.0, dims=unit2)
        _density = process.units.Number(1.0, dims='kilogram/metre^3')
        #return 1, 1
    else:
        print('   *** error units : {:} not recognized'.format(_units_in))
        sys.exit()
    
    stress = _stress.convert('newton/metre^2').value
    density = _density.convert('kilogram/metre^3').value
    
    return stress, density
#
def get_units(_units_in):
    """
    """
    if "EN" in _units_in.upper() :
        units = ["foot", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
        _mass_factor = 0.453592
        _length_factor, _force_factor = process.units.convert_units_SI(units[0], 
                                                                       units[4])        
    
    elif "MN" in _units_in.upper():
        units = ["metre", "1.0*kilogram", "second", 'centigrade', "kilonewton", "pascal"]
        _mass_factor = 0.001
        _length_factor, _force_factor = process.units.convert_units_SI(units[0], 
                                                                       units[4])
    
    elif "ME" in _units_in.upper():
        units = ["metre", "1.0*kilogram", "second", 'centigrade', "newton", "pascal"]
        _mass_factor = 0.001
        _length_factor, _force_factor = process.units.convert_units_SI(units[0], 
                                                                       units[4])
        
        _force_factor = _force_factor * 9.80665002864
    
    else:
        units = ["foot", "1.0*pound", "second", 'fahrenheit', "kilolbf", "psi"]
        _mass_factor = 0.453592
        _length_factor, _force_factor = process.units.convert_units_SI(units[0], 
                                                                       units[4])
    
    return _length_factor, _force_factor, _mass_factor
#    
#
def get_material(_Emod, _Gmod, _Fy, _density,
                 _units_in, _material, _mat_no,
                 prefix='MT'):
    """
    """
    _mat_name = prefix + str(_Emod) +'x'+ str(_Gmod) +'x'+ str(_Fy) +'x'+ str(int(_density))
    _factor = 1000
    if 'SI' in _units_in:
        _mat_name = prefix + str(round(_Emod/_factor**3)) +'x'+ str(round(_Gmod/_factor**3)) \
            +'x'+ str(round(_Fy/_factor**2)) +'x'+ str(int(_density))
        _factor = 1
    #
    _mat_name = re.sub(r"\.", "_", _mat_name)
    _mat_name = re.sub(r"\-", "minus", _mat_name)
    _mat_name = re.sub(r"\+", "plus", _mat_name)
    #
    # check if material already exist
    try:
        _material[_mat_name]
    except KeyError:
        _stress_factor, _density_factor = material_units(_units_in)
        _mat_no += 1
        _material[_mat_name] = f2u.Material(_mat_name, _mat_no)
        _material[_mat_name].E = _Emod * _stress_factor * _factor
        _material[_mat_name].G = _Gmod * _stress_factor * _factor
        _material[_mat_name].Fy = _Fy * _stress_factor
        _material[_mat_name].density = _density * _density_factor
        _material[_mat_name].type = 'Elastic'
    #
    return _mat_name, _mat_no
#