# 
# Copyright (c) 2009-2020 fem2ufo
# 


# Python stdlib imports
import math
import copy
#
# package imports
import fem2ufo.f2u_model.control as f2u
#import fem2ufo.process.control as process
from fem2ufo.process.modules.euclid import Point3, Plane, Line3, LineSegment3
import fem2ufo.preprocess.asas.operations.process as asas_process

#
#
#
def get_3points(memb):
    """
    """
    # nodes member 1
    A = Point3(memb.node[0].x, 
               memb.node[0].y, 
               memb.node[0].z)

    B = Point3(memb.node[1].x, 
               memb.node[1].y, 
               memb.node[1].z)

    C = Point3(memb.node[2].x, 
               memb.node[2].y, 
               memb.node[2].z)
    
    return A,B,C
#
def get_4points(memb):
    """
    """
    # nodes member 1
    A, B, C = get_3points(memb)

    D = Point3(memb.node[3].x, 
               memb.node[3].y, 
               memb.node[3].z)
    
    return A, B, C, D
#
def find_plate_nodes(model, _memb_no):
    """
    """
    concept = model.component.concepts
    group = model.component.sets
    _temp = tuple(key for key in sorted(concept, key=lambda name: concept[name].number)
                  if 'shell' in concept[key].type  or 'membrane' in concept[key].type)
    #
    for key1 in _temp:
        memb1 = concept[key1]
        if len(memb1.node) == 3:
            continue
        # nodes member 1
        A,B,C,D = get_4points(memb1)
        #
        #
        AB = B - A
        AC = C - A
        AD = D - A
        #
        _angle_BC = AB.angle(AC)
        _angle_BD = AB.angle(AD)
        #          
        if _angle_BC > _angle_BD:
            _new_nodes = [memb1.node[0],
                          memb1.node[1],
                          memb1.node[3],
                          memb1.node[2]]
            #
            memb1.node = _new_nodes
            #
            # nodes member 1
            A,B,C,D = get_4points(memb1)
        #
        DC = C - D
        DA = A - D
        #
        plane = Plane(A, B, C)
        DAC = DC.cross(DA)
        line = Line3(D, DAC)
        DD = plane.intersect(line)
        #
        if D != DD :
            segAC = LineSegment3(A, C)
            segBD = LineSegment3(B, D)
            E = segAC.connect(segBD)
            #
            _node_no = len(model.component.nodes) + 1
            _node_name = 'nn_' + str(_node_no)
            #
            model.component.add_node(_node_name, _node_no)
            model.component.nodes[_node_name].set_coordinates(E.p.x, E.p.y, E.p.z)
            #
            for x in range(2):
                _PLnodes = [memb1.node[x+1],
                            memb1.node[x+2],
                            model.component.nodes[_node_name]]
                
                _section = memb1.section[0]
                _memb_name = memb1.name + '_' + str(x+1)
                _memb_no += 1
                get_new_plate(_memb_name, _memb_no, _PLnodes, 
                              concept, _section, group)
            #
            _PLnodes = [memb1.node[3],
                        memb1.node[0],
                        model.component.nodes[_node_name]]
            #
            _section = memb1.section[0]
            _memb_name = memb1.name + '_' + str(4)
            _memb_no += 1
            get_new_plate(_memb_name, _memb_no, _PLnodes, 
                          concept, _section, group)            
            #
            memb1.node[3].elements.remove(memb1.name)
            memb1.node[2].elements.remove(memb1.name)
            memb1.node = memb1.node[:2]
            memb1.node.append(model.component.nodes[_node_name])
            model.component.nodes[_node_name].elements.append(memb1.name)
#
#
def get_new_plate(_memb_name, _memb_no, _PLnodes, 
                  concept, _section, group):
    """
    """
    concept[_memb_name] = f2u.Geometry.ShellElement(_memb_name,
                                                    _memb_no)
    concept[_memb_name].type = 'shell'

    for _node in _PLnodes:
        concept[_memb_name].node.append(_node)
        _node.elements.append(_memb_name)
    #
    _group_name = _section.name
    #
    concept[_memb_name].section.append(_section)
    concept[_memb_name].material.append(_section.material)
    _section.elements.append(_memb_name)
    _plate_group_name = _group_name # 'PLT_' + _group_name
    group[_plate_group_name].items.append(_memb_name)
#
#
def get_new_beam(_memb_name, _memb_no, _node_1, _node_2,
                  concept, _section, group, nodes):
    """
    """
    concept[_memb_name] = f2u.Geometry.Concept(_memb_name,
                                               _memb_no)
    concept[_memb_name].type = 'beam'
    concept[_memb_name].overlap = True
    concept[_memb_name].properties = f2u.Geometry.BeamElementProperty('hydrodynamic') 
    #
    #for _node in _PLnodes:
    #    concept[_memb_name].node.append(_node)
    #    _node.elements.append(_memb_name)
    #
    concept[_memb_name].node.append(nodes[_node_1])
    nodes[_node_1].elements.append(_memb_name)

    concept[_memb_name].node.append(nodes[_node_2])
    nodes[_node_2].elements.append(_memb_name)    
    #
    #
    concept[_memb_name].section.append(_section)
    concept[_memb_name].material.append(_section.material)
    _section.elements.append(_memb_name)
    #
    _group_name = _section.name
    _plate_group_name =  'STF_' + _group_name
    try:
        group[_plate_group_name].items.append(_memb_name)
    except KeyError:
        group[_plate_group_name] = f2u.Geometry.Groups(_plate_group_name)
        group[_plate_group_name].items.append(_memb_name)
#
def add_stiffeners(stiffeners, nodes, _node_base, model):
    """
    """
    _node_1 = nodes[0]
    _node_3 = nodes[2]
    _node_no = max([_node.number for _node in model.component.nodes.values()])
    #_node_no = max(_node_no)
    _memb_no = max([_beam.number for _beam in model.component.concepts.values()])
    
    for _stiffener in stiffeners:
        _spacing = _stiffener[1]
        _section = model.component.sections[_stiffener[0]]
        #        
        if _stiffener[2] == "X":
            _node_2 = nodes[1]
            _node_x = nodes[3]
            #_lenght = _node_1.distance(_node_2)
            #_step = int(_lenght / _spacing)
        else:
            _node_2 = nodes[3]
            _node_x = nodes[1]
        #
        #_lenght = _node_1.distance(_node_3)
        #
        _L1 = _node_1.distance(_node_x) / 2.0
        _L12 = _node_1.distance(_node_x) / 2.0
        _step1 = int(_L1 / _spacing)
        _vector = _node_x - _node_1
        _normalized_1 = _vector.normalized()        
        #
        _L2 = _node_2.distance(_node_3) / 2.0
        _L22 = _node_2.distance(_node_3) / 2.0
        _step2 = int(_L2 / _spacing)
        _vector = _node_3 - _node_2
        _normalized_2 = _vector.normalized()        
        #
        _node_no, _memb_no = new_beam(_node_1, _node_2, _L1, _L2, 
                                      _normalized_1, _normalized_2,
                                      _node_no, _memb_no, model, _section)
        #
        _steps = [_step1, _step2]
        _steps = min(_steps)
        #print('-- ', _L1)
        for x in range(_steps):
            _L1 -=  _spacing
            _L2 -=  _spacing
            # left
            _node_no, _memb_no = new_beam(_node_1, _node_2, _L1, _L2, 
                                          _normalized_1, _normalized_2,
                                          _node_no, _memb_no, model, _section)
            # rigth
            _L12 +=  _spacing
            _L22 +=  _spacing            
            _node_no, _memb_no = new_beam(_node_1, _node_2, _L12, _L22, 
                                          _normalized_1, _normalized_2,
                                          _node_no, _memb_no, model, _section)             
        _steps
        
#
def new_beam(_node_1, _node_2, _L1, _L2, 
             _normalized_1, _normalized_2,
             _node_no, _memb_no, model, _section):
    """
    """       
    coord = [_node_1[x] + _L1 * _normalized_1[x]  for x in range(3)]
    _node_no += 1
    _node_name1 = _node_no
    model.component.add_node(_node_name1, _node_no)
    model.component.nodes[_node_no].set_coordinates(coord[0],
                                                    coord[1],
                                                    coord[2])
    #       
    coord = [_node_2[x] + _L2 * _normalized_2[x]  for x in range(3)]
    _node_no += 1
    _node_name2 = _node_no
    model.component.add_node(_node_name2, _node_no)
    model.component.nodes[_node_no].set_coordinates(coord[0],
                                                    coord[1],
                                                    coord[2])        
    #
    _memb_name = 'STF_' + str(_node_name1) + '_' + str(_node_name2)
    _memb_no += 1
    get_new_beam(_memb_name, _memb_no, 
                 _node_name1, _node_name2,
                 model.component.concepts, 
                 _section, 
                 model.component.sets,
                 model.component.nodes)
    #
    return _node_no, _memb_no
#
#
def find_short_members(model, tol=0.010):
    """
    """
    concept = model.component.concepts
    #nodes = model.component.nodes
    joints = model.component.joints
    _beams = [key for key in sorted(concept, key=lambda name: concept[name].number)
              if 'beam' in concept[key].type]
    
    _short = []
    _node_delete = []
    for key in _beams:
        _beam = concept[key]
        
        #if '8102_8101' in _beam.name :
        #    print(_beam.flexible_length, _beam.zero_length(tol))
            #print('here')
        
        if _beam.zero_length(tol):

            if _beam.node[1].boundary:
                _node_fix = _beam.node[1]
                _node_off = _beam.node[0]
            else:
                _node_fix = _beam.node[0]
                _node_off = _beam.node[1]
            #
            # check if joint exist (i.e. pile head)
            #
            try:
                joints[_node_fix.name].overlap.remove(key)
                #joints[_node_off.name] = joints[_node_fix.name]
                del joints[_node_off.name]
            except AttributeError:
                joints[_node_off.name].overlap.remove(key)
                joints[_node_fix.name] = joints[_node_off.name]
                del joints[_node_off.name]
            except KeyError:
                try:
                    joints[_node_off.name].overlap.remove(key)
                    joints[_node_fix.name] = joints[_node_off.name]
                    del joints[_node_off.name]
                except KeyError:
                    pass
            #except ValueError:
            #    print('---->?')
            #    pass
            #
            #
            _short.append(key)
            _new_set = set(_node_fix.elements)
            for memb_id in _node_off.elements:
                _memb = concept[memb_id]
                
                if _memb.node[0].name == _node_off.name:
                    _memb.node[0] = _node_fix
                else:
                    _memb.node[1] = _node_fix
                
                _new_set.add(memb_id)
                #_node_off.elements.remove(memb_id)
            
            _node_fix.elements = list(_new_set)
            #
            if not _node_off.elements:
                _node_delete.append(_node_off.name)
            
    for memb_id in _short:
        del concept[memb_id]
    
    #for _node_name in _node_delete:
    #    del nodes[_node_name]
    #print('here')
#
#
def find_member(concept, node1, node2):
    """
    """
    try:
        _name = str(node1) + '_' + str(node2)
        return concept[_name]
    except KeyError:
        try:
            _name = str(node2) + '_' + str(node1)
            return concept[_name]
        except KeyError:
            #print('+++++')
            print("   ** error member no found")
            return None
#
#
# --------------------------------------------------------------------
#                finding overlaping shell
# --------------------------------------------------------------------
#
#
def find_overlapping_shell(model, stiffeners):
    """
    """
    concept = model.component.concepts
    #group = model.component.sets
    _plates = [key for key in sorted(concept, key=lambda name: concept[name].number)
               if 'shell' in concept[key].type  or 'membrane' in concept[key].type]
    #
    _plates2 = [key for key in sorted(concept, key=lambda name: concept[name].number)
               if 'shell' in concept[key].type  or 'membrane' in concept[key].type]
    #
    #_plane = []
    #_line = []
    _nodes = []
    for key1 in _plates:
        memb1 = concept[key1]
        #if 'A003' in memb1.name:
        #    print('-->')
        _prop_name = 'PLT_' + memb1.section[0].name
        if len(memb1.node) == 3:
            A, B, C = get_3points(memb1)
            _nodes.append([A, B, C])
        else:
            # nodes member 1
            A, B, C, D = get_4points(memb1)
            AB = B - A
            AC = C - A
            AD = D - A
            _angle_BC = AB.angle(AC)
            _angle_BD = AB.angle(AD)
            if _angle_BC > _angle_BD:
                _new_nodes = [memb1.node[0],
                              memb1.node[1],
                              memb1.node[3],
                              memb1.node[2]]
                #
                memb1.node = _new_nodes
                A,B,C,D = get_4points(memb1)
            
            _nodes.append([A, B, C, D])
            #
            # check if stiffeners 
            try:
                _stiffener = stiffeners[_prop_name]
                _node_base = memb1.node[0]
                add_stiffeners(_stiffener, _nodes[-1], _node_base, model)
            except KeyError:
                #print(_prop_name)
                continue            
        #
        #BA = A - B
        #CB = C - B
        #CBA = BA.cross(CB)
        #_line.append(Line3(B, CBA))
        #_plane.append(Plane(A, B, C))
    #
    _plate_overlap = []
    for i, key1 in enumerate(_plates):
        #memb1 = concept[key1]
        #_plane1 = _plane[i]
        #_line1 = _line[i]
        _node1 = _nodes[i]
        for key2 in _plates2:
            # check if same member
            if key1 == key2:
                _plates2.remove(key2)
                continue
            memb1 = concept[key2]
            try:
                A,B,C,D = get_4points(memb1)
                # define member 2
                #memb2 = concept[key2]
                #_line2 = _line[2]
                #CC = _plane1.intersect(_line2)
                #_node2 = _nodes[j]
                if (_node1[0] == A and _node1[1] == B):
                    if (_node1[2] == C and _node1[3] == D):
                        #print('-->')
                        _plate_overlap.append(key2)
                        #
                        #try:
                        #    _plates.remove(key2)
                        #except ValueError:
                        #    pass
            except IndexError:
                A, B, C = get_3points(memb1)
                if (_node1[0] == A and _node1[1] == B and _node1[2] == C):
                    _plate_overlap.append(key2)                
            #if
    #
    #seen_set = set()
    #duplicate_set = set(x for x in _plate_overlap if x in seen_set or seen_set.add(x))
    #_plate_overlap = seen_set - duplicate_set
    _plate_overlap = set(_plate_overlap)
    for _plate in _plate_overlap:
        del concept[_plate]
    #
    #print('--->')
#
#
# get cones
#
def get_chord(_braces, _joint, _concept, _atol=1.5):
    """
    """
    _atol = _atol * math.pi / 180.0
    _branch = {}
    for _memb_name in _braces:
        _member = _concept[_memb_name]
        _n0 = _member.node[0]
        _n1 = _member.node[1]
        _branch[_member.name] = [_n0, _n1]
    
    _chord, _overlapping = asas_process.chord(_joint,  _concept, 
                                              _branch, _atol)
    
    return _chord, _overlapping
#
#
def update_cone_load(_cone, _cone_nodes, model, 
                     _new_member, end_distance):
    """
    """
    for key, memb_load in _cone.load.items():
        for _udl in memb_load.distributed: 
            _udl.distance_from_node[_cone_nodes[0]] += end_distance[0]
            _udl.distance_from_node[_cone_nodes[1]] += end_distance[1]
        #
        try:
            _new_member.load[key].distributed.extend(memb_load.distributed)
            _new_member.load[key].point.extend(memb_load.point)
        except KeyError:
            _new_member.load[key] = memb_load
        #
        _load = model.load.functional[key]
        _load.concept.remove(_cone.name)
        if not _new_member.name in _load.concept:
            _load.concept.append(_new_member.name)
#
#
def get_cones(cone_group, model):
    """
    """
    #
    _new_group = []
    _tobe_deleted = []
    for _memb_name in cone_group.concepts:
        _cone = model.component.concepts[_memb_name]
        #if '7023_7021' in _memb_name:
        #    print('-->', _cone.length_node2node)
        #if '4033_4031' in _memb_name :
        #    print('here')        
        #
        _new_K = _cone.properties.K
        _node_1 = _cone.node[0]
        _node_2 = _cone.node[1]
        _memb_cone = copy.copy(_node_1.elements)
        _memb_cone.extend(_node_2.elements)
        _memb_cone = list(set(_memb_cone))
        _memb_cone.remove(_memb_name)
        _tubular_joint = f2u.Geometry.Joints(name = 'dummy',
                                             node = _node_1)

        _chord, _overlapping = get_chord(_memb_cone,
                                         _tubular_joint, 
                                         model.component.concepts)
        #
        try:
            _cone_bottom = model.component.concepts[_chord[0]]
            _cone_top = model.component.concepts[_chord[1]]
        except IndexError:
            print('   *** warning --> cone {:} not processed'
                  .format(_memb_name))
            continue
        #
        if _cone_bottom.section[0].properties.D < _cone_top.section[0].properties.D:
            _cone_bottom = model.component.concepts[_chord[1]]
            _cone_top = model.component.concepts[_chord[0]]
        #
        # Effective length 
        for x in range(3):
            if _new_K[x] < _cone_bottom.properties.K[x]:
                _new_K[x] = _cone_bottom.properties.K[x]

            if _new_K[x] < _cone_top.properties.K[x]:
                _new_K[x] = _cone_top.properties.K[x]
        #
        # create new group
        _new_memb_name = _memb_name + '_cone'
        _new_group.append(_new_memb_name)
        #
        # create new beam element to include new cone members
        model.component.concepts[_new_memb_name] = copy.deepcopy(_cone_bottom)
        model.component.concepts[_new_memb_name].name = _new_memb_name
        #
        _new_member = model.component.concepts[_new_memb_name]
        _new_member.load = {}
        #
        # update material
        _new_member.material.append(_cone.material[0])
        _new_member.material.append(_cone_top.material[0])
        # update section
        _new_member.section.append(_cone.section[0])
        _new_member.section.append(_cone_top.section[0])
        #
        # update steps
        _new_member.step = [_cone_bottom.length_node2node,
                            _cone.length_node2node,
                            _cone_top.length_node2node]
        #
        #
        # update nodes
        _new_nodes = [_cone_bottom.node[0].name, 
                      _cone_bottom.node[1].name]
        #
        _cone_bottom_nodes = [0, 1]
        end_distance_1 = [0, _new_member.step[1] + _new_member.step[2]]
        end_distance_2 = [_new_member.step[0], _new_member.step[2]]
        _cone_nodes = [0, 1]
        # cone node
        _next_node = _cone.node[1]
        if _next_node.name in _new_nodes:
            if _next_node.name == _new_nodes[0]:
                _new_nodes = [_cone_bottom.node[1].name, 
                              _cone_bottom.node[0].name]
                # update new member nodes
                _new_member.node = [_cone_bottom.node[1],
                                    _cone_bottom.node[0]]
                # swap cone bottom nodes to keep cone [0, 1]
                _cone_bottom_nodes = [1, 0]
            #
            #end_distance_1 = [_new_member.step[1] + _new_member.step[2], 0]
            #end_distance_2 = [_new_member.step[2], _new_member.step[0]]
            #_cone_nodes = [1, 0]
            _next_node = _cone.node[0]
        else:
            if _cone.node[0].name == _new_nodes[0]:
                _new_nodes = [_cone_bottom.node[1].name, 
                              _cone_bottom.node[0].name]
                # update new member nodes
                _new_member.node = [_cone_bottom.node[1],
                                    _cone_bottom.node[0]]
                # swap cone bottom nodes to keep cone [0, 1]
                _cone_bottom_nodes = [1, 0]
        #
        _new_member.node.append(_next_node)
        _new_nodes.append(_next_node.name)
        # top cone node
        _cone_top_nodes = [0, 1]
        end_distance_3 = [_new_member.step[0] + _new_member.step[1] , 0]
        _next_node = _cone_top.node[1]
        if _next_node.name in _new_nodes:
            _next_node = _cone_top.node[0]
            _cone_top_nodes = [1, 0]
            #end_distance_3 = [0, _new_member.step[0] + _new_member.step[1]]
        #
        _new_member.node.append(_next_node)
        _new_nodes.append(_next_node.name)
        #
        # update K property
        _new_member.properties.K = _new_K
        #
        # update load
        #
        if _cone_bottom.load:
            update_cone_load(_cone_bottom, _cone_bottom_nodes, model, 
                             _new_member, end_distance_1)
        #
        if _cone.load:
            update_cone_load(_cone, _cone_nodes, model, 
                             _new_member, end_distance_2)                    
        #
        if _cone_top.load:
            update_cone_load(_cone_top, _cone_top_nodes, model, 
                             _new_member, end_distance_3)
        #
        _tobe_deleted.extend([_cone.name, _cone_bottom.name, _cone_top.name])
    #
    # updating group
    _group = model.component.sets
    _group_no = cone_group.number
    _name = 'GS{:}_cones'.format(_group_no)
    _group[_name] = f2u.Geometry.Groups(_name, _group_no)
    _group[_name].concepts = _new_group
    del _group['cones_new_set']
    #cone_group.concepts = _new_group
    # deleting old members
    for _memb_name in _tobe_deleted:
        try:
            del model.component.concepts[_memb_name]
        except KeyError:
            continue
#    
#
def get_nodesXarea(nodes, area, footprint):
    """ """
    node_fetch = [key for key, node in nodes.items()
                  if math.isclose(node.z, footprint[2])]
    #
    #centre = 
    tol1 = footprint[0] + area[0]
    tol2 = footprint[0] - area[0]
    node_1 = [key for key in node_fetch
              if nodes[key].x <= tol1 
              and  nodes[key].x >= tol2]
    #
    tol1 = footprint[1] + area[1]
    tol2 = footprint[1] - area[1]    
    node_2 = [key for key in node_1
              if nodes[key].y <= tol1 
              and  nodes[key].y >= tol2]
    return node_2
#