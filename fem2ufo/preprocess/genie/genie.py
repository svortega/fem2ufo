# 
# Copyright (c) 2009-2018 fem2ufo
# 


# Python stdlib imports
import sys
import os

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.genie.model.js as genie_js
import fem2ufo.postprocess.csv_format.csv_format as xcl

#
class GENIE_model:
    """Class reads GeniE concept model and place data in fem2ufo model concept"""
    #
    def __init__(self, tol=False):
        """
        """
        # js model converted to SI units
        self.units_in = None
        self.units_out = process.units.set_SI_units()
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """

        try:
            path = os.path.normcase(path)
            os.chdir(path)
        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()

        retval = os.getcwd()
        print('')
        print('--- Current working directory :')
        print('--- {:}'.format(retval))
    #
    def js_model(self, js_file):
        """
        """
        self.model = genie_js.read_model(js_file)
        #self.file_extension = 'js'
        #print('ok')
    #
    def xml_model(self, xml_file):
        """
        """
        pass
    #
    #
    def metocean(self, file_name):
        """
        """
        pass
    #
    def loading(self, file_name, file_format=False):
        """
        """
        if not file_format:
            file_format = self.file_extension
        
        if file_format == 'js':
            self.model.load = genie_js.read_basic_load(file_name, self.model)
        
    #
    def foundation(self, file_name, file_format=False):
        """
        """
        pass
    #
    def piles(self, file_name, file_format=False):
        """
        """
        pass
    #
    def load_combination(self, file_name, file_format=False):
        """
        """
        #
        if not self.model.load:
            print('  *** error --> basic load not found')
            sys.exit()
        #
        if not file_format:
            file_format = self.file_extension
        #
        if file_format == 'js':
            self.model = genie_js.read_load_combination(file_name, self.model)        
        
    #
    def get_model(self):
        """
        """
        # check units
        #
        if self.units_out.count(None) == len(self.units_out):
            self.units_out = self.units_in
        #
        self.model.data['units'] = self.units_out
        #
        return self.model
    #
    #