#
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports
import sys
import re
from itertools import islice

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.genie.operations.common as genie_common

#
def read_basic_load(file_name, model):
    """
    """
    js_file = genie_common.check_file(file_name)
    if not js_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    #
    print('')
    print('--- Reading load combination data from {:} input file'.format(file_name))
    print('')
    print('    * First pass')
    #
    dat_temp = open('CheckMe_'+ asas_file,'w+')
    #
    _keyWord = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A']
    _load_comb = {}
    _basicLC = {}
    _NoBLC = 0
    #
    for line in open(jsFile):
        if not line: 
            continue
        #
        # Jump commented lines
        if re.match(r"//", line): 
            continue
        # find key words and clean lines from comments
        if re.search(r"//", line): 
            line = remove_comment(line)
        #
        if re.search(r"setFemLoadcase", line, 0):
            _NoBLC += 1
            # print('1 ',line)
            _bLC = re.findall(r"\((\d+)\)", line)
            _NoBLC = int(_bLC[0])
            pattern = r"\."
            line = remove_comment(line, pattern)
            # print('2 ',_bLC, line)
            _basicLC[_NoBLC] = femData.BasicLoad(_NoBLC, line)
            #
    #
    #
    # print ('    * Second pass')
    #

    # DataInput.E7 = "File Read"
    print("File Read")
    #
    return _basicLC
    #
#
#
def read_fuctional_load(file_name):
    """
    """
    #
    js_file = process.common.check_input_file(file_name, file_extension='js')
    if not js_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    #
    #
    #_functional = {}
    _load = {}
    _section_load = {}
    _node_load = {}    
    #
    #_load_number = 0
    #
    with open(js_file) as fin: 
        for line in fin:
            line = genie_common.check_line_data(line)
            if not line:
                continue
            
            elif re.search(r"\=\s*LoadCase\(", line, 0):
                #_load_number += 1
                _load_name = genie_common.split_line(line, "=")
                _load[_load_name] = f2u.Load.Basic(_load_name)
            
            elif re.search(r"\.setFemLoadcase\(", line, 0):
                _load_name = genie_common.split_line(line, ".setFemLoadcase")
                _case = genie_common.split_line(line, "setFemLoadcase", 'left')
                _number = int(genie_common.get_bracket(_case))
                try:
                    _load[_load_name].number = _number
                except KeyError:
                    continue
    #
    #
    #
    #
    #lnumber = [ item.number for item in _load.values()]
    #max_load = max(lnumber)
    #
    return _load
#
#
def read_load_combinations(file_name, model, level=False):
    """
    """
    #
    js_file = process.common.check_input_file(file_name, file_extension='js')
    if not js_file:
        print('   *** error file {:} not found'.format(file_name))
        print("      try again", sys.exit())
    # current data
    try:
        _current = model.metocean.current
    except AttributeError:
        _current = {}
    #
    # wind data
    try:
        _wind = model.metocean.wind
    except AttributeError:
        _wind = {}    
    # wave data
    _wave_pot = {}
    try:
        for _name, _wave in model.metocean.wave.items():
            try:
                _wave_pot[_wave.name].append(_name)
            except KeyError:
                _wave_pot[_wave.name] = [_name]
        #
        _wave = model.metocean.wave
    except AttributeError:
        _wave = {}
    #
    #
    if model.analysis:
        _analysis = model.analysis
    else:
        model.set_analysis('Analysis')
        _analysis = model.analysis
    #
    #
    _condition = {}
    _load_comb = {}
    _load_dummy = {}
    _load = {}
    _section_load = {}
    _node_load = {}
    _elevation = {}
    #
    _start_load = {}
    #
    _load_number = 0
    _buoy_number = 0
    _elev_number = 0
    #
    _condition_name = "N/A"
    _wave_set = None
    #
    with open(js_file) as fin:
        for line in fin:
            line = genie_common.check_line_data(line)
            if not line:
                continue
            #
            if re.search(r"\=\s*LoadCombination\(", line, 0):
                _load_number += 1
                _load_name = genie_common.split_line(line, "=")
                #
                _load[_load_name] = f2u.Load.Combination(_load_name,
                                                             _load_number)
                #
                _data = genie_common.split_line(line, "LoadCombination", 'left')
                _load_analysis = genie_common.get_bracket(_data)
                #
                if _load_analysis:
                    try:
                        _load_comb[_load_analysis][_load_name] = _load[_load_name]
                        #print('ok')
                    except KeyError:
                        _load_comb[_load_analysis] = {}
                        _load_comb[_load_analysis][_load_name] = _load[_load_name]
                #
                else:
                    _load_dummy[_load_name] = _load[_load_name]
            #
            elif re.search(r"\.firstLoadCaseNumber\(", line, 0):
                _analysis_name = genie_common.split_line(line, ".step")
                #_analysis_name
                #
                _case = genie_common.split_line(line, "firstLoadCaseNumber", 'left')
                _number = int(genie_common.get_bracket(_case))
                _start_load[_analysis_name] = _number - 1
            #
            elif re.search(r"\=\s*DeterministicTime\(", line, 0):
                _condition_name = genie_common.split_line(line, "=")
                
                _condition[_condition_name] = {}
                _condition[_condition_name]['wave'] = {}
                _condition[_condition_name]['wind'] = {}
                _condition[_condition_name]['current'] = {}
                
                _case = genie_common.split_line(line, "DeterministicTime", 'left')
                _location_name = genie_common.get_bracket(_case)
                _condition[_condition_name]['location'] = _location_name
            #
            elif re.search(r"\=\s*Location\(", line, 0):
                _elev_name = genie_common.split_line(line, "=")
                _elev_number += 1
                _elevation[_elev_name] = f2u.Metocean.Condition(_elev_name,
                                                            _elev_number)
                #
                _data = genie_common.split_line(line, "=", "left")
                _data = genie_common.split_line(_data, "Location", 'left')
                _data = genie_common.get_bracket(_data)
                _data = _data.split(',')
                #
                #_item = _data[0].replace('m', '')
                _item, _conv = process.units.convert_units_length_metric(_data[0])
                _elevation[_elev_name].surface = float(_item) * _conv
                #
                #_item = _data[1].replace('m', '')
                _item, _conv = process.units.convert_units_length_metric(_data[1])
                _elevation[_elev_name].mudline = float(_item) * _conv
            #
            elif _condition_name in line:
                if re.search(r"\)\.regularWaveSet\s*\=", line, 0):
                    _wave_set = genie_common.split_line(line, "=", 'left')
                    _wave_set = _wave_set.replace(';', '')
                    _wave_set = _wave_set.strip()
                    #print(_wave_set)
                    #_condition[_condition_name]['wave'] = {}
                #
                #elif re.search(r"\.addCalmSea\(", line, 0):
                    #_wave_set = 'CalmSea'
                    #_buoy_number += 1
                    #if _buoy_number in 
                    #print('---')
                #
                elif re.search(r"\.component\(", line, 0):
                    _data = genie_common.split_line(line, "component", 'left')
                    _case = genie_common.split_line(_data, "water")
                    _comp_number = int(genie_common.get_bracket(_case, ').'))
                    _wave_name = _wave_set + '_number_' + str(_comp_number)
                    
                    if re.search(r"\.current\(", _data, 0):
                        _case = genie_common.split_line(_data, ".current", 'left')
                        _curr_name = genie_common.get_bracket(_case)
                        try:
                            _current[_curr_name].number = _comp_number
                            _condition[_condition_name]['current'][_comp_number] = _current[_curr_name]
                        except KeyError:
                            print('   *** warning current {:} not recognized'
                                  .format(_curr_name))
                    #
                    elif re.search(r"\.waveModel\(", _data, 0):
                        _case = genie_common.split_line(_data, "waveModel", 'left')
                        _theory = genie_common.get_bracket(_case)
                        _theory = _theory.replace('()', '')
                        
                        _order = False
                        if 'stream' in _theory.lower():
                            _order = genie_common.get_bracket(_theory)
                            _order = int(_order.replace(')', ''))
                            _theory = genie_common.split_line(_theory, "(")
                            #
                        
                        if _wave_name in _wave_pot[_wave_set]:
                            _wave[_wave_name].number = _comp_number
                            _wave[_wave_name].theory = _theory
                            
                            if _order:
                                _wave[_wave_name].order = _order
                            
                            #try:
                            #    _curr = _condition[_condition_name]['current'][_comp_number]
                            #    _wave[_wave_name].current = _curr.name
                            #except KeyError:
                            #    _wave[_wave_name].current = None
                            #
                            _condition[_condition_name]['wave'][_comp_number] = _wave[_wave_name]
                        
                        else:
                            print('   *** warning wave {:} not recognized'
                                  .format(_comp_number))
                    #
                    elif re.search(r"\.windProfile\(", _data, 0):
                        _case = genie_common.split_line(_data, "windProfile", 'left')
                        _wind_name = genie_common.get_bracket(_case)
                        try:
                            _wind[_wind_name].number = _comp_number
                            _condition[_condition_name]['wind'][_comp_number] = _wind[_wind_name]
                        except KeyError:
                            print('   *** warning wind {:} not recognized'
                                  .format(_comp_number))
                        #print('---')
                    #
                    #else:
                    #    print('---> ', line2)
                    #
                    #else:
                    #    break
            #
    #
    _analysis_number = 0
    _elev = {}
    #
    with open(js_file) as fin:   
        for line in fin:
            line = genie_common.check_line_data(line)
            if not line:
                continue
            #
            elif re.search(r"\=\s*Analysis\(", line, 0):
                _analysis_number += 1
                _analysis_name = genie_common.split_line(line, "=")
                _analysis.case[_analysis_name] = f2u.Analysis.Case(_analysis_name,
                                                                   _analysis_number)
                #
                while True:
                    data = list(islice(fin, 1))
                    line2 = genie_common.check_line_data(data[0])
                    if not line2:
                        continue
                    #
                    elif _analysis_name in line2:
                        if re.search(r"\(\s*WaveLoadActivity\s*\(", line2, 0):
                            _case = genie_common.split_line(line2, "WaveLoadActivity", 'left')
                            _wave_set = genie_common.get_bracket(_case)
                            _wave_set = _wave_set.replace(')', '')
                            #
                            _location_name = _condition[_wave_set]['location']
                            _analysis.case[_analysis_name].condition = _elevation[_location_name]
                        #
                        #elif re.search(r"\.deterministicSeastates\s*\(", line2, 0):
                        #    if re.search(r"\.seastate\s*\(", line2, 0):
                        #        _case = genie_common.split_line(line2, "seastate", 'left')
                        #        print('-->')
                        #
                        elif re.search(r"\.addLevel\s*\(", line2, 0):
                            _case = genie_common.split_line(line2, "addLevel", 'left')
                            _lev = genie_common.get_bracket(_case)
                            #_no_elev += 1
                            _name_elev = _analysis_name
                            # FIXME : units to be converted
                            _item, _conv = process.units.convert_units_length_metric(_lev)
                            _elev[_name_elev] = float(_item) * _conv
                            #_elev[_name_elev] = _lev.replace('m', '')
                        #
                        elif re.search(r"\.seastate\s*\(", line2, 0):
                            _case = genie_common.split_line(line2, "seastate", 'left')
                            _wave_no = genie_common.split_line(_case, ".")
                            _wave_no = int(genie_common.get_bracket(_wave_no, ')'))
                            # add start load number
                            try:
                                _meto_no = _wave_no + _start_load[_analysis_name]
                            except KeyError:
                                _meto_no = _wave_no
                            #
                            try:
                                _wave_data = genie_common.split_line(line2, "doppleroption", 'left')
                                continue
                            except IndexError:
                                _wave_data = genie_common.split_line(line2, "dataPhase", 'left')
                                _wave_data = genie_common.get_bracket(_wave_data)
                                _wave_data = _wave_data.split(',')
                            #
                            # wave data defined
                            try:
                                _wave_temp = _condition[_wave_set]['wave'][_wave_no]
                                _wave_res = _wave_temp.name
                                _name = _analysis_name + '_seastate_' + str(_wave_no)
                                
                                # check if wave name already exist
                                if '_number_' in _wave_temp.name:
                                    _wave_name =  _wave_temp.name 
                                else:
                                    _wave_name =  _wave_temp.name + '_number_' + str(_wave_no)                                   
                                    _wave_temp.name = _wave_name
                                #
                                #_seastate[_wave_no] = f2u.MetoceanCombination(int(_wave_no), _wave_name)
                                
                                _analysis.case[_analysis_name].metocean_combination[_wave_no] = f2u.Metocean.Combination(_name,
                                                                                                                         int(_meto_no))
                                _analysis.case[_analysis_name].metocean_combination[_wave_no].wave = _wave_temp
                                _analysis.case[_analysis_name].metocean_combination[_wave_no].wave_direction = _wave_temp.direction
                                #_wave_temp.kinematics = float(_wave_data[6])
                                _analysis.case[_analysis_name].metocean_combination[_wave_no].wave_kinematics = float(_wave_data[6])
                                #
                                #_wave_temp.buoyancy = _wave_data[3].strip()
                                _analysis.case[_analysis_name].metocean_combination[_wave_no].buoyancy = _wave_data[3].strip()
                                #_wave_temp.analysis = _analysis_name
                                #_analysis.case[_analysis_name].metocean_combination[_wave_no].analysis = _analysis_name
                                #
                                _analysis.case[_analysis_name].metocean_combination[_wave_no].design_load = _wave_data[4].strip()
                                #
                                #_analysis.case[_analysis_name].wave.append(_wave_temp)
                                #
                                try:
                                    _curr = _condition[_wave_set]['current'][_wave_no]
                                    _analysis.case[_analysis_name].metocean_combination[_wave_no].current = _curr
                                    _analysis.case[_analysis_name].metocean_combination[_wave_no].current_direction = _curr.direction
                                    
                                    if not 'NoStretching' in _wave_data[0]:
                                        #_wave_temp.current_stretching = True
                                        _analysis.case[_analysis_name].metocean_combination[_wave_no].current_stretching = True
                                    #_wave_temp.current_blockage = float(_wave_data[5])
                                    _analysis.case[_analysis_name].metocean_combination[_wave_no].current_blockage = float(_wave_data[5])
                                    #
                                    #_analysis.case[_analysis_name].current.append(_curr)
                                    #
                                except KeyError:
                                    pass
                                #
                                #
                                try:
                                    _viento = _condition[_wave_set]['wind'][_wave_no]
                                    _analysis.case[_analysis_name].metocean_combination[_wave_no].wind = _viento
                                    _analysis.case[_analysis_name].metocean_combination[_wave_no].wind_direction = _viento.direction
                                    #
                                    #_analysis.case[_analysis_name].wind.append(_viento)
                                    #
                                except KeyError:
                                    pass
                            # probably calm sea and/or wind load only
                            except KeyError:
                                _wave_name = _wave_res +'_number_'+ str(_wave_no)
                                _name = _analysis_name + '_seastate_' + str(_wave_no)
                                _wave[_wave_name] = f2u.Metocean.Wave(_name, _wave_no)
                                _wave[_wave_name].number = _wave_no
                                _wave[_wave_name].name = _wave_name
                                _wave[_wave_name].theory = 'calm sea'
                                _wave[_wave_name].direction = 0
                                _condition[_wave_set]['wave'][_wave_no] = _wave[_wave_name]                                    
                                #
                                # add start load number
                                try:
                                    _meto_no = _wave_no + _start_load[_analysis_name]
                                except KeyError:
                                    _meto_no = _wave_no
                                #
                                _analysis.case[_analysis_name].metocean_combination[_wave_no] = f2u.Metocean.Combination(_name,
                                                                                                                         int(_meto_no))
                                _analysis.case[_analysis_name].metocean_combination[_wave_no].wave = _wave[_wave_name]
                                _analysis.case[_analysis_name].metocean_combination[_wave_no].wave_direction = 0
                                _analysis.case[_analysis_name].metocean_combination[_wave_no].buoyancy = _wave_data[3].strip()
                                _analysis.case[_analysis_name].metocean_combination[_wave_no].design_load = _wave_data[4].strip()
                                #
                                try:
                                    _viento = _condition[_wave_set]['wind'][_wave_no]
                                    _analysis.case[_analysis_name].metocean_combination[_wave_no].wind = _viento
                                    _analysis.case[_analysis_name].metocean_combination[_wave_no].wind_direction = _viento.direction
                                    #
                                    #_analysis.case[_analysis_name].wind.append(_viento)
                                    #
                                except KeyError:
                                    pass                                
                                #
                            #
                            # check if water depth data
                            try:
                                _lev = float(_wave_data[7].replace('m', ''))
                                #_lev = float(_wave_data[7])
                               # _wave[_wave_name].water_depth = _lev
                            except IndexError:
                                #_wave[_wave_name].water_depth = _elev[_name_elev]
                                _lev = _elevation[_location_name].mudline + _elevation[_location_name].surface
                            #
                            _wave[_wave_name].water_depth = _lev
                        #
                    #
                    else:
                        break
            #
            elif re.search(r"\.setFemLoadcase\(", line, 0):
                _load_name = genie_common.split_line(line, ".setFemLoadcase")
                _case = genie_common.split_line(line, "setFemLoadcase", 'left')
                _number = int(genie_common.get_bracket(_case))
                try:
                    _load[_load_name].number = _number
                except KeyError:
                    continue
    #
    with open(js_file) as fin:
        for line in fin:
            line = genie_common.check_line_data(line)
            if not line:
                continue
            #
            if re.search(r"\=\s*LoadCombination\(", line, 0):
                _data = genie_common.split_line(line, "LoadCombination", 'left')
                _load_analysis = genie_common.get_bracket(_data)                
            #
            elif re.search(r"\.addCase\(", line, 0):
                _load_name = genie_common.split_line(line, ".")
                _data = genie_common.split_line(line, "addCase", "left")
                _case = genie_common.get_bracket(_data)
                #
                if re.search(r"\.WLC\(", _case, 0):
                    #_load_analysis = genie_common.split_line(_case, ".")
                    _wave_data = genie_common.split_line(_case, _load_analysis, 'left')
                    _wave_data = _wave_data.split(',')
                    _item_number = int(genie_common.split_line(_wave_data[0], "(", 'left'))
                    _factor = float(_wave_data[2])
                    # check if wave
                    #try:
                    _seastate = _analysis.case[_load_analysis].metocean_combination[_item_number]
                    _load[_load_name].metocean_load.append([_seastate.name, _factor])
                    #except KeyError:
                    #    pass
                    #
                else:
                    _case = _case.split(",")
                    _load_id = _case[0]
                    _factor = float(_case[1])
                    #try:
                    _load[_load_name].functional_load.append([_load_id, _factor])
                    #except KeyError:
                    #    pass
            #
            elif re.search(r"\.designCondition\(", line, 0):
                _load_name = genie_common.split_line(line, ".")
                _data = genie_common.split_line(line, "designCondition", "left")
                design_condition = genie_common.get_bracket(_data)
                try:
                    _load[_load_name].design_condition = design_condition
                except KeyError:
                    pass
            #
            #
            #elif re.search(r"\=\s*Location\(", line, 0):
            #    _elev_name = genie_common.split_line(line, "=")
            #    _analysis.case[_elev_name].condition = _elevation[_elev_name]
    #
    # if load in load_dummy then insert
    # functional loads instead
    if _load_dummy:
        for _name, lcomb in _load.items():
            _func_new = []
            #if 'LC250_' in _name :
            #    print('here')
            for lc in lcomb.functional_load:
                try:
                    _dummy = _load_dummy[lc[0]]
                    for lc2 in _dummy.functional_load:
                        _func_new.append([lc2[0], lc2[1]*lc[1]])
                except KeyError:
                    _func_new.append(lc)
            #
            lcomb.functional_load = _func_new
    #
    #
    _level_zero = {}
    _level_one = {}
    #
    for _load_analysis, _comb  in _load_comb.items():
        for _name, lcomb in _comb.items():
            #if _name == 'C324':
            #    print('here')
            if not level:
                _functional = model.load.functional
            else:
                _functional = _analysis.case[_load_analysis].load_combination[0] 
            #
            # flag determines if level zero = True or level one = False
            flag = True
            _func_new = []
            for lc in lcomb.functional_load:
                # if basic load in in functional then level zero
                try:
                    _functional[lc[0]]
                    _func_new.append(lc)
                # check if level one
                except KeyError:
                    try:
                        _dummy = _load_dummy[lc[0]]
                        for lc2 in _dummy.functional_load:
                            _func_new.append([lc2[0], lc2[1]*lc[1]])
                    except KeyError:
                        # if basic load within the actual load combination
                        # then it is assumed this load comb is in level one
                        try:
                            _comb[lc[0]]
                            flag = False
                            break
                        except KeyError:
                            # if basic load within the dummy load case
                            # then what?
                            print('   *** warning load {:} type not found'.format(lc[0]))
                            continue
            #
            if flag:
                lcomb.functional_load = _func_new
                try:
                    _level_zero[_load_analysis][_name] = lcomb
                except KeyError:
                    _level_zero[_load_analysis] = {}
                    _level_zero[_load_analysis][_name] = lcomb
            else:
                try:
                    _level_one[_load_analysis][_name] = lcomb
                except KeyError:
                    _level_one[_load_analysis] = {}
                    _level_one[_load_analysis][_name] = lcomb
    #
    #print('-->')
    return _level_zero, _level_one
#
#