# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports
#import sys
import re

# package imports

#
def remove_comment(_lineIn, pattern=r"//"):
    """
    """
    reobj = re.compile(pattern)
    for match in reobj.finditer(_lineIn):
        _index = match.start()

    _lineOut = _lineIn[0:_index]
    _lineOut = _lineOut.strip()
    return _lineOut
    #
#
#
#
#
def check_line_data(line):
    """
    """
    if not line: 
        line_out = False
    #
    elif re.match(r"//", line): 
        line_out = False
    #
    elif re.search(r"//", line): 
        line_out = remove_comment(line)
    
    else:
        line_out = line
    
    return line_out
#
#
def split_line(line, flag, rigth=False):
    """
    """
    extension = line.split(flag, 1)
    
    if rigth:
        line_out = extension[1].strip()
    else:
        line_out = extension[0].strip()
    
    return line_out
#
#
def get_bracket(line, end_bracket=");"):
    #
    if "(" in line:
        extension = line.split("(", 1)
        extension = extension[1].split(end_bracket, 1)
    #
    elif "[" in line:
        extension = line.split("[", 1)
        extension = extension[1].split("]", 1)
    #
    line_out = extension[0].strip()
    
    return line_out
#