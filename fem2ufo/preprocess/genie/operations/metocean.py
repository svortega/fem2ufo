#
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports
import sys
import re
import math

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.genie.operations.common as genie_common

#
def get_array(line, unit, unit_factor=1):
    """
    """
    _items = genie_common.split_line(line, "Array", "left")
    _items = genie_common.get_bracket(_items)
    _items = _items.split(',')
    for x in range(len(_items)):
        _items[x] = float(_items[x].replace(unit, '')) * unit_factor
    #
    return _items
#
#
def read_metocean(model, file_name):
    """
    """
    #
    #
    js_file = process.common.check_input_file(file_name, file_extension='js')
    if not js_file:
        print('   *** error file {:} not found'.format(file_name))
        print("       try again", sys.exit())
    #
    #
    print('')
    print('---------------- READ METOCEAN Module --------------')    
    #
    _wave_number = 0
    _wind_number = 0
    _current_number = 0
    _elev_number = 0
    #
    _wave = {}
    _wind = {}
    _current = {}
    _elevation = {}
    #
    with open(js_file) as fin:
        #    
        for line in fin:
            #
            line = genie_common.check_line_data(line)
            if not line:
                continue
            #
            elif re.search(r"\=\s*RegularWaveSet\(", line, 0):
                _set_name = genie_common.split_line(line, "=")
                _wave_number = 0
                #_wave_number += 1
                #_wave[_wave_name] = f2u.Wave(_wave_number, _wave_name)
                #while True:
                #    data = list(islice(fin, 1))
            # 
            elif re.search(r"\.add\s*\(RegularWave\(", line, 0):
                _wave_number += 1
                _wave_name = genie_common.split_line(line, ".")
                _wave_name = _wave_name + '_number_' + str(_wave_number)
                
                _wave[_wave_name] = f2u.Metocean.Wave(_set_name,
                                                  _wave_number)                
                
                
                _data = genie_common.split_line(line, "RegularWave", 'left')
                _case = genie_common.get_bracket(_data)
                _case = _case.split(',')
                #print('ok')
                try:
                    _dir = _case[0].split('deg')
                    _wave[_wave_name].direction = float(_dir[0])
                except ValueError:
                    _dir = _case[0].split('rad')
                    _deg = math.degrees(float(_dir[0]))
                    _wave[_wave_name].direction = round(_deg, 3)
                #
                _wh = _case[1].split('m')
                _wave[_wave_name].height = float(_wh[0]) #* _length_factor
                _period = genie_common.split_line(_case[2], "WavePeriod", 'left')
                _period = genie_common.get_bracket(_period, ")")
                _period = _period.replace('s', '')
                _wave[_wave_name].period = float(_period)
            # 
            elif re.search(r"\=\s*CurrentProfileRelDir\(", line, 0):
                _current_name = genie_common.split_line(line, "=")
                _current_number += 1
                _current[_current_name] = f2u.Metocean.Current(_current_name,
                                                           _current_number)
                
                _prof = genie_common.split_line(line, "CurrentProfileRelDir", 'left')
                _prof = genie_common.get_bracket(_prof)
                _prof = _prof.split(',')
                
                _current[_current_name].profile.append([_prof[0].strip(), _prof[2].strip()])
                _current[_current_name].direction = _prof[1].strip()
                
                #
                _dir_type = _prof[3].strip()
                #
                if _dir_type == 'dtRelativeX':
                    _current[_current_name].alignment = 'x axis'
                
                elif _dir_type == 'dtRelativeHeading':
                    _current[_current_name].alignment = 'wave'
                
                elif _dir_type == 'dtAlongHeading':
                    _current[_current_name].alignment = 'along'
                
                else:
                    print('   *** error current alignment type : {:} not found'
                          .format(_dir_type))
                    sys.exit()                
                
                if _prof[4].strip() == 'true':
                    _current[_current_name].absolute_elevation = True
                
                
                #print('cur')
            # 
            elif re.search(r"\=\s*WindProfileRelDir\(", line, 0):
                _wind_name = genie_common.split_line(line, "=")
                _wind_number += 1
                
                _wind[_wind_name] = f2u.Metocean.Wind(_wind_name, 
                                                  _wind_number)
                
                _data = genie_common.split_line(line, "=", "left")
                _data = genie_common.split_line(_data, "WindProfileRelDir", 'left')
                _data = genie_common.get_bracket(_data)
                _data = _data.split(',')
                
                _item = _data[0].replace('m/s', '')
                _item = _item.strip()
                _wind[_wind_name].velocity = float(_item)
                
                _item = _data[1].replace('m', '')
                _item = _item.strip()                
                _wind[_wind_name].height = float(_item)
                
                # 
                _item = _data[2].strip()
                # wind profile of type General
                try:
                    _wind[_wind_name].power = float(_item)
                    _dir_type = _data[3].strip()
                    _wind_type = _data[4].strip()
                    
                    try:
                        _item = _data[5].replace('deg', '')
                        _item = float(_item.strip())
                    except ValueError:
                        _item = _data[5].replace('rad', '')
                        _item = float(_item.strip())
                        _item = round(math.degrees(_item), 3)
                    _wind[_wind_name].direction = float(_item)
                    
                    _wind[_wind_name].gust_factor = float(_data[6].strip())
                    
                # wind profile of type Normal or Extreme
                except ValueError:
                    _dir_type = _item
                    _wind_type = _data[3].strip()
                    try:
                        _item = _data[4].replace('deg', '')
                        _item = float(_item.strip())
                    except ValueError:
                        _item = _data[4].replace('rad', '')
                        _item = float(_item.strip())
                        _item = round(math.degrees(_item), 3)
                    _wind[_wind_name].direction = _item
                    
                    _wind[_wind_name].period_ratio = float(_data[5].strip()) 
                #
                #
                if _dir_type == 'dtRelativeX':
                    _wind[_wind_name].alignment = 'x axis'
                
                elif _dir_type == 'dtRelativeHeading':
                    _wind[_wind_name].alignment = 'wave'
                
                elif _dir_type == 'dtAlongHeading':
                    _wind[_wind_name].alignment = 'along'
                
                else:
                    print('   *** error wind alignment type : {:} not found'
                          .format(_dir_type))
                    sys.exit()
                #
                #
                if _wind_type == 'wpGeneral':
                    _wind[_wind_name].formula = 'general'
                
                elif _wind_type == 'wpNormal':
                    _wind[_wind_name].formula = 'normal'
                
                elif _wind_type == 'wpABS':
                    _wind[_wind_name].formula = 'abs'
            
                elif _wind_type == 'wpExtreme':
                    _wind[_wind_name].formula = 'extreme'
                
                elif _wind_type == 'wpExtremeAPI21':
                    _wind[_wind_name].formula = 'extreme_api21'
                
                else:
                    print('   *** error wind formula type : {:} not found'
                          .format(_wind_type))
                    sys.exit()
            #
            elif re.search(r"\=\s*Location\(", line, 0):
                _elev_name = genie_common.split_line(line, "=")
                _elev_number += 1
                _elevation[_elev_name] = f2u.Metocean.Condition(_elev_name,
                                                            _elev_number)
                #
                _data = genie_common.split_line(line, "=", "left")
                _data = genie_common.split_line(_data, "Location", 'left')
                _data = genie_common.get_bracket(_data)
                _data = _data.split(',')
                #
                _item = _data[0].replace('m', '')
                _item = _item.strip()
                _elevation[_elev_name].surface = float(_item)
                #
                _item = _data[1].replace('m', '')
                _item = _item.strip()
                _elevation[_elev_name].mudline = float(_item)
        #
    #
    _curr_lev = {}
    _angle = {}
    _velocities = {}
    #
    with open(js_file) as fin:
        #    
        for line in fin:
            #
            line = genie_common.check_line_data(line)
            if not line:
                continue
            #
            elif re.search(r"\_elevations\s*=\s*Array", line, 0):
                _current_name = genie_common.split_line(line, "=")
                _current_name = _current_name.strip()
                _curr_lev[_current_name] = get_array(line, 'm')
                #print('-->')
            #
            elif re.search(r"\_angles\s*=\s*Array", line, 0):
                _current_name = genie_common.split_line(line, "=")
                _current_name = _current_name.strip()
                try:
                    _angle[_current_name] = get_array(line, 'deg')
                except ValueError:
                    _rad = get_array(line, 'rad')
                    try:
                        _angle[_current_name] = math.degrees(_rad)
                    except TypeError:
                        _deg = [math.degrees(x) for x in _rad]
                        _angle[_current_name] = _deg
            #
            elif re.search(r"\_velocities\s*=\s*Array", line, 0):
                _current_name = genie_common.split_line(line, "=")
                _current_name = _current_name.strip()
                _velocities[_current_name] = get_array(line, 'm/s')
            #
    #
    #
    for _curr in _current.values():
        _dir = _angle[_curr.direction]
        _curr.direction = max(_dir)
        _elev = _curr_lev[_curr.profile[0][0]]
        _vel = _velocities[_curr.profile[0][1]]
        _curr.velocity = max(_vel)
        _temp = []
        for x in range(len(_vel)):
            _temp.append([_elev[x], _vel[x] / _curr.velocity])
        #
        _curr.profile = _temp
        #  
    #
    #
    #model = f2u('metocean')
    model.set_metocean()
    model.metocean.wave = _wave
    model.metocean.wind = _wind
    model.metocean.current = _current
    #_metocean.elevation = _elevation
    #
    #return model.metocean
#
#