# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import re
import math
import sys


# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.genie.operations.common as genie_common
from fem2ufo.preprocess.common.foundation import get_spring_material

#
def check_spring(spring, _diameter, spr_type):
    """
    """
    i = 0
    for _key, _spr in sorted(spring.items()):
    #for _spr in spring.values():
        _disp = _spr.displacement
        _name = _spr.name
        _diam = _diameter[_name]
        #  Qz
        if 'qz' in spr_type.lower():
            _stress_factor = (_diam**2 * math.pi) / 4.0
            _forceTemp = [press * _stress_factor for press in _spr.force]
            _force = _forceTemp

            if _spr.force[1] == 0.0 and _disp[1] == 0.0:
                f_add = [0 for x in range(len(_forceTemp))]
                d_add = [item * -1 for item in _disp[::-1]]
            
                _force = f_add[:len(_forceTemp)-2]
                _force.extend(_forceTemp[1:])
            
                _disp = d_add[:len(_disp)-2]
                _disp.extend(_spr.displacement[1:])
        # Py & Tz
        else:
            if 'py' in spr_type.lower():
                _stress_factor = _diam
            # tz
            else:
                _stress_factor = _diam * math.pi
            #
            _forceTemp = [press * _stress_factor for press in _spr.force]
            _force = _forceTemp
            #
            if _spr.force[0] == 0.0 and _disp[0] == 0.0:
                f_add = [item * -1 for item in _forceTemp[::-1]]
                d_add = [item * -1 for item in _disp[::-1]]
                
                _force = f_add[:len(_forceTemp)-1]
                _force.extend(_forceTemp)
            
                _disp = d_add[:len(_disp)-1]
                _disp.extend(_spr.displacement)
        #
        i += 1
        _spr.force = _force
        _spr.displacement = _disp
        #_spr.name = spr_type
        _spr.name =  spr_type +'_'+ _key
        _spr.number = i
    #
    return spring
#  
def get_curve_list(data, conv_type):
    """
    """
    _items = data.split(',')
    _curve = []
    for _item in _items:
        _data, _factor = conv_type(_item)
        
        _curve.append(float(_data) * _factor)
    #
    return _curve
#
def get_curve(data, PY):
    """
    """
    _data = genie_common.get_bracket(data)
    _diameter = genie_common.split_line(_data, "Array")
    _data = genie_common.split_line(_data, _diameter, "left")
    _diameter = _diameter.replace(',', '')
    
    _item, _conv = process.units.convert_units_length_metric(_diameter)
    
    _diameter = float(_item) * _conv
    
    _curve_1 = genie_common.split_line(_data, ", Array", "left")
    _curve_1 = genie_common.get_bracket(_curve_1, ')')
    PY.force = get_curve_list(_curve_1, process.units.convert_units_pressure_metric)

    _curve_2 = genie_common.split_line(_data, ", Array")
    _curve_2 = _curve_2.replace("Array", "")
    _curve_2 = genie_common.get_bracket(_curve_2, ')')
    PY.displacement = get_curve_list(_curve_2, process.units.convert_units_length_metric)
    
    return _diameter
#
#
def read_soil(file_name):
    """
    """
    #
    #
    js_file = process.common.check_input_file(file_name, 
                                              file_extension='js')
    if not js_file:
        print('   *** error file {:} not found'.format(file_name))
        sys.exit()
    #
    #
    print('')
    print('------------------ READ SOIL Module ----------------')
    #
    #
    _user_data = {}
    _elevation = {}
    soil = {}
    _PY = {}
    _TZ = {}
    _QZ = {}
    #_soil_id = {}
    soil_curves = {}
    #
    #_isoil = 0
    _springNo = 1
    _bottom = 0
    _Zcoord = 0    
    #
    _elev_number = 0
    _diameter = {}
    _scour = {}
    _scour_no = 0
    #
    with open(js_file) as fin: 
        for line in fin:
            line = genie_common.check_line_data(line)
            if not line:
                continue
            #
            elif re.search(r"\=\s*Location\(", line, 0):
                _location_name = genie_common.split_line(line, "=")
                _elev_number += 1
                _elevation[_location_name] = f2u.Metocean.Condition(_location_name,
                                                                _elev_number)
                #
                _data = genie_common.split_line(line, "=", "left")
                _data = genie_common.split_line(_data, "Location", 'left')
                _data = genie_common.get_bracket(_data)
                _data = _data.split(',')
                #
                _item, _conv = process.units.convert_units_length_metric(_data[0])
                _elevation[_location_name].surface = float(_item) * _conv
                #
                _item, _conv = process.units.convert_units_length_metric(_data[1])
                _elevation[_location_name].mudline = float(_item) * _conv
                #
                soil_curves[_location_name] = {}
                _nolayer = 0
                
                _isoil = _location_name
                try:
                    soil[_isoil]
                except KeyError:
                    soil[_isoil] = f2u.Foundation.Soil(_location_name,
                                                 _elev_number)
                #
                soil[_isoil].mudline = _elevation[_location_name].mudline
                #_soil_id[_location_name] = _isoil
            #
            elif re.search(r"\.insertSoilBorder\s*\(", line, 0):
                _nolayer += 1
                _data = genie_common.split_line(line, "insertSoilBorder", 'left')
                _data = genie_common.get_bracket(_data)
                _item, _conv = process.units.convert_units_length_metric(_data)
                #_depth = _data.replace('m', '')
                _depth = float(_item) * _conv
                _zbottom = soil[_isoil].mudline - _depth
                _depth = abs(_depth)
                soil[_isoil].layers[_depth] = f2u.Foundation.Layer(_depth, _nolayer)
                soil[_isoil].layers[_depth].depth = _zbottom
                soil_curves[_location_name][_nolayer] = _depth
            #
            elif re.search(r"\.addManualPY\s*\(", line, 0):
                _curves = genie_common.split_line(line, ".addManualPY")
                _pyNo = _curves #+ '_py'
                _PY[_pyNo] = f2u.Geometry.SpringElement(_pyNo)
                #
                _data = genie_common.split_line(line, ".addManualPY", "left")
                _diam = get_curve(_data, _PY[_pyNo])
                _diameter[_curves] = _diam
            #
            elif re.search(r"\.addManualTZ\s*\(", line, 0):
                _curves = genie_common.split_line(line, ".addManualTZ")
                _tzNo = _curves #+ '_tz'
                _TZ[_tzNo] = f2u.Geometry.SpringElement(_tzNo)
                _data = genie_common.split_line(line, ".addManualTZ", "left")
                _diam = get_curve(_data, _TZ[_tzNo])
            #
            elif re.search(r"\.addManualQZ\s*\(", line, 0):
                _curves = genie_common.split_line(line, ".addManualQZ")
                _qzNo = _curves #+ '_qz'
                _QZ[_qzNo] = f2u.Geometry.SpringElement(_qzNo)
                _data = genie_common.split_line(line, ".addManualQZ", "left")
                _diam = get_curve(_data, _QZ[_qzNo])
            #
            elif re.search(r"\=\s*Scour\(", line, 0):
                _scour_name = genie_common.split_line(line, "=")
                _scour_no += 1
                _scour[_scour_name] = f2u.Foundation.Scour(_scour_name, _scour_no)
                #
                _data = genie_common.split_line(line, "=", "left")
                _data = genie_common.split_line(_data, "Scour", 'left')
                _data = genie_common.get_bracket(_data)
                _data = _data.split(',')
                
                _item, _conv = process.units.convert_units_length_metric(_data[0])
                _scour[_scour_name].general = float(_item) * _conv  # float(_data[0].replace('m', ''))
                
                _item, _conv = process.units.convert_units_length_metric(_data[1])
                _scour[_scour_name].local = float(_item) * _conv    # float(_data[1].replace('m', ''))
                
                _scour[_scour_name].slope = float(_data[2].replace('deg', ''))
            #
    #
    #
    with open(js_file) as fin:
        for line in fin:
            line = genie_common.check_line_data(line)
            if not line:
                continue
            #
            elif re.search(r"\=\s*Location\(", line, 0):
                _location_name = genie_common.split_line(line, "=")
                _isoil = _location_name # _soil_id[_location_name]
            #   
            elif re.search(r"\.soilCurves\s*\=", line, 0):
                _data = genie_common.split_line(line, "=", "left")
                _curves = _data.replace(';', '')
                #
                _data = genie_common.split_line(line, ".soil", "left")
                _data = int(genie_common.get_bracket(_data, ')'))
                _depth = soil_curves[_location_name][_data]
                #
                soil[_isoil].layers[_depth].diameter = _diameter[_curves]
                _number = soil[_isoil].layers[_depth].number
                
                _pyNo = _curves #+ '_py'
                soil[_isoil].layers[_depth].curve['PY'] = _PY[_pyNo]
                _PY[_pyNo].number = _number
                
                _tzNo = _curves #+ '_tz'
                soil[_isoil].layers[_depth].curve['TZ'] = _TZ[_tzNo]
                _TZ[_tzNo].number = _number
                
                _qzNo = _curves #+ '_qz'
                soil[_isoil].layers[_depth].curve['QZ'] = _QZ[_qzNo]
                _QZ[_qzNo].number = _number
            # 
            elif re.search(r"\.numberOfSublayers\s*\=", line, 0):
                _data = genie_common.split_line(line, "=", "left")
                _sublayer = int(_data.replace(';', ''))
                #
                _data = genie_common.split_line(line, ".soil", "left")
                _data = int(genie_common.get_bracket(_data, ')'))
                _depth = soil_curves[_location_name][_data]
                
                soil[_isoil].layers[_depth].sublayers = _sublayer
    #
    # check & update springs
    _PY = check_spring(_PY, _diameter, spr_type='py')
    _TZ = check_spring(_TZ, _diameter, spr_type='tz')
    _QZ = check_spring(_QZ, _diameter, spr_type='qz')
    #
    spring = [_PY, _TZ, _QZ]
    #
    _material_spring = get_spring_material(spring)
    #
    #
    #_model = f2u.Foundation('soil', 4, )
    #_model.soil = soil
    #_model.materials = _mat
    #_model.data = _user_data
    #
    #if _scour:
    #    for _soil in _model.soil.values():
    #        _soil.scour = _scour
    #
    #
    print('--- End READ SOIL module')      
    #
    #
    return soil, _material_spring, _user_data
    #