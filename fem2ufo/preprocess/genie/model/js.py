#
# Copyright (c) 2009-2020 fem2ufo
# 


# Python stdlib imports
import sys
import re

# package imports
import fem2ufo.f2u_model.control as f2u
#import fem2ufo.process.control as process
from fem2ufo.preprocess.common.foundation import read_foundation
#
#import fem2ufo.preprocess.genie.operations.common as genie_common
import fem2ufo.preprocess.genie.operations.load as genie_load
import fem2ufo.preprocess.genie.operations.metocean as genie_meto

#
#
def read_model(file_name):
    """
    """
    #
    #
    print('')
    print('------------------ READJS Module -------------------')    
    #
    #
    # create geometry concept model
    model = f2u.F2U_model(file_name, 1)
    #
    model.data['format'] = 'genie'
    #
    #
    # read metocean data
    genie_meto.read_metocean(model, file_name)
    #
    #
    # read soil
    try:
        file_format = 'genie'
        units_in = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
        units_out = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']        
        read_foundation(model, file_name, file_format, 
                        units_in, units_out)
    except KeyError:
        print('    ** Warning, soil spring missing')
        pass  # probably no soil data in the model
    #
    # read basic loading
    func_load = genie_load.read_fuctional_load(file_name)
    # create load concept model
    model.set_load('loading')    
    model.load.functional = func_load
    # read load combinations
    _level_zero, _level_one = genie_load.read_load_combinations(file_name, model)
    #  
    if _level_zero:
        for _name, _comb in _level_zero.items():
            try:
                model.analysis.case[_name].load_combination[0].update(_comb)
            except KeyError:
                model.analysis.case[_name].load_combination[0] = _comb
    #
    if _level_one:
        for _name, _comb in _level_one.items():
            try:
                model.analysis.case[_name].load_combination[1].update(_comb)
            except KeyError:
                model.analysis.case[_name].load_combination[1] = _comb
    #     
    #
    #print('--->')
    return model
#
def read_metocean(file_name):
    """
    """
    #
    _metocean = genie_meto.read_metocean(file_name)
    #
#
def read_basic_load(file_name, model=False):
    """
    """
    _load = genie_load.read_fuctional_load(file_name)
    
    if not model:
        model.load = f2u.Load.LoadType(2, 'loading')
    #
    model.load.functional = _load
    
    return model.load
#
#
def read_load_combination(file_name, model):
    """
    """
    _level_zero, _level_one = genie_load.read_load_combinations(file_name, model, True) 
    #
    #
    if _level_zero:
        for _name_analysis, _combination in _level_zero.items():
            # update level one
            try:
                _comb = model.analysis.case[_name_analysis].load_combination[1]
                for _name, _load in _combination.items():
                    try:
                        dummy = _comb[_name]
                        dummy.functional_load.extend(_load.functional_load)
                        #print('here')
                    except KeyError:
                        _comb[_name] = _load
                        #print('here')
            # start level one
            except KeyError:
                model.analysis.case[_name_analysis].load_combination[1] = _combination
    #
    if _level_one:
        for _name_analysis, _combination in _level_one.items():
            model.analysis.case[_name_analysis].load_combination[1].update(_combination)
    #
    #print('--->')
    return model
#