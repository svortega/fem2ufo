#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import sys
import os


# package imports
import fem2ufo.f2u_model.control as f2uModel
import fem2ufo.process.control as process
import fem2ufo.preprocess.database.operations.read_csv as read_csv

#
def get_column(data, column, row_start, row_end):
    """
    """
    #_columns = len(data[0])
    #_rows = len(data)
    _column = process.spreadsheet.col_to_num(column) - 1
    ss = process.spreadsheet.SpreadSheet()
    
    for i in range(row_start, row_end):
        for j in range(_columns):
            data_out = data[i][j]
    
    return data_out
#
#
# weight
class WEIGHT_database:
    """
    Class reads the following database data and place data in fem2ufo database concept:
       - weight
       - time_history
    """
    
    def __init__(self):
        """
        """
        self.units_in = [None, None, None, None, None, None]
        self.data_file = None
        self.data = {}
        self.item_code = {'discipline':{}}
        self.component = None
        self.weight_items = {'mass': {'permanent': {},
                                      'variable': {}}}
        
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """

        try:
            path = os.path.normcase(path)
            os.chdir(path)

        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()

        retval = os.getcwd()
        print('')
        print('--- Current working directory :')
        print('--- {:}'.format(retval))
    #
    def units(self, **kwargs):
        """
        Provide units of the SESAM input files (mandatory)
        Available units: length, mass, time, temperature, force, pressure/stress  
        
        **Parameters**:
           :length: [mandatory]  
           :force: [mandatory]  
           :temperature :  
           :gravity: [default : 9.81ms^2]  
        """

        for key, value in kwargs.items():
            _unit = process.units.find_unit_case(key)
            self.units_in = process.units.units_module(_unit, value,
                                                       self.units_in)

    #
    def database(self, input_file, file_format):
        """
        """
        
        self.data_file = process.common.check_input_file(input_file, 
                                                         file_format)
    
        if not self.data_file:
            print('    ** I/O error : Database file {:} not found'
                  .format(input_file))
            sys.exit()
        
        #print('ok')
    #
    #
    def set_database_row(self, start, end):
        """
        """
        self.data = {'row': {'start': start, 
                             'end': end}}
        #print('ok')
    #
    def set_database_column(self, start, end):
        """
        """
        self.data = {'column' : {'start': start, 
                                 'end': end}}
    #    
    #
    #
    def read_rows(self, **kwargs):
        """
        """
        pass
    #
    def read_columns(self, **kwargs):
        """
        """
        if 'mass' in kwargs:
            _type = kwargs['mass']
            del kwargs['mass']
            
            self.weight_items['mass'][_type] =  kwargs
            
            #print('mass')
            #for key, value in kwargs.items():
            #    key        
        else:
            self.weight_items.update(kwargs)

    #
    # Weigh section
    #
    def weight_database_input(self, component, file,
                              file_format=False):
        """
        """
        pass
    #
    #
    def set_weight_component(self, component):
        """
        """
        self.component = component
    #
    #
    def assign_weight_item_code(self, discipline=None, 
                                area=None, module=None,
                                code=None):
        """
        
        **Parameters**:
        discipline
        area
        module
        
        code
        """
        
        self.item_code
    #
    def assign_discipline_code(self, discipline, code):
        """
        
        **Parameters**:
        discipline
        area
        module
        
        code
        """
        
        self.item_code['discipline'] = {code : discipline}
    #
    # output
    #    
    def get_database(self):
        """
        """
        wt_data = read_csv.read_database(self.data_file)
        
        try:
            self.weight_database
        
        except AttributeError:
            _name = self.data_file.split('.')
            self.weight_database = f2uModel.Weight.Database(_name[0], 1)
        
        #_row = len(wt_data)
        #_column = len(wt_data[0])
        
        row_start = self.data['row']['start']-1
        row_end = self.data['row']['end']-1
        
        for item, column in self.weight_items.items():
            item
        
            data_out = get_column(wt_data, column,
                                  row_start, row_end)
        
        print('mass')
    #
#
#

    