#
# Copyright (c) 2009-2019 fem2ufo
# 

# Python stdlib imports
import sys
import math
import os
from collections import defaultdict

# package imports
import fem2ufo.f2u_model.control as f2uModel
import fem2ufo.process.control as process
import fem2ufo.master.operation.signal_stats as signal_stats
from fem2ufo.preprocess.database.model.tubular_data import TubularData
#
#
#
def get_time_history_items(kwargs):
    """
    """
    _keys = {"acceleration": r"\b(acc(eleration)?)\b",
             "velocity": r"\b(vel(ocity)?)\b",
             "displacement": r"\b(disp(lacement)?)\b",
             "time" : r"\b(t(ime)?)\b",
             "time_step" : r"\b(t(ime)?(\_)?step(s)?)\b",
             "first_row_to_read" : r"\b(first(\_)?(row|line)(\_)?(to)?(\_)?(read)?)\b",
             "last_row_to_read" : r"\b(last(\_)?(row|line)(\_)?(to)?(\_)?(read)?)\b"}
    #
    _colums = []
    _names = []
    _rows = [None, None]
    #
    for key, value in kwargs.items():
        _data = process.common.find_keyword(key, _keys)
        #
        if "acceleration" in _data:
            _colums.append(value)
            _names.append("acceleration")
        elif "velocity" in _data:
            _colums.append(value)
            _names.append("velocity")
        elif "displacement" in _data:
            _colums.append(value)
            _names.append("displacement")
        elif "time" in _data:
            _colums.append(value)
            _names.append("time")
        elif "time_step" in _data:
            print("time_step")

        elif "first_row_to_read" in _data:
            _rows[0] = value

        elif "last_row_to_read" in _data:
            _rows[1] = value
    #
    return _colums, _names, _rows
#
def find_max(response, time):
    """
    """
    max_res = max(response)
    min_res = min(response)
    maximum = max(max_res, abs(min_res))
    index = response.index(max_res)
    if maximum == abs(min_res):
        index = response.index(min_res)
        return min_res, time[index]
    
    return max_res, time[index]
#
def response_plot(response, number, title, units, time=True):
    """
    """
    import matplotlib.pyplot as plt
    #from matplotlib.pylab import rcParams
    #
    #rcParams['figure.figsize'] = 10, 5
    figure = plt.figure(number)
    #
    acc_plot = figure.add_subplot(3,1,1)
    #_units = 'Acceleration [{:}]'.format('g')
    _units = 'Acceleration [{:}/{:}^2]'.format(units[0], units[2])
    acc_plot.set_ylabel(_units)
    acc_plot.grid(b=True, which='major', linestyle='-')
    acc_plot.grid(b=True, which='minor', linestyle='--')
    if time:
        acc_plot.set_title(title)
        #acc_plot.set_xlabel('Time [sec]')
        acc_plot.plot(response['time'], response['acceleration'])
        #
        max_acc, _time= find_max(response['acceleration'], response['time'])
        #acc_plot.set_title('Max : {: 6.5f} ({:} sec)'.format(max_acc, _time),
        #                   fontsize=8)
        acc_plot.text(.5,.9, 'Max : {: 6.5f} ({:} sec)'.format(max_acc, _time),
                      horizontalalignment='left', transform=acc_plot.transAxes,
                      fontsize=8) 
    else:
        acc_plot.set_title('Response Spectra')
        #acc_plot.set_xlabel('Period [sec]')
        acc_plot.plot(response['period'], response['acceleration'])        
    
    #
    vel_plot = figure.add_subplot(3,1,2)
    #vel_plot.set_title('Acceleration')
    _units = 'Velocity [{:}/{:}]'.format(units[0], units[2])
    vel_plot.set_ylabel(_units)
    vel_plot.grid(b=True, which='major', linestyle='-')
    vel_plot.grid(b=True, which='minor', linestyle='--')
    if time:
        #vel_plot.set_xlabel('Time [sec]')
        vel_plot.plot(response['time'], response['velocity'])
        #
        max_acc, _time= find_max(response['velocity'], response['time'])
        #vel_plot.set_title('Max : {: 6.5f} ({:} sec)'.format(max_acc, _time),
        #                   fontsize=8)
        vel_plot.text(.5,.9, 'Max : {: 6.5f} ({:} sec)'.format(max_acc, _time),
                          horizontalalignment='left', transform=vel_plot.transAxes,
                          fontsize=8)         
    else:
        #vel_plot.set_xlabel('Period [sec]')
        vel_plot.plot(response['period'], response['velocity'])        
    #
    #
    disp_plot = figure.add_subplot(3,1,3)
    #disp_plot.set_title('Acceleration')
    _units = 'Displacement [{:}]'.format(units[0])
    disp_plot.set_ylabel(_units)
    disp_plot.grid(b=True, which='major', linestyle='-')
    disp_plot.grid(b=True, which='minor', linestyle='--')
    if time:
        disp_plot.set_xlabel('Time [sec]')
        disp_plot.plot(response['time'], response['displacement'])
        #
        max_acc, _time= find_max(response['displacement'], response['time'])
        #disp_plot.set_title('Max : {: 6.5f} ({:} sec)'.format(max_acc, _time),
        #                    fontsize=8)
        disp_plot.text(.5,.9, 'Max : {: 6.5f} ({:} sec)'.format(max_acc, _time),
                          horizontalalignment='left', transform=disp_plot.transAxes,
                          fontsize=8)         
    else:
        disp_plot.set_xlabel('Period [sec]')
        disp_plot.plot(response['period'], response['displacement'])        
    #
    plt.show()
#
def maxmin_rth(respose):
    """
    """
    t, rd_resp, rv_resp, acc_resp = respose
    
    x_pos = max(acc_resp)
    x_neg = abs(min(acc_resp))
    acc_max = max(x_pos, x_neg)
    #acc = [acc_max, x_pos, x_neg]
    #
    rd_pos = max(rd_resp)
    rd_neg = abs(min(rd_resp))
    rd_max = max(rd_pos, rd_neg)
    #rd = [rd_max, rd_pos, rd_neg]
    #
    rv_pos = max(rv_resp)
    rv_neg = abs(min(rv_resp))
    rv_max = max(rv_pos, rv_neg)
    #rv = [rv_max, rv_pos, rv_neg]

    return rd_max, rv_max, acc_max
#
#
class TimeSeries(TubularData):
    """
    Class reads the following database data and place data in fem2ufo database concept:
       - time_history
    """
    def __init__(self, name=None, number=None):
        """
        """
        #
        # default SI units
        self.units_in = process.units.set_SI_units()
        self.units_out = process.units.set_SI_units()
        self.gravity = 9.80665 # m/s**2
        #
        self.data_file = None
        self.time_series = {}
        TubularData.__init__(self)
        #
        self.response_sdof = {}
        self.response_spectra = {}
        self.factor = 1.0
        #
        self.name = name
        self.number=number
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """
        try:
            path = os.path.normcase(path)
            os.chdir(path)
        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()
        #
        retval = os.getcwd()
        print('')
        print('--- Current working directory :')
        print('--- {:}'.format(retval))
    #
    def units(self, **kwargs):
        """
        Provide units of the SESAM input files (mandatory)
        Available units: length, mass, time, temperature, force, pressure/stress  
        
        **Parameters**:
           :length: [mandatory]
           :force: [mandatory]  
           :temperature :  
           :gravity: [default : 9.81 m/s**2]
        """
        _grav_flag = True
        for key, value in kwargs.items():
            _unit = process.units.find_unit_case(key)
            if 'acceleration' in _unit:
                _grav_flag = process.units.find_acceleration(value)
                if _grav_flag:
                    continue
                _unit = 'length'
            elif 'velocity' in _unit:
                _unit = 'length'
            #
            self.units_in = process.units.units_module(_unit, value,
                                                       self.units_in)
        #
        self.units_out = self.units_in
        factors, _grav = process.units.get_factors_and_gravity(self.units_in, self.units_out)
        if _grav_flag:
            self.gravity = _grav
        else:
            self.gravity = 1.0
    #
    def time_delay(self, time, time_step=None, value=0):
        """
        value : to fill delay time [0 default]
        """
        if not self.database:
            self._get_database()
        
        if not time_step:
            time_step = self.database['time'][1] - self.database['time'][0]
            steps = int(time // time_step) + 1
        else:
            steps = int(time // time_step)
        #
        for key, item in self.database.items():
            if 'time' in key:
                max_time = float(item[-1])
                add_time = [(time_step * i) for i in range(steps)]
                item = [x + time for x in item]
                self.database['time'] = add_time + item
                continue
            #           
            add_time = [value for i in range(steps)]
            self.database[key] = add_time + item
        #
        #print('-->')
    #
    def time_expand(self, time_history_name, time, time_step=None, value=None):
        """
        value = to fill added time [last value of the series as default]
        """
        #if not self.database:
        #    TimeSeries_database.read_database(self)
        try:
            _database = self.time_series[time_history_name]
        except KeyError:
            print('    ** error time history {:} not found'
                  .format(time_history_name))
            return
        
        if not time_step:
            time_step = _database['time'][1] - _database['time'][0]
            steps = int(time // time_step) + 1
        else:
            steps = int(time // time_step)
        #
        for key, item in _database.items():
            if 'time' in key:
                max_time = float(item[-1])
                add_time = [(max_time + time_step * i) for i in range(1, steps+1)]
                _database['time'] = item + add_time
                continue
            #
            _value = float(item[-1])
            if value:
                _value = value
            #
            add_time = [_value for i in range(steps)]
            _database[key] = item + add_time
    #
    def column_factor(self, factor, column_name=None):
        """
        if not column name, factor all data except time
        """
        if column_name:
            if 'time' in column_name:
                print('    *** error time colum data: {:} cannnot be factored'.format(column_name))
                ExitProg = input('Press Enter Key To Continue')
                sys.exit()            
            
            try:
                _column = self.database[column_name]
                self.database[column_name] = [x * factor for x in _column]
            except KeyError:
                print('    *** error colum name : {:} not found'.format(column_name))
                ExitProg = input('Press Enter Key To Continue')
                sys.exit()                
        else:
            for column_name, _column in self.database.items():
                if 'time' in column_name:
                    continue
                _column = [x * factor for x in _column]
    #
    # input data
    #
    def read_time_serie(self, name, **kwargs):
        """
        """
        #
        _colums, column_names, _rows = get_time_history_items(kwargs)
        #
        if _rows[0]:
            self.first_row_to_read = _rows[0]
        
        if _rows[1]:
            self.last_row_to_read = _rows[1]
        #
        self.get_columns(_colums, column_names)
        self.time_series[name] = self.database
        #
        self._calculate_missing_data(name, column_names)
    #
    #
    def set_time_serie(self, name, **kwargs):
        """
        """
        #
        _colums, column_names, _rows = get_time_history_items(kwargs)
        # get time series 
        self.time_series[name] = {}
        _series_lenght = []
        for x, _item in enumerate(column_names):
            try:
                self.time_series[name][_item] = self.database[_colums[x]]
                _series_lenght.append(len(self.database[_colums[x]]))
            except KeyError:
                print('    ** error database : {:} not found'.format(_colums[x]))
                continue
        # check if time vs serie lenght are different, 
        # if so, use the minimum lenght for both
        if len(set(_series_lenght)) > 1:
            _min_lenght = min(_series_lenght)
            for _key in self.time_series[name].keys():
                _value = self.time_series[name][_key]
                self.time_series[name][_key] =_value[:_min_lenght]
        #
        self._calculate_missing_data(name, column_names)
        #
        #print('stop')
    #
    #
    def update_database(self, new_name, time, new_data,
                        data_type=None):
        """
        """
        if self.time_series:
            self.time_series[new_name] = {}
            self.time_series[new_name]['time'] = time
            self.time_series[new_name][data_type] = new_data
        else:
            self.database[new_name] = new_data
    #
    # processing
    #
    def integrate(self, time_history_name, case_name,
                   new_name=None, factor=1.0):
        """
        """
        from scipy.integrate import cumtrapz
        import numpy as np
        #
        #_units_input = self.units_in
        #_units_output = self.units_out
        #factors, _grav = process.units.get_factors_and_gravity(_units_input,
        #                                                       _units_output)        
        #
        #_time = np.array(self.database['time'])
        #_serie = np.array(self.database[column_name])
        _time, _serie = self.get_serie(time_history_name,
                                       case_name)
        #
        _serie = np.array(_serie)
        #_serie *= self.gravity
        #
        # remove the mean from accel
        #_serie = _serie - np.mean(_serie)        
        #
        _new_serie = cumtrapz(_serie, _time, initial=0)  * factor 
        #
        #self.database[new_name] = _new_serie.tolist()
        #
        if new_name:
            self.update_database(new_name, 
                                 time=_time, 
                                 new_data=_new_serie.tolist(),
                                 data_type='velocity')
        else:
            return _new_serie.tolist()
    #
    def double_integral(self, acceleration_name, 
                         new_name=None, factor=1.0):
        """
        """
        from scipy.integrate import cumtrapz
        import numpy as np
        #
        #
        _time, _serie = self.get_serie(acceleration_name,
                                       case_name='acceleration')
        #
        _serie = np.array(_serie)
        #_serie *= self.gravity
        #
        # Method 1
        # remove the mean from accel
        #_serie -= np.mean(_serie)
        # first integral
        first_integral = cumtrapz(_serie, _time, initial=0)
        # remove the mean from first integral
        #first_integral -= np.mean(first_integral)
        # second integral
        _second_integral = cumtrapz(first_integral, _time, initial=0)
        # remove the mean from second integral
        #_second_integral -= np.mean(_second_integral)
        # find step zero drift
        _drift = _second_integral[0]
        _second_integral -= _drift
        # Apply factor 
        _second_integral *= factor
        # get back to python's list format
        #self.database[new_name] = _second_integral.tolist()
        #
        if new_name:
            self.update_database(new_name, 
                                 time=_time, 
                                 new_data=_second_integral.tolist(),
                                 data_type='displacement')
        else:
            return _second_integral.tolist()
    #
    def acc2disp(self, acceleration_name, 
                  new_name=None, factor=1.0):
        """
        """
        import numpy as np
        from scipy import signal, fftpack
        #
        # get stats 
        dur, dt, dtmax, dtmin = self.get_statistics(column_name=acceleration_name)
        #
        _time, _serie = self.get_serie(acceleration_name,
                                       case_name='acceleration')       
        #
        _serie = np.array(_serie)
        #_serie *= self.gravity
        _serie = _serie.astype(complex)
        #       
        #
        # second method
        # 1. Remove the mean from the accel. data
        _mean = np.mean(_serie)
        _serie -= _mean
        # 2. Take the Fourier transform (FFT) of the accel. data.
        fft_data = fftpack.fft(_serie)
        #       
        # 3 . Convert the transformed accel. data to displacement data 
        #     by dividing each element by -omega^2, where omega is the
        #     frequency band       
        freqs = np.fft.fftfreq(_serie.size, d=dt)
        Dk = -(2 * np.pi * freqs)**2
        #
        #freqs = np.fft.fftfreq(_serie.size)
        #Dk = -(2 * np.pi * np.arange(fft_data.size))**2
        #
        _new = fft_data / Dk
        _new[0] = 0j
        #
        # Now take the inverse FFT to get back to the 
        # time-domain and scale your result.
        _velocity = np.fft.ifft(_new)
        # find step zero drift
        _drift = _velocity[0]
        _velocity -= _drift
        # Get real part and apply factor
        _velocity = _velocity.astype(complex).real
        _velocity *= factor
        #
        # get back to python's list format
        #self.database[new_name] = _velocity.tolist()
        #
        if new_name:
            self.update_database(new_name, 
                                 time=_time, 
                                 new_data=_velocity.tolist(),
                                 data_type='displacement')
        else:
            return _velocity.tolist()
        #
        #
    #
    # output
    #
    def get_serie(self, series_name, 
                  case_name=None,
                  time_name='time'):
        """
        """
        try:
            if case_name:
                _time = self.time_series[series_name][time_name]
                _serie = self.time_series[series_name][case_name]
            else:
                return self.time_series[series_name]
        except KeyError:
            try:
                _serie = self.database[series_name]
                _time = self.database[time_name]
            except KeyError:
                raise IOError('    *** error time serie name {:} not found'
                              .format(series_name))
            except TypeError:
                raise IOError('    *** error time serie name {:} not found'
                              .format(series_name))
        #
        _time = _time[:len(_serie)]
        #
        return _time, _serie
    #
    def get_statistics(self, series_name=None):
        """
        """
        if series_name:
            _time, _serie = self.get_serie(series_name,
                                           case_name='acceleration')
        else:
            try:
                _keys = list(self.database.keys())
                _keys.remove('time')
                _time = self.database['time']
                _serie = self.database[_keys[0]]
            except KeyError:
                print('    *** warning time serie not found')
                return
        #
        dur, dt, dtmax, dtmin = signal_stats.check_th_data(_time, _serie)
        #
        return dur, dt, dtmax, dtmin
    #
    # plotting 
    #
    def plot(self, series_name, case):
        """
        """
        import matplotlib.pyplot as plt
        #
        for _column_name in series_name:
            _time, _serie = self.get_serie(_column_name, 
                                           case_name=case)
            if not _time:
                continue
            #
            #plt.plot(_time, _serie, label=_name)
            plt.plot(_time[:len(_serie)], _serie, label=_column_name)
        #
        plt.xlabel('time [sec]')
        
        if 'velocity' in case:
            _units = ' [{:}/{:}]'.format(self.units_in[0], self.units_in[2])
        elif 'displacement' in case:
            _units = ' [{:}]'.format(self.units_in[0])
        elif 'acceleration' in case:
            _units = ' [g]'
        
        _ylabel = case + _units
        plt.ylabel(_ylabel)
        plt.grid(b=True, which='major', linestyle='--')#, color='b'')
        plt.legend()
        plt.show()
    #
    def plot_acceleration(self, *series_name):
        """
        """
        self.plot(series_name, case='acceleration')
    #
    def plot_velocity(self, *series_name):
        """
        """
        self.plot(series_name, case='velocity')
    #
    def plot_displacement(self, *series_name):
        """
        """
        self.plot(series_name, case='displacement')    
    #
    def plot_time_serie(self, *series_name):
        """
        """
        #
        for x, _column_name in enumerate(series_name):
            _serie = self.time_series[_column_name]
            response_plot(_serie, title='Ground Motion', 
                          units=self.units_in, number=x+1)
    #
    def plot_spectral_density(self, series_name):
        """
        """
        import numpy as np
        from scipy import signal, fftpack
        #
        _time, _serie = self.get_serie(series_name,
                                       case_name='acceleration')
        #
        # get stats 
        dur, dt, dtmax, dtmin = self.get_statistics(series_name=series_name)
        #
        import matplotlib.pyplot as plt
        #plt.plot(fft_data, freqs, label=series_name)
        plt.figure()
        plt.xlabel('Frequency [Hz]')
        plt.ylabel('Spectral Density')
        plt.grid(b=True, which='major', color='k', linestyle='-')
        plt.grid(b=True, which='minor', color='r', linestyle='--')
        #plt.legend()
        #
        # method 1
        #fft_data = fftpack.fft(_serie)
        #ss = np.abs(fft_data)**2
        #freqs = np.fft.fftfreq(_serie.size, dtmin)
        #idx = np.argsort(freqs)        
        #plt.loglog(freqs, ss)
        #
        #
        Nd = len(_serie)
        fs = 1/dtmin
        #f, Pxx_den = signal.welch(_serie, fs, nperseg=Nd, scaling='density')
        #Psm = signal.medfilt(Pxx_den, 11)
        #
        #plt.loglog(f, Pxx_den*100, color='g')
        #
        T_delta = dtmin
        #f_delta = 1.0/(Nd*T_delta)
        freq = np.linspace(0.0, 1.0/(2.0*T_delta), Nd//2)
        #
        # Spectral density function
        SS = 1.0/Nd * np.abs(fftpack.fft(_serie))**2
        SSsm = signal.medfilt(SS[0:Nd//2], 11)
        #plt.loglog(freq, SSsm)
        plt.loglog(freq, SS[0:Nd//2])
        #
        #       
        plt.show()
        #print('-->')
    #
    def plot_response_sdof(self, series_name, period, 
                           damping=0.05, factor=1.0):
        """
        """
        #
        #try:
        #    self.response_sdof[series_name]
        #except KeyError:
        self.response_sdof[series_name] = self._response_sof(series_name,
                                                             period, damping,
                                                             factor)
        #
        #
        response_plot(self.response_sdof[series_name], 
                      title='SDOF Ground Motion Response Time History',
                      units=self.units_in, number=1)
        #
        
    #
    def plot_response_spectra(self, series_name, 
                              Tmax=None, T0=0, damping=0.05, 
                              dt=0.05, factor=1.0, print_file=False):
        """
        ----------  
        Optional :
        period : 
        T0     : start time (zero default)
        dt     : time step
        print_file : dump data in csv file (False default)
        """
        #
        file_name = None
        if print_file:
            file_name = series_name
        #
        self.response_spectra[series_name] = self._response_spectra(series_name,
                                                                    Tend=Tmax, T0=T0, 
                                                                    damping=damping, 
                                                                    dt=dt, factor=factor,
                                                                    file_name=file_name)
        #
        response_plot(self.response_spectra[series_name], 
                      title='Response Spectra', 
                      units=self.units_in,
                      number=2,  time=False)
    #
    #
    # writing out data
    #
    def _write_csv_by_series_name(self, file_name, time_history_name):
        """
        """
        from csv import writer
        #
        # add csv extension to file
        if not '.csv' in file_name:
            file_name = file_name + '.csv'
        # get data
        _data = [['time', '{:}'.format(self.units_in[2])], 
                 ['acceleration', '{:}'.format('g')], 
                 ['velocity', '{:}/{:}'.format(self.units_in[0], self.units_in[2])], 
                 ['displacement', '{:}'.format(self.units_in[0])]]
        #
        _serie = self.time_series[time_history_name]
        #
        _data[0].extend(_serie['time'])
        _data[1].extend(_serie['acceleration'])
        _data[2].extend(_serie['velocity'])
        _data[3].extend(_serie['displacement'])
        #
        # transpose data 
        _data = zip(*_data)
        # write cvs file
        with open(file_name, "w", newline='') as csv_file:
            _writer = writer(csv_file, delimiter=',')
            _writer.writerows(_data)
    
    #
    def write_csv(self, file_name, time_history_name=None,
                  column_names=None):
        """
        Write data to a CSV file path
        """
        # get data
        if time_history_name:
            self._write_csv_by_series_name(file_name, time_history_name)
        #
        if column_names:
            self.write_to_csv(file_name, column_names)
    #
    #
    # calcs
    #
    def _response_sof(self, serie_name,
                     period, damping, factor=1.0,
                     ground_motion=True):
        """
        """
        #
        new_response = defaultdict(list)
        if ground_motion:
            _time, _acceleration = self.get_serie(serie_name,
                                                  case_name='acceleration')
            _accel_factored = [_item * factor for _item in _acceleration]
            _accel_grav = [_item * self.gravity for _item in _accel_factored]
            _response = process.solver.ode_base_input(time=_time, 
                                                      acceleration=_accel_grav, 
                                                      period=period, 
                                                      gamma = damping)
        else:
            _time, _force = self.get_serie(serie_name,
                                           case_name='force')
            _force_factored = [_item * factor for _item in _force]
            _response = process.solver.ode_arbitrary_force(time=_time, 
                                                           force=_force_factored,
                                                           period=period, 
                                                           gamma = damping)
        # update 
        new_response['time'] = _response[0]
        new_response['displacement'] = _response[1]
        new_response['velocity'] = _response[2]
        #
        _accel = [_acc / self.gravity for _acc in _response[3]]
        new_response['acceleration'] = _accel
        
        return new_response
    #
    def _response_spectra(self, acceleration_name,
                          Tend=None, T0=0, damping=0.05, 
                          dt=0.05, factor=1.0, 
                          file_name=None):
        """
        """
        #
        _time, _acceleration = self.get_serie(acceleration_name,
                                              case_name='acceleration')
        #
        _accel_factored = [_acc * factor for _acc in _acceleration]
        _accel_grav = [_acc * self.gravity for _acc in _accel_factored]        
        #
        # calculate response
        if not Tend:
            dur, _dt, _dtmax, _dtmin = self.get_statistics(series_name=acceleration_name)
            Tend = math.ceil(dur)
        #
        #
        new_spectra = defaultdict(list)
        new_spectra['period'].append(0)
        new_spectra['displacement'].append(0)
        new_spectra['velocity'].append(0)
        new_spectra['acceleration'].append(0)            
        #
        _data = []
        _data.append([['time', '{:}'.format(self.units_in[2])],
                      ['time', '{:}'.format(self.units_in[2])],
                      ['time', '{:}'.format(self.units_in[2])]])
        _data[-1][0].extend(_time)
        _data[-1][1].extend(_time)
        _data[-1][2].extend(_time)
        #
        T = 0
        while Tend > T:
            T += dt
            # response = [time, displacement, velocity, acceleration]
            _response = process.solver.ode_base_input(time=_time, 
                                                      acceleration=_accel_grav, 
                                                      period=T, 
                                                      gamma = damping)
            #
            _data.append([['period = {:1.3E} sec'.format(T), 
                           'acceleration {:}'.format('g')], 
                          ['period = {:1.3E} sec'.format(T), 
                           'velocity {:}/{:}'.format(self.units_in[0], self.units_in[2])], 
                          ['period = {:1.3E} sec'.format(T), 
                           'displacement {:}'.format(self.units_in[0])]])
            #
            _data[-1][0].extend(_response[3])
            _data[-1][1].extend(_response[2])
            _data[-1][2].extend(_response[1])
            #
            _rd, _rv, _acc = maxmin_rth(_response)
            new_spectra['period'].append(T)
            new_spectra['displacement'].append(_rd)
            new_spectra['velocity'].append(_rv)
            new_spectra['acceleration'].append(_acc/self.gravity)
        #
        if file_name:
            from csv import writer
            # transpose data
            _data = list(zip(*_data))
            #_data = list(map(list, zip(*_data)))
            _acceleration = zip(*_data[0])
            _velocity = zip(*_data[1])
            _displacement = zip(*_data[2])
            # TODO: create new module
            # write cvs file
            _new_file = file_name + '_acceleration.csv'
            with open(_new_file, "w", newline='') as csv_file:
                _writer = writer(csv_file, delimiter=',')
                _writer.writerows(_acceleration)
            #
            _new_file = file_name + '_velocity.csv'
            with open(_new_file, "w", newline='') as csv_file:
                _writer = writer(csv_file, delimiter=',')
                _writer.writerows(_velocity)
            #
            _new_file = file_name + '_displacement.csv'
            with open(_new_file, "w", newline='') as csv_file:
                _writer = writer(csv_file, delimiter=',')
                _writer.writerows(_displacement)
            #
            #
            _data_heading = [['period [sec]', 'displacement [{:}]'.format(self.units_in[0]),
                             'velocity [{:}/{:}]'.format(self.units_in[0], self.units_in[2]),
                             'acceleration [{:}/{:}^2]'.format(self.units_in[0], self.units_in[2])]]
            _data_response = [new_spectra['period'], new_spectra['displacement'],
                              new_spectra['velocity'], new_spectra['acceleration']]
            _data_response = list(zip(*_data_response))
            _new_file = file_name + '_response_spectra.csv'
            with open(_new_file, "w", newline='') as csv_file:
                _writer = writer(csv_file, delimiter=',')
                #_writer.writerow(new_spectra.keys())
                _writer.writerows(_data_heading)
                _writer.writerows(_data_response)
        #
        return new_spectra
    #
    def _calculate_missing_data(self, name, column_names):
        """
        """
        if 'acceleration' in column_names:
            self.time_series[name]['acceleration'] = [_item * self.gravity 
                                                      for _item in self.time_series[name]['acceleration']]
        
        if not 'velocity' in column_names:
            self.time_series[name]['velocity'] = self.integrate(time_history_name=name,
                                                                case_name='acceleration')
            #
            if not 'displacement' in column_names:
                self.time_series[name]['displacement'] = self.double_integral(acceleration_name=name)
        else:
            if not 'displacement' in column_names:
                self.time_series[name]['displacement'] = self.integrate(time_history_name=name,
                                                                        case_name='velocity')
#

    