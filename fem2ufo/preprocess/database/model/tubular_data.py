#
# Copyright (c) 2009-2019 fem2ufo
# 

# Python stdlib imports
import sys
import os


# package imports
import fem2ufo.process.control as process


#
#
class TubularData:
    """Class to load data from text file"""
    
    def __init__(self):
        """
        """
        self.read_data = {'row': {'start': 0, 
                                  'end': None},
                          'column' : {'number': [], 
                                      'name': []}
                          }
        #
        self.database = None
        self.file_format = None
        self.delimeter = None
        self.sheet=None
    #
    def csv_file(self, file, file_format='csv',
                 delimeter=','):
        """
        """
        self.delimeter = delimeter
        self.file_format = file_format
        self.data_file = process.common.check_input_file(file, 
                                                         file_format)
    
        if not self.data_file:
            print('    ** I/O error : Database file {:} not found'
                      .format(file))
            sys.exit()
        
        self.data_file
    #
    def text_file(self, file, file_format,
                  delimeter=None):
        """
        """
        self.delimeter = delimeter
        self.file_format = file_format
        self.data_file = process.common.check_input_file(file, 
                                                         file_format)

        if not self.data_file:
            print('    ** I/O error : Database file {:} not found'
                      .format(file))
            sys.exit()
        
        self.data_file
    #
    def excel_file(self, file, sheet, file_format='xlsx'):
        """
        """
        self.file_format = file_format
        self.sheet = sheet
        self.data_file = process.common.check_input_file(file, 
                                                         file_format)
    
        if not self.data_file:
            print('    ** I/O error : Database file {:} not found'
                      .format(file))
            sys.exit()
        
        self.data_file
        
    #
    @property
    def first_row_to_read(self):
        """
        """
        return self.read_data['row']['start']
    #
    @first_row_to_read.setter
    def first_row_to_read(self, row_number):
        """
        The number of lines to skip at the beginning of the file.
        """
        self.read_data['row']['start'] = row_number
    #
    @property
    def last_row_to_read(self):
        """
        """
        return self.read_data['row']['end']
    #
    @last_row_to_read.setter
    def last_row_to_read(self, row_number):
        """
        The number of lines to skip at the end of the file.
        """
        self.read_data['row']['end'] = row_number
    #
    def maximun_rows(self, number_rows):
        """
        The maximum number of rows to read. 
        If given, the value must be at least 1. Default is to read the entire file.
        """
        self.read_data['row']['start'] = number_rows
    #
    def get_columns(self, colums, names):
        """
        Which columns to read, with 0 being the first
        If names is True, the field names are read from the first valid
        line after the first skip_header lines. 
        If names is a sequence or a single-string of comma-separated names, 
        the names will be used to define the field names in a structured dtype.
        
        colums : list
        names : list
        """
        _new_columns = []
        for _column in colums:
            try:
                _new_columns.append(int(_column))
            except ValueError:
                _item = process.spreadsheet.letter2num(_column)
                _new_columns.append(_item)
        #
        self.read_data['column']['number'] = _new_columns
        if names:
            self.read_data['column']['name'] = names
        #
        # read data
        self._read_database()
    #
    def _read_database(self):
        """
        """
        if 'csv' in self.file_format:
            self.database = process.common.read_cvs(self.data_file, self.read_data)
        elif 'xlsx' in self.file_format:
            self.database = process.common.read_xlsx(file_name=self.data_file, 
                                                     sheet=self.sheet,
                                                     read_data=self.read_data)
        else:
            self.database = process.common.read_text(self.data_file, 
                                                     self.read_data,
                                                     self.delimeter)
            
            #print('    *** error file format not implemented {:}\n'.format(self.file_format))
            #ExitProg = input('    Press Enter Key To Continue')
            #sys.exit()
        #
        #print('-->')
    #
    def _get_database(self):
        """
        """
        if 'csv' in self.file_format:
            return process.common.read_cvs(self.data_file, self.read_data)
        elif 'xlsx' in self.file_format:
            return process.common.read_xlsx(file_name=self.data_file, 
                                            sheet=self.sheet,
                                            read_data=self.read_data)
        else:
            raise IOError('    *** error file format not implemented {:}\n'
                          .format(self.file_format))
        #
        #print('-->')    
    #
    def write_to_csv(self, file_name, column_names):
        """
        Write data to a CSV file path
        """
        import csv
        from itertools import zip_longest
        # add csv extension to file
        if not '.csv' in file_name:
            file_name = file_name + '.csv'
        # get data
        _data = []
        _header = []
        for _column_name in column_names:
            try:
                _data.append(self.database[_column_name])
                _header.append(_column_name)
            except KeyError:
                print('    *** warning column name {:} not found'
                      .format(_column_name))
        #
        # transpose data 
        _data = zip_longest(*_data, fillvalue="")
        # write cvs file
        with open(file_name, "w", newline='') as csv_file:
            _writer = csv.writer(csv_file) # , delimiter=','
            _writer.writerow(_header)
            _writer.writerows(_data)
        #
        csv_file.close()
        # print('-->')