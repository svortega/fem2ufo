#
# Copyright (c) 2013 fem2ufo
#
#
#
#
from openpyxl import load_workbook
import htmlout as HTML5
#
#
# ====================================================================
#                             GENERAL
# ====================================================================
#
#
class Assembly(object):
    """
    """
    __slots__ = ('number', 'name', 'areas', 'type', 'disciplines',
                 'change', 'dummy')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name


#
class Discipline(object):
    """
    """
    __slots__ = ('number', 'name', 'code', 'type', 'assumption',
                 'item')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        self.item = []


#
class Area(object):
    """
    This describe all project main areas/modules and
    sub-areas, including their location, size, code and name
    """
    __slots__ = ('number', 'name', 'code', 'discipline', 'item',
                 'north', 'east', 'vertical')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        self.item = []


#
class Item(object):
    """
    unit_weight   : Unit weight for the piece, e.g. 42, 3 kg for unit length(1m) 
                    of an HE200A-beam.
    number_off    : Number of identical pieces. Only integer numbers shall be used.
    size          : The size of each piece, e.g. 2,350 m for the length of an 
                    HE200A-beam.
    
    dry_weight    : The dry weight of each piece in kg, that is the product of 
                    Number off x Size x Unit weight, 
                    e.g. 1 x 2,350 x 42,3 = 99 kg for one HE200A with a length of
                    2,350 m.
    content_weight: The weight of the content in kg in a piping line, vessel, 
                    tank etc.
    
    coordinates   : The co-ordinates in the platform north direction, the platform
                    east direction and elevation.
    """
    __slots__ = ('number', 'name', 'code', 'tag',
                 'unit', 'size',
                 'dry', 'content',
                 'status', 'weight',)
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        self.weight = {}
        #


#
class Weight(object):
    """
    """
    __slots__ = ('number', 'name', 'mass', 'tolerance',
                 'coordinates', 'noItem')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        #


#
class Coordinates(object):
    """
    """
    __slots__ = ('north', 'east', 'vertical')
    #
    def __init__(self, North, East, Vertical):
        self.north = North
        self.east = East
        self.vertical = Vertical
        #
        #


#
#
def readExcel(_xlFile, _sheetName):
    #
    # wb = Workbook()
    # dest_filename = r'empty_book.xlsx'
    # ws = wb.worksheets[0]
    # ws.title = "range names"

    # for col_idx in range(1, 40):
    #    col = get_column_letter(col_idx)
    #    for row in range(1, 60):
    #        ws.cell('%s%s'%(col, row)).value = '%s%s' % (col, row)

    # ws = wb.create_sheet()
    # ws.title = 'Pi'
    # ws.cell('F5').value = 3.14
    # wb.save(filename = dest_filename)

    wb = load_workbook(filename=_xlFile, use_iterators=True)
    ws = wb.get_sheet_by_name(name=_sheetName)  # ws is now an IterableWorksheet

    _data = []
    # 'B4:AI19325'
    for row in ws.iter_rows('B4:AH500'):  # it brings a new method: iter_rows()
        _item = []
        for cell in row:
            _item.append(cell.value)
            # print(cell.value)

        _data.append(_item)
    #
    return _data
    #

#
xlFile = 'WeightByItemEAPDQ_Q4_2011.xlsx'
sheetName = 'WCR'
_data = readExcel(xlFile, sheetName)

_dis = list(set(_row[7] for _row in _data))
_module = list(set(_row[8] for _row in _data))  # _module = list(set(_module))

# print('ok')
#
#
_discipline = {}
for x in range(len(_dis)):
    _name = _dis[x]
    _discipline[_name] = Discipline(x, _name)
#
#
_areas = {}
for x in range(len(_module)):
    _name = _module[x]
    _areas[_name] = Area(x, _name)
    _areas[_name].discipline = _discipline
#
_items = {}
x = 0
for _row in _data:
    # set up item
    _items[x] = Item(x, _row[2])
    # set dry weight
    _items[x].weight['dry'] = Weight(0, 'dry')
    _items[x].weight['dry'].mass = _row[4]
    _items[x].weight['dry'].coordinates = Coordinates(_row[13], _row[14], _row[15])
    # set content weight
    _items[x].weight['content'] = Weight(0, 'content')
    _items[x].weight['content'].mass = _row[5]
    _items[x].weight['content'].coordinates = Coordinates(_row[13], _row[14], _row[15])
    # 
    _nameDisc = _row[7]
    _nameArea = _row[8]
    _areas[_nameArea].discipline[_nameDisc].item.append(_items[x])
    x += 1
#
_assembly = {}
_assembly['topside'] = Assembly(0, 'topside')
_assembly['topside'].disciplines = _discipline
_assembly['topside'].areas = _areas
#
#
#
# Print File
#
_file = 'Test.html'
_file = 'Test_2.xml'
XMLmod = HTML5.printWCR(_assembly)
#
#
printOut = open(_file, 'w+')
printOut.write("".join(XMLmod))
printOut.close()
print('--- Model File   : {:}'.format(_file))
#
print('ok')
#
