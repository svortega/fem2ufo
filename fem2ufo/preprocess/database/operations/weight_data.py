#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import os
import sys


# package imports
import fem2ufo.process.control as process
import fem2ufo.preprocess.csv_format.read_wr as readWR

#
#
#
class WEIGHT_data:
    """Class reads Weight database and place data in fem2ufo model concept"""
    
    def __init__(self):
        """
        """
        self.factors = [0, 0, 0, 0, 0, 0]
        self.units_in = ["", "", "", "", "", ""]
        self.units_out = ["", "", "", "", "", ""]
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """

        try:
            path = os.path.normcase(path)
            os.chdir(path)

        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()

        retval = os.getcwd()
        print('')
        print('--- Working directory :')
        print('--- {:}'.format(retval))

    #
    def units(self, **kwargs):
        """
        Provide units of the weight database (mandatory)
        Available units: 
            length, 
            mass, 
            time, 
            temperature, 
            force, 
            pressure/stress
        
        **Parameters**:
           :length: [mandatory]  
           :force: [mandatory]  
           :mass :  [mandatory]
           :gravity: [default : 9.81ms^2]  
        """

        for key, value in kwargs.items():
            _unit = process.find_unit_case(key)
            self.units_in = process.units_module(_unit, value,
                                                 self.units_in)

    #
    def read_database(self, file_name, file_format='csv'):
        """
        Provide weight database
        **Parameters**:
          :file_name
        """
        readWR.read_weight_database(file_name)
    #
    def area(self, name, description):
        """
        """
        pass
    #
    def discipline(self, name, description):
        """
        """
        pass
    #
    def item(self, tag, description):
        """
        """
        pass
    #
    def coordinates(self, x, y, z):
        """
        """
        pass
    #
    def mass_dry(self):
        """
        """
        pass
    #
    def mass_content(self):
        """
        """
        pass
    #
    def get_database(self):
        """
        """
        pass