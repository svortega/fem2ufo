# 
# Copyright (c) 2009-2016 fem2ufo
# 
#
import csv
import datetime

#import html as HTML5
from collections import defaultdict
#
#
#
#
def read_database(file_name):
    #
    data = defaultdict(list)
    #
    #_file = 'WeightByItemEAPDQ_Q4_2011_2.csv'
    today = datetime.date.today()
    #
    wt_data = []
    #
    with open(file_name, newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            wt_data.append(row)
            #for (i, v) in enumerate(row):
                #data[i].append(v)
                #
                # reader = csv.DictReader(f) # read rows into a dictionary format
                # for row in reader:
                #    for (k,v) in row.items():
                #        col[k].append(v)
                #
                #
                # reader = csv.reader(f, dialect='excel')
                #    for _index, _item in enumerate(row):
                #        if _item == 'Discipline':
                #            print(_item, _index)
                #    print(row)
    #
    return wt_data
#
#
#
def read_wt():
    #
    # define groups
    discipline = {}
    areas = {}
    item = {}
    _item_no = 0
    for _index, _items in data.items():
    
        for _item in _items:
    
            # if _item == 'Discipline':
            # print(_item, _index)
            # _dis = list(set(_items[1:]))
            # for _no, _name in enumerate(_dis):
            #    discipline[_name] = Discipline(_no + 1, _name)
    
            if _item == 'Module':
                # print(_item, _index)
                _module = list(set(_items[1:]))
                for _no, _name in enumerate(_module):
                    areas[_name] = Area(_no + 1, _name)
                    # areas[_name].discipline = discipline
    
            elif _item == 'Tag':
                for _no, _tag in enumerate(_items[1:]):
    
                    if not _tag:
                        print('?????')
    
                    try:
                        item[_tag].units[_no + 1] = Unit(_no + 1, "N/A")
    
                    except KeyError:
                        _item_no += 1
                        # set up item
                        item[_tag] = Item(_item_no, _tag)
                        # item[_tag].tag = _tag
                        item[_tag].units[_no + 1] = Unit(_no + 1, "N/A")
                        # print('ok') Item Description
    #
    #
    # populate items
    for no, _case in item.items():
    
        for _no, _item in _case.units.items():
            # item description
            _item.name = data[2][_no]
    
            # item coordinates
            _item.coordinates = [data[13][_no],
                                 data[14][_no],
                                 data[15][_no]]
            # item quantity
            _item.number_off = data[3][_no]
    
            # item dimension
            _item.dimension = [1, 1, 1]
    
            # set dry weight
            _item.weight['dry'] = Weight(_no, 'dry')
            _item.weight['dry'].unit = data[4][_no]
            _item.weight['dry'].mass.append(float(data[17][_no]))
            _item.weight['dry'].mass.extend([float(data[18][_no]),
                                             float(data[19][_no]),
                                             float(data[20][_no])])
            # _item.weight['dry'].cog = Coordinates(data[18][_no],
            #                                      data[19][_no],
            #                                      data[20][_no])
            _item.weight['dry'].status = data[0][_no]
    
            # set content weight
            _item.weight['content'] = Weight(_no, 'content')
            _item.weight['content'].unit = data[5][_no]
            _item.weight['content'].mass.append(float(data[25][_no]))
            _item.weight['content'].mass.extend([float(data[26][_no]),
                                                 float(data[27][_no]),
                                                 float(data[28][_no])])
            # _item.weight['content'].cog = Coordinates(data[26][_no],
            #                                          data[27][_no],
            #                                          data[28][_no])
            #
            _item.weight['content'].status = data[0][_no]
            #
        _nameDisc = data[7][_no]
        _nameArea = data[8][_no]
        try:
            areas[_nameArea].discipline[_nameDisc].item.append(_case)
    
        except KeyError:
            areas[_nameArea].discipline[_nameDisc] = Discipline(_no + 1, _nameDisc)
            areas[_nameArea].discipline[_nameDisc].item.append(_case)
            # areas[_nameArea].discipline[_nameDisc].type = data[1][_no]
    
    #
    # Create assembly
    _assembly = {}
    _assembly['topside'] = Assembly(1, 'topside')
    # _assembly['topside'].disciplines = discipline
    _assembly['topside'].areas = areas
    _assembly['topside'].items = item
    #
    # Create WR database
    wr = WeightReport(1, 'test')
    wr.database = _file
    wr.date = today
    wr.assembly = _assembly

# 
# Print File
#XMLmod = HTML5.print_WCR(wr)
#
#_file = 'Test_2.xml'
#printOut = open(_file, 'w+')
#printOut.write("".join(XMLmod))
#printOut.close()
#print('--- Model File   : {:}'.format(_file))
#
#print('ok')
