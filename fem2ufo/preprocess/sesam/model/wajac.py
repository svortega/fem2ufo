# 
# Copyright (c) 2009-2016 fem2ufo
#

# Python stdlib imports
import math
import re
#import sys
from operator import itemgetter
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process

#
# --------------------------------------------------------------------
#  SESAM specifics
# --------------------------------------------------------------------
#
#
def read_metocean(model, _metocean_file, _format, 
                  analysis_name):
    """
    """
    #import fem2ufo.preprocess.sesam.wajac as wajac
    geometry = model.component
    #
    if not analysis_name:
        _name_file = _metocean_file.split(".")
        _name_file = _name_file[0].strip()
        analysis_name = _name_file    
    
    # read wajac file
    _meto, _case = read_wajac(_metocean_file, analysis_name, model)
    #
    #
    #
    # include cdcm from geometry file and find flooded memebrs
    if geometry.cdcm:
        i = len(_meto.cdcm)
        for _cd in geometry.cdcm.values():
            # FIXME: Check this work
            if not _cd.items:
                continue                
            #
            if _cd.coefficient:
                i += 1
                _meto.cdcm[i] = _cd
                _meto.cdcm[i].number = i
                _meto.cdcm[i].name = i
    #
    #
    _no_floo = len(_meto.flooded)
    for _concept in geometry.concepts.values():
        if _concept.properties:
            if _concept.properties.flooded:
                try:
                    _meto.flooded['_flooded_'].concepts.append(_concept.name)
                except KeyError:
                    _meto.flooded['_flooded_'] = f2u.Geometry.Groups('_flooded_', _no_floo)
                    _meto.flooded['_flooded_'].concepts.append(_concept.name)
                
                for _memb in _concept.element.keys():
                    _meto.flooded['_flooded_'].items.append(_memb)
    
                #
    #
    for key, _flood in _meto.flooded.items():
        if key == '_flooded_':
            continue
        # check if set exist from main model
        try:
            _group = geometry.sets[key]
        except KeyError:
            continue
        
        if _group.items:
            _flood.items = _group.items
        
        if _group.concepts:
            _flood.items = _group.concepts

    # 
    # check Buoyancy Diameter
    if _meto.buoyancy:
        for _floo in _meto.buoyancy.values():
            _barea = float(_floo.coefficient['nonflooded'])  # nonflooded buoyancy area
            _sarea = float(_floo.coefficient['steel'])  # steel cross section buoyancy area
            _item_new = []
            for _item in _floo.items:
                try:
                    _memb = geometry.elements[_item]

                    if _memb.section[0].shape == "PIPE":
                        _item_new.append(_item)
                    else:
                        print('   Warining Buoyancy Diameter --> section {:} is not tubular'
                              .format(_item))
                except KeyError:
                    continue
            #
            # calculating buoyancy diametre
            _diam_buoy = math.sqrt((4 * _barea / math.pi))
            _diam_int = math.sqrt((4 * _sarea / math.pi))
            # update items
            _floo.items = _item_new
            #_floo.coefficient[0][1] = _diam_buoy
            #_floo.coefficient[1][1] = _diam_int
            _floo.coefficient['nonflooded'] = _diam_buoy
            _floo.coefficient['steel'] = _diam_int                
        #
        # add buoyancy diamtre to geometry's sets
        _new_set_name = '_bdiam'
        process.geometry.add_new_groups(geometry.sets,
                                   _meto.buoyancy,
                                   _new_set_name)
    #
    # non hydro find set fe number (model set exist already)
    # TODO : sort the case when members are given
    # sets not longer used ?
    #if _meto.non_hydro:
    #    for _nonHydro in _meto.non_hydro.values():
    #        _tb_rem = []
    #        for _key, _item in _nonHydro.sets.items():
    #            name = [_key]
    #            _no = process.concept.find_item_fe_number_by_name(geometry.sets,
    #                                                              name)
    #            try:
    #                _nonHydro.sets[_key] = _no[0]
    #            except IndexError:
    #                _tb_rem.append(_key)
    #        #
    #        for _name in _tb_rem:
    #            del _nonHydro.sets[_name]
    #
    # add cdcm group members to model's sets
    _new_set_name = '_cdcm'
    process.geometry.add_new_groups(geometry.sets,
                                    _meto.cdcm,
                                    _new_set_name)
    #
    # add flooded member to model's sets
    if _meto.flooded:
        _new_set_name = '_flood'
        process.geometry.add_new_groups(geometry.sets,
                                        _meto.flooded,
                                        _new_set_name)
    #
    # add hydro diam to model's sets
    if _meto.hydro_diameter:
        _new_set_name = '_hdiam'
        process.geometry.add_new_groups(geometry.sets,
                                   _meto.hydro_diameter,
                                   _new_set_name)
    #
    # add marine growth to model's sets
    if _meto.marine_growth:
        _new_set_name = '_mgrowth'
        process.geometry.add_new_groups(geometry.sets,
                                   _meto.marine_growth,
                                   _new_set_name)
    #
    # if _meto.wind:
    #    _new_set_name = '_wind'
    #     process.add_group(geometry.sets,
    #                       _meto.wind,
    #                       _new_set_name)
    #
    # update metocean
    if model.metocean:
        print('????? now what??')
    else:
        model.set_metocean()
        model.metocean =  _meto
        #_metocean = model['metocean']
    #
    # update analysis
    try:
        model.analysis.case[analysis_name] = _case
    #
    except AttributeError:
        model.set_analysis(analysis_name)
        model.analysis.case[analysis_name] = _case
    #    
#
#
# --------------------------------------------------------------------
#
#
def wind_theory(line_in):
    """
    """
    #
    _key = {"general": r"0",
            "normal": r"1",
            "abs": r"abs",
            "extreme": r"2",
            "extreme_api21": r"3"}

    key_word = process.common.find_keyword(line_in, _key)

    return key_word
#
#
#
#
def wave_theory(line_in):
    """
    """
    #
    _key = {"airy_linear": r"1.1",
            "airy_extrapolated": r"1.2",
            "airy_extretched": r"1.3",
            "stokes_5": r"5.0",
            "stream": r"6.",
            "cnoidal": r"7.0",
            "newwave": r"8.0",
            #"buoyancy": r"9.0",
            "calm_sea": r"9.0|10.0",
            "irregular": r"11.0",
            "user_defined": r"12.0"}

    key_word = process.common.find_keyword(line_in, _key)

    return key_word
#
#
def read_wajac(file_name, analysis_name, model):
    """
    Module to read wajac input file
    """
    #
    print('')
    print('---------------- READ METOCEAN Module --------------')

    # read T fem file
    wajac_file = process.common.check_input_file(file_name, 
                                                 file_extension='inp')

    if not wajac_file :
        raise IOError('    ** I/O error : wajac file {:} not found'
                      .format(file_name))
        #sys.exit()
    #
    _group = { _set.name : key  for key, _set in model.component.sets.items()}
    #
    _metocean = {}
    _user_data = {}
    _wave = {}
    _elevation = {}
    _mgrowth = {}
    _current = {}
    _diameter = {}
    _wind = {}
    _wind_cd = {}
    _cdcm_temp = {}
    _cdcm = {}
    _buoy = {}
    _non_hydro = {}
    _flooded = {}
    _seastate = {}
    #
    _user_data['wajac'] = f2u.InputUserData()
    _user_data['wajac'].file_name = file_name
    _user_data['wajac'].program = 'wajac'
    #
    #
    _no_elev = 0
    _no_floo = 0
    _mudline = 0
    _nohydro_no = 0
    _load_no = 0
    _no_wind = 0
    _no_buoy = 0
    _no_cdcm = 0
    _constants = []
    #
    print('--- Reading WAJAC.INP file')
    print('    - First pass')
    #
    for line in open(wajac_file):
        # Jump empty lines
        if not line:
            continue
        # Jump commented lines
        if re.match(r"C\b", line):
            continue
            # print('--> ',line)
        # Segment line
        keyword = line.split()
        #    Flag word
        if line[0] != ' ':
            flag1 = keyword[0]
        #
        if flag1 == 'TITL':
            _user_data['wajac'].title = str(line[5:]).strip()
            # print(line)
        #
        elif flag1 == 'FMOD':
            _user_data['wajac'].user = keyword[1]
        #
        elif flag1 == 'CONS':
            if float(keyword[1]) == 1.0:
                _constants = keyword[2:]
                # print(line)
        #
        elif flag1 == 'LONO':
            _load_no = int(float(keyword[1])) - 1
        #
        elif flag1 == 'MUDP':
            _mudline = float(keyword[1])
        #
        elif flag1 == 'SEA':
            if not line[3:10].strip():
                isea = int(float(line[10:15]))
                _wave[isea] = f2u.Metocean.Wave(isea, isea)
                _theory = wave_theory(line[15:20].strip())
                _wave[isea].theory = _theory
                if 'stream' in _theory:
                    _order = line[15:20].split('.')
                    _order = int(_order[1])
                    if _order == 0:
                        _wave[isea].order = 8
                    else:
                        _wave[isea].order = _order
                
                _wave[isea].height = float(line[20:30])
                _wave[isea].period = float(line[30:40])
                _wave[isea].phase = float(line[40:50])
                _wave[isea].time = float(line[50:60])
                _wave[isea].step = float(line[60:70])
                _wave[isea].number_step = float(line[70:80])
                # var 1
            else:
                raise NotImplementedError('sea --> error !!!!')
        #
        elif flag1 == 'CRNT':
            if keyword[0] == 'CRNT':
                ctno = int(float(line[5:10]))
                _current[ctno] = f2u.Metocean.Current(ctno, ctno)

            _z = float(line[40:50])
            _v = float(line[50:60])
            _current[ctno].profile.append((_z, _v))
        #
        elif flag1 == 'MGRW':
            hmgrw = float(line[30:40])
            hrough = float(line[40:50])
            opt = int(float(line[50:60]))
            imem = int(float(line[60:65]))
            grwf = float(line[65:75])

            try:
                _mgrowth[imem].option = opt
                _mgrowth[imem].factor = grwf
                _mgrowth[imem].roughness = hrough
            except KeyError:
                _mgrowth[imem] = f2u.Metocean.MarineGrowth(imem, imem)
                _mgrowth[imem].option = opt
                _mgrowth[imem].factor = grwf
                _mgrowth[imem].roughness = hrough

            if float(line[10:20]) < 0:
                _mgrowth[imem].type = 'profile'
                _mgrowth[imem].profile.append((float(line[20:30]), hmgrw))
            else:
                _mgrowth[imem].type = 'specified'
                _mgrowth[imem].profile.append((float(line[10:20]), hmgrw))
                _mgrowth[imem].profile.append((float(line[20:30]), hmgrw))
        #
        #
        elif flag1 == 'SPEX':
            if not '-1.' in keyword[7:]:
                print('--> SPEX need to fix this')
            # Hydrodynamic Diameter property 
            else:
                # members
                if len(keyword) > 9:
                    m1 = int(float(line[10:20]))
                    m2 = int(float(line[20:30]))
                    inc = int(float(line[30:40]))
                    diam = float(line[50:60])

                    if m2 == 0:
                        m2 = m1
                        inc = 1

                    for x in range(m1, m2 + 1, inc):
                        try:
                            _diameter[diam].append(x)
                        except KeyError:
                            # _diam_no += 1
                            _diameter[diam] = [x]  # Parameters(_diam_no, diam)
                            # _diameter[diam].items.append(x)
                # groups
                else:
                    print('--> fix SPEX groups')
        #
        elif flag1 == 'SPEC':
            print('--> fix SPEC')
            print(line)
        #
        elif flag1 == 'FLOO':
            # elements
            if len(keyword) > 4:
                m1 = int(float(line[10:20]))
                m2 = int(float(line[20:30]))
                inc = int(float(line[30:40]))

                try:
                    _flooded['_flooded_']
                except KeyError:
                    _no_floo += 1
                    #_flooded[_no_floo] = f2u.Metocean.Parameters(_no_floo, _no_floo)
                    #_flooded[_no_floo].type = 'flooded_members'
                    #_flooded[_no_floo].flooded = True
                    _flooded['_flooded_'] = f2u.Geometry.Groups('_flooded_', _no_floo)

                if m2 == 0:
                    m2 = m1
                    inc = 1

                for x in range(m1, m2 + 1, inc):
                    _flooded['_flooded_'].items.append(x)
            # groups
            else:
                _setnam = str(line[10:40]).strip()
                _no_floo += 1
                #_flooded[_no_floo] = f2u.Metocean.Parameters(_no_floo, _no_floo)
                #_flooded[_no_floo].type = 'flooded_members'
                #_flooded[_no_floo].flooded = True
                #_flooded[_no_floo].sets[_setnam] = 0
                _flooded[_setnam] = f2u.Geometry.Groups(_setnam, _no_floo)
        #
        elif flag1 == 'ELIM':
            # elements
            if len(keyword) > 4:
                m1 = int(float(line[10:20]))
                m2 = int(float(line[20:30]))
                inc = int(float(line[30:40]))
                try:
                    _test = _non_hydro['elim']
                except KeyError:
                    #_nohydro_no += 1
                    _non_hydro['elim'] = f2u.Geometry.Groups('elim')
                    #f2u.Metocean.Parameters(_nohydro_no, _nohydro_no)
                    #_non_hydro[_nohydro_no].type = 'non_hydrodynamic'

                if m2 == 0:
                    m2 = m1
                    inc = 1

                for x in range(m1, m2 + 1, inc):
                    _non_hydro['elim'].items.append(x)

            # groups
            else:
                _setnam = str(line[10:40]).strip()
                try:
                    _gname = _group[_setnam]
                    _group = model.component.sets[_gname]
                except KeyError:
                    print('    *** warning non hydro group {:} not found'
                          .format(_setnam))
                    continue
                
                try:
                    _test = _non_hydro['elim']
                except KeyError:
                    _non_hydro['elim'] = f2u.Geometry.Groups('elim', _group.number)
                
                _non_hydro['elim'].items.extend(_group.items)
                #_nohydro_no += 1
                #_non_hydro[_nohydro_no] = f2u.Metocean.Parameters(_nohydro_no, _nohydro_no)
                #_non_hydro[_nohydro_no].type = 'non_hydrodynamic'
                #_non_hydro[_nohydro_no].sets[_setnam] = 0
                # print('groups', line)
        #
        elif flag1 == 'WINX':
            wid = int(float(line[5:10]))
            _wind[wid] = f2u.Metocean.Wind(wid, wid)
            #            
            _wind[wid].velocity = float(line[10:20])
            _wind[wid].direction = float(line[20:30])
            _wind[wid].gust_factor = float(line[30:40])
            _wind[wid].height = float(line[40:50])
            _wind[wid].power = float(line[50:60])
            _wind[wid].period_ratio = float(line[60:70])
            _wind[wid].formula = wind_theory(line[70:75].strip())
        #
        elif flag1 == 'WIND':
            wid = int(float(line[5:10]))
            _wind[wid] = f2u.Metocean.Wind(wid, wid)
            #            
            _wind[wid].velocity = float(line[10:20])
            _wind[wid].direction = float(line[20:30])
            _wind[wid].gust_factor = float(line[30:40])
            _wind[wid].height = float(line[40:50])
            _wind[wid].power = float(line[50:60])
            _wind[wid].period = float(line[60:65])
            _wind[wid].formula = wind_theory(line[65:70].strip())
        #
        elif flag1 == 'CDWN':
            # members
            try:
            #if len(keyword) > 7:
                m1 = int(float(line[10:20]))
                m2 = int(float(line[20:30]))
                inc = int(float(line[30:40]))
                value_x = float(line[50:55])
                value_z = float(line[60:65])
                _name = str(value_x) + '_' + str(value_z)

                try:
                    _test = _wind_cd[_name].type
                except KeyError:
                    _no_wind += 1
                    _wind_cd[_name] = f2u.Metocean.Parameters(_no_wind, _no_wind)
                    _wind_cd[_name].type = 'specified'
                    _wind_cd[_name].coefficient['cd'] = [0, value_x, value_z]
                    #_wind_cd[_name].coefficient.append(['cdy', value_x])
                    #_wind_cd[_name].coefficient.append(['cdz', value_z])

                if m2 == 0:
                    m2 = m1
                    inc = 1

                for x in range(m1, m2 + 1, inc):
                    _wind_cd[_name].items.append(x)

            # groups
            except ValueError:
            #else:
                _no_wind += 1
                print('--> fix CDWN groups')
        #
        elif flag1 == 'CDWR':
            _no_wind += 1
            _wind_cd[_name] = f2u.Metocean.Parameters(_no_wind, _no_wind)
            _wind_cd[_name].type = 'specified'             
            print('----> fix CDWR')
        #
        elif flag1 == 'BUOA':
            try:
            #if len(keyword) > 7:
                m1 = int(float(line[10:20]))
                m2 = int(float(line[20:30]))
                inc = int(float(line[30:40]))
                anfloo = float(line[50:60])
                afloo = float(line[60:70])

                if anfloo == -1 and afloo == -1:
                    continue

                _name = str(anfloo) + '_' + str(afloo)

                try:
                    _test = _buoy[_name]
                except KeyError:
                    _no_buoy += 1
                    _buoy[_name] = f2u.Metocean.Parameters(_no_buoy, _no_buoy)
                    _buoy[_name].type = 'buoyancy_area'
                    _buoy[_name].coefficient['nonflooded'] =  anfloo
                    _buoy[_name].coefficient['steel'] = afloo

                if m2 == 0:
                    m2 = m1
                    inc = 1

                for x in range(m1, m2 + 1, inc):
                    _buoy[_name].items.append(x)

            # groups
            except ValueError:
            #else:
                _no_buoy += 1
                print('--> fix BUOA groups')
        #
        elif flag1 == 'END':
            _end = keyword
            #
            #
            #
            #
            #
    #
    print('    - Second pass')
    #
    for line in open(wajac_file):
        # Jump empty lines
        if not line:
            continue
        # Jump commented lines
        if re.match(r"C\b", line):
            continue
            # print('--> ',line)
        # Segment line
        keyword = line.split()
        #  Flag word
        if line[0] != ' ':
            flag1 = keyword[0]
        #
        if flag1 == 'DPTH':
            _no_elev += 1
            _elevation[_no_elev] = f2u.Metocean.Condition(_no_elev, _no_elev)
            if _mudline != 0:
                _elevation[_no_elev].mudline = _mudline
                _elevation[_no_elev].surface = float(keyword[1]) - abs(_mudline)
            else:
                _elevation[_no_elev].mudline = float(keyword[1])
                _elevation[_no_elev].surface = 0

            if _constants:
                _elevation[_no_elev].gravity = _constants[0]
                _elevation[_no_elev].water_density = _constants[1]
                _elevation[_no_elev].fluid_viscosity = _constants[2]
                _elevation[_no_elev].air_density = _constants[3]
                _elevation[_no_elev].air_viscosity = _constants[4]
                # print(line)
        #
        elif flag1 == 'SEAOPT':
            isea = int(float(line[10:15]))
            _seastate[str(isea)] = f2u.Metocean.Combination(isea, _wave[isea].name)
            _seastate[str(isea)].wave = _wave[isea]
            _seastate[str(isea)].wave_direction = float(line[15:20])
            _seastate[str(isea)].wave_kinematics = float(line[20:25])
            #_wave[isea].direction = float(line[15:20])
            #_wave[isea].kinematics = float(line[20:25])

            ctno = int(float(line[25:30]))

            try:
                _seastate[str(isea)].current = _current[ctno]
                try:
                    _seastate[str(isea)].current_direction = _current[ctno].direction
                except AttributeError:
                    _seastate[str(isea)].current_direction = float(line[15:20])
                _seastate[str(isea)].current_blockage = float(line[30:35])
                _seastate[str(isea)].current_stretching = float(line[35:40])
                #_wave[isea].current = _current[ctno]
                #_wave[isea].current_blockage = float(line[30:35])
                #_wave[isea].current_stretching = float(line[35:40])
            except KeyError:
                pass
            #
            _seastate[str(isea)].buoyancy = int(float(line[40:45]))
            _seastate[str(isea)].report = int(float(line[45:50]))
            #_wave[isea].buoyancy = int(float(line[40:45]))
            #_wave[isea].report = int(float(line[45:50]))

            # wind method
            wid = int(float(line[50:55]))
            try:
                _seastate[str(isea)].wind = _wind[wid]
                _seastate[str(isea)].wind_direction = _wind[wid].direction
                _seastate[str(isea)].wind_method = int(float(line[55:60]))
                #_wave[isea].wind = _wind[wid]
                #_wind[wid].method = int(float(line[55:60]))
            except KeyError:
                pass
        #
        elif flag1 == 'MEMGRW':
            # members
            if len(keyword) > 5:
                m1 = int(float(line[10:20]))
                m2 = int(float(line[20:30]))
                inc = int(float(line[30:40]))
                imem = int(float(line[50:55]))

                if m2 == 0:
                    m2 = m1
                    inc = 1

                for x in range(m1, m2 + 1, inc):
                    _mgrowth[imem].items.append(x)
            # groups
            else:
                print('--> fix SPEX groups')
        #
        #
        elif flag1 == 'CMAS':
            print(' ----> fix cmas here')
        #
        elif flag1 == 'COEF':
            print(' ----> fix coef here')
        #
        elif flag1 == 'CDFN':
            #
            #line = line.replace('CDFN', '')
            #line = line.replace(keyword[1], '')            
            #line = line.strip()
            #
            _diamater = float(line[7:16])
            
            rough = [float(line[24:32]), float(line[16:24]), float(line[16:24]),
                     float(line[40:48]), float(line[32:40]), float(line[32:40])]
            
            smooth = [float(line[56:64]), float(line[48:56]), float(line[48:56]),
                      float(line[72:80]), float(line[64:72]), float(line[64:72])]            
            #
            _name = 'default'
            try:
                _cdcm[_name].diameter_function(_diamater, rough, smooth)
            except KeyError:
                #_no_cdcm += 1
                _cdcm[_name] = f2u.Metocean.DragMassCoefficient(_name, 1)
                _cdcm[_name].diameter_function(_diamater, rough, smooth)
            #
            #
            #
            #_profileCd = [[1000, float(keyword[6])], [0.0, float(keyword[6])], 
            #              [-0.001, float(keyword[2])], [_mudline, float(keyword[2])]]
            #_profileCm = [[1000, float(keyword[8])], [0.0, float(keyword[8])], 
            #              [-0.001, float(keyword[4])], [_mudline, float(keyword[4])]]
            #            
            
            #if _no_cdcm == 1:
            #    _cdcm_temp_flag = line
            #    _name = 'default'
                #_cdcm_temp[_name] = f2u.Metocean.Parameters(_no_cdcm, _no_cdcm)
                #_cdcm_temp[_name].type = 'profile'
                #_cdcm_temp[_name].profile['cd'] = _profileCd
                #_cdcm_temp[_name].profile['cm'] = _profileCm
                #
            #
            #if _cdcm_temp_flag != line:
                #_name = keyword[1]
                #_cdcm_temp[_name] = f2u.Metocean.Parameters(_no_cdcm, _no_cdcm)
                #_cdcm_temp[_name].type = 'profile'
                #_cdcm_temp[_name].profile['cd'] = _profileCd
                #_cdcm_temp[_name].profile['cm'] = _profileCm
                #_cdcm_temp[_name].item.append(keyword[1])
            
            #else:
            #    _cdcm_temp[_name].items.append(keyword[1])
        #        
    #
    #
    # Postprocessing
    #
    for _lev in _elevation.values():
        _lev.gravity = float(_constants[0])
        _lev.water_density = float(_constants[1])
        _lev.fluid_viscosity = float(_constants[2])
        _lev.air_density = float(_constants[3])
        _lev.air_viscosity = float(_constants[4])    
    #
    _lev = {}
    if _elevation:
        min_water_depth = min([ abs(_elev.mudline - _elev.surface) 
                                for _elev in _elevation.values()])
        #
        water_depth = [min_water_depth]
        #
        for _lev_name, _elev in  _elevation.items():
            if min_water_depth == abs(_elev.mudline - _elev.surface):
                _lev = _elev
                _lev.number = 1
                _lev.name = analysis_name
                minlev = abs(_elev.mudline - _elev.surface)
            else:
                water_depth.append(abs(_elev.mudline - _elev.surface))
        #
        _lev.water_depth = water_depth    
    #
    # current
    for _curr in _current.values():
        _max = max(_curr.profile, key=itemgetter(1))[1]
        _curr.velocity = _max
        _temp = []
        for _items in _curr.profile:
            try:
                _temp.append((_items[0], _items[1] / _max))
            # TODO : check if this works
            except ZeroDivisionError:
                _temp.append((_items[0], 0))
        _curr.profile = _temp

    #
    for _mg in _mgrowth.values():
        _temp = []
        for _items in _mg.profile:
            _temp.append((_items[0] + _mudline, _items[1], _mg.roughness))

        _mg.profile = _temp
    #
    _no_cases = []
    for _w in _wave.values():
        _new_no = int(float(_w.name)) + _load_no
        _w.name = _new_no
        _w.water_depth = min_water_depth
        _no_cases.append(_new_no)
    #
    _load_no = max(_no_cases)
    for _curr in _current.values():
        _new_no = int(float(_curr.name)) + _load_no
        _curr.name = _new_no
        #
    #
    i = len(_cdcm)
    if _cdcm_temp:
        
        for _cd in _cdcm_temp.values():
            i += 1
            _cdcm[i] = _cd
            _cdcm[i].number = i
            _cdcm[i].name = i
    #
    _wind_items = []
    _air_drag = {}
    i = 1
    for _cd in _wind_cd.values():
        _wind_items.extend(_cd.items)
        _cdy = _cd.coefficient['cd'][1] # cdy
        _cdz = _cd.coefficient['cd'][2] # cdz

        if _cdy == -1.0 and _cdz == -1.0:
            continue
        # 
        i += 1
        _air_drag[i] = f2u.Metocean.DragMassCoefficient(i, i) # _cd
        _air_drag[i].number = i
        _air_drag[i].name = i
        _cdm = _cd.coefficient['cd']
        _cdm.extend([0, 0, 0])
        _air_drag[i].set_cdcm(_cdm)
        _air_drag[i].type = 'specified'
    #
    #
    if _wind:
        _load_no = max(_no_cases)
        _no_cases = []
        for _wnd in _wind.values():
            _new_no = int(float(_wnd.name)) + _load_no
            _wnd.name = _new_no
            _wnd.items.extend(_wind_items)
            _no_cases.append(_new_no)
    #
    i = 0
    _buoyancy = {}
    for _by in _buoy.values():
        i += 1
        _buoyancy[i] = _by
        _buoyancy[i].number = i
        _buoyancy[i].name = i
    #
    #
    _diam_out = {}
    i = 0
    for _key, _item in _diameter.items():
        i += 1
        _diam_out[i] = f2u.Metocean.Parameters(i, i)
        _diam_out[i].coefficient = _key
        _diam_out[i].items = _item
        _diam_out[i].type = 'hydrodynamic_diameter'
    #
    print('--- End Reading WAJAC.INP file')
    #
    #
    _case = False
    if _seastate:
        _case = f2u.Analysis.Case(analysis_name, 1)
        _case.metocean_combination = _seastate
        
        if _lev:
            _case.condition = _lev
        #try:
        #    model['analysis'].case[combination_name].metocean_combination.update(_seastate)
        #except KeyError:
        #    model['analysis'].case[combination_name] = f2u.Case(1, combination_name)
        #    model['analysis'].case[combination_name].metocean_combination = _seastate
        #
        #_analysis[analysis_name] = _seastate
        
    #
    _model = f2u.F2U_model('metocean')
    _model.set_metocean()
    _model.metocean.data = _user_data
    _model.metocean.wave = _wave
    _model.metocean.current = _current
    #_model.metocean.condition = _elevation
    _model.metocean.marine_growth = _mgrowth
    _model.metocean.wind = _wind
    _model.metocean.hydro_diameter = _diam_out
    _model.metocean.non_hydro = _non_hydro
    _model.metocean.cdcm = _cdcm
    _model.metocean.air_drag = _air_drag
    _model.metocean.buoyancy = _buoyancy
    _model.metocean.flooded = _flooded
    #
    return _model.metocean, _case
    #
    #
    #
    #
    #
