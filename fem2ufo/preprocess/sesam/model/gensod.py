# 
# Copyright (c) 2009-2015 fem2ufo
# 
# Python stdlib imports
import math
import re
import sys
from itertools import islice

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
from fem2ufo.preprocess.common.foundation import get_spring_material

#
#
# --------------------------------------------------------------------
#                             Sub
# --------------------------------------------------------------------
#
#
#
# FIXME : ugly code
def read_gensoil(GENSOD_file, _length=1, _force=1):
    """
    """
    #
    # read T fem file
    GENSODinp = process.common.check_input_file(GENSOD_file, 
                                                file_extension='lis')

    if not GENSODinp:
        print('    ** I/O error : GENSOD file {:} not found'
              .format(GENSOD_file))
        sys.exit()
    #
    #   Define Constans
    #
    i = 0
    x = 0
    nolayer = 0
    #
    isoil = 1
    #
    #   Define Flags
    flag1 = 'off'
    flag2 = 'off'
    #
    #   Define Dictionaries
    _user_data = {}
    _user_data['GENSOD'] = f2u.InputUserData()
    _user_data['GENSOD'].file_name = GENSODinp
    #
    print('')
    print('------------------- REDSOIL Module -----------------')
    print('--- Reading GENSOIL file')
    #
    soil = {}
    _PY = {}
    _TZ = {}
    _QZ = {}
    #
    _isoil = 0
    _springNo = 1
    _bottom = 0
    _Zcoord = 0
    #
    with open(GENSODinp) as fin:
        for line in fin:
            line = line.replace(':', ' ')
            line = line.replace('=', ' ')
            line = line.strip()
            #
            keyword = line.split()
            # Jump empty lines
            if not line: 
                continue
            #
            # --------------------------------------------------------------------
            #                             Read User Data
            # --------------------------------------------------------------------
            #
            if 'VERSION' in line:
                _user_data['GENSOD'].version1 = keyword[1]
                _user_data['GENSOD'].program = 'GENSOD'
            #
            elif 'C    Date' in line:
                _user_data['GENSOD'].date = keyword[3]
                _user_data['GENSOD'].user = keyword[9]
            #
            # --------------------------------------------------------------------
            #                             Find Z level
            # --------------------------------------------------------------------
            #        
            if 'Z-LEVEL OF NON-SCOURED SOIL SURFACE' in line:
                _Zcoord = float(keyword[1])
            #
            # --------------------------------------------------------------------
            #                             Read Soil Data
            # --------------------------------------------------------------------
            #
            elif 'Z-LEVEL OF GROUND WATER TABLE' in line:
                _mudline = float(keyword[1])
            #
            #elif 'LAY' == keyword[0] and 'TYP' == keyword[1]:
            #    x = 0
            #    while True:
            #        data = list(islice(fin, 1))
            #        data = data[0].split()
            #        if not data:
            #            continue
            #        #
            #        _mudline = float(data[3]) - abs(float(data[4]))
            #        flag2 = 'off'
            #        break
            #
            elif 'COMPLETE P-Y / T-Z / Q-Z RESULTS' in line:
                #
                flag2 = 'layers'
                _nolayer = int(keyword[8])
                _diam = float(keyword[12])
                #
                if _nolayer == 1:
                    _isoil += 1
                    print('    * reading soil : {:}'.format(_isoil))
                    _top = _Zcoord
                #
                try:
                    soil[_isoil]
                except KeyError:
                    soil[_isoil] = f2u.Foundation.Soil(_isoil, _isoil)
            #
            # FIXME : need to be fixed
            elif '********************************' in keyword[0] and 'ZBOTM' == keyword[1]:
                _bottom = float(keyword[2])
                _depth = float(keyword[2])
                
                # if layer exists then update spring values with the next data
                try:
                    soil[_isoil].layers[_depth]
                # create new soil layer
                except KeyError:
                    soil[_isoil].layers[_depth] = f2u.Foundation.Layer(_depth, _nolayer)
                    soil[_isoil].diameter = _diam
                    soil[_isoil].mudline = _mudline
                    #
                    _springNo += 3
                    _pyNo = _springNo - 3
                    _tzNo = _springNo - 2
                    _qzNo = _springNo - 1
                #
                _PY[_pyNo] = f2u.Geometry.SpringElement('Py', _pyNo)
                _TZ[_tzNo] = f2u.Geometry.SpringElement('Tz', _tzNo)
                _QZ[_qzNo] = f2u.Geometry.SpringElement('Qz', _qzNo)
                #
                soil[_isoil].layers[_depth].curve['PY'] = _PY[_pyNo]
                soil[_isoil].layers[_depth].curve['TZ'] = _TZ[_tzNo]
                soil[_isoil].layers[_depth].curve['QZ'] = _QZ[_qzNo]
                #
                soil[_isoil].layers[_depth].depth = _depth
                soil[_isoil].layers[_depth].diameter = _diam
                #soil[_isoil].layers[_depth].bottom = _bottom
                flag2 = 'on'
                #
                #
                _tck = abs(soil[_isoil].layers[_depth].depth) - abs(_top)
    
                if _tck > 0:
                    # res = _tck
                    soil[_isoil].layers[_depth].thickness = _tck
                    _top = soil[_isoil].layers[_depth].depth
                else:
                    # _tck = res
                    print('    *** warning soil layer {:} has zero thickness'.
                          format(_nolayer))
                    # ignored springs
                    flag2 = 'off'
            #
            elif 'POINT' == keyword[0] and 'PY-STRESS' == keyword[1]:
                x = 0
                flag1 = 0
                #
                while True:
                    data = list(islice(fin, 1))
                    try:
                        data = data[0].split()
                    except IndexError:
                        line=""
                        break
                    
                    if not data:
                        continue
                    #
                    # check if a number
                    try:
                        float(data[0])
                        if len(data) < 2:
                            break
                    except ValueError:
                        break
                    #
                    x += 1
                    #
                    # -----------------------------------------------------------------------
                    #    Read P-Y Data
                    # -----------------------------------------------------------------------
                    #
                    _PY[_pyNo].force.append(float(data[1])
                                            * (soil[_isoil].layers[_depth].diameter
                                               * 1.0) * _force)
                    #
                    _PY[_pyNo].displacement.append(float(data[2]) * _length)
                    #
                    # -----------------------------------------------------------------------
                    #    Read T-Z Data
                    # ------------------------------------------------------------------------
                    #
                    _TZ[_tzNo].force.append(float(data[3]) * math.pi
                                            * (soil[_isoil].layers[_depth].diameter
                                               * 1.0) * _force)
                    #
                    _TZ[_tzNo].displacement.append(float(data[4]) * _length)
                    #
                    # -----------------------------------------------------------------------
                    #     Read Q-Z Data
                    # ------------------------------------------------------------------------
                    #
                    _QZ[_qzNo].force.append(float(data[5])
                                            * (math.pi / 4.0)
                                            * (soil[_isoil].layers[_depth].diameter ** 2)
                                            * _force)
                    #
                    _QZ[_qzNo].displacement.append(float(data[6]) * _length)
    #
    #
    spring = [_PY, _TZ, _QZ]
    print('--- End READ SOIL module')
    #
    _material_spring = get_spring_material(spring)
    #
    # adjust bottom elevation
    for _key1, _soil in sorted(soil.items()):
        _Zcoord = 0
        for _key2, _layer in sorted(_soil.layers.items()):
            _Zcoord += _layer.thickness
            _layer.depth = _Zcoord
    #
    return soil, _material_spring, _user_data
    #

#
#
#
