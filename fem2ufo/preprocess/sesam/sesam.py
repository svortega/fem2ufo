#
# Copyright (c) 2009-2018 fem2ufo
# 


# Python stdlib imports
import os
import sys

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.preprocess.sesam.model.wajac as wajac
from fem2ufo.preprocess.sesam.model.fem import (read_Tfem, 
                                                read_Lfem, 
                                                get_piles)
from fem2ufo.preprocess.common.foundation import read_foundation

#
# ====================================================================
#
class SESAM_model(object):
    """Class reads SESAM FE model and place dat in a FE model concept"""
    #
    def __init__(self, tol=False, joint_angle=1, joint_coplanary=0.01):
        """
        **Parameters**:
           :tol: set tolerance to identied supernode at pile head 
           :angle : maximum angle tolerance to idenfy Chord elements (1 degree default)
           :coplanary : maximum tolerance to identify if braces are coplanar 
                        as well as normal to chord (0.01 default)
        """
        #
        self.format = 'sesam'
        self.tolerance = tol
        self.joint_angle = joint_angle
        self.joint_coplanary = joint_coplanary       
        #       
        self.check_out = []
        self.factors = [0, 0, 0, 0, 0, 0]
        self.units_in = None
        self.units_out = None
        self.gravity = False

        self.model = {}
        self.gemetry_file = False
        self.loading_file = False
        self.foundation_file = False
        self.metocean_file = False

        self.fill_gap = False
        self.load_mass = []

        self.delete_member_list = []
        self.delete_member_group_list = []

        self.delete_node_list = []
        self.delete_node_group_list = []

        self.delete_load_list = []
        self.delte_load_group_list = []

        self.update_material_number = {}
        self.update_material_name = {}

        self.new_material_number = {}
        self.new_material_name = {}

    #
    # input data
    #
    def directory_path(self, path):
        """
        **Parameters**:
           :path: path to the working directory
        """
        try:
            path = os.path.normcase(path)
            os.chdir(path)
        except FileNotFoundError:
            print('    ** path error : {:}\n'.format(path))
            print('    ** End')
            sys.exit()

        retval = os.getcwd()
        print('')
        print('--- Working directory :')
        print('--- {:}'.format(retval))
    #
    def units(self, **kwargs):
        """
        Provide units of the SESAM input files (mandatory)
        Available units: length, mass, time, temperature, force, pressure/stress  
        
        **Parameters**:
           :length: [mandatory]  
           :force: [mandatory]  
           :temperature :  
           :gravity: [default : 9.81 m/s**2]  
        """

        for key, value in kwargs.items():
            _unit = process.units.find_unit_case(key)
            self.units_in = process.units.units_module(_unit, value,
                                                       self.units_in)
        #
        self.units_out = self.units_in
        #
    #
    def geometry(self, geometry):
        """
        **Parameters**:  
           :geometry : T1.FEM file
           :load     : L1.FEM file (optional)
        """
        #
        self.gemetry_file = geometry
        # set master node tolerance
        if not self.tolerance:
            try:
                self.tolerance = process.units.get_tolerance(self.units_in[0])
            except SyntaxError:
                print('  **  error input units not provided')
                print('      process terminated')
                sys.exit()
        # read data
        self.model = read_Tfem(self.gemetry_file, 
                               self.joint_angle, 
                               self.joint_coplanary)
        #
        if not self.model.component:
            print('  error --> geometry not found')
            sys.exit()
        #
        # Processing piles below mudline
        try:
            get_piles(self.model)
            # Find  twin node for pile head
            process.geometry.find_super_node(self.model.component, 
                                             self.model.foundation.piles,
                                             self.tolerance)
        except IndexError:
            print('    * No piles found')
        #
        self.model.data.update(self.model.component.data)
        self.model.data['format'] = 'sesam'
    #
    def loading(self, loading):
        """
        **Parameters**:
           :load: L1.FEM file
        """
        self.loading_file = loading

        if not self.model.component:
            print('  error --> geometry not found')
            sys.exit()
        #
        read_Lfem(self.loading_file, self.model)
        self.model.data.update(self.model.load.data)
    #
    def foundation(self, foundation_file,
                   file_format=False, foundation_units=False):
        """
        **Parameters**:  
           :foundation: GENSOD.lis file  
           :Format: sesam (default) 
           :units : [length, mass, time, temperature, force, pressure/stress]
        """
        #
        self.foundation_file = foundation_file

        if not file_format:
            file_format = self.format

        if not foundation_units:
            foundation_units = self.units_in

        if not self.units_in[0] and self.units_in[4]:
            print('  **  error input units not provided')
            print('      process terminated')
            sys.exit()

        read_foundation(self.model,
                        foundation_file,
                        file_format, 
                        foundation_units,
                        self.units_out)
        #
        self.model.data.update(self.model.foundation.data)
        process.foundation.renumber_soil(self.model.foundation.materials,
                                         self.model.component.materials)
    #
    def metocean(self, metocean, analysis_name=False,
                 design_condition=None):
        """
        **Parameters**:
           :metocean : wajac.inp
        """

        self.metocean_file = metocean

        if not self.model.component:
            print('  error --> geometry not found')
            sys.exit()

        wajac.read_metocean(self.model,
                            self.metocean_file,
                            self.format,
                            analysis_name)

        #self.model.data.update(self.model.metocean.data)
        #

    #
    # process
    #
    #
    def delete_element(self, *members):
        """
        Delete FE elements from model by concept name
        including loads associated with this element
        
        **Parameters**:
           :elements: name or number list
        """

        _name, _number = process.geometry.select_items(members)

        member_list = process.concept.find_fe_number_by_concept_name(self.model.component.concepts,
                                                                     _name, _number)

        self.delete_member_list.extend(member_list)
    #
    #
    def _get_fe_number_by_name(items, component):
        """
        """
        _name, _number = process.geometry.select_items(items)
    
        member_list = process.concept.find_item_fe_number_by_name(component,
                                                                     _name, _number)
        return member_list    
    #
    def delete_element_group(self, *sets):
        """
        Delete FE elements from model by member's sets
        including loads associated with this element
        
        **Parameters**:
           : sets: name or number list
        """
        #
        group_list = SESAM_model._get_fe_number_by_name(sets, self.model.component.sets)
        
        self.delete_member_group_list.extend(group_list)

    #
    def delete_node(self, *nodes):
        """
        Delete FE nodes from model including loads associated with this node
        
        **Parameters**:
           : nodes: name or number list
        """
        #
        group_list = SESAM_model._get_fe_number_by_name(nodes, self.model.component.sets)
        
        self.delete_node_list.extend(node_list)
    #
    def delete_node_group(self, *sets):
        """
        Delete FE nodes from model including loads associated with this node
        
        **Parameters**:
           : sets: name or number list
        """
        #
        group_list = SESAM_model._get_fe_number_by_name(sets, self.model.component.sets)
        self.delete_node_group_list.extend(group_list)
    #
    #
    def delete_load(self, *loads):
        """
        Delete FE model load cases
        
        **Parameters**:
           : loads: name or number list
        """
        _load_no = SESAM_model._get_fe_number_by_name(loads, self.model.load.functional)
        self.delete_load_list.extend(_load_no)
    #
    def load_to_mass(self, *load, option='node', factor=1.0):
        """
        Convert load to mass
        
        **Parameters**:
           :load : loads: name or number list
           :factor: load factor (default=1)
           :option: string
                    -node mass lumped at element nodes (default)
                    -member mass distributed to element
        """

        _load_no = SESAM_model._get_fe_number_by_name(load, self.model.load.functional)
        
        if not self.gravity:
            _units_input = self.units_in
            _units_output = self.units_in
            factors, _grav = process.units.get_factors_and_gravity(_units_input,
                                                                   _units_output)

        _factor = []
        _option = []
        _grav_item = []
        for _no in _load_no:
            _option.append(option)
            _factor.append(factor)
            _grav_item.append(_grav)

        try:
            self.load_mass[0].extend(_load_no)
            self.load_mass[1].extend(_factor)
            self.load_mass[2].extend(_option)
            self.load_mass[3].extend(_grav_item)
        except IndexError:
            self.load_mass.append(_load_no)
            self.load_mass.append(_factor)
            self.load_mass.append(_option)
            self.load_mass.append(_grav_item)

            # print('??')

    #
    def update_material(self, **kwargs):
        """
        Update an existing material properties
        
        **Parameters**:
          :name/number  : material name or fe number list
          :Fy: Yield strenght  
          :Emodulus: Elastic modulus  
          :poisson: poisson modulus  
          :density: material density  
          :alpha: thermal  
        """
        _mat, _type = process.material.assign_material_item(kwargs)

        if _type == 'number':
            self.update_material_number.update(_mat)
            # _mat_name = {}
        else:
            self.update_material_name.update(_mat)
            # _mat_no = {}
    #
    def load_fill_gap(self, fill_gap=True):
        """
        FE load re-number consecutively
        """
        self.fill_gap = fill_gap

    #
    # modelling
    # 
    def add_material(self, **kwargs):
        """
        Create a new material
        
        **Parameters**:
          :name: material name
          :Fy: Yield strengh
          :Emodulus: Elastic modulus
          :poisson: poisson modulus
          :density: material density
          :alpha: thermal
        """
        _mat, _type = process.material.assign_material_item(kwargs)

        if _type == 'number':
            self.new_material_number.update(_mat)
        else:
            self.new_material_name.update(_mat)

    #
    def add_joint(self, *nodes):
        """
        Define tubular joint by providing a node. fem2ufo finds the
        joint components : cord and brace members.
        
        **Parameters**:
          :nodes : node number or name list
        """        
        # TODO : implement this section
        group_list = SESAM_model._get_fe_number_by_name(nodes, self.model.component.sets)
        process.add_new_joint(node_list)

    #
    def add_wave(self):
        """
        Add a new wave
        """
        # TODO : implement this section
        self.model['metocean'] = process.metocean.add_wave(_model.metocean,
                                                           _noWave, _nameWave,
                                                           _theory, height, period)

    #
    # output
    #
    def get_model(self, meshing=False):
        """
        Processing model --> mandatory
        """
        print('')
        print('----------------- Processing Model -----------------')
        # print('')

        # deleting elements
        _msg = process.geometry.delete_elements(self.model.component,
                                                self.delete_member_list,
                                                self.delete_member_group_list)
        #
        self.check_out.extend(_msg)

        # deleting nodes
        _msg = process.geometry.delete_nodes(self.model.component,
                                             self.delete_node_list,
                                             self.delete_node_group_list)
        self.check_out.extend(_msg)

        #
        # converting load to mass
        if self.load_mass:
            process.load.convert_load_to_mass(self.model.component,
                                              self.model.load,
                                              self.load_mass)

        # deleting loads
        _msg = process.load.delete_load(self.model.load,
                                        self.delete_load_list,
                                        self.delte_load_group_list)

        self.check_out.extend(_msg)

        # filling load gap (consecutive numbering)
        if self.fill_gap:
            process.load.fill_gap_load(self.model.load.functional)

        #
        # check if mass piramid material
        _msg = process.material.find_mass_piramid_material(self.model.component)
        self.check_out.extend(_msg)
        #
        # new material
        process.material.add_new_material(self.model.component,
                                          self.model.foundation,
                                          self.new_material_name,
                                          self.new_material_number)
        # updating material
        process.material.update_material(self.model.component,
                                         self.update_material_name,
                                         self.update_material_number)
        #
        #
        # clean model
        self.model = process.geometry.clean_model(self.model)
        #
        # check units
        #
        if self.units_out.count(None) == len(self.units_out):
            self.units_out = self.units_in
        #
        self.model.data['units'] = self.units_out
        #
        return self.model
    #
    def plot(self):
        """
        Plotting model (no yet implemented)
        """
        pass
        #
