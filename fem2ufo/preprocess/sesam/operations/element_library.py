# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports


# package imports
import fem2ufo.preprocess.sesam.operations.concept as concept

# --------------------------------------------------------------------
#                       SESAM ELEMENT LIBRARY
# --------------------------------------------------------------------
#
element_type = {}
#
element_type[1] = concept.ElementType( 'N/A',    1, 0, 'No yet defined')
element_type[2] = concept.ElementType( 'BEPS',   2, 2, '2-D, 2 Node Beam', 'beam')
element_type[3] = concept.ElementType( 'CSTA',   3, 3, 'Plane Constant Strain Triangle', 'plate')
element_type[4] = concept.ElementType( 'N/A',    4, 0, 'No yet defined')
element_type[5] = concept.ElementType( 'RPBQ',   5, 4, 'Rec Plate Bending Modes', 'plate')
element_type[6] = concept.ElementType( 'ILST',   6, 6, 'Plane Lin Strain Triangle', 'plate')
element_type[7] = concept.ElementType( 'N/A',    7, 0, 'No yet defined')
element_type[8] = concept.ElementType( 'IQQE',   8, 8, 'Plane Quadrilateral Membrane Element', 'Membrane')
element_type[9] = concept.ElementType( 'LQUA',   9, 4, 'Plane Quadrilateral Membrane Element', 'Membrane')
element_type[10] = concept.ElementType( 'TESS', 10, 2, 'Truss Element', 'truss')
element_type[11] = concept.ElementType( 'GMAS', 11, 1, '1-Noded Mass Element', 'mass')
element_type[12] = concept.ElementType( 'GLMA', 12, 2, 'General 2-Noded Mass Element', 'mass')
element_type[13] = concept.ElementType( 'GLDA', 13, 2, 'General 2-Noded Damping Element')
element_type[14] = concept.ElementType( 'N/A',  14, 0, 'No yet defined')
element_type[15] = concept.ElementType( 'BEAS', 15, 2, '3-D, 2 Node Beam', 'beam')
element_type[16] = concept.ElementType( 'AXIS', 16, 2, 'Axial Spring', 'spring')
element_type[17] = concept.ElementType( 'AXDA', 17, 2, 'Axial Damper')
element_type[18] = concept.ElementType( 'GSPR', 18, 1, 'Spring to Ground', 'spring')
element_type[19] = concept.ElementType( 'GDAM', 19, 1, 'Damper to Ground')
element_type[20] = concept.ElementType( 'IHEX', 20, 20, 'Isoparametric Hexahedron', 'solid')
element_type[21] = concept.ElementType( 'LHEX', 21, 8, 'Linear Hexahedron', 'solid')
element_type[22] = concept.ElementType( 'SECB', 22, 3, 'Subparametric Curved Beam', 'beam')
element_type[23] = concept.ElementType( 'BTSS', 23, 3, 'General Curved Beam', 'beam')
element_type[24] = concept.ElementType( 'FQUS & FFQ', 24, 4,
                                             'Flat Quadrilateral Thin Shell & Free Formulation Quadrilateral Shell',
                                             'shell')
element_type[25] = concept.ElementType('FTRS & FFRS', 25, 3,
                                             'Flat Triangular Thin Shell & Free Formulation Triangular Shell', 'shell')
element_type[26] = concept.ElementType( 'SCTS', 26, 6, 'Subparametric Curved Triangular Thick Shell', 'shell')
element_type[27] = concept.ElementType( 'MCTS', 27, 6, 'Subpar Curved Triang Thick Sandwich Elem', 'shell')
element_type[28] = concept.ElementType( 'SCQS', 28, 8, 'Subparametric Curved Quadrilateral Thick Shell', 'shell')
element_type[29] = concept.ElementType( 'MCQS', 29, 8, 'Subpar Curved Quad Thick Sandwich Elem', 'shell')
element_type[30] = concept.ElementType( 'IPRI', 30, 15, 'Isoparametric Triangular Prism', 'solid')
element_type[31] = concept.ElementType( 'ITET', 31, 10, 'Isoparametric Tatrahedrom', 'solid')
element_type[32] = concept.ElementType( 'TPRI', 32, 6, 'Triangular Prism', 'solid')
element_type[33] = concept.ElementType( 'TETR', 33, 4, 'Tetrahedron', 'solid')
element_type[34] = concept.ElementType( 'LCTS', 34, 6, 'Subparam Layered Curved Triang Thick Shell')
element_type[35] = concept.ElementType( 'LCQS', 35, 8, 'Subparam Layered Curved Quad Thick Shell')
element_type[36] = concept.ElementType( 'TRS1', 36, 18, '2nd Order Hexahed Tran Elem Solid Shell', 'solid')
element_type[37] = concept.ElementType( 'TRS2', 37, 15, '2nd Order Hexahed Tran Elem Solid Shell', 'solid')
element_type[38] = concept.ElementType( 'TRS3', 38, 12, '2nd Order Hexahed Tran Elem Solid Shell', 'solid')
element_type[39] = concept.ElementType( 'N/A',  39, 0, 'No yet defined')
element_type[40] = concept.ElementType( 'GLSH', 40, 2, 'General Spring / Shim Element', 'spring')
element_type[41] = concept.ElementType( 'AXCS', 41, 3, 'Axisymmetric Constant Strain Triangle', 'plate')
element_type[42] = concept.ElementType( 'AXLQ', 42, 4, 'Axisymmetric Quadrilateral', 'plate')
element_type[43] = concept.ElementType( 'AXLS', 43, 6, 'Axisymmetric Linear Strain Triangle', 'plate')
element_type[44] = concept.ElementType( 'AXQQ', 44, 8, 'Axisymmetric Constant Strain Quadrilateral', 'plate')
element_type[45] = concept.ElementType( 'PILS', 45, 1, 'Pile / Soil')
element_type[46] = concept.ElementType( 'PCAB', 46, 2, 'Plane Cable-Bar Element')
element_type[47] = concept.ElementType( 'PSPR', 47, 1, 'Plane Spring Element')
element_type[48] = concept.ElementType( ' ',    48, 4, '4-Node Contact Element With Triangular Shape')
element_type[49] = concept.ElementType( 'N/A',  49, 2, '2-Node Link Element')
element_type[50] = concept.ElementType( 'N/A',  50, 0, 'No yet defined')
element_type[51] = concept.ElementType( 'CTCP', 51, 2, '2-Node Contact Element', 'contact')
element_type[52] = concept.ElementType( 'CTCL', 52, 4, '4-Node Contact Element', 'contact')
element_type[53] = concept.ElementType( 'CTAL', 53, 4, '4-Node Axisymetric Contact Element', 'contact')
element_type[54] = concept.ElementType( 'CTCC', 54, 6, '6-Node Contact Element', 'contact')
element_type[55] = concept.ElementType( 'CTAL', 55, 6, '6-Node (3+3) Axisymetric Contact Element', 'contact')
element_type[56] = concept.ElementType( 'CTLQ', 56, 8, '8-Node (4+4) Contact Element', 'contact')
element_type[57] = concept.ElementType( 'CTCQ', 57, 16, '16-Node (8+8) Contact Element', 'contact')
element_type[58] = concept.ElementType( 'CTMQ', 58, 18, '18-Node (9+9) Contact Element', 'contact')
element_type[59] = concept.ElementType( 'N/A',  59, 0, 'No yet defined')
element_type[60] = concept.ElementType( 'N/A',  60, 0, 'No yet defined')
element_type[61] = concept.ElementType( 'HCQS', 61, 9, '9-Node Shell Element', 'shell')
element_type[62] = concept.ElementType( 'N/A',  62, 0, 'No yet defined')
element_type[63] = concept.ElementType( 'N/A',  63, 0, 'No yet defined')
element_type[64] = concept.ElementType( 'N/A',  64, 0, 'No yet defined')
element_type[65] = concept.ElementType( 'N/A',  65, 0, 'No yet defined')
element_type[66] = concept.ElementType( 'SLQS', 66, 8, 'Semiloof Quad Courved Thin Shell (32 DOF)', 'shell')
element_type[67] = concept.ElementType( 'SLTS', 67, 6, 'Semiloof Triang Courved Thin Shell (24 DOF)', 'shell')
element_type[68] = concept.ElementType( 'SLCB', 68, 3, 'Semiloof Curved Beam (11 DOF)', 'beam')
element_type[69] = concept.ElementType( 'N/A',  69, 0, 'No yet defined')
element_type[70] = concept.ElementType( 'MATR', 70, 100, 'General Matrix Elem with Arbitrary No Nodes', 'matrix')
#
