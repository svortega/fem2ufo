#
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import sys
import math
from itertools import islice

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process

#
#
def extrac_load(_keyword, _item_no, _start=0):
    """
    function extrac load from the FEM data
    """
    _elementlist = []

    if _item_no <= (4 - _start):
        _loop = _item_no
        _item_end = 0
    else:
        _loop = 4
        _item_end = _item_no + _start - 4

    for _no in range(_start, _loop):
        _load_no = float(_keyword[_no])
        _elementlist.append(_load_no)
    #
    return _elementlist, _item_end
#
# FIXME : code very slow
def read_Lfem(l_fem, model):
    """
    fem2ufo module reads SESAM T.FEM files
    
    **Parameters**:  
      l_fem_file : SESAM L.FEM file, mandatory (loading usually from WAJAC)
      _load      : fem2ufo concept loading data, optional (internal use)
      _user_data : user data, optional (internal use)
    
    **Returns**:
      :user_data: L.FEM file user data
      :load: fem2ufo gravity, node and member concept loading
    
    load
        |_ data
        |_ functional
    
    """
    #
    #
    #
    lfem_file = process.common.check_input_file(l_fem, file_extension='fem')
    if not lfem_file:
        print('    ** I/O error : L.FEM file {:} not found'
              .format(l_fem))
        sys.exit()           
    #
    # check input data
    if not model.load:
        model.set_load()      
    #
    nodes = model.component.nodes
    _node_off = []
    _member = model.component.elements
    _member_off = []
    #
    _section_load = {}
    _node_load = {}
    #
    _user_data = {}
    #_user_data['LFEM'] = f2u.InputUserData()
    #_user_data['LFEM'].file_name = tfem_file
    #
    # ----------------------------------------------------------------------
    #                         Read L.FEM input file 
    # ----------------------------------------------------------------------
    #
    #
    print('--- Reading loading from {:} file'
          .format(lfem_file))
    print('    * First pass')
    #
    with open(lfem_file) as fin:
        for line in fin:
            # Segment line
            keyword = line.split()
            # Flag word
            if line[0] != ' ':
                flag1 = keyword[0]
            #
            # --------------------------------------------------------------------
            #                           Read User Data
            # --------------------------------------------------------------------
            #
            if 'DATE:' in line.upper():
                try:
                    _user_data['LFEM'].date = str(keyword[1])
    
                except KeyError:
                    _user_data['LFEM'] = f2u.InputUserData()
                    _user_data['LFEM'].file_name = l_fem
                    _user_data['LFEM'].date = str(keyword[1])
            #
            elif 'PROGRAM:  SESAM WAJAC' in line.upper():
                try:
                    _user_data['LFEM'].version1 = str(keyword[4])
                    _user_data['LFEM'].program = 'WAJAC'
    
                except KeyError:
                    _user_data['LFEM'] = f2u.InputUserData()
                    _user_data['LFEM'].file_name = l_fem
                    _user_data['LFEM'].version1 = str(keyword[4])
                    _user_data['LFEM'].program = 'WAJAC'
            #
            elif 'USER:' in line.upper():
                try:
                    _user_data['LFEM'].user = str(keyword[1])
    
                except KeyError:
                    _user_data['LFEM'] = f2u.InputUserData()
                    _user_data['LFEM'].file_name = l_fem
                    _user_data['LFEM'].user = str(keyword[1])
            #
            # ----------------------------------------------------------------------
            #                             LOAD SECTION
            # ----------------------------------------------------------------------
            #
            # Read Gravity Load
            #
            elif 'BGRAV' in line:
                _loadcase_no = int(float(keyword[1]))
                _grav = []
                data = list(islice(fin, 1))
                _rows = data[0].split()
                #else:
                _grav.append(float(_rows[0]))
                _grav.append(float(_rows[1]))
                _grav.append(float(_rows[2]))
                #
                model.load.functional[_loadcase_no].gravity.extend(_grav)
            #                
            #
            # Read Nodal Load
            #
            elif 'BNLOAD' in line:
                _load_case_no = int(float(keyword[1]))
                _type = int(float(keyword[2]))
                _complex = int(float(keyword[3]))
                _loadlist = []
                #
                data = list(islice(fin, 1))
                #
                _rows = data[0].split()
                #
                _nodenumber = int(float(_rows[0]))
                _dof_no = int(float(_rows[1]))
                
                # check if node exist
                try:
                    _node = nodes[_nodenumber]
                except KeyError:
                    _node_off.append(_nodenumber)
                    flag1 = 'BNLOAD_NA'
                    print('error load on node number', _nodenumber)
                    continue
                #
                if _complex == 1:
                    _dof_no = 2 * _dof_no
                #
                _dummy_list, _dof_no = extrac_load(_rows, _dof_no, 2)
                _loadlist.extend(_dummy_list)
                #
                #else:
                _no_rows = math.ceil(_dof_no / 4.0)
                data = list(islice(fin, _no_rows))
                for _data in data:
                    _rows = _data.split()               
                    _dummy_list, _dof_no = extrac_load(_rows, _dof_no)
                    _loadlist.extend(_dummy_list)
                #
                # check if no more items
                if _dof_no <= 0:
                    try:
                        _node.load[_load_case_no].point.append(_loadlist)
                    except KeyError:
                        _node.load[_load_case_no] = f2u.Load.Node(_nodenumber, _nodenumber)
                        _node.load[_load_case_no].complex = _complex
                        _node.load[_load_case_no].type = _type
                        _node.load[_load_case_no].dof = _dof_no
                        _node.load[_load_case_no].point.append(_loadlist)
                        #model.load.functional[_load_case_no].node.append(_node.load[_load_case_no])
                        model.load.functional[_load_case_no].node.append(_nodenumber)
                    #print ('ok')
            #
            # Read Member Uniform Load
            #
            elif 'BELOAD1' in line:
                _load_case_no = int(float(keyword[1]))
                _type = int(float(keyword[2]))
                _complex = int(float(keyword[3]))
                _option = int(float(keyword[4]))
                _distancefromnode = []
                _member_udl = []
                data = list(islice(fin, 2))
                # 
                _rows = data[0].split()
                # Internal element number (generated by the program)
                memb_number = int(float(_rows[0]))
                
                try:
                    _element = _member[memb_number]
                except KeyError:
                    _member_off.append(memb_number)
                    flag1 = 'BELOAD1_NA'
                
                # L1 & L2
                _distancefromnode.append(float(_rows[1]))
                _distancefromnode.append(float(_rows[2]))
                # Product of last degree of freedom
                _dof_no = int(float(_rows[3]))
                _edof = int(float(_rows[3]))

                # fill section data
                if _complex == 1:
                    _dof_no = 2 * _dof_no
                    _edof = 2 * _edof
                #
                #if rows == 2:
                _rows = data[1].split()
                _dummy_list, _dof_no = extrac_load(_rows, _dof_no, 1)
                _member_udl.extend(_dummy_list)
                #
                #else:
                _no_rows = math.ceil(_dof_no / 4.0)
                data = list(islice(fin, _no_rows))
                for _data in data:
                    _rows = _data.split()                
                    _dummy_list, _dof_no = extrac_load(_rows, _dof_no)
                    _member_udl.extend(_dummy_list)

                # check if no more items
                if _dof_no <= 0:
                    # TODO: check this if works
                    # add udl per node
                    _nnod = int(_edof / 3.0)
                    _nnod = int(_edof / _nnod)
                    _udl_temp = []
                    _udl_temp.append(_member_udl[:_nnod])
                    _udl_temp.append(_member_udl[_nnod:])
                    # add zero mx, my & mz
                    _udl_temp[0].extend([0,0,0])
                    _udl_temp[1].extend([0,0,0])                    
                    #
                    _udl = f2u.Load.BeamUDL(memb_number, _load_case_no)
                    _udl.input_udl(_udl_temp, _distancefromnode)
                    _udl.type = _type
                    _udl.complex = _complex
                    _udl.option = _option
                    _udl.dof = _dof_no
                    
                    if _option == 0:
                        _udl.beam_length = _element.flexible_length
                    else:
                        _udl.beam_length = _element.length_node2node
                    
                    try:
                        _element.load[_load_case_no].distributed.append(_udl)
                        #_element.load[_load_case_no].distance.append(_distancefromnode)
                    except KeyError:
                        _element.load[_load_case_no] = f2u.Load.Element(memb_number, memb_number)
                        #_element.load[_load_case_no].type = _type
                        #_element.load[_load_case_no].complex = _complex
                        #_element.load[_load_case_no].option = _option
                        #_element.load[_load_case_no].dof = _dof_no
                        
                        _element.load[_load_case_no].distributed.append(_udl)
                        #_element.load[_load_case_no].distance.append(_distancefromnode)                        
                        #
                        #model.load.functional[_load_case_no].element.append(_element.load[_load_case_no])
                        model.load.functional[_load_case_no].element.append(memb_number)
                    # print  _loadCaseNo, _sectionL[_elementNumber].distributed, _sectionL[_elementNumber].distance
            #
            # Read Elements with Initial Strain Due to Thermal Expansion 
            #
            elif 'BEISTE' in line:
                _load_case_no = int(float(keyword[1]))
                _type = int(float(keyword[2]))
                _option = int(float(keyword[4]))
                _loadlist = []
                data = list(islice(fin, 1))
                #
                _rows = data[0].split()
                # Internal element number (generated by the program)
                memb_number = int(float(_rows[0]))
                _nodenumber = int(float(_rows[1])) - 1
                _element = _member[memb_number]
                #
                try:
                    _element.load[_load_case_no].option = _option
                    _element.load[_load_case_no].temperature.append(float(_rows[3]))
                except KeyError:
                    _element.load[_load_case_no] = f2u.Load.Element(memb_number, memb_number)
                    #_element.load[_load_case_no].type = _type
                    #
                    #_element.load[_load_case_no].option = _option
                    _element.load[_load_case_no].temperature.append(float(_rows[3]))
                #
                _no_rows = math.ceil(_nodenumber / 4.0)
                data = list(islice(fin, _no_rows))
                for _data in data:
                    _rows = _data.split()
                    if _type == 1:
                        for _no in _rows:
                            _element.load[_load_case_no].temperature.append(float(_no))
                    else:
                        print('fix this, just for shells')

                # fix this svo
                #model.load.functional[_load_case_no].element.append(_element.load[_load_case_no])
                model.load.functional[_load_case_no].element.append(memb_number)
            #
            # Read Nodes with Prescribed Displacements and Accelerations
            #
            elif 'BNDISPL' in line:
                _load_case_no = int(float(keyword[1]))
                _type = int(float(keyword[2]))
                _complex = int(float(keyword[3]))
                data = list(islice(fin, 1))
                #
                _rows = data[0].split()
                # Internal element number (generated by the program)
                _nodenumber = int(float(_rows[0])) - 2
                _dof_no = int(float(_rows[1]))
                
                _node = nodes[_nodenumber]                   
                
                try:
                    _node.load[_load_case_no]
                except KeyError:
                    _node.load[_load_case_no] = f2u.Load.Node(_nodenumber, _nodenumber)
                    _node.load[_load_case_no].complex = _complex
                    _node.load[_load_case_no].type = _type
                    _node.load[_load_case_no].dof = _dof_no
                    #model.load.functional[_load_case_no].node.append(_node.load[_load_case_no])
                    model.load.functional[_load_case_no].node.append(_nodenumber)

                if _type == 3:
                    _node.load[_load_case_no].acceleration.append(float(_rows[2]))
                    _node.load[_load_case_no].acceleration.append(float(_rows[3]))
                else:
                    _node.load[_load_case_no].displacement.append(float(_rows[2]))
                    _node.load[_load_case_no].displacement.append(float(_rows[3]))
                #
                _no_rows = math.ceil(_nodenumber / 4.0)
                data = list(islice(fin, _no_rows))
                for _data in data:
                    _rows = _data.split()                
                    for _no in _rows:
                        if _type == 3:
                            _node.load[_load_case_no].acceleration.append(float(_no))
                        else:
                            _node.load[_load_case_no].displacement.append(float(_no))

                # fix this svo
                #model.load.functional[_load_case_no].node.append(_node.load[_load_case_no])
            #
            # Read Elements with Surface Loads
            #
            elif flag1 == 'BEUSLO':
                _load_case_no = int(float(keyword[1]))
                _type = int(float(keyword[2]))
                _complex = int(float(keyword[3]))
                _pressurelist = []
                #
                data = list(islice(fin, 1))
                _rows = data[0].split()
                memb_number = int(float(_rows[0]))
                _dof_no = int(float(_rows[1]))
                
                try:
                    _element = _member[memb_number]
                except KeyError:
                    _member_off.append(memb_number)
                    flag1 = 'BEUSLO_NA'
                #
                if _complex == 1:
                    _dof_no = 2 * _dof_no
                #
                _no_rows = math.ceil(_dof_no / 4.0)
                data = list(islice(fin, _no_rows))
                for _data in data:
                    _rows = _data.split()                 
                    _dummy_list, _dof_no = extrac_load(_rows, _dof_no)
                    _pressurelist.extend(_dummy_list)
                # check if no more items
                if _dof_no <= 0:
                    try:
                        _element.load[_load_case_no].pressure.append(_pressurelist)
                    except KeyError:
                        _element.load[_load_case_no] = f2u.Load.Element(memb_number, memb_number)
                        _element.load[_load_case_no].pressure.append(_pressurelist)
                    #
                    #model.load.functional[_load_case_no].element.append(_element.load[_load_case_no])
                    model.load.functional[_load_case_no].shell.append(memb_number)
            #
            # line loads 
            elif 'BELLO2' in line:
                _load_case_no = int(float(keyword[1]))
                _type = int(float(keyword[2]))
                _complex = int(float(keyword[3]))
                _layer = int(float(keyword[4]))
                _line_list = []
                #
                data = list(islice(fin, 1))
                _rows = data[0].split()
                memb_number = int(float(_rows[0]))
                _dof_no = int(float(_rows[1]))
                _intno = int(float(_rows[2]))
                _line = int(float(_rows[3]))
                #
                try:
                    _element = _member[memb_number]
                    _element_type = _element.type.element
                except KeyError:
                    _member_off.append(memb_number)
                    flag1 = 'BELLO2_NA'
                #
                if _complex == 1:
                    _dof_no = 2 * _dof_no
                #
                data = list(islice(fin, 1))
                _rows = data[0].split()
                _side = int(float(_rows[0]))
                _line_list.extend([float(_item) for _item in _rows[1:]])
                #
                _dof_no -= 3
                #
                _no_rows = math.ceil(_dof_no / 4.0)
                data = list(islice(fin, _no_rows))
                for _data in data:
                    _rows = _data.split()
                    _dummy_list, _dof_no = extrac_load(_rows, _dof_no)
                    _line_list.extend(_dummy_list)
                #
                if 'shell' in _element_type.lower():
                    _load_node_1 = []
                    _load_node_2 = []
                    for x in range(3):
                        _b = _element.line_length(_line)
                        R1, R2 = f2u.Load.trapezoid_udl(w1=_line_list[x], 
                                                        w2=_line_list[x+3], 
                                                        _a=0, _b=_b, _c=0)
                        _load_node_1.append(R1)
                        _load_node_2.append(R2)
                    #
                    _load_node_1.extend([0, 0, 0])
                    _load_node_2.extend([0, 0, 0])                    
                    #
                    _number_nodes = len(_element.node)
                    end_1, end_2 = f2u.Geometry.get_node_end(_number_nodes, _line)
                    _node_1 = _element.node[end_1]
                    _node_2 = _element.node[end_2]
                    #print('--->')
                    # node end 1
                    try:
                        _node_1.load[_load_case_no].point.append(_load_node_1)
                    except KeyError:
                        _node_1.load[_load_case_no] = f2u.Load.Node(_node_1.number, _node_1.number)
                        _node_1.load[_load_case_no].complex = _complex
                        _node_1.load[_load_case_no].type = _type
                        _node_1.load[_load_case_no].dof = 6
                        _node_1.load[_load_case_no].point.append(_load_node_1)
                        #
                        model.load.functional[_load_case_no].node.append(_node_1.number)
                    # node end 2
                    try:
                        _node_2.load[_load_case_no].point.append(_load_node_2)
                    except KeyError:
                        _node_2.load[_load_case_no] = f2u.Load.Node(_node_2.number, _node_2.number)
                        _node_2.load[_load_case_no].complex = _complex
                        _node_2.load[_load_case_no].type = _type
                        _node_2.load[_load_case_no].dof = 6
                        _node_2.load[_load_case_no].point.append(_load_node_2)
                        #
                        model.load.functional[_load_case_no].node.append(_node_2.number) 
                else:
                    print('fix BELLO2')
            #
            #else:
            #    print('print line'.format(line))
        #
    # print (' ')
    if _user_data:
        model.load.data = _user_data
    #
    print('--- End reading loading from {:} file'
          .format(lfem_file))
    # 
    # print ('end')
    #
    #
    #return model
    #
    #
    # _FEMinput.close()
    #
    #
    #
