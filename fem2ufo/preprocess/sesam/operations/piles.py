#
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
from copy import copy as copy_all


# package imports
#import fem2ufo.model.feclass.f2uFEmodel as femData
import fem2ufo.process.control as process

import fem2ufo.f2u_model.control as f2u

# --------------------------------------------------------------------
#                     Remove piles from FEM file
# --------------------------------------------------------------------
#
def renumbering(_item):
    #
    _number = 0
    for _key, _nod in sorted(_item.items()):
        _number += 1
        _nod.number = _number
    #
    return _item
    #


#
def get_model_by_concept(concept_type, _concepts, 
                         _elements, _nodes):
    """
    """
    #    
    _pelement = {}
    _pnode = {}
    _psections = {}
    _pmaterial = {}
    _concept_temp = {}
    _tobe_deleted = []
    #
    _members = list(set([key for key, _concept in _concepts.items()
                         for _memb_number, case in _concept.role.items()
                         if concept_type in case]))
    #
    #
    for key in _members:
    #for key, _concept in _concepts.items():
        #flag = False
        _concept = _concepts[key]
        for _memb_number, case in _concept.role.items():
            # find concept type
            #if concept_type in case:
            _member = _concept.element[_memb_number]
            _node1 = _member.node[0].number
            _node2 = _member.node[1].number
            
            #flag = True
            _concept.type = concept_type

            # material
            _mat = _member.material[0]
            try:
                _pmaterial[_mat.number]
            except KeyError:
                _pmaterial[_mat.number] = copy_all(_mat)

            # sections
            _sec = _member.section[0]
            try:
                _psections[_sec.number]
            except KeyError:
                _psections[_sec.number] = copy_all(_sec)

            # elements
            #try:
            _pelement[_memb_number] = copy_all(_elements[_memb_number])
            # delete element
            del _elements[_memb_number]
            #except KeyError:
                #print('-->?', _memb_number)
                #_pelement[_memb_number] = copy_all(_member)

            # delete node end 1
            try:
                _pnode[_node1] = copy_all(_nodes[_node1])
                del _nodes[_node1]
            except KeyError:
                pass

            # delete node end 2
            try:
                _pnode[_node2] = copy_all(_nodes[_node2])
                del _nodes[_node2]
            except KeyError:
                pass
            #
        #
        #if flag:
            _concept_temp[key] = copy_all(_concept)
            _tobe_deleted.append(key)
    #
    _tobe_deleted = list(set(_tobe_deleted))
    for _memb in _tobe_deleted:
        try:
            del _concepts[_memb]
        except KeyError:
            continue
    #
    # Create pile model
    model = f2u.F2U_model(concept_type, 3)
    model.set_component()
    model.component.nodes = _pnode
    model.component.elements = _pelement
    model.component.sections = _psections
    model.component.materials = _pmaterial
    model.component.concepts =  _concept_temp
    #
    #
    #
    return model.component
    #
    #


#
def update_sets(_group, _item):
    """
    """
    for key in sorted(_group, key = lambda name: _group[name].number):
        #if  key == '_shim_':
        #    print('-->')
        _set = _group[key]
        _tempset = []
        for memb_number in _set.items:
            try:
                _memb = _item[memb_number]
                _tempset.append(_item[memb_number].number)
            except KeyError:
                continue
                #_tempset.append(memb_number)
        _set.items = _tempset
    #
    #return _group
    #


#
# TODO : this is in soil.py
def sort_group_top_node(_group, _node_toptip, model,
                        _coord='z'):
    """
    """
    _cno = str(_coord).lower()
    _coordinate = {'x': 0, 'y': 1, 'z': 2}

    _dummy = {}
    for _no, _node in sorted(_node_toptip.items()):
        _node[0] = int(_node[0])
        _node[1] = int(_node[1])
        _no1 = model.nodes[_node[0]].get_coordinates()
        _no2 = model.nodes[_node[1]].get_coordinates()

        if abs(_no1[_coordinate[_cno]]) > abs(_no2[_coordinate[_cno]]):
            _node_toptip[_no] = [_node[1], _node[0]]
        #
        _members = {i: {model.elements[i].node[0].number, 
                        model.elements[i].node[1].number}
                    for i in _group[_no]}
        #
        _dummy[_no] = []
        _len = len(_members)
        _nodebase = _node_toptip[_no]
        _noderesidual = {_node_toptip[_no][0]}
        #
        while _len:
            for _memb_no, _memb in sorted(_members.items()):
                if _nodebase[0] in _memb:
                    _dummy[_no].append(_memb_no)
                    #
                    _noderesidual = _memb - _noderesidual
                    _nodebase = list(_noderesidual)
                    del _members[_memb_no]
            # calculate new length
            _len = len(_members)
    # 
    # print('ok')
    return _dummy, _node_toptip


#
def get_piles(model):
    """
    """
    #
    print('    * Processing piles below mudline')
    #
    # Find piles in group then remove them form main model
    # and placed them in pile model
    concept_type = 'pile'
    _concept =  model.component.concepts
    _element = model.component.elements
    _node = model.component.nodes 
    
    model.set_foundation()
    model.foundation.piles = get_model_by_concept(concept_type, _concept, 
                                                  _element, _node)
    
    # update main model sets member's number
    # TODO : this module is causing issues
    # find permanent fix
    #update_sets(model.component.sets, 
    #            model.component.elements)

    # Pile model
    _pile = model.foundation.piles
    _pile.sets = {}
    _number = 0
    _set_total = len(_pile.concepts)
    for _pile_concept in _pile.concepts.values():
        _name = '_pile_' + _pile_concept.name
        _number += 1
        _pile.sets[str(_number)] = f2u.Geometry.Groups(_name, _number)
        _items = [_item for _item in _pile_concept.element.keys()]
        _pile.sets[str(_number)].items = _items
        #print('-->')
        _name2 = 'end_nodes__' + str(_number)
        _set_number = _set_total + _number
        _pile.sets[str(_set_number)] = f2u.Geometry.Groups(_name2, _set_number)
        _element_pile = _pile.elements[_items[0]]
        _pile.sets[str(_set_number)].nodes = [_element_pile.node[0].name]
        _element_pile = _pile.elements[_items[-1]]
        _pile.sets[str(_set_number)].nodes.append(_element_pile.node[1].name)       
    #
    #
    #
    # add pile member's number to the pile group
    #_temp = [_item for _member in _pile.concepts.values()
    #         for _item in _member.element.keys()]
    #
    # create new pile group
    #_pgroup = f2u.Geometry.Groups('_pile', 1)
    #_pgroup.items = list(set(_temp))
    #
    #
    # Create pile sets
    #
    # split pile group in subgroups with members sharing node ends
    #_pgroup, _node_toptip = process.geometry.split_sets(_pgroup.items, 
    #                                                    _pile.elements, 
    #                                                    _pgroup.name)
    # need to add here finding top & bottom node
    #_pgroup, _node_toptip = sort_group_top_node(_pgroup, 
    #                                            _node_toptip, 
    #                                            _pile)
    #    
    #_pile.sets = {}
    #_setnumber = 0
    #
    # element sets
    #for _name, _set in sorted(_pgroup.items()):
        ## print(_name)
        ##_setnumber = int(_name.replace('_pile_', ''))
        #_setnumber = _name.replace('_pile_', '')
        ## _setNo += 1
        #_pile.sets[_setnumber] = f2u.Geometry.Groups(_name, int(_setnumber))
        ##_pile.sets[_setnumber].type = 2
        #_pile.sets[_setnumber].items = _set
    #
    # end nodes group
    #_setnumber = len(_pile.sets)
    #for _name, _set in sorted(_node_toptip.items()):
        #_namenumber = int(_name.replace('_pile_', ''))
        #_setnumber2 = _setnumber + _namenumber
        #_name2 = 'end_nodes__' + str(_namenumber)
        ## print(_name, _name2)
        #_pile.sets[str(_setnumber2)] = f2u.Geometry.Groups(_name2, _setnumber2)
        ##_pile.sets[_setnumber2].type = 1
        #_pile.sets[str(_setnumber2)].nodes = _set
        ##
    #
    #return model
    #
    #print('piles')

#