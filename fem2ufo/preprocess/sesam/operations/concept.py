#
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.process.control as process
import fem2ufo.f2u_model.control as f2u
#

# --------------------------------------------------------------------
#                           SESAM SPECIFIC 
# --------------------------------------------------------------------
#
#
#
class FEMproperty(object):
    """
    SESAM Specific FEM Concept Properties Class
    
    FEMproperty
        |_ number
        |_ type
        |_ role
        |_ parent
        |_ part
        |_ joint
    
    **Parameters**:  
      :number:  integer internal number 
    """
    __slots__ = ('name', 'type', 'role', 'parent', 'part', 'joint',
                 'attribute')
    #
    def __init__(self, IRCON, SCONTYPE=0, SCONROLE=0, IRPARENT=0):
        self.name = IRCON
        self.type = SCONTYPE
        self.role = SCONROLE
        self.parent = IRPARENT
        self.part = []
        self.joint = []
        #
        # def populate(self):
        #    return True


#
class ElementType(object):
    """
    SESAM Specific FEM Element Type
    
    ElementType
        |_ name
        |_ number
        |_ nodetotal
        |_ description
        |_ element
        |_ code
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'nodetotal', 'description', 'code',
                 'element')
    #
    def __init__(self, Name=None, Number=0, Nodes=0,
                 Description='No yet defined', Type='N/A', Code='N/A'):
        #
        self.number = Number
        self.name = Name
        self.nodetotal = Nodes
        self.description = Description
        self.element = Type
        self.code = Code
        #
        #


#
class Hydrodynamics():
    """
    SESAM Specific FEM Concept Hydrodynamic Properties
    
    Hydrodynamics
        |_ name
        |_ number
        |_ flooded
        |_ coefficient 
        |_ items
        |_ sets
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'items', 'sets',
                 'flooded', 'coefficient', 'type')
    #
    def __init__(self, name, number):
        #
        self.number = number
        self.name = name
        self.flooded = 0  # non flooded by default
        self.coefficient = []
        self.items = []
        self.sets = {}
        #


#
# --------------------------------------------------------------------
#                       Concept Manipulation
# --------------------------------------------------------------------
#
#
#
def concept_property(line_in):
    """
    """
    _key = {"selector": r"1",
            "cross_section": r"2",
            "material": r"3",
            "segment": r"4",
            "hydrodynamic": r"5",
            "code_checking": r"6",
            "orientation": r"7",
            "eccentricity": r"8",
            "pile": r"9",
            "N/A": r"10",
            "N/A": r"11"}

    key_word = process.common.find_keyword(line_in, _key)

    return key_word


#
def hydro_atribute(line_in):
    """
    """
    _key = {"flooded": r"500",
            "cdx": r"501",
            "cdy": r"502",
            "cdz": r"503",
            "cmx": r"504",
            "cmy": r"505",
            "cmz": r"506"}

    key_word = process.common.find_keyword(line_in, _key)

    return key_word
#
#
def concept_type(_type_input):
    """
    """
    _type2 = ['undefined', 'joint', 'segment', 'member',
              'pile joint', 'pile segment', 'pile',
              'segmented beam']

    try:
        _type = _type2[_type_input]

    except KeyError:
        _type = 'undefined'

    return _type
#
#
def concept_role(_type_input):
    """
    """
    _type2 = ['undefined', 'stub', 'can', 'cone', 'mid section',
              'chord', 'brace', 'main pile', 'inner pile',
              'conductor pile', 'pile group']

    try:
        _type = _type2[_type_input]

    except KeyError:
        _type = 'undefined'

    return _type
#
#
#
def update_concepts(_elements, _concepts, _concept_temp,
                    _meshsc, _conproptemp, _groups):
    """
    """
    # update concept properties
    for _prop in _concept_temp.values():
        try:
            _noprop = _prop.attribute
            _prop.attribute = _conproptemp[_noprop]
        except AttributeError:
            continue
    #
    # update main concept
    for item in _concepts.values():
        # update properties upper level (i.e. piles)
        try:
            item.attribute[item.name] = _concept_temp[item.number].attribute
        except AttributeError:
            pass
        #
        #if item.number == 15:
        #if 'bm_1115_2067' in item.name.lower():
        #    print('here')
        
        _node_list = []
        _node_dict = {}
        # mesh level
        _items = _concept_temp[item.number].part
        for _mesh_no in _items:
            # update meshed members
            _mesh_items = _meshsc[_mesh_no].part
            _attribute = _concept_temp[_mesh_no]
            for memb_number in _mesh_items:
                _memb = _elements[memb_number]
                #if _memb.number == 15:
                #    print('--> here')
                
                item.element[memb_number] = _memb
                _role = concept_type(_attribute.type)               
                #
                #item.material.append(_memb.material[0])
                #item.section.append(_memb.section[0])
                #item.step.append(_memb.flexible_length)
                #
                item.type = _memb.type.element
                item.role[memb_number] = _role
                _memb.concept = item.name
                #
                _node_dict[_memb.node[0].name] = _memb.node[0]
                _node_dict[_memb.node[1].name] = _memb.node[1]
                #
                #item.node.append(_memb.node[0])
                if not _node_list:
                    _node_list.append(_memb.node[0].name)
                    _node_list.append(_memb.node[1].name)
                    #
                    item.material.append(_memb.material[0])
                    item.section.append(_memb.section[0])
                    item.step.append(_memb.flexible_length)                    
                else:
                    try:
                        _index = _node_list.index(_memb.node[0].name)
                        #if _index:
                        _index += 1
                        _node_list.insert(_index, _memb.node[1].name)
                        #
                        item.material.insert(_index, _memb.material[0])
                        item.section.insert(_index, _memb.section[0])
                        item.step.insert(_index, _memb.flexible_length)                        
                    except ValueError:
                        try:
                            _index = _node_list.index(_memb.node[1].name)
                            _node_list.insert(_index, _memb.node[0].name)
                            #
                            item.material.insert(_index, _memb.material[0])
                            item.section.insert(_index, _memb.section[0])
                            item.step.insert(_index, _memb.flexible_length)
                            
                        except ValueError:
                            _node_list.append(_memb.node[0].name)
                            _node_list.append(_memb.node[1].name)                            
                            #
                            item.material.append(_memb.material[0])
                            item.section.append(_memb.section[0])
                            item.step.append(_memb.flexible_length)                            
                #
                # update properties mesh level
                try:
                    item.properties[memb_number] = _attribute.attribute
                except AttributeError:
                    continue
        #
        for _node in _node_list:
            item.node.append(_node_dict[_node])
        #
        # update end nodes
        #item.node.append(_memb.node[1])
        #
    #
    # update concept groups
    #
    for key, _set in _groups.items():
        _new_set = set()
        for _member_no in _set.items:
            _member = _elements[_member_no]
            if not 'beam' in _member.type.element:
                continue
            _new_set.add(_member.concept)
        _set.concepts = list(_new_set)    
    #
    #
#
def update_cdcm(cdcm_temp, concept):
    """
    cdcm, concept
    """
    #
    # updating cdcm
    _cdcm = {}
    _item = {'x':0, 'y':1, 'z':2}
    for _temp in cdcm_temp.values():
        _cd = [0, 0, 0]
        _cm = [0, 0, 0]
        #
        _name = _temp.name
        _no_cdcm = _temp.number
        _cdcm[_name] = f2u.Metocean.DragMassCoefficient(_name, _no_cdcm)
        # 
        #
        #
        for _coeff in _temp.coefficient:
            
            if 'cd' in _coeff[0]:
                _loc = _coeff[0].replace('cd', '')
                _loc = _loc.strip()
                _cd[_item[_loc]] = _coeff[1]
            
            elif 'cm' in _coeff[0]:
                _loc = _coeff[0].replace('cm', '')
                _loc = _loc.strip()
                _cm[_item[_loc]] = _coeff[1]
            else:
                print('   **warning cdcm data missing')
        #
        _temp.coefficient = {}
        if sum(_cd) != 0 and sum(_cm) != 0:
            _temp.coefficient['cd'] = _cd
            _temp.coefficient['cm'] = _cm
            _ext = _cd
            _ext.extend(_cm)
            _cdcm[_name].set_cdcm(_ext)
    #
    # finding member witin concept
    for _memb in concept.values():
        _flood_flag = False
        _cdcm_flag = False
        for key, _items in _memb.properties.items():
            for _prop in  _items:
                if _prop[0] == 'hydrodynamic':
                    #cdcm_temp[_prop[1]].items.append(key)
                    if _cdcm[_prop[1]].coefficient:
                        _cdcm[_prop[1]].items.append(key)
                        _cdcm_flag = _cdcm[_prop[1]]
                    if cdcm_temp[_prop[1]].flooded == 1.0:
                        _flood_flag = True
                    #
        #
        # reset concepts properties
        _memb.properties = {}
        if _cdcm_flag:
            _memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
            _memb.properties.cdcm = _cdcm_flag
        # flood element
        if _flood_flag:
            try:
                _memb.properties.flooded = 1
            except AttributeError:
                _memb.properties = f2u.Geometry.BeamElementProperty('hydrodynamic')
                _memb.properties.flooded = 1
        #
    #
    return _cdcm
    #


#
#
#
#
def update_truss_element(_truss, _element, _concepts, _group, _hinge):
    """
    """
    # ---------------------------
    # create hinge
    #
    # fixed
    _node_no = len(_hinge) + 1
    _option = 1
    _hinge_name = 'hinge_truss_' + str(_node_no)
    _hinge[_node_no] = f2u.Geometry.Hinges(_hinge_name, _node_no)
    _hinge[_node_no].option = _option
    _hinge[_node_no].fix = [1, 1, 1, 1, 1, 1]
    _fix = _node_no
    #
    # hinged
    _node_no = len(_hinge) + 1
    _option = 1
    _hinge_name = 'hinge_truss_' + str(_node_no)
    _hinge[_node_no] = f2u.Geometry.Hinges(_hinge_name, _node_no)
    _hinge[_node_no].option = _option
    _hinge[_node_no].fix = [1, 1, 1, 1, 0, 0] 
    # ---------------------------
    #
    name_ng = '_truss_'
    new_set_name, new_set_no = process.geometry.add_group(_group, _truss, name_ng)
    _truss_set = _group[new_set_name]
    # split pile group in subgroups with members sharing node ends
    _rem = {_element[_memb].concept for _memb in _truss_set.items}
    _rem = list(_rem)
    _truss_set.concepts = _rem
    
    _list_truss = []
    _counter = 0
    for _itm in _rem:
        _concept = _concepts[_itm]
        _items = [key for key in _concept.element.keys()]
        #
        if len(_items) > 1:
            _truss_group, _truss_node = process.geometry.split_sets(_items, _element,
                                                                    'temp', 'both')            
            
            for _name_m, _node_item in _truss_node.items():
                _nod_1 = _node_item[0]
                _nod_2 = _node_item[1]
                _end_1 = _truss_group[_name_m][0]
                _end_2 = _truss_group[_name_m][-1]
                
                _member1 = _element[_end_1]
                _member2 = _element[_end_2]

                if _member1.node[0].name == _nod_1:
                    _member1.releases.append(_hinge[_node_no])
                    _member1.releases.append(_hinge[_fix])
                    
                else:
                    _member1.releases.append(_hinge[_fix])
                    _member1.releases.append(_hinge[_node_no])

                if _member2.node[0].name == _nod_2:
                    _member2.releases.append(_hinge[_node_no])
                    _member2.releases.append(_hinge[_fix])
                else:
                    _member2.releases.append(_hinge[_fix])
                    _member2.releases.append(_hinge[_node_no])
        #
        else:
            _member1 = _element[_items[0]]
            _member1.releases.append(_hinge[_node_no])
            _member1.releases.append(_hinge[_node_no])
        #
        #
        _concept.releases.append(_hinge[_node_no])
        _concept.releases.append(_hinge[_node_no])
    #
#
#
#
def get_group_compactness(_element, _group, _dt60, _dt80):
    """
    Find circular and open sections compactness

    """
    _memb60 = []
    _memb80 = []
    _noncompact = []
    _slender = []
    _linear = []
    #
    for _memb in _element.values():
    
        if not 'beam' in _memb.type.element:
            continue
    
        elif _memb.section[0].shape == 'general':
            continue
    
        elif 'BAR' in _memb.section[0].shape:
            continue
    
        elif _memb.material[0].Fy == 0:
            _linear.append(_memb.number)
    
        elif _memb.section[0].number in _dt60:
            _memb60.append(_memb.number)
            _memb.section[0].properties.compactness = 'noncompact'
    
        elif _memb.section[0].number in _dt80:
            _memb80.append(_memb.number)
            _memb.section[0].properties.compactness = 'slender'
    
        elif _memb.section[0].shape == 'PIPE':
            _memb.section[0].properties.compactness = 'compact'
    
        # open section
        else:
    
            class_B41a, class_B41b = process.compactness(_memb.section[0].properties,
                                                         _memb.material[0])
    
            if class_B41b == 'noncompact':
                _noncompact.append(_memb.number)
    
            elif class_B41b == 'slender':
                _slender.append(_memb.number)
    
    #
    if _dt60:
        name_ng = 'Dt_60'
        new_set_name, new_set_number = process.geometry.add_group(_group, _memb60, name_ng)
    #
    if _dt80:
        name_ng = 'Dt_80'
        new_set_name, new_set_number = process.geometry.add_group(_group, _memb80, name_ng)
    #
    if _slender:
        name_ng = 'slender_sections'
        new_set_name, new_set_number = process.geometry.add_group(_group, _slender, name_ng)
    #
    if _noncompact:
        name_ng = 'noncompact_sections'
        new_set_name, new_set_number = process.geometry.add_group(_group, _noncompact, name_ng)
    #
    if _linear:
        name_ng = 'zero_Fy'
        new_set_name, new_set_number = process.geometry.add_group(_group, _linear, name_ng)
    #
    #return _group
    #print('stop')
    #
#