#
# Copyright (c) 2009-2020 fem2ufo
# 

# Python stdlib imports
import os
import sys
import math
from itertools import islice

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
#
from fem2ufo.preprocess.sesam.operations.lfem import read_Lfem
import fem2ufo.preprocess.sesam.operations.element_library as sesam_library
import fem2ufo.preprocess.sesam.operations.concept as sesam_concept

#
#
#
#
#
#
def flat_data(fin, line, _item_no, _slice=3):
    """
    """
    # print(line)
    _no_rows = math.ceil(_item_no / 4.0) - 1
    #
    data = list(islice(fin, _no_rows))
    data.insert(0, line)
    
    data2 = []
    for _data in data:
        data2.extend(_data.split())
    
    # delete ID, NFIELD & IRCON
    del data2[0:_slice]
    
    return data2
#
# TODO : code very slow
def read_Tfem(t_fem, joint_angle=1, joint_coplanary=0.01):
    """
    fem2ufo module reads SESAM T.FEM files
    
    **Parameters**:  
      :t_fem:  SESAM T.FEM file (geometry and functional loading)
      :joint_angle : maximum angle tolerance to idenfy Chord elements (1 degree default)
      :joint_coplanary : maximum tolerance to identify if braces are coplanar 
                         as well as normal to chord (0.01 default)
    
    **Returns**:
      fem2ufo FE concept model
    
    model
        |_ data
        |_ nodes
        |_ sections
        |_ elements
        |_ materials
        |_ vectors
        |_ eccentricities
        |_ hinges
        |_ sets
        |_ joints
        |_ boundaries
        |
        |_ concepts
        |_ cdcm
    
    """
    #
    #
    print('')
    print('------------------ READFEM Module ------------------')    
    #
    # read T fem file
    tfem_file = process.common.check_input_file(t_fem, file_extension='fem')

    if not tfem_file:
        print('    ** I/O error : T.FEM file {:} not found'
              .format(t_fem))
        sys.exit()    
    #
    _node_mass = {}
    _mass_matrix = {}
    _section = {}
    _element = {}
    _material = {}
    #_tubular_joint = {}
    _load = {}
    _concept = {}
    _group = {}
    #_boundary = {}
    _hydro = {}
    # temp dictionaries
    _unit_vector = {}
    _eccentricity = {}
    _hinge = {}
    _mesh_sc = {}
    _concept_temp = {}
    _conproptemp = {}
    # Define Flags
    flag1 = 'off'
    #
    _user_data = {}
    _user_data['TFEM'] = f2u.InputUserData()
    _user_data['TFEM'].file_name = tfem_file
    #
    model = f2u.F2U_model(tfem_file, 1)
    model.set_component()
    #
    print('--- Reading T.FEM file')
    #
    # ++++++++++++++++
    #  FIRST PASS
    # ++++++++++++++++    
    print('    * First pass')
    #
    fem_temp = open('femTemp.fem', 'w+')
    #
    _truss = []
    _shim = []
    _spring = []
    _dt60 = []
    _dt80 = []
    _bound_type = ['free', 'fixed', 'prescribed', 'dependent', 'link']
    #
    with open(tfem_file) as fin:
        for line in fin:
            #    Segment line
            keyword = line.split()
            #
            #    Flag word
            if line[0] != ' ':
                flag1 = keyword[0]
            #
            #
            #
            # --------------------------------------------------------------------
            #                           Read User Data
            # --------------------------------------------------------------------
            #
            if 'DATE:' in line.upper():
                try:
                    _user_data['TFEM'].date = str(keyword[1])
    
                except TypeError:
                    _user_data['TFEM'] = f2u.InputUserData()
                    _user_data['TFEM'].date = str(keyword[1])
            #
            elif 'USER:' in line.upper():
                try:
                    _user_data['TFEM'].user = str(keyword[1])
    
                except TypeError:
                    _user_data['TFEM'] = f2u.InputUserData()
                    _user_data['TFEM'].user = str(keyword[1])
            #
            elif 'PROGRAM:  SESAM GENIE' in line.upper():
                try:
                    _user_data['TFEM'].version1 = str(keyword[4])
                    _user_data['TFEM'].program = 'GENIE'
    
                except TypeError:
                    _user_data['TFEM'] = f2u.InputUserData()
                    _user_data['TFEM'].version1 = str(keyword[4])
                    _user_data['TFEM'].program = 'GENIE'
            #
            #
            elif 'PROGRAM:  SESAM GAMESHA' in line.upper():
                try:
                    _user_data['TFEM'].version2 = str(keyword[4])
    
                except TypeError:
                    _user_data['TFEM'] = f2u.InputUserData()
                    _user_data['TFEM'].version2 = str(keyword[4])
            #
            #
            # ----------------------------------------------------------------------
            #                             Read Nodes
            # ----------------------------------------------------------------------
            #
            elif 'GNODE' in line:
                _node_name = str(int(float(keyword[1])))
                _node_number = int(float(keyword[2]))
                #_node[_node_number] = f2u.Geometry.Node(_node_name, _node_number)
                model.component.add_node(_node_number, _node_number)
                #model.component.nodes[_node_number].name = _node_name
            #
            # ----------------------------------------------------------------------
            #                             Read Elements
            # ----------------------------------------------------------------------
            #
            elif 'GELMNT1' in line:
                memb_name = str(int(float(keyword[1])))
                memb_number = int(float(keyword[2]))
                _elem_type = int(float(keyword[3]))
                _elem_type_add = int(float(keyword[4]))
                _type = sesam_library.element_type[_elem_type]
                #
                if 'mass' in _type.element:
                    #print('mass')
                    _data = list(islice(fin, 1))
                    _data = _data[0].strip()
                    try:
                        _node_number = int(float(_data))
                        _node = model.component.nodes[_node_number]
                        _node_mass[memb_number] = _node_number
                        #try:
                        #    _node_mass[memb_number].append(_node_number)
                        #except KeyError:
                        #    _node_mass[memb_number] = [_node_number]
                    except KeyError:
                        continue
                else:
                    #
                    if 'beam' in _type.element:
                        _element[memb_number] = f2u.Geometry.BeamElement(memb_name,
                                                                         memb_number)
                    #
                    elif 'truss' in _type.element:
                        _element[memb_number] = f2u.Geometry.BeamElement(memb_name,
                                                                         memb_number)
                        _truss.append(memb_number)
                    #
                    elif 'spring' in _type.element:
                        _element[memb_number] = f2u.Geometry.SpringElement(memb_name,
                                                                           memb_number)
                    #
                    elif 'shell' in _type.element or 'plate' in _type.element or 'membrane' in _type.element.lower():
                        _element[memb_number] = f2u.Geometry.ShellElement(memb_name,
                                                                          memb_number)
                    #
                    else:                
                        print('no beam')
                    #
                    _element[memb_number].type = sesam_library.element_type[_elem_type]
                    _total_node = _element[memb_number].type.nodetotal
                    
                    _no_rows = math.ceil(_total_node / 4.0)
                    data = list(islice(fin, _no_rows))
        
                    #else:
                    for _data in data:
                        _data = _data.split()
                        for _item in _data:
                            try:
                                _node_number = int(float(_item))
                                _element[memb_number].node.append(model.component.nodes[_node_number])
                                model.component.nodes[_node_number].elements.append(memb_number)
                            except KeyError:
                                break
            #
            #
            # ----------------------------------------------------------------------
            #                            Read UNIVECTORS
            # ----------------------------------------------------------------------
            #
            elif 'GUNIVEC' in line:
                _unit_vector_no = int(float(keyword[1]))
                _unit_vector[_unit_vector_no] = f2u.Geometry.GuidePoint(_unit_vector_no, 
                                                                        _unit_vector_no)
                _unit_vector[_unit_vector_no].cos.append(float(keyword[2]))
                _unit_vector[_unit_vector_no].cos.append(float(keyword[3]))
                _unit_vector[_unit_vector_no].cos.append(float(keyword[4]))
                _unit_vector[_unit_vector_no].type = 'beam'
            #
            elif 'BNTRCOS' in line:
                _unit_vector_no = int(float(keyword[1]))
                _unit_vector[_unit_vector_no] = f2u.Geometry.GuidePoint(_unit_vector_no, 
                                                                        _unit_vector_no)
                _unit_vector[_unit_vector_no].cos.append(float(keyword[2]))
                _unit_vector[_unit_vector_no].cos.append(float(keyword[3]))
                _unit_vector[_unit_vector_no].cos.append(float(keyword[4]))
                _unit_vector[_unit_vector_no].type = 'matrix'
                
                data = list(islice(fin, 2))
                #
                for _data in data:
                    _data = _data.split()
                    for _no_x in _data:
                        _unit_vector[_unit_vector_no].cos.append(float(_no_x))
            #
            # ----------------------------------------------------------------------
            #                          Read Eccentricities
            # ----------------------------------------------------------------------
            #
            elif 'GECCEN' in line:
                _eccentnumber = int(float(keyword[1]))
                _eccentricity[_eccentnumber] = f2u.Geometry.Eccentricities(_eccentnumber, 
                                                                           _eccentnumber)
                _eccentricity[_eccentnumber].eccentricity.append(float(keyword[2]))
                _eccentricity[_eccentnumber].eccentricity.append(float(keyword[3]))
                _eccentricity[_eccentnumber].eccentricity.append(float(keyword[4]))
            # 
            # ----------------------------------------------------------------------
            #                            Read Materials
            # ----------------------------------------------------------------------
            #
            elif 'MISOSEL' in line:
                _mat_no = (int(float(keyword[1])))
                _mat_name = str(_mat_no)
                _material[_mat_name] = f2u.Material(_mat_name, _mat_no)
                
                _material[_mat_name].set_properties(E=keyword[2],
                                                    poisson=keyword[3],
                                                    density=keyword[4])
                
                data = list(islice(fin, 1))
                data = data[0].split()
                #
                _material[_mat_name].damping = float(data[0])
                _material[_mat_name].alpha = float(data[1])
                _material[_mat_name].Fy = float(data[3])
                _material[_mat_name].type = 'Plastic'
                if _material[_mat_name].Fy == 0:
                    _material[_mat_name].type = 'Elastic'
            #
            # General 2noded Spring/Shim Element
            elif flag1 == 'MSHGLSP':
                #if keyword[0] == 'MSHGLSP':
                # print('fix this', line)
                _mat_no = int(float(keyword[1]))
                _mat_name = str(_mat_no)
                _type = 'shim'
                if float(keyword[2]) == 2:
                    _type = 'spring'
                    _spring.append(_mat_name)
                else:
                    _shim.append(_mat_name)

                _material[_mat_name] = f2u.Material(_mat_name, _mat_no)
                _material[_mat_name].type = _type
                _dof1 = int(float(keyword[3]))
                _dof2 = int(float(keyword[4]))
                _tdof = _dof1
                _matrix_1 = process.mathmet.zeros(_dof1, _dof1)
                _matrix_2 = process.mathmet.zeros(_dof2, _dof2)
                _material[_mat_name].stiffness = [_matrix_1, _matrix_2]
                # print('????', _dof1)
                i = 0
                j = 0
                k = 0
                count = 0
                _item_no = (((_dof1 + _dof2) - 1) * ((_dof1 + _dof2)/2.0)
                            + (_dof1 + _dof2))
                data = flat_data(fin, line, _item_no + 5, 5)                    
                #
                #else:
                # print('fix this',i,j, line)
                # print(keyword)
                for _no_x in range(len(data)):
                    if count == _dof1 + _dof2:
                        k = 0
                        i += 1
                        j = i
                        _tdof = _dof1
                        count = i

                    if j == _tdof:
                        j = 0
                        k = 1
                        _tdof = _dof2

                    if i == _tdof:
                        # i = 0
                        # k = 1
                        continue

                    _material[_mat_name].stiffness[k][j][i] = float(data[_no_x])
                    j += 1
                    count += 1
            #
            # spring element to Ground
            elif 'MGSPRNG' in line:
                #if keyword[0] == 'MGSPRNG':
                _mat_no = int(float(keyword[1]))
                _mat_name = str(_mat_no)
                _type = 'spring to ground'
                _material[_mat_name] = f2u.Material(_mat_name, _mat_no)
                _material[_mat_name].type = _type
                # _material[_matNo].dummy = 'diagonal'
                _tdof = int(float(keyword[2]))
                _material[_mat_name].stiffness = process.mathmet.zeros(_tdof, _tdof)
                
                _item_no = (_tdof - 1) * (_tdof/2.0) + _tdof
                data = flat_data(fin, line, _item_no)
                
                i, j = 0, 0
                for _no_x in range(len(data)):
                    if j == _tdof:
                        i += 1
                        j = i
                    _material[_mat_name].stiffness[j][i] = float(data[_no_x])
                    _material[_mat_name].stiffness[i][j] = float(data[_no_x])
                    j += 1
            #
            # ----------------------------------------------------------------------
            #                        Read Degree of Fixation
            # ----------------------------------------------------------------------
            #
            elif 'BELFIX' in line:
                #if keyword[0] == 'BELFIX':
                _fix_no = int(float(keyword[1]))
                _option = float(keyword[2])
                #_trano = float(keyword[3])
                _hinge_name = 'hinge_' + str(_fix_no)
                _hinge[_fix_no] = f2u.Geometry.Hinges(_hinge_name, _fix_no)
                _hinge[_fix_no].option = _option
                #_hinge[_fix_no].trano = _trano
                _temp_list = []
                
                data = list(islice(fin, 2))
    
                #
                for _data in data:
                    _data = _data.split()
                    for _no_x in _data:
                        _temp_list.append(float(_no_x))

                _hinge[_fix_no].fix = _temp_list
            #
            # ----------------------------------------------------------------------
            #                     Read Cross Sections Properties
            # ----------------------------------------------------------------------
            #
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ General Beam ~~~~~~~~~~~~~~~~~~~~~~~~~~
            #  
            elif 'GBEAMG' in  line:
                #if keyword[0] == 'GBEAMG':
                # 
                _geom_no = int(float(keyword[1]))
                area = float(keyword[3])
                _ix = float(keyword[4])
                #_rows = 0
                #
                _sect_type = 'GENERAL SECTION'
                _name = 'GeneralBeam_' + str(_geom_no)
                _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no,
                                                          'beam', 'general')
                # _section[_geomNo].name = ('GeneralBeam_' + str(_geomNo))
                _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                # _section[_geomNo].properties.units(_forceUnit, _lengthUnit)
                #
                data = list(islice(fin, 3))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                #
                _iy = float(_rows[0])
                _iz = float(_rows[1])
                _iyz = float(_rows[2])
                _wxmin = float(_rows[3])

                #elif _rows == 2:
                _rows = data[1].split()
                #
                wymin = float(_rows[0])
                wzmin = float(_rows[1])
                shary = float(_rows[2])
                sharz = float(_rows[3])

                #elif _rows == 3:
                _rows = data[2].split()
                #
                _shceny = float(_rows[0])
                _shcenz = float(_rows[1])
                _sy = float(_rows[2])
                _sz = float(_rows[3])

                _section[_geom_no].properties.general_section_data(area, _ix, _iy, _iz, _iyz,
                                                                   _wxmin, wymin, wzmin,
                                                                   shary, sharz, _shceny,
                                                                   _shcenz, _sy, _sz)
            #
            #
            # ~~~~~~~~~~~~~~~~~~~~~  Shell Geometry ~~~~~~~~~~~~~~~~~~~~~ 
            #
            elif 'GELTH' in line:
                _sect_type = 'PLATE'
                _geom_no = int(float(keyword[1]))
                _name = 'Shell_' + str(_geom_no)
                _th = float(keyword[2])

                _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'shell', 'plate')
                _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                _section[_geom_no].properties.input_data(_th)
            #
            #
            # ----------------------------------------------------------------------
            #                                 Read Sets
            # ----------------------------------------------------------------------
            #
            elif 'TDSETNAM' in  line:
                _set_no = int(float(keyword[2]))
                _group[_set_no] = f2u.Geometry.Groups(_set_no, _set_no)
                
                data = list(islice(fin, 1))
                _data = data[0].split()
                _group[_set_no].name = str(_data[0])
            #
            #
            # ----------------------------------------------------------------------
            #                        Read Structural Concept 
            # ----------------------------------------------------------------------
            #
            elif 'TDSCONC' in line:
                concept_number = int(float(keyword[2]))
                _codnam = float(keyword[3])
                _codtext = float(keyword[4])
                #
                data = list(islice(fin, 1))
                _data = data[0].split()
                #else:
                concept_name = str(_data[0])
                _concept[concept_name] = f2u.Concept(concept_name, 
                                                     concept_number)
                #_concept[concept_name].codname = _codnam
                #_concept[concept_name].codtext = _codtext                
                #_concept[concept_name].name = str(_data[0])
            #
            #
            elif 'SCONCEPT' in line:
                _item_no = int(float(keyword[1]))
                _scon_no = int(float(keyword[2]))
                _scon_type = int(float(keyword[3]))
                _scon_role = int(float(keyword[4]))
                # 
                _concept_temp[_scon_no] = sesam_concept.FEMproperty(_scon_no, 
                                                              _scon_type, 
                                                              _scon_role)
                
                #
                data = flat_data(fin, line, _item_no, 5)
                #
                # Parent concept reference number
                _ir_parent = int(float(data[0]))
                _concept_temp[_scon_no].parent = _ir_parent
                data.pop(0)
                #
                while data:
                    try:
                        # Number of part concepts
                        _part_no = int(float((data[0])))
                        data.pop(0)
                        # Number of joint (connection) concepts
                        _joint_no = int(float((data[0])))
                        data.pop(0)
                    
                    except IndexError:
                        break
                    
                    #for x in range(_part_no):
                    _items = [int(float(y)) for y in data[0:_part_no]]
                    del data[0:_part_no]                    
                    _concept_temp[_scon_no].part.extend(_items)
                    
                    _items = [int(float(y)) for y in data[0:_joint_no]]
                    del data[0:_joint_no]                
                    _concept_temp[_scon_no].joint.extend(_items)
                #
            #
            #
            elif 'SCONMESH' in line:
                #if keyword[0] == 'SCONMESH':
                #
                # NFIELD
                _item_no = int(float(keyword[1]))
                # IRCON Concept reference number (unique)
                _mesh_sc_no = int(float(keyword[2]))
                # Create object
                _mesh_sc[_mesh_sc_no] = sesam_concept.FEMproperty(_mesh_sc_no)
                #
                data = flat_data(fin, line, _item_no)
                #
                while data:
                    try:
                        1.0 /float(data[0])
                    except ZeroDivisionError:
                        break
                    # Number of Finite Element representation types
                    _num_rep = int(float(data[0]))
                    data.pop(0)
                    for x in range(_num_rep):
                        # FE representation type:
                        #     1.- Node: Refers internal node number (GNODE)
                        #     2.- Element: Refers internal element number (GELMNT1)
                        _type_rep = int(float(data[0]))
                        data.pop(0)
                        # Number of FE representations
                        _nfe_rep = int(float(data[0]))
                        data.pop(0)
                        
                        _items = [int(float(y)) for y in data[0:_nfe_rep]]
                        del data[0:_nfe_rep]
                        
                        if _type_rep == 2:
                            _mesh_sc[_mesh_sc_no].part.extend(_items)
                        else:
                            _mesh_sc[_mesh_sc_no].joint.extend(_items)
            #
            elif 'SPROHYDR' in line:
                #if keyword[0] == 'SPROHYDR':
                _item_no = int(float(keyword[1]))
                irpattr = int(float(keyword[2]))
                _hydro[irpattr] = sesam_concept.Hydrodynamics(irpattr, 
                                                              irpattr)
                _hydro[irpattr].type = 'specified'                 
                
                data = flat_data(fin, line, _item_no)
                
                while data:
                    # Number of attributes for current property
                    _num_rep = int(float((data[0])))
                    data.pop(0)
                    
                    for x in range(_num_rep):
                        # Attribute number (ref. value table below)
                        try:
                            _type_rep = (int(float(data[0])))
                            _test = 1.0 / _type_rep
                            data.pop(0)
                        except ZeroDivisionError:
                            break
                        
                        typeattr = sesam_concept.hydro_atribute(str(_type_rep))                       
                        
                        # Attribute value
                        value = float(data[0])
                        data.pop(0)
                        
                        if typeattr == 'flooded':
                            _hydro[irpattr].flooded = value
                        else:
                            _hydro[irpattr].coefficient.append((typeattr, value))                        
                #
            #
            # ----------------------------------------------------------------------
            #                           Read Load Name
            # ----------------------------------------------------------------------
            elif 'TDLOAD' in line:
                _load_number = int(float(keyword[2]))
                _dummy = float(keyword[3])
                _load[_load_number] = f2u.Load.Basic(_load_number,
                                                         _load_number)
                #
                data = list(islice(fin, 1))
                _data = data[0].split()
                _load[_load_number].name = str(_data[0])
            #
            #
            else:
                fem_temp.write(line)
                # print('print line')
                #
    #
    fem_temp.close()
    # 
    #
    # ++++++++++++++++
    #  SECOND PASS
    # ++++++++++++++++
    #
    print('    * Second pass')
    #
    fem_temp = open('femTemp2.fem', 'w+')
    #
    with open('femTemp.fem') as fin:
        for line in fin:
            #
            # --------------------------------------------------------------------
            #
            # --------------------------------------------------------------------
            #    Segment line
            keyword = line.split()
            #    Flag word
            if line[0] != ' ':
                flag1 = keyword[0]
            #
            #
            # ----------------------------------------------------------------------
            #                             Read Nodes
            # ----------------------------------------------------------------------
            #        
            if 'GCOORD' in line:
                _node_number = int(float(keyword[1]))
                model.component.nodes[_node_number].set_coordinates(float(keyword[2]), 
                                                                    float(keyword[3]), 
                                                                    float(keyword[4]))
            #
            # --------------------------------------------------------------------
            #                     Read Boundary Conditions
            # --------------------------------------------------------------------
            #
            elif 'BNBCD' in line: 
                _node_number = int(float(keyword[1]))
                model.component.add_boundary(name=_node_number,
                                             node_name=_node_number)
                _fixity = []
                _fixity.append(float(keyword[3]))
                _fixity.append(float(keyword[4]))
                
                data = list(islice(fin, 1))
                data = data[0].split()
                
                for _no_x in data:
                    _fixity.append(float(_no_x))
                
                #
                model.component.boundaries[_node_number].set_releases(_fixity)
                _type = int(max(_fixity))
                _type = _bound_type[_type]
                model.component.boundaries[_node_number].type = _type
            #
            elif 'BLDEP' in line:
                _node_slave = int(float(keyword[1]))
                _node_master = int(float(keyword[2]))
                _nddof = int(float(keyword[3]))
                _ndep = int(float(keyword[4]))
                if not _nddof:
                    _nddof = _ndep
                # try:
                #model.component.boundaries[_node_master].slave.append(model.component.boundaries[_node_slave])
                #model.component.boundaries[_node_master].master = model.component.nodes[_node_master]
                #
                try:
                    model.component.boundaries[_node_master]
                except KeyError:
                    model.component.add_boundary(name=_node_master,
                                                 node_name=_node_master)
                    model.component.nodes[_node_master].boundary = []
                #
                model.component.boundaries[_node_master].type = 'master'
                model.component.boundaries[_node_master].slave.append(_node_slave)
                # Slave
                try:
                    model.component.boundaries[_node_slave]
                except KeyError:
                    model.component.add_boundary(name=_node_slave,
                                                 node_name=_node_slave)
                #
                model.component.boundaries[_node_slave].master = _node_master
                model.component.boundaries[_node_slave].link =  model.component.boundaries[_node_slave].releases
                #
                model.component.boundaries[_node_slave].releases = []
                model.component.nodes[_node_slave].boundary = [_node_slave]
                
                data = list(islice(fin, _ndep))
                # else:
                # FIXME: Not test it yet
                #
                model.component.boundaries[_node_slave].dependence = [0, 0, 0, 0, 0, 0]
                for _data in data:
                    _data = _data.split()
                    _contribution = float(_data[2]) * 1e+10
                    if _contribution != 0:
                        _dof = int(float(_data[1]))
                        #_dependency = int(float(_data[0]))
                        model.component.boundaries[_node_slave].dependence[_dof-1] = _contribution
                        #
                    #for _item in _data:
                    #    _item
                    #    model.component.nodes[_node_slave].boundary.append(_data)
            # FIXME
            elif 'BQDP' in flag1 :
                print('BQDP --> need to fix this card')
                if keyword[0] == 'BQDP':
                    pass
                else:
                    pass
            #
            # FIXME
            elif 'BNDOF' in flag1:
                _node_number = int(float(keyword[1]))
                _displacement = int(float(keyword[2]))
                _rotation = int(float(keyword[3]))
                model.component.nodes[_node_number].rotation = _rotation
                #print('BNDOF --> need to fix this card')
                _unit_vector[_rotation].nodes.append(model.component.nodes[_node_number])
            #
            # --------------------------------------------------------------------
            #                      Read Node Point Masses
            # --------------------------------------------------------------------
            #        
            elif 'BNMASS' in line:
                #if keyword[0] == 'BNMASS':
                _node_number = int(float(keyword[1]))
                _ndof = int(float(keyword[2])) - 2
                model.component.nodes[_node_number].mass.append(float(keyword[3]))
                model.component.nodes[_node_number].mass.append(float(keyword[4]))
                
                _no_rows = math.ceil(_ndof / 4.0)
                data = list(islice(fin, _no_rows))
                #else:
                for _data in data:
                    _data = _data.split()
                    for _item in _data:                
                    #for _no_x in range(4):
                        try:
                            model.component.nodes[_node_number].mass.append(float(_item))
                        except:
                            break
            #
            elif 'MGMASS' in line:
                # print('**** fix this generic mass input')
                _mat_name = int(float(keyword[1]))
                _tdof = int(float(keyword[2]))
                _mass_matrix[_mat_name] = process.mathmet.zeros(_tdof, _tdof)
                #
                #
                _item_no = (_tdof - 1) * (_tdof/2.0) + _tdof
                data = flat_data(fin, line, _item_no)                
                #
                i, j = 0, 0
                for _no_x in range(len(data)):
                    if j == _tdof:
                        i += 1
                        j = i
                    _mass_matrix[_mat_name][j][i] = float(data[_no_x])
                    _mass_matrix[_mat_name][i][j] = float(data[_no_x])
                    j += 1
            #
            # ----------------------------------------------------------------------
            #                          Read Materials
            # ----------------------------------------------------------------------
            #
            elif 'TDMATER' in line:
                _mat_no = int(float(keyword[2]))
                _mat_name = str(_mat_no)
                data = list(islice(fin, 1))
                _data = data[0].split()
                _material[_mat_name].name = str(_data[0])
            # 
            # ----------------------------------------------------------------------
            #                        Read Section Name   
            # ----------------------------------------------------------------------
            #
            elif 'TDSECT' in line:
                _geom_no = int(float(keyword[2]))
                #
                data = list(islice(fin, 1))
                _data = data[0].split()
                try:
                    _section[_geom_no].name = str(_data[0])
                
                except KeyError:
                    continue
            # 
            # ----------------------------------------------------------------------
            #                     Read Cross Sections Properties
            # ----------------------------------------------------------------------
            #
            # Massive Bar 
            elif 'GBARM' in line:
                #if keyword[0] == 'GBARM':
                #
                _sect_type = 'RECTANGULAR BAR'
                _geom_no = int(float(keyword[1]))
                _hz = float(keyword[2])
                _bt = float(keyword[3])
                _bb = float(keyword[4])

                try:
                    # _section[_geomNo].name = 'SquareBar_' + str(_geomNo)
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'SQUARE BAR'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _name = 'SquareBar_' + str(_geom_no)
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'square_bar')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                #    
                _section[_geom_no].properties.input_data(_hz, _bt, _bb)
                #
                data = list(islice(fin, 1))
    
                #else:
                _rows = data[0].split()
                #
                _section[_geom_no].SFV = float(_rows[0])
                _section[_geom_no].SFH = float(_rows[1])
            #
            # Box Section
            elif 'GBOX' in line:
                #if keyword[0] == 'GBOX':
                _sect_type = 'BOX'
                _geom_no = int(float(keyword[1]))
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _tb = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'BOX'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _name = 'Box_' + str(_geom_no)
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'box')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                
                #
                data = list(islice(fin, 1))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                #
                _tt = float(_rows[0])
                _by = float(_rows[1])

                _section[_geom_no].properties.input_data(_hz, _ty, _by, _tt, _by, _tb)
                _section[_geom_no].SFV = float(_rows[2])
                _section[_geom_no].SFH = float(_rows[3])
                #               
            #
            # Channel Section 
            elif 'GCHAN' in line:
                #if keyword[0] == 'GCHAN':
                _sect_type = 'CHANNEL'
                _geom_no = int(float(keyword[1]))
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _by = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'CHANNEL'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _name = 'Channel_' + str(_geom_no)
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'channel')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                #
                data = list(islice(fin, 2))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                #
                _tz = float(_rows[0])
                _section[_geom_no].SFV = float(_rows[1])
                _section[_geom_no].SFH = float(_rows[2])

                #elif _rows == 2:
                _rows = data[1].split()
                #
                _kk = float(_rows[0])
                _section[_geom_no].properties.input_data(_hz, _ty, _by, _tz, _kk)
                #
            # Channel Section with inside curvature 
            elif 'GCHANR' in line:
                #if keyword[0] == 'GCHANR':
                _sect_type = 'CHANNEL'
                _geom_no = int(float(keyword[1]))
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _by = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'CHANNEL'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _name = 'Channel_' + str(_geom_no)
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'channel')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                #
                data = list(islice(fin, 2))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                #
                _tz = float(_rows[0])
                _section[_geom_no].SFV = float(_rows[1])
                _section[_geom_no].SFH = float(_rows[2])

                #elif _rows == 2:
                _rows = data[1].split()
                #
                _kk = float(_rows[0])
                _rr = float(_rows[1])
                _section[_geom_no].properties.input_data(_hz, _ty, _by, _tz, _kk, _rr)
                #
            # Section Type Double Bottom
            elif 'GDOBO' in line:
                #if keyword[0] == 'GDOBO':
                _sect_type = 'I SECTION'
                _geom_no = int(float(keyword[1]))
                #
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _by = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'DOUBLE_BOTTOM'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _name = ('DoubleBottom_' + str(_geom_no))
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'double_bottom')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                #
                data = list(islice(fin, 1))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                #
                _tt = float(_rows[0])
                _tb = float(_rows[1])

                _section[_geom_no].properties.input_data(_hz, _ty, _by, _tt, _tb)
                _section[_geom_no].SFV = float(_rows[2])
                _section[_geom_no].SFH = float(_rows[3])
                #
            #
            # I Beam Section 
            elif 'GIORH' in line:
                #if keyword[0] == 'GIORH':
                _sect_type = 'I SECTION'
                _geom_no = int(float(keyword[1]))
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _bt = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'IBEAM'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'ibeam')
                    _section[_geom_no].name = 'Ibeam_' + str(_geom_no)
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                # 
                data = list(islice(fin, 2))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                #
                _tt = float(_rows[0])
                _bb = float(_rows[1])
                _tb = float(_rows[2])

                _section[_geom_no].properties.input_data(_hz, _ty, _bt, _tt, _bb, _tb)
                _section[_geom_no].SFV = float(_rows[3])

                #elif _rows == 2:
                _rows = data[1].split()
                #
                _section[_geom_no].SFH = float(_rows[0])
            #
            # I Beam Section with inside curvature
            elif 'GIORHR' in line:
                #if keyword[0] == 'GIORHR':
                _sect_type = 'I SECTION'
                _geom_no = int(float(keyword[1]))
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _bt = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'IBEAM'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _name = 'Ibeam_' + str(_geom_no)
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'ibeam')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                # 
                data = list(islice(fin, 2))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                #
                _tt = float(_rows[0])
                _bb = float(_rows[1])
                _tb = float(_rows[2])
                _section[_geom_no].SFV = float(_rows[3])

                #elif _rows == 2:
                _rows = data[1].split()
                #
                _rt = float(_rows[1])
                _rb = float(_rows[2])
                _section[_geom_no].properties.input_data(_hz, _ty, _bt, _tt, _bb, _tb, _rt, _rb)
                _section[_geom_no].SFH = float(_rows[0])
                #
            #
            # L Section
            elif 'GLSEC' in line:
                #if keyword[0] == 'GLSEC':
                _sect_type = 'ANGLE'
                _geom_no = int(float(keyword[1]))
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _by = float(keyword[2])
                _tb = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'ANGLE'
                    _section[_geom_no].properties.SectionType = _sect_type
                except:
                    _name = 'Angle_' + str(_geom_no)
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'angle')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                # 
                data = list(islice(fin, 1))
                _rows = data[0].split()
                #
                _tz = float(_rows[0])
                _kk = float(_rows[3])

                _section[_geom_no].properties.input_data(_hz, _ty, _by, _tz, _kk)
                _section[_geom_no].SFV = float(_rows[1])
                _section[_geom_no].SFH = float(_rows[2])
                #
            # L Section with inside curvature
            elif 'GLSECR' in line:
                #if keyword[0] == 'GLSECR':
                _sect_type = 'ANGLE'
                _geom_no = int(float(keyword[1]))
                #
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _tb = float(keyword[4])
                #
                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'ANGLE'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _name = 'Angle_' + str(_geom_no)
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'angle')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                # 
                data = list(islice(fin, 2))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                #
                _tz = float(_rows[0])
                _kk = float(_rows[3])
                _section[_geom_no].SFV = float(_rows[1])
                _section[_geom_no].SFH = float(_rows[2])

                #elif _rows == 2:
                _rows = data[1].split()
                #
                _rr = float(_rows[0])
                _section[_geom_no].properties.input_data(_hz, _ty, _by, _tz, _kk, _rr)
            #
            # Pipe Section 
            elif 'GPIPE' in line:
                #if keyword[0] == 'GPIPE':
                _sect_type = 'TUBULAR'
                _geom_no = int(float(keyword[1]))
                _dy = float(keyword[3])
                _thk = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'PIPE'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'pipe')
                    _section[_geom_no].name = 'Pipe_' + str(_geom_no)
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                #
                _section[_geom_no].properties.input_data(_dy, _thk)

                if _dy / _thk > 80.0:
                    _dt80.append(_geom_no)

                elif _dy / _thk > 60.0:
                    _dt60.append(_geom_no)
                
                #
                data = list(islice(fin, 1))
    
                #else:
                _rows = data[0].split()
                _section[_geom_no].SFV = float(_rows[0])
                _section[_geom_no].SFH = float(_rows[1])
            #
            # Cross Section T on Plate
            elif 'GTONP' in line:
                #if keyword[0] == 'GTONP':
                _sect_type = 'I SECTION'
                _geom_no = int(float(keyword[1]))
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _bt = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'T_PLATED'
                    _section[_geom_no].properties.SectionType = _sect_type

                except:
                    _name = 'TonPlate_' + str(_geom_no)
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 't_plated')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                # 
                data = list(islice(fin, 2))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                #
                _tt = float(_rows[0])
                _bp = float(_rows[1])
                _tp = float(_rows[2])
                _section[_geom_no].properties.input_data(_hz, _ty, _bt, _tt, _bp, _tp)
                _section[_geom_no].SFV = float(_rows[3])

                #elif _rows == 2:
                _rows = data[1].split()
                _section[_geom_no].SFH = float(_rows[0])
                #                
            #
            # Unsymetrical I Beam Section
            elif 'GUSYI' in line:
                #if keyword[0] == 'GUSYI':
                _sect_type = 'OPEN THIN WALLED'
                _geom_no = int(float(keyword[1]))
                _hz = float(keyword[2])
                _ty = float(keyword[3])
                _bt = float(keyword[4])

                try:
                    _section[_geom_no].type = 'BEAM'
                    _section[_geom_no].shape = 'UIBEAM'
                    _section[_geom_no].properties.SectionType = _sect_type
                except:
                    _name = 'UnsymetricalBeam_' + str(_geom_no)
                    _section[_geom_no] = f2u.Geometry.Section(_name, _geom_no, 'beam', 'uibeam')
                    _section[_geom_no].properties = f2u.SecProp.SectionProperty(_sect_type)
                # 
                data = list(islice(fin, 2))
    
                #else:
                #if _rows == 1:
                _rows = data[0].split()
                _b1 = float(_rows[0])
                _tt = float(_rows[1])
                _bb = float(_rows[2])
                _b2 = float(_rows[3])

                #elif _rows == 2:
                _rows = data[1].split()
                #
                _tb = float(_rows[0])
                _section[_geom_no].properties.input_data(_hz, _ty, _bt, _tt, _bb, _tb, _b1, _b2)
                _section[_geom_no].SFV = float(_rows[1])
                _section[_geom_no].SFH = float(_rows[2])
                #
            #
            #
            # ----------------------------------------------------------------------
            #                     Read Joints Modelled in GeniE
            # ----------------------------------------------------------------------
            #
            elif 'TDNODE' in line:
                _joint_number = int(float(keyword[2]))
                model.component.add_joint(name = _joint_number, 
                                          node_name = _joint_number)
                
                _data = list(islice(fin, 1))
                _data = _data[0].split()
                model.component.joints[_joint_number].name = str(_data[0])
            # 
            #
            # ----------------------------------------------------------------------
            # FIXME: this module very ineficient
            # ----------------------------------------------------------------------
            #
            elif 'GELREF1' in line:
                _rows_no = 0
                memb_number = int(float(keyword[1]))
                _mat_no = int(float(keyword[2]))
                _mat_name = str(_mat_no)
                try:
                    _mat = _material[_mat_name]
                except KeyError:
                    # check if mass element
                    try:
                        _mass = _mass_matrix[_mat_no]
                        _node_number = _node_mass[memb_number]
                        _node = model.component.nodes[_node_number]
                        _node.mass = [_mass[0][0], _mass[1][1], _mass[2][2],
                                      _mass[3][3], _mass[4][4], _mass[5][5]]
                        continue
                    except KeyError:
                        print('    ** Warning no material defined for element {:}'
                              .format(memb_number))
                        del _element[memb_number]
                        flag1 = ''
                        continue

                #_add_no = int(float(keyword[3]))
                #_int_no = int(float(keyword[4]))
                _element[memb_number].material.append(_material[_mat_name])
                _total_node = _element[memb_number].type.nodetotal
                _temp = []
                #
                _mat.elements.append(memb_number)
                #
                data = list(islice(fin, 2))
                
                #
                _rows = data[0].split()
                
                # strain/stress section [to be implemented]
                for _items in _rows:
                    _element[memb_number].stress.append(int(float(_items)))
                    
                #
                #elif _rows == 2:
                _rows = data[1].split()
                # ----------------------------------
                # Geometry
                _geom_flag = 0
                try:
                    _geom_no = int(float(_rows[0]))
                    for x in range(_total_node):
                        _element[memb_number].section.append(_section[_geom_no])
                except KeyError:
                    if int(float(_rows[0])) == -1:
                        _geom_flag = _total_node
                #
                # ----------------------------------
                # Fixation
                _fix_flag = 0
                try:
                    _fix_no = int(float(_rows[1]))
                    for x in range(_total_node):
                        _element[memb_number].releases.append(_hinge[_fix_no])
                    #print('fix')
                except KeyError:
                    if int(float(_rows[1])) == -1:
                        _fix_flag = _total_node
                #
                # ----------------------------------
                # Exentricity
                _ecc_flag = 0
                try:
                    _exent_no = int(float(_rows[2]))
                    for x in range(_total_node):
                        _element[memb_number].offsets.append(_eccentricity[_exent_no])
                    #print('eccen')
                except KeyError:
                    if int(float(_rows[2])) == -1:
                        _ecc_flag = _total_node
                # ----------------------------------
                # Local Vector
                _vec_flag = 0
                try:
                    _trans_no = int(float(_rows[3]))
                    for x in range(_total_node):
                        _element[memb_number].guidepoint.append(_unit_vector[_trans_no])
                    #print('vec')
                except KeyError:
                    if int(float(_rows[3])) == -1:
                        _vec_flag = _total_node
                # ----------------------------------
                #
                _total = _geom_flag + _fix_flag + _ecc_flag + _vec_flag
                # --------------->
                #
                if _total > 0:
                    #
                    _no_rows = math.ceil(_total / 4.0)
                    data = flat_data(fin, line, _total + 4, 5)
                    #
                    # Geometry
                    _items = [int(float(y)) for y in data[0:_geom_flag]]
                    del data[0:_geom_flag]
                    _geom_flag = 0
                    for _item in _items:
                        _element[memb_number].section.append(_section[_item])
                    
                    # Fixation
                    _items = [int(float(y)) for y in data[0:_fix_flag]]
                    del data[0:_fix_flag]
                    _fix_flag = 0
                    for _item in _items:
                        try:
                            _element[memb_number].releases.append(_hinge[_item])
                        except KeyError:
                            _element[memb_number].releases.append(None)
                    
                    # Exentricity
                    _items = [int(float(y)) for y in data[0:_ecc_flag]]
                    del data[0:_ecc_flag]
                    _ecc_flag = 0
                    for _item in _items:
                        try:
                            _element[memb_number].offsets.append(_eccentricity[_item])
                        except KeyError:
                            _element[memb_number].offsets.append(None)
                    
                    # Local Vector
                    _items = [int(float(y)) for y in data[0:_vec_flag]]
                    del data[0:_vec_flag]
                    _vec_flag = 0
                    for _item in _items:
                        try:
                            _element[memb_number].guidepoint.append(_unit_vector[_item])
                        except KeyError:
                            _element[memb_number].guidepoint.append(None)
                    #
                #
            #
            #
            # ----------------------------------------------------------------------
            #                               Read Sets
            # ----------------------------------------------------------------------
            #
            elif 'GSETMEMB' in line:
                #
                nmemb = int(float(keyword[1])) - 4
                _set_no = int(float(keyword[2]))
                _type_set = int(float(keyword[4]))
                #_group[_set_no].type = _type_set
                _row_no = 0
                #
                #if _set_no == 42:
                #    print('-->')
                #
                _no_rows = math.ceil(nmemb / 4.0) - 1
                data = list(islice(fin, 1))
                
                #else:
                #if _row_no == 1:
                _rows = data[0].split()
                #
                for _no_x in range(1, len(_rows)):
                    try:
                        _member_no = int(float(_rows[_no_x]))
                        _try = 1.0 / _member_no
                        
                        if _type_set == 1:
                            _group[_set_no].nodes.append(_member_no)
                        else:
                            _group[_set_no].items.append(_member_no)

                    except ZeroDivisionError:
                        break
                #else:
                if _no_rows > 0 :
                    data = list(islice(fin, _no_rows))
                    for _data in data:
                        _data = _data.split()                 
                        for _no_x in _data:
                            try:
                                _member_no = int(float(_no_x))
                                _try = 1.0 / _member_no
                                
                                if _type_set == 1:
                                    _group[_set_no].nodes.append(_member_no)
                                else:
                                    _group[_set_no].items.append(_member_no)                                
        
                            except ZeroDivisionError:
                                break 
                #
            #
            #
            # ----------------------------------------------------------------------
            #                               Concepts
            # ----------------------------------------------------------------------
            #
            #
            elif 'SCONPLIS' in line:
                # NFIELD
                _item_no = int(float(keyword[1]))                    
                _scon_no = int(float(keyword[2]))
                # print(line)
                data = flat_data(fin, line, _item_no)
                
                while data:
                    try:
                        1.0 /float(data[0])
                    except ZeroDivisionError:
                        break
                    # Number of property selectors
                    _num_rep = int(float(data[0]))
                    data.pop(0)
                    for x in range(_num_rep):
                        # Property attribute type number
                        _type_prop = int(float(data[0]))
                        data.pop(0)
                        # Property selector reference numbers (internal id)
                        _irpsele = int(float(data[0]))
                        data.pop(0)
                        
                        _concept_temp[_scon_no].attribute = _irpsele
                #
            #
            elif flag1 == 'SPROSELE':
                # NFIELD
                _item_no = int(float(keyword[1]))                  
                _irpsele = int(float(keyword[2]))
                
                data = flat_data(fin, line, _item_no)
                
                while data:
                    try:
                        1.0 /float(data[0])
                    except ZeroDivisionError:
                        break
                    # Number of property attribute types
                    _num_rep = int(float(data[0]))
                    data.pop(0)
                    for x in range(_num_rep):
                        # Property attribute type number
                        _typeprop = str(int(float(data[0])))
                        _typeprop = sesam_concept.concept_property(_typeprop)
                        data.pop(0)
                        # Property attribute reference numbers (internal id)
                        irpattr = int(float(data[0]))
                        data.pop(0)
                        
                        try:
                            _conproptemp[_irpsele].append((_typeprop, irpattr))
                        except KeyError:
                            _conproptemp[_irpsele] = [(_typeprop, irpattr)]
            #
            # FIXME:
            #elif 'SPROCODE' in line:
            #    pass
            #
            else:
                fem_temp.write(line)
    #
    fem_temp.close()
    #
    os.remove('femTemp.fem')
    #
    #
    #
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #                                   POSTPROCESSING
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #    
    # Populate concept object
    sesam_concept.update_concepts(_element, _concept, 
                                  _concept_temp, _mesh_sc,
                                  _conproptemp, _group)
    #
    # update cdcm elements
    _cdcm = sesam_concept.update_cdcm(_hydro, _concept)
    #
    if _truss:
        sesam_concept.update_truss_element(_truss, _element, 
                                           _concept, _group, _hinge)
    #
    # Shim group contains material numbers
    if _shim:
        name_ng = '_shim_'
        process.geometry.add_group(_group, _shim, name_ng)
        #
    #
    # Spring group contains material numbers
    if _spring:
        name_ng = '_spring_'
        process.geometry.add_group(_group, _spring, name_ng)
        #
    #
    #
    # print(_element[1].length)
    #
    #
    sesam_concept.get_group_compactness(_element, _group, _dt60, _dt80)
    #
    # --------------------------------------------------------------------------------------
    #                                     END READ FILE
    # --------------------------------------------------------------------------------------
    #
    #
    # 
    print('--- End reading T.FEM file')
    #
    # model = f2u.F2U_model(tfem_file, 1)
    #
    #_name = tfem_file.split('.')[0]
    #model.set_component()
    #model.component.nodes = _node
    model.component.sections = _section
    model.component.elements = _element
    model.component.materials = _material
    model.component.vectors = _unit_vector
    model.component.eccentricities = _eccentricity
    model.component.hinges = _hinge
    model.component.sets = _group
    #model.component.joints = _tubular_joint
    model.component.data = _user_data
    #model.component.boundaries = _boundary
    model.component.cdcm = _cdcm
    model.component.concepts = _concept    
    #
    #
    # ----------------------------------------------------------------------
    #                             LOAD SECTION
    # ----------------------------------------------------------------------
    #
    #
    model.set_load()
    model.load.functional = _load
    read_Lfem('femTemp2.fem', model)
    os.remove('femTemp2.fem')
    #
    #
    # find tubular joints
    #
    _msg = process.joint.tubular_joint(model.component.joints,
                                       model.component.elements,
                                       joint_angle, 
                                       joint_coplanary)
    # self.check_out.extend(_msg) 
    #
    print('--- End redFEM module')
    #
    #
    #
    #    
    #
    return model
#
#