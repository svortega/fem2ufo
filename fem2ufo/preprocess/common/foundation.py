#
# Copyright (c) 2009-2016 fem2ufo
#


# Python stdlib imports
import math
import sys

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
#

#
def get_spring_material(spring):
    """
    """
    _mat = {}
    _number = 0
    for i in range(len(spring)):
        #for _mat_name, _spr in spring[i].items():
        for key in sorted(spring[i], key = lambda name: spring[i][name].number):
            _spr = spring[i][key]
            _mat_name = _spr.name + "_" + str(key)
            _number += 1
            _mat[_mat_name] = f2u.Material(_mat_name, _number)
            _mat[_mat_name].spring = _spr
            _mat[_mat_name].type = 'spring'
            _spr.name = _mat_name
    #
    return _mat
#
#
def read_foundation(model, _soil_file, _format, 
                    _units_input, _units_output):
    """
    """
    #
    if _format == 'sesam':
        import fem2ufo.preprocess.sesam.model.gensod as gensod

        # here factors are getting for unit conversion purposes
        # units_output = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
        #factors, _grav = process.units.get_factors_and_gravity(_units_input,
        #                                                       _units_output)
        #
        _soil, _material, _user_data = gensod.read_gensoil(_soil_file) #, 
                                                           #_length=factors[0], 
                                                           #_force=factors[4])
    #
    elif _format == 'asas':
        import fem2ufo.preprocess.asas.model.splinter as splinter
        #
        if not _units_input:
            print('  **  error global units not provided in ASAS model')
            print('      process terminated')
            sys.exit()        
        #
        _soil, _material, _user_data = splinter.read_splinter(_soil_file, _units_input)
        _units_input = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
    #
    elif _format == 'genie':
        """
        """
        # TODO : wrong folder
        import fem2ufo.preprocess.genie.operations.foundations as genie_soil
        #
        _soil, _material, _user_data = genie_soil.read_soil(_soil_file)
        _units_input = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
    #
    elif _format == 'usfos':
        print("usfos soil format not yet implemented")
        sys.exit()
    #
    elif _format == 'sacs':
        import fem2ufo.preprocess.sacs.model.psi as psi
        #
        _soil, _material, _user_data = psi.read_psi(_soil_file)
        _units_input = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
    #
    else:
        print("soil format {:} not recognized".format(_format))
        sys.exit()
    #
    #
    #if _units_input != _units_output:
    if _units_output[0] != _units_input[0]  and _units_output[4] != _units_input[4]:
        print('---> fix foundation units')
        factors, _grav = process.units.get_factors_and_gravity(_units_input,
                                                               _units_output)
        1/0.0
    #
    #
    if not model.foundation:
        model.set_foundation()
    #
    model.foundation.soil = _soil
    model.foundation.materials = _material
    model.foundation.data = _user_data
    #
#
#