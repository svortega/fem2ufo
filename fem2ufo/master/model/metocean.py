#
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.process.control as process


#
#
#
def wave_type(line_in, key_word, ind=2):
    """
    """
    _key = {"irregular": r"\b(irregular|no[n]?\s*linear)\b",
            "spool": r"\b(random|spool)\b",
            "grid": r"\b(grid)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
def wave_spectrum(line_in, key_word, ind=3):
    """
    """
    _key = {"jonswap": r"\b(j(onswap)?)\b",
            "pm": r"\b(p(ierson)?\s*m(oscovitz)?)\b",
            "user": r"\b(user(\s*defined)?)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
def wave_data(line_in, key_word, ind=4):
    """
    """
    _key = {"hs": r"\b(hs)\b",
            "tp": r"\b(tp)\b",
            "direction": r"\b(dir(ection)?)\b",
            "seed": r"\b(seed)\b",
            "numberfrequencies": r"\b(n(umber)?\s*freq(uencies)?)\b",
            "tmin": r"\b(t\s*min(imum)?)\b",
            "tmax": r"\b(t\s*max(imum)?)\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
def wave_word(line_in, key_word, ind=3):
    """
    """
    _key = {"spectrum": r"\b(spect(re)?|spectrum)\b",
            "grid": r"\b(grid)\b",
            "data": r"\b(data)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
def wave_spectrum_data(line_in, key_word, ind=3):
    """
    """
    _key = {"frequency": r"\b(freq(uency)?)\b",
            "spectraldensity": r"\b(spec(tral)?\s*dens(ity)?)\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
# ----------------------------
# hydrostatic pressure modules
#
def elev_opt(line_in, key_word, ind=1):
    """
    """
    _key = {"surface": r"\b((hi)?\s*surf(ace)?)\b",
            "mudline": r"\b(sea\s*bed|mud\s*line)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                             key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
def den_word(line_in, key_word, ind=1):
    """
    """
    _key = {"fluid": r"(sea)?\s*water|fluid",
            "air": r"air"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#