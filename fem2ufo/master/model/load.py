#
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.process.control as process


#
# ----------------------
# loading modules
#
def load_words(line_in, key_word, ind=1):
    """
    """
    _key = {"mass": r"\b((to|2)\s*mass)\b",
            "impulse": r"\b((to|2)\s*impulse)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
def opt_word(line_in, key_word, ind=2):
    """
    """
    _key = {"fillgap": r"fill(ing)?\s*gap"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
def factor_word(line_in, key_word, ind=2):
    """
    """
    _key = {"factor": r"factor"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])
    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#