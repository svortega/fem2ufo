#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import re
import sys

# package imports
import fem2ufo.process.control as process
import fem2ufo.master.operation.common_module as master_common
import fem2ufo.master.operation.weight_module as master_weight
import fem2ufo.master.operation.th_module as th_module
import fem2ufo.master.model.load as master_load
import fem2ufo.master.model.analysis as master_analysis
import fem2ufo.master.model.geometry as master_geom


#
def database_words(line_in, key_word, ind=2):
    """
    Returns database commands key words
    """
    #
    _key = {"weight": r"\b(weight)\b",
            #"database": r"\b(database)\b",
            "time_serie": r"\b(t(ime)?\s*(h(istor(y|ies))?|serie(s)?))\b",}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word

#
def tubular_data_words(line_in, key_word, ind=3):
    """
    Returns tubular data commands key words
    """
    #
    _key = {"column": r"\b(column)\b",
            "row" : r"\b(row)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word

#
def limit_words(line_in, key_word, ind=2):
    """
    Returns weight commands key words
    """
    #
    _key = {"start": r"\b(start)\b",
            "end" : r"\b(end)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word

#
def pile_words(line_in, key_word, ind=2):
    """
    Returns weight commands key words
    """
    #
    _key = {"subsidence": r"\b(subsidence|collapse|sinking|settling)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word

#
#
def read_database_module(Master, line, key_word, read_data):
    """
    Module identify lines with tubular data commands in Master file
    """
    while line:
        line, key_word = database_words(line, key_word, 2)
        if not line:
            break
        
        if 'database' in key_word[2]:
            pass
        
        elif key_word[2] == 'weight':
            line, key_word = master_weight.weight_words(line, key_word, 3)
            
            if key_word[3] != 'item':
                line = master_common.line_missing(line)
            
            line, key_word = master_weight.item_options(line, key_word, 4)
            if not line:
                break
            
            # set data section
            Master[key_word[0]][key_word[1]][key_word[2]][key_word[4]]['data']['start'] = read_data['start']
            Master[key_word[0]][key_word[1]][key_word[2]][key_word[4]]['data']['end'] = read_data['end']
            #
            if 'mass' in key_word[4]:
                line, key_word = master_weight.mass_type(line, key_word, 4)
                if not line:
                    break
                
                line, key_word = master_weight.mass_words(line, key_word, 5)
                
                if key_word[5] == 'cog':
                    line, key_word = master_common.coord_word(line, key_word, 6)
                    key_word[5] = key_word[5] +'_'+ key_word[6]
                    #print('-->')
                    line, key_word = tubular_data_words(line, key_word, 6)
                    item = line.strip()
                
                else:
                    line, key_word = tubular_data_words(line, key_word, 6)
                    
                    try:
                        line, item = master_common.word_pop(line)
                    except IndexError:
                        break                    
                    
                Master[key_word[0]][key_word[1]][key_word[2]][key_word[4]][key_word[5]][key_word[6]] = item
                key_word[5] = None
                key_word[6] = None                    
            #---
            else:
                if key_word[4]:
                    line, key_word = tubular_data_words(line, key_word, 5)
                    
                    try:
                        line, item = master_common.word_pop(line)
                    except IndexError:
                        break
                    
                    Master[key_word[0]][key_word[1]][key_word[2]][key_word[4]][key_word[5]] = item
                    
                    key_word[4] = None
                    key_word[5] = None
                
                else:
                    line = master_common.line_missing(line)
        #
        elif key_word[2] == 'time_serie':
            # get recently added time history
            try:
                _time_history = list(Master['database'].keys())[-1]
            except IndexError:
                print('    *** error : time history input file not found')
                ExitProg = input('Press Enter Key To Continue')
                sys.exit()
            
            line, key_word = limit_words(line, key_word, 3)
            
            if key_word[3] :
                line, key_word = tubular_data_words(line, key_word, 4)
                line, key_word = master_common.item_id(line, key_word, 5)
                try:
                    line, item = master_common.word_pop(line)
                except IndexError:
                    break                
                if 'name' in key_word[5]:
                    item = process.spreadsheet.letter2num(item)
                    #print(' --> name')
                
                Master[key_word[2]][_time_history][key_word[3]][key_word[4]] = item
            else:
                line, key_word = th_module.time_history_words(line, key_word, 3)
                line, key_word = tubular_data_words(line, key_word, 4)
                line, key_word = master_common.item_id(line, key_word, 5)
                #
                # get tubular data location
                if key_word[4]:
                    try:
                        line, item = master_common.word_pop(line)
                    except IndexError:
                        break
                    if 'name' in key_word[5]:
                        item = process.spreadsheet.letter2num(item)
                    #
                    if 'time' in key_word[3]:
                        Master[key_word[2]][_time_history][key_word[3]][key_word[4]] = item
                    else:
                        #       0        1     2     3     4     5       6
                        # th [name, number, type, column, row, dof, factor]
                        try:
                            Master[key_word[2]][_time_history][key_word[1]].append([None, None, key_word[3], item, 1, None, 1.0])
                        except TypeError:
                            Master[key_word[2]][_time_history][key_word[1]] = []
                            Master[key_word[2]][_time_history][key_word[1]].append([None, None, key_word[3], item,  1, None, 1.0])
                else:
                    line = master_common.line_missing(line)
        #
        else:
            line = master_common.line_missing(line)
    
    return key_word, line
#
#
def set_database_module(Master, line, key_word, read_data):
    """
    Module identify lines with tubular data commands in Master file
    """
    while line:
        line, key_word = database_words(line, key_word, 2)
        if not line:
            break
        
        if key_word[2] == 'data':
            line, key_word = master_common.step_word(line, key_word, 3)
            line, key_word = tubular_data_words(line, key_word, 4)
            
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                break
            
            read_data[key_word[3]] = [key_word[4], item]
            #print('-->')
            
        elif key_word[2] == 'weight':
            line, key_word = master_weight.weight_words(line, key_word, 3)
            if not line:
                break            
            
            if key_word[3] == 'item':
                line, key_word = master_weight.item_options(line, key_word, 4)
                if not line:
                    break
                
                line, key_word = master_common.item_id(line, key_word, 5)
                
                try:
                    line, item = master_common.word_pop(line)
                except IndexError:
                    break
                
                Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]][key_word[4]][key_word[5]] = item
                
                key_word[4] = None
                key_word[5] = None                
                print('-->')
            
            elif key_word[3] == 'system':
                print('-->')            
            
            elif key_word[3] == 'component':
                line, key_word = master_common.item_id(line, key_word, 4)
                
                try:
                    line, item = master_common.word_pop(line)
                except IndexError:
                    break            
                
                
                Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]][key_word[4]] = item
                
                key_word[3] = None
                key_word[4] = None
                
            else:
                line = master_common.line_missing(line)
        
        elif key_word[2] == 'time_serie':
            # get recently added time history
            #
            
            line, key_word = th_module.time_history_words(line, key_word, 3)
            #
            #if not key_word[3]:
            #    line, key_word = master_common.item_id(line, key_word, 3)
            #    try:
            #        line, item = master_common.word_pop(line)
            #    except IndexError:
            #        break
                #print('--->')
            #    Master[key_word[2]][item][key_word[3]] = item
            #    continue
            #
            try:
                _time_history = list(Master['database'].keys())[-1]
                #print('--->', _time_history, len(Master['time_history']))
            except IndexError:
                print('    *** error : time history input file not found')
                ExitProg = input('Press Enter Key To Continue')
                sys.exit()
            #
            # get time
            if key_word[3]:
                if 'time' in key_word[3]:
                    line, key_word = th_module.time_words(line, key_word, 4)
                    if key_word[4]:
                        try:
                            line, item = master_common.word_pop(line)
                        except IndexError:
                            break
                        
                        Master[key_word[2]][_time_history][key_word[3]][key_word[4]][key_word[3]] = item
                        
                        line, key_word = th_module.time_step(line, key_word, 5)
                        if key_word[5]:
                            try:
                                line, item = master_common.word_pop(line)
                            except IndexError:
                                break
                            
                            Master[key_word[2]][_time_history][key_word[3]][key_word[4]][key_word[5]] = item
                        
                        continue
                    else:
                        line = master_common.line_missing(line)
                    #print('time')
                else:
                    line = master_common.line_missing(line)
            #
            # additional settings
            #if not key_word[3]:
            line, key_word = master_common.item_id(line, key_word, 3)
            # get time modifiers
            if key_word[3]:
                try:
                    line, item = master_common.word_pop(line)
                except IndexError:
                    break
                
                if 'number' in key_word[3]:
                    #       0        1     2     3     4     5       6
                    # th [name, number, type, column, row, dof, factor]
                    #
                    if Master[key_word[2]][_time_history]['database'][-1][0]:
                        print('--> create a new one')
                    else:
                        Master[key_word[2]][_time_history]['database'][-1][1] = int(item) # get th number
                        # get start row
                        _row = Master[key_word[2]][_time_history]['start']['row']
                        Master[key_word[2]][_time_history]['database'][-1][4] = int(_row)
                    # check name
                    if not Master[key_word[2]][_time_history]['database'][-1][0]:
                        Master[key_word[2]][_time_history]['database'][-1][0] = 'TH_' + str(int(item))
                    #
                    #
                    #Master[key_word[2]][_time_history][key_word[3]] = int(item)
                    continue
                elif 'name' in key_word[3]:
                    if Master[key_word[2]][_time_history]['database'][-1][0]:
                        print('--> create a new one')
                    else:
                        Master[key_word[2]][_time_history]['database'][-1][0] = item
                        # check number
                        #if not Master[key_word[2]][_time_history]['database'][-1][1]:
                        #    Master[key_word[2]][_time_history]['database'][-1][1] = 50000
                    #
                    continue
                else:
                    line = master_common.line_missing(line)
            #
            # get dof
            #else:
            line, key_word = master_geom.control_word(line, key_word, 3)
            if key_word[3]:
                if 'dof' in key_word[3]:
                    line, key_word = master_common.coord_word(line, key_word, 4)
                    #print('-->')
                    Master[key_word[2]][_time_history]['database'][-1][5] = key_word[4]
                    #       0        1     2     3     4     5       6
                    # th [name, number, type, column, row, dof, factor]
                    #
                    #Master[key_word[2]][_time_history][key_word[3]] = key_word[4]
                # get factor
                #else:
                #line, key_word = master_load.factor_word(line, key_word, 3)
                elif 'factor' in key_word[3]:
                    line, factor = master_common.word_pop(line)
                    #
                    Master[key_word[2]][_time_history]['database'][-1][6] = float(factor)
                    #Master[key_word[2]][_time_history][key_word[3]] = factor
                    
                else:
                    line = master_common.line_missing(line)
            else:
                line = master_common.line_missing(line)            
            #print('--->')
            #Master[key_word[2]][_time_history][key_word[3]][key_word[4]] = 1.0
        else:
            line = master_common.line_missing(line)
    #
    return key_word, line
#
#
#
def apply_database_module(Master, line, key_word):
    """
    Module identify lines with tubular data commands in Master file
    """
    while line:
        line, key_word = database_words(line, key_word, 2)
        if not line:
            break
        
        elif key_word[2] == 'weight':
            line, key_word = master_weight.weight_words(line, key_word, 3)
            if not line:
                break
            
            if key_word[3] != 'item':
                line = master_common.line_missing(line)
            
            line, key_word = master_weight.item_options(line, key_word, 4)
            if not line:
                break
            
            line, key_word = master_common.item_id(line, key_word, 5)
            
            try:
                line, name = master_common.word_pop(line)
            except IndexError:
                break
            
            line, key_word = master_weight.wt_item_id(line, key_word, 6)
            
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                break
            
            Master[key_word[0]][key_word[1]][key_word[2]][key_word[6]][key_word[4]][name] = item
            key_word[5] = None
            key_word[6] = None 
        
        elif key_word[2] == 'time_serie':
            # get recently added time history
            try:
                _time_history = list(Master['database'].keys())[-1]
                # get th data
                _th_number = Master[key_word[2]][_time_history]['database'][-1][1]
                _th_dof = Master[key_word[2]][_time_history]['database'][-1][5]
                _th_factor = Master[key_word[2]][_time_history]['database'][-1][6]
            except IndexError:
                print('    *** error : time history input file not found')
                ExitProg = input('Press Enter Key To Continue')
                sys.exit()
            
            line, key_word = master_common.item_fe(line, key_word, 3)
            line, key_word = master_analysis.motion_word(line, key_word, 4)
            
            if 'soil' in key_word[3] :
                line, key_word = master_analysis.motion_application(line, key_word, 5)
                #
                try:
                    line, item = master_common.word_pop(line)
                except IndexError:
                    continue
                #
                _depth = float(item)
                _th_case = key_word[4]
                #
                try:
                    Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]][_depth].append([_th_number, _th_case, 
                                                                                                              _th_dof, _th_factor, _depth])
                except TypeError:
                    Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]][_depth] = []
                    Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]][_depth].append([_th_number, _th_case, 
                                                                                                              _th_dof, _th_factor, _depth])
            # pile to soil
            elif 'pile' in key_word[3] :
                line, key_word = pile_words(line, key_word, ind=4)
                line, key_word = master_common.item_id(line, key_word, 5)
                line, item = master_common.word_pop(line)
                
                _th_case = 'displacement'
                _item_id = item.strip()
                
                if 'number' in key_word[5]:
                    _item_id = int(float(item))                
                
                try:
                    Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]].append([_th_number, _th_case, 
                                                                                                      _th_dof, _th_factor, _item_id])
                except TypeError:
                    Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]] = []
                    Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]].append([_th_number, _th_case, 
                                                                                                      _th_dof, _th_factor, _item_id])                 
            
            # member & node
            else:
                line, key_word = master_common.item_id(line, key_word, 5)
                line, item = master_common.word_pop(line)
                line, key_word = master_common.beam_end(line, key_word, 6)
                
                _th_case = key_word[4]
                _item_id = item.strip()
                if 'number' in key_word[5]:
                    _item_id = int(float(item))

                if 'element' in key_word[3] :
                    _end = int(float(key_word[6]))
                    try:
                        Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]].append([_th_number, _th_case, 
                                                                                                          _th_dof, _th_factor, [_item_id, _end]])
                    except TypeError:
                        Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]] = []
                        Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]].append([_th_number, _th_case, 
                                                                                                          _th_dof, _th_factor, [_item_id, _end]])                     
                # node
                else:
                    try:
                        Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]].append([_th_number, _th_case, 
                                                                                                          _th_dof, _th_factor, _item_id])
                    except TypeError:
                        Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]] = []
                        Master[key_word[2]][_time_history][key_word[0]][key_word[3]][key_word[5]].append([_th_number, _th_case, 
                                                                                                          _th_dof, _th_factor, _item_id])                  
        #
        return key_word, line
        #    