#
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.process.control as process
import fem2ufo.master.operation.common_module as master_common


# ----------------------
# joint modules
#
def joint_rule(line_in, key_word, ind=2):
    """
    """
    _key = {"msl": r"msl|n(on)?\s*lin(ear)?",
            "doe": r"doe",
            "apiwsd": r"api(\s*wsd)?",
            "iso": r"iso(\s*19902)?",
            "norsok": r"norsok"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
def msl_options(line_in, key_word, ind=3):
    """
    """
    _key = {"mean": r"mean",
            "char": r"char(acteristic)?[s]?",
            "fcrack": r"f([i]?[r]?st)?\s*crack",
            "scalefactor": r"(scal[e]?\s*)?fact(or)?[s]?"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
    #
#
def joint_option(line_in, key_word, ind=2):
    """
    """
    _key = {"rule": r"rule[s]?",
            "tolerance": r"tol(erance)?",
            "case": r"case",
            "geometry": r"geom(etry)?",
            "eccentricity": r"eccentricit(y|ies)|offset(s)?"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
def joint_tol(line_in, key_word, ind=3):
    """
    """
    _key = {"angle": r"ang(le)?",
            "coplanary": r"cop(lanary)?"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def create_module(line, key_word, item_no):
    """
    """
    # while line:
    line, key_word = master_common.aux_word(line, key_word, 2)

    if not key_word[2] :
        line = line_missing(line)

    line, key_word = master_common.item_fe(line, key_word, 3)

    # new joint
    if key_word[2] == 'from':
        line, key_word = item_id(line, key_word, 4)
        line, item = master_common.word_pop(line)
        # from member end 1/2/3
        if key_word[3] == 'element':
            line, key_word = master_common.beam_end(line, key_word, 5)

            try:
                Master[key_word[1]][key_word[0]][item_no][key_word[3]][key_word[4]].append([item, key_word[5]])
            except TypeError:
                Master[key_word[1]][key_word[0]][item_no][key_word[3]][key_word[4]] = [item, key_word[5]]
        # from node
        else:
            try:
                Master[key_word[1]][key_word[0]][item_no][key_word[3]][key_word[4]].append(item)
            except TypeError:
                Master[key_word[1]][key_word[0]][item_no][key_word[3]][key_word[4]] = [item]
    #
    return key_word, line
    #
#
#
# ----------------------
# material modules
#
def mat_word(line_in, key_word, ind=1):
    """
    """
    _key = {"Fy": r"yield(\s*strength)?|fy",
            "poisson": r"poisson|nu|v",
            "E": r"young(\s*modulus)?|e",
            "density": r"density|rho",
            "alpha": r"(coefficient(\s*of)?\s*)?thermal(\s*expansion)?|alpha"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
# ----------------------
# fracture modules
#
def frac_word(line_in, key_word, ind=1):
    """
    """
    _key = {"strain": r"\b(strain|epsilon|e)\b",
            "cut": r"\b(cut)\b",
            # "time": r"\b(t(ime)?)\b",
            "load": r"\b(l(oad)?\s*comb(ination)?)\b",
            "uc": r"\b(ir|uc|m(ember)?\s*u(tili[zs]?ation)?)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
# ---------------------------------
# node control displacement modules
#
def control_word(line_in, key_word, ind=1):
    """
    """
    _key = {"dof": r"\b((i|g(lobal)?\s*)?d(egree)?\s*o[f]?\s*f(reedom)?)\b",
            "factor": r"\b((w(eight)?\s*)?fac(tor)?)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#