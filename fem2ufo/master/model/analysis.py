#
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.process.control as process
#import fem2ufo.master.operation.common as mtr_common



# ----------------------
# analysis modules
#
# def analysisWord(line_in, key_word, ind=1):
#    #
#    _KEY = {"seismic": r"\b(seismic|earthquake)\b",
#            "ship_impact" : r"\b((ship|vessel)\s*impact)\b"} #,
# "type": r"\b(type|case)\b"}
#    
#    key_word[ind], line_out, _match = searchLine(line_in, _KEY, key_word [ind])

#    if _match:
#        for x in range(ind+1, len(key_word)): key_word[x] = 'N/A'
#
#    return line_out, key_word     
#
#
def seismic_word(line_in, key_word, ind=1):
    """
    """
    _key = {"prescribed": r"\b(prescribe[d]?)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def motion_word(line_in, key_word, ind=1):
    """
    """
    #
    _key = {"displacement": r"\b(disp(lacement)?)\b",
            "velocity": r"\b(vel(ocity)?)\b",
            "acceleration": r"\b(acc(eleration)?)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def motion_application(line_in, key_word, ind=1):
    """
    """
    _key = {"depth": r"\b(depth(\s*below\s*mudline|seabed)?)\b"}
    # "intensity" : r"intensity"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def shipimpact_word(line_in, key_word, ind=1):
    """
    """
    _key = {"vessel": r"\b(vessel)\b",
            "installation": r"\b(installation)\b",
            "scenario": r"\b(scenario)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def vessel_word(line_in, key_word, ind=2):
    """
    """
    _key = {"mass": r"\b(mass)\b",
            "velocity": r"\b(velocity)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def installation_word(line_in, key_word, ind=2):
    """
    """
    _key = {"mass": r"\b(mass)\b",
            "velocity": r"\b(velocity)\b",
            "J": r"\b(j)\b",
            "zpivot": r"\b(zpivot)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def analysis_options(line_in, key_word, ind=0):
    """
    """
    _key = {"dynamic": r"\b(dynamic[s]?)\b",
            "static": r"\b(static[s]?)\b",
            "seismic": r"\b(seismic|earthquake)\b",
            "ship_impact": r"\b((ship|vessel)\s*impact)\b",
            "natural_frequency": r"\b(natural\s*frequency|eigenvalue)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word


#