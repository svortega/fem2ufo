#
# Copyright (c) 2009-2016 fem2ufo
# 

""" Read master input file"""


#
# Python stdlib imports
import sys
import re
import time
import os


# package imports
import fem2ufo.process.control as process
import fem2ufo.master.operation.common_module as master_commun
import fem2ufo.master.operation.operations as master_ops




# --------------------------------------------------------------------
#                               Main
# --------------------------------------------------------------------
#
#
#
class easyaccessdict(dict):
    """
    """
    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, name, value):
        super().__setitem__(name, value)

    def __missing__(self, name):
        super().__setitem__(name, easyaccessdict())
        return super().__getitem__(name)
#
#
class File():
    """
    """
    __slots__ = ('name', 'number', 'format', 'geometry', 'units',
                 'loading', 'metocean', 'foundation', 'data')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        #
#
#
def to_fixed_point(match):
    """
    Return the fixed point form of the matched number.

    Parameters:
        match is a MatchObject that matches exp_regex or similar.
    """
    coefficient, decimals, sign, exponent = match.groups()
    value = float(match.group(0))

    return str(value)
#
#
def remove_comment(_line_in):
    """
    """
    pattern = r"#|\'|\*{2}|\"|!|%|/{2}"
    reobj = re.compile(pattern)
    _index = []
    
    for match in reobj.finditer(_line_in):
        _index.append(match.start())

    _line_out = _line_in[0:_index[0]]
    _line_out = _line_out.strip()
    #
    return _line_out
#
#
def master_word(line_in, key_word, ind=0):
    """
    Return key master command
    """
    _key = {"input": r"\b(input[s]?)\b",
            "output": r"\b(output[s]?)\b",
            "print": r"\b(pr[i]?nt|wr[i]?t?[e]?)\b",
            "delete": r"\b(del(ete)?[d]?|elim(inate)?[d]?|remove[d]?)\b",
            "apply": r"\b(apply|assign)\b",
            "set": r"\b(set)\b",
            "omit": r"\b(omit(ed)?|ignore[d]?)\b",
            "grouted": r"\b(gr[o]?[u]?t(ed)?)\b",
            "new": r"\b((create[d]?|new)(\s*from)?)\b",
            "optimize": r"\b(opt(imi[s]?[z]?e)?|efficien(cy)?)\b",
            "convert": r"\b(conver(t|sion))\b",
            "updated": r"\b(update[d]?|revise[d]?|refresh(ed)?)\b",
            "read": r"\b(read)\b"}
    
    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
#
#
# --------------------------------------------------------------------------------------
#                              READ MASTER INPUT FILE
# --------------------------------------------------------------------------------------
#
#
# ALERT : This module can get in an infinive loop 
def read_master_file(master_file):
    """
    """
    #
    #global Master
    #
    print('')
    print('----------------- REDMASTER Module -----------------')
    #
    master_file = master_commun.check_file(master_file)   
    #    
    Master = easyaccessdict()
    # TODO: set tolerance to find supernodes
    Master.input.tolerance = 0.10
    Master.joint.tolerance.angle = 1.0
    Master.joint.tolerance.coplanary = 0.01
    Master.hinges = 'off'
    #Master.database = {}
    #Master.time_history = {}
    #Master.model = {}
    #
    _start = time.time()
    PERIOD_OF_TIME = 20
    #
    print('--- Reading master file')
    #
    exp_regex = re.compile(r"(\d+(\.\d+)?)[Ee](\+|-)(\d+)")
    #
    option = 'node'
    _time = {}
    # units_in = ['metre', 'kilogram', 'second', 'centigrades', 'newton', 'gravity']
    units_in = [None, None, 'second', None, None, 'gravity']
    analysis_type = 'static'
    time_history = ''
    read_data = {'start':None, 'end':None}
    #
    item_no = 'N/A'
    item_name = 'N/A'
    _disp_factor = 1
    key_word = [None, None, None, None, None, None, None]
    #
    for line in open(master_file):
        #
        # chck if program in a loop
        #if time.time() > _start + PERIOD_OF_TIME:
        #    print('  ** error process time exceded')
        #    print('  ** error in line: {:}'.format(line))
        #    print('     program aborted')
        #    sys.exit()
        #
        # Jump commented lines
        if re.match(r"#|\*|'|\"|!|%|/{2}", line):
            continue
        # find key words and clean lines from comments
        if re.search(r"#|'|\*|\"|!|%|/{2}", line):
            line = remove_comment(line)
        # Jump empty lines
        if not line.strip():
            continue
        # remove separators
        line = re.sub(r",|;|=|:|&", " ", line)
        line = exp_regex.sub(to_fixed_point, line)
        line, key_word = master_word(line, key_word)
        #
        if key_word[0] == 'input' or key_word[0] == 'output':
            key_word, units_in = master_ops.io_module(Master, line, 
                                                      key_word, units_in)
        #
        elif key_word[0] == 'delete' or key_word[0] == 'omit' or key_word[0] == 'grouted':
            key_word = master_ops.delete_module(Master, line, key_word)
        #
        elif key_word[0] == 'set':
            key_word = master_ops.set_module(Master, line, 
                                             key_word,
                                             read_data)
        #
        elif key_word[0] == 'apply':
            key_word = master_ops.apply_module(Master, line, key_word)
        #
        elif key_word[0] == 'optimize':
            key_word = master_ops.optimum_module(Master, line, key_word)
        #
        elif key_word[0] == 'convert':
            key_word, option, _time = master_ops.convert_module(Master, line, 
                                                                key_word, option, _time)
        # 
        elif key_word[0] == 'new' or key_word[0] == 'updated':
            key_word, item_name, item_no = master_ops.new_module(Master, line, key_word, 
                                                                 item_name, item_no)
        #
        elif key_word[0] == 'read':
            key_word = master_ops.read_module(Master, line, 
                                              key_word, read_data)
        #
        else:
            line = master_commun.line_missing(line)
        #
    # 
    # --------------------------------------------------------------------
    #          
    #
    if not Master.input.units[0] or not Master.input.units[4]:
        print('  ** error input units not provided')
        print('     program aborted')
        sys.exit()
    else:
        if Master.input.units[1] == Master.output.units[1]:
            if Master.input.units[0] != Master.output.units[0] \
               or Master.input.units[4] != Master.output.units[4]:
                print('  *** warning mass output unit not provided')
                Master.output.units[1] = None
    #
    #
    print('--- End reading master file')
    #
    return Master
    #
