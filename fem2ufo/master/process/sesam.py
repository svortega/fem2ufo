#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import sys


# package imports
import fem2ufo.process.control as process
from fem2ufo.preprocess.control import SESAM_model
#import fem2ufo.process.model.modify as modify


#
#
def get_sesam_model(master):
    """
    master input file \n
    Return : model \n
    """
    # get model
    if master.model.geometry:
        file_name = master.model.geometry.file  # geometry file name
        # get tolerance
        _dim0 = master.model.geometry.units[0]
        _tolerance = process.units.get_tolerance(_dim0,
                                                 float(master.model.geometry.tolerance))
        # Get joint tolerances
        j_angle = float(master.joint.tolerance.angle)
        j_coplanary = float(master.joint.tolerance.coplanary)
        #
        # set f2u geometry object
        model = SESAM_model(tol = _tolerance,
                            joint_angle = j_angle, 
                            joint_coplanary = j_coplanary)
        #
        # set working directory
        if master.model.geometry.path:
            model.directory_path(master.model.geometry.path)
        # Define input units (mandatory)
        model.units(force=master.model.geometry.units[4], 
                    length=master.model.geometry.units[0])
        #
        # set f2u geometry input file
        model.geometry(master.model.geometry.file)
        #
        # set f2u Loading input file file
        if master.model.loading.file:
            model.loading(master.model.loading.file)
    else:
        print("   *** error geometry data not found")
        sys.exit()
    #
    # --------------------------------------------------------------------
    # soil section
    # --------------------------------------------------------------------
    #
    if master.model.foundation:
        _format = master.model.foundation.format   # input format type file
        _file_soil = master.model.foundation.file  # foundation file
        _units = master.model.foundation.units     # foundation units
        #
        # set f2u foundation input data
        model.foundation(foundation_file = _file_soil,
                         file_format = _format,
                         foundation_units = _units)
    else:
        model.model.foundation = {}
        print("--- Foundation data not found")
    #
    # --------------------------------------------------------------------
    # metocean section
    # --------------------------------------------------------------------
    #
    if master.model.metocean:
        # input format type file
        _format = master.model.metocean.format
        file_metocean = master.model.metocean.file
        model.metocean(file_metocean)
    else:
        print("--- Metocean data not found")
    #
    return model.model

#