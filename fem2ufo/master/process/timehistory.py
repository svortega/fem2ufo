#
# Copyright (c) 2009-2017 fem2ufo
# 

# Python stdlib imports
import sys


# package imports
import fem2ufo.preprocess.control as preprocess
import fem2ufo.process.control as process
from fem2ufo.master.process.usfos import print_usfos_time_history

#
#
# Main 
def timehistory_conversion(master):
    """ Process to get time history data according to master input file"""
    #
    _units_output = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
    #
    _keys = {"acceleration": r"\b(acc(eleration)?)\b",
             "velocity": r"\b(vel(ocity)?)\b",
             "displacement": r"\b(disp(lacement)?)\b",
             "time" : r"\b(t(ime)?)\b"}
    #
    # --------------------------------------------------------------------
    #
    # --------------------------------------------------------------------
    #
    _time_history = {}
    #
    #_files = [_item for _item in master.time_history.values()]
    #_th_files = []
    #
    _num_defaul = 50000
    for key, _item in master.time_serie.items():
        _th = master.database[key]
        # here factors are getting for unit conversion purposes
        #_item['units'] = _th.units[:4]
        _units_input = _th.units[:5]
        _acceleration_units = _th.units[5]
        factors, _grav = process.units.get_factors_and_gravity(_units_input,
                                                               _units_output)
        #
        _time = _item.time.column
        _row = int(_item.start.row)
        for _database in _item['database']:
            #
            #       0        1     2     3     4     5       6
            # db [name, number, type, column, row, dof, factor]            
            #
            #_case = _database[2]
            _name = _database[0]
            if not _database[1]:
                _num_defaul += 100
                _database[1] = _num_defaul
            
            #_number = _database[1]
            # _dof = _database[5]
            #_factor = _database[6]
            #_th_files.append(_name_th)
            _time_history[_name] = preprocess.TimeSeries(name=_database[0],
                                                         number=_database[1])
            _time_history[_name].units(acceleration=_acceleration_units)
            #
            if _th.path:
                _time_history[_name].directory_path(_th.path)
            #
            if 'csv' in _th.format:
                _time_history[_name].csv_file(file=_th.file)
            elif 'excel' in _th.format:
                _time_history[_name].excel_file(file=_th.file, 
                                                sheet=_th.tap)
            else:
                print('    *** error format {:} not supported'.format(_th.format))
                continue
            #
            _names = ['time', _database[2]]
            _columns = [_time, int(_database[3])]
            #
            # get databse data
            _time_history[_name].first_row_to_read = _row
            _time_history[_name].get_columns(_columns, _names)
            #_time_history[_name].read_database()
            #
            #
            _case = process.common.find_keyword(_database[2], _keys)
            _th_name = _name # + '_' + _database[5]
            #
            if "acceleration" in _case:
                #_database.append("acceleration")
                _time_history[_name].set_time_serie(name=_th_name,
                                                    time='time',
                                                    acceleration=_database[2])
            elif "velocity" in _case:
                #_database.append("velocity")
                _time_history[_name].set_time_serie(name=_th_name,
                                                    time='time',
                                                    velocity=_database[2])
            elif "displacement" in _case:
                #_database.append("displacement")
                _time_history[_name].set_time_serie(name=_th_name,
                                                    time='time',
                                                    displacement=_database[2])
            else:
                print('    ** error time_history_case {:} not recognized'.format(_database[2]))
                print('       provide : acceleration, velocity or displacement')
                continue            
            #
    #
    # printing time history database
    #
    if 'ufo' in master.output.format:
        _name = 'TimeHistory_database'
        if master.analysis.name:
            _name = master.analysis.name
        #
        print_usfos_time_history(_name, _time_history)
        #
        #print('-->')
    else:
        print('    *** error output format {:} not supported'.format(master.output.format))
        ExitProg = input('Press Enter Key To Continue')
        sys.exit()
    #
    # print('-->')
    #
    #