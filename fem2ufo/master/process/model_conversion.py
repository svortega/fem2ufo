#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import sys

#
# package imports
import fem2ufo.process.control as process
import fem2ufo.master.operation.operations as master_ops
#import fem2ufo.process.file.formatting as formatting

from fem2ufo.master.process.sesam import get_sesam_model
from fem2ufo.master.process.usfos import print_usfos_model
from fem2ufo.master.process.control import get_control_file

#
#
#
#
# Main 
def model_conversion(master):
    """ Process and run fe model conversion according to Master commands"""
    #
    # --------------------------------------------------------------------
    # master section
    # --------------------------------------------------------------------
    # 
    # get model
    if 'sesam' in master.model.geometry.format :
        model = get_sesam_model(master)
    else:
        print("   *** error model geometry input fomat : {:} no yet implemented"
              .format(master.model.geometry.format))
        sys.exit()
    #
    # --------------------------------------------------------------------
    # Update Master to replace items names by FE numbers (control)
    # --------------------------------------------------------------------
    #
    control = get_control_file(master,
                               model.component,
                               model.load)
    #
    # --------------------------------------------------------------------
    # Sorting units before continuing with the operation
    # --------------------------------------------------------------------
    # 
    _units_input = control.input['units'][:5]
    _units_output = control.output['units'][:5]
    #
    # if metocean then units are changed to SI [m, N]
    try:
        model.metocean.data
        _units_output = ['metre', 'kilogram', 'second', 'kelvin', 'newton', 'pascal']
    except AttributeError:
        pass
    #
    # here factors are getting for unit conversion purposes
    factors, _grav = process.units.get_factors_and_gravity(_units_input,
                                                           _units_output)
    control.gravity = _grav
    #
    # --------------------------------------------------------------------
    # Processing model with the master/control information
    # --------------------------------------------------------------------
    #
    _process_checkout = master_ops.get_model(control, model)
    #
    # process model with analysis information
    control, model = master_ops.get_analysis(master, control, model)
    #
    # Tidying up model after processing
    model = process.geometry.clean_model(model)
    #
    # print model
    _model_check_out = process.common.printing.print_model_info(model, factors)
    #
    # --------------------------------------------------------------------
    # Output section
    # --------------------------------------------------------------------
    #
    # getting output file name
    file_name = control.output['file_name']
    file_name = process.common.split_file_name(file_name)
    
    if master.output.format == 'ufo':
        # print ufo & optional file 
        #_shim_gap = control.gap
        _ufo_check_out = print_usfos_model(model, control, 
                                           file_name[0], factors)
    else:
        print("   *** error output fomat : {:} no yet implemented"
              .format(master.output.format))
        sys.exit()
    #
    # printing check out file
    file_checkout = str(file_name[0]) + '_check_me.txt'
    add_out = open(file_checkout, 'w+')
    add_out.write("".join(_model_check_out))
    header = 'MODEL PROCESSING SECTION'
    add_out.write("".join(process.common.printing.print_head_line(header)))
    add_out.write("".join(_process_checkout))
    #add_out.close()
    #add_out = open(file_checkout, 'a')
    add_out.write("".join(_ufo_check_out))
    add_out.close()
    #
    print('--- Writing Chek Me file')
    print('    * File : {:}'.format(file_checkout))
    #