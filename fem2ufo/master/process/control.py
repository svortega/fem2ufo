# 
# Copyright (c) 2009-2015 fem2ufo
# 

""" Process Master commands according to fe model"""

# Python stdlib imports


# package imports
#import fem2ufo.model.feclass.FEmodel as f2uFE
import fem2ufo.process.control as process



# --------------------------------------------------------------------
# Control
# --------------------------------------------------------------------
#
class ControlInput(object):
    """
    """
    __slots__ = ('type', 'linear', 'node_control', 'jelly',
                 'nonstructural', 'fracture', 'hydropressure',
                 'delete', 'joint', 'material', 'mass',
                 'optimize', 'analysis', 'input', 'output',
                 'gravity', 'centralisers', 'reinforce', 'hinges')

    def __init__(self):
        #
        self.type = ''
        self.linear = {'element': [], 'element_group': []}
        self.node_control = {'node': [], 'node_group': [],
                             'dof': [1, 1, 1]}
        self.jelly = {'element': [], 'element_group': [],
                      'factor': 10.0}
        self.reinforce = {'element': [], 'element_group': [],
                          'factor': 1000.0}   
        self.nonstructural = {'element': [], 'element_group': []}
        self.fracture = {'element': [], 'element_group': [],
                         'strain': 15}
        self.hydropressure = {'element': [], 'element_group': [],
                              'data': [0, 0, 0, 0, 0]}
        self.delete = {'element': [], 'element_group': [],
                       'node': [], 'node_group': [],
                       'load': [], 'load_group': []}
        #
        self.joint = {'type': ''}
        #
        self.mass = {'load': [], 'dof': [0, 0, 1, 0, 0, 0]}
        #
        #
        self.material = {'mean': '',
                         'new': {'name': [], 'number': []},
                         'update': {'name': [], 'number': []}}
        #
        self.optimize = {'load_fill_gap': False}
        #
        self.analysis = {'type': 'static',
                         'seismic': {'soil_depth': {},
                                     'node': {}},
                         'subsidence' :{'pile' : {}},
                         'ship_impact': {'case': '',
                                         'installation': []},
                         'wave': {'case': '',
                                  'data': [],
                                  'spectrum': [],
                                  'user_defined': [],
                                  'grid': []}}
        #
        self.input = {'units': [], 'path': ''}
        #
        self.output = {'units': [], 'file_name': '',
                       'format': ''}
        #
        # self.gravity = 9.80665
        self.hinges = False
        self.centralisers = {'gap' : False,
                             'element': [], 
                             'element_group': []}
        #
        #


#
# --------------------------------------------------------------------
#
def extract_member_group(_subject, _item, master, _elements, _sets):
    """
    Get elements number & sets by concept name
    """
    _member_no = []
    _group_no = []

    if master[_subject][_item]['group']:
        _name = master[_subject][_item]['group']['name']
        _number = master[_subject][_item]['group']['number']
        _group_no = find_fe_number_by_name_master(_name, _number, _sets)

    if _item != 'element':
        _name = master[_subject][_item]['name']
        _number = master[_subject][_item]['number']
        _member_no = find_fe_number_by_name_master(_name, _number, _elements)

    return _member_no, _group_no
#
# --------------------------------------------------------------------
#
# TODO: pehaps move this to process/operations/concept
def find_fe_number_by_name_master(name, number, subject,
                                  _factor=False):
    """
    Find item group's fe number by concept name and append it to
    a group's fe number
    """
    # convert name to number
    _no_items = len(name.start)
    #
    for i in range(_no_items):
        _start = name.start[i]
        _end = name.end[i]
        _name = [_start, _end]
        # find name number
        j = 0
        for _item in _name:
            for _sub in subject.values():
                if _sub.name.lower() == str(_item).lower():
                    # start item 1
                    if j == 0:
                        # get first item fe number
                        try:
                            number.start.append(_sub.number)
                        except TypeError:
                            number.start = [_sub.number]
                        #
                        # if factor given
                        if _factor:
                            try:
                                number.factor.append(name.factor[i])
                                number.option.append(name.option[i])
                            except TypeError:
                                number.factor = [name.factor[i]]
                                number.option = [name.option[i]]
                    # continue rest of items
                    else:
                        try:
                            number.end.append(_sub.number)
                        except TypeError:
                            number.end = [_sub.number]
                    #
                    break
            j += 1
    #
    return check_sets_number(number, subject, _factor)
    #
#
#
def check_sets_number(number, subject, _factor=False):
    """
    check if given item group's exist in model's group
    """
    _no_items = len(number.start)
    _list = []
    factor = []
    option = []
    #
    for i in range(_no_items):
        _start = int(number.start[i])
        try:
            _no = subject[_start].number
        except KeyError:
            continue
        #
        _end = int(number.end[i])
        for j in range(_end, _start, -1):
            try:
                _end = subject[j].number
                break
            except KeyError:
                continue
        #
        _max = max(_start, _end)
        _min = min(_start, _end)
        _list.extend([x for x in range(_min, _max + 1)])

        if _factor:
            try:
                _fact = float(number.factor[i])
            except KeyError:
                _fact = 1.0

            factor.extend([_fact for x in range(_min, _max + 1)])
            _opt = number.option[i]
            option.extend([_opt for x in range(_min, _max + 1)])
    #
    if _factor:
        return _list, factor, option
    else:
        return _list
#
#
# --------------------------------------------------------------------
#                     Control File
# --------------------------------------------------------------------
#
def get_control_file(master, _geometry, _load):
    """
    Get fe model fem2ufo control parameters from master file\n
    
    Input : \n
           master input file \n
           geometry \n
           load \n
    Output :
           control file \n
    """
    #
    print('')
    print('-------------- Getting Control Commads -------------')
    #
    control = ControlInput()
    #
    #
    # --------------------------------------------------------------------
    # Input/output Section
    # --------------------------------------------------------------------
    #
    #
    if master.input.path:
        control.input['path'] = master.input.path
    #
    # units
    control.input['units'] = master.input.units
    #
    if master.output.units:
        control.output['units'] = master.output.units
    else:
        # take input units
        control.output['units'] = master.input.units
    #
    # Output
    # Format
    control.output['format'] = master.output.format
    # Name file
    if master.output.geometry.file:
        control.output['file_name'] = master.output.geometry.file
    else:
        control.output['file_name'] = master.model.geometry.file
    #
    #
    if master.analysis.type:
        control.analysis['type'] = master.analysis.type
    #
    #
    # --------------------------------------------------------------------
    # General Section
    # --------------------------------------------------------------------
    #
    # Node control displacement
    if master.control.displacement:
        if master.control.displacement.element.name:
            _memb_name = master.control.displacement.element.name
        else:
            _memb_name = []

        if master.control.displacement.element.number:
            _memb_number = master.control.displacement.element.number
        else:
            _memb_number = []

        if master.control.displacement.node.number:
            _node_number = master.control.displacement.node.number
        else:
            _node_number = []

        _node = process.concept.find_node_in_element_end(_geometry,
                                                     _memb_name,
                                                     _memb_number,
                                                     _node_number)

        control.node_control['node'] = _node
    else:
        control.node_control = None
    #
    if 'fillgap' in master.load.optimize :
        control.optimize['load_fill_gap'] = True
    #
    # hinges
    if 'on' in master.hinges:
        control.hinges = True
    #
    # joint
    if master.joint:
        control.joint['type'] = master.joint.rule
        control.joint['eccentricity'] = True
        if master.joint.eccentricity == 'delete':
            control.joint['eccentricity'] = False
    #
    # pressure
    if master.hydropressure:
        _subject = 'hydropressure'
        _fe_item = 'element'
        _name = master[_subject]['element']['name']['start']
        _member_no = process.concept.find_fe_number_by_concept_name(_geometry.concepts, _name)

        _member_dummy, _group_no = extract_member_group(_subject, _fe_item,
                                                        master,
                                                        _geometry.elements,
                                                        _geometry.sets)
        #
        if _group_no:
            control.hydropressure['element_group'] = _group_no
        #
        if _member_no:
            control.hydropressure['element'] = _member_no
        #
        control.hydropressure['data'][0] = master.hydropressure.elevation.surface
        control.hydropressure['data'][1] = master.hydropressure.elevation.mudline

        if master.hydropressure.density.fluid:
            control.hydropressure['data'][2] = master.hydropressure.density.fluid

        if master.hydropressure.density.air:
            control.hydropressure['data'][3] = master.hydropressure.density.air

        if master.hydropressure.density.gravity:
            control.hydropressure['data'][4] = master.hydropressure.density.gravity
    else:
        control.hydropressure = None
    #
    # fracture
    if master.fracture:
        _subject = 'fracture'
        _fe_item = 'element'
        _name = master[_subject]['element']['name']['start']
        _member_no = process.concept.find_fe_number_by_concept_name(_geometry.concepts, _name)

        _member_dummy, _group_no = extract_member_group(_subject, _fe_item,
                                                        master,
                                                        _geometry.elements,
                                                        _geometry.sets)
        #
        if _group_no:
            control.fracture['element_group'] = _group_no
        #
        if _member_no:
            control.fracture['element'] = _member_no
        #
        control.fracture['strain'] = float(master.fracture['strain'])
    else:
        control.fracture = None
    #
    # Linear
    if master.linear:
        _subject = 'linear'
        _fe_item = 'element'
        _name = master[_subject]['element']['name']['start']
        _member_no = process.concept.find_fe_number_by_concept_name(_geometry.concepts, _name)

        _member_dummy, _group_no = extract_member_group(_subject, _fe_item,
                                                        master,
                                                        _geometry.elements,
                                                        _geometry.sets)
        #
        if _group_no:
            control.linear['element_group'] = _group_no
        #
        if _member_no:
            control.linear['element'] = _member_no
    else:
        control.linear = None
    #
    # Nonstructural
    if master.nonstructural:
        _subject = 'nonstructural'
        _fe_item = 'element'
        _name = master[_subject]['element']['name']['start']
        _member_no = process.concept.find_fe_number_by_concept_name(_geometry.concepts, _name)

        _member_dummy, _group_no = extract_member_group(_subject, _fe_item,
                                                        master,
                                                        _geometry.elements,
                                                        _geometry.sets)
        #
        if _group_no:
            control.nonstructural['element_group'] = _group_no
        #
        if _member_no:
            control.nonstructural['element'] = _member_no
    else:
        control.nonstructural = ''
    #
    # Overlapping
    if master.overlapping:
        _subject = 'overlapping'
        _fe_item = 'element'
        _name = master[_subject]['element']['name']['start']
        _member_no = process.concept.find_fe_number_by_concept_name(_geometry.concepts, _name)

        _member_dummy, _group_no = extract_member_group(_subject, _fe_item,
                                                        master,
                                                        _geometry.elements,
                                                        _geometry.sets)
        #
        if _group_no:
            control.overlapping['element_group'] = _group_no
        #
        master.overlapping.element.number = []
        if _member_no:
            control.overlapping['element'] = _member_no
    #
    # Yelly
    if master.jelly:
        if master.jelly.factor:
            control.jelly['factor'] = float(master.jelly.factor)
        #
        _subject = 'jelly'
        _fe_item = 'element'
        _name = master[_subject]['element']['name']['start']
        _member_no = process.concept.find_fe_number_by_concept_name(_geometry.concepts, _name)

        _member_dummy, _group_no = extract_member_group(_subject, _fe_item,
                                                        master,
                                                        _geometry.elements,
                                                        _geometry.sets)
        #
        if _group_no:
            control.jelly['element_group'] = _group_no
        #
        if _member_no:
            control.jelly['element'] = _member_no
    else:
        control.jelly = None
    #
    # reinforce
    if master.reinforce:
        if master.reinforce.factor:
            control.reinforce['factor'] = float(master.reinforce.factor)
        #
        _subject = 'reinforce'
        _fe_item = 'element'
        _name = master[_subject]['element']['name']['start']
        _member_no = process.concept.find_fe_number_by_concept_name(_geometry.concepts, _name)

        _member_dummy, _group_no = extract_member_group(_subject, _fe_item,
                                                        master,
                                                        _geometry.elements,
                                                        _geometry.sets)

        # 
        if _group_no:
            control.reinforce['element_group'] = _group_no
        #
        if _member_no:
            control.reinforce['element'] = _member_no
    else:
        control.reinforce = None
    #
    # delete
    if master.delete.element:
        _subject = 'delete'
        _fe_item = 'element'
        _name = master[_subject]['element']['name']['start']
        _member_no = process.concept.find_fe_number_by_concept_name(_geometry.concepts, _name)

        _member_dummy, _group_no = extract_member_group(_subject, _fe_item,
                                                        master,
                                                        _geometry.elements,
                                                        _geometry.sets)
        #
        if _group_no:
            control.delete['element_group'] = _group_no
        #
        if _member_no:
            control.delete['element'] = _member_no
    #
    if master.delete.node:
        _subject = 'delete'
        _fe_item = 'node'
        _node_no, _group_no = extract_member_group(_subject, _fe_item, master,
                                                   _geometry.nodes,
                                                   _geometry.sets)
        #
        if _group_no:
            control.delete['node_group'] = _group_no
        #
        if _node_no:
            control.delete['node'] = _node_no
    #
    #
    if master.centralisers.gap:
        control.centralisers['gap'] = float(master.centralisers.gap)
        _subject = 'centralisers'
        _fe_item = 'element'
        _name = master[_subject]['element']['name']['start']
        _member_no = process.concept.find_fe_number_by_concept_name(_geometry.concepts, _name)
    
        _member_dummy, _group_no = extract_member_group(_subject, _fe_item,
                                                        master,
                                                        _geometry.elements,
                                                        _geometry.sets)
    
        # 
        if _group_no:
            control.centralisers['element_group'] = _group_no
        #
        if _member_no:
            control.centralisers['element'] = _member_no
    #
    # --------------------------------------------------------------------
    # material modification section
    # --------------------------------------------------------------------
    # 
    if master.material.mean:
        control.material['mean'] = 1.0 + float(master.material.mean) / 100.0
    #
    if master.material.new:
        if master.material.new.name:
            control.material['new']['name'] = master.material.new.name

        if master.material.new.number:
            control.material['new']['number'] = master.material.new.number
    #
    if master.material.updated:
        if master.material.updated.name:
            control.material['update']['name'] = master.material.updated.name

        if master.material.updated.number:
            control.material['update']['number'] = master.material.updated.number
    #
    # --------------------------------------------------------------------
    # Loading modification section
    # --------------------------------------------------------------------
    #    
    # convert load to mass
    if master.mass.convert: 
        _name = master.mass.convert.load.name
        _number = master.mass.convert.load.number
        _flag = True
        _number, _factor, _option = find_fe_number_by_name_master(_name, _number,
                                                                  _load.functional,
                                                                  _flag)
        if _number:
            control.mass['load'] = [_number, _factor, _option]
    #
    # deleting load
    if master.delete.load:
        _subject = 'delete'
        _fe_item = 'load'
        _load_no, _group_no = extract_member_group(_subject, _fe_item, master,
                                                   _load.functional,
                                                   _geometry.sets)

        if _load_no:
            control.delete['load'] = _load_no

        if _group_no:
            control.delete['load_group'] = _group_no
    #
    #
    return control
#
# 
