#
# Copyright (c) 2009-2018 fem2ufo
# 

""" Read master input file"""


#
# Python stdlib imports
from math import ceil

#
# package imports
import fem2ufo.postprocess.usfos.ufo as usfos
import fem2ufo.postprocess.usfos.operation.process as ufo_operation

#
# Output
#
def print_ufo_model(model, file_name, factors, 
                    _shim_gap, hinges, th_factor=1):
    """
    """
    #
    check_out = []
    #
    print('--- Format : {:}'.format('usfos'))
    #
    #
    # --------------------------------------------------------------------
    # perform usfos specific postprocessing
    # --------------------------------------------------------------------
    #
    #
    # --------------------------------------------------------------------
    # check if spring material
    # --------------------------------------------------------------------                
    # TODO : move this to ufo.py
    check_out = ufo_operation.process_spring_ufo(model.component,
                                                 factors, _shim_gap)
    #
    # --------------------------------------------------------------------
    # write ufo file
    # --------------------------------------------------------------------
    #
    fe_model = usfos.model_ufo()
    #
    fe_model.model(model.component,
                   model.load.functional,
                   factors, hinges)
    #
    if model.metocean:
        fe_model.metocean(model.metocean, factors)
    #
    if model.foundation:
        fe_model.foundation(model.foundation, factors)
    #
    # --------------------------------------------------------------------
    # write additional ufo files
    # --------------------------------------------------------------------
    #    
    #
    try:
    #if model.analysis:
        fe_model.combination(model.analysis, factors)
    except AttributeError:
        pass
    #
    # time domain load
    if model.load.time_domain:
        #  TODO: need to decide on this
        _factor = th_factor
        _timehistory = model.load.time_domain
        fe_model.time_domain(_timehistory, _factor)
        #
    # --------------------------------------------------------------------
    # print ufo files
    # --------------------------------------------------------------------
    # 
    fe_model.print_model(file_name)
    #
    #
    return check_out
    #
#
def print_ufo_control_master(control, model, file_name, factors):
    """
    control
    model
    filename
    factors
    """
    #
    #
    _analysis_type = control.analysis['type']
    #
    ufo_control = usfos.control_ufo()
    ufo_control.model(model)
    ufo_control.analysis(_analysis_type)
    #
    if control.node_control:
        _cnodes = control.node_control['node']
        _dof = control.node_control['dof']  # TODO : need to fix this
        # _dof = [1,1,1]
        ufo_control.node_control(_cnodes, _dof)
    #
    if control.linear:
        _group_list = control.linear['element_group']
        _memb_list = control.linear['element']

        ufo_control.linear(_memb_list, _group_list)
    #
    if control.jelly:
        _group_list = control.jelly['element_group']
        _memb_list = control.jelly['element']

        ufo_control.jelly(_memb_list, _group_list)
    #
    if control.nonstructural:
        _group_list = control.nonstructural['element_group']
        _member_list = control.nonstructural['element']

        ufo_control.nonstructural(_member_list, _group_list)
    #
    if control.fracture:
        _strain = float(control.fracture['strain']) / 100.
        _group_list = control.fracture['element_group']
        _member_list = control.fracture['element']

        ufo_control.fracture(_strain, _member_list, _group_list)

    # joint
    if model.component.joints and control.joint['type']:
        _rule = control.joint['type']
        _joints = model.component.joints

        ufo_control.joints(_joints, _rule, 
                           control.joint['eccentricity'])
    else:
        print('    ** Warning -- > No joints defined')
        _joints = ''
    #
    # Hydrodynamics
    try:
        for _metocean in model.analysis.case.values():
            ufo_control.metocean(_metocean.metocean_combination,
                                 factors=factors)
    except AttributeError:
        if control.hydropressure:
            # print hydro pressure instead
            _hisurf = control.hydropressure['data'][0]  # elevation.surface
            _losurf = control.hydropressure['data'][1]  # elevation.mudline

            _fluid_density = control.hydropressure['data'][2]  # density.fluid
            #_gravity = control.hydropressure['data'][4]  # gravity
            _gravity = control.gravity

            _group_list = control.hydropressure['element_group']
            _memb_list = control.hydropressure['element']

            ufo_control.hydropressure(_memb_list, _group_list,
                                      _hisurf, _losurf,
                                      _fluid_density, _gravity, factors)
        else:
            print('    ** Warning -- > No hydropressure defined')

    if model.foundation:
        ufo_control.foundation(model.foundation,
                               factors)
    else:
        print('    ** Warning -- > No soil defined')
    #
    # seismic
    #
    if control.analysis['seismic']:
        # soil
        _series = ufo_operation.get_elevations_seismic(control.analysis['seismic']['soil_depth'])
        
        for _case, _soil_depth in _series.items():
            if _soil_depth:
                ufo_control.seismic_soil_motion(_case, _soil_depth, factors)
        # node
        _nodes = control.analysis['seismic']['node']
        for _case, _node in _nodes.items():
            if _node:
                ufo_control.seismic_node_motion(_case, _node)
    #
    if control.analysis['subsidence']:
        _piles = control.analysis['subsidence']['pile']
        for _case, _pile in _piles.items():
            if _pile:
                ufo_control.pile_subsidence(_case, _pile)
    #
    ufo_control.print_control_file(file_name)
    #
    #
#
def print_usfos_time_history(name, time_series,
                             time_history_case=None): # column_name, 
    """
    """
    th_usfos = usfos.USFOS_conversion()
    
    for key, item in time_series.items():
        _column_name = item.read_data['column']['name']
        _column_name.remove('time')
        th_usfos.time_history(name=item.name, 
                              number=item.number,
                              time_serie=item,
                              time_serie_name=key)
    
    th_usfos.print_time_history(file_name=name,
                                time_history_case=time_history_case)
#
def print_usfos_model(model, control, file_name, factors):
    """
    """
    _shim_gap = control.centralisers # ['gap']
    _hinges = control.hinges
    _ufo_check_out = print_ufo_model(model, file_name, factors, 
                                     _shim_gap, _hinges)

    # print ufo control file
    print_ufo_control_master(control, model, file_name,
                             factors)
    
    return _ufo_check_out
#