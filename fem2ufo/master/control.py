# 
# Copyright (c) 2009-2015 fem2ufo
# 

# Python stdlib imports


# package imports
#
from fem2ufo.master.process.master import read_master_file
from fem2ufo.master.process.model_conversion import model_conversion
from fem2ufo.master.process.database import database_conversion
#from fem2ufo.master.process.timehistory import timehistory_conversion
#



#
# 
def master_input(master_file):
    """ Process and run conversion according to Master commands"""
    #
    # get master file user information
    master = read_master_file(master_file)
    #
    #
    # process database data
    if master.database:
        database_conversion(master)
    #
    # model conversion
    if master.model.geometry:
        model_conversion(master)
    #
#

