#
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.master.operation.common_module as master_common
import fem2ufo.master.model.geometry as master_geom


#
#
# ---------------------------------
#
# 
def factor_module(Master, line, key_word):
    """
    """
    if key_word[1] == 'jelly' or key_word[1] == 'reinforce':
        line, key_word = master_geom.control_word(line, key_word, 2)

        if key_word[2] :
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                return key_word, line

            Master[key_word[1]][key_word[2]] = item
    else:
        line = line_missing(line)
    #
    return key_word, line
#
#