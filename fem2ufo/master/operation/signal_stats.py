# 
# Copyright (c) 2015 steelpy


# Python stdlib imports
from math import pi,sqrt,floor, log, sin, cos, exp, ceil

def sample_rate_check(a,b,sr,dt, screen=None):
    """
    """
    num = len(a)
    dtmin=1e+50
    dtmax=0

    for i in range(1, num-1):
        if a[i] - a[i-1] < dtmin:
            dtmin = a[i] - a[i-1]
            if a[i] - a[i-1] > dtmax:
                dtmax = a[i] - a[i-1]
    #
    if screen:
        try : 
            srmax=float(1/dtmin)
        except ZeroDivisionError : 
            srmax = 0
    
        try : 
            srmin=float(1/dtmax)
        except ZeroDivisionError : 
            srmin = 0
        
        print ("")
        print ("--- Sample Check")
        print ("  dtmin = {: 4.6f} sec".format(dtmin))
        print ("     dt = {: 4.6f} sec".format(dt))
        print ("  dtmax = {: 4.6f} sec".format(dtmax))
        
        print ("  srmax = {: 4.6f} samples/sec".format(srmax ))
        print ("     sr = {: 4.6f} samples/sec".format(sr))
        print ("  srmin = {: 4.6f} samples/sec".format(srmin))
    
        print ("  dtmin        dt      dtmax     srmax       sr      srmin")
        print ("{:1.3e}  {:1.3e} {:1.3e} {:1.3e} {:1.3e} {:1.3e}"
               .format(dtmin, dt, dtmax, srmax, sr, srmin,))

        if srmax-srmin > 0.01*sr:
            print(" ")
            print(" Warning: sample rate difference ")
            #sr = None
            #while not sr:
            #    try:
            #        print(" Enter new sample rate ")
            #        s = stdin.readline()
            #        sr=float(s)
            #        dt=1/sr
            #    except ValueError:
            #        print ('Invalid Number')
    return dtmax, dtmin
#
def signal_stats(a, b, screen=None):
    """
    a is the time column.
    b is the amplitude column.
    num is the number of coordinates
    Return
          sr - sample rate
          dt - time step
        mean - average
          sd - standard deviation
         rms - root mean square
        skew - skewness
    kurtosis - peakedness
         dur - duration
    """
    num = len(b)
    bmax=max(b)
    bmin=min(b)
    #
    dur = a[num-1]-a[0]
    dt = dur/float(num-1)
    sr = 1/dt
    #
    if screen:
        ave = mean_calc(b)
        _var = var(b)
        rms = sqrt(_var)
        sd = std(b)
        #
        print ("")
        print ("--- Signal Statisitcs")
        print (" Maximum    Minimum     Mean    std dev     rms      start      end     duration")
        print ("{:1.3e} {: 1.3e} {:1.3e} {:1.3e} {:1.3e} {:1.3e} {:1.3e} {:1.3e}"
               .format(bmax, bmin, ave, sd, rms, a[0], a[num-1], dur))
    
        print ("      max = {: 4.6f} ".format(bmax))
        print ("      min = {: 4.6f} ".format(bmin))
        print ("     mean = {: 4.6f} ".format(ave ))
        print ("  std dev = {: 4.6f} ".format(sd))
        print ("      rms = {: 4.6f} ".format(rms))
        #print (" skewness = %8.4g " % skewness )
        #print (" kurtosis = %8.4g " % kurtosis )
    
        print ("    start = {: 4.6f} sec".format(a[0]) )
        print ("      end = {: 4.6f} sec".format(a[num-1]))
        print ("      dur = {: 4.6f} sec".format(dur))
    #
    #return sr,dt,ave,sd,rms,skewness,kurtosis,dur
    return sr, dt, dur # ave, sd, rms,
#
# mean
def mean_calc(nums):
    """
    """
    return sum(nums) / len(nums)

# variance
def var(data):
    """
    Use the Computational Formula for Variance.
    """
    # 
    n = len(data)
    ss = sum(x**2 for x in data) - (sum(data)**2) / n
    return ss/(n-1)    

# standard deviation
def std(data):
    """
    """
    return sqrt(var(data))

#
def check_th_data(a, b):
    """
    check data
    """
    # 
    sr, dt, dur = signal_stats(a, b)
    dtmax, dtmin = sample_rate_check(a, b, sr, dt)
    dtmin = ceil(dtmin * 100)/100.
    return dur, dt, dtmax, dtmin
    #