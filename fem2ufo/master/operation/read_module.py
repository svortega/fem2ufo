#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
#import re


# package imports
import fem2ufo.process.control as process
import fem2ufo.master.operation.common_module as master_common
import fem2ufo.master.model.database as master_db

#
def read_words(line_in, key_word, ind=2):
    """
    Returns database commands key words
    """
    #
    _key = {"database" : r"\b(data\s*base)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
def read_module(Master, line, key_word, read_data):
    """
    """
    while line:
        line, key_word = read_words(line, key_word, 1)
    
        if not line:
            continue
        
        if key_word[1] == 'database':
            key_word, line = master_db.read_database_module(Master, line, key_word,
                                                         read_data)
        else:
            line = master_common.line_missing(line)
    
    return key_word