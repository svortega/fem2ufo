#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.master.operation.common_module as master_common
import fem2ufo.master.model.geometry as master_geom
#
#
# ----------------------
#
def new_module(Master, line, key_word, 
               item_name, item_no):
    """
    """
    while line:
        line, key_word = master_common.item_concept(line, key_word, 1)

        if not key_word[1]:
            line, key_word = master_common.item_fe(line, key_word, 1)

        line, key_word = master_common.item_id(line, key_word, 2)

        if not line:
            continue

        elif key_word[2] == 'name' or key_word[2] == 'number':
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                continue
            item_name = key_word[2]
            item_no = item
            Master[key_word[1]][key_word[0]][item_name][item_no][key_word[2]] = item
            key_word[2] = None
        #
        if key_word[1] == 'material':
            line, key_word = master_geom.mat_word(line, key_word, 2)

            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                continue

            Master[key_word[1]][key_word[0]][item_name][item_no][key_word[2]] = item
        #
        elif key_word[1] == 'joint':
            key_word, line = create_module(line, key_word, item_no)
        #
        else:
            line = master_common.line_missing(line)
    #
    return key_word, item_name, item_no
    #
#