#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.master.operation.common_module as master_common

#
#
def weight_words(line_in, key_word, ind=2):
    """
    Returns database commands key words
    """
    #
    _key = {"item": r"\b(item(s)?)\b",
            "component": r"\b(component)\b",
            "system": r"\b(system)\b"}

    key_word[ind], line_out, _match = master_common.search_line(line_in, _key, 
                                                             key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
def item_options(line_in, key_word, ind=2):
    """
    Returns database commands key words
    """
    #
    _key = {"tag": r"\b(tag(s)?)\b",
            "description": r"\b(description)\b",
            "discipline": r"\b(discipline)\b",
            "area": r"\b(area(s)?)\b",
            "module": r"\b(module(s)?)\b",
            "date": r"\b(date)\b",
            "status": r"\b(status)\b",
            "mass": r"\b(mass)\b"}

    key_word[ind], line_out, _match = master_common.search_line(line_in, _key, 
                                                             key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def wt_item_id(line_in, key_word, ind=2):
    """
    Returns database commands key words
    """
    #
    _key = {"code": r"\b(code)\b"}

    key_word[ind], line_out, _match = master_common.search_line(line_in, _key, 
                                                             key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def mass_type(line_in, key_word, ind=2):
    """
    Returns database commands key words
    """
    #
    _key = {"mass_permanent": r"\b(permanent|dry|dead)\b",
            "mass_variable": r"\b(variable|content|live)\b"}

    key_word[ind], line_out, _match = master_common.search_line(line_in, _key, 
                                                             key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def mass_words(line_in, key_word, ind=2):
    """
    Returns database commands key words
    """
    #
    _key = {"magnitud": r"\b(magnitud)\b",
            "cog": r"\b(cog)\b"}

    key_word[ind], line_out, _match = master_common.search_line(line_in, _key, 
                                                             key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
