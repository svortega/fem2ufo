#
# Copyright (c) 2009-2017 fem2ufo
# 

# Python stdlib imports
#import re


# package imports
import fem2ufo.process.control as process

#
def time_history_words(line_in, key_word, ind=3):
    """
    Returns tubular data commands key words
    """
    #
    _key = {"time" : r"\b(t(ime)?)\b",
            "displacement" : r"\b(dis(placement)?)\b",
            "acceleration" : r"\b(acc(eleration)?)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def time_words(line_in, key_word, ind=4):
    """
    """
    _key = {"delay": r"\b(delay)\b",
            "expand": r"\b(expand|augment|increase|extend)\b"}

    #key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key,
    #                                                                key_word[ind])
    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])    

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
def time_step(line_in, key_word, ind=4):
    """
    """
    _key = {"step": r"\b(step)\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key,
                                                                    key_word[ind])
    #key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
    #                                                             key_word[ind])    
    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
#