#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.master.operation.common_module as master_common
import fem2ufo.master.model.load as mtr_load

#
#
# ----------------------
#
def impulse_words(line_in, key_word, ind=3):
    """
    """
    _key = {"ramped": r"\b(ramp(ed)?)\b",
            "truncated": r"\b(truncate[d]?)\b",
            "stepped": r"\b(step(ped)?)\b"}

    key_word[ind], line_out, _match = search_line(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
def convert_module(Master, line, key_word, _option, _time):
    """
    """
    while line:
        line, key_word = mtr_load.load_words(line, key_word, 1)  # full search
        # line, key_word = itemFE(line, key_word, 1) # full search

        if not line:
            continue
        # check key word found
        elif key_word[1] :
            # line, key_word = loadWords(line, key_word, 2) # full search
            line, key_word = master_common.item_fe(line, key_word, 2)  # full search
            # load section
            if key_word[2] == 'load':
                line, key_word = master_common.item_id(line, key_word, 3)  # first string
                line, key_word = master_common.step_word(line, key_word, 4)  # first string

                if not key_word[4]:
                    line, key_word = mtr_load.factor_word(line, key_word, 4)  # first string
                    try:
                        line, item = master_common.word_pop(line)
                    except IndexError:
                        continue
                    # factor
                    if key_word[4] == 'factor':
                        _index = len(Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['start'])
                        try:
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['factor'][_index - 1] = float(item)
                        except:
                            line = line_missing(line)
                    # single load
                    else:
                        try:
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['start'].append(item)
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['end'].append(item)

                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['factor'].append(1)
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['option'].append(_option)
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['time'].append(_time)
                        except TypeError:
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['start'] = [item]
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['end'] = [item]

                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['factor'] = [1]
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['option'] = [_option]
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['time'] = [_time]
                        except:
                            line = line_missing(line)
                # star and end load defined
                else:
                    line, key_word = mtr_load.factor_word(line, key_word, 4)  # first string
                    try:
                        line, item = master_common.word_pop(line)
                    except:
                        continue

                    if key_word[4] == 'factor':
                        _index = len(Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['start'])
                        try:
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['factor'][_index - 1] = float(
                                item)
                        except:
                            line = line_missing(line)
                    else:
                        try:
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]][key_word[4]].append(item)
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['option'].append(_option)
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['time'].append(_time)

                            if key_word[4] == 'end':
                                Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['factor'].append(1)
                        except TypeError:
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]][key_word[4]] = [item]

                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['factor'] = [1]
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['option'] = [_option]
                            Master[key_word[1]][key_word[0]][key_word[2]][key_word[3]]['time'] = [_time]
                        except:
                            line = line_missing(line)
                            # option
            else:
                line, key_word = master_common.item_option(line, key_word, 3)  # first string
                if key_word[3] == 'option':
                    if key_word[1] == 'impulse':
                        line, key_word = impulse_words(line, key_word, 4)  # full search

                        if key_word[4]:
                            _option = key_word[4]
                            key_word[3] = None
                    else:
                        _option = key_word[2]
                else:
                    line, key_word = input_word(line, key_word, 3)  # full search
                    if key_word[3] == 'time':
                        line, key_word = step_word(line, key_word, 4)  # first string
                        try:
                            line, item = master_common.word_pop(line)
                        except IndexError:
                            continue
                        _time[key_word[4]] = item
                        # print('ok')
                    else:
                        line = line_missing(line)
                        # error reporting
        else:
            line = master_common.line_missing(line)
    #
    return key_word, _option, _time


#