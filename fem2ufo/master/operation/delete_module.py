#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.master.operation.common_module as master_common
import fem2ufo.master.model.geometry as master_geom

# ----------------------
# delete modules
#
def delete_module(Master, line, key_word):
    """
    """
    while line:
        line, key_word = master_common.item_fe(line, key_word, 1)

        if not key_word[1]:
            line, key_word = master_common.item_concept(line, key_word, 1)

        if not line:
            continue
        
        elif key_word[1]:
            line, key_word = master_common.aux_fe(line, key_word, 2)
            
            if 'joint' in key_word[1]:
                line, key_word = master_geom.joint_option(line, key_word, 2)
                Master['joint'][key_word[2]] = key_word[0]
                #line = None
            elif key_word[2] == 'group':
                line, key_word = master_common.item_id(line, key_word, 3)
                line, key_word = master_common.step_word(line, key_word, 4)

                if not key_word[4]:
                    try:
                        line, item = master_common.word_pop(line)
                    except IndexError:
                        continue

                    try:
                        Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]]['start'].append(item)
                        Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]]['end'].append(item)
                    except TypeError:
                        Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]]['start'] = [item]
                        Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]]['end'] = [item]
                    except:
                        line = master_common.line_missing(line)
                else:
                    try:
                        line, item = master_common.word_pop(line)
                    except:
                        continue

                    try:
                        Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]][key_word[4]].append(item)
                    except TypeError:
                        Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]][key_word[4]] = [item]
                    except:
                        line = master_common.line_missing(line)
            else:
                line, key_word = master_common.item_id(line, key_word, 2)
                line, key_word = master_common.step_word(line, key_word, 3)

                if not key_word[3]:
                    try:
                        line, item = master_common.word_pop(line)
                    except IndexError:
                        continue

                    try:
                        Master[key_word[0]][key_word[1]][key_word[2]]['start'].append(item)
                        Master[key_word[0]][key_word[1]][key_word[2]]['end'].append(item)
                    except TypeError:
                        Master[key_word[0]][key_word[1]][key_word[2]]['start'] = [item]
                        Master[key_word[0]][key_word[1]][key_word[2]]['end'] = [item]
                    except:
                        line = master_common.line_missing(line)
                else:
                    try:
                        line, item = master_common.word_pop(line)
                    except:
                        continue

                    try:
                        Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]].append(item)
                    except TypeError:
                        Master[key_word[0]][key_word[1]][key_word[2]][key_word[3]] = [item]
                    except:
                        line = master_common.line_missing(line)
        else:
            line = master_common.line_missing(line)
    #
    return key_word
#
#