#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.process.control as process
import fem2ufo.master.operation.common_module as master_common
import fem2ufo.master.operation.factor_module as master_factor
import fem2ufo.master.operation.io_module as io_module
import fem2ufo.master.model.analysis as master_analysis
import fem2ufo.master.model.geometry as master_geom
import fem2ufo.master.model.metocean as master_metocean
import fem2ufo.master.model.database as master_db
#
# ----------------------
#
#
def greek_word(line_in, key_word, ind=3):
    """
    Returns greek name
    """
    _key = {"gamma": r"\b(gamma)\b",
            "beta": r"\b(beta)\b"}

    key_word[ind], line_out, _match = process.common.match_keyword(line_in, _key,
                                                                   key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
def set_Words(line_in, key_word, ind=1):
    """
    Returns Set commands key words
    """
    #
    _key = {"fracture": r"\b(frac(ture[d]?)?)\b",
            "reinforce" : r"\b(re\s*inforce(d)?)\b",
            "jelly": r"\b(jelly|spaghet(ti)?[y]?|rubber)\b",
            "mean": r"\b(mean)\b",
            "control": r"\b(control)\b",
            "rule": r"\b(rule[s]?)\b",
            "tolerance": r"\b(tol(erance)?)\b",
            "elevation": r"\b(elev(ation)?)\b",
            "density": r"\b(density)\b",
            "gravity": r"\b(gravity)\b",
            "analysis": r"\b(analysis|assessment)\b",
            "wave": r"\b(wave)\b",
            "wind": r"\b(wind)\b",
            "wid": r"\b(w(ave)?\s*i[n]?\s*d(eck)?)\b",
            "data_base" : r"\b(data(\s*base)?)\b",
            "hinges" : r"\b((hinge|release)(s)?)\b",
            "centralisers": r"\b(centraliser(s)?)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
#
def set_module(Master, line, key_word, 
               read_data):
    """
    Module identify lines with Set commands in Master file
    """
    while line:
        line, key_word = set_Words(line, key_word, 1)

        if key_word[1] == 'mean':
            line, key_word = master_common.item_fe(line, key_word, 2)
            # print('** ', line, key_word)
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                continue

            Master[key_word[2]][key_word[1]] = item
            key_word[1] = None
            key_word[2] = None

        elif key_word[1] == 'elevation':
            line, key_word = master_metocean.elev_opt(line, key_word, 2)
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                continue
            # print('***Elev ', line, key_word)

            Master['hydropressure'][key_word[1]][key_word[2]] = float(item)

        elif key_word[1] == 'density':
            line, key_word = master_metocean.den_word(line, key_word, 2)
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                continue
            # print('***Den ', line, key_word)

            Master['hydropressure'][key_word[1]][key_word[2]] = float(item)

        elif key_word[1] == 'gravity':
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                continue
            Master['hydropressure'][key_word[1]] = float(item)

        elif key_word[1] == 'fracture':
            line, key_word = master_geom.frac_word(line, key_word, 2)
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                continue

            Master[key_word[1]][key_word[2]] = item

        elif key_word[1] == 'jelly' or key_word[1] == 'reinforce':
            key_word, line = master_factor.factor_module(Master, line, key_word)

        elif key_word[1] == 'analysis':
            # analysis_type
            line, key_word = master_analysis.analysis_options(line, key_word, 2)

            if key_word[2] :
                analysis_type = key_word[2]
            else:
                line = master_common.line_missing(line)

            Master['analysis']['type'] = analysis_type
            
            line, key_word = master_common.item_id(line, key_word, 3)
            if key_word[3] :
                try:
                    line, item = master_common.word_pop(line)
                except IndexError:
                    break
                #
                Master['analysis'][key_word[3]] = item
                #
        elif key_word[1] == 'control':
            line, key_word = master_analysis.motion_word(line, key_word, 2)
            line, key_word = master_geom.control_word(line, key_word, 3)

            if key_word[3] == 'factor':
                try:
                    line, item = master_common.word_pop(line)
                except IndexError:
                    continue

                Master[key_word[1]][key_word[2]][key_word[3]] = item
            else:
                line, key_word = master_common.coord_word(line, key_word, 4)
                if key_word[4]:
                    try:
                        Master[key_word[1]][key_word[2]][key_word[3]].append(key_word[4])
                    except TypeError:
                        Master[key_word[1]][key_word[2]][key_word[3]] = [key_word[4]]
                else:
                    line = master_common.line_missing(line)

        elif key_word[1] == 'wave':
            line, key_word = master_metocean.wave_type(line, key_word, 2)

            if key_word[2]:
                # waveSet = key_word[2]
                Master['metocean'][key_word[1]]['type'] = key_word[2]
            else:
                line = master_common.line_missing(line)
        
        elif key_word[1] == 'hinges':
            line, key_word = master_common.yesno_word(line, key_word, 2)
            Master['hinges'] = key_word[2]
        
        elif key_word[1] == 'centralisers':
            line, key_word = master_common.item_concept(line, key_word, 2)
            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                continue

            Master[key_word[1]][key_word[2]] = item
        # database includes weigth & time history
        elif key_word[1] == 'data_base':
            key_word, line = master_db.set_database_module(Master, line, 
                                                           key_word, read_data)
        # joint section
        else:
            line, key_word = master_common.item_concept(line, key_word, 2)

            if key_word[2] == 'joint':
                if key_word[1] == 'rule':
                    line, key_word = master_geom.joint_rule(line, key_word, 3)

                    Master[key_word[2]][key_word[1]] = key_word[3]
                elif key_word[1] == 'tolerance':
                    line, key_word = master_geom.joint_tol(line, key_word, 3)
                    try:
                        line, item = master_common.word_pop(line)
                    except IndexError:
                        continue
                    Master[key_word[2]][key_word[1]][key_word[3]] = item
                else:
                    line = master_common.line_missing(line)
            else:
                line = master_common.line_missing(line)
                #
    #
    return key_word


#
#