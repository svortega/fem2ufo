#
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import sys

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.control as process
import fem2ufo.postprocess.usfos.operation.process as ufo_operation

from fem2ufo.master.operation.io_module import io_module
from fem2ufo.master.operation.set_module import set_module
from fem2ufo.master.operation.new_module import new_module
from fem2ufo.master.operation.apply_module import apply_module
from fem2ufo.master.operation.delete_module import delete_module
from fem2ufo.master.operation.optimum_module import optimum_module
from fem2ufo.master.operation.convert_module import convert_module
from fem2ufo.master.operation.read_module import read_module
#
#
#
#
def get_model(control, model):
    """
    Input:/n
    control :/n
    model   :/n
    
    Return:/n
    model
    """
    print('')
    print('----------------- Processing Model -----------------')
    #
    _check_out = []
    # 
    # FE model specifc
    # 
    # ---- Material section ----
    # convert load to mass
    if control.mass['load']:
        _grav = control.gravity
        _dof = control.mass['dof']
        _load_tb_converted = control.mass['load']
        _grav_list = [_grav for x in range(len(_load_tb_converted[0]))]
        _load_tb_converted.append(_grav_list)
        
        #model.load_to_mass('CASE101_', factor=0.8, option='member')

        process.load.convert_load_to_mass(model.component,
                                          model.load,
                                          _load_tb_converted,
                                          _dof)

    # create new material
    _mat_name = control.material['new']['name']
    _mat_number = control.material['new']['number']
    process.material.add_new_material(model.component, 
                                      model.foundation,
                                      _mat_name, 
                                      _mat_number)

    # update existing material
    _mat_name = control.material['update']['name']
    _mat_number = control.material['update']['number']
    process.material.update_material(model.component,
                                     _mat_name, _mat_number)
    # Yelly
    if control.jelly:
        _factor = float(control.jelly['factor'])

        _memb_no = []
        if control.jelly['element']:
            _memb_no.append([control.jelly['element'], _factor])

        _group_no = []
        if control.jelly['element_group']:
            _group_no.append([control.jelly['element_group'], _factor])

        process.material.add_modified_material('jelly', 
                                               model.component,
                                               model.foundation,
                                               _memb_no, _group_no)
    #
    if control.reinforce:
        _factor = float(control.reinforce['factor'])
        
        _memb_no = []
        if control.reinforce['element']:
            _memb_no.append([control.reinforce['element'], _factor])

        _group_no = []
        if control.reinforce['element_group']:
            _group_no.append([control.reinforce['element_group'], _factor])
        
        process.material.add_modified_material('reinforced', 
                                               model.component,
                                               model.foundation,
                                               _memb_no, _group_no)
    
    #
    # ---- Delete section ----
    #
    # deleting elements
    _memb_no = control.delete['element']
    _group_no = control.delete['element_group']
    _msg = process.geometry.delete_elements(model.component,
                                            _memb_no, _group_no)
    _check_out.extend(_msg)
    #  deleting node
    _node_no = control.delete['node']
    _group_no = control.delete['node_group']
    _msg = process.geometry.delete_nodes(model.component,
                                         _node_no, _group_no)
    _check_out.extend(_msg)
    #  deleting load
    _load_no = control.delete['load']
    _group_no = control.delete['load_group']
    _msg = process.load.delete_load(model.load,
                                    _load_no, _group_no)
    _check_out.extend(_msg)
    # check if mass piramid material
    _units_input = control.input['units']
    _msg = process.material.find_mass_piramid_material(model.component)
    _check_out.extend(_msg)
    #
    # ---- processing section ----
    # set mean material
    try:
        _factor = float(control.material['mean'])
        process.material.set_mean_material(model.component,
                                           _factor)
    except ValueError:
        print('    ** warining no mean material defined')
    # 
    # remove eccentricity from members end at join for usfos purposes
    if not control.joint['eccentricity']:
        _msg = ufo_operation.joint_ufo(model.component)
        _check_out.extend(_msg)
    #
    # create consecutive load number
    _fillgap = control.optimize['load_fill_gap']

    if _fillgap:
        process.load.fill_gap_load(model.load.functional)
    #
    #
    return _check_out
#
def get_analysis(master, control, model):
    """
    Get analysis fem2ufo control parameters from master file\n
    
    Input:\n
    master input file\n
    control\n
    model\n
    
    Output:\n
    control file \n
    """

    print('')
    print('----------------- Analysis Module ------------------')
    #
    _fillgap = control.optimize['load_fill_gap']
    _loading = model.load.functional
    _load_no = process.load.find_load_number(_loading, _fillgap)
    #    
    #
    if master.analysis.wave:
        print('wave')
    #
    if 'seismic' in master.analysis.type:
    #if master.analysis.seismic:
        #
        print('--- {:} Analysis'.format(master.analysis.type))
        #
        #control.analysis['seismic']['case'] = []
        #
        _units_output = master.input.units[:5]
        #
        # metocean
        _wave_no = ''
        if model.metocean:
            _load_no += 1
            _wave_no = _load_no
            _wave_name = 'calm_sea'
            _theory = "calm_sea"
            height = 0.0
            period = 10.0
            #
            process.metocean.add_wave(model.metocean,
                                      _wave_no, _wave_name,
                                      _theory, height, period)
        #
        # Foundation
        #_node_total = []
        _th_temp = []
        _depth_temp = {}
        for key, _time_history in master.time_serie.items():
            _th_name = {_th[1]:_th[0] for _th in _time_history.database}
            # soil 
            if _time_history.apply.soil:
            
                _units_input = master.database[key].units[:5]
            
                factors, _grav = process.units.get_factors_and_gravity(_units_input,
                                                                       _units_output)
                
                control.analysis['seismic']['soil_depth'] = {'acceleration': {},
                                                             'displacement': {}}
                # get depth below mudline adjusted by the model's units
                _layers = _time_history.apply.soil.depth
                _layer_levels = [_layer for _layer in sorted(_layers.keys())]
                # check if model soil
                if model.foundation:
                    for x, _layer in enumerate(_layer_levels):
                        _time_series = _time_history.apply.soil.depth[_layer]
                        _depth = float(_layer) * factors[0]
                        # [number, dof, factor, node, top, bottom]
                        _soil_depth = {}
                        for _serie in _time_series:
                            _new_name = _th_name[_serie[0]] +'_soil_' + str(x+1)
                            _soil_depth[_new_name] = [_serie[0], _serie[2], _serie[3], None, None, _depth]                 
                        
                        control.analysis['seismic']['soil_depth'][_serie[1]].update(_soil_depth)
                        
                        if control.output['format'] != 'ufo' :
                            process.foundation.insert_soil_layer(model.foundation, _depth)
                #
                else:
                    print('    *** Error --> No soil defined')
                    sys.exit()
            # pile
            if _time_history.apply.pile:
                _pile_total = {}
                
                _pile_id = _time_history.apply.pile.name
                for x, _serie in enumerate(_pile_id):
                    _new_name = _th_name[_serie[0]] + '_pile_' + str(x+1)
                    _pile = [_serie[4]]
                    member_list = process.concept.find_fe_number_by_concept_name(model.foundation.piles.concepts,
                                                                                 _pile)                    
                    _pile_total[_new_name] = [_serie[0], _serie[2], _serie[3], member_list]
                
                _pile_id = _time_history.apply.pile.number
                for x, _serie in enumerate(_pile_id):
                    _new_name = _th_name[_serie[0]] + '_pile_' + str(x+1)
                    member_list = [_serie[4]]
                    _pile_total[_new_name] = [_serie[0], _serie[2], _serie[3], member_list]
                
                control.analysis['subsidence']['pile']['displacement'] = _pile_total 
            # member & node
            x = 0
            _node_total = {'acceleration': {},
                           'velocity': {},
                           'displacement': {}}
            
            if _time_history.apply.element:
                if _time_history.apply.element.name:
                    _memb_number = []
                    _node_number = []
                    for _serie in _time_history.apply.element.name:
                        _memb_name = [_serie[4]]
                        _node = process.concept.find_node_in_element_end(model.component,
                                                                         _memb_name,
                                                                         _memb_number,
                                                                         _node_number)
                        if _node:
                            x += 1
                            _new_name= _th_name[_serie[0]] + '_node_' + str(x)
                            _node_total[_serie[1]][_new_name] = [_serie[0], _serie[2], _serie[3], _node]
                
                if _time_history.apply.element.number:
                    _memb_name  = []
                    _node_number = []
                    for _serie in _time_history.apply.element.number:
                        _memb_number = [_serie[4]]
                        _node = process.concept.find_node_in_element_end(model.component,
                                                                         _memb_name,
                                                                         _memb_number,
                                                                         _node_number)
                        if _node:
                            x += 1
                            _new_name= _th_name[_serie[0]] + '_node_' + str(x+1)
                            _node_total[_serie[1]][_new_name] = [_serie[0], _serie[2], _serie[3], _node]
            #
            if _time_history.apply.node:
                _node_number = _time_history.apply.node.number
                for _serie in _node_number:
                    _node = process.concept.find_node([_serie[4]], model.component)
                    if _node:
                        x += 1
                        _new_name= _th_name[_serie[0]] + '_node_' + str(x+1)
                        _node_total[_serie[1]][_new_name] = [_serie[0], _serie[2], _serie[3], _node]
        #
        #
        control.analysis['seismic']['node'] = _node_total
        #
        print('--- End Seismic module')
        print('')
        #
    else:
        control.analysis['seismic'] = None
    #
    if master.analysis.ship_impact:
        import analysisModule as analysisMod
        print('--- {:} Analysis'.format(master.analysis.type))

        ms = float(master.analysis['ship_impact'].vessel.mass)
        vs = float(master.analysis['ship_impact'].vessel.velocity)
        mi = 0
        vi = 0
        J = 0
        z = 0
        if master.analysis['ship_impact'].installation:
            mi = float(master.analysis['ship_impact'].installation.mass)
            vi = float(master.analysis['ship_impact'].installation.velocity)

            try:
                J = float(master.analysis['ship_impact'].installation.J)
                z = float(master.analysis['ship_impact'].installation.zpivot)
            except:
                pass

        print('    ** Calculating collision energy (Es)')
        master.analysis['ship_impact'].Es = analysisMod.collisionEnergy(ms, vs, mi, vi, J, z)

        print('    ** Calculating ship identation curve')
        impact_force = master.analysis['ship_impact'].scenario
        # fix here
        D = 4
        d1, p1, p2, p3 = analysisMod.shipDeformationCurve(impact_force, D)

        if master.analysis.type == 'static':
            master.analysis['ship_impact'].vessel.identation = [d1, p1, p2, p3]
            # print('ok')
    else:
        control.analysis['ship_impact'] = None
    #
    #
    return control, model
    #
#

