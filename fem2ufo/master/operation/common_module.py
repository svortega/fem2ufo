#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import re, sys, os
from copy import deepcopy


# package imports
import fem2ufo.process.control as process


# --------------------------------------------------------------------
#                               Main
# --------------------------------------------------------------------
#
#
def check_file(file, _ext='fem'):
    """
    Check if file exist in directory
    """
    # check file extension
    file = str(file)
    extension = file.split(".")[-1]
    extension = str(extension).lower()
    _ext = str(_ext).strip()

    if extension != _ext:
        file = str(file) + '.' + str(_ext)

    try:
        _input = open(file)
    except IOError:
        print("    ** I/O Error : master file {:} no found"
              .format(file))
        retval = os.getcwd()
        print("       in the current directory :", retval)
        sys.exit()

    return file
#
#
# -----------------------
# common modules
#
#
def aux_word(line_in, key_word, ind=1):
    """
    """
    _key = {"to": r"\b(to)\b",
            "from": r"\b(from)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
def yesno_word(line_in, key_word, ind=0):
    """
    """
    _key = {"on": r"\b(y(es)?|on)\b",
            "off": r"\b(n[o]?|off)\b"}

    #
    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
    #
#
def inout_word(line_in, key_word, ind=0):
    """
    """
    _key = {"input": r"\b(input)\b",
            "output": r"\b(output)\b"}

    #
    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
    #
#
def coord_word(line_in, key_word, ind=0):
    """
    Return x, y, z coordinates
    """
    _key = {"x": r"\b(x|east)\b", 
            "y": r"\b(y|north)\b", 
            "z": r"\b(z|vertical)\b"}
    #
    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
def line_missing(line):
    """
    """
    print('    **  error in line : {:}'.format(line))
    print('    --> program terminated')
    sys.exit()
    # 
#
#
# -----------------------
#
#
def word_pop(line_in, count=1):
    """
    """
    _word = str(line_in).split()
    _temp = re.compile(_word[0])
    line_out = re.sub(_temp, " ", line_in, count)
    line_out = str(line_out).strip()
    word_out = str(_word[0]).strip()
    #
    return line_out, word_out
#
#
# -----------------------
#
def aux_fe(line_in, key_word, ind=2):
    """
    """
    _key = {"group": r"\b(group[s]?)\b"}
    # "load" : r"\b(load[s]?|action[s]?)\b",}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
#
def item_fe(line_in, key_word, ind=2):
    """
    FE components
    """
    _key = {"node": r"\b(node[s]?)\b",
            "element": r"\b(element[s]?|member[s]?)\b",
            "material": r"\b(mat(erial)?[s]?)\b",
            "offset": r"\b(offset[s]?)\b",
            "load": r"\b(load[s]?|action[s]?)\b",
            "pile": r"\b(pile[s]?)\b",
            "soil": r"\b(soil|foundation)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
# -----------------------
#
def item_concept(line_in, key_word, ind=2):
    """
    Returns GeniE concept
    """
    _key = {"joint": r"\b(j[o]?[i]?nt[s]?|chj[o]?[i]?nt[s]?)\b",
            "gap": r"\b(gap[s]?|shim[s]?)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
#
def item_id(line_in, key_word, ind=0):
    """
    """
    _key = {"number": r"\b(n[u]?mb[e]?r[s]?)\b",
            "name": r"\b(n[a]?m[e]?[s]?|id[s]?)\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
#
def beam_end(line_in, key_word, ind=4):
    """
    """
    _key = {"1": r"\b((end|position)?\s*(1|one))\b",
            "2": r"\b((end|position)?\s*(2|two))\b",
            "3": r"\b((end|position)?\s*(3|mid(dle|del)?|halfway|midway|cent(er|re)))\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
#
def item_option(line_in, key_word, ind=0):
    """
    """
    _key = {"option": r"\b(option|choise|selection)\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def item_part(line_in, key_word, ind=0):
    """
    """
    _key = {"brace": r"\b(brace)\b",
            "leg": r"\b(leg)\b",
            "pile": r"\b(pile)\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
#
def step_word(line_in, key_word, ind=2):
    """
    """
    _key = {"start": r"\b(start|from)\b",
            "end": r"\b(end|to)\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
#
