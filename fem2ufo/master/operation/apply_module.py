#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.process.control as process
import fem2ufo.master.operation.common_module as master_common
import fem2ufo.master.model.analysis as master_analysis
import fem2ufo.master.model.database as master_db

# ----------------------
#
def apply_words(line_in, key_word, ind=1):
    """
    """
    _key = {"hydropressure": r"(ext(ernal)?)?\s*h((ydro)?static)?\s*pres(sure)?",
            "fracture": r"\b(frac(ture[d]?)?)\b",
            "linear": r"\b(linear)\b",
            "nonstructural": r"\b(non\s*stru(ctural)?)\b",
            "reinforce" : r"\b(re\s*inforce(d)?)\b",
            "jelly": r"\b(jelly|spaghet(ti)?[y]?|rubber)\b",
            "control": r"\b(control)\b",
            "overlapping": r"\b(overlapping|internal)\b",
            "centralisers": r"\b(centraliser(s)?)\b",
            "wave": r"\b(wave)\b",
            "wind": r"\b(wind)\b",
            "database" : r"\b(data\s*base)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
def apply_module(Master, line, key_word):
    """
    """
    while line:
        line, key_word = apply_words(line, key_word, 1)

        if not line:
            continue
        # control/wave section
        elif key_word[1]:
            if key_word[1] == 'control':
                line, key_word = master_analysis.motion_word(line, key_word, 2)
                line, key_word = master_common.item_fe(line, key_word, 3)
                line, key_word = master_common.item_id(line, key_word, 4)

                if not line:
                    continue
                line, item = master_common.word_pop(line)

                # from member end 1/2
                if key_word[3] == 'element':
                    line, key_word = master_common.beam_end(line, key_word, 5)

                    try:
                        Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]].append([item, key_word[5]])
                    except TypeError:
                        Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]] = []
                        Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]].append([item, key_word[5]])
                # from node & remove offsets
                else:
                    try:
                        Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]].append(int(item))
                    except TypeError:
                        Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]] = [int(item)]
            #
            elif key_word[1] == 'wave':
                # if waveSet == '' :
                #    print('    **   wave type must be set')
                #    line = lineMissing(line)

                line, key_word = wave_word(line, key_word, 2)

                if key_word[2] == 'data':
                    line, key_word = wave_data(line, key_word, 3)

                    try:
                        line, item = master_common.word_pop(line)
                    except IndexError:
                        continue

                    Master['metocean'][key_word[1]][key_word[2]][key_word[3]] = item
                
                elif key_word[2] == 'spectrum':
                    line, key_word = wave_spectrum(line, key_word, 3)

                    if key_word[3] == 'user':
                        line, key_word = input_word(line, key_word, 4)

                        if key_word[4] == 'file':
                            try:
                                line, item = master_common.word_pop(line)
                            except IndexError:
                                continue

                            Master['metocean'][key_word[1]][key_word[2]][key_word[3]][key_word[4]] = item

                        else:
                            line, key_word = wave_spectrum_data(line, key_word, 4)
                            try:
                                line, item = master_common.word_pop(line)
                            except IndexError:
                                continue

                            try:
                                Master['metocean'][key_word[1]][key_word[2]][key_word[3]][key_word[4]].append(item)
                            except TypeError:
                                Master['metocean'][key_word[1]][key_word[2]][key_word[3]][key_word[4]] = [item]

                    elif key_word[3] == 'jonswap':
                        line, key_word = greek_word(line, key_word, 4)
                        try:
                            line, item = master_common.word_pop(line)
                        except IndexError:
                            continue

                        Master['metocean'][key_word[1]][key_word[2]][key_word[3]][key_word[4]] = item

                    elif key_word[3] == 'pm':
                        Master['metocean'][key_word[1]][key_word[2]][key_word[3]] = 'on'

                    else:
                        line = master_common.line_missing(line)

                elif key_word[2] == 'grid':
                    line, key_word = input_word(line, key_word, 3)

                    if key_word[3] == 'file':
                        try:
                            line, item = master_common.word_pop(line)
                        except IndexError:
                            continue

                        Master['metocean'][key_word[1]][key_word[2]]['type'][key_word[3]][key_word[4]][
                            key_word[5]] = item
                    else:
                        line = master_common.line_missing(line)

                else:
                    line = master_common.line_missing(line)

                    # elif key_word[1] == 'wid':
            #
            elif key_word[1] == 'database':
                key_word, line = master_db.apply_database_module(Master, line, key_word)
            # 
            # linear/ nonstructural/jelly/reinforce section/ centralisers
            else:
                line, key_word = master_common.item_fe(line, key_word, 2)
                # print(' ***-',line)
                if key_word[2]:
                    line, key_word = master_common.aux_fe(line, key_word, 3)
                    # groups
                    if key_word[3] == 'group':
                        line, key_word = master_common.item_id(line, key_word, 4)
                        line, key_word = master_common.step_word(line, key_word, 5)
                        if not key_word[5]:
                            try:
                                line, item = master_common.word_pop(line)
                            except IndexError:
                                continue

                            try:
                                Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]]['start'].append(item)
                                Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]]['end'].append(item)
                            except TypeError:
                                Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]]['start'] = [item]
                                Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]]['end'] = [item]
                            except:
                                line = master_common.line_missing(line)

                        else:
                            try:
                                line, item = master_common.word_pop(line)
                            except:
                                continue

                            try:
                                Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]].append(item)
                            except TypeError:
                                Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]] = [item]
                            except:
                                line = master_common.line_missing(line)
                    # members
                    else:
                        line, key_word = master_common.item_id(line, key_word, 3)
                        line, key_word = master_common.step_word(line, key_word, 4)
                        if not key_word[4]:
                            try:
                                line, item = master_common.word_pop(line)
                            except IndexError:
                                continue

                            try:
                                Master[key_word[1]][key_word[2]][key_word[3]]['start'].append(item)
                                Master[key_word[1]][key_word[2]][key_word[3]]['end'].append(item)
                            except TypeError:
                                Master[key_word[1]][key_word[2]][key_word[3]]['start'] = [item]
                                Master[key_word[1]][key_word[2]][key_word[3]]['end'] = [item]

                            except:
                                line = master_common.line_missing(line)
                        # 
                        else:
                            try:
                                line, item = master_common.word_pop(line)
                            except:
                                continue

                            try:
                                Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]].append(item)
                            except TypeError:
                                Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]] = [item]
                            except:
                                line = master_common.line_missing(line)
                else:
                    line = master_common.line_missing(line)
        # analysis section
        else:
            analysis_type = Master['analysis']['type']
            # line, key_word = analysisOptions(line, key_word, 1)
            if analysis_type == 'seismic':
                if not _time_history:
                    print('error -- > set time history')
                    line = master_common.line_missing(line)

                line, key_word = seismic_word(line, key_word, 2)
                # prescribed
                if key_word[2] == 'prescribed':
                    # type motion
                    line, key_word = motion_word(line, key_word, 3)
                    Master['analysis'][analysis_type]['case'] = key_word[3]

                    line, key_word = item_fe(line, key_word, 4)

                    if not key_word[3]:
                        master_common.line_missing(line)
                    # Soil
                    elif key_word[4] == 'soil':
                        line, key_word = motion_application(line, key_word, 5)
                        # print(key_word[3], item)
                        try:
                            line, item = master_common.word_pop(line)
                        except IndexError:
                            continue

                        try:
                            Master['analysis'][analysis_type][key_word[4]][key_word[5]].append([float(item), _time_history])
                        except TypeError:
                            Master['analysis'][analysis_type][key_word[4]][key_word[5]] = [[float(item), _time_history]]

                    # member & node
                    else:
                        line, key_word = item_id(line, key_word, 5)
                        line, item = master_common.word_pop(line)
                        line, key_word = beam_end(line, key_word, 6)

                        if key_word[4] == 'element' and not key_word[6]:
                            line = master_common.line_missing(line)
                            #

                        try:
                            Master['analysis'][analysis_type][key_word[4]][key_word[5]].append(
                                [item, key_word[6], _time_history])
                        except TypeError:
                            Master['analysis'][analysis_type][key_word[4]][key_word[5]] = []
                            Master['analysis'][analysis_type][key_word[4]][key_word[5]].append(
                                [item, key_word[6], _time_history])
            
            elif analysis_type == 'ship_impact':
                line, key_word = shipimpact_word(line, key_word, 2)

                if key_word[2] == 'scenario':
                    line, key_word = item_part(line, key_word, 3)
                    Master['analysis'][key_word[1]][key_word[2]] = key_word[3]

                elif key_word[2] == 'vessel':
                    line, key_word = vessel_word(line, key_word, 3)
                    line, item = master_common.word_pop(line)
                    Master['analysis'][key_word[1]][key_word[2]][key_word[3]] = item

                elif key_word[2] == 'installation':
                    line, key_word = installation_word(line, key_word, 3)
                    line, item = master_common.word_pop(line)
                    Master['analysis'][key_word[1]][key_word[2]][key_word[3]] = item
                    line = ''
                    # member
                else:
                    line, key_word = item_fe(line, key_word, 2)
                    line, key_word = item_id(line, key_word, 3)
                    line, item = master_common.word_pop(line)
                    line, key_word = beam_end(line, key_word, 4)
                    if not key_word[4]:
                        line = master_common.line_missing(line)

                    try:
                        Master['analysis'][key_word[1]][key_word[2]][key_word[3]][item].append(key_word[4])
                    except TypeError:
                        Master['analysis'][key_word[1]][key_word[2]][key_word[3]][item] = [key_word[4]]
                        # Master[key_word[1]][key_word[2]][key_word[3]][key_word[4]].append([item, key_word[5]])
                    except UnboundLocalError:
                        line = master_common.line_missing(line)
            else:
                line = master_common.line_missing(line)
    #
    return key_word
#