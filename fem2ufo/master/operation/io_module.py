#
# Copyright (c) 2009-2018 fem2ufo
# 

# Python stdlib imports
import re
from copy import deepcopy


# package imports
import fem2ufo.process.control as process
import fem2ufo.master.operation.common_module as master_common

#
def item_model(line_in, key_word, ind=1):
    """
    Return input model type
    """
    _key = {"format": r"\b(f[o]?rm[a]?t)\b",
            "header": r"\b(header[s]?)\b",
            "geometry": r"\b(geo(metry)?|model)\b",
            "loading": r"\b(load(ing)?[s]?|opt(ional)?)\b",
            "foundation": r"\b(soil|found(ation)?[s]?)\b",
            "metocean": r"\b(met(ocean)?|wave|wind|environment|hydro(dynamic[s]?)?)\b",
            "database" : r"\b((weight\s*)?data\s*base)\b"}
            #"time_history" : r"\b(t(ime)?\s*h(istory)?)\b"}
    #
    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
#
def format_options(line_in, key_word, ind=0):
    """
    Return format 
    """
    #
    _key = {"ufo": r"\b(ufo|usfos|tore[s]?)\b",
            "sesam": r"\b(fem|(clasic\s*)?sesam)\b",
            "xml": r"\b(xml|web)\b",
            "js": "\b(genie|j(ava)?s(cript)?)\b",
            "abaqus": r"\b(ab(b)?a([cqk]us)?)\b",
            "asas": r"\b(asas)\b",
            "sacs": r"\b(sacs)\b",
            "staad": r"\b(staad)\b",
            "csv" : r"\b(csv)\b",
            "text" : r"\b(text)\b",
            "excel" :r"\b(excel|spread\s*sheet)\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
    #
#
def excel_options(line_in, key_word, ind=0):
    """
    Return format 
    """
    #
    _key = {"tap" :r"\b(sheet|tap)\b"}

    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
# -----------------------
#
def item_units(line_in, key_word, ind=0):
    """
    Returns units items
    """
    _key = {"length": r"\b(l(a|e)ng(th|[d]?e)?|long(itud|ueur)?|largo|disp(lacement))\b",
            "force": r"\b(force|fuerza|kraft)\b",
            "mass": r"\b(mass(e[n]?)?|masa)\b",
            "temperature": r"\b(temp(eratur(e|a)?)?)\b",
            "acceleration": r"\b(acc(eleration)?)\b",
            "velocity" : r"\bvel(ocity)?\b"}
    #
    key_word[ind], line_out, _match = process.common.match_keywords(line_in, _key, 
                                                                    key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)

    return line_out, key_word
#
def units_module(item, key_word, _no):
    """
    Allocated units items
    """
    #
    if key_word[_no] == 'length':
        item = process.units.find_length_unit(item)

    elif key_word[_no] == 'force':
        item = process.units.find_force_unit(item)

    elif key_word[_no] == 'mass':
        item = process.units.find_mass_unit(item)

    elif key_word[_no] == 'temperature':
        item = process.units.find_temperature_unit(item)
    
    elif key_word[_no] == 'acceleration':
        item = process.units.find_acceleration_unit(item)
    
    #elif key_word[_no] == 'acceleration':
    #    item = process.units.find_mass_unit(item)

    else:
        line = master_common.line_missing(item)

    return item
#
# ----------------------
# input/output modules
#
def input_word(line_in, key_word, ind=0):
    """
    Returns master input commands
    """
    #
    _key = {"file": r"\b(file[s]?)\b",
            #"time": r"\b(time)\b",
            "path": r"\b(path)\b",
            "units": r"\b(unit[s]?)\b"}

    key_word[ind], line_out, _match = process.common.search_line(line_in, _key, 
                                                                 key_word[ind])

    if _match:
        process.common.update_key_word(key_word, ind)
    #
    return line_out, key_word
#
#
def io_module(Master, line, key_word, _units):
    """
    Module identify lines with input commands in Master file
    """
    while line:
        line, key_word = input_word(line, key_word, 1)
        
        if not key_word[1] :
            line = master_common.line_missing(line)
        
        elif key_word[1] == 'units':
            line, key_word = item_units(line, key_word, 2)

            try:
                line, item = master_common.word_pop(line)
            except IndexError:
                continue

            item = units_module(item, key_word, 2)

            if key_word[2] == "length":
                _units[0] = item

            elif key_word[2] == "mass":
                _units[1] = item

            elif key_word[2] == "time":
                _units[2] = item

            elif key_word[2] == "temperature":
                _units[3] = item

            elif key_word[2] == "force":
                _units[4] = item
            
            elif key_word[2] == "acceleration":
                _units[5] = item
                # FIXME : line should be removed
                line = ""
            else:
                line = master_common.line_missing(line)
            #
            key_word[2] = None
        elif key_word[1] == 'path':
            #try:
            line, item = master_common.word_pop(line)
            #except:
            #    line = master_common.line_missing(line)
            if len(item) == 1:
                Master[key_word[0]][key_word[1]] = item + ':' + line
            else:
                Master[key_word[0]][key_word[1]] = item

            line = None
        else:
            line, key_word = item_model(line, key_word, 2)

            if 'format' in key_word[2]:
                line, key_word = format_options(line, key_word, 3)
                if not key_word[3]:
                    print('   *** error file format {:} not available'
                          .format(line))
                    line = master_common.line_missing(line)
                
                Master[key_word[0]][key_word[2]] = key_word[3]
                Master[key_word[0]]['units'] = deepcopy(_units)
                continue
            #
            line, item = master_common.word_pop(line)
            #
            if 'database' in key_word[2]:
                _step = len(Master[key_word[2]])
                _name = item
                #
                Master[key_word[2]][_name][key_word[1]] = item
                _format = Master[key_word[0]]['format']
                Master[key_word[2]][_name]['format'] = deepcopy(_format)
                Master[key_word[2]][_name]['units'] = deepcopy(_units)
                if Master['input']['path']:
                    Master[key_word[2]][_name]['path'] = deepcopy(Master['input']['path'])
                else:
                    Master[key_word[2]][_name]['path'] = None
                
                if 'excel' in _format:
                    line, key_word = excel_options(line, key_word, 3)
                    try:
                        line, item = master_common.word_pop(line)
                    except IndexError:
                        continue
                    #
                    Master[key_word[2]][_name][key_word[3]] = item
                    #print('-->')
                ##
                #_analysis_type = Master['analysis']['type']
                ##
                #if not Master['analysis']['name']:
                #    Master['analysis']['name'] = _analysis_type + '_' + str(_step)
                #_name_analysis = Master['analysis']['name']
                ##
                #try:
                #    Master['analysis'][_analysis_type][_name_analysis]['files'].append(_name)
                #except TypeError:
                #    Master['analysis'][_analysis_type] = {}
                #    Master['analysis'][_analysis_type][_name_analysis] = {'type' : _analysis_type,
                #                                                          'name' : _name_analysis,
                #                                                          'number': 1, 'database' : [_name]}
            else:
                Master['model'][key_word[2]][key_word[1]] = item
                Master['model'][key_word[2]]['format'] = deepcopy(Master[key_word[0]]['format'])
                Master['model'][key_word[2]]['units'] = deepcopy(_units)
                Master['model'][key_word[2]]['tolerance'] = deepcopy(Master[key_word[0]]['tolerance'])
                if Master['input']['path']:
                    Master['model'][key_word[2]]['path'] = deepcopy(Master['input']['path'])
                else:
                    Master['model'][key_word[2]]['path'] = None
    #
    return key_word, _units
    #
#