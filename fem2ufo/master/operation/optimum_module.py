#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports
import re


# package imports
import fem2ufo.master.operation.common_module as master_common
import fem2ufo.master.model.load as master_load

#
#
#
# ----------------------
#
def optimum_module(Master, line, key_word):
    """
    """
    while line:
        line, key_word = master_common.item_fe(line, key_word, 1)

        if key_word[1] == 'load':
            line, key_word = master_load.opt_word(line, key_word, 2)
            Master[key_word[1]][key_word[0]] = key_word[2]
        else:
            line = line_missing(line)

    return key_word


#