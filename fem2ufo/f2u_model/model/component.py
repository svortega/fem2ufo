# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import sys

# package imports
import fem2ufo.f2u_model.feclass.geometry as geometry
import fem2ufo.f2u_model.control as f2u

#
def get_number(items):
    """
    return maximum consecutive number of items
    """
    _number = [item.number for item in items.values()]
    
    try:
        return max(_number) 
    except ValueError:
        return 0

# GEOMETRY Section
class Component:
    """
    FE Geometry model class
    
    **Parameters**:
      :number: integer
      :name: string
      :nodes: list of node's class
      :elements: list of element's class
      :materials: list of material's class
      :sets: list of groups (elements, nodes, etc)
      :sections: list of section's class
      :vectors: list of guide points
      :eccentricities: list of eccentricities
      :joints: list of joint's class
      :hinges: list of hinges definitios
      :loads: list of load's class
      :data: FE model data
      :units: FE model units
      :soil: list of soil's class
      :hydrodynamics: hydrodynamic's data
      :boundaries: list of FE model boundary
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    __slots__ = ('number', 'name', 'nodes', 'elements', 'materials',
                 'sets', 'sections', 'vectors', 'eccentricities',
                 'joints', 'hinges', 'loads', 'data', 'units',
                 'boundaries', 'cdcm', 'concepts')
    #
    def __init__(self, name, number=None):
        """
        """
        self.number = number
        self.name = name
        self.nodes = {}
        self.joints = {}
        self.boundaries = {}
        self.materials = {}
    #
    def add_node(self, name, number=None,
                 coordinates=None):
        """
        """
        if not number:
            number = get_number(self.nodes) + 1
        
        try:
            self.nodes[name]
            #print('   *** error node {:} already exist'.format(name))
            return
        except KeyError:
            self.nodes[name] = geometry.Node(name, number)
        
        if coordinates:
            if type(coordinates) == list:
                self.nodes[name].set_coordinates(coordinates[0],
                                                 coordinates[1],
                                                 coordinates[2])
            elif type(coordinates) == dict:
                self.nodes[name].set_coordinates(coordinates['x'],
                                                 coordinates['y'],
                                                 coordinates['z'])
            else:
                print('   *** error Node input format not recognized')
    #
    def add_joint(self, name, node_name):
        """
        """
        #print(type(node_name))
        
        try:
            self.nodes[node_name]
        except KeyError:
            print('   *** error : Node {:} not found'.print(node_name))
            sys.exit
        
        self.joints[name] = geometry.Joints(name, self.nodes[node_name])
    #
    def add_boundary(self, name, node_name=False,
                     element_name=False, boundary=False):
        """
        """
        
        self.boundaries[name] = geometry.Boundary(name)
        
        if node_name:
            try:
                self.nodes[node_name]
            except KeyError:
                print('   *** error : Node {:} not found'.print(node_name))
                sys.exit
            
            self.nodes[node_name].set_boundary(self.boundaries[name])
            self.boundaries[name].nodes.append(node_name)
        #
        elif element_name:
            print('fix boundary element here')
            sys.exit()
        #
        if boundary:
            print(' add here boundary')
    #
    def add_material(self, name, number=None):
        """
        """
        if not number:
            number = get_number(self.materials) + 1
        
        self.materials[name] = f2u.Material(name, number)
    #
    #def add_group(self, name, number=None):
    #    """ """
    #    pass
#