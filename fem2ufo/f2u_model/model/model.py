# 
# Copyright (c) 2009-2020 fem2ufo
# 


# Python stdlib imports
import sys

# package imports
from fem2ufo.f2u_model.model.component import Component
#from fem2ufo.f2u_model.feclass.material import Material



#
class Analysis:
    """
    """
    __slots__ = ('number', 'name', 'case')
    #
    def __init__(self, name, number=None):
        #
        self.number = number
        self.name = name
        self.case = {}
#
class Load:
    """
    FE Load Cases
    
    LoadType
        |_ name
        |_ number
        |_ functional
        |_ combination_level
        |_ time_domain
        |_ frequency_domain
        |_ temperature
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'functional',
                 'time_domain', 'frequency_domain',
                 'temperature', 'data', 'equipment')
    #
    def __init__(self, Name, Number=None):
        #
        self.number = Number
        self.name = Name
        self.functional = {}
        self.equipment = {}
        self.time_domain = {}
        self.frequency_domain = {}
        self.temperature = {}
        #

#
class Metocean:
    """
    FE Metocean Class
    
    Metocean
        |_ name
        |_ number
        |_ data
        |_ type
        |_ wave
        |_ current
        |_ wind
        |_ cdcm
        |_ non_hydro
        |_ elevation
        |_ hydro_diameter
        |_ buoyancy
        |_ flooded
        |_ seastate
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
      
      : seastate : metocean combination
    """
    #
    # TODO: need to remove cdcm, this is member property
    __slots__ = ('number', 'name', 'wave', 'current', 'data',
                 'marine_growth', 'wind', 'cdcm', 'non_hydro', 
                 'condition', 'hydro_diameter', 'buoyancy',
                 'flooded', 'air_drag', 'buoyancy_area')
    #
    def __init__(self, name, number=None):
        #
        self.number = number
        self.name = name
        self.hydro_diameter = {}
        self.buoyancy_area = {}
        self.non_hydro = False
        self.marine_growth = {}
        self.buoyancy = {}
        self.flooded = {}
        self.air_drag = {}
#
class Foundation:
    """
    """
    __slots__ = ('number', 'name', 'soil',
                 'data', 'piles', 'materials')
    #
    def __init__(self, Name, Number=None):
        #
        self.number = Number
        self.name = Name
        self.soil = {}
#
#
#
class F2U_model:
    """
    fem2ufo fe concept model
    
    Model
        |_ name
        |_ number
        |_ data
        |_ component
        |_ load
        |_ foundation
        |_ metocean
        |_ analysis
    
    """
    __slots__ = ('number', 'name', 'data',
                 'component', 'load', 'foundation',
                 'metocean', 'analysis')
    
    def __init__(self, Name, Number=None):
        #
        self.number = Number
        self.name = Name
        self.data = {}
        self.component = None
        self.load = None
        self.foundation = None
        self.metocean = None
        self.analysis = None
    #
    def set_component(self, name=None, number=None):
        """
        component
            |_ number: integer
            |_ name: string
            |_ nodes: list of node's class
            |_ elements: list of element's class
            |_ materials: list of material's class
            |_ sets: list of groups (elements, nodes, etc)
            |_ sections: list of section's class
            |_ vectors: list of guide points
            |_ eccentricities: list of eccentricities
            |_ joints: list of joint's class
            |_ hinges: list of hinges definitios
            |_ loads: list of load's class
            |_ data: FE model data
            |_ units: FE model units
            |_ boundaries: list of FE model boundary
        """
        if not name:
            name = self.name
        
        self.component = Component(name, self.number)
    #
    def set_load(self, name=None, number=None):
        """
        LoadType
            |_ name
            |_ number
            |_ functional
            |_ combination_level
            |_ time_domain
            |_ frequency_domain
            |_ temperature
        """
        if not name:
            name = self.name
        
        self.load = Load(name, number)
    #
    def set_foundation(self, name=None, number=None):
        """
        """
        if not name:
            name = self.name
        
        self.foundation = Foundation(name, self.number)        
    #
    def set_metocean(self, name=None, number=None):
        """
        metocean
            |_ name
            |_ number
            |_ data
            |_ type
            |_ wave
            |_ current
            |_ wind
            |_ cdcm
            |_ air_drag
            |_ non_hydro
            |_ elevation
            |_ hydro_diameter
            |_ buoyancy
            |_ flooded
            |_ seastate
        """
        if not name:
            name = self.name
        
        self.metocean = Metocean(name, self.number)        
    #
    def set_analysis(self, name=None, number=None):
        """
        """
        if not name:
            name = self.name
        
        self.analysis = Analysis(name, self.number)        
    #

#
#