# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports


# package imports
import fem2ufo.f2u_model.operations.geometry as geomop

#
#
# HYDRODYNAMICS Section
#
class Condition():
    """
    FE Metocean Condition Class
    
    Condition
        |_ name
        |_ number
        |_ surface
        |_ gravity
        |_ water_density
        |_ air_density
        |_ fluid_viscosity
        |_ air_viscosity
        |_ zone
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    __slots__ = ('number', 'name', 'surface', 'mudline',
                 'gravity', 'water_density', 'air_density',
                 'fluid_viscosity', 'air_viscosity',
                 'water_depth')

    def __init__(self, name, number=None):
        self.number = number
        self.name = name
        # self.mudline = 0
        self.surface = 0
        #
        self.gravity = 9.81  # m/s2
        # Density (especific mass)
        self.water_density = 1025.0  # kg/m3
        self.air_density = 1.226  # kg/m3
        # Kinematic viscosity
        self.fluid_viscosity = 1.19e-6  # m2/s
        self.air_viscosity = 1.462e-5  # m2/s
        #
        self.water_depth = []
#
class Wave():
    """
    FE Wave Class
    
    Wave
        |_ name
        |_ number
        |_ theory
        |_ order
        |_ height
        |_ period
        |_ phase
        |_ direction
        |_ cdcm
        |_ time
        |_ step
        |_ number_step
        |_ kinematics
        |_ current
        |_ c_blockage
        |_ c_stretching
        |_ buoyancy
        |_ wind
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'theory', 'height', 'period', 'water_depth',
                 'phase', 'time', 'step', 'number_step', 'order', 'direction',
                 'items', 'length')
    #
    def __init__(self, name,  number):
        #
        self.number = number
        self.name = name
        self.order = None
        self.theory = None
        #
        #
        self.water_depth = None
        self.items = []
        #
        #
#
class Current():
    """
    FE Current Class
    
    Current
        |_ name
        |_ number
        |_ profile
        |_ items
        |_ sets
        |_ absolute_elevation : True if elevations are relative to seabed, 
                                False if elevations are relative SWL Z (default)
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'profile',
                 'velocity', 'alignment', 'direction',
                 'items', 'sets', 'absolute_elevation',
                 'class_type', 'water_depth')
    #
    def __init__(self, name, number):
        #
        self.number = number
        self.name = name
        # CurrentProfile
        self.profile = []
        # Current Velocity
        self.items = []
        self.sets = {}
        #
        self.absolute_elevation = False
        #
        self.class_type = 'current'

#
class Wind():
    """
    FE Wind Class
    
    Wind
        |_ name
        |_ number
        |_ type
        |_ profile
        |_ items
        |_ sets
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'items', 'sets', 'pressure',
                 'velocity', 'alignment', 'gust_factor', 'height',
                 'period_ratio', 'formula', 'power', 'method', 
                 'density', 'class_type', 'direction', 'water_depth')
                 #'duration')
    #
    def __init__(self, name, number):
        #
        self.number = number
        self.name = name
        self.power = 0.20
        self.gust_factor = None
        self.items = []
        self.sets = {}
        #self.duration = 1.0 # hr
        self.height = 10.0 # m
        self.period_ratio = 1.0
        self.pressure = None
        #
        self.class_type = 'wind'
        self.alignment = 'x axis'
        #
#
class MarineGrowth():
    """
    FE Marine Growth Class
    
    MarineGrowth
        |_ name
        |_ number
        |_ type : constant_thickness / depth_profile
        |_ profile
        |_ inertia
        |_ factor
        |_ items
        |_ sets
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'profile', 'option',  
                 'inertia', 'factor', 'density', 'roughness',
                 'items', 'sets', 'type', 'sea_water_density', 
                 'density_factor', 'thickness', 'absolute_elevations')
    #
    def __init__(self,  name, number):
        #
        self.number = number
        self.name = name
        self.profile = [] # [z-level, thickness, roughness]
        self.inertia = True
        self.absolute_elevations = False
        self.density_factor = 1.36
        self.density = 1400.0 # kg/m3
        self.items = []
        self.sets = {}
        #self.zone = None
        self.roughness = 0
        #
        self.sea_water_density = 1032.0 # kg/m3
    #
    def get_density_factor(self, density):
        """
        """
        self.density_factor = density/self.sea_water_density
        
        return self.density_factor
    #
    def constant(self, thickness, roughness, 
                 density_factor=None):
        """
        Marine Growth defined with a Constant thickness
        """
        self.type = 'constant_thickness'
        self.thickness = thickness
        self.roughness = roughness
        if density_factor:
            self.density_factor = density_factor
    #
    def depth_profile(self):
        """
        """
        self.type = 'profile'
    #
#
class DragMassCoefficient():
    """
    type : specified, profile, diameter, rule, roughness/Reynolds number, roughness/KC number
    smooth  = [cd_Normal_x, cd_Longitudinal_y , cd_Longitudinal_z, cm_Normal_x, cm_Longitudinal_y , cm_Longitudinal_z]
    rough = [cd_Normal_x, cd_Longitudinal_y , cd_Longitudinal_z, cm_Normal_x, cm_Longitudinal_y , cm_Longitudinal_z]
    
    coefficient = [cd_Normal_x, cd_Longitudinal_y , cd_Longitudinal_z, cm_Normal_x, cm_Longitudinal_y , cm_Longitudinal_z]
    """
    __slots__ = ('number', 'name', 'items', 'sets', 'profile',
                 'type', 'zone', 'diameter', #'smooth', 'rough',
                 'Reynolds_number', 'KC_number', 'direction',
                 'concepts', 'coefficient')
    
    def __init__(self,  name, number):
        #
        self.number = number
        self.name = name
        self.coefficient = None
        #self.rough = None
        self.type = None
        #
        self.items = []
        self.sets = {}
    #
    #@property
    def set_cdcm(self, cdcm):
        """
        """
        if type(cdcm) == list:
            self.coefficient = geomop.CDCM._make(cdcm)
        
        elif type(cdcm) == dict:
            self.coefficient = CDCM.Fixity(**cdcm)
        # ?
        else:
            self.coefficient = geomop.CDCM(Cdx, Cdy, Cdz, Cmx, Cmy, Cmz)
        #
        self.type = 'specified'     
        #return _cdcm
    #
    def specified_roughX(self, cdcm):
        """
        Applicable to the wet part (fouled)
        """
        self.type = 'specified'
        self.rough = DragMassCoefficient.set_cdcm(self, cdcm)
    #
    def specified_smoothX(self, cdcm):
        """
        Applicable to members without marine growth (smooth)
        """
        self.type = 'specified'
        self.smooth = DragMassCoefficient.set_cdcm(self, cdcm)
    #      
    #
    def diameter_function(self, diameter, smooth, rough):
        """
        """
        #_smooth = get_cdcm(cdcm_clean)
        #_rough = get_cdcm(cdcm_faulted)
        
        if self.type:
            self.diameter[diameter] = [smooth, rough]
        
        else:
            self.type = 'diameter_function'
            self.diameter = {}
            self.diameter[diameter] = [smooth, rough]
    #
    def depth_profile(self, depth, cdcm):
        """
        """
        #_coefficient = MorrisonCoefficient.set_cdcm(self, cdcm)
        
        if self.type:
            self.profile.extend([depth, cdcm])
        
        else:
            self.type = 'profile'
            self.profile = [depth, cdcm]
    #
    def rule(self, rule):
        """
        """
        self.type = 'rule'
        
    #
    def set_global_direction(self):
        """
        """
        pass
#
# TODO :  tobe deleted
class Parameters():
    """
    FE Hydrodynamic Parameters
    
    Parameters
        |_ name
        |_ number
        |_ type
        |_ flooded
        |_ coeficient
        |_ items
        |_ sets
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'items', 'sets', 'profile',
                 'coefficient', 'type', 'zone') # 'flooded', 
    #
    def __init__(self,  name, number):
        #
        self.number = number
        self.name = name
        self.items = []
        self.sets = {}
        #self.flooded = False  # non flooded by default
        self.coefficient = {} # [x, y, z]
        self.zone = None
        #
        self.profile = {}
#
class Zone():
    """
    """
    __slots__ = ('number', 'name', 'xmin', 'xmax',
                 'ymin', 'ymax', 'zmin', 'zmax')
    #
    def __init__(self, name, number=None):
        self.number = number
        self.name = name
#  
#
class Combination():
    """
    FE MetoceanCombination Class
    
    MetoceanCombination
                      |_ name
                      |_ number
    """
    __slots__ = ('number', 'name', 'buoyancy', 'condition',
                 'wave', 'wave_direction', 'wave_kinematics', 
                 'wind', 'wind_direction', 'wind_method', 'wind_areas',
                 'current', 'current_direction', 'current_blockage',
                 'current_stretching', 'design_load',
                 'report', 'class_type', 'drag_areas')
    #
    def __init__(self, name, number):
        #
        self.number = number
        self.name = name
        #
        self.wave = None
        self.wave_direction = None
        self.wave_kinematics = 1.0
        #
        self.current = None
        self.current_direction = None
        self.current_blockage = 1.0
        self.current_stretching = False
        #
        self.wind = None
        self.wind_direction = None
        self.wind_method = None
        self.wind_areas = []
        #
        self.buoyancy = "Off"
        #
        self.class_type = 'metocean'
        #self.areas = None
        #
        self.design_load = 'NoDesignLoads'
    #
    def get_name(self, number=False, name=False):
        """
        """
        if not number:
            number = self.number
        #
        if not name:
            name = self.name
        #
        self.name = 'MET' + str(number).zfill(3) + '_' + str(name)
    #
    #
    #
#

