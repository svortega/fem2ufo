#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports



# package imports
#


#
class Database():
    """
    """
    __slots__ = ('number', 'name', 'description',
                 'component', 'database', 'date')

    #
    def __init__(self, Name, Number):
        self.number = Number
        self.name = Name
        self.component = {}

#
#
#
class Component(object):
    """
    Major assembly (i.e. topside, jacket, etc)
    """
    __slots__ = ('number', 'name', 'areas', 'type',
                 'discipline', 'change', 'description',
                 'items', 'cog')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        self.description = 'NA'


#
class Area(object):
    """
    This describe all project main areas/modules and
    sub-areas, including their location, size, code and name
    """
    __slots__ = ('number', 'name', 'code', 'description',
                 'discipline', 'coordinates', 'size', 'cog')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        self.description = 'NA'
        self.discipline = {}


#
class Discipline(object):
    """
    """
    __slots__ = ('number', 'name', 'code', 'description',
                 'item', 'type', 'assumtion')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        self.item = []
        self.description = 'NA'


#
class Item(object):
    """
    units : Number of item's pieces
    
    """
    __slots__ = ('number', 'name', 'code',
                 'units', 'description',
                 'registration', 'cog')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        self.units = {}
        self.description = 'NA'
        #


#
class Unit():
    """
    
    dry     : The dry weight of each piece in kg, that is the product of 
              Number off x Size x Unit weight, 
              e.g. 1 x 2,350 x 42,3 = 99 kg for one HE200A with a length of
              2,350 m.
    content : The weight of the content in kg in a piping line, vessel, 
              tank etc.
    
    size       : The size of each piece, e.g. 2,350 m for the length of an 
                 HE200A-beam.
    coordinates   : The co-ordinates in the platform north direction, the platform
                    east direction and elevation.
    number_off : Number of identical pieces
    """
    __slots__ = ('number', 'name', 'weight',
                 'coordinates', 'number_off',
                 'dimension', 'description')

    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        self.weight = {}
        self.dimension = [1, 1, 1]
        self.description = 'NA'


#
class Mass(object):
    """
    cog   : The co-ordinates in the mass, the platform
            east direction and elevation.
    unit  : unit weight for the piece
    """
    __slots__ = ('number', 'name', 'magnitud', 'tolerance',
                 'noItem', 'status', 'unit', 'cog')
    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name
        self.magnitud = []
        #
        #
        #
        #


#
class LoadingCondition():
    """
    Reported loading condition
    """
    __slots__ = ('number', 'name', 'description',
                 'temporary', 'permanent')

    #
    def __init__(self, Number, Name):
        self.number = Number
        self.name = Name

    #

#