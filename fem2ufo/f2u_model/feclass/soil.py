# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports


# package imports

#
# SOIL Section
class Soil:
    """
    FE Soil class
    
    Soil
        |_ name
        |_ number
        |_ type
        |_ layers [ly[0], ly[1], ly[2]..., ly[n]]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'type', 'scour',
                 'layers', 'mudline', 'diameter',
                 'displacement')

    def __init__(self, Name, Number):
        self.number = Number
        self.name = Name
        self.layers = {}
        self.displacement = []
#
class Layer:
    """
    FE Soil Layers Class
    
    Layer
        |_ name
        |_ number
        |_ thickness  : thickness of the layer
        |_ depth      : vertical distance from mudline
        |_ diamter    : pile reference diameter
        |_ curve [spr[0], spr[1], spr[2],..., spr[n]]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'curve', 'sublayers',
                 'depth', 'diameter','thickness')

    def __init__(self, Name, Number):
        self.number = Number
        self.name = Name
        self.curve = {}
        self.sublayers = 1
#
#
class Scour:
    """
    FE Soil Scour Class
    
    Scour
        |_ name
        |_ number
        |_ general : General scour [m]
        |_ local   : Local scour around the piles [m]
        |_ slope   : Side slope of local scour holes [degrees]
        |_ 
    """
    __slots__ = ('number', 'name', 'general', 'local',
                 'slope')
    
    def __init__(self, Name, Number):
        self.number = Number
        self.name = Name    
#
