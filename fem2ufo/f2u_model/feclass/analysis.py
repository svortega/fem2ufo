#
# Copyright (c) 2009-2020 fem2ufo
# 
# Python stdlib imports



# package imports
#


#
#
class Case:
    """
    """
    __slots__ = ('number', 'name', 'condition',
                 'design_condition', #'start_load',
                 'metocean_combination',
                 'load_combination')
    
    def __init__(self, name, number=None):
        """
        """
        self.number = number
        self.name = name
        self.metocean_combination = {}
        self.load_combination = {}
        #self.start_load = 0
#
#