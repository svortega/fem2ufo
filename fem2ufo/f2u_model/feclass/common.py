# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports


# package imports

# Input User Data Details
#
class InputUserData:
    """
    Read user data, etc from input data file
    """
    __slots__ = ('user', 'date', 'version1', 'version2', 'file_name',
                 'title', 'program', 'project', 'notes', 'units')

    def __init__(self):
        self.user = 'N/A'
        self.date = 'N/A'
        self.version1 = 'N/A'
        self.version2 = 'N/A'
        self.file_name = 'N/A'
        self.program = 'N/A'
#
#