# 
# Copyright (c) 2009-2020 fem2ufo


# Python stdlib imports
import math
#import sys
#import datetime
import re
#from operator import attrgetter

# package imports
#import fem2ufo.process.operations.units as units
from fem2ufo.process.modules.buckingham import Number
#
# ----------------------------------------
#      Sections Material
# ----------------------------------------
#
def find_mat_density(MatInput, _UnitLength):
    """
    """
    _MatInput = str(MatInput).lower()
    #_MatInput = _MatInput.replace('SECTION','')
    #_MatInput= _MatInput.replace('SHAPE','')
    _MatInput = _MatInput.replace(' ','')
    _MatInput = _MatInput.replace('-','')
    _MatInput = _MatInput.replace('_','')
    _MatInput = _MatInput.strip()
    # kg/m^3
    Metals = {
        'aluminium' : 2640,
        'aluminium bronze' : 8700,
        'aluminium foil' : 2750,
        'antifriction metal' : 10600,
        'beryllium' : 1840,
        'beryllium copper' : 8250,
        'brass casting' : 8700,
        'brass' : 8730,
        'bronze lead' : 8700,
        'bronze phosphorous' : 8920,
        'bronze' : 8900,
        'cast iron' : 7800,
        'cobolt' : 8746,
        'copper' : 8930,
        'delta metal' : 8600,
        'electrum' : 8900,
        'gold' : 19320,
        'iron' : 7850,
        'lead' : 11340,
        'light alloy based on Al' : 2800,
        'light alloy based on Mg' : 1870,
        'magnesium' : 1738,
        'mercury' : 13593,
        'molybdenum' : 10188,
        'monel' : 8840,
        'nickel' : 8800,
        'nickel silver' : 8900,
        'platinum' : 21400,
        'plutonium' : 19800,
        'silver' : 10490,
        'steel' : 7850,
        'stainless steel' : 8000,
        'tin' : 7280,
        'titanium' : 4500,
        'tungsten' : 19600,
        'uranium' : 18900,
        'vanadium' : 5494,
        'white metal' : 7100,
        'zinc' : 7135,
    }
    #
    _density = Metals.get(_MatInput)
    _UnitMass = Number(1, dims = 'kilogram/metre^3')
    if _UnitLength == 'inch' or _UnitLength == 'foot' or _UnitLength == 'mile' :
        _units = _UnitMass.convert('pound/'+str(_UnitLength)+'^3').value
    else:
        _units = _UnitMass.convert('kilogram/'+str(_UnitLength)+'^3').value
    
    return (_density*_units)
#
def find_sec_mass(_Material, _Units, _Area):
    """
    """
    try:
        _MatDensity = float(_Material)
        _MaterialType = 'USER DEFINED'
    except:
        _MatDensity = find_mat_density(_Material, _Units)
        _MaterialType = _Material
    
    _SecMass = _MatDensity * _Area
    #
    if _Units == 'inch' or _Units == 'foot' or _Units == 'mile' :
        _UnitMass = Number(1, dims = 'pound/'+ str(_Units))
        _SecMass = _SecMass * _UnitMass.convert('pound/foot').value
    else:
        _UnitMass = Number(1, dims = 'kilogram/'+ str(_Units))
        _SecMass = _SecMass * _UnitMass.convert('kilogram/metre').value
    #
    return _MaterialType, _SecMass
#
#
# ----------------------------------------
#      Sections Profiles Names
# ----------------------------------------
#
def select(key, keyWord):
    """
    """
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        find = rgx.match(keyWord)
        if find: 
            return _key
    raise IOError('    ** error : section {:} no available'.format(keyWord))
#
#
def find_section(keyWord):
    """
    """
    _ignore = re.compile(r"sec[t]?(ion)?[s]?|shape|beam|fab|\_|\-", re.I)
    _sectionName  = _ignore.sub("", keyWord)
    #print('--- ',_sectionName)
    _key = {"angle" : r"\b(1|l|ang(le)?(s)?)\b",
            "box" : r"\b(2|hss|box|rhs|square)\b",
            "channel" : r"\b(3|mc|ch(anne)?(l)?(s)?)\b",
            "i section" : r"\b(4|i|h|w(f(c)?)?|m|hp|ub|uc|ubp|asb|plg)\b",
            "unsymmetric" : r"\b(10|pgu)\b",
            "rectangular bar" : r"\b(5|rectangular\s*b(ar)?[s]?|square\s*b(ar)[s]?|rtb)\b",
            "circular bar" : r"\b(6|circular\s*b(ar)[s]?|solid\s*circle)\b",
            "tee" : r"\b(7|(w|m|s)?tee[s]?|dal)\b",
            "tubular" : r"\b(8|hss\s*round|pipe|tub(ular)?|chs)\b",
            "elliptical segment" : r"\b((ellip(tical)?|circ(ular)?)\s*seg(ment)?[s]?)\b",
            "elliptical sector" : r"\b((ellip(tical)?|circ(ular)?)\s*sec(tor)?[s]?)\b",
            "super ellipse" : r"\b(super\s*ellip(se|tical)?)\b",
            "open thin walled" : r"\b(open\s*thin\s*wall(ed)?[s]?)\b",
            "general section" : r"general",
            "shell" : r"\b(pl(ate)?[s]?|shell)\b",
            "membrane" : r"\b(membrane)\b",
            "cone" : r"\b(con(e)?)\b"}
    
    _sectionOut = select(_key, _sectionName)
    return _sectionOut
#
#
#
#
# ----------------------------------------
#      Basic Shapes
# ----------------------------------------
#
def rectangle(D, B):
    """
    Calculate the section properties of a rectangular section

    +   +-----+
        |     |    
    D   |     |   Z
        |     |   ^
    +   +-----+   + > Y
        *  b  *

    Parameters
    ----------
    D : float
    Section Heigh
    B : float
    Base

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    _Zc : float
    Elastic neutral centre
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    Zpy : float
    Plastic modulus about mayor axis
    SFy : float
    Shape factor mayor axis
    ry : float
    Radius of gyration about mayor Axis
    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    Zpz : float
    Plastic modulus about minor axis
    SFz : float
    Shape factor minor axis
    rz : float
    Radius of gyration about minor Axis
    SC : float
    Shear centre
    Cw : float
    Warping constant

    Notes
    ----------
    Uses formulas from:
    1.- Formulas for stress, strain and strucutral matrices [W.D. Pilkey]
    2.- Roark's formulas for stress and strain [7th Edition]
    3.- Wikipedia

    Examples
    ----------

    """
    _D = float(D)
    _Tw = 0
    _Bft = float(B)
    _Tft = 0
    _Type = 'RECTANGLE'
        
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = _Bft * _D
    #-------------------------------------------------
    #   Elastic Neutral Centre 
    _Zc = 0 
    _Yc = 0
    #-------------------------------------------------
    #   Plastic Neutral Centre 
    _Zp = 0
    _Yp = 0
    #-------------------------------------------------
    #   Shear Centre 
    _SCz = 0
    _SCy = 0
    #-------------------------------------------------
    #   Warping Constant Cw
    _Cw = 0
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    #   Second Moment of Area about Mayor Axis
    _Iy = _Bft*_D**3 / 12.0
    
    #   Elastic Modulus about Mayor Axis
    _Zey = _Bft*_D**2 / 6.0
    
    #   Plastic Modulus about Mayor Axis
    _Zpy = _Bft*_D**2 / 4.0
    
    #   Shape Factor
    _SFy = 1.50
    
    #   Radius of gyration about Mayor Axis
    _ry = _D / math.sqrt(12)
    
    #-------------------------------------------------
    #   Second Moment of Area about Minor Axis
    _Iz = _Bft**3 *_D / 12.0
    
    #   Elastic Modulus about Minor Axis
    _Zez = _Bft**2 *_D / 6.0
    
    #   Plastic Modulus about Minor Axis
    _Zpz = _Bft**2 *_D / 4.0
    
    #   Shape Factor
    _SFz = 1.50

    #   Radius of gyration about Minor Axis 
    _rz = _Bft / math.sqrt(12)
    
    #-------------------------------------------------
    #   Torsional Constant
    if _D == _Bft:
        _J = 0.1406 * _D**4
    else:
        _J = ((_D * _Bft**3 / 3.0) * 
              (1 - (0.630 * _Bft / _D) +
               (0.052 * _Bft**5 / _D**5)))
    #   Product of inertia
    _Iyz = 0.0
    _Jx = _Bft*_D*(_Bft**2 + _D**2) / 12.0
    _rp = math.sqrt((_Bft**2 + _D**2) / 12.0)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#
def circle(D):
    """
    """
    _D = float(D)
    _R = 0.50*_D
    _Type = 'CIRCLE'
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = math.pi * _R**2
    
    #-------------------------------------------------
    #   Elastic Neutral Centre 
    _Zc = 0.0
    _Yc = 0.0
    
    #-------------------------------------------------
    #   Shear Centre 
    _SCz = 0
    
    _SCy = 0
    
    #-------------------------------------------------
    #   Warping Constant Cw
    _Cw = 0
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    
    #   Second Moment of Area about Mayor Axis
    _Iy = math.pi * _R**4 / 4.0
    
    #   Elastic Modulus about Mayor Axis
    _Zey = math.pi * _R**3 / 4.0
    
    #   Plastic Modulus about Mayor Axis
    _Zpy = 4 * math.pi * _R**3 / 3.0
    
    #   Shape Factor
    _SFy = 1.698
    
    #   Radius of gyration about Mayor Axis
    _ry = _R / 2.0
    
    #-------------------------------------------------
    #   Second Moment of Area about Minor Axis
    
    _Iz = math.pi * _R**4 / 4.0
    
    #   Elastic Modulus about Minor Axis
    _Zez = math.pi * _R**3 / 4.0
    
    #   Plastic Modulus about Minor Axis
    _Zpz = 4 * math.pi * _R**3 / 3.0
    
    #   Shape Factor
    _SFz = 1.698

    #   Radius of gyration about Minor Axis 
    _rz = _R / 2.0
    
    #-------------------------------------------------
    #   Torsional Constant
    _J = math.pi * _D**4 / 32.0
    #   Product of inertia
    _Iyz = 0.0
    _Jx = _Iy + _Iz
    _rp = _D / math.sqrt(8.0)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#
def semicircle(D):
    """
    """
    _Type = 'SEMICIRCLE'
    _R = 0.50 * D
    
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = math.pi * _R**2 / 2.0
    
    #-------------------------------------------------
    #   Elastic Neutral Centre 
    _Zc = 4 * _R / (3 * math.pi)
    _Yc = 0
    
    #-------------------------------------------------
    #   Plastic Neutral Centre 
    _Zp = 0.4040 * _R
    _Yp = 0
    
    #-------------------------------------------------
    #   Shear Centre 
    _SCz = 0 # (8.0 / (15.0 * math.pi)) * _R * (3 + 4 * v) / (1 + v)
    _SCy = 0
    
    #-------------------------------------------------
    #   Warping Constant Cw
    _Cw = 'N/A'
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    
    #-------------------------------------------------
    #   Second Moment of Area about Mayor Axis
    _Iy = math.pi / 8.0 * _R**4
    #   Elastic Modulus about Mayor Axis
    _Zey = math.pi / 8.0 * _R**3
    #   Plastic Modulus about Mayor Axis
    _Zpy = 2 / 3.0 * _R**3
    #   Shape Factor
    _SFy = 1.698
    #   Radius of gyration about Mayor Axis
    _ry = _R / 2.0
    
    #-------------------------------------------------
    #   Second Moment of Area about Minor Axis
    _Iz = 0.1098 * _R**4 
    #   Elastic Modulus about Minor Axis
    _Zez = 0.1908 * _R**3 
    #   Plastic Modulus about Minor Axis
    _Zpz = 0.354 * _R**3 
    #   Shape Factor
    _SFz = 1.856 
    #   Radius of gyration about Minor Axis 
    _rz = 0.2643 * _R 
    
    #-------------------------------------------------
    #   Torsional Constant
    _J = 0
    
    #-------------------------------------------------
    #   Product of inertia
    _Iyz = 0.0
    _Jx = _Iy + _Iz
    _rp = math.sqrt(_Jx / _Area)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#
def trapeziod(a, b, h, c=False):
    """
    """
    _Type = 'TRAPEZOID'
    if not c:
        c = (a - b) / 2.0
    
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = h * (a + b) / 2.0
    
    #-------------------------------------------------
    #   Elastic Neutral Centre 
    _Zc = h/2.0 - (h / 3.0 * ((2*a + b) / (a + b)))
    _Yc = a/2.0 - (2*a**2 + 2*a*b -a*c - 2*b*c - b**2) / (3*(a + b))
    #-------------------------------------------------
    #   Plastic Neutral Centre 
    _Zp = 'N/A'
    _Yp = 'N/A'
    
    #-------------------------------------------------
    #   Shear Centre 
    _SCz = 'N/A'
    _SCy = 'N/A'
    
    #-------------------------------------------------
    #   Warping Constant Cw
    _Cw = 'N/A'
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    #   Second Moment of Area about Mayor Axis
    _Iy = (h / (36*(a+b)) * (a**4 + b**4 + 2*a*b *(a**2 + b**2)
                             - c*(a**3 + 3*a**2*b - 3*a*b**2 - b**3)
                             + c**2*(a**2 + 4*a*b + b**2)))
    #   Elastic Modulus about Mayor Axis
    _Zey = _Iy / ((2*a**2 + 2*a*b -a*c - 2*b*c - b**2) / (3*(a + b)))
    #   Plastic Modulus about Mayor Axis
    _Zpy = 'N/A'
    #   Shape Factor
    _SFy = 'N/A'
    #   Radius of gyration about Mayor Axis
    _ry = math.sqrt((a**2 + b**2) / 24.0)
    
    #-------------------------------------------------
    #   Second Moment of Area about Minor Axis
    _Iz = (h**3 / 36.0) * ((a**2 + 4 * a * b + b**2) / (a + b))
    #   Elastic Modulus about Minor Axis
    _Zez = _Iz / (h / 3.0 * ((2*a + b) / (a + b))) 
    #   Plastic Modulus about Minor Axis
    _Zpz = 'N/A'
    #   Shape Factor
    _SFz = 'N/A'
    #   Radius of gyration about Minor Axis 
    _rz = h * (math.sqrt(a**2 + 4 * a * b + b**2) / (math.sqrt(18) * (a + b)))
    
    #-------------------------------------------------
    #   Torsional Constant
    _J = 0
    
    #-------------------------------------------------
    #   Product of inertia
    _Iyz = (h**2 / (72*(a+b)) * (b*(3*a**2 - 3*a*b - b**2)
                                 + a**3 - c * (2*a**2 + 8*a*b + 2*b**2)))
    _Jx = _Iy + _Iz
    _rp = math.sqrt(_Jx / _Area)
    #
    _Iyz = 0
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#
# ----------------------------------------
#      Standard Sections Profiles
# ----------------------------------------
#
def Angle(D, Tw, B=False, r=False):
    """
    Calculate the section properties of an angle section

         +   +
             |         
        D    |         Z
             |         ^
         +   +-----+   + > Y
             *  B  *

    Parameters
    ----------
    D : float
       Section Heigh
    Tw : float
        Web/flange thickness
    B : float
        Base
    r1 : float
        Root Radious

    Returns
    ----------
    A : float
        Section area
    Zc : float
        Elastic neutral centre
    Yc : float
        Elastic neutral centre
    Iy : float
    	Second moment of area about mayor axis
    ry : float
    	Radius of gyration about mayor Axis
    Iz : float
    	Second moment of area about minor axis
    rz : float
    	Radius of gyration about minor Axis
    Cw : float
    	Warping constant

    Notes
    ----------
    Uses formulas from:
    1.- Full plastic capacity of equal angle sections under biaxial
        bending and normal force [A.E. Charalampakis]
    2.- Formulas for stress, strain and strucutral matrices [W.D. Pilkey]
    3.- Roark's formulas for stress and strain [7th Edition]
    4.- Wikipedia

    Examples
    ----------

    """
    #  Beam Section Definition
    _D = float(D)
    _Tw = float(Tw)
    
    if B:
        _Bft = float(B)
    else:
        _Bft = _D
    #
    if r:
        _r = 0.50*float(r)
    else:
        _r = 0.50 * (_Tw / math.sqrt(2.0))
    #
    _Type = 'ANGLE'
    #
    #-------------------------------------------------
    #
    # Simmetric angle
    if _D == _Bft :
        #-------------------------------------------------
        #   Cross-Sectional Area (A.1)
        _Area = (2 * _D * _Tw) - _Tw**2 + (2 - math.pi/2.0) * _r**2
        
        # Distance v1 of elastic centroid from heel (A.2)
        _v1 = (((6 * math.sqrt(2))*(((50/3.0 - 5*math.pi)*_r**3)
                                    - ((2 - math.pi / 2.0)*(_D - 3*_Tw)*_r**2)
                                    + ((_D**2 + _D*_Tw - _Tw*2)*_Tw))) /
               (((24 - 6*math.pi)*_r**2) + (24*_D*_Tw) - (12*_Tw**2)))
        
        #  Distance v2 (A.3)
        _v2 = ((_D/math.sqrt(2)) + (1 - math.sqrt(2)) + 
               (_Tw / math.sqrt(2)) - _v1)
        
        # Distance v3 (A.4)
        _v3 = _D/math.sqrt(2)
        
        # Moment of inertia around u-u (A.5)
        _Iu = (((70 - 21*math.pi)*_r**4 / 24.0) + 
               ((_D - _Tw)**2 * (math.pi - 4) * _r**2 / 4.0) -
               (_Tw**4 / 12.0) + (_D**3 * _Tw / 3.0) + (_D * _Tw**3 / 3.0) -
               (_D**2 * _Tw**2 / 2.0))
        
        # Elastic section modulus around u-u (A.6)
        _Wu = _Iu / _v3
        
        # Moment of inertia around v-v (A.7)
        _Iv = ((1.0 / ((288 - 72 * math.pi)*_r**2 + 288*(_D - (_Tw / 2.0))*_Tw)) *
               ((((7926 * math.pi) - (1233 * math.pi**2) - 12776)*_r**6)
                + (432*( math.pi - (10.0 / 3.0))*(_D - _Tw)*( math.pi - 4)*_r**5)
                + (((1422 * math.pi) - (36 * math.pi**2) - 4188)*_Tw**2)
                + (((72*((-79 * math.pi/2.0) +  math.pi**2 + (349/3.0)) * _D*_Tw) 
                   - (36 * _D**2 * ( math.pi - 4)**2)*_r**4))
                + (432*( math.pi - (10.0/3.0))*(4*_Tw**2 / 3.0 +  _D**2 -
                                                4 * _D*_Tw)*_Tw*_r**3)
                - (24*(_D**3 - (9*_Tw * _D**2) + (13*_Tw**2 *_D) - (13*_Tw**3 / 4.0)) *
                   ((math.pi - 4)*_Tw*_r**2))
                + (-(72 * _D*_Tw**5) + (12*_Tw**6) + (24*_Tw**2 *_D**4) - 
                   (48*_Tw**3 *_D**3) + (96*_Tw**4 *_D**2))))
        
        # Elastic section modulus around v-v (A.8)
        _Wv = _Iv / _v1
        
        # Distance e (A.9)
        _e = _v1 / math.sqrt(2)
        _Zc = _e
        _Yc = _e
        
        # Moment of inertia around y-y or z-z (A.10)
        _Iy = ((1.0 / (((288 - 72*math.pi)*_r**2) + (288*(_D - _Tw/2.0)*_Tw)))
               * (((3732*math.pi - 5968 - 585*math.pi**2)*_r**6)
                  + ((216*(math.pi - 4)*(math.pi - 10.0/3.0)*(_D - _Tw)*_r**5) +
                     (60*_D**4*_Tw**2))
                  + (-(120*_D**3*_Tw**3) + (132*_D**2*_Tw**4) - (72*_D*_Tw**5) +
                     (12*_Tw**6) + (((216*(math.pi - 10.0/3.0))) * 
                                    ((_D**2 - 4*_D*_Tw + 4*_Tw**2 / 3.0)*_r**3*_Tw))) 
                  + (((-27*_D**2 *(math.pi - 4)**2)
                      + (54*(272/3.0 - 94*math.pi/3.0 + math.pi**2)*_D*_Tw)
                      + ((846*math.pi - 2448 - 27*math.pi**2)*_Tw**2))*_r**4)
                  + (12*(math.pi - 4)*(_D**3 + 3*_D**2 *_Tw - 8*_D*_Tw**2 + 2*_Tw**3)*_r**2 *_Tw)))
        
        _Iz = _Iy
        
        # Elastic section modulus araund y-y or z-z (A.11)
        _Zey = _Iy / (_D - _e)
        _Zez = _Zey
        
        # Radii of gyration around any axis (A.12)
        try:
            _ry = math.sqrt(_Iy / _Area)
        except ValueError:
            print('fix angle Iy')
            _ry = 1
        
        _rz = _ry
        
        # Plastic Properties
        # Distance v1p of the plastic centroid from heel (A.13)
        _v1p = ((3 * (math.pi - 4)*_r**2 + 4*_D*_Tw + 6*_Tw**2) /
                (8*math.sqrt(2)*_Tw))
        
        # Major plastic section modulus around u'-u' (A.14)
        _Wup = ((((48*_r**3 + 3*(math.pi - 4)*(_D - _Tw)*_r**2 - 
                   6*_D*_Tw**2 + 6*_D**2 * _Tw + 2*_Tw**3)*math.sqrt(2))
                 / 12.0) - (16*_r**3 / 3.0))
        
        # Minor Plastic section modulus around v'-v' (A.15)
        _Wvp = ((math.sqrt(2)/(192*_Tw)) * ((-27*(math.pi - 4)**2 * _r**4)
                                            + (96*(3*math.pi - 10)*_r**3 * _Tw) 
                                            - ((12*(math.pi - 4)*_r**2) * ((2*_D - 11*_Tw)*_Tw))
                                            + (4*_Tw**2*(12*_D**2 - 12*_D*_Tw + 13*_Tw**2))))
        
        _phi = 1.0
        
        #-------------------------------------------------
        #   Plastic Modulus about Minor Axis
        #   error, needs fix
        
        _Zpy = 0
        _Zpz = 0        
        #
    #
    else:
        _b1 = _Bft - _Tw
        _h1 = _D - _Tw
        
        #-------------------------------------------------
        #   Cross-Sectional Area
        _Area = (_D + _b1)  * _Tw
        
        #-------------------------------------------------
        #   Elastic Neutral Centre
        _Zc = (_D**2 + _b1 * _Tw) / (2 * (_D + _b1))
        _Yc = (_Bft**2 + _h1 * _Tw) / (2 * (_Bft + _h1))
        
        #-------------------------------------------------
        #   Shear Centre 
        _SCz = _Tw / 2.0
        _SCy = _Tw / 2.0
        
        #-------------------------------------------------
        #               Section Properties
        #-------------------------------------------------
        
        #   Second Moment of Area about Mayor Axis
        
        _Iy = ((_Tw*(_D - _Zc)**3 + _Bft*_Zc**3 -
                _b1*(_Zc - _Tw)**3) / 3.0)
        
        _Zey = _Iy / (_D - _Zc)
        
        _Zpy = 0
        
        #   Radius of gyration about Mayor Axis
        _ry = math.sqrt(_Iy / _Area)
        
        _SFy = 0
        
        #-------------------------------------------------
        #   Second Moment of Area about Minor Axis
        
        _Iz = ((_Tw*(_Bft - _Yc)**3 + _D*_Yc**3 -
                _h1*(_Yc - _Tw)**3) / 3.0)
        
        _Zez = _Iz / (_Bft - _Yc)
        _Zpz = 0
        
        #   Radius of gyration about Minor Axis 
        _rz = math.sqrt(_Iz / _Area)
        
        _SFz = 0
        
        #-------------------------------------------------
        #   Product of inertia
        _Izy = (_Bft*_b1*_D*_h1*_Tw)/(4*(_Bft + _h1))
        
        _Iu = (0.50 * (_Iz + _Iy) +
               0.50 * math.sqrt((_Iz - _Iy)**2 + 4 * _Izy**2))
        
        _Iv = (0.50 * (_Iz + _Iy) -
               0.50 * math.sqrt((_Iz - _Iy)**2 + 4 * _Izy**2))
        
        _phi = (math.atan(2 * _Izy / (_Iy - _Iz)))/2.0
        
        _Wu = 0
        _Wv = 0
        _Wup = 0
        _Wvp = 0
        _v1p = 0
    #
    _b1 = _D - 0.50 * _Tw
    _b2 = _Bft - 0.50 * _Tw
    _c1 = _b1 - 0.50 * _Tw
    _c2 = _b2 - 0.50 * _Tw
    #
    #-------------------------------------------------
    #   Warping Constant Cw (Bleich 1952, Picard and Beaulie 1991)
    _Cw = (((_b1**3 + _b2**3) * (_Tw **3)) / 36.0)
    
    #-------------------------------------------------
    #   Torsional Constant (fillets neglected)
    _J = (((_b1 + _b2) * (_Tw**3)) / 3.0)
    
    #-------------------------------------------------
    #   Product of inertia
    _Iyz = (_Tw * (_b1 * _Yc * (_b1 - 2 * _Zc)) +
            _Tw * (_b2 * _Zc * (_b2 - 2 * _Yc)))
    
    _Jx = _Iy + _Iz
    try:
        _rp = math.sqrt(_Jx / _Area)
    except ValueError:
        print('fix angle Jx')
        _rp = 1    
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#
def Box(D, Tw, B, Tf):
    """
    Calculate the section properties of a box section

    +   +------+
        | +--+ |
        | |  | |    
    D   | |  | |  
        | |  | |   Z
        | +--+ |   ^
    +   +------+   + > Y
        *  B   *

    Parameters
    ----------
    D : float
    Section Heigh
    Tw : float
    Web thickness
    B : float
    Base
    Tf : float
    Flange thickness

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    _Zc : float
    Elastic neutral centre
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    Zpy : float
    Plastic modulus about mayor axis
    SFy : float
    Shape factor mayor axis
    ry : float
    Radius of gyration about mayor Axis
    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    Zpz : float
    Plastic modulus about minor axis
    SFz : float
    Shape factor minor axis
    rz : float
    Radius of gyration about minor Axis
    SC : float
    Shear centre
    Cw : float
    Warping constant

    Notes
    ----------
    Uses formulas from:
    1.- Formulas for stress, strain and strucutral matrices [W.D. Pilkey]
    2.- Roark's formulas for stress and strain [7th Edition]
    3.- Wikipedia

    Examples
    ----------

    """
    #print ('box')
    _D = float(D)
    _Tw = float(Tw)
    _Bft = float(B)
    _Tft = float(Tf)
    _Type = 'BOX'
    #
    _hi = _D - 2 * _Tft
    _bi = _Bft - 2 * _Tw
    
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = _Bft * _D - _bi * _hi
    
    #-------------------------------------------------
    #   Elastic Neutral Centre 
    _Zc = _Bft / 2.0
    _Yc = _D / 2.0
    
    #-------------------------------------------------
    #   Shear Centre 
    _SCz = 0
    _SCy = 0
    
    #-------------------------------------------------
    #   Warping Constant Cw
    _Cw = 0
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    #   Second Moment of Area about Mayor Axis
    _Iy = ((_Bft *_D**3 - _bi * _hi**3) / 12.0)
    
    #   Elastic Modulus about Mayor Axis
    _Zey = ((_Bft * _D**3 - _bi * _hi**3) / (6.0 * _D))
    
    #   Plastic Modulus about Mayor Axis
    _Zpy = ((_Bft*_D**2 - _bi * _hi**2) / 4.0)
    
    #   Shape Factor
    _SFy = (1.50 * (_D * (_Bft*_D**2 - _bi * _hi**2)) /
            (_Bft*_D**3 - _bi * _hi**3))
    
    #   Radius of gyration about Mayor Axis
    _ry = math.sqrt(_Iy / _Area)
    
    #-------------------------------------------------
    #   Second Moment of Area about Minor Axis
    _Iz = ((_D *_Bft**3 - _hi * _bi**3) / 12.0)
    
    #   Elastic Modulus about Minor Axis
    _Zez = ((_D * _Bft**3 - _hi * _bi**3) / (6.0 * _Bft))
    
    #   Plastic Modulus about Minor Axis
    _Zpz = ((_D *_Bft**2 - _hi * _bi**2) / 4.0)
    
    #   Shape Factor
    _SFz = (1.50 * (_Bft * (_D  * _Bft**2 - _hi * _bi**2)) /
            ( _D * _Bft**3 -  _hi * _bi**3))
    
    #   Radius of gyration about Minor Axis 
    _rz = math.sqrt(_Iz / _Area)
    #-------------------------------------------------
    #   Torsional Constant
    # Mean corner radious
    _Rc = 1.50 * (_Tw + _Tft) / 2.0
    # Mid countour length
    _p = (2*((_D - _Tft) + (_Bft - _Tw)) 
          - 2 * _Rc**2 * (4 - math.pi))
    # Enclosed Area
    _Ap = ((_D - _Tft) * (_Bft - _Tw) 
           - _Rc**2 * (4 - math.pi))
    # for thin walled sections b/t >= 10
    _J = ((4*_Ap**2 * ((_Tw + _Tft) / 2.0)) / _p)
    #-------------------------------------------------
    #   Product of inertia
    _Iyz = 0.0
    _Jx = _Iy + _Iz
    _rp = math.sqrt(_Jx / _Area)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#
def Channel(D, Tw, B, Tf):
    """
    Calculate the section properties of a channel section

    +   +-----+
        |         
    D   |         Z
        |         ^
    +   +-----+   + > Y
        *  B  *

    Parameters
    ----------
    D : float
    Section Heigh
    Tw : float
    Web thickness
    B : float
    Base
    Tf : float
    Flange thickness

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    _Zc : float
    Elastic neutral centre
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    Zpy : float
    Plastic modulus about mayor axis
    SFy : float
    Shape factor mayor axis
    ry : float
    Radius of gyration about mayor Axis
    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    Zpz : float
    Plastic modulus about minor axis
    SFz : float
    Shape factor minor axis
    rz : float
    Radius of gyration about minor Axis
    SC : float
    Shear centre
    Cw : float
    Warping constant

    Notes
    ----------
    Uses formulas from:
    1.- Formulas for stress, strain and strucutral matrices [W.D. Pilkey]
    2.- Roark's formulas for stress and strain [7th Edition]
    3.- Wikipedia

    Examples
    ----------

    """
    #print ('Channel')
    _D = float(D)
    _Tw = float(Tw)
    _Bft = float(B)        
    _Tft = float(Tf)
    _Type = 'CHANNEL'
    #
    _b = _Bft - 0.50 * _Tw
    _C = _Bft - _Tw
    _h = _D - _Tft
    _D2 = _D - 2 * _Tft
    
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = (_h * _Tw) + (2 * _b * _Tft)
    
    #-------------------------------------------------
    #   Elastic Neutral Centre 
    _Zc = 0.50 * _D
    _Yc = ((2 * _Bft**2 * _Tft + _D2 * _Tw**2) /
           (2 * _Bft * _D - 2 * _D2 * _C))
    
    #-------------------------------------------------
    #   Shear Centre 
    _SCz = 0.50 * _D
    _SCy = ((3 * _Tft * _Bft**2) / (6 * _b * _Tft + _h * _Tw))
    
    #-------------------------------------------------
    #   Warping Constant Cw
    _Cw = ((_b**3 * _h**2 * _Tft / 12.0) * 
           ((2 * _h * _Tw + 3 * _b * _Tft) /
            (_h * _Tw + 6 * _b * _Tft)))
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    
    #   Second Moment of Area about Mayor Axis
    _Iy = (_Bft * _D**3 - _C * _D2**3) / 12.0
    
    #   Elastic Modulus about Mayor Axis
    _Zey = 2 * _Iy / _D
    #
    #   Plastic Modulus about Mayor Axis
    _Zpy = (((_D**2 * _Tw) / 4.0) +
            (_Tft * _C * (_D - _Tft)))
    
    #   Shape Factor
    _SFy = _Zpy * _D / (2 * _Iy)
    
    #   Radius of gyration about Mayor Axis
    _ry = math.sqrt(_Iy / _Area)
    
    #-------------------------------------------------
    #   Second Moment of Area about Minor Axis
    _Iz = ((_D * _Bft**3 / 3.0) - (_C**3 * _D2 / 3.0) 
           - (_Area * (_Bft - _Yc)**2))
    
    #   Elastic Modulus about Minor Axis
    _Zez = _Iz / (_Bft - _Yc)
    
    #   Plastic Modulus about Minor Axis
    if (2*_Tft*_C) > (_D*_Tw):
        _Zpz = ((_C**2 *_Tft / 2.0) - (_D**2 * _Tw**2 / (8 * _Tft)) +
                (_D * _Tw * _Bft / 2.0))
    else:
        _Zpz = (((_Tw**2 *_D) / 4.0) +
                (_Tft * _C * (_Bft - (_Tft * _C / _D))))
    
    #   Shape Factor
    _SFz = (_Zpz * (_Bft - _Yc) / _Iz)
    
    #   Radius of gyration about Minor Axis 
    _rz = math.sqrt(_Iz / _Area)
    
    #-------------------------------------------------
    #   Torsional Constant
    _J = (2 * _b * _Tft**3 + _h * _Tw**3) / 3.0
    
    #   Product of inertia
    _Iyz = 0.0
    _Jx = _Iy + _Iz
    _rp = math.sqrt(_Jx / _Area)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#    
def Isection(D, Tw, Bft, Tft, Bfb=False, Tfb=False):
    """
    Calculate the section properties of a I section

         * Bft *
    +    +-----+
            |         
    D       |         Z
            |         ^
    +  +---------+    + > Y
       *   Bfb   *

    Parameters
    ----------
    h : float
    Section Heigh
    Tw : float
    Web thickness
    Bft : float
    Top flange base 
    Tff : float
    Top flange thickness
    Bfb : float
    Bottom flange base 
    Tfb : float
    Bottom flange thickness

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    _Zc : float
    Elastic neutral centre
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    Zpy : float
    Plastic modulus about mayor axis
    SFy : float
    Shape factor mayor axis
    ry : float
    Radius of gyration about mayor Axis
    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    Zpz : float
    Plastic modulus about minor axis
    SFz : float
    Shape factor minor axis
    rz : float
    Radius of gyration about minor Axis
    SC : float
    Shear centre
    Cw : float
    Warping constant

    Notes
    ----------
    Uses formulas from:
    1.- Formulas for stress, strain and strucutral matrices [W.D. Pilkey]
    2.- Roark's formulas for stress and strain [7th Edition]
    3.- Wikipedia

    Examples
    ----------

    """
    #  Beam Section Definition
    _D = float(D)
    _Tw = float(Tw)
    _Bft = float(Bft)
    _Tft = float(Tft)
    _Type = 'I SECTION'
    #
    if Bfb:
        _Bfb = float(Bfb)
    else:
        _Bfb = float(Bft)
    #
    if Tfb:
        _Tfb = float(Tfb)
    else:
        _Tfb = float(Tft)
    #
    _Stress = 'OFF'
    _ShearStress = 'AVERAGE'
    _Build = 'N/A'
    #-------------------------------------------------   
    #
    _hw = (_D - _Tft - _Tfb)
    _ho = (_D - 0.5 * _Tft - 0.5 * _Tfb)
       
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = (_Bft*_Tft + _Bfb*_Tfb + _hw*_Tw)
    
    #-------------------------------------------------
    #   Elastic Neutral Centre 
    _Zc = ((_Bft * _Tft**2 / 2.0 +
            _Bfb * _Tfb * (_hw + _Tft + _Tfb / 2.0) +
            _hw * _Tw * (_hw / 2.0 + _Tft)) / 
           (_Bft * _Tft + _hw*_Tw + _Bfb * _Tfb))
    _Yc = 0
    
    #   Warping Constant Cw
    # Picard and Beaulieu 1991
    _d = _D - (_Tft + _Tfb)/2.0
    _alpha = 1.0 / (1 + (_Bft/float(_Bfb))**3 * (_Tft /float(_Tfb)))
    _Cw = (_d**2 * _Bft**3 * _Tft * _alpha)/12.0
    
    #-------------------------------------------------
    #   Torsional Constant
    if _Bft == _Bfb and _Tft == _Tfb :
        _J = ((2 * _Bft * _Tft**3 / 3.0) + (_D * _Tw**3 / 3.0))
    else:
        _J = ((_Bft * _Tft**3 + _Bfb * _Tfb**3 +
               _d * _Tw**3) / 3.0)
    
    #-------------------------------------------------
    #   Shear Centre
    _SCz = (((_D - _Tft / 2.0 - _Tfb / 2.0) * _Tft * _Bft**3) /
            (_Tft * _Bft**3 + _Tfb * _Bfb**3))
    _SCy = 0
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    #   Second Moment of Area about Mayor Axis
    _Iy = (_Bft * _Tft**3 / 12.0 +
           _Bft * _Tft * (_Zc - _Tft / 2.0)**2 +
           _Bfb * _Tfb**3 / 12.0 +
           _Bfb * _Tfb * (_hw + _Tfb / 2.0 + _Tft - _Zc)**2 +
           _Tw * _hw**3 / 12.0 +
           _Tw * _hw * (_hw / 2.0 + _Tft - _Zc)**2)
    
    #-------------------------------------------------        
    #   Elastic Modulus about Mayor Axis
    if _Zc >= (_D - _Zc):
        _Zey = _Iy / _Zc
    else:
        _Zey = _Iy / (_D - _Zc)
    
    #-------------------------------------------------
    #   Plastic Modulus about Mayor Axis
    _Zpy = ((_Tw * _hw**2 / 4.0) +
            (_Bft * _Tft * (_Zc - _Tft / 2.0)) +
            (_Bfb * _Tfb * (_D - _Zc - _Tfb / 2.0)))
    
    #-------------------------------------------------
    #   Radius of gyration about Mayor Axis
    _ry = (_Iy / _Area)**0.5
    _SFy = _Zpy / _Zey
    #   Second Moment of Area about Minor Axis
    _Iz = (_Bft**3 * _Tft / 12.0 +
           _Bfb**3 * _Tfb / 12.0 +
           _Tw**3 * _hw / 12.0)
    
    #-------------------------------------------------
    #   Elastic Modulus about Minor Axis
    if _Bft >= _Bfb:
        _Zez = 2 * _Iz / _Bft
    else:
        _Zez = 2 * _Iz / _Bfb
    
    #-------------------------------------------------
    #   Plastic Modulus about Minor Axis  
    _Zpz = ((_Tft * _Bft**2 + _Tfb * _Bfb**2 +
             _hw * _Tw**2) / 4.0)
    
    #-------------------------------------------------
    #   Radius of gyration about Minor Axis  
    _rz = (_Iz / _Area)**0.5
    _SFz = _Zpz / _Zez
    
    #   Product of inertia
    _Iyz = 0
    _Jx = _Iy + _Iz
    _rp = math.sqrt(_Jx / _Area)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#
def Tee(D, Tw, Bft, Tf):
    """
    Calculate the section properties of a T section

        *  B  *
    +   +-----+
           |         
    D      |       Z
           |       ^
    +      +       + > Y

    Parameters
    ----------
    D : float
    Section Heigh
    Tw : float
    Web thickness
    B : float
    Base
    Tf : float
    Flange thickness

    Returns
    ----------
    Area : float
    Section area
    Zc : float
    Elastic neutral centre
    _Zc : float
    Elastic neutral centre
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    Zpy : float
    Plastic modulus about mayor axis
    SFy : float
    Shape factor mayor axis
    ry : float
    Radius of gyration about mayor Axis
    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    Zpz : float
    Plastic modulus about minor axis
    SFz : float
    Shape factor minor axis
    rz : float
    Radius of gyration about minor Axis
    SC : float
    Shear centre
    Cw : float
    Warping constant

    Notes
    ----------
    Uses formulas from:
    1.- Formulas for stress, strain and strucutral matrices [W.D. Pilkey]
    2.- Roark's formulas for stress and strain [7th Edition]
    3.- Wikipedia

    Examples
    ----------

    """
    _D = float(D)
    _Tw = float(Tw)
    _Bft = float(Bft)        
    _Tft = float(Tf)
    _Type = 'TEE'
    #
    _C = _Bft - _Tw
    _h = _D - _Tft / 2.0
    _D2 = _D - _Tft
    
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = _Bft*_Tft + _Tw * _D2
    
    #-------------------------------------------------
    #   Elastic Neutral Centre 
    _Zc = (((_D**2 * _Tw) + (_C * _Tft**2)) /
           (2 * (_Bft*_Tft + _D2*_Tw)))
    
    _Yc = 0
    
    #-------------------------------------------------
    #   Shear Centre 
    _SCz = _Tft / 2.0
    _SCy = 0
    
    #-------------------------------------------------
    #   Warping Constant Cw
    _Cw = (_Tft**3 * _Bft**3 / 144.0) + (_Tw**3 * _h**3 / 36.0)
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    
    #   Second Moment of Area about Mayor Axis
    _Iy = ((_Tw * (_D - _Zc)**3 + _Bft * _Zc**3 -
            _C * (_Zc - _Tft)**3) / 3.0)
    
    #   Elastic Modulus about Mayor Axis
    _Zey = (_Iy / (_D - _Zc))
    
    #   Plastic Modulus about Mayor Axis
    if _Tw * _D2 > _Bft * _Tft :
        _Zpy = ((_D2**2 * _Tw / 4.0) - 
                (_Bft**2 * _Tft**2 / (4.0 * _Tw)) + 
                (_Bft * _Tft * _D / 2.0))
    else:
        _Zpy = ((_Tft**2 * _Bft / 4.0) +
                0.50 * _Tw * _D2*(_D - _Tw*_D2 / (2 * _Bft)))
    
    #   Shape Factor
    _SFy = (_Zpy * (_D - _Zc) / _Iy)
    
    #   Radius of gyration about Mayor Axis
    _ry = math.sqrt(_Iy / _Area)
    
    #-------------------------------------------------
    #   Second Moment of Area about Minor Axis
    _Iz = (_Bft**3 * _Tft + _D2 * _Tw**3) / 12.0
    
    #   Elastic Modulus about Minor Axis
    _Zez = 2 * _Iz / _Bft
    
    #   Plastic Modulus about Minor Axis
    _Zpz = (_Bft**2 * _Tft + _Tw**2 * _D2) / 4.0
    
    #   Shape Factor
    _SFz = _Zpz * _Bft / (2 * _Iz)
    
    #   Radius of gyration about Minor Axis 
    _rz = math.sqrt( _Iz / _Area)
    
    #-------------------------------------------------
    #   Torsional Constant
    _J = (_Bft * _Tft**3 + _h * _Tw**3) / 3.0
    
    #   Product of inertia
    _Iyz = 0
    _Jx = _Iy + _Iz
    _rp = math.sqrt(_Jx / _Area)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#
def Tubular(D, Tw):
    """
    Calculate the section properties of a Tubular section

    Parameters
    ----------
    D : float
    Diameter
    Tw : float
    Thickness wall

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    _Zc : float
    Elastic neutral centre
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    Zpy : float
    Plastic modulus about mayor axis
    SFy : float
    Shape factor mayor axis
    ry : float
    Radius of gyration about mayor Axis
    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    Zpz : float
    Plastic modulus about minor axis
    SFz : float
    Shape factor minor axis
    rz : float
    Radius of gyration about minor Axis
    SC : float
    Shear centre
    Cw : float
    Warping constant

    Notes
    ----------
    Uses formulas from:
    1.- Formulas for stress, strain and strucutral matrices [W.D. Pilkey]
    2.- Roark's formulas for stress and strain [7th Edition]
    3.- Wikipedia

    Examples
    ----------

    """
    _D = float(D)
    _Tw = float(Tw)
    _Type = 'TUBULAR'
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = ((math.pi / 4.)*
             (_D**2 -(_D - 2*_Tw)**2))
    
    # Centroid
    _Zc = _D / 2.0
    _Yc = 0
    
    # Shear centre
    _SCz = 0
    _SCy = 0
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    
    #   Second Moment of Area about Mayor Axis
    #   --------------------------------------
    _Iy = ((math.pi / 64.0)*
            (_D**4 - (_D-2*_Tw)**4))
    _Iz = _Iy
    
    #   Elastic Modulus about Mayor Axis
    #   --------------------------------------
    _Zey = (2 * _Iy / _D)
    _Zez = _Zey
    
    #-------------------------------------------------
    #   Plastic Modulus about Mayor Axis
    _Zpy = ((_D**3 -
             (_D - 2*_Tw)**3) / 6.0)
    
    _Zpz = _Zpy
    
    #-------------------------------------------------
    #   Radius of gyration about Mayor Axis
    _ry = math.sqrt((_D**2 + (_D-2*_Tw)**2)) / 4.0
    
    _rz = _ry
    
    _SFy = _Zpy / _Zey
    _SFz = _SFy
    
    #-------------------------------------------------
    #   Warping Constant Cw
    _Cw = 0
    
    #-------------------------------------------------
    #   Torsional Constant
    _J = (2*_Iy)
    
    #-------------------------------------------------
    #   Polar Moment of Inertia
    _Ip = ((math.pi/32.0)*
           (_D**4 - 
            (_D - 2*_Tw)**4))
    
    #   Product of inertia
    _Iyz = 0
    _Jx = 2 * _Iy 
    _rp = math.sqrt(_Jx / _Area)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _J, _Iyz
#
def HollowSemicircle(R, Tw):
    """
    Calculate the section properties of a Hollow Semicircle

    Parameters
    ----------
    D : float
    Diameter
    Tw : float
    Thickness wall

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    _Zc : float
    Elastic neutral centre
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    Zpy : float
    Plastic modulus about mayor axis
    SFy : float
    Shape factor mayor axis
    ry : float
    Radius of gyration about mayor Axis
    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    Zpz : float
    Plastic modulus about minor axis
    SFz : float
    Shape factor minor axis
    rz : float
    Radius of gyration about minor Axis
    SC : float
    Shear centre
    Cw : float
    Warping constant

    Notes
    ----------
    Uses formulas from:
    1.- Formulas for stress, strain and strucutral matrices [W.D. Pilkey]
    2.- Roark's formulas for stress and strain [7th Edition]
    3.- Wikipedia

    Examples
    ----------

    """
    _D = float(R)
    _Tw = float(Tw)
    _Type = 'HOLLOWSEMICIRCLE'
    #
    _R = float(R)
    _Ri = _R  - _Tw
    _b = (_R + _Ri) / 2.0
    #-------------------------------------------------
    #   Cross-Sectional Area
    _Area = (math.pi * (_R**2 - _Ri**2)) / 2.0
    
    # Centroid
    _Zc = (2 * _b / math.pi) * (1 + (_Tw / _b)**2 / 12.0)
    
    _Zc1 = _R - _Zc
    
    _Yc = 0
    _Yc2 = _R
    
    # Shear Centre
    _SCz = 2 * _D 
    _SCy = 0
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    #   Second Moment of Area about Mayor Axis
    #   --------------------------------------
    _Iy = (((math.pi / 8.0) * (_R**4 - _Ri**4)) 
           - ((8.0 / (9 * math.pi)) 
              * ((_R**3 - _Ri**3)**2) / (_R**2 - _Ri**2)))
    
    _Iz = ((math.pi / 8.0) * (_R**4 - _Ri**4))
    
    #   Elastic Modulus about Mayor Axis
    #   --------------------------------------
    _Zey = min(_Iy / _Zc, _Iy /_Zc)
    _Zez = _Iz / _Yc2
    
    #-------------------------------------------------
    #   Plastic Modulus about Mayor Axis
    _C = _Tw / float(_R)
    
    # Plastic neutral axis
    _Zp = (_R * (0.7071 - 0.2716 * _C 
                 - 0.4299 * _C**2 + 0.3983 * _C**3))
    _Yp = 0
    
    # Plastic section moduli mayor axis
    _Zpy = (_R**2 * _Tw * (0.8284 - 0.9140 * _C +
                           0.7245 * _C**2 - 0.2850 * _C**3))
    
    # Plastic section moduli minor axis
    _Zpz = 0.6667 * (_R**3 - _Ri**3)
    
    #-------------------------------------------------
    #   Radius of gyration
    _ry = math.sqrt(_Iy / _Area)
    _rz = math.sqrt(_Iz / _Area)
    
    #-------------------------------------------------
    #   Torsional Constant
    _J = ((_Tw * _R**5 / 3.0) * (2 * math.pi**3 
                                 - (12 * math.pi**2 / math.pi)))
    _Iyz = 0
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _Zp, _J, _Iyz
#
def HollowSemiellipse(a, b, Tw):
    """
    Calculate the section properties of a Hollow Semiellipse
    with constant wall thickness Tw.
    The midthickness perimeter is an ellipse 0.2 < a/b < 0.50

    Parameters
    ----------
    D : float
    Diameter
    Tw : float
    Thickness wall

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    _Zc : float
    Elastic neutral centre
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    Zpy : float
    Plastic modulus about mayor axis
    SFy : float
    Shape factor mayor axis
    ry : float
    Radius of gyration about mayor Axis
    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    Zpz : float
    Plastic modulus about minor axis
    SFz : float
    Shape factor minor axis
    rz : float
    Radius of gyration about minor Axis
    SC : float
    Shear centre
    Cw : float
    Warping constant

    Notes
    ----------
    Uses formulas from:
    1.- Formulas for stress, strain and strucutral matrices [W.D. Pilkey]
    2.- Roark's formulas for stress and strain [7th Edition]
    3.- Wikipedia

    Examples
    ----------

    """
    _D = float(a)
    _Bft = float(b)
    _Tw = float(Tw)
    _Type = 'HOLLOWSEMIELLIPSE'
    #
    _a = float(a) - 0.50 * _Tw
    _b = float(b) - 0.50 * _Tw
    
    # Note : there is a limit on the maximum 
    # wall thickness allowed in this case.
    # Cusps will form in the perimeter at
    # the ends of the mayor axis if this 
    # maximum is exceeded.
    if _a/_b < 1.0 :
        _tmax = 2 * _a**2 / _b
    else: 
        _tmax = 2 * _b**2 / _a
    
    if _Tw > _tmax :
        sys.exit('error : t > tmax')
         
    #-------------------------------------------------
    #   Cross-Sectional Area
    _C = (_a - _b) / (_a + _b)
    _K1 = 0.2464 + 0.002222 * ((_a / _b) + (_b / _a))
    _K2 = 1 - 0.3314 * _C + 0.0136 * _C**2 + 0.1097 * _C**3
    _K3 = 1 + 0.9929 * _C - 0.2287 * _C**2 - 0.2193 * _C**3
    
    _Area = ((_Tw * math.pi / 2.0) * 
             (_a + _b) * (1.0 + _K1 * ((_a - _b) / (_a + _b))**2))
    
    # Centroid
    _Zc = ((2.0 * _a * _K2 / math.pi) 
           + (_Tw**2 * _K3 / (6.0 * math.pi * _a)))
    
    _Zc1 = _a + _Tw / 2.0 - _Zc
    
    _Yc = 0
    _Yc1 = _b + _Tw / 2.0
    
    #-------------------------------------------------
    #               Section Properties
    #-------------------------------------------------
    
    #   Second Moment of Area about Mayor Axis
    #   --------------------------------------
    _K4 = 0.1349 + 0.1279 * (_a / _b) - 0.01284 * (_a / _b)**2
    _K5 = 0.1349 + 0.1279 * (_a / _b) - 0.01284 * (_b / _a)**2
    
    _Iy = ((((_Tw * _a**2 * math.pi / 8.0) * (_a + 3 * _b)) 
            * (1 + _K4 * ((_a - _b) / (_a + _b))**2))
           + (((_Tw**3 * math.pi / 32.0) * (3 * _a + _b)) 
            * (1 + _K5 * ((_a - _b) / (_a + _b))**2)))
    
    _Iy = _Iy - _Area * _Zc**2
    
    _K2 = 0.1349 + 0.1279 * (_b / _a) - 0.01284 * (_b / _a)**2
    _K3 = 0.1349 + 0.1279 * (_a / _b) - 0.01284 * (_a / _b)**2
    
    _Iz = ((((_Tw * _a**2 * math.pi / 8.0) * (_b + 3 * _a)) 
            * (1 + _K2 * ((_b - _a) / (_b + _a))**2))
           + (((_Tw**3 * math.pi / 32.0) * (3 * _b + _a)) 
            * (1 + _K3 * ((_b - _a) / (_b + _a))**2)))
    
    #   Plastic Modulus about Mayor Axis
    #   --------------------------------------
    # Let Zp be the vertical distance  from  the bottom
    # to the plastic neutral axis
    _DD = _Tw / _tmax
    _DD = max(_DD , 0.20)
    _DD = min(_DD , 1.0)
    
    if _a / _b > 0.25 and _a / _b < 1.0:
        _C1 = 0.5067 - 0.5588 * _DD + 1.3820 * _DD**2
        _C2 = 0.3731 + 0.1938 * _DD - 1.4078 * _DD**2
        _C3 = -0.140 + 0.0179 * _DD + 0.4885 * _DD**2
        _C4 = 0.0170 - 0.0079 * _DD - 0.0565 * _DD**2
        #
        _C5 = -0.0292 + 0.3749 * math.sqrt(_DD) + 0.0578 * _DD
        _C6 = 0.36740 - 0.8531 * math.sqrt(_DD) + 0.3882 * _DD
        _C7 = -0.1218 + 0.3563 * math.sqrt(_DD) - 0.1803 * _DD
        _C8 = 0.01540 - 0.0448 * math.sqrt(_DD) + 0.0233 * _DD
    elif _a / _b >= 1.0 and _a / _b < 4.0:
        _C1 = 0.4829 + 0.0725 * _DD - 0.1815 * _DD**2
        _C2 = 0.1957 - 0.6608 * _DD + 1.4222 * _DD**2
        _C3 = 0.0203 + 1.8999 * _DD - 3.4356 * _DD**2
        _C4 = 0.0578 - 1.6666 * _DD + 2.6012 * _DD**2
        #
        _C5 = 0.22410 - 0.3922 * math.sqrt(_DD) + 0.2960 * _DD
        _C6 = -0.6637 + 2.7357 * math.sqrt(_DD) - 2.0482 * _DD
        _C7 = 1.52110 - 5.3864 * math.sqrt(_DD) + 3.9286 * _DD
        _C8 = -0.8498 + 2.8763 * math.sqrt(_DD) - 1.8874 * _DD
    else : 
        sys.exit('error a/b > 4 or a/b < 0.25')
    
    # Plastic neutral axis
    _Zp = (_a * (_C1 + _C2 / (_a / _b) + _C3 / (_a / _b)**2 
                 + _C4 / (_a / _b)**3))
    
    _Yp = 0
    
    # Plastic section moduli mayor axis
    _Zpy = (4.0 * _a**2 * _Tw * (_C5 + _C6 / (_a / _b) 
                                 + _C7 / (_a / _b)**2 
                                 + _C8 / (_a / _b)**3))
    
    # Plastic section moduli minor axis
    _K4 = 0.1835 + 0.895 * (_b / _a) - 0.00978 * (_b / _a)**2
    
    _Zpz = (0.50 * (((1.3333 * _Tw * _b * (_b + 2 * _a))
                    * (1 + _K4 * (_b - _a) / (_a + _b))**2)
                    + _Tw**3 / 3.0))
    
    #-------------------------------------------------
    #   Radius of gyration
    _ry = math.sqrt(_Iy / _Area)
    _rz = math.sqrt(_Iz / _Area)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz, _Zp
#
#
def Cone():
    """
        * Bft *
    +   +-----+  
        |     |
    D  |       |
      |         |
    + +---------+
      *   Bfb   *
    """
    _type = "Cone"
#
# ----------------------------------------
#      Elliptical Sections Profiles
# ----------------------------------------
#
def ellipticalSegment(a, b, thetaG):
    """
    Calculate the circular and elliptical segments
    cross section properties
    
    Parameters
    ----------
    a : float
    Mayor Axis
    b : float
    Minor Axis
    thetaG : float (degrees)
    Angle

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    Yc : float
    Elastic neutral centre
    
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    ry : float
    Radius of gyration about mayor Axis

    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    rz : float
    Radius of gyration about minor Axis
    
    Notes
    ----------
    Uses formulas from:
    1.- Geometric properties for the design of unusual member
        cross-sections in bending [A.J. Sadowski]

    Examples
    ----------    
    """
    #
    _D = float(a)
    _Tw = float(b)
    _theta = thetaG
    _p = 0
    _q = 0
    _Type = 'ELLIPTICAL SEGMENT'
    
    _thetaG = math.radians(thetaG)
    _thetaG = min(abs(_thetaG), 0.50 * math.pi)
    
    # Area
    _Area = 0.50 * a * b * (2 * _thetaG - math.sin( 2 * _thetaG))
    
    # Centroid
    _Zc = ((4.0 * b * math.sin(_thetaG)**3) 
           / (3.0 * (2 * _thetaG - math.sin(2 * _thetaG))))
    
    _Yc = 0
    
    # Second Moment of Area about x
    _Iy = ((a * b**3 / 16.0) 
           * (4 * _thetaG - math.sin(4 * _thetaG)))
    
    # Second Moment of Area about y
    _Iz = ((a**3 * b / 24.0) 
           * (6.0 * _thetaG - math.sin(2 * _thetaG) 
              * (3.0 + 2.0 * math.sin(_thetaG)**2)))
    
    # Second Moment of Area about the horizontal centroidal C
    _Ic = _Iy - _Area * _Zc**2
    
    # The distances from the centroid to the extreme fibres
    _y1 = a * math.sin(_thetaG)
    _z1 = b - _Zc
    _z2 = _Zc - b * math.cos(_thetaG)
    
    # elastic section moduli
    _Zey = min(_Ic / _z1, _Ic / _z2)
    _Zez = _Iz / _y1
    
    # plastic section moduli
    _Zpy = 0
    _Zpz = 0
    
    # radii of gyration
    _ry = math.sqrt(_Ic / _Area)
    _rz = math.sqrt(_Iz / _Area)
    #
    return _Area, _Zc, _Yc, _Iy, _Zey, _Ic, _ry, _Iz, _Zez, _Zpz, _rz
#
def ellipticalSector(a, b, thetaG):
    """
    Calculate the circular and elliptical sectors
    cross section properties
    
    Parameters
    ----------
    a : float
    Mayor Axis
    b : float
    Minor Axis
    thetaG : float (degrees)
    Angle

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    Yc : float
    Elastic neutral centre
    
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    ry : float
    Radius of gyration about mayor Axis

    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    rz : float
    Radius of gyration about minor Axis
    
    Notes
    ----------
    Uses formulas from:
    1.- Geometric properties for the design of unusual member
        cross-sections in bending [A.J. Sadowski]

    Examples
    ----------    
    """    
    #
    _D = float(a)
    _Tw = float(b)
    _theta = thetaG
    _p = 0
    _q = 0
    _Type = 'ELLIPTICAL SECTOR'
    
    _thetaG = math.radians(thetaG)
    _thetaG = min(_thetaG, 0.50 * math.pi)
    
    # Area
    _Area = a * b * _thetaG
    
    # Centroid
    _Zc = (2 * b * math.sin(_thetaG)) / (3 * _thetaG)
    _Yc = 0
    
    # Second Moment of Area about x
    _Iy = ((a * b**3 / 8.0) 
           * (2 * _thetaG + math.sin(2 * _thetaG)))
    
    # Second Moment of Area about y
    _Iz = ((a**3 * b / 8.0) 
           * (2 * _thetaG - math.sin(2 * _thetaG)))
    
    # Second Moment of Area about the horizontal centroidal C
    _Ic = _Iy - _Area * _Zc**2
    
    # The distances from the centroid to the extreme fibres
    _y1 = a * math.sin(_thetaG)
    _z1 = b - _Zc
    _z2 = _Zc - b * math.cos(_thetaG)
    
    # elastic section moduli
    _Zey = min(_Ic / _z1, _Ic / _z2)
    _Zez = _Iz / _y1
    
    # plastic section moduli
    _Zpy = 0
    _Zpz = 0
    
    # radii of gyration
    _ry = math.sqrt(_Ic / _Area)
    _rz = math.sqrt(_Iz / _Area)
    #
    #
    return _Area, _Zc, _Yc, _Ic, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz
#
def superEllipse(a, b, p=2.0, q=2.0):
    """
    Calculate the superellipse cross section properties
    Superellipses as a function of the powers p and q
    
    Parameters
    ----------
    a : float
    Mayor Axis
    b : float
    Minor Axis
    p : float
    q : float

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    Yc : float
    Elastic neutral centre
    
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    ry : float
    Radius of gyration about mayor Axis

    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    rz : float
    Radius of gyration about minor Axis
    
    Notes
    ----------
    Uses formulas from:
    1.- Geometric properties for the design of unusual member
        cross-sections in bending [A.J. Sadowski]

    Examples
    ----------    
    """
    # 
    _D = float(a)
    _Tw = float(b)
    _theta = 90
    _p = p
    _q = q
    _Type = 'SUPER ELLIPSE'
    
    if p <= 0 or q <= 0:
        sys.exit("error p & q > 0")
    
    # Area
    _Area = ((2.0 * a * b / q) *
          ((math.gamma(1.0 / q) * math.gamma((1.0 + p) / p))
           / (math.gamma((p + p * q + q) / (p * q)))))
    
    # Centroid
    _Zc = ((math.pow(4, 1.0 / q) * b / (2 * math.sqrt(math.pi))) *
           ((math.gamma((2.0 + q) / (2 * q)) * math.gamma((p + p * q + q) / (p * q)))
            / (math.gamma((2 * p + p * q + q) / (p * q)))))
    _Yc = 0
    
    # Second Moment of Area about x
    _Iy = ((2.0 * a * b**3 / q) *
           ((math.gamma(3.0 / q) * math.gamma((1.0 + p) / p))
            / (math.gamma((3 * p + p * q + q) / (p * q)))))
    
    # Second Moment of Area about y
    _Iz = ((2.0 * a**3 * b / p) *
           ((math.gamma(3.0 / p) * math.gamma((1.0 + q) / q))
            / (math.gamma((p + p * q + 3 * q) / (p * q)))))
    #print('Jy',_Iz / 10**4)
    
    # Second Moment of Area about the horizontal centroidal C
    _Ic = _Iy - _Area * _Zc**2
    #print('Jx',_Ic / 10**4)
    
    # The distances from the centroid to the extreme fibres
    _y1 = a 
    _z1 = b - _Zc
    _z2 = _Zc 
    
    # elastic section moduli
    _Zey = min(_Ic / _z1, _Ic / _z2)
    _Zez = _Iz / _y1
    
    # plastic section moduli
    _Zpy = 0
    _Zpz = 0
    
    # radii of gyration
    _ry = math.sqrt(_Ic / _Area)
    _rz = math.sqrt(_Iz / _Area)
    #
    #
    return _Area, _Zc, _Yc, _Ic, _Zey, _Zpy, _ry, _Iz, _Zez, _Zpz, _rz
    #
#
def quarterCircle(r):
    """
    Calculate a quarter of a circle
    
    Parameters
    ----------
    r : float
    radius

    Returns
    ----------
    A : float
    Section area
    Zc : float
    Elastic neutral centre
    Yc : float
    Elastic neutral centre
    
    Iy : float
    Second moment of area about mayor axis
    Zey : float
    Elastic modulus about mayor axis
    ry : float
    Radius of gyration about mayor Axis

    Iz : float
    Second moment of area about minor axis
    Zez : float
    Elastic modulus about minor axis
    rz : float
    Radius of gyration about minor Axis
    
    Notes
    ----------
    Uses formulas from:
    1.- Structural Engineering Formulas
        Ilya Mikelson

    Examples
    ----------    
    """
    
    # Area
    _Area = math.pi * r**2 / 4.0
    #
    # Centroid
    _Zc = 4 * r / (3 * math.pi)
    _Yc = _Zc
    
    # Second Moment of Area about x
    _Iy = 0.07135 * r**4
    _Iy1 = 0.05489 * r**4
    _Iy2 = math.pi * r**4 / 16.0
    
    # Second Moment of Area about y
    _Iz = 0.03843 * r**4
    _Iz1 = _Iy1
    _Iz2  = _Iy2
    
    return _Area, _Zc, _Yc, _Iy, _Iy1, _Iy2, _Iz, _Iz1, _Iz2
#
#
# ----------------------------------------
#
class SectionProperty():
    """
    ------------------------------------------------
    Area    = Cross-Sectional Area
    Zc      = Elastic Neutral Centre 
    SC      = Shear Centre
    J       = Torsional Constant
    Cw      = Warping Constant
    ------------------------------------------------
    Mayor Axis
    ------------------------------------------------
    Iip     = Second Moment of Area In Plane
    Zeip    = Elastic Modulus In Plane
    Zpip    = Plastic Modulus In Plane
    rip     = Radius of gyration In Plane
    ------------------------------------------------
    Minor Axis
    ------------------------------------------------
    Iop     = Second Moment of Area Out of Plane
    Zeop    = Elastic Modulus Out of Plane
    Zpop    = Plastic Modulus Out of Plane
    rop     = Radius of gyration Out of Plane
    ------------------------------------------------
    """
    
    def __init__(self, Type, name=None):
        """
        """
        self.shape = find_section(str(Type))
        self.build = 'welded'
        self.name = name
        self.compactness = False
        self.shearAreaV = 1.0
        self.shearAreaH = 1.0
    
    def input_data(self, D, Tw=0, Bft=0, Tft=0, Bfb=0, Tfb=0,
                    B1=0, B2=0):
        """
        Solid circle and semicircle : D
        
        """
        self.D   = float(D) 
        self.Tw  = float(Tw)
        self.Bft = float(Bft)
        self.Tft = float(Tft)
        self.B1  = float(B1)
        self.Bfb = float(Bfb)
        self.Tfb = float(Tfb)
        self.B2  = float(B2)
        self.theta = Bfb
    
    def general_section_data(self, Area, Ix, Iy, Iz, Iyz, Zex, Zey, Zez, 
                             ShearAreaY, ShearAreaZ, SCy, SCz, Sy=1, Sz=1):
        """
        Area : Cross sectonal area
        
        Ix   : Torsional moment of inertia about shear centre
        Iy   : Moment of inertia about y axis
        Iz   : Moment of inertia about z axis
        
        Iyz  : Product of inertia about y and z axes
        
        Zex  : Minimum torsional section modulus about shear centre
        Zey  : Minimum sectional modulus about y axis
        Zez  : Minimum sectional modulus about z axis 
        
        shearAreaV : Shear area y direction
        shearAreaH : Shear area z direction
        SCV : Shear center location y direction
        SCH : Shear center location z direction
        
        SV  : Static area moment about y axis
        SH  : Static area moment about z axis
        """
        
        self.Area = float(Area)
        self.J = float(Ix)
        self.ZeJ = float(Zex)
        #
        self.Iip = float(Iy)
        self.Zeip = float(Zey)
        self.SCV = float(SCy)
        self.shearAreaV = float(ShearAreaY)
        self.SV = float(Sy)
        #
        self.Iop = float(Iz)
        self.Zeop = float(Zez)
        self.SCH = float(SCz)
        self.shearAreaH = float(ShearAreaZ)
        self.SH = float(Sz)
        #
        self.Iyz = float(Iyz)
        #
        #
    
    def get_property(self, _length_factor=1.0):
        """
        """
        if self.shape.upper() == 'I SECTION':
            if self.Bfb == 0.0:
                self.Bfb = self.Bft
            #
            if self.Tfb == 0.0:
                    self.Tfb = self.Tft
            #
            _D   = self.D   * _length_factor
            _Tw  = self.Tw  * _length_factor
            _Bft = self.Bft * _length_factor
            _Tft = self.Tft * _length_factor
            _Bfb = self.Bfb * _length_factor
            _Tfb = self.Tfb * _length_factor
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop, self.J, self.ZeJ) = Isection(_D, _Tw, 
                                                                                    _Bft, _Tft, 
                                                                                    _Bfb, _Tfb)
        elif self.shape.upper() == 'TEE':
            _D   = self.D   * _length_factor
            _Tw  = self.Tw  * _length_factor
            _Bft = self.Bft * _length_factor
            _Tft = self.Tft * _length_factor
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop, self.J, self.ZeJ) = Tee(_D, _Tw, 
                                                                               _Bft, _Tft)
        elif self.shape.upper() == 'CHANNEL':
            _D   = self.D   * _length_factor
            _Tw  = self.Tw  * _length_factor
            _Bft = self.Bft * _length_factor
            _Tft = self.Tft * _length_factor
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop, self.J, self.ZeJ) = Channel(_D, _Tw, 
                                                                                   _Bft, _Tft)
        elif self.shape.upper() == 'BOX':
            _D   = self.D   * _length_factor
            _Tw  = self.Tw  * _length_factor
            _Bft = self.Bft * _length_factor
            _Tft = self.Tft * _length_factor
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop, self.J, self.ZeJ) = Box(_D, _Tw, 
                                                                               _Bft, _Tft)
        elif self.shape.upper() == 'RECTANGULAR BAR':
            _D  = self.D  * _length_factor
            _Tw = self.Tw * _length_factor
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop, self.J, self.ZeJ) = rectangle(_D, _Tw)
        elif self.shape.upper() == 'CIRCULAR BAR':
            _D = self.D * _length_factor
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop, self.J, self.ZeJ) = circle(_D)
        elif self.shape.upper() == 'ANGLE':
            _D  = self.D  * _length_factor
            _Tw = self.Tw * _length_factor
            
            if self.Bft != 0:
                _Bft = self.Bft
            else: _Bft = self.D
            
            if self.Tft != 0:
                _r1 = self.Tft
            else: _r1 = 0
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop, self.J, self.ZeJ) = Angle(_D, _Tw, _Bft, _r1)
        elif self.shape.upper() == 'TUBULAR':
            _D  = self.D  * _length_factor
            _Tw = self.Tw * _length_factor
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop, self.J, self.ZeJ) = Tubular(_D, _Tw)
        elif self.shape.upper() == 'GENERAL SECTION':
            self.Area = self.Area * _length_factor**2
            self.J = self.J
            #
            self.Iip = self.Iip * _length_factor**4
            self.Zeip = self.Zeip * _length_factor**3
            self.SCV = self.SCV * _length_factor
            self.shearAreaV = self.shearAreaV * _length_factor**2
            self.SV = self.SV
            #
            self.Iop = self.Iop * _length_factor**4
            self.Zeop = self.Zeop * _length_factor**3
            self.SCH = self.SCH * _length_factor
            self.shearAreaH = self.shearAreaH * _length_factor**2
            self.SH = self.SH
            #
            self.Iyz = self.Iyz * _length_factor**3
            self.ZeJ = self.ZeJ * _length_factor**3
            self.J = self.J * _length_factor**3
            
        elif self.shape.upper() == 'ELLIPTICAL SEGMENT':
            _D  = self.D  * _length_factor
            _Tw = self.Tw * _length_factor
            _theta = self.theta
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop) = ellipticalSegment(_D, _Tw, _theta)
        elif self.shape.upper() == 'ELLIPTICAL SECTOR':
            _D  = self.D  * _length_factor
            _Tw = self.Tw * _length_factor
            _theta = self.theta
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop) = ellipticalSector(_D, _Tw, _theta)
        elif self.shape.upper() == 'SUPER ELLIPSE':
            _D  = self.D  * _length_factor
            _Tw = self.Tw * _length_factor
            #
            (self.Area, 
             self.CoV, self.CoH, self.Iip, self.Zeip, self.Zpip, self.rip, 
             self.Iop, self.Zeop, self.Zpop, self.rop) = superEllipse(_D, _Tw)
        else:
            #print('Section type no currently supported')
            sys.exit('Section type no currently supported')
    
    def get_name(self, _length_unit, _number):
        """
        """
        base_units = Number(1, dims=_length_unit)
        _length_factor = base_units.convert('millimetre').value
        #      
        if self.shape.upper() == 'GENERAL SECTION':
            _section_name = 'GB' + str(_number).zfill(3)
        elif self.shape.upper() == 'CONE':
            _section_name = 'CN' + str(_number).zfill(3)
        elif 'shell' in self.shape.lower():
            _section_name = 'PLT' + str(_number).zfill(3)
        elif 'membrane' in self.shape.lower():
            _section_name = 'PLT' + str(_number).zfill(3)
        else:
            _D   = round(self.D   * _length_factor)
            _Tw  = round(self.Tw  * _length_factor)
            _Bft = round(self.Bft * _length_factor)
            _Tft = round(self.Tft * _length_factor)
            _Bfb = round(self.Bfb * _length_factor)
            _Tfb = round(self.Tfb * _length_factor)
            #
            if self.shape.upper() == 'I SECTION':
                if _Bfb == 0.0:
                    _Bfb = _Bft
                #
                if _Tfb == 0.0:
                    _Tfb = _Tft
                #
                if _Bfb == _Bft and _Tfb == _Tft:
                    try:
                        _area = self.Area
                    except AttributeError:
                        SectionProperty.get_property(self)
                        _area = self.Area                 
                    
                    _mass = find_sec_mass('steel', 
                                          _length_unit, 
                                          _area)
                
                    _mass = round(_mass[1])
                    
                    _section_name = 'IS' + str(_number).zfill(3)+ '_' + str(_D) + 'x' + str(_Bft) + 'x' + str(_mass)
                else:
                    _section_name = 'PG' + str(_number).zfill(3)+ '_' + str(_D) + 'x' + str(_Tw) + 'x' + str(_Bft)+ 'x' + str(_Tft) + 'x' + str(_Tw)
            elif self.shape.upper() == 'TEE':
                try:
                    _area = self.Area
                except AttributeError:
                    SectionProperty.get_property(self)
                    _area = self.Area
                
                _mass = find_sec_mass('steel', 
                                      _length_unit, 
                                      _area)
            
                _mass = round(_mass[1])
                
                if _Bfb != 0 :
                    _Bf = _Bfb
                else:
                    _Bf = _Bft
                
                _section_name = 'TE' + str(_number).zfill(3)+ '_' + str(_D) + 'x' + str(_Bf) + 'x' + str(_mass)
            elif self.shape.upper() == 'CHANNEL':
                try:
                    _area = self.Area
                except AttributeError:
                    SectionProperty.get_property(self)
                    _area = self.Area
                
                _mass = find_sec_mass('steel', 
                                      _length_unit, 
                                      _area)
            
                _mass = round(_mass[1])
                
                _section_name = 'CH' + str(_number).zfill(3)+ '_' + str(_D) + 'x' + str(_Bft) + 'x' + str(_mass)
            elif self.shape.upper() == 'BOX':
                #
                # here try to adjust section to mach diven Iip & Iop
                # but Iip mostly correct, Iop understimated but not critical
                #
                #try:
                #    self.Area
                #    #
                #    if self.Bfb == 0.0:
                #        self.Bfb = self.Bft
                #    #
                #    if self.Tfb == 0.0:
                #        self.Tfb = self.Tft
                #    #
                #    # main axis
                #    bi = self.Bft - 2 * self.Tw
                #    di = self.D - (self.Tfb + self.Tft)
                #    _eq = (self.Bft*self.D**3.0 - 12*self.Iip) / bi
                #    di2 = math.pow(_eq, 1/3.0)
                #    #
                #    if di2 < di:
                #        _Tf = (self.D - di) / 2.0
                #        if _Tf > self.Tft:
                #            print('--->')
                #            #self.Tft = _Tf
                #            #self.Tfb = _Tf
                #    #
                #    # weak axis
                #    di = self.D - (self.Tfb + self.Tft)
                #    _eq = (self.Bft**3.0 * self.D - 12*self.Iop) / di
                #    bi2 = math.pow(_eq, 1/3.0)
                #    #
                #    if bi2 < bi:
                #        _Tw = (self.Bft - bi2) / 2.0
                #        if _Tw > self.Tw:
                #            print('--->')
                #            #self.Tw = _Tw
                #    #
                #    _D   = round(self.D   * _length_factor)
                #    _Tw  = round(self.Tw  * _length_factor)
                #    _Bft = round(self.Bft * _length_factor)
                #    _Tft = round(self.Tft * _length_factor)
                #    _Bfb = round(self.Bfb * _length_factor)
                #    _Tfb = round(self.Tfb * _length_factor)
                #    #print('--->')
                #    #_section_name = 'BG' + str(_number).zfill(3) + '_' + str(_D) + 'x' + str(_Bft) + 'x' + str(_Tw) + 'x' + str(_Tft)
                #
                #except AttributeError:
                #    pass
                #
                #
                #
                if _Bfb == 0.0:
                    _Bfb = _Bft
                #
                if _Tfb == 0.0:
                    _Tfb = _Tft            
                #
                if _Tw == _Tft :
                    _section_name = 'BX' + str(_number).zfill(3) + '_' + str(_D) + 'x' + str(_Bft) + 'x' + str(_Tw)
                else:
                    _section_name = 'BG' + str(_number).zfill(3) + '_' + str(_D) + 'x' + str(_Bft) + 'x' + str(_Tft) + 'x' + str(_Tw) 
            elif self.shape.upper() == 'RECTANGULAR BAR':
                _section_name = 'RB' + str(_number).zfill(3)+ '_' + str(_D)
            elif self.shape.upper() == 'CIRCULAR BAR':
                _section_name = 'CB' + str(_number).zfill(3)+ '_' + str(_D)
            elif self.shape.upper() == 'ANGLE':
                try:
                    _area = self.Area
                except AttributeError:
                    SectionProperty.get_property(self)
                    _area = self.Area
                
                _mass = find_sec_mass('steel', 
                                      _length_unit, 
                                      _area)
                _mass = round(_mass[1])
                
                if _Bfb != 0 :
                    _Bf = _Bfb
                else:
                    _Bf = _Bft
                
                _section_name = 'LS' + str(_number).zfill(3)+ '_' + str(_D) + 'x' + str(_Bf) + 'x' + str(_mass)
            elif self.shape.upper() == 'TUBULAR':
                _section_name = 'TS' + str(_number).zfill(3) + '_' + str(_D) + 'x'+ str(_Tw)
            else:
                sys.exit('   *** error : section {:} no currently supported'
                         .format(self.shape))
        #
        return _section_name
#