# 
# Copyright (c) 2009-2021 fem2ufo
# 
# Python stdlib imports
from math import sqrt, radians, sin, cos, isclose
from collections import OrderedDict

# package imports
import fem2ufo.f2u_model.operations.geometry as geomop

#
#
class Concept(object):
    """
    Element Concept Class
    
    Concept
        |_ name
        |_ number
        |_ type
        |_ codname
        |_ codtext
        |_ role
        |_ element
        |_ node
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    __slots__ = ('number', 'name', 'type', 'role', 'element',
                 'overlap', 'mass',
                 'node', 'section', 'material', 'step',
                 'offsets', 'releases', 'properties', 'load',
                 'code_check')
    #
    def __init__(self, Name, Number):
        """
        """
        self.number = Number
        self.name = Name
        self.type = {}
        self.role = {}
        #self.attribute = {}
        self.element = OrderedDict()
        #
        self.node =  [] #OrderedDict()
        self.step = []
        self.material =  [] #OrderedDict()
        self.section =  [] #OrderedDict()
        self.overlap = False
        self.offsets = []
        self.releases = []
        self.properties = {}
        #
        self.load = {}
        self.mass = []
    #
    def get_end_nodes(self):
        """
        """
        if not self.node:
            list_nodes = []
            _nodes = {}
            for _memb in self.element.values():
                _node1 = _memb.node[0]
                _node2 = _memb.node[1]
                _nodes[_node1.number] = _node1
                _nodes[_node2.number] = _node2
                list_nodes.append(_node1.number)
                list_nodes.append(_node2.number)
            #
            list_res = [x for x in list_nodes if list_nodes.count(x) >= 2]
            if list_res:
                list_nodes = list(set(list_nodes) - set(list_res))
            #
            for _node_number in list_nodes:
                self.node.append(_nodes[_node_number])
    #
    @property
    def flexible_length(self):
        """
        """
        #
        if not self.node:
            self.get_end_nodes()
        #
        # TODO: if not beam return false
        if self.offsets:
            _case = 2
            try:
                node1 = self.offsets[0].eccentricity
                _case = self.offsets[0].case
            except AttributeError:
                node1 = [0, 0, 0]
            #
            try:
                node2 = self.offsets[-1].eccentricity
                _case = self.offsets[-1].case
            except AttributeError:
                node2 = [0, 0, 0]
            
            # global case
            if _case == 1:
                _dx = ((self.node[0].x + node1[0]) 
                       - (self.node[1].x + node2[0]))
                _dy = ((self.node[0].y + node1[1]) 
                       - (self.node[1].y + node2[1]))
                _dz = ((self.node[0].z + node1[2]) 
                       - (self.node[1].z + node2[2]))
                #
                _x1 = (_dx*_dx + _dy*_dy + _dz*_dz)**0.50
            # local case
            else:
                # beam
                _dx = self.node[0].x - self.node[-1].x
                _dy = self.node[0].y - self.node[-1].y
                _dz = self.node[0].z - self.node[-1].z
                _x1 = (_dx*_dx + _dy*_dy + _dz*_dz)**0.50
                # considering shortening only (ASAS)
                # TODO : this should work for any format
                _x1 = _x1 - node1[0] + node2[0]
        else:
            _dx = self.node[0].x - self.node[-1].x
            _dy = self.node[0].y - self.node[-1].y
            _dz = self.node[0].z - self.node[-1].z
            #
            _x1 = (_dx*_dx + _dy*_dy + _dz*_dz)**0.50
        #
        return _x1
    #
    @property
    def length_node2node(self):
        """
        node to node lenght
        """
        _dx = self.node[0].x - self.node[-1].x
        _dy = self.node[0].y - self.node[-1].y
        _dz = self.node[0].z - self.node[-1].z
        #
        _x1 = (_dx * _dx + _dy * _dy + _dz * _dz)**0.50
        #
        return _x1
    #
    def zero_length(self, tolerance=0.001):
        """
        """
        #
        _dx = self.node[0].x - self.node[-1].x
        _dy = self.node[0].y - self.node[-1].y
        _dz = self.node[0].z - self.node[-1].z
        #
        _x1 = (_dx*_dx + _dy*_dy + _dz*_dz)**0.50
        
        if _x1 <= tolerance:
            return True
        else:
            return False
    #
    def find_coordinate(self, node_end, distance):
        """
        """
        # 
        _nodeNo3 = [0, 0, 0]
        #
        #if math.isclose(node_distance, 0, rel_tol=0.01):
        if distance <= 0.001 :
            _nodeNo3[0] = self.node[node_end].x 
            _nodeNo3[1] = self.node[node_end].y
            _nodeNo3[2] = self.node[node_end].z
        else:
            if node_end == 1:
                _v1 = (self.node[0].x - self.node[1].x)
                _v2 = (self.node[0].y - self.node[1].y)
                _v3 = (self.node[0].z - self.node[1].z)
            else:
                _v1 = (self.node[1].x - self.node[0].x)
                _v2 = (self.node[1].y - self.node[0].y)
                _v3 = (self.node[1].z - self.node[0].z)
            #
            _norm = sqrt(_v1 ** 2 + _v2 ** 2 + _v3 ** 2)
            #
            _v1 /= _norm
            _v2 /= _norm
            _v3 /= _norm
            
            _nodeNo3[0] = (self.node[node_end].x + _v1 * distance)
            _nodeNo3[1] = (self.node[node_end].y + _v2 * distance)
            _nodeNo3[2] = (self.node[node_end].z + _v3 * distance)
        #
        return geomop.Coordinates(*_nodeNo3) #_nodeNo3
    #
    def guidepoint(self):
        """ """
        angle = radians(self.properties.local_system)
        L = self.length_node2node
        dx = self.node[0].x - self.node[-1].x
        dy = self.node[0].y - self.node[-1].y
        dz = self.node[0].z - self.node[-1].z
        #
        l = dx/L
        m = dy/L
        n = dz/L
        # 5% tol from vertical Z
        if isclose(abs(n), 1, rel_tol=0.05):
            return [sin(angle), cos(angle), 0]
        #
        return [sin(angle), 0, cos(angle)]
        #print('-->')
#
#
