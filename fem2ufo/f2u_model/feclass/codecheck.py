# 
# Copyright (c) 2009-2020 fem2ufo
# 
# Python stdlib imports

# package imports

#
# CODE CHECK Section
class CodeCheck:
    """
    FE Code Check Class
    
    CodeCheck
        |_ name
        |_ number
        |
        |_ Lb [x, y, z] (Unbraced length of the compression flange for local buckling due to bending)
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    __slots__ = ('number', 'name', 'Lb')

    def __init__(self):
        """
        """
        #self.name = Name
        #self.number = Number
        #
        self.Lb = [False, False, False]
#
#