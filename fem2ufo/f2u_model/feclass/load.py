#
# Copyright (c) 2009-2021 fem2ufo
# 

# Python stdlib imports
import sys
from collections import namedtuple

# package imports
#

#
# Reactions Calcs
#
def two_triangle_udl(w1, w2, a, b, c):
    """
         w1|     
    |------|-----|-----|
   R1  a      b  |  c  R2
                 |
                 | w2
    """
    #
    _L0  = a + b + c
    
    # get slope
    m = (w2 - w1) / b
    
    # get triangle base
    h1 = abs(w1 / m)
    h2 = abs(w2 / m)
    
    # get total load
    P1 = w1 * h1 / 2.0
    P2 = w2 * h2 / 2.0
    #
    if abs(P1) > abs(P2):
        R1 = (((P1 * (2/3.0*h1 + h2 + c))
               - (P2 * (h2/3.0 + c)))/_L0)
        R2 = P1 + P2 - R1
    else:
        #
        R2 = ((P2 * (2/3.0 * h2 + h1 + a) 
               - P1 * (h1/3.0 + a))/_L0)
        R1 = P1 + P2 - R2
    #
    return R1, R2
#
def find_proportion(w1, w2):
    """
    find the box and triangular load proportion
    wbox = box load
    wtrg = triangle load
    """
    # case 1 - both load the same magnitud
    if w1 == w2:
        wbox = w1
        wtrg = w2
    # case 2 - diff magnitud
    else:
        #
        if w1 < 0 :
            wbox = max(w1, w2)
            wtrg = min(w1, w2) - wbox
        
        else:
            wbox = min(w1, w2)
            wtrg = max(w1, w2) - wbox
    #
    return wbox, wtrg   
#
def trapezoid_udl(w1, w2, _a, _b, _c):
    """
                 | w2
         w1|     |
    |------|-----|-----|
   R1  a      b     c  R2
   
    """
    
    _L0  = _a + _b + _c
    
    wb, wt  = find_proportion(w1, w2)
    
    # box reactions
    Pbox = wb * _b / (2 * _L0)
    R1box = Pbox * (2 * _c + _b)
    R2box = Pbox * (2 * _a + _b)
    
    # udl is uniform       
    if wb == wt:           
        return R1box, R2box    
    
    # triangle reactions
    Ptrg = wt * _b / 2.0
    # find R1
    if wb == w1:
        R1trg = Ptrg *(_b/3.0 + _c) / _L0
        R2trg = Ptrg *(2*_b/3.0 + _a) / _L0
    else:
        R1trg = Ptrg *(2*_b/3.0 + _a) / _L0 
        R2trg = Ptrg *(_b/3.0 + _c) / _L0
    
    # total trapezoid reactions
    R1 = (R1box + R1trg)
    R2 = (R2box + R2trg)
    
    return R1, R2
#
def triangle_udl(w1, w2, a, b, c):
    """
         w1     
    |------+-----|-----|
   R1  a      b  |  c  R2
                 |
                 | w2
    """
    _L0  = a + b + c
    
    w = w1
    
    try:
        _test = 1.0 / w
        P = w * b / 2.0
        R1 = P * (2/3.0 * b + c) / _L0
        R2 = P - R1
    
    except ZeroDivisionError:
        w = w2
        P = w * b / 2.0
        R2 = P * (2/3.0 * b + a) / _L0
        R1 = P - R2
    #
    return R1, R2
#
#
# udl flat clacs
#
def reaction_to_two_trg_udl(R1, R2, _L0):
    """
    """
    #
    w1 = 2*R1 / _L0
    w2 = 2*R2 / _L0
    w3 = w1 + w2
    #
    return w1, w2
#
def reaction_to_trpz_udl(R1, R2, _a, _b, _c):
    """
    |------|-----|-----|
   R1  a      b     c  R2
    """
    
    # total beam lenght
    _L0  = _a + _b + _c
    
    # find centroid
    _x = _b/2.0 + _b/6.0 * (abs(R1 - R2) / (R1 + R2))
    #
    if abs(R1) > abs(R2):
        Lx1 = _a + _x
        Lx2 = _c + (_b - _x)
    else:
        Lx1 = _a + (_b -_x)
        Lx2 = _c + _x
    
    # Find max beam moment
    Mmax = R1 * Lx1
    Mmax2 = R2 * Lx2
    
    if abs(Mmax) < abs(Mmax2):
        Mmax = Mmax2
    #   
    #
    try:
        1/0.0
        
        a = Lx1
        b = Lx2
        L = _L0
        #
        P = Mmax * L / (a*b)
        #if abs(P) < abs(R1 + R2):
        #    P = R1 + R2
        print('-->', P, R1+R2)
        
        # bending moment
        BM1 = a * (L**2 - a**2) / (3*L**2)
        BM2 = b * (L**2 - b**2) / (3*L**2)
    
        # shear
        V1 = (1/3.0 - a**2 / L**2) / 2.0
        V2 = (1/3.0 - b**2 / L**2) / 2.0
    
        #
        w1 = (Mmax / BM1) / (1 - (V1 / V2))
        w2 = (w1 * BM1 - Mmax) / BM2
        #print('-->', w1+w2, P)
        #
        # reactions
        #
        Rw1 = (2 * w1 / (3*L)) - (3 * w2 / L) - P
        Rw2 = P - Rw1
        #print('==>', R1+R2, P)
        #
        w1 =  -3 * Rw1 / L
        w2 =  -3 * Rw2 / L
        #print('-->')
    
    except ZeroDivisionError:
        # find reactions
        R1n = Mmax / Lx1
        R2n = Mmax / Lx2
        #
        if abs(R1n) < abs(R2n):
            R1n = R1 + R2 - R2n
        
        R2n = R1 + R2 - R1n        
    
        # convert reactions to udl
        w1 = 2 * R1n / _L0
        w2 = 2 * R2n /_L0
    #
    return w1, w2    
#
#
# --------------------
# LOAD Clases
# --------------------
#
class Basic:
    """
    FE Load Class
    
    Load
        |_ name
        |_ number
        |_ node [nd1, nd2, nd3,..., ndi]
        |_ element [elm1, elm2, elm3,..., elmi]
        |_ time_series [th[0], th[1], th[2],..., th[n]]
        |_ gravity [x, y, z, mx, my, mz]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'node',  'gravity',
                 'sets', 'time_series', 'class_type',
                 'analysis', 'concept', 'element',
                 'areas', 'shell', 'acceleration')
    #
    def __init__(self, name, number=None):
        """
        """
        #
        self.name = name
        if isinstance(number, (float, int, str)):
            self.number = int(number)
        self.node = []
        self.element = []
        self.concept = []
        self.gravity = []
        self.time_series = []
        self.areas = []
        self.shell = []
        self.acceleration = []
        #
        self.class_type = 'basic load'
        self.analysis = None
        #self.design_condition = 'lcOperating'
    #
    def get_name(self, number=False, name=False):
        """
        """
        if not number:
            number = self.number
        #
        if not name:
            name = self.name
        
        self.name = 'BLC' + str(number).zfill(3) + '_' + str(name)
#
class Node:
    """
    FE Node Load class
    
    NodeLoad
        |_ name
        |_ number
        |_ type
        |_ complex
        |_ point [x, y, z, mx, my, mz]
        |_ acceleration [th[0], th[1], th[2],..., th[n]]
        |_ displacement [th[0], th[1], th[2],..., th[n]]
        |_ mass
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'complex', 'type', 'dof',
                 'point', 'displacement', 'acceleration')
    #
    def __init__(self, name, number=None, Complex=0, Type=1, DOF=6):
        #
        self.number = number
        self.name = name
        self.complex = Complex
        self.type = Type
        self.dof = DOF
        self.point = []
        self.acceleration = []
        self.displacement = []
    
#
class Element:
    """
    FE Element Load Class
    
    ElementLoad
        |_ name
        |_ number
        |_ type
        |_ option
        |_ complex
        |_ point [x, y, z, mx, my, mz]
        |_ distributed
        |_ pressure
        |_ temperature
        |_ distance
        |_ mass
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name',  # 'type', 'complex', 'dof',
                  'distributed', 'point', # 'option',
                 'pressure', 'temperature')
    #
    def __init__(self, name, number=None):
        """ """
        self.name = name
        self.number = number
        self.distributed = []
        self.point = []
        self.pressure = []
        self.temperature = []
        #self.distance = []
    #
#
class BeamUDL:
    """
    """
    __slots__ = ('number', 'name', 'beam_length',
                 'distance_from_node', 'magnitude',
                 'type', 'complex', 'option', 'dof')
    #
    def __init__(self, Name, Number=None, DOF=6):
        """ """
        self.number = Number
        self.name = Name
        self.dof = DOF
    
    def input_udl(self, magnitude, distance=False):
        """
        magnitude = list[Fx1, Fy1, Fz1, Fx2, Fy2, Fz2]
        distance = list[node 1, node 2]
        """
        self.magnitude = magnitude
        self.distance_from_node = [0, 0]
        if distance:
            self.distance_from_node = distance
        
    
    def get_flat_udl(self, DoF_list):
        """
        converts from partial udl to full udl along the beam
        """
        _udl = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
        for DoF in DoF_list:
            _type = BeamUDL.get_udl_type(self, DoF)
            if not _type:
                continue
            
            R1, R2 = BeamUDL.get_beam_reactions(self, DoF, _type)
            
            if R1 is None:
                print('  error load type {:} not found'.format(_type))
                sys.exit()                
            elif _type == 'double_inverted_triangle':
                _udl[0][DoF], _udl[1][DoF] = reaction_to_two_trg_udl(R1, R2, 
                                                                     self.beam_length)
            else:
                _L1 = self.beam_length - self.distance_from_node[0] - self.distance_from_node[1]
                if _L1 == 0.0:
                    _udl[0][DoF], _udl[1][DoF] = 0, 0
                else:
                    _udl[0][DoF], _udl[1][DoF] = reaction_to_trpz_udl(R1, R2,
                                                                      self.distance_from_node[0], 
                                                                      _L1,
                                                                      self.distance_from_node[1])
        #
        return _udl
    
    def get_udl_type(self, x):
        """
        """
        #for x in range(3):
        # check if udl end 1 or 2 is zero
        try:
            _test1 = 1.0 / self.magnitude[0][x]
            _test2 = 1.0 / self.magnitude[1][x]
            #
            if self.magnitude[0][x] == self.magnitude[1][x]:
                _type = 'uniform'
            
            # check if load with diff sign
            elif self.magnitude[0][x] / self.magnitude[1][x] < 0:
                _type = 'double_inverted_triangle'
            #
            else:
                _type = 'trapezoidal'
        # udl should be single triangle or zero
        except ZeroDivisionError:
            # check if udl end 2 is zero
            try:
                _test = 1.0 / self.magnitude[1][x]
                _type = 'triangle'
            #
            except ZeroDivisionError:
                # check if udl end 1 is zero
                try:
                    _test = 1.0 / self.magnitude[0][x]
                    _type = 'triangle'
                #
                except ZeroDivisionError:
                    _type = False
        #
        return _type
    
    def get_beam_reactions(self, x, _type=False):
        """
        """
        #for x in range(3):
        if not _type:
            _type = BeamUDL.get_udl_type(self, x)
        
        # effective udl length
        _L1 = self.beam_length - self.distance_from_node[0] - self.distance_from_node[1]
        
        if _type == 'uniform' or _type == 'trapezoidal':
            #R1 = (self.magnitude[x] * _L1 * self.distance_from_node[1]) / self.beam_length
            #R2 = (self.magnitude[x + 3] * _L1 * self.distance_from_node[0]) / self.beam_length
            R1, R2 = trapezoid_udl(self.magnitude[0][x], 
                                   self.magnitude[1][x],
                                   self.distance_from_node[0], _L1,
                                   self.distance_from_node[1])            
            
        
        elif _type == 'double_inverted_triangle':
            R1, R2 = two_triangle_udl(self.magnitude[0][x], 
                                      self.magnitude[1][x], 
                                      self.distance_from_node[0], _L1,
                                      self.distance_from_node[1])
        
        #elif _type == 'trapezoidal':
        #    R1, R2 = trapezoid_udl(self.magnitude[x], 
        #                           self.magnitude[x + 3],
        #                           self.distance_from_node[0], _L1,
        #                           self.distance_from_node[1])
        
        elif _type == 'triangle':
            R1, R2 = triangle_udl(self.magnitude[0][x], 
                                  self.magnitude[1][x],
                                  self.distance_from_node[0], _L1,
                                  self.distance_from_node[1])
        
        else:
            return None, None
        #
        return R1, R2
#
#
class Combination:
    """
    FE Load Combination Class
    
    LoadCombination
        |_ name
        |_ number
        |_ design_condition : operating/storm
        |_ functional_load [load number, factor]
        |_ wave_load [load number, factor]
        |_ wind_load [load number, factor]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'functional_load',
                 'level', 'metocean_load', 
                 'design_condition', 'class_type')
    #
    def __init__(self, Name, Number=None, level=0):
        #
        self.name = Name
        self.number = Number
        self.level = level
        self.functional_load = []
        self.metocean_load = []
        #
        #self.analysis = None
        self.class_type = 'combination'
    #
    def get_name(self, number=False, name=False, level=False):
        """
        """
        if not number:
            number = self.number
        #
        if not name:
            name = self.name
        #
        if not level:
            level = self.level
        
        _level_name = 'LCA'
        if level != 0:
            _level_name = 'LCF'
        
        self.name = _level_name + str(number).zfill(3) + '_' + str(name)
#
Size = namedtuple('Size', ['length', 'width', 'height'])
Mass = namedtuple('Mass', ['mass', 'x', 'y', 'z'])
windPressure = namedtuple('wind_pressure', ['drag_coefficient', 'suction_factor'])
#
#
class Projection:
    """
    """
    __slots__ = ('_projection')
    
    def __init__(self):
        self._projection = [None, None, None]
    
    def __getitem__(self, index):
        """
        """
        return self._projection[index]
    
    def __setitem__(self, index, value):
        """
        """
        self._projection[index] = value
#
#
class Equipment:
    """
    name
    number
    size = [length, height, width]
    mass = [mass, CoGx, CoGy, CoGz]
    wind_pressure [drag_coefficient, suction_factor]
    local_system [master, y_dir, x-dir, angle]
    """
    __slots__ = ('number', 'name', 'type',
                 '_mass', '_size', 'foot_print',
                 'wind_pressure', 'joints', 'tolerance',
                 'load_interface', 'local_system')
    
    def __init__(self, name, number, size=None):
        """
        """
        self.name = name
        self.number = number
        self._size = size
        self._mass = None
        self.foot_print = []
        # wind section
        self.wind_pressure = [None, None, None]
        self.joints = []
        self.load_interface = []
        # new stuff
        self.local_system = []
        self.tolerance = [0, 0, 0] # m
    #
    @property
    def size(self):
        """
        """
        return self._size
    @size.setter
    def size(self, value):
        """
        """
        self._size = Size(*value)
    #
    @property
    def mass(self):
        """
        """
        return self._mass
    @mass.setter
    def mass(self, mass):
        """
        """
        _mass = mass[0]
        CoG = mass[1]
        if not CoG:
            self._mass = Mass(_mass, 
                              self._size.length/2.0,
                              self._size.width/2.0,
                              self._size.height/2.0)
        else:
            self._mass = Mass(_mass, CoG[0], CoG[1], CoG[2])
    #
    def wind_pressure_x(self, *value):
        """
        """
        self.wind_pressure[0] = windPressure(value[0], value[1])
    
    def wind_pressure_y(self, *value):
        """
        """
        self.wind_pressure[1] = windPressure(value[0], value[1])
    
    def wind_pressure_z(self, *value):
        """
        """
        self.wind_pressure[2] = windPressure(value[0], value[1])
#
#
#
#class TimeSeries:
#    """
#    """
#    def __init__(self, name, number=None):
#        """
#        """
#        #
#        self.name = name
#        if number:
#            self.number = int(number)
#        #
#        self.dof = None
#        self.node = None
#        self.depth_below_mudline = None
#        self.factor = 1.0
#
#

TimeSeries = namedtuple('TimeSeries',['name','number',
                                      'database', 'factors'])