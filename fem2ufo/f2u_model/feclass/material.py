# 
# Copyright (c) 2009-2020 fem2ufo
# 


# Python stdlib imports

# package imports
import fem2ufo.process.common.control as common

#
def find_material_item(word_in):
    """
    """
    _key = {"Fy": r"\b((fy|yield)(\s*strength|stress)?)\b",
            "Fu": r"\b((fu|u(ltimate)?\s*t(ensile))(\s*strength|stress)?)\b",
            "E": r"\b((E(lastic)?|young(\'s)?|tensile)(\s*modulus)?)\b",
            "poisson": r"\b(poisson(\s*ration)?|nu|\u03BD)\b",
            "alpha": r"\b(termal\s*expansion|alpha|\u03B1)\b",
            "stiffness": r"\b(stiffness|k)\b",
            "density": r"\b(density|rho|\u03C1)\b",
            "spring": r"\b(spring)\b",
            "type": r"\b(type)\b",
            "damping": r"\b(damping)\b",
            "G": r"\b(g|shear\s*modulus)\b",
            "name": r"\b(name[s]?|id|nom(bre)?[s]?|navn)\b",
            "number": r"\b(num(ber[s]?|mer|ero[s]?))\b",
            "factor": r"\b(factor)\b",
            "grade" :  r"\b(grade)\b"}

    _match = common.find_keyword(word_in, _key)

    return _match
# 
class Material:
    """
    FE model material class
    
    Material
        |_ name
        |_ number
        |_ type
        |_ Emodulus
        |_ poisson
        |_ Fy
        |_ density
        |_ alpha
        |_ damping
        |_ stiffness [k1, k2, k3,..., kn]
        |_ spring    [sp[0],...]
        |_ Gmodulus (shear modulus)
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
      :coordinate:  list node coordinates 
    """
    __slots__ = ('number', 'name', 'E', 'poisson', 'density',
                 'Fy', 'Fu', 'damping', 'alpha', 'stiffness', 'spring',
                 'type', 'group', 'grade', 'elements', 'G', 'concepts')

    def __init__(self, name, number=None):
        #
        self.number = number
        self.name = name
        self.stiffness = []
        self.spring = []
        self.grade = None
        self.elements = []
        self.concepts = list()
        self.alpha = 0
        self.poisson = 0.30
        self.Fy = 0
        self.damping = 0
        self.type = None
    #
    def set_properties(self, **kwargs):
        """
        """
        for key, value in kwargs.items():
            
            _item = find_material_item(key)
            
            if _item == 'Fy':
                self.Fy = float(value)
            
            elif _item == 'Fu':
                self.Fu = float(value)
            
            elif _item == "E":
                self.E = float(value)
                
            elif _item == "G":
                self.G = float(value)
            
            elif _item == "poisson":
                self.poisson = float(value)
            
            elif _item == "density":
                self.density = float(value)
            
            elif _item == "damping":
                self.damping = float(value)
            
            elif _item == "alpha":
                self.alpha = float(value)
            
            elif _item == "type":
                self.type = value
            
            else:
                print("   *** warning material item {:} not recognized".format(key))
    #
    def get_name(self, number, _grade=False):
        """
        """
        if not _grade:
            try:
                _grade = round(self.grade)
            except TypeError:
                _grade = 'Zero'
        # in case material already named (i.e. sacs case)
        _name = '_' + self.name
        if '_MT' in _name:
            _name = ''
        #
        self.name = 'MT' + str(number).zfill(3) + '_' + str(_grade) + _name
    #
    def equal(self, other, grade=None):
        """
        """
        if not grade:
            grade = other.grade
        # TODO : check if material type is needed
        if self.type == other.type \
        and self.E == other.E \
        and self.grade == grade \
        and self.poisson == other.poisson \
        and self.density == other.density :
            return True
        else:
            return False

#