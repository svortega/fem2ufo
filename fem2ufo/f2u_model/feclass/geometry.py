# 
# Copyright (c) 2009-2021 fem2ufo
# 


# Python stdlib imports
from math import isclose
from collections import OrderedDict

# package imports
import fem2ufo.f2u_model.operations.geometry as geomop

#
#
def point_load2mass(point_load, gravity, mass_factor):
    """
    """
    # start node mass conversion
    #if not item_mass:
    #    item_mass = [0, 0, 0, 0, 0, 0]
    
    #for DoF in DoF_list:
    # check dof to be converted
    try:
        _test = 1.0 / point_load
        item_mass = abs((point_load / gravity) * mass_factor)
        #_w[DoF] = 0
    except ZeroDivisionError:
        #print('   **warning load {:} is zero'
        #      .format(load_number))
        return 0
    
    return item_mass
#
#
def get_node_end(_number_nodes, line):
    """
    """
    if _number_nodes == 3:
        #print('triangle')
        if line == 1:
            return 1, 2
        elif line == 2:
            return 0, 2
        else:
            return 0, 1
    else:
        #print('quad')
        if line == 1:
            return 0, 1
        elif line == 2:
            return 1, 2
        elif line == 3:
            return 2, 3
        else:
            return 3, 0   
#
def get_node2node_length(node, end_1, end_2):
    """
    """
    _dx = node[end_1].x - node[end_2].x
    _dy = node[end_1].y - node[end_2].y
    _dz = node[end_1].z - node[end_2].z
    _length = (_dx*_dx + _dy*_dy + _dz*_dz)**0.50
    #
    return _length
#
#
#  FE Classes
#
class Node:
    """
    FE model node class 
    
    Node
        |_ name
        |_ number
        |_ coordinates[x, y, z]
        |_ boundary b[n]
        |_ mass [mx, my, mz,..., mn]
        |_ elements [element[1], element[2],.., element[n]]  
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
      :set_coordinate:  list node coordinates 
      : x (returns coordinate x)
      : y (returns coordinate y)
      : z (returns coordinate y)
      :set_boundary:  
      :mass:
      :elements: list of elements attached to the node                  
    """
    __slots__ = ('number', 'name', 'coordinate_system',
                 'boundary', 'mass', 'load', 'elements',
                 'coordinates', 'rotation')
    #
    #
    def __init__(self, name, number):
        #
        self.name = name
        self.number = number
        #self.coordinate = namedtuple('coordinate', 'x y z')
        self.load = {}
        self.elements = []
        self.boundary = {}
        self.mass = []
        self.rotation=None
        #
    #
    @property
    def x(self):
        return self.coordinates.x
    #
    @property
    def y(self):
        return self.coordinates.y
    #
    @property
    def z(self):
        return self.coordinates.z
    # 
    def set_coordinates(self, coord_x, coord_y, coord_z):
        """
        read node's coordinates 
        
        **Parameters**:
          :coord: [x, y, z]
        """
        self.coordinates = geomop.Coordinates(coord_x, coord_y, coord_z)
    #
    def get_coordinates(self, factor=1.0):
        """
        get a list with the node's coordinates
        
        **Parameters**:
          :factor: factor applied to the node's coordinates (1 default)
        """
        #print(self.coordinates)
        return [self.coordinates.x * factor, self.coordinates.y * factor, self.coordinates.z * factor]
    #
    @property
    def print_coordinates(self):
        """
        """
        return self.coordinates
    #
    def convert_load_to_mass(self, load_number, gravity, mass_factor=1, 
                             DoF=2, delete_load=False):
        """
        """
        # 
        _total = Node.get_sum_point_load(self, load_number)
        if not _total:
            return None
        
        # start node mass conversion
        if not self.mass:
            self.mass = [0, 0, 0, 0, 0, 0]
        #
        self.mass[DoF] += point_load2mass(_total[DoF], gravity, mass_factor)
        _total[DoF] = 0
        
        # if not load, empty list
        if sum(_total) == 0.0:
            self.load[load_number].point = []
        #
        else: 
            # check if load should be deleted
            if delete_load:
                self.load[load_number].point = []
            # update point load with total
            else:
                self.load[load_number].point = [_total]
        # 
    #
    def get_sum_point_load(self, load_number):
        """
        sums all the point loads within the list
        """
        # check if point load exist
        try:
            #self.load[load_number]
            if not self.load[load_number].point:
                return False 
        except KeyError:
            return False
        # sum all point load within the load case
        _total = [sum(i) for i in zip(*self.load[load_number].point)]
        return _total    
    #
    def set_boundary(self, boundary=None):
        """
        """
        if boundary:
            self.boundary = boundary
        
        else:
            self.boundary = {}
    #
    def equal(self, other):
        """
        """
        if (isclose(self.x, other.x, abs_tol=1e-03) 
            and isclose(self.y, other.y, abs_tol=1e-03)
            and isclose(self.z, other.z, abs_tol=1e-03)):
            return True
        
        return False
#
class GuidePoint:
    """
    FE element guide point (univector)
    
    GuidePoint
        |_ name
        |_ number
        |_ cosine [vx, vy, vz,..., vn]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
      :cosine: univector
    """
    __slots__ = ('number', 'name', 'cos', 'type',
                 'nodes')
    #
    def __init__(self, Name, Number=False):
        #
        self.number = Number
        self.name = Name
        self.cos = []
        # self.cos1  = unvx
        # self.cos2  = unvy
        # self.cos3  = unvz
        #
        self.nodes = []

#
class Groups:
    """
    FE model groups
    
    Sets
        |_ name
        |_ number
        |_ type
        |_ items [m1, m2, m3,..., mn]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
      :type:
      :items:
    """
    #
    __slots__ = ('number', 'name', 'items', 'nodes', 'concepts')

    def __init__(self, Name, Number=None):
        #
        self.number = Number
        self.name = Name
        self.concepts = []
        self.items = []
        self.nodes = []
        #
    #
    def get_name(self, _number=False):
        """
        """
        if not _number:
            _number = self.number
        #
        _name = ""
        if self.name != str(self.number):
            _name = '_'+ self.name
        #
        self.name = 'GR' + str(_number).zfill(3) + _name

#
class Eccentricities:
    """
    Member eccentricities
    
    Eccentricities
        |_ name
        |_ number
        |_ case
        |_ eccentricity [x, y, z,..., n]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
      :eccentricity: list
      :case: 
            1- global axis system
            2- local  system
    """
    #
    __slots__ = ('number', 'name', 'eccentricity',
                 'case', 'option')

    def __init__(self, Name, Number=None):
        #
        self.number = Number
        self.name = Name
        self.eccentricity = []
        #self.eccentricity = namedtuple('eccentricity', 'x y z')
        self.case = 1
        # self.Y  = Y
        # self.Z  = Z
    #
    def normal(self):
        """
        """
        _v1 = self.eccentricity[0]
        _v2 = self.eccentricity[1]
        _v3 = self.eccentricity[2]
        #
        return (_v1**2 + _v2**2 + _v3**2)**0.50       

#
class Section:
    """
    Section property
    
    Section
        |_ name
        |_ number
        |_ type
        |_ shape
        |_ properties (object)
        |_ SFV shear factor vertical
        |_ SFH shear factor horizontal
        |_ elements [m1, m2, m3,..., mn]
        |_ length section's length
        |_ group 
        |_ step [section_1, section_2,...,section_n]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'type', 'shape',
                 'properties', 'SFV', 'SFH', 'length',
                 'elements', 'group', 'step', 'material')
    #
    def __init__(self, Name, Number, Type, Shape,
                 SFv=1.0, SFh=1.0):
        #
        self.number = Number
        self.name = Name
        self.type = Type
        self.shape = Shape
        self.SFV = SFv
        self.SFH = SFh
        self.elements = []
        #self.step = []
    #
    def equal(self, other):
        """
        """
        if other.shape == 'general section':
            
            if self.shape == 'general section':
                if self.properties.Area == other.properties.Area \
                and self.properties.Iip == other.properties.Iip \
                and self.properties.Iop == other.properties.Iop \
                and self.properties.J == other.properties.J \
                and self.properties.SCV == other.properties.SCV \
                and self.properties.SCH == other.properties.SCH :
                    return True
                else:
                    return False
            else:
                return False
        #
        elif self.shape == 'general section':
            
            if other.shape == 'general section':
                if self.properties.Area == other.properties.Area \
                and self.properties.Iip == other.properties.Iip \
                and self.properties.Iop == other.properties.Iop \
                and self.properties.J == other.properties.J \
                and self.properties.SCV == other.properties.SCV \
                and self.properties.SCH == other.properties.SCH :
                    return True
                else:
                    return False
            else:
                return False         
        #
        else:
            if self.properties.B1 == other.properties.B1 \
            and self.properties.B2 == other.properties.B2 \
            and self.properties.Bfb == other.properties.Bfb \
            and self.properties.Bft == other.properties.Bft \
            and self.properties.D == other.properties.D \
            and self.properties.Tfb == other.properties.Tfb \
            and self.properties.Tft == other.properties.Tft \
            and self.properties.Tw == other.properties.Tw :
                return True
            else:
                return False            
    #
    def get_name(self, length_unit, number=False):
        """
        """
        if not number:
            number = self.number
        
        if self.shape == 'general section':
            self.name = 'GB' + str(number).zfill(3)
        
        else:
            self.name = self.properties.get_name(length_unit, number)
#
class Hinges:
    """
    FE elements releases
    
    Hinges
        |_ name
        |_ number
        |_ type
        |_ fix [x, y, z, mx, my, mz]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    __slots__ = ('number', 'name', 'option', 'fix')

    def __init__(self, Name, Number=None, Option='', Trano='', Fix=''):
        #
        self.number = Number
        self.name = Name
        self.option = Option
        self.fix = Fix
    #
    def get_name(self, _number=False):
        """
        """
        if not _number:
            _number = self.number
        
        rel = "".join(str(e) for e in self.fix)
        #
        self.name = 'HG' + str(_number).zfill(3) +'_'+ rel
#
class Boundary:
    """
    FE Fixity
    
    Boundary
        |_ name
        |_ number
        |_ type
        |_ constrain [x, y, z, mx, my, mz]
    
    **Parameters**:  
      :name : coupling, pile
      :type : free, fix, dependent, prescribed, supernode, master
      :constrain : 
            0- free
            1- fixed
            2- prescribed
            3- dependent
            4- supernode
    """
    
    __slots__ = ('number', 'name', 'type', 'releases',
                 'master', 'slave', 'nodes', 'elements',
                 'link', 'dependence')
    #
    fix_type = ['release', 'gap', 'prescribed', 'dependence',
                'link', 'master', 'rigid', 'constrain']
    #
    def __init__(self, Name, Number=None):
        """
        """
        self.number = Number
        self.name = Name
        self.type = 'free'
        self.slave = []
        self.nodes = []
        self.link = []
    #
    def set_releases(self, constrain):
        """
        """
        if type(constrain) == list:
            self.releases = geomop.Fixity._make(constrain)
        
        elif type(constrain) == dict:
            self.releases = geomop.Fixity(**constrain)
    #
    @property
    def fixed(self):
        """
        """
        constrain = [1, 1, 1, 1, 1, 1]
        self.releases = geomop.Fixity._make(constrain)
    #
    @property
    def pinned(self):
        """
        """
        constrain = [1, 1, 1, 0, 0, 0]
        self.releases = geomop.Fixity._make(constrain)
#
class Joints:
    """
    FE concept joint
    
    Joints
        |_ name
        |_ number
        |_ type {shim}
        |_ node [nd[1], nd[2], nd[3],..., nd[n]]
        |_ chord [m[1], m[2]]
        |_ brace [m[1], m[2], m[3],..., m[n]]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'node', 'name', 'overlap',
                 'chord', 'brace', 'type', 'spring')

    def __init__(self, name, node=None):
        """
        """
        #self.number = Number
        self.name = name
        self.node = node
        #self.classification = 'msl'
        #self.flag = False
        self.brace = []
        self.chord = []
        self.type = {}
    #
    def get_name(self, number=None, name=False):
        """
        """
        if not number:
            number = self.node.number
        #
        if not name:
            name =  self.name # "jnt"
        #
        self.name = name + str(number).zfill(3) + '_' + str(self.node.name)
    #
    def set_spring(self, spring):
        """
        """
        if type(spring) == list:
            self.spring = geomop.Spring._make(spring)
        
        elif type(spring) == dict:
            self.constrains = geomop.Spring(**spring)
#
#
#
#
class SpringElement:
    """
    FE concept spring 
    
    Spring
        |_ name
        |_ number
        |_ force [f0, f1, f2,..., fn]
        |_ displacement [d0, d1, d2,..., dn]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'force', 'displacement',
                 'Pdelta', 'material', 'node', 'type',
                 'hydrodynamics', 'stress', 'section', # need to remove this line
                 'releases', 'guidepoint', 'offsets') # need to remove this line

    def __init__(self, Name, Number=None):
        self.number = Number
        self.name = Name
        self.force = []
        self.displacement = []
        self.Pdelta = []
        #
        self.material = []
        self.node = []
        self.type = 'spring'
        #
        self.stress = []
        self.section = []
        self.releases = []
        self.offsets = []
        self.guidepoint = []
    
    @property
    def length_node2node(self):
        """
        """
        # TODO: if not beam return false
        # if self.type.element ==
        #
        _dx = self.node[0].x - self.node[1].x
        _dy = self.node[0].y - self.node[1].y
        _dz = self.node[0].z - self.node[1].z        
        _x1 = (_dx * _dx + _dy * _dy + _dz * _dz)**0.50
        return _x1

#
class BeamElement:
    """
    FE Beam Element class  
    
    Element 
        |_ name
        |_ number
        |_ type
        |_ material [mt[0],..]
        |_ section [st[0],...]
        |_ node [nd[0], nd[1], nd[2],..., nd[n]]
        |_ offset [ecc[0], ecc[1], ecc[2],..., ecc[n]]
        |_ releases [rel[0], rel[1], rel[2],..., rel[n]]
        |_ guidepoint [v[0], v[1], v[2],..., v[n]]
        |_ mass
        |_ stress
        |_ code_check
        |_ concept
        |_ hydrodynamics
        |_ aerodynamics
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    __slots__ = ('number', 'name', 'type', 'material', 'node', 'load',
                 'section', 'offsets', 'releases', 'guidepoint', 'mass',
                 'stress', 'concept', 'code_check', 'overlap',  'group', # group?
                 'properties', 'step')
    #
    def __init__(self, name, number=None, Type='N/A'):
        self.number = number
        self.name = name
        self.type = Type
        self.material = []
        self.node = []
        self.section = []
        self.offsets = []
        self.releases = []
        self.guidepoint = []
        self.stress = []
        self.mass = []
        self.load = {}
        self.overlap = False
        self.properties = None
        self.code_check = None
        self.step = []

    @property
    def flexible_length(self):
        """
        """
        # TODO: if not beam return false
        if self.offsets:
            _case = 2
            try:
                node1 = self.offsets[0].eccentricity
                _case = self.offsets[0].case
            except AttributeError:
                node1 = [0, 0, 0]
            #
            try:
                node2 = self.offsets[1].eccentricity
                _case = self.offsets[1].case
            except AttributeError:
                node2 = [0, 0, 0]
            
            # global case
            if _case == 1:
                _dx = ((self.node[0].x + node1[0]) 
                       - (self.node[1].x + node2[0]))
                _dy = ((self.node[0].y + node1[1]) 
                       - (self.node[1].y + node2[1]))
                _dz = ((self.node[0].z + node1[2]) 
                       - (self.node[1].z + node2[2]))
                #
                _x1 = (_dx * _dx + _dy * _dy + _dz * _dz)**0.50
            # local case
            else:
                # beam
                _dx = self.node[0].x - self.node[1].x
                _dy = self.node[0].y - self.node[1].y
                _dz = self.node[0].z - self.node[1].z
                _x1 = (_dx * _dx + _dy * _dy + _dz * _dz)**0.50
                # considering shortening only (ASAS)
                # TODO : this should work for any format
                _x1 = _x1 - node1[0] + node2[0]
        else:
            _dx = self.node[0].x - self.node[1].x
            _dy = self.node[0].y - self.node[1].y
            _dz = self.node[0].z - self.node[1].z
            #
            _x1 = (_dx * _dx + _dy * _dy + _dz * _dz)**0.50
        #
        return _x1

    #
    @property
    def length_node2node(self):
        """
        node to node lenght
        """
        #
        return get_node2node_length(self.node, end_1=0, end_2=1)
        #
        #_dx = self.node[0].x - self.node[1].x
        #_dy = self.node[0].y - self.node[1].y
        #_dz = self.node[0].z - self.node[1].z
        #
        #_x1 = (_dx * _dx + _dy * _dy + _dz * _dz)**0.50
        #
        #return _x1
    #
    def convert_load_to_mass(self, load_number, DoF, gravity, mass_factor=1, 
                             delete_load=False, mass_beam=False):
        """
        """
        if not self.load[load_number].distributed:
            return False
        #
        #DoF = 2
        
        # allow member mass
        if mass_beam :
            
            _udl = BeamElement.get_sum_distributed_load(self, load_number)
            
            if not self.mass:
                self.mass = [0, 0, 0, 0, 0, 0]
            
            if _udl:
                self.mass[DoF] += abs((_udl[0][DoF] + _udl[1][DoF]) / (2 * gravity) * mass_factor)
            
            # FIXME: load is not updating
        # node mass only
        else:
            R0 = 0
            R1 = 0
            # TODO: Check if this works
            for _member_udl in self.load[load_number].distributed:
                
                r1, r2 = _member_udl.get_beam_reactions(DoF)
                if not r1:
                    continue
                #
                R0 += r1
                R1 += r2
                #
                _member_udl.magnitude[0][DoF] = 0
                _member_udl.magnitude[1][DoF] = 0
                # if not load, empty list
                # FIXME: load is not updating
                if sum(_member_udl.magnitude[0]) == 0.0 and sum(_member_udl.magnitude[1]) == 0.0:
                    _member_udl.magnitude = []
                    _member_udl.distance_from_node = []
            #
            if not self.node[0].mass:
                self.node[0].mass = [0, 0, 0, 0, 0, 0]
            
            self.node[0].mass[DoF] += point_load2mass(R0, gravity, mass_factor)
            
            if not self.node[1].mass:
                self.node[1].mass = [0, 0, 0, 0, 0, 0]
            
            self.node[1].mass[DoF] += point_load2mass(R1, gravity, mass_factor)
            # 
        #
        # check if delete load
        if delete_load:
            self.load[load_number].distributed = []
        
        #print('ok')
    #
    def get_sum_distributed_load(self, load_number, DoF_list=False, load_factor=1):
        """
        Flats udl and sum all the udl loads within the list
        """
        member_load = self.load[load_number]
        if not member_load.distributed:
            return False
        
        if not DoF_list:
            DoF_list = [0, 1, 2]
        
        _udl = []
        for _member_udl in member_load.distributed:
            if not _member_udl.magnitude:
                continue
            L = _member_udl.beam_length
            x1 = _member_udl.distance_from_node[0]
            x2 = _member_udl.distance_from_node[1]            
            node1_check = x1 / L
            node2_check = x2 / L
            if _member_udl.type == 2:
                #print('--->')
                _udl2 = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
                wx = L - x1 - x2
                a = x1 + wx * 0.50
                b = x2 + wx * 0.50
                for x in DoF_list:
                    P = _member_udl.magnitude[0][x] * wx * -1.0
                    Mmax = P * a * b / L
                    #Mmax = 2 * P * a**2 * b**2 / L**3
                    if Mmax == 0 :
                        continue
                    # bending moment
                    BM1 = a * (L**2 - a**2) / (3*L**2)
                    BM2 = b * (L**2 - b**2) / (3*L**2)
                    #BM1 = L*a/6.0 * (1 - a**2 / L**2)
                    #BM2 = L*b/6.0 * (1 - b**2 / L**2)
                    # shear
                    V1 = (1/3.0 - a**2 / L**2) / 2.0
                    V2 = (1/3.0 - b**2 / L**2) / 2.0
                    #
                    try:
                        w1 = (Mmax / BM1) / (1 - (V1 / V2))
                        w2 = (w1 * BM1 - Mmax) / BM2
                        #print('-->', w1+w2, P)
                        # reactions
                        #
                        R1 = (2 * w1 / (3*L)) - (3 * w2 / L) - P
                        R2 = P - R1
                        #print('==>', R1+R2, P)
                        #
                        # get slope
                        #m = (R2 - R1) / L
                        # get triangle base
                        #h1 = abs(R1 / m)
                        #h2 = abs(R2 / m)
                        #print('L = ', h1+h2, L)
                        #
                        _udl2[0][x] =  -3 * R1 / L
                        _udl2[1][x] =  -3 * R2 / L
                    except ZeroDivisionError:
                        R1 = P/2.0
                        R2 = R1
                        _udl2[0][x] =  -2 * R1 / L
                        _udl2[1][x] =  -2 * R2 / L
                _udl.append(_udl2)
            else:
                # check if distributed force
                # TODO: Check if this works
                if node1_check > 0.01 or node2_check > 0.01:
                    
                    _udl.append(_member_udl.get_flat_udl(DoF_list))
                else:
                    _udl2 = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
                    for DoF in DoF_list:
                        _udl2[0][DoF], _udl2[1][DoF] = _member_udl.magnitude[0][DoF], _member_udl.magnitude[1][DoF]
                    _udl.append(_udl2)
        #
        #
        total = len(_udl)
        if not total:
            return []
        #
        udl_total = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
        for mm in range(total):
            for DoF in DoF_list:
                udl_total[0][DoF] += _udl[mm][0][DoF]
                udl_total[1][DoF] += _udl[mm][1][DoF]
        #
        return [udl_total[0], udl_total[1]]
    #
    @property
    def slope(self):
        """
        """
        _dx = self.node[0].x - self.node[1].x
        _dy = self.node[0].y - self.node[1].y
        _dz = self.node[0].z - self.node[1].z
        
        run = (_dx * _dx + _dy * _dy )**0.50
        rise =  (_dx * _dx + _dy * _dy + _dz * _dz)**0.50  
        
        try:
            slope = rise/run
        except ZeroDivisionError:
            slope = 0
        
        return slope
#
class BeamElementProperty:
    """
    Hydrodynamic/Aerodynamic element's properties
    
    Element_Property
          |_ number
          |_ name
          |_ type : hydrodynamic/aerodynamic/code_check
          |
          |_ cdcm
          |_ air_drag
          |_ marine_growth
          |_ flooded
          |
          |_ L  [x, y, z] (Effective buckling length)
          |_ K  [x, y, z] (Effective length factor)
          |_ Cm [x, y, z] (moment amplification factor)
          |
          |_ local_system (rorate around local x axis)
    """
    __slots__ = ('number', 'name', 'type',  
                 'local_system', 'hydrodynamic_diameter',
                 'L', 'Cm', 'K', 'Lb', 'tapered',
                 'marine_growth', 'flooded',
                 'cdcm', 'air_drag', 'buoyancy_area')
    #
    def __init__(self, Type=None):
        #
        self.type = Type
        # hydro
        #self.cdcm = []
        # aero
        #self.air_drag = []
        #
        self.flooded = False # non flooded by default
        # code check
        self.Cm = [False, 1, 1]
        self.K = [False, 1, 1]
        self.L = [False, 
                   'bucklingLengthOptionBeamLength', 
                   'bucklingLengthOptionBeamLength']
        #
        self.tapered = []
        #
    #
#
#
class ShellElement:
    """
    FE Shell Element class  
    
    Element 
        |_ name
        |_ number
        |_ type
        |_ material [mt[0],..]
        |_ section [st[0],...]
        |_ node [nd[0], nd[1], nd[2],..., nd[n]]
        |_ offset [ecc[0], ecc[1], ecc[2],..., ecc[n]]
        |_ releases [rel[0], rel[1], rel[2],..., rel[n]]
        |_ guidepoint [v[0], v[1], v[2],..., v[n]]
        |_ mass
        |_ stress
        |_ code_check
        |_ concept
        |_ hydrodynamics
        |_ aerodynamics
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    __slots__ = ('number', 'name', 'type', 'material', 'node', 'load',
                 'section', 'offsets', 'releases', 'guidepoint', 'mass',
                 'stress', 'concept', 'code_check', 'group', 'overlap',
                 'properties', 'step', 'element')
    #
    def __init__(self, Name, Number=None, Type='N/A'):
        """
        """
        self.number = Number
        self.name = Name
        self.type = Type
        self.material = []
        self.node = []
        self.section = []
        self.offsets = []
        self.releases = []
        self.guidepoint = []
        self.stress = []
        self.mass = []
        self.load = {}
        self.overlap = False
        self.properties = None
        self.step = []
        self.element = OrderedDict()
    #
    def get_sum_distributed_load(self, load_number, load_factor=1):
        """
        Flats udl and sum all the udl loads within the list
        """
        if not self.load[load_number].pressure:
            return False
        
        _udl = []
        #
        for _pressure in self.load[load_number].pressure:
            _udl.append(_pressure)
        #
        #
        try:
            _total_0 = [sum(i) for i in zip(*_udl)]
            _total = [_load*load_factor for _load in _total_0]
            return _total
        except IndexError:
            return []
    #
    def line_length(self, line):
        """
        When line load is specified, the relation between local node numbers and loaded line will be:
        4 nodes:
            LINE =1 means line load between node 1 and 2 
            LINE =2 means line load between node 2 and 3 
            LINE =3 means line load between node 3 and 4 
            LINE =4 means line load between node 4 and 1
        3 nodes:
            Line 1 means line load between nodes 2 and 3. 
            Line 2 means line load between nodes 1 and 3. 
            Line 3 means line load between nodes 1 and 2.
        """
        _number_nodes = len(self.node)
        end_1, end_2 = get_node_end(_number_nodes, line)
        return get_node2node_length(self.node, end_1, end_2)
    
        #if _number_nodes == 3:
            #print('triangle')
            #if line == 1:
            #    return get_node2node_length(self.node, end_1=1, end_2=2)
            #elif line == 2:
            #    return get_node2node_length(self.node, end_1=0, end_2=2)
            #else:
            #    return get_node2node_length(self.node, end_1=0, end_2=1)
        #else:
            #print('quad')
            #if line == 1:
            #    return get_node2node_length(self.node, end_1=0, end_2=1)
            #elif line == 2:
            #    return get_node2node_length(self.node, end_1=1, end_2=2)
            #elif line == 3:
            #    return get_node2node_length(self.node, end_1=2, end_2=3)
            #else:
            #    return get_node2node_length(self.node, end_1=3, end_2=0)
        #
        #return
#
#
class SolidElement:
    """
    FE Solid Element class  
    
    Element 
        |_ name
        |_ number
        |_ type
        |_ material [mt[0],..]
        |_ section [st[0],...]
        |_ node [nd[0], nd[1], nd[2],..., nd[n]]
        |_ offset [ecc[0], ecc[1], ecc[2],..., ecc[n]]
        |_ releases [rel[0], rel[1], rel[2],..., rel[n]]
        |_ guidepoint [v[0], v[1], v[2],..., v[n]]
        |_ mass
        |_ stress
        |_ code_check
        |_ concept
        |_ hydrodynamics
        |_ aerodynamics
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
      
    """
    __slots__ = ('number', 'name')
    
    def __init__(self, Name, Number=None, Type='N/A'):
        self.number = Number
        self.name = Name