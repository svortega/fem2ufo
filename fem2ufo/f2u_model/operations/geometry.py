# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
from collections import namedtuple
#import sys

# package imports


#
class Coordinates(namedtuple('Coordinates', 'x y z')):
    """
    """
    __slots__ = ()
    
    def __str__(self):
        return "{: 14.5f} {: 14.5f} {: 14.5f}".format(self.x, self.y, self.z)
#
#
class Fixity(namedtuple('Fixity', 'x y z mx my mz')):
    """
    """
    __slots__ = ()
    
    def __str__(self):
        return "{: 14.5f} {: 14.5f} {: 14.5f}".format(self.x, self.y, self.z)
#
#
class Spring(namedtuple('Spring', 'x y z mx my mz')):
    """
    """
    __slots__ = ()
    
    def __str__(self):
        return "{: 14.5f} {: 14.5f} {: 14.5f}".format(self.x, self.y, self.z)
#
#
#
class CDCM(namedtuple('CdCm', 'Cdx Cdy Cdz Cmx Cmy Cmz')):
    """
    """
    __slots__ = ()
    
    def __str__(self):
        return "{: 14.5f} {: 14.5f} {: 14.5f}".format(self.x, self.y, self.z)
#
#
class CD(namedtuple('Cd', 'Cdx Cdy Cdz')):
    """
    """
    __slots__ = ()
    
    def __str__(self):
        return "{: 14.5f} {: 14.5f} {: 14.5f}".format(self.Cdx, self.Cdy, self.Cdz)
#
#
#
