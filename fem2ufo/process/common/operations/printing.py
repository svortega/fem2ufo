#
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports


# package imports

#
#
# ------------ new -----------------------
#
def print_column_num(items, max_slots=6):
    """
    """
    check_out = []
    _noMemb = len(items)
    _noSlot = 0
    i = 0
    for _item in items:
        _noSlot += 1
        i += 1
        check_out.append(" {:11.0f}".format(int(_item)))
        if _noSlot == max_slots or i == _noMemb:
            check_out.append("\n")
            _noSlot = 0
    #
    return check_out
#
def print_column_str(items, max_slots=6):
    """
    """
    check_out = []
    _noMemb = len(items)
    _noSlot = 0
    i = 0

    for _item in items:

        _noSlot += 1
        i += 1
        check_out.append(" {:}".format(_item))

        if _noSlot == max_slots or i == _noMemb:
            check_out.append("\n")
            _noSlot = 0
    #
    return check_out
#
#
def print_head_line(head, length=84):
    """
    print header line
    """
    check_out = []
    _head = str(head).upper()
    _space = length - 2 - len(_head)
    check_out.append("'\n")
    check_out.append("'\n")
    check_out.append("'{:}\n".format(length * '-'))
    check_out.append("'{:} {:}\n".format(_space * " ", _head))
    check_out.append("'{:}\n".format(length * '-'))
    check_out.append("'\n")
    check_out.append("'\n")

    return check_out
#
#
def load(load):
    """
    """
    header = 'LOAD LIST'
    FEMufo = print_head_line(header)
    #
    if load:
        FEMufo.append("'L_Case Load Name                      Old\n")
        FEMufo.append("'\n")
        i = 0
        j = 0
        _loadNo = len(load)
        for _key, _load in sorted(load.items()):

            #
            j += 1
            i += 1

            # if j == 1: FEMufo.append("'")

            FEMufo.append("  {:5.0f} {:28s} {:5.0f}"
                          .format(_load.number, _load.name[:28], _key))

            if j == 2 or i == _loadNo:
                FEMufo.append("\n")
                j = 0
    #
    else:
        FEMufo.append("' No load found\n")

    return FEMufo


#
def sections(section):
    #  
    # FEMufo = []
    header = 'LIST SECTION PROPERTIES'
    FEMufo = print_head_line(header)

    FEMufo.append("'{:} Original Database\n".format(65 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'secID Type    Name\n")
    FEMufo.append("'\n")
    #
    i = 0
    j = 0
    _secNo = len(section)
    for _sec in section.values():
        j += 1
        i += 1
        _type = _sec.shape
        if _sec.shape == "GENERAL_BEAM":
            _type = "GENBEAM"

        FEMufo.append("  {:4.0f} {:7s} {:28s}".
                      format(_sec.number, _type, _sec.name[:28]))

        if j == 2 or i == _secNo:
            FEMufo.append("\n")
            j = 0
    #
    return FEMufo


#
def materials(material, factors):
    """
    """
    header = 'LIST MATERIAL PROPERTIES'
    FEMufo = print_head_line(header)
    FEMufo.append("'{:} Original Database\n".format(65 * "-"))
    FEMufo.append("' \n")
    FEMufo.append("' Mat_ID    Type  E-modulus  Poisson      Yield    Density  Thermal-X   Name\n")
    FEMufo.append("' \n")
    #
    # units
    _stress = factors[4] / factors[0] ** 2
    _density = factors[1] / factors[0] ** 3
    # need to fix temperature    
    #
    for _mat in material.values():
        _type = 'Plastic'
        if _mat.Fy == 0:
            _type = 'Elastic'
        
        try:
            FEMufo.append("  {:6.0f} {:7s} {:1.4e} {:1.6f} {:1.4e} {:1.4e} {:1.4e} ! {:}\n"
                          .format(_mat.number, _type, _mat.E * _stress,
                                  _mat.poisson, _mat.Fy * _stress,
                                  _mat.density * _density, _mat.alpha,
                                  _mat.name))
        except AttributeError:
            pass
    #
    return FEMufo


#
def sets(sets):
    """
    """
    header = 'SET LIST'
    FEMufo = print_head_line(header)
    FEMufo.append("'{:} Original Database\n".format(65 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'    ID Name\n")
    FEMufo.append("'\n")
    #
    i = 0
    j = 0
    _setNo = len(sets)
    for _set in sets.values():

        j += 1
        i += 1

        # if j == 1:
        #    FEMufo.append("'")        

        FEMufo.append("  {:5.0f} {:35s}".
                      format(_set.number, _set.name[:35]))

        if j == 2 or i == _setNo:
            FEMufo.append("\n")
            j = 0
    #
    return FEMufo
    #
#
#
def print_model_info(model, factors):
    """
    """
    # printing records of conversion process
    _model_checkout = []
    _model_checkout.extend(sections(model.component.sections))
    _model_checkout.extend(materials(model.component.materials, factors))
    _model_checkout.extend(sets(model.component.sets))
    _model_checkout.extend(load(model.load.functional))

    return _model_checkout
    #
#
def directory():
    import os

    # --------------------------------------------------------------------
    #                    Show Files in current Directory
    # --------------------------------------------------------------------
    #
    print('----------------------------------------------------')
    print('                Current Directory')
    print(os.getcwd())
    print(' ')
    #
    print('----------------------------------------------------')
    print('               Files on directory')
    # print os.listdir(os.getcwd())
    directory = "."
    files = [v for v in os.listdir(directory) if os.path.splitext(v)[1] == ".FEM"]
    files2 = [v for v in os.listdir(directory) if os.path.splitext(v)[1] == ".fem"]
    print("List of fileNames:")
    print(files)
    print(files2)
    #    
#