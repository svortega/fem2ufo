# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import sys
from os.path import abspath, exists
from collections import defaultdict
import csv
#

# package imports
import fem2ufo.f2u_model.feclass.sectprop as SecProp


def paths():
    print ("__file__: %s" % __file__)
    print ("sys.argv: %s" % sys.argv[0])

    a_f = abspath(__file__)
    a_s = abspath(sys.argv[0])

    print ("abs __file__: %s" % a_f)
    print ("abs sys.argv: %s" % a_s)

#
def read_csv(file_name):
    """
    """
    data = defaultdict(list)
    #
    #paths()
    #
    #print( os.path.dirname(os.path.abspath(sys.argv[0])))
    #
    #f_path = abspath(file_name)
    f_path = abspath(__file__)
    f_path = f_path.replace('library.py', file_name)
    #
    #
    if exists(f_path):
        wt_data = []
        with open(f_path, newline='') as f:
            reader = csv.reader(f)
            for row in reader:
                wt_data.append(row)
        #
        return wt_data
    else:
        raise IOError('   ** error file : {:} not found'.format(file_name))
        #sys.exit()
    #
#
# TODO : find permanent fix for this
def get_section_library(name, _property, file_name):
    """
    """
    if file_name:
        file_name = [file_name + '.csv']
    else:
        file_name = ['aisc.csv', 'aiscnew.csv','euro.csv', 'uk.csv']
    
    for _name_file in file_name:
        database = read_csv(_name_file)
        for i in range(len(database)):
            if name in database[i][0]:
                _shape = database[i][1]
                _property[name] = SecProp.SectionProperty(_shape, name)
                _shape = _property[name].shape
                # i beam
                if 'i section' in _shape:
                    # convert from mm to meter
                    _property[name].input_data(float(database[i][2]) / 1000.0,
                                               float(database[i][3]) / 1000.0,
                                               float(database[i][4]) / 1000.0, 
                                               float(database[i][5]) / 1000.0,                                       
                                               float(database[i][4]) / 1000.0, 
                                               float(database[i][5]) / 1000.0)
                elif 'tubular' in _shape:
                    _property[name].input_data(float(database[i][9]) / 1000.0,
                                               float(database[i][10]) / 1000.0)
                
                elif 'channel' in _shape:
                    # convert from mm to meter
                    _property[name].input_data(float(database[i][2]) / 1000.0,
                                               float(database[i][3]) / 1000.0,
                                               float(database[i][4]) / 1000.0, 
                                               float(database[i][5]) / 1000.0)
                # angle
                elif 'angle' in _shape:
                    # convert from mm to meter
                    _property[name].input_data(float(database[i][4]) / 1000.0,
                                               float(database[i][2]) / 1000.0,
                                               float(database[i][5]) / 1000.0, 
                                               float(database[i][3]) / 1000.0)            
                elif 'box' in _shape:
                    # convert from mm to meter
                    _property[name].input_data(float(database[i][4]) / 1000.0,
                                               float(database[i][5]) / 1000.0,
                                               float(database[i][2]) / 1000.0, 
                                               float(database[i][5]) / 1000.0)
                
                else:
                    _property[name].input_data(float(database[i][2]) / 1000.0,
                                               float(database[i][3]) / 1000.0,
                                               float(database[i][4]) / 1000.0, 
                                               float(database[i][5]) / 1000.0)
                #
                print('    * section {:} found in library {:}'.format(name, _name_file))
                #
                return _shape
    #
    raise IOError('   ** error section {:} not found in section library {:}' .format(name, file_name))
    #sys.exit()
#
#