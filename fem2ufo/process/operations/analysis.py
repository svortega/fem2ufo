#
# Copyright (c) 2009-2015 fem2ufo
# 



# Python stdlib imports
import re
import sys

# package imports
#import fem2ufo.process.file.inout as inout


#
# --------------------------------------------------------------------
#                       Seismic Module
# --------------------------------------------------------------------
#
#
# --------------------------------------------------------------------
#                       Ship Impact Module
# --------------------------------------------------------------------
#
def collisionEnergy(ms, vs, mi=0, vi=0, J=0, z=0):
    """
    DNV RP C204
    
    input:
    ms : Ship mass
    vs : Impact speed
    -----------------
    mi : mass of instalation
    vi : velocity of instalation
    ----------------------------
    J  : mass moment of inertia of installation
    z  : distance from pivot point to point contact
    
    output:
    Es
    """
    # Fixed installations
    if mi == 0:
        Es = 0.50 * ms * vs ** 2

    else:
        # Compliant installations
        if J == 0:
            Es = 0.50 * ms * vs ** 2 * ((1 - vi / vs) ** 2 / (1 + ms / mi))
        # Articulated columns
        else:
            Es = 0.50 * ms * ((1 - vi / vs) ** 2 / (1 + ms * z ** 2 / J))

    return Es


#
#
def shipDeformationCurve(impactForce, D):
    """
    DNV RP C204
    
    input:
    impactForce : broadside/stern
    D : member diameter
    
    output:
    p[0] : Maximum identation
    p[1] : Collision load at zero identation
    p[2] : Collision load at 50% identation
    p[3] : Collision load at max identation
    """
    # broadside
    pb15 = [(3, 1.5), (0, 8), (1, 27), (1, 11), (3, 11)]
    pb10 = [(3, 10), (0, 8), (1.5, 45), (1.5, 35), (3, 35)]
    # stern
    ps15 = [(3.0, 1.5), (0, 6), (1, 15), (1, 9), (3, 9)]
    ps10 = [(3.0, 10), (0, 8), (1.1, 31), (1.1, 29), (3, 29)]
    #       
    #
    if impactForce == 'brace':
        pD = interpolateCurve(D, ps15, ps10)

    else:
        pD = interpolateCurve(D, pb15, pb10)

    return pD[0][0], pD[1][1], pD[2][1], pD[4][1]
    #


#
#
def interpolateCurve(D, pb15, pb25):
    diff = [pb25[0][1] - pb15[0][1] + 1]
    m = [0]
    for x in range(1, len(pb15)):
        try:
            diff.append((pb25[x][1] - pb15[x][1]) / diff[0])
        except ZeroDivisionError:
            diff.append(0)
        #
        try:
            m.append((pb25[x][1] - pb15[x][1]) / (pb25[x][0] - pb15[x][0]))
        except ZeroDivisionError:
            m.append(0)

    pD = [(3, D)]
    for x in range(1, len(diff)):
        if D <= pb15[0][1]:
            pD.append([pb15[x][0], pb15[x][1]])

        elif D >= pb25[0][1]:
            pD.append([pb25[x][0], pb25[x][1]])

        else:
            _y = pb15[x][1] + D * diff[x]
            try:
                _x = (D * diff[x] / m[x]) + pb15[x][0]
            except ZeroDivisionError:
                _x = 0
            pD.append([_x, _y])
    #
    pD[x][0] = D

    return pD


#
#
# --------------------------------------------------------------------
#                       Seismic Module
# --------------------------------------------------------------------
#
def read_time_history(timehistory, time_shift=1):
    """
    Read time history data
    """
    #
    _th_no = len(timehistory)
    #
    _step = []
    _time = []
    _time_history = {}
    _th_history = []
    for _no in range(_th_no):
        _file = timehistory[_no][0]
        _factor = float(timehistory[_no][1])
        if _file in _th_history:
            continue

        _th_history.append(_file)
        #
        _file_th = inout.check_input_file(_file)
        if not _file_th:
            print('   *** error file {:} not found'.format(file_name))
            print("      try again", sys.exit())            
        
        _th_data = inout.read_raw_data(_file_th, _factor)
        # check data
        sr, dt, dur = inout.check_th_data(_th_data[0], _th_data[1])
        print('')

        _th3 = [[] for row in range(len(_th_data))]

        for x in range(int(sr) + int(time_shift)):
            _th3[0].append(dt * x)
            for x in range(1, len(_th_data)):
                _th3[x].append(0)

        for x in range(len(_th_data[0])):
            _th3[0].append(_th_data[0][x] + int(time_shift))
            for y in range(1, len(_th_data)):
                _th3[y].append(_th_data[y][x])
        #
        _timemax = max(_th3[0])
        _step.append(dt)
        _time.append(_timemax)
        #
        _time_history[_file_th] = _th3

    return _time_history, _step, _time


#
#

def find_analysis_case(word_in):
    _key = {"static": r"\b(stati(c(o)?|que|s(k|che)))\b",
            "dynamic": r"\b(d(y|i)nami(c(o)?|que|s(k|che)))\b",
            "seismic": r"\b(s(e)?ismi(c(o)?|que|kk|sch)|erthquake)\b",
            "ship_impact": r"\b((ship|vessel)(\_|\-)?impact|skip(\_|\-)?innvirkning|impact(\_|\-)?navire)\b",
            "natural_frequency": r"\b(natural(\_|\-)?frequency|eigenvalue|e(i)?genfre(kvens|quenz)|frequenc(e|ia)(\_|\-)?natur(elle|al))\b"}

    _match = match_line(word_in, _key)

    if not _match:
        print('  **  erorr analysis case {:} not recognized'.format(word_in))
        print('      process terminated')
        sys.exit()

    return _match


#
def match_line(word_in, key):
    """
    search key word at the begining of the string
    """
    word_out = False

    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.match(word_in)

        if keys:
            word_out = _key

    return word_out
    #
