#
# Copyright (c) 2009-2017 fem2ufo
# 


# Python stdlib imports
#import sys
#import re
import copy

#
#
def get_step_section(_member, _item, _bucket):
    """
    """
    #
    for x, _sect_step in enumerate(_member.section): 
        if x == 0: 
            _member.section[0] = _item
            continue
        for _item_stp in  _bucket[_sect_step.shape].values():
            if _item_stp.equal(_sect_step):
                _member.section[x] = _item_stp
                break
#
# -----------------------------
#
def simplify_sections(sections, elements, _units,
                      rename_sections=True):
    """
    Identify duplicated sections and simplify
    """
    print('    * Simplifying Sections')
    #
    _bucket, _duplicated = simplify_main_sections(sections, elements,
                                                  _units, flag=False)
    #
    for key, _section in sections.items():
        #if 'S1A_grouted' in key:
        #    print('-->')
        for _item_name, _item in _bucket[_section.shape].items():

            if _section.number == _item.number:
                for _member_name in _section.elements:
                    try:
                        _member = elements[_member_name]
                        #  loop all member sections
                        get_step_section(_member, _item, _bucket)
                    except KeyError:
                        continue
            
            elif _section.equal(_item):
                for _member_name in _section.elements:
                    try:
                        _member = elements[_member_name]
                        _item.elements.append(_member_name)
                        #  loop all member sections
                        get_step_section(_member, _item, _bucket)
                    except KeyError:
                        continue
    #     
    # remove duplicated sections
    for _sect_no in _duplicated:
        del sections[_sect_no]
    #
    # rename sections
    if rename_sections:
        for i, _section in enumerate(sections.values()):
            _section.number = i + 1
            _section.get_name(_units[0])
    #
    #print('-->')
    #
#
def simplify_main_sections(sections, elements, _units,
                           flag=True):
    """
    Identify duplicated sections and simplify
    """
    #
    _bucket, _bucket_step = get_buckets(sections)
    #
    _duplicated = filter_bucket(_bucket)
    _duplicated.extend(filter_bucket(_bucket_step))
    _duplicated.extend(find_duplicated(_bucket, _bucket_step))
    #
    for _key, _item in _bucket.items():
        if  'shell' in _key or 'membrane' in _key :
            #print('-->')
            continue
        for _section in _item.values():
            _remove = []
            #  loop all member sections
            for _member_name in _section.elements:
                # get element
                try:
                    _member = elements[_member_name]
                except KeyError:
                    _remove.append(_member_name)
                    continue
                #  loop all member sections
                for x, _sect_step in enumerate(_member.section):
                    if x == 0:
                        #_member.section[0] = _item
                        continue
                    # if more than 1 section then section has steps
                    # loop sections in steps
                    for _shape_step in  _bucket[_section.shape].values():
                        if _sect_step.number == _shape_step.number:
                            continue
                        #
                        elif _shape_step.equal(_sect_step):
                            _member.section[x] = _shape_step
                            break
                    #
            #
            for _member_name in _remove:
                _section.elements.remove(_member_name)
    #
    if flag:
        print('    * Simplifying Pile Sections')
        # remove duplicated sections
        for _sect_no in _duplicated:
            del sections[_sect_no]
        #
        # rename sections
        i = 0
        for _section in sections.values():
            i += 1
            _section.number = i
            _section.get_name(length_unit=_units[0])
    #
    else:
        print('    * Simplifying Main Sections')
        return _bucket, _duplicated
    #print('-->')
    #
#
def get_buckets(sections):
    """
    """
    _bucket = {}
    _bucket_str = {}
    #
    for _sect_name, _section in sections.items():
        # FIXME : this flag : "_" may be weak
        #         change it for something strong
        if "_step_" in _sect_name:
            try:
                _bucket_str[_section.shape].update({_sect_name: _section})
            except KeyError:
                _bucket_str[_section.shape] = {}
                _bucket_str[_section.shape].update({_sect_name: _section})
        #
        else:
            try:
                _bucket[_section.shape].update({_sect_name: _section})
            except KeyError:
                _bucket[_section.shape] = {}
                _bucket[_section.shape].update({_sect_name: _section})
    #
    return _bucket, _bucket_str
#
def filter_bucket(_bucket):
    """
    """
    _duplicated = []
    for _type, _items in _bucket.items():
        _dummy = copy.copy(_items)
        _to_delete = []
        for _sec_id, _section in _items.items():
            try:
                del _dummy[_sec_id]
            except KeyError:
                continue
            #
            for _item_name, _item in _dummy.items():
                if _section.equal(_item):
                    _to_delete.append(_item_name)
        #
        _duplicated.extend(_to_delete)
        for _item_no in _to_delete:
            try:
                del _items[_item_no]
            except KeyError:
                continue
    #
    _duplicated = list(set(_duplicated))
    return _duplicated
#
def find_duplicated(_bucket, _bucket_str):
    """
    """
    _duplicated = []
    for key, _items in _bucket_str.items():
        _to_delete = []
        for _sect_name, _section in _items.items():
            try:
                for _item_name, _item in _bucket[key].items():
                    if _section.equal(_item):
                        _to_delete.append(_sect_name)
            # inser new section type if not found in main bucket
            except KeyError:
                _bucket[key] = {}
        #
        _duplicated.extend(_to_delete)
        for _section in _to_delete:
            del _bucket_str[key][_section]
        #
        _bucket[key].update(_bucket_str[key])
        #
    #
    _duplicated = list(set(_duplicated))
    return _duplicated
#
#