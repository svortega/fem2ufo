#
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import copy

# package imports
#import fem2ufo.process.analysis.signalstat as signal_staistic

#
#
def renumber_soil(soil_material, comp_material):
    """
    Renumber non-linear soil spring's according to fe model material
    """
    print('    * Renumering soil spring data')
    #
    _mat_list = [_mat.number for _mat in comp_material.values()]
    _mat_no = max(_mat_list)    
    #
    _new_no = 0
    mat_soil = soil_material
    for key in sorted(mat_soil, key = lambda name: mat_soil[name].number):
        _spr = mat_soil[key]
        _new_no += 1
        _spr.number = _mat_no + _new_no
        _spr.spring.number = _mat_no + _new_no
    #
    #
#
#
def fill_spring(_spring):
    """
    """
    #for _spring in springs.values():
    f_add = [item * -1 for item in _spring.force[::-1]]
    d_add = [item * -1 for item in _spring.displacement[::-1]]

    _force = f_add[:len(_spring.force)-1]
    _force.extend(_spring.force)

    _disp = d_add[:len(_spring.displacement)-1]
    _disp.extend(_spring.displacement)

    _spring.force = _force
    _spring.displacement = _disp
#
#
#
def process_seismic_soil(_soil, _soil_depth):
    """
    """
    _depthTemp = []
    for _depth in _soil_depth:
        _soil, _temp = process_insert_soil_layer(_soil, _depth)
        _depthTemp.append(_temp)
    #
    return _soil, _depthTemp
#
#
def insert_soil_layer_module(soil, depth_below_mudline):
    """
    """
    #
    for key in sorted(soil.layers, key = lambda name: soil.layers[name].number):
        _layer = soil.layers[key]
        if depth_below_mudline >= abs(_layer.depth):
            _resDepth = (_layer.depth, key)
            _endDepth = (_layer.depth, key)
        else:
            _endDepth = (_layer.depth, key)
            break
    #
    # find suitable layer, no need for inserting new layer
    if _resDepth[0] / depth_below_mudline <= 0.10:
        depth_below_mudline = _resDepth[1]
    #
    elif _endDepth[0] / depth_below_mudline <= 0.10:
        depth_below_mudline = _endDepth[1]
    # insert new layer in soil
    else:
        # print('need to insert new layer in soil')
        soil.layers[depth_below_mudline] = copy.copy(soil.layers[_resDepth[1]])
        soil.layers[depth_below_mudline].name = depth_below_mudline
        soil.layers[depth_below_mudline].depth = depth_below_mudline
        soil.layers[depth_below_mudline].thickness = abs(depth_below_mudline - soil.layers[_resDepth[1]].depth)

        # renumber soil layers
        #for _key1, soil in sorted(soil.items()):
        i = 0
        for key in sorted(soil.layers, key = lambda name: soil.layers[name].depth):
        #for _key2, _layer in sorted(soil.layers.items()):
            _layer = soil.layers[key]
            i += 1
            _layer.number = i
    #
    #return soil, depth_below_mudline
#
#
def insert_soil_layer(_foundation, _depth):
    """
    """
    for key, _soil in _foundation.soil.items():
        #_depth_below_mudline = _depth + _soil.mudline
        insert_soil_layer_module(_soil, _depth)
    #
    #return _foundation, _depth
#