#
# Copyright (c) 2009-2018 fem2ufo
# 


# Python stdlib imports
#import sys
#import re

# package imports
import fem2ufo.process.common.control as common
#import fem2ufo.process.analysis.signalstat as signal_staistic

# --------------------------------------------------------------------
#                            Modify Load
# --------------------------------------------------------------------
#
def remove_load(_list, load):
    """
    Delete loads
    """
    print('    * Deleting loads')
    #
    header = 'DELETING LOADS'
    check_out = common.printing.print_head_line(header)

    _load_on = []
    _load_off = []
    #
    for _item in _list:

        try:
            del load[_item]
            _load_on.append(_item)

        except KeyError:
            _load_off.append(_item)
    #
    if _load_on:
        check_out.append('\n')
        check_out.append("'{:} Loads Deleted\n".format(65 * " "))
        check_out.extend(common.printing.print_column_num(_load_on))

    if _load_off:
        check_out.append('\n')
        check_out.append("'{:} ** Warning Loads not Found\n".format(65 * " "))
        check_out.extend(common.printing.print_column_num(_load_off))

    return check_out
    #


#
#
#
# Mass load convertion
#
def load2mass_node(load_no, loading, node,
                   _dof, _grav, factor):
    """
    Convert nodal loading to node mass
    """
    _node_off = []
    
    for _node_number in loading[load_no].node:
        # find node
        try:
            _node = node[_node_number]
        except KeyError:
            _node_off.append(_node_number)
            continue
        
        # TODO : Fix _dof 
        for x in range(len(_dof)):
            try:
                test = 1.0/_dof[x]
                _node.convert_load_to_mass(load_number = load_no, 
                                           gravity = _grav, 
                                           mass_factor = factor, 
                                           DoF = x,
                                           delete_load = False)
            except ZeroDivisionError:
                continue
    #
    #return loading, node
#
#
# TODO: check if it works
def load2mass_member(load_no, loading, element,
                     _dof, _grav, factor, _option='node'):
    """
    Convert member load to node or element mass
    """
    _elem_off = []
    for _memb_number in loading[load_no].element:
        # check if member exists
        #_elem_name = str(_elem_no)
        try:
            _elem = element[_memb_number]

        except KeyError:
            _elem_off.append(_memb_number)
            continue
        #
        mass_beam_option = False
        if _option != 'node':
            mass_beam_option = True
        #
        _elem.convert_load_to_mass(load_number = load_no, 
                                   DoF = 2,
                                   gravity = _grav, 
                                   mass_factor = factor, 
                                   delete_load = False,
                                   mass_beam = mass_beam_option)
    #
    #
    #return loading, element
    #
#
#
def load2mass(loading, element, node, _dof,
              load_list, _grav_list,
              _option=False, factor=False):
    """
    """
    #
    if not factor:
        factor = [1 for x in load_list]

    if not _option:
        _option = ['node' for x in load_list]
    #
    print('    * Converting load to mass')
    #
    for i in range(len(load_list)):

        _load_no = load_list[i]

        try:
            loading[_load_no]

        except KeyError:
            print('    ** warning load number {:} not found'
                  .format(_load_no))
            continue

        # nodal loading
        load2mass_node(_load_no, loading, node,
                       _dof, _grav_list[i], factor[i])
        #
        # beam loading
        load2mass_member(_load_no, loading, element,
                         _dof, _grav_list[i], factor[i],
                          _option[i])
        #
    #
    #return node, element, loading
    #


#
#
#
def fill_gap_load(load):
    """
    Renumber load cases consecutively from 1 onwards
    """
    print('    * Filling loading gap')
    #
    _new_no = 0
    for _key, _load in sorted(load.items()):
        _new_no += 1
        _load.number = _new_no
    #
    #return load
    #


#
#
# --------------------------------------------------------------------
#                            Modify Load
# --------------------------------------------------------------------
#
def delete_load(loading, _load_no, _group_no):
    """
    Delete loads from the fe model
    """
    #
    _msg = []

    if _group_no:
        print(' error --> group load not yet available')
    #
    if _load_no:
        _msg = remove_load(_load_no,
                           loading.functional)
    #
    return _msg


#
#
def convert_load_to_mass(geometry, loading,
                         _load_tb_converted,
                         dof=False):
    """
    Convert load to mass\n
    
    Input:\n
    geometry : model geometry\n
    loading  : model fuctional loading\n
    _load_tb_converted : [load, factor, option]\n
    _grav : gravity\n
    dof   : gravity's degree of fredom [default vetical z]\n
    """

    # set mass in the vertical z direction
    if not dof:
        dof = [0, 0, 1, 0, 0, 0]

    # check if any load to be converted
    if _load_tb_converted:
        #
        _number, _factor, _option, _grav = _load_tb_converted
        #
        load2mass(loading.functional,
                  geometry.elements,
                  geometry.nodes, dof,
                  _number, _grav,
                  _option, _factor)
    #
    #return geometry, loading


#
#
#
# number
def find_load_number(loading, _fillgap=False):
    """
    Find load number
    """
    if not loading:
        return 0

    if _fillgap:
        _load_no = len(loading)

    else:
        _current_no = []

        for _load in loading.items():
            _current_no.append(_load[0])

        _load_no = max(_current_no)

    return _load_no


#