# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import copy
import inspect
import types
#

# package imports
import fem2ufo.f2u_model.control as f2u
import fem2ufo.process.modules.euclid as euclid
import fem2ufo.process.common.operations.printing as printing
import fem2ufo.process.common.operations.headers as headers

#
def update_offsets(_eccentricity):
    """
    """
    # ------------
    # find offsets
    #
    # get maimum offset number
    _items_offset = [_key for _key in _eccentricity.keys()]
    
    #_ecc_no = [_offset.number for _offset in _eccentricity.values()]
    
    #if _ecc_no:
    #    _new_number = max(_ecc_no)
    #
    #
    #
    for _key in _items_offset:
        #_eccentnumber = _offset.number
        _offset = _eccentricity[_key]
        
        #if _eccentnumber == 100007:
        #    print('here')
        
        #try:
        #    while True:
        #        _new_number += 1000
        #        _ecc_name = str(_new_number)
        #        # this new numnber must not be equal than element number
        #        _section[_ecc_name]
        #
        #except KeyError:
        _new_name = str(_key) + '_1'
        _eccentricity[_new_name] = f2u.Geometry.Eccentricities(_new_name)
        _eccentricity[_new_name].case = _offset.case
        _eccentricity[_new_name].eccentricity = _offset.eccentricity[3:]
        _offset.eccentricity = _offset.eccentricity[:3]
    #
    #print('--->')
    #     
#
#
def get_group_items(number, sets):
    """
    Get list of items from given set's fe number
    """
    _items = []
    for _no in number:
        try:
            _items.extend(sets[int(_no)].items)

        except KeyError:
            print('    ** Warning item No : {:} not found in Set'
                  .format(_no))
            continue
    #
    return _items
#
#
def update_sets_name(groups, renumber=True):
    """
    """
    #
    print('    * Updating Sets')
    #
    if renumber:
        i = 0
        for _set in groups.values():
            i += 1
            _set.get_name(i)
    else:
        for _set in groups.values():
            _set.get_name()
#
#
# --------------------------------------------------------------------
#                      Element Section
# --------------------------------------------------------------------
#
def delete_elements(geometry, _member_no, _group_no):
    """
    delete members from fe model
    """
    _check_out = []

    if _group_no:
        _msg = delete_group_elements(_group_no,
                                     geometry.sets,
                                     geometry.elements,
                                     geometry.nodes)
        _check_out.extend(_msg)
    #
    if _member_no:
        # delete members
        _nodes_deleted, _msg = delete_element_list(_member_no,
                                                   geometry.elements)
        _check_out.extend(_msg)

        # delete nodes
        _msg = delete_element_node(_nodes_deleted, 
                                   geometry.nodes,
                                   geometry.elements)
        _check_out.extend(_msg)

    return _check_out


#
#
# TODO : need to verify this delete node's group works
def delete_nodes(geometry, _node_no, _group_no):
    """
    Delte nodes from fe model\n
    Input:\n
    geometry  : fe model geometry\n
    _node_no  : list of nodes fe number to be deleted\n
    _group_no : list of node's sets fe number to be deleted
    """
    _check_out = []

    if _group_no:
        # 
        for _node_set in _group_no:
            _node_set2 = [_node_set]

            if geometry.sets[_node_set].type == 1:
                _node_no.extend(get_group_items(_node_set2, geometry.sets))

            else:
                print('    ** Warning --> Group {:} <> Node'
                      .format(geometry.sets[_node_set].name))

    #
    if _node_no:
        _msg = delete_node_list(_node_no,
                                       geometry.nodes,
                                       geometry.elements)
        _check_out.extend(_msg)

    return _check_out


#
#
# elements
#
def delete_element_list(member_list, elements):
    """
    Delete elements from fe model\n
    
    Input:\n
    member_list : List of elements to be deleted\n
    elements    : Elements from fe model
    """
    #
    print('    * Deleting elements')
    #
    check_out = []
    check_out.append("'{:} Elements Deleted\n".format(65 * " "))
    check_out.append('\n')
    #
    _nodes = []
    _list = []
    for _memb in member_list:
        # colect member nodes
        try:
            for _nod in elements[_memb].node:
                _nodes.append(_nod.number)
                
            # delete elements
            _list.append(_memb)
            del elements[_memb]

        except KeyError:
            continue

    # print deleted members
    check_out.extend(printing.print_column_num(_list))

    # remove duplicated nodes
    _nodes = list(set(_nodes))
    #
    return _nodes, check_out


#
def delete_element_node(node_list, nodes, elements):
    """
    Delete nodes in the fe model\n
    
    Input:\n
    nodel_list : list of nodes to be deleted
    nodes      : nodes from fe model
    element    : elements from fe model
    """
    #
    print('    * Deleting nodes')
    #
    check_out = []
    _list = []
    _shim_out = []
    #
    for _node in node_list:
        _flag = False

        for _memb in nodes[_node].elements:
            #_memb_name = str(_memb)
            try:
                _elem = elements[_memb]
                _flag = True
                # Assume shim element is atached to the node
                if _elem.material[0].type == "shim":
                    _shim_out.append(_memb)
                    del elements[_memb]

                break

            except KeyError:
                continue

        if _flag == False:
            _list.append(nodes[_node].number)
            del nodes[_node]

    # print deleted nodes
    check_out.append("'{:} Nodes Deleted\n".format(68 * " "))
    check_out.append('\n')
    check_out.extend(printing.print_column_num(_list))

    # print if shim element deleted
    if _shim_out:
        check_out.append("'{:} Shim Element Deleted\n".format(68 * " "))
        check_out.append('\n')
        check_out.extend(printing.print_column_num(_shim_out))

    return check_out


#
def delete_group_elements(_set_number, sets, elements, nodes):
    """
    Delete group of elements from fe model\n
    Input:\n
    _set_number : list of set's fe number to be deleted\n
    sets        : fe model sets\n
    elements    : fe model elements\n
    nodes       : fe model nodes\n
    """
    #
    print('    * Deleting group of elements')

    header = 'DELETING ELEMENTS SETS'
    check_out = headers.print_head_line(header, subfix="")

    # check_out = []
    # get items from froup
    _memb = get_group_items(_set_number, sets)

    # delete members
    _nodes_delete, _msg = delete_element_list(_memb, elements)
    check_out.extend(_msg)
    # delete nodes
    _msg = delete_element_node(_nodes_delete, nodes, elements)
    check_out.extend(_msg)

    # deleting empty set in sets
    _list_on = []
    _list_off = []
    for _set in _set_number:
        try:
            del sets[int(_set)]
            _list_on.append(_set)
        except KeyError:
            _list_off.append(_set)
            continue
    #

    # print deleted sets
    if _list_on:
        check_out.append('\n')
        check_out.append("'{:} Sets Deleted\n".format(69 * " "))
        check_out.extend(printing.print_column_num(_list_on))

    if _list_off:
        check_out.append('\n')
        check_out.append('{:} ** Warning Sets Not Found\n'.format(65 * " "))
        check_out.extend(printing.print_column_num(_list_off))

    return check_out


#
# nodes
#
def delete_node_list(node_list, nodes, elements):
    """
    """
    print('    * Deleting nodes')
    #
    header = 'DELETING NODES'
    check_out = headers.print_head_line(header)

    _list_nodes_on = []
    _list_nodes_off = []
    _list_members = []

    for _node in node_list:
        # find if node exist
        try:
            # deleting members sharing the node
            for _memb in nodes[_node].elements:
                # find if member exist
                try:
                    _list_members.append(elements[_memb].number)
                    del elements[_memb]

                except KeyError:
                    continue
            # deleting node
            _list_nodes_on.append(nodes[_node].number)
            del nodes[_node]

        except KeyError:
            _list_nodes_off.append(_node)
            continue
    #
    if _list_nodes_on:
        check_out.append('\n')
        check_out.append("'{:} Nodes Deleted\n".format(68 * " "))
        check_out.extend(printing.print_column_num(_list_nodes_on))

        if _list_members:
            check_out.append('\n')
            check_out.append("'{:} Elements Deleted\n".format(65 * " "))
            check_out.extend(printing.print_column_num(_list_members))

    if _list_nodes_off:
        check_out.append('\n')
        check_out.append("'{:} ** Warning Nodes not Found\n".format(60 * " "))
        check_out.extend(headers.print_column_num(_list_nodes_off))

    return check_out


#
#
# --------------------------------------------------------------------
#                          Tidying up model
# --------------------------------------------------------------------
#
def clean_group(sets, elements):
    """
    Cleaning model's sets by removing empty sets
    """
    print('    * Tidying up groups')

    for _set in sets.values():
        _new_item = []
        for _memb in _set.items:
            try:
                _elem = elements[_memb]
                _new_item.append(_memb)
            except KeyError:
                _new_item.append(_memb)
        _set.items = _new_item
    #
    #return sets
    #
#
#
#
# ------ svo to clean this
# FIXME : this def must ensure reliability
def split_sets(group, elements, _name_id, opt=''):
    """
    Form sub-groups based on elements following the shared end nodes
    """
    #
    _members = {i: {elements[i].node[0].name, 
                    elements[i].node[1].name}
                for i in group}
    #
    #_members = {}
    #for i in group:
    #    for key, value, in elements[i].node[0].items():
    #        _members[key]
    #
    _group = {}
    _node_out = {}
    i = 0
    _memb2 = group[0]
    _item = {_memb2}
    _nodes = _members[_memb2]
    _len = len(_members)
    #
    # this infinite loop is dangerous need check
    while _len:
        # check all members in the group
        _nod_group = []
        for _loop in range(_len):
            for k2, v2 in sorted(_members.items()):
                _nod_group.extend(list(v2))

                if _memb2 == k2:
                    continue
                # check if member share node
                if _nodes.intersection(v2):
                    _item.add(k2)
                    # nodes in a or b but not both
                    _nodes = _nodes ^ v2
            #
            # remove members found
            for _memb_number in _item:
                try:
                    del _members[_memb_number]
                except KeyError:
                    continue

            # calculate new length
            _len = len(_members)
        # 
        # create new group
        i += 1
        _name = str(_name_id) + '_' + str(i)
        #
        # add member to group
        try:
            _group[_name].append(list(_item))
        except KeyError:
            _group[_name] = list(_item)
        #
        # add node to group
        try:
            _node_out[_name].append(list(_nodes))
        except KeyError:
            _node_out[_name] = list(_nodes)
            #
        # re-set remaining nodes
        try:
            _memb2, _nodes = next(iter(_members.items()))
            _item = {_memb2}
        except StopIteration:
            break
    #
    #
    if opt == 'group':
        return _group
    #
    elif opt == 'node':
        return _node_out
    #
    else:
        return _group, _node_out
#
#
def add_list_to_group(group_type, group, newgroup):
    """
    """
    if group_type == 1:
        group.nodes = newgroup
    
    if group_type == 3:
        group.concepts = newgroup
    
    else:
        group.items = newgroup
#
#
def add_group(oldgroup, newgroup, name, 
              number=None, group_type=2):
    """
    function adds new groups
    """
    if number:
        _set_no = number
    else:
        _set_no = len(oldgroup)
        _set_no += 1
    
    _set_name = name
    try:
        oldgroup[_set_name]
        _set_name = name + '_' + str(_set_no)
    except KeyError:
        pass
    #
    # new group
    oldgroup[_set_name] = f2u.Geometry.Groups(_set_name, _set_no)
    #
    if type(newgroup) is list:
        group = oldgroup[_set_name]
        add_list_to_group(group_type, group, newgroup)
    #
    elif type(newgroup) is dict:
        print('fix dictionary type in add group')
    #
    elif type(newgroup) is str:
        #
        group = oldgroup[_set_name]
        add_list_to_group(group_type, group, [newgroup])        
    #
    #
    else:
        try:
            oldgroup[_set_name].nodes = newgroup.nodes
        except AttributeError:
            pass
        #
        try:
            oldgroup[_set_name].concepts = newgroup.concepts
        except AttributeError:
            pass
        #
        try:
            oldgroup[_set_name].items = newgroup.items
        except AttributeError:
            pass
            #print('error check what type is set name')
    #
    return _set_name, _set_no
#
#
def add_new_groups(old_group, new_group,
                   name='_new_set'):
    """
    Add new set to existing group\n
    
    Input:\n
    old_group : existing group\n
    new_group : new group to be added to existing group\n
    name      : name new group [default : new_set]\n
    """

    _set_no = len(old_group)

    for _set in new_group.values():
        _set_no += 1
        _set_name = name + '_' + str(_set.number)
        #
        newset_name, newset_no = add_group(old_group, _set, 
                                           _set_name, _set_no)
        
        #
        # update new group
        try:
            _set.sets[newset_name] = newset_no
            #_set.sets[newset_name].name = newset_name
        except AttributeError:
            _set.number = newset_no
            _set.name = newset_name
#
#
# --------------------------------------------------------------------
#                             Modify joint
# --------------------------------------------------------------------
# 
def add_new_joint():
    """
    Add new joint to the fe model
    """
    _Jnt_dummy[_no] = modelfe.Joints(_no, _no,
                                     _geometry.nodes[_items[0]])

    _branch = {}
    for _memb in _items[1]:
        # print('ok')
        _node_1 = _geometry.elements[_memb].node[0]
        _node_2 = _geometry.elements[_memb].node[1]
        _branch[_memb] = [_node_1, _node_2]

    _atol = 0
    _chord, avec, _x_element, _overlapping, _msg = chord(_Jnt_dummy[_no],
                                                         _geometry.elements,
                                                         _branch,
                                                         _atol)
# 
#
#
# --------------------------------------------------------------------
#                            find D/t ratio
# --------------------------------------------------------------------
#
def find_compactness(geometry):
    """
    Find & create sets with tubular elements with D/t > 60 & 80\n
    
    input: \n
    geometry
    """
    print('    * Processing members with D/t > 60 & 80')
    #
    geometry.sets, _msg = dt_ratio(geometry.sections,
                                          geometry.elements,
                                          geometry.sets)
    return geometry, _msg
#
#
#
# --------------------------------------------------------------------
#                          Tidying up model
# --------------------------------------------------------------------
#
#
def clean_model(model):
    """
    Clean fe model from deleted elements, sets, etc.\n
    
    Input:\n
    model : fe model\n
    """
    #
    print('    * Cleaning model')
    #
    # Tidying up groups after deleting stuff
    clean_group(model.component.sets,
                model.component.elements)
    # 
    #
    # renumber soil spring data (material number)
    try:
        if model.foundation:
            pass
            #model['soil'].materials = renumber_soil(model['soil'].materials,
            #                                               model['geometry'].materials)
    except AttributeError:
        model.foundation = {}
    #
    try:
        if model.metocean:
            pass
    except AttributeError:
        model.metocean = {}
    #
    return model
#
# --------------------------------------------------------------------
#                            find D/t ratio
# --------------------------------------------------------------------
#
def dt_ratio(section, element, sets):
    """
    Create sets with tubular elements with D/t > 60 & 80\n
    
    section : 
    element :
    sets    :
    
    """
    #
    header = 'D/T SECTION'
    check_out = headers.print_head_line(header)
    #
    _dt60 = []
    _dt80 = []
    _noncompact = []
    _slender = []
    for _sec in section.values():
        if _sec.shape == 'PIPE':
            _dt = _sec.properties.D / _sec.properties.Tw
            if _dt > 80:
                _dt80.append(_sec.number)

            elif _dt > 60:
                _dt60.append(_sec.number)
                # else:
                #    print('ok')
    #
    _set_no = len(sets)
    # creating set with sections d/t > 60
    if _dt60:
        _set_no += 1
        _name = 'Dt_60'
        _nlnam = 1
        _ncnam = len(_name)
        _codnam = _nlnam * 100 + _ncnam
        _codtxt = 0
        #
        sets[_set_no] = modelfe.Groups(_set_no)
        sets[_set_no].codnam = _codnam
        sets[_set_no].codtxt = _codtxt
        sets[_set_no].name = _name
        sets[_set_no].type = 2
        sets[_set_no].isorig = 0
        _number60 = _set_no

        check_out.append('\n')
        check_out.append('{:} Sections with D/t > 60\n'.format(60 * " "))
        check_out.extend(headers.print_column_num(_dt60))
    # creating set with sections d/t > 80
    if _dt80:
        _set_no += 1
        _name = 'Dt_80'
        _nlnam = 1
        _ncnam = len(_name)
        _codnam = _nlnam * 100 + _ncnam
        _codtxt = 0
        #
        sets[_set_no] = modelfe.Groups(_set_no)
        sets[_set_no].codnam = _codnam
        sets[_set_no].codtxt = _codtxt
        sets[_set_no].name = _name
        sets[_set_no].type = 2
        sets[_set_no].isorig = 0
        _number80 = _set_no

        check_out.append('\n')
        check_out.append('{:} Sections with D/t > 80\n'.format(60 * " "))
        check_out.extend(headers.print_column_num(_dt80))

    # adding members to the sets
    _member_off = []
    for _memb in element.values():
        try:
            if _memb.section[0].number in _dt60:
                sets[_number60].items.append(_memb.number)
            elif _memb.section[0].number in _dt80:
                sets[_number80].items.append(_memb.number)
        except IndexError:
            _member_off.append(_memb.number)
    #
    if _member_off:
        check_out.append('\n')
        check_out.append('{:} ** Warning Elements with no Section\n'.
                         format(47 * " "))
        check_out.extend(headers.print_column_num(_member_off))

    return sets, check_out
    #
#
#
# --------------------------------------------------------------------
#                     find super node
# --------------------------------------------------------------------
# TODO: need to be checked
def find_super_node_New(geometry, piles, _tol):
    """
    Find supernodes 
    """
    # boundary, node,
    print('    * Finding link nodes')
    #
    # remove redundant boundaries 
    #
    #
    _bound_node = {}
    for key, _node in geometry.nodes.items():
        try:
            if _node.boundary.type == 'link':
                _bound_node[key] = _node.boundary
        except AttributeError:
            continue
    #
    #
    for _name in sorted(piles.concepts, key=lambda name: piles.concepts[name].number):
        _concept = piles.concepts[_name]
        _concept.get_end_nodes()
        _end_nodes = [_concept.node[0], _concept.node[-1]]
        
        for _node_pile in _end_nodes:
            #_node_number = _item.nodes[0]
            _xcoord, _ycoord, _zcoord = _node_pile.get_coordinates()
            # create a point in space
            _point = euclid.Point3(_xcoord, _ycoord, _zcoord)
            
            for key, boundary in _bound_node.items():
                _node = geometry.nodes[key]
                _x1coord, _y1coord, _z1coord = _node.get_coordinates()
                # set tolerance of node
                _sphere = euclid.Sphere(euclid.Point3(_x1coord,
                                                      _y1coord,
                                                      _z1coord), _tol)
                
                # check if node within the boundary
                if _sphere.intersect(_point):
                    _flag = True
                    print('    ** link node found {:6.0f} --> {:}'
                          .format(_node_pile.number, _node.number))
                    
                    _new_name = _node_pile.number
                    _node_pile.number = _node.number
                    
                    #piles.sets[_name] = f2u.Geometry.Groups(_name, _setnumber2)
                    #piles.sets[_name].items.append(piles.elements[_memb].number)
                    
                    boundary.nodes.remove(key)
                    _node.boundary = {}
                    # delete boundary if exist
                    try:
                        del geometry.boundaries[key]
                    except KeyError:
                        pass
                    #
                    # add pile to overlap items
                    try:
                        _joint = geometry.joints[_node.name]
                        _joint.overlap.append(_name)
                    #except AttributeError:
                        #_new_name = 'p2p_' + _node.name
                        #try:
                        #    _joint = geometry.joints[_new_name]
                        #    _joint.overlap.append(_name)
                        #except AttributeError:
                        #pass
                    except:
                        #_joint = geometry.joints[_new_name]
                        #print('-->')
                        pass
                    #
                    _flag = False
                    break
            #
            if not _flag:
                break
        #
        if _flag:
            print('error --> link node node {:} not found'.format(_node_number))
        #
    #    
    #
    #return model, pile
    #


#
# TODO: need to be checked
def find_super_node(geometry, piles, _tol):
    """
    Find supernodes 
    """
    # boundary, node,
    print('    * Finding link nodes')
    #
    # remove redundant boundaries 
    #
    #_bound_node = {key: _node.boundary for key, _node in geometry.nodes.items() 
    #               if _node.boundary.type == 'supernode'}
    #
    _bound_node = {}
    for key, _node in geometry.nodes.items():
        try:
            if _node.boundary.type == 'link':
                _bound_node[key] = _node.boundary
        except AttributeError:
            continue
    #
    # for key in sorted(concept, key=lambda name: concept[name].number)
    #for _name, _item in sorted(piles.sets.items()):
    for _name in sorted(piles.sets, key=lambda name: piles.sets[name].number):
        _item = piles.sets[_name]
        
        if 'end_nodes' in _item.name:
            _node_number = _item.nodes[0]
            _xcoord, _ycoord, _zcoord = piles.nodes[_node_number].get_coordinates()
            # create a point in space
            _point = euclid.Point3(_xcoord, _ycoord, _zcoord)

            for key, boundary in _bound_node.items():
                _node = geometry.nodes[key]
                _x1coord, _y1coord, _z1coord = _node.get_coordinates()
                # set tolerance of node
                _sphere = euclid.Sphere(euclid.Point3(_x1coord,
                                                      _y1coord,
                                                      _z1coord),
                                        _tol)
                
                # check if node within the boundary
                if _sphere.intersect(_point):
                    _setno = _item.name.replace('end_nodes__', '')
                    _memb = piles.sets[_setno].items[0]
                    #
                    _flag = True
                    for _no in range(2):
                        # pile.elements[_memb].node[x].number = 0
                        _node_pile_number = piles.elements[_memb].node[_no].number

                        if _node_number == _node_pile_number:
                            # print(' memb found {:}'.format(pile.elements[_memb].name))
                            print('    * Pile head node found {:6.0f} --> {:}'
                                  .format(_node_number, _node.number))
                            piles.elements[_memb].node[_no].number = _node.number
                            piles.sets[_name].items.append(piles.elements[_memb].number)
                            
                            boundary.nodes.remove(key)
                            _node.boundary = {}
                            _flag = False

                            # try:
                            #del geometry.boundaries[_node.number]
                            # except KeyError:
                            #    pass

                            break
                    #
                    if _flag:
                        print('    *** error --> Pile head node {:} not found'.format(_node_number))
    #
    #    
    #
    #return model, pile
    #
#
#
#
#
def select_items(files):
    """
    """
    _number = []
    _name = []
    for _item in files:
        if type(_item) is list:
            for _memb in _item:
                _str, _int = get_items(_memb)
                _name.extend(_str)
                _number.extend(_int)
        else:
            _str, _int = get_items(_item)
            _name.extend(_str)
            _number.extend(_int)
    #
    return _name, _number
#
#
def get_items(_item):
    """
    """
    _str = []
    _int = []

    if type(_item) is str:
        _str.append(_item)

    elif type(_item) is int:
        _int.append(_item)

    else:
        print('   ** warining item {:} not recognized'
              .format(_item))

    return _str, _int
#
#
#
# -----------------------------
#
def simplify_hinges(_hinge, _element):
    """
    Identify duplicated hinges and simplify it
    """
    #
    print('    * Simplifying Hinges')
    #
    _dummy = copy.copy(_hinge)
    _to_delete = []
    for _rel_name, _rel in _hinge.items():
        try:
            del _dummy[_rel_name]
        except KeyError:
            continue
        
        for _item_name, _item in _dummy.items():
            if _rel.fix == _item.fix:
                memb_name = _item.name.split('_')
                _node_end = int(memb_name[1])
                memb_name = str(int(memb_name[0]))
                _memb = _element[memb_name]
                _memb.releases[_node_end] = _rel
                _to_delete.append(_item.name)
        #
        for _remove in _to_delete:
            try:
                del _dummy[_remove]
            except KeyError:
                continue
    #
    for _remove in _to_delete:
        del _hinge[_remove]
    #
    i = 0
    for _rel in _hinge.values():
        i += 1
        _rel.get_name(i)
    #
    #print(' -->')
#
#




