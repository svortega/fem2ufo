#
# Copyright (c) 2009-2015 fem2ufo
#

# Python stdlib imports
import math

# package imports

#
def compactness(_section, material):
    '''
    Chapter B
    This chapter addresses general requirements for the analysis
    and design of steel structures applicable to all chapters of
    the specification.
    
    This chapter is organized as follows:
    B4. Member Properties
    '''
    # global SectionType, Build, SecComp

    # B4.3 Gross and Net Area Determination
    # B4.3a Gross Area
    # The gross area, Ag, of a member is the total cross-sectional area.
    _section.Ag = _section.Area

    # B4.3b Net Area
    # For members without holes, the net area, An, is equal to the
    # gross area, Ag.
    _section.An = _section.Ag

    #    ----- Table B4.1a &  Table B4.1b -----

    class_B41a = table_B41a(_section, material)
    # print('class Table B4.1a :', class_B41a)

    class_B41b_flange, class_B41b_web = table_B41b(_section, material)
    # print('class Table B4.1b  flange:', class_B41b_flange,' web:',class_B41b_web)

    if 'slender' in [class_B41b_flange, class_B41b_web]:
        class_B41b = 'slender'

    elif 'noncompact' in [class_B41b_flange, class_B41b_web]:
        class_B41b = 'noncompact'

    else:
        class_B41b = 'compact'

    # 
    return class_B41a, class_B41b
    #


#
def table_B41a(_section, material):
    '''
    TABLE B4.1a
    Width-to-Thickness Ratios: Compression Elements
    Members Subject to Axial Compression
    '''
    _Fy = material.Fy
    if material.Fy == 0:
        _Fy = material.E

    # h : Clear distance between flanges less the fillet or corner 
    #     radius for rolled shapes; distance between adjacent lines 
    #     of fasteners or the clear distance between flanges when 
    #     welds are used for built-up shapes.        
    hw = _section.D - _section.Tft - _section.Tfb

    # CASE 6 & 8 <-------------
    if 'box' in str(_section.SectionType).lower():
        # Case 6
        _ratio = hw / _section.Tw
        _lambda_r = 1.40 * math.sqrt(material.E / _Fy)
        _case = '6'
        _case_flag = 'Stiffened'

        # Case 8
        _ratio_flange = _section.Bft / _section.Tft
        _lambda_r_flange = 1.49 * math.sqrt(material.E / _Fy)
        _case_flange = '8'

        if _ratio_flange > _lambda_r_flange:
            _ratio = _ratio_flange
            _lambda_r = _lambda_r_flange
            _case = _case_flange

        _case_ratio = 'b/t'

    # CASE 1 & 4 <------------
    elif 'tee' in str(_section.SectionType).lower():
        # CASE 1  Flanges of tees
        try:
            _ratio = 0.50 * _section.Bft / _section.Tft
        except ZeroDivisionError:
            _ratio = 0.50 * _section.Bfb / _section.Tfb
            _section.Bft = _section.Bfb
            _section.Tft = _section.Tfb
        
        _lambda_r = 0.56 * math.sqrt(material.E / _Fy)
        _case = '1'
        _case_ratio = 'b/t'
        _case_flag = 'UnStiffend'

        # CASE 4  Stems of tees
        _ratio_stem = _section.D / _section.Tw
        _lambda_r_stem = 0.75 * math.sqrt(material.E / _Fy)
        _case_stem = '4'

        if _ratio_stem > _lambda_r_stem:
            _ration = _ratio_stem
            _lambda_r = _lambda_r_stem
            _case = _case_stem
            _case_ratio = 'd/t'

    # CASE 1 & 5 <-------------
    elif 'channel' in str(_section.SectionType).lower():
        # CASE 1
        _ratio = _section.Bft / _section.Tft
        _lambda_r = 0.56 * math.sqrt(material.E / _Fy)
        _case = '1'
        _case_ratio = 'b/t'
        _case_flag = 'unstiffend'

        # CASE 5
        _ratio_web = hw / _section.Tw
        _lambda_r_web = 1.49 * math.sqrt(material.E / _Fy)
        _case_web = '5'

        if _ratio_web > _lambda_r_web:
            _ratio = _ratio_web
            _lambda_r = _lambda_r_web
            _case = _case_web
            _case_ratio = 'h/tw'
            _case_flag = 'stiffened'

    # CASE 1, 2 & 5 <----------
    elif 'i section' in str(_section.SectionType).lower():
        _ratio_top = 0.5 * _section.Bft / _section.Tft
        try:
            _ratio_bottom = 0.5 * _section.Bfb / _section.Tfb
        except ZeroDivisionError:
            _section.Bfb = _section.Tft
            _section.Tfb = _section.Tft
            _ratio_bottom = _ratio_top
        
        _ratio = max(_ratio_top, _ratio_bottom)

        _case_ratio = 'b/t'
        _case_flag = 'unstiffend'

        # CASE 1
        if _section.build == 'rolled':
            _lambda_r = 0.56 * math.sqrt(material.E / _Fy)
            _case = '1'

        # CASE 2
        else:
            # Kc shall not be taken less than 0.35 nor
            # greater than 0.76 for calculation purposes.                    
            _Kc = 4.0 / math.sqrt(hw / _section.Tw)
            if _Kc < 0.35:
                _Kc = 0.35

            elif _Kc > 0.76:
                _Kc = 0.76
            # print 'Kc =',Kc
            _lambda_r = 0.64 * math.sqrt(_Kc * material.E / _Fy)
            _case = '2'

        # CASE 5
        _ratio_web = hw / _section.Tw
        _lambda_r_web = 1.49 * math.sqrt(material.E / _Fy)
        _case_web = '5'

        if _ratio_web > _lambda_r_web:
            _ratio = _ratio_web
            _lambda_r = _lambda_r_web
            _case = _case_web
            _case_ratio = 'h/tw'
            _case_flag = 'Stiffened'

    # CASE 7 <-----------------
    elif 'flange cover plate' in str(_section.SectionType).lower():
        _ratio = _section.Bfc / _section.Tfc
        _lambda_r = 1.40 * math.sqrt(material.E / _Fy)
        _case = '7'
        _case_ratio = 'b/t'
        _case_flag = 'Stiffened'

    # Case 3 <-----------------
    elif 'angle' in str(_section.SectionType).lower():
        try:
            _ratio = _section.Bfb / _section.Tfb
        except ZeroDivisionError:
            _section.Bfb = _section.Bft 
            _section.Tfb = _section.Tft
            _ratio = _section.Bft / _section.Tft
        
        _lambda_r = 0.45 * math.sqrt(material.E / _Fy)
        _case = '3'
        _case_ratio = 'b/t'
        _case_flag = 'UnStiffend'

    # CASE 9 <-----------------
    elif 'tubular' in str(_section.SectionType).lower():
        _ratio = _section.D / _section.Tw
        _lambda_r = 0.11 * material.E / _Fy
        _case = '9'
        _case_ratio = 'D/t'
        _case_flag = 'stiffened'

        # Case 8 <-----------------
    # All other stiffened elements
    else:
        try:
            _ratio = _section.Bft / _section.Tft
            _lambda_r = 1.49 * math.sqrt(material.E / _Fy)
            _case = '8'
        except ZeroDivisionError:
            print('   ** warning section {:} not implemented'.
                  format(_section.SectionType))
            return 'slender'

    #
    _class = 'nonslender'

    if _ratio > _lambda_r:
        _class = 'slender'
    #
    return _class


#
def table_B41b(_section, material):
    '''
    TABLE B4.1b
    Width-to-Thickness Ratios: Compression Elements
    Members Subject to Flexure
    '''
    _Fy = material.Fy
    if material.Fy == 0:
        _Fy = material.E

    # h : Clear distance between flanges less the fillet or corner 
    #     radius for rolled shapes; distance between adjacent lines 
    #     of fasteners or the clear distance between flanges when 
    #     welds are used for built-up shapes.
    hw = _section.D - _section.Tft - _section.Tfb

    # CASE 17 & 19 <------------
    if 'box' in str(_section.SectionType).lower():
        # Case 17
        _ratio_flange = _section.Bft / _section.Tft
        _lambda_p_flange = 1.12 * math.sqrt(material.E / _Fy)
        _lambda_r_flange = 1.40 * math.sqrt(material.E / _Fy)
        _case_flange = '17'
        _case_flange_ratio = 'b/t'

        # CASE 19
        _ratio_web = hw / _section.Tw
        _lambda_p_web = 2.42 * math.sqrt(material.E / _Fy)
        _lambda_r_web = 5.70 * math.sqrt(material.E / _Fy)
        _case_web = '19'
        _case_web_ratio = 'h/tw'



    # CASE 10 & 14 <------------
    elif 'tee' in str(_section.SectionType).lower():
        # CASE 10 
        #_ratio_flange = 0.5 * _section.Bft / _section.Tft
        try:
            _ratio_flange = 0.50 * _section.Bft / _section.Tft
        
        except ZeroDivisionError:
            _ratio_flange = 0.50 * _section.Bfb / _section.Tfb        
        
        _lambda_p_flange = 0.38 * math.sqrt(material.E / _Fy)
        _lambda_r_flange = 1.0 * math.sqrt(material.E / _Fy)
        _case_flange = '10'
        _case_flange_ratio = 'b/t'

        # CASE 14
        _ratio_web = hw / _section.Tw
        _lambda_p_web = 0.84 * math.sqrt(material.E / _Fy)
        _lambda_r_web = 1.03 * math.sqrt(material.E / _Fy)
        _case_web = '14'
        _case_web_ratio = 'h/tw'

    # CASE 10 & 15 <------------
    elif 'channel' in str(_section.SectionType).lower():
        # CASE 10 
        _ratio_flange = _section.Bft / _section.Tft
        _lambda_p_flange = 0.38 * math.sqrt(material.E / _Fy)
        _lambda_r_flange = 1.0 * math.sqrt(material.E / _Fy)
        _case_flange = '10'
        _case_flange_ratio = 'b/t'

        # CASE 15
        _ratio_web = hw / _section.Tw
        _lambda_p_web = 3.76 * math.sqrt(material.E / _Fy)
        _lambda_r_web = 5.70 * math.sqrt(material.E / _Fy)
        _case_web = '15'
        _case_web_ratio = 'h/tw'
    #
    elif 'i section' in str(_section.SectionType).lower():
        # flange
        _ratio_flange = max(0.5 * _section.Bft / _section.Tft,
                            0.5 * _section.Bfb / _section.Tfb)

        _case_flange_ratio = 'b/t'

        # web
        _ratio_web = hw / _section.Tw
        _case_web_ratio = 'h/tw'

        # symmetry
        sec_symmetry = 'doubly'
        if 'asymmetrical' in str(_section.SectionType).lower():
            sec_symmetry = 'singly'

        # but shall not be taken less than 0.35 nor 
        # greater than 0.76 for calculation purposes.        
        Kc = 4.0 / math.sqrt(hw / _section.Tw)
        if Kc < 0.35:
            Kc = 0.35

        elif Kc > 0.76:
            Kc = 0.76

        # Sxc, Sxt : Elastic section modulus referred to   
        # compression and tension flanges, respectively
        _Sxc = _section.Bft * _section.Tft ** 2 / 6.0
        _Sxt = _section.Bfb * _section.Tfb ** 2 / 6.0

        # for major axis bending of compact and noncompact 
        # web built-up I-shaped members with :
        FL = 0.7 * _Fy
        if _Sxt / _Sxc < 0.7:
            FL = max(_Fy * (_Sxt / _Sxc),
                     0.5 * _Fy)

        # Mp = plastic bending moment
        # -------------------------------------------------
        #   Elastic Neutral Centre 
        _Zc = ((_section.Bft * _section.Tft ** 2 / 2.0 +
                _section.Bfb * _section.Tfb * (hw + _section.Tft + _section.Tfb / 2.0) +
                hw * _section.Tw * (hw / 2.0 + _section.Tft)) /
               (_section.Bft * _section.Tft + hw * _section.Tw + _section.Bfb * _section.Tfb))

        # -------------------------------------------------
        #   Plastic Modulus about Mayor Axis
        _Zpy = ((_section.Tw * hw ** 2 / 4.0) +
                (_section.Bft * _section.Tft * (_Zc - _section.Tft / 2.0)) +
                (_section.Bfb * _section.Tfb * (_section.D - _Zc - _section.Tfb / 2.0)))

        Mpx = _Fy * _Zpy  ##Zx

        # FIXME : Mym needs to be corrected
        # My is the moment at yielding of the extreme fiber
        Mym = _Fy * _section.Zeip
        # 

        # hc : Elastic Centroid to the Compression Flange
        # Twice the distance from the center of gravity to 
        # the following:
        # The inside face of the compression flange less the 
        # fillet or corner radius.
        # For rolled shapes; the nearest line of fasteners at
        # the compression flange or the inside faces of the 
        # compression flange when welds are used, for built-up
        # sections.
        hc = 2 * (_Zc - _section.Tft)
        hctw = hc / _section.Tw

        # FIXME : hp needs to be corrected
        # hp : Twice the distance from the plastic neutral axis 
        # to the nearest line of fasteners at the compression 
        # flange or the inside face of the compression flange
        # when welds are used.
        hp = hc
        #    

        # CASE 10 
        if _section.build == 'rolled':
            _lambda_p_flange = 0.38 * math.sqrt(material.E / _Fy)
            _lambda_r_flange = 1.0 * math.sqrt(material.E / _Fy)
            _case_flange = '10'

        # CASE 11 
        else:
            _lambda_p_flange = 0.38 * math.sqrt(material.E / _Fy)
            _lambda_r_flange = 0.95 * math.sqrt(Kc * material.E / FL)
            _case_flange = '11'

        # CASE 15 
        if sec_symmetry == 'doubly':
            _lambda_p_web = 3.76 * math.sqrt(material.E / _Fy)
            _lambda_r_web = 5.70 * math.sqrt(material.E / _Fy)
            _case_web = '15'

        # CASE 16
        else:
            _lambda_r_web = 5.70 * math.sqrt(material.E / _Fy)
            _lambda_p = (((hc / hp) * math.sqrt(material.E / _Fy)) /
                         (0.54 * (Mpx / Mym) - 0.09) ** 2)
            _lambda_p_web = min(_lambda_p, _lambda_r_web)
            _case_web = '16'

            # Case 12
    elif 'angle' in str(_section.SectionType).lower():
        _ratio_flange = _section.Bfb / _section.Tfb
        _lambda_p_flange = 0.54 * math.sqrt(material.E / _Fy)
        _lambda_r_flange = 0.91 * math.sqrt(material.E / _Fy)
        _case_flange = '12'
        _case_flange_ratio = 'b/t'

        _ratio_web = _section.D / _section.Tw
        _lambda_p_web = _lambda_p_flange
        _lambda_r_web = _lambda_r_flange
        _case_web = _case_flange
        _case_web_ratio = 'd/tw'

    # CASE 18 <----------------- 
    elif 'flange cover plate' in str(_section.SectionType).lower():
        _ratio_flange = _section.Bfb / _section.Tfb
        _lambda_p_flange = 1.12 * math.sqrt(material.E / _Fy)
        _lambda_r_flange = 1.40 * math.sqrt(material.E / _Fy)
        _case_flange = '18'
        _case_web_flange = 'b/t'
    #
    # CASE 20 <-----------------
    elif 'tubular' in str(_section.SectionType).lower():
        _ratio_web = _section.D / _section.Tw
        _lambda_p_web = 0.07 * material.E / _Fy
        _lambda_r_web = 0.31 * material.E / _Fy
        _case_web = '20'
        _case_web_ratio = 'D/t'

        _ratio_flange = _ratio_web
        _lambda_p_flange = _lambda_p_web
        _lambda_r_flange = _lambda_r_web
        _case_flange = _case_web
        _case_flange_ratio = 'D/t'
    else:
        print('   *** warning section {:} not implemented'.
              format(_section.SectionType))
        return 'slender', 'slender'

    # Find flange compacness
    _class_flange = 'noncompact'

    if _ratio_flange < _lambda_p_flange:
        _class_flange = 'compact'

    if _ratio_flange > _lambda_r_flange:
        _class_flange = 'slender'

    # find web compacness
    _class_web = 'noncompact'

    if _ratio_web < _lambda_p_web:
        _class_web = 'compact'

    if _ratio_web > _lambda_r_web:
        _class_web = 'slender'

    return _class_flange, _class_web
    #
