#
# Copyright (c) 2009-2015 fem2ufo
#

# Python stdlib imports
import math
import cmath
import re

# package imports
import fem2ufo.process.operations.feedback as feedback


# --------------------------------------------------------------------
#                      Find joint chord and braces
# --------------------------------------------------------------------
#
#
class JointType():
    #
    __slots__ = ('T', 'Y', 'X', 'K', 'KT', 'SKT')
    #
    def __init__(self):
        # self.number
        # self.name
        self.T = []
        self.X = []
        self.K = []
        self.KT = []
        self.SKT = []
        #


#
def dcosr(v1, v2):
    """
    calculates direction cosines of a vectors
    """
    # nodal coordinates vector 1
    x1, y1, z1 = v1.get_coordinates()
    # nodal coordinates vector 2
    x2, y2, z2 = v2.get_coordinates()
    #
    xd = x2 - x1
    yd = y2 - y1
    zd = z2 - z1
    d = math.sqrt(xd ** 2 + yd ** 2 + zd ** 2)
    al = xd / d
    am = yd / d
    an = zd / d
    #
    return al, am, an
    #


#
def det(a11, a12, a13, a21, a22, a23, a31, a32, a33):
    """
    Calculates a 3x3 determinant
    """
    _det = (a11 * a22 * a33 + a12 * a23 * a31 + a13 * a32 * a21
            - a31 * a22 * a13 - a32 * a23 * a11 - a33 * a12 * a21)
    #
    return _det
    #


#
def side(v1, v2, v3, vp, ptol):
    """
    Given vector 1 to vector 2 and a point v3, 
    finds if vp is on same side of vector 3. 
    All 4 points are coplanar
    """
    iside = 0
    #
    ax, ay, az = dcosr(v1, v2)
    bx, by, bz = dcosr(v1, v3)
    cx, cy, cz = dcosr(v1, vp)

    abx = ay * bz - az * by
    aby = az * bx - ax * bz
    abz = ax * by - ay * bx
    dab = math.sqrt(abx ** 2 + aby ** 2 + abz ** 2)

    acx = ay * cz - az * cy
    acy = az * cx - ax * cz
    acz = ax * cy - ay * cx
    dac = math.sqrt(acx ** 2 + acy ** 2 + acz ** 2)

    abx = abx / dab
    aby = aby / dab
    abz = abz / dab

    acx = acx / dac
    acy = acy / dac
    acz = acz / dac

    sl = abs(abx + acx)
    sm = abs(aby + acy)
    sn = abs(abz + acz)

    if sl < ptol and sm < ptol and sn < ptol:
        iside = 1
    #
    return iside
    #


#
def angle2(m1, m2, angle):
    """
    """
    #
    try:
        jd = str(m1) + '_' + str(m2)
        ang = angle[jd]

    except:

        try:
            jd = str(m2) + '_' + str(m1)
            ang = angle[jd]

        except:
            SystemExit('error angle between member {:} and member {:} no found'
                       .format(m1, m2))
    #
    return ang
    #


#
def selec(m1, m2, _element):
    """
    Select chord members based on its geometry
    """

    check_out = []

    # consider diameters
    dch_d1 = _element[m1].section[0].properties.D
    dch_d2 = _element[m2].section[0].properties.D

    # if diameter 1 = diameter 2
    if dch_d1 == dch_d2:
        dch_t1 = _element[m1].section[0].properties.Tw
        dch_t2 = _element[m2].section[0].properties.Tw
        # if thickness 1 = thickness 2
        if dch_t1 == dch_t2:
            dch_fy1 = _element[m1].material[0].Fy
            dch_fy2 = _element[m2].material[0].Fy

            # if Fy1 < Fy2 select diameter 2
            if dch_fy1 < dch_fy2:
                check_out.append('\n')
                check_out.append('D1 = D2, tw1 = tw2, Fy1 < Fy2 --> D2 [{:}]\n'
                                 .format(_element[m2].name))
                return m2, m1, check_out

                # select diameter 1
            else:
                check_out.append('\n')
                check_out.append('sections with equal D [{:1.3e}], tw [{:1.3e}] and Fy [{:1.3e}] --> D1 [{:}]\n'
                                 .format(dch_d1, dch_t1, dch_fy1, _element[m1].name))
                return m1, m2, check_out

        # if thickness 1 < thickness 2 select diameter 2
        elif dch_t1 < dch_t2:
            check_out.append('\n')
            check_out.append('D1 = D2, tw1 < tw2 --> D2 [{:}]'
                             .format(_element[m2].name))
            return m2, m1, check_out
        # select diameter 1
        else:
            check_out.append('\n')
            check_out.append('D1 = D2, tw1 > tw2 --> D1 [{:}]\n'
                             .format(_element[m1].name))
            return m1, m2, check_out
    # if diameter 1 < diameter 2 select diameter 2
    elif dch_d1 < dch_d2:
        check_out.append('\n')
        check_out.append('D1 < D2 --> D2 [{:}]\n'
                         .format(_element[m2].name))
        return m2, m1, check_out
    # select diameter 1
    else:
        check_out.append('\n')
        check_out.append('D1 > D2 --> D1 [{:}]\n'
                         .format(_element[m1].name))
        return m1, m2, check_out
        #
        #


#
# --------------------------------------------------------------------
#
def chord(_joint, _element, _branch, _atol):
    """
    Find chord and braces members
    """
    #
    # fileCheck = 'checkMe.txt'
    check_out = []
    #
    nx = 0
    ncount = 0
    _chord = []
    _avec = {}
    _brace = []
    _toremove = []
    _chrd = []
    # _angle = []
    _x_memb = []
    _branch_new = dict(_branch)

    # find chords of branch based on tolerance (tol)
    for m1, [im1, jm1] in _branch.items():

        if im1.name != _joint.node.name:
            jm1 = im1
            im1 = _joint.node

        # find direction cosines chord 1
        c1l, c1m, c1n = dcosr(im1, jm1)

        # delete current branch
        del _branch_new[m1]

        # iterate with remaining branchs
        for m2, [im2, jm2] in _branch_new.items():

            if im2.name != _joint.node.name:
                jm2 = im2
                im2 = _joint.node

            # find direction cosines chord 2
            c2l, c2m, c2n = dcosr(im2, jm2)

            # find angle between chord 1 & 2
            _ang_complex = cmath.acos(c1l * c2l + c1m * c2m + c1n * c2n)
            _ang = _ang_complex.real

            jd2 = str(m1) + '_' + str(m2)
            _avec[jd2] = _ang

            # in angle << tolerance, probably an overlaping member
            if _ang <= _atol:
                # print('angle', _ang, 'mem', m1, m2)
                check_out.append('\n')
                check_out.append('** AN ANGLE AT NODE: {:} IS {:1.4e} deg BETWEEN ELEMENTS {:} {:}\n'
                                 .format(_joint.node.number,
                                         math.degrees(_ang), m1, m2))
                check_out.append('   REVISE Angle Tolerance\n')
                check_out.append('\n')
                # member with smallest D, t or Fy will be eliminated
                # assuming nopt = 1 (to be fixed)
                ts, tr, _msg = selec(m1, m2, _element)
                check_out.extend(_msg)
                _toremove.append(tr)
                continue

            # check if member is a brace
            _ang2 = abs(_ang - math.pi)
            if _ang2 > _atol:
                _brace.append(m2)
                continue

            # then these 2 members form the chord
            _chrd.append([m1, m2])
    #
    # delete elements probably overlaping existing members
    _chord = []
    for _items in _chrd:
        if set(_items) & set(_toremove):
            continue
        _chord.append(_items)
    #
    if len(_chord) > 1:
        # Find if X brace exist
        _chord, _x_memb, ncount, nx, _msg = Xjoint(_chord, _element,
                                                   ncount, nx)
        check_out.extend(_msg)

    else:
        try:
            _chord = _chord[0]
        except IndexError:
            check_out.append('\n')
            check_out.append('Error joint {:} [node {:}] has no chords, check tolerance --> joint omited\n'
                             .format(_joint.name, _joint.node.name))
    #
    _brace = set(_brace)
    # _braces = _Xelement
    # for _memb in _Xelement:
    #    for _mem in _memb:
    #        _braces.update(_branch[_mem])    
    #
    return _chord, _avec, _x_memb, _toremove, check_out
    #


#
# FIXME : joint types not currently implemented
def Xjoint(_chord, _element, ncount, nx):
    """
    X-joint
    """
    #
    check_out = []
    mtot = len(_chord)
    _x_files = []
    # go over all chord elements to find brace 1 sharing node
    mn1j1 = _chord[0][0]
    mn2j1 = _chord[0][1]

    # consider diameters if nopt = 1 (to be fixed)
    _m1a, _m1b, _msg = selec(mn1j1, mn2j1, _element)
    check_out.extend(_msg)
    _chord_1 = [_m1a, _m1b]
    # checkOut.append('selecting m1 --> stay {:}, remove {:}'.format(_m1a, _m1b))

    # go over all chord elements to find brace 2 sharing node
    for j2 in range(1, mtot):

        mn1j2 = _chord[j2][0]
        mn2j2 = _chord[j2][1]
        #
        _m2a, _m2b, _msg = selec(mn1j2, mn2j2, _element)
        check_out.extend(_msg)
        # FIXME: why this is not used?
        _chord_2 = [_m2a, _m2b]
        # checkOut.append('selecting m2 --> stay {:}, remove {:}'.format(_m2a, _m2b))

        ts, tr, _msg = selec(_m1a, _m2a, _element)
        check_out.extend(_msg)
        # checkOut.append('Final selec -- > stay {:}, remove {:}'.format(ts, tr))

        ncount += 1

        if ts in _chord_1:
            _x_files.append([_m2a, _m2b])
            check_out.append('\n')
            check_out.append('{:}.  X    CHORD : {:} {:}   BRANCH   : {:} {:}\n'.
                             format(ncount, mn1j1, mn2j1, mn1j2, mn2j2))
            #
        else:
            _chord_1 = [_m2a, _m2b]
            _x_files.append([_m1a, _m1b])
            nx += 1
            check_out.append('\n')
            check_out.append('{:}.  X    CHORD : {:} {:}   BRANCH   : {:} {:}\n'.
                             format(ncount, mn1j2, mn2j2, mn1j1, mn2j1))
            #
    #
    return _chord_1, _x_files, ncount, nx, check_out
    #


#
def TYjoint(_chord, _element, _branch, nt, ny,
            ncount, _nopt, _avec, _atol):
    """
    TY joint
    """
    #
    # fileCheck = 'checkMe.txt'
    check_out = []
    #
    _brace = []
    _angle = []
    _type = []

    # go over all chord elements to find braces sharing node
    # for _crd in joint.chord:
    # for j1 in range(mtot):
    # chord 1 & 2
    m1 = _chord[0]
    m2 = _chord[1]

    # go over all elements (branch) to find braces (sharing chords)
    for mm in _branch.keys():
        # if mm == m1 or mm == m2 : continue

        # consider diameters (need check this)
        if _nopt == 1:
            dchmin = min(_element[m1].section[0].properties.D,
                         _element[m2].section[0].properties.D)
            dbrmax = _element[mm].section[0].properties.D

            if dbrmax > dchmin:
                continue

        # find angle between chord and brace
        ang = angle2(m1, mm, _avec)
        an = abs(ang - math.pi / 2.0)

        # check if T
        if an <= _atol:
            nt += 1
            ncount += 1
            check_out.append('\n')
            check_out.append('{:5d}.  T    CHORD : {:4d} {:4d}   BRANCH   : {:4d}\n'.
                             format(ncount, m1, m2, mm))
            _brace.append(mm)
            _angle.append(ang)
            _type.append('T')
            continue

            # if not T then Y
        ny += 1
        ncount += 1
        check_out.append('\n')
        check_out.append('{:5d}.  Y    CHORD : {:4d} {:4d}   BRANCH   : {:4d}\n'.
                         format(ncount, m1, m2, mm))
        _brace.append(mm)
        _angle.append(ang)
        _type.append('Y')
    # L60:
    # L50:
    #
    return nt, ny, ncount
    #


#
def Kjoint(_joint, _element, _branch, nk,
           ncount, ptol, _avec, _atol, _nopt):
    """
    K joint
    """
    #
    # fileCheck = 'checkMe.txt'
    check_out = []
    #
    pi2 = math.pi / 2.0
    # mtot = len(_joint.chord)

    # ktot is n0. of K joists
    ak = []
    bk = []
    ck = []
    dk = []
    kc1 = []
    kc2 = []
    kb1 = []
    kb2 = []
    _brace = []
    _angle = []
    _type = []

    # go over all chord elements to find braces sharing node
    # for j in range(mtot):
    m1 = _joint.chord[0]
    m2 = _joint.chord[1]

    # element[mn1[j1]].section[0].properties.D
    im1 = _element[m1].node[0]
    jm1 = _element[m1].node[1]

    if im1.number != _joint.node.number:
        jm1 = im1
        im1 = _joint.node

    # go over all elements (branch) to find branch branch number j1
    _branch_new = dict(_branch)
    for mm1, [imm, jmm] in _branch.items():
        #
        # if mm1 == m1 or mm1 == m2 : continue
        if imm.number == _joint.node.number:
            lmm = jmm
        elif jmm.number == _joint.node.number:
            lmm = imm
        #
        a = det(im1.coordinate[1], im1.coordinate[2], 1.,
                jm1.coordinate[1], jm1.coordinate[2], 1.,
                lmm.coordinate[1], lmm.coordinate[2], 1.)

        b = -det(im1.coordinate[0], im1.coordinate[2], 1.,
                 jm1.coordinate[0], jm1.coordinate[2], 1.,
                 lmm.coordinate[0], lmm.coordinate[2], 1.)

        c = det(im1.coordinate[0], im1.coordinate[1], 1.,
                jm1.coordinate[0], jm1.coordinate[1], 1.,
                lmm.coordinate[0], lmm.coordinate[1], 1.)

        d = -det(im1.coordinate[0], im1.coordinate[1], im1.coordinate[2],
                 jm1.coordinate[0], jm1.coordinate[1], jm1.coordinate[2],
                 lmm.coordinate[0], lmm.coordinate[1], lmm.coordinate[2])

        # go over all elements (branch) to find branch branch number j2
        del _branch_new[mm1]
        for mm2, [imm, jmm] in _branch_new.items():
            # if mm2 == m1 or mm2 == m2 : continue
            if imm.number == _joint.node.number:
                kmm = jmm

            elif jmm.number == _joint.node.number:
                kmm = imm

            # check for coplanarity
            res = abs(a * kmm.coordinate[0]
                      + b * kmm.coordinate[1]
                      + c * kmm.coordinate[2]
                      + d)

            if res > ptol:
                continue

            # check if branch on the same side or not
            iside = side(im1, jm1, lmm, kmm, ptol)

            if iside == 1:
                continue

            # check if branche is perpendicular to chord or not
            # ang = angle(j1, m1n, ntot, avec)
            ang = angle2(m1, mm1, _avec)
            t1 = abs(ang - pi2)

            # ang = angle(j2, m1n, ntot, avec)
            ang = angle2(m1, mm2, _avec)
            t2 = abs(ang - pi2)

            if t1 <= _atol or t2 <= _atol:
                continue

            # check if branch is on either side of the perpendicular to the chord
            icond = 0
            # ang1 = angle(j1, m1n, ntot, avec)  # branch 1, chord 1, total chord elements
            ang1 = angle2(m1, mm1, _avec)

            # ang2 = angle(j2, m2n, ntot, avec)  # branch 2, chord 2, total chord elements
            ang2 = angle2(m2, mm2, _avec)
            if (ang1 < pi2) and (ang2 < pi2):
                icond = 1

            # ang1 = angle(j2, m1n, ntot, avec)
            ang1 = angle2(m1, mm2, _avec)

            # ang2 = angle(j1, m2n, ntot, avec)
            ang2 = angle2(m2, mm1, _avec)

            if (ang1 < pi2) and (ang2 < pi2):
                icond = 1

            if icond == 0:
                continue

                # consider diameters
            if _nopt == 1:
                dchmin = min(_element[m1].section[0].properties.D,
                             _element[m2].section[0].properties.D)

                dbrmax = max(_element[mm1].section[0].properties.D,
                             _element[mm2].section[0].properties.D)

                if dbrmax > dchmin:
                    continue
                    #
            # ktot += 1
            ak.append(a)
            bk.append(b)
            ck.append(c)
            dk.append(d)
            kc1.append(m1)
            kc2.append(m2)
            kb1.append(mm1)
            kb2.append(mm2)
            nk += 1
            ncount += 1
            check_out.append('{:5d}.  K    CHORD : {:4d} {:4d}   BRANCH   : {:4d} {:4d}\n'.
                             format(ncount, m1, m2, mm1, mm2))

            _brace.append([mm1, mm2])
            _angle.append([ang1, ang2])
            _type.append('k')
            # L100:
    # L90:
    # L80:
    #
    return nk, ncount, ak, bk, ck, dk, kc1, kc2, kb1, kb2
    #


#
def KT_SKTjoint(_joint, _element, _branch, nskt, nkt, ncount,
                ak, bk, ck, dk, kc1, kc2, kb1, kb2,
                _avec, _nopt, _atol, ptol):
    """
    K-T/SK-T joint
    """
    #
    # fileCheck = 'checkMe.txt'
    check_out = []
    #
    # ntot = len(iel)
    ktot = len(ak)
    pi2 = math.pi / 2.0
    _brace = []
    _angle = []
    _type = []

    # go over each K
    for j in range(ktot):
        k1 = kc1[j]
        k2 = kc2[j]
        k3 = kb1[j]
        k4 = kb2[j]

        # for j1 in range(ntot):
        for m1, [im1, jm1] in _branch.items():
            #
            if m1 == k1 or m1 == k2 or m1 == k3 or m1 == k4:
                continue

            if im1.number == _joint.node.number:
                llm = jm1

            elif jm1.number == _joint.node.number:
                llm = im1

            res = abs(ak[j] * llm.coordinate[0]
                      + bk[j] * llm.coordinate[1]
                      + ck[j] * llm.coordinate[2]
                      + dk[j])

            # an1 = angle(j1, jj1, ntot, avec)
            an1 = angle2(m1, k1, _avec)

            # an2 = angle(j1, jj2, ntot, avec)
            an2 = angle2(m1, k3, _avec)

            an1 = abs(an1 - pi2)
            an3 = an2
            an2 = abs(an2 - pi2)

            # consider diameters
            if _nopt == 1:
                dchmin = min(_element[k1].section[0].properties.D,
                             _element[k2].section[0].properties.D)

                dbrmax = max(_element[k3].section[0].properties.D,
                             _element[k4].section[0].properties.D,
                             _element[m1].section[0].properties.D)

                if dbrmax > dchmin:
                    continue

            # check if branch is coplanar as well as normal to chord, then KT
            if an1 <= _atol and an3 < pi2 and res <= ptol:
                nkt += 1
                ncount += 1
                check_out.append('{:5d}.  K-T  CHORD : {:4d} {:4d}   BRANCH K : {:4d} {:4d}   BRANCH T : {:4d}\n'.
                                 format(ncount, k1, k2, k3, k4, m1))
                _brace.append(m1)
                _angle.append(an1)
                _type.append('T')
                _brace.append([k3, k4])
                _angle.append([an2, an3])
                _type.append('SK-T')

                # check if bracnh is out of plane and norm_al to chord and a K limb, then SKT
            if an1 <= _atol and an2 <= _atol and res > ptol:
                nskt += 1
                ncount += 1
                check_out.append('{:5d}.  SK-T CHORD : {:4d} {:4d}   BRANCH K : {:4d} {:4d}   BRANCH T : {:4d}\n'.
                                 format(ncount, k1, k2, k3, k4, m1))
                _brace.append(m1)
                _angle.append(an1)
                _type.append('T')
                _brace.append([k3, k4])
                _angle.append([an2, an3])
                _type.append('SK-T')
                # L130:
    # L110:
    #
    return nkt, nskt
    #


#
#
def filter_tubular_joint(_jnt, _element):
    """
    """
    check_out = []
    _status = ""
    # Create a list with all members sharing node
    _branch = {}
    for _memb_no in _jnt.node.elements:

        # check if element exist
        try:
            _member = _element[_memb_no]
        except KeyError:
            continue

        # check if non-tubular member at the joint
        try:
            if _member.section[0].shape == 'PIPE':
                _n0 = _member.node[0]
                _n1 = _member.node[1]
                _branch[_member.number] = [_n0, _n1]
            else:
                _status = "nontubular"
                _branch = {}
                break
        # 
        # trying to capture shim/spring modelling
        except IndexError:
            if _member.type.element == 'spring':
                _status = "shim_or_spring"
                _branch = {}
                break
            else:
                _status = "unknow"
                _branch = {}
                break
                #
    if _status:
        check_out.append('\n')
        check_out.append('** WARNING Tubular joint {:} is no valid, it has {:} element\n'
                         .format(_jnt.name, _status))
    #
    return _branch, check_out


#
#
def tubular_joint(joint, element, _atol=0, _ptol=1.0e-02,
                  _ncase=0, _nopt=1):
    """
    Find braces and cans in a joint concept\n
    
    joint   : Node number of the joint
    element : Element data
    atol    : Tolerance of any angle in degrees (1 degree)
    ptol    : Tolerance on test of coplanary, etc. (0.01)
    ncase   : joint case (5)
              1 - T and Y
              2 - X
              3 - K
              4 - K-T
              5 - All
    nopt    : 0 - Joint independent of diameter
              1 - Chord/brace diameters are considered for
                  viability of a joint
    """
    #
    print('    * Finding tubular joints components')
    #
    if _ncase == 0:
        _ncase = 5

    if _atol != 0.:
        _atol = abs(_atol * math.pi / 180.0)

    if _atol == 0.:
        _atol = math.pi / 180.0

    if _ptol == 0.:
        _ptol = 1.0e-02
    #
    # fileCheck = 'checkMe.txt'

    header = 'TUBULAR JOINT SECTION'
    check_out = feedback.print_head_line(header)
    #
    # checkOut = []
    check_out.append('\n')
    check_out.append('Tubular joint module\n')
    check_out.append('\n')
    check_out.append("angle tolerance = {:1.6e} ptol = {:1.6e}\n"
                     .format(float(_atol), float(_ptol)))
    check_out.append('\n')
    # checkOut.close()

    # set counters to count no. of each joint and total no.
    nt = 0
    ny = 0
    nx = 0
    nk = 0
    nkt = 0
    nskt = 0
    ncount = 0
    _jnt_copy = dict(joint)

    for key, _jnt in _jnt_copy.items():
        _branch, _msg = filter_tubular_joint(_jnt, element)
        check_out.extend(_msg)
        # check no braces
        if not _branch:
            check_out.append('\n')
            check_out.append('** WARNING Tubular joint {:} has not braces\n'
                             .format(_jnt.name))
            del joint[key]
            continue
        # Find chords can members of the joint (geometry only)
        _chord, avec, _x_element, _overlapping, _msg = chord(_jnt, element, _branch, _atol)
        check_out.extend(_msg)
        #
        if _chord:
            joint[key].chord.extend(_chord)
        # if no chords then next master node
        else:
            del joint[key]
            continue
        #
        # delete _chords members in branch
        _braces = dict(_branch)
        for _items in joint[key].chord:
            try:
                del _braces[_items]
            except IndexError:
                continue
        #
        # joint[key].chord.extend(_chord)
        # joint[key].theta = _angle
        joint[key].brace.extend(_braces)
        #
        mtot = len(joint[key].chord)
        ntot = len(_braces)
        #
        #
        check_out.append('\n')
        check_out.append('NODE = {:}  NTOT  = {:}  NTOT = {:}\n'
                         .format(key, ntot, mtot))
        check_out.append('--------------------\n')
        check_out.append('CHORDS ARE :\n')
        # for ij in range(mtot):
        check_out.append("{:} {:}\n".format(_jnt.chord[0], _jnt.chord[1]))

        # check_out.append('N0. TYPE CHORD-BRANCH\n')

        #
        # FIXME: don't understand, then code below ignored
        continue

        # ---------------------------------------------
        # Find braces (and joint types) sharing chords
        # ---------------------------------------------

        # T, Y-joint
        if _ncase == 1 or _ncase == 5:
            nt, ny, ncount = TYjoint(_chord[0], element, _braces,
                                     nt, ny, ncount, _nopt, avec, _atol)

        # X-joint
        if ntot < 4:
            continue

        # if _ncase == 2 or _ncase == 5 :
        #    nx, ncount = Xjoint(_jnt, _element, nx, ncount, _nopt)
        #
        if _x_element:
            # nx += 1
            _ang = []
            for _item in _x_element:
                _ang2 = angle2(_item[0], _item[1], avec)
                _ang.append(_ang2)

            try:
                joint[key].type['X']['brace'].extend(_x_element)
                joint[key].type['X']['angle'].extend(_ang)
            except:
                joint[key].type.update({'X': {'brace': _x_element, 'angle': _ang}})
                # joint[key].Type.X.append[_mn2 ]
        #
        # checkOut.append('===X')

        # K-joint
        if _ncase < 3:
            continue
        nk, ncount, ak, bk, ck, dk, kc1, kc2, kb1, kb2 = Kjoint(_jnt, element, _braces,
                                                                nk, ncount, _ptol, avec,
                                                                _atol, _nopt)


        # K-T/SK-T joint
        if ntot < 5 or _ncase < 4:
            continue

        if len(ak) == 0:
            continue

        nkt, nskt = KT_SKTjoint(_jnt, element, _braces, nskt, nkt, ncount,
                                ak, bk, ck, dk, kc1, kc2, kb1, kb2,
                                avec, _nopt, _atol, _ptol)
        #
        # L25:
        #
        # joint[key].brace = copy.deepcopy(_braces)
        #
    # L10:
    #
    if ncount > 0:
        # checkOut = []
        # checkOut.append(ncount, nt, ny, nx, nk, nkt, nskt)
        check_out.append('\n')
        check_out.append("TOTAL NO. OF JOINT IN THE STRUCTURE = {:}\n".format(ncount))
        check_out.append("No. of T   = {:}\n".format(nt))
        check_out.append("No. of Y   = {:}\n".format(ny))
        check_out.append("No. of X   = {:}\n".format(nx))
        check_out.append("No. of K   = {:}\n".format(nk))
        check_out.append("No. of KT  = {:}\n".format(nkt))
        check_out.append("No. of SKT = {:}\n".format(nskt))
        # checkOut.close()
    #
    #
    # print('ok')
    #
    return check_out
    #
#
#
#
#
#
def find_join_rule(word_in):
    """
    Find joint rule reserve / key words 
    """
    _key = {"msl": r"\b(msl)\b",
            "norsok": r"\b(norsok(\_|\-)?(r2)?)\b",
            "nor_r3": r"\b(nor(sok)?(\_|\-)?r3)\b",
            "iso": r"\b(iso(\_|\-)?(19902(\:)?2007)?)\b",
            "api-wsd": r"\b(api(\_|\-)?(wsd|asd))\b"}

    _match = match_line(word_in, _key)

    if not _match:
        _match = False

    return _match


#
def match_line(word_in, key):
    """
    search key word at the begining of the string
    """
    word_out = False

    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.match(word_in)

        if keys:
            word_out = _key

    return word_out
    #
#
#
#
#
def find_chord_brace(_joint, _element, _branch, _atol):
    """
    Find chord and braces members
    """
    #
    _brace = []
    _toremove = []
    _chrd = []
    _branch_new = dict(_branch)

    # find chords of branch based on tolerance (tol)
    for m1, [im1, jm1] in _branch.items():

        if im1.name != _joint.node.name:
            jm1 = im1
            im1 = _joint.node

        # find direction cosines chord 1
        c1l, c1m, c1n = dcosr(im1, jm1)

        # delete current branch
        del _branch_new[m1]

        # iterate with remaining branchs
        for m2, [im2, jm2] in _branch_new.items():

            if im2.name != _joint.node.name:
                jm2 = im2
                im2 = _joint.node

            # find direction cosines chord 2
            c2l, c2m, c2n = dcosr(im2, jm2)

            # find angle between chord 1 & 2
            _ang_complex = cmath.acos(c1l * c2l + c1m * c2m + c1n * c2n)
            _ang = _ang_complex.real

            # in angle << tolerance, probably an overlaping member
            if _ang <= _atol:
                # member with smallest D, t or Fy will be eliminated
                # assuming nopt = 1 (to be fixed)
                ts, tr, _msg = selec(m1, m2, _element)
                _toremove.append(tr)
                continue

            # check if member is a brace
            _ang2 = abs(_ang - cmath.pi)
            if _ang2 > _atol:
                _brace.append(m2)
                continue
            # then these 2 members form the chord
            _chrd.append([m1, m2])
    #
    # delete elements probably overlaping existing members
    _chord = []
    for _items in _chrd:
        if set(_items) & set(_toremove):
            continue
        _chord.extend(_items)
    #
    _brace = list(set(_brace))
    #
    return _chord, _brace
    #
#
#
#
