#
# Copyright (c) 2009-2017 fem2ufo
# 

# Python stdlib imports



# package imports
import fem2ufo.f2u_model.control as f2u


#
#
def insert_wave(metocean, _no_wave, _name_wave, _theory, height, period,
                phase=0.0, time=0.0, step=0.0, number_step=1,
                direction=0.0):
    """
    """
    #_theory = wave_theory(_theory)

    metocean.wave[_no_wave] = f2u.Metocean.Wave(_no_wave, _name_wave)
    metocean.wave[_no_wave].theory = _theory

    metocean.wave[_no_wave].height = 0.0
    metocean.wave[_no_wave].period = 10.0
    metocean.wave[_no_wave].phase = 0.0
    metocean.wave[_no_wave].time = 0.0
    metocean.wave[_no_wave].step = 0.0
    metocean.wave[_no_wave].number_step = 1
    metocean.wave[_no_wave].direction = 0.0
    #
    return metocean
#
#
def add_wave(_metocean, _no_wave, _name_wave,
             _theory, height, period):
    """
    """
    _metocean = insert_wave(_metocean, _no_wave, _name_wave,
                            _theory, height, period)

    return _metocean
#


