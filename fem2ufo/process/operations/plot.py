#
#
#
import sys
#
import matplotlib.pyplot as plt
#
#
def plotModel(elements):
    #
    import matplotlib as mpl
    from mpl_toolkits.mplot3d import Axes3D
    #
    mpl.rcParams['legend.fontsize'] = 10

    fig = plt.figure()
    ax = fig.gca(projection='3d')


    # theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
    # z = np.linspace(-2, 2, 100)
    # r = z**2 + 1
    # x = r * np.sin(theta)
    # y = r * np.cos(theta)
    for _memb in elements.values():
        x = []
        y = []
        z = []
        for _node in _memb.node:
            x.append(_node.coordinate[0])
            y.append(_node.coordinate[1])
            z.append(_node.coordinate[2])

        ax.plot(x, y, z, label='jacket')

    # print(_memb.name)

    # ax.plot(x, y, z, label='jacket')
    # ax.legend()

    plt.show()
    print('ok')


#
def pltTH(_timehistory):
    # plot time history
    import SDoF as sdof
    import matplotlib.pyplot as plt

    _noTH = 0
    for _th in _timehistory:
        _noTH += 1
        _noSeries = len(_th[1])
        a = _th[1][0]
        for x in range(1, _noSeries):
            b = _th[1][x]
            # _figOut = str(_th[0]) + 'time_history'
            label_line = 'data_' + str(x)

            sdof.time_history_plot(a, b, _noTH,
                                   'Time(sec)', 'Accel(G)',
                                   _th[0], label_line,
                                   'time_history')
    #
    # plt.legend()
    plt.show()

#
