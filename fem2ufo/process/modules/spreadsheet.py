# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
#import math

# package imports
from fem2ufo.process.modules.xlsx.xlsx import Workbook

#
#
#
def col_to_num(col_str):
    """ Convert base26 column string to number. """
    expn = 0
    col_num = 0
    for char in reversed(col_str):
        col_num += (ord(char) - ord('A') + 1) * 26**expn
        expn += 1
    return col_num
#
def letter2num(letters, zbase=False):
    """A = 1, C = 3 and so on. Convert spreadsheet style column enumeration to a number.
    
    Answers:
    A = 1, Z = 26, AA = 27, AZ = 52, ZZ = 702, AMJ = 1024

    >>> letter2num('A') == 1
    True
    >>> letter2num('Z') == 26
    True
    >>> letter2num('AZ') == 52
    True
    >>> letter2num('ZZ') == 702
    True
    >>> letter2num('AMJ') == 1024
    True
    >>> letter2num('AMJ', zbase=True) == 1023
    True
    >>> letter2num('A', zbase=True) == 0
    True
    """
    letters = letters.upper()
    res = 0
    weight = len(letters) - 1
    for i, c in enumerate(letters):
        res += (ord(c) - 64) * 26**(weight - i)
    if not zbase:
        return res
    return res - 1
#
#
import string
class ColumnName(dict):
    """
    """
    def __init__(self):
        super(ColumnName, self).__init__()
        self.alphabet = string.ascii_uppercase
        self.alphabet_size = len(self.alphabet)

    def __missing__(self, column_number):
        ret = self[column_number] = self.get_column_name(column_number)
        return ret

    def get_column_name(self, column_number):
        if column_number <= self.alphabet_size:
            return self.alphabet[column_number - 1]
        else:
            return self.alphabet[int(((column_number - 1) / self.alphabet_size)) - 1] + self.alphabet[((column_number - 1) % self.alphabet_size)]
#
#
#
class SpreadSheet:
    """
    """
    _cells = {}
    tools = {}
    
    def __setitem__(self, key, formula):
        if isinstance(formula, str) and formula[0] == '=':
            formula = formula[1:]
        else:
            formula = (formula,)
        self._cells[key] = formula
    
    def getformula(self, key):
        c = self._cells[key]
        if isinstance(c, str):
            return '=' + c
        return c[0]
    
    def __getitem__(self, key ):
        c = self._cells[key]
        if isinstance(c, str):
            return eval(c, SpreadSheet.tools, self)
        return c[0]
#
#
#
#from math import sin, pi
#SpreadSheet.tools.update(sin=sin, pi=pi, len=len)
#ss = SpreadSheet()
#ss['a1'] = 5
#ss['a2'] = '=a1*6'
#ss['a3'] = '=a2*7'
#
#print(ss['a3'])
#
#assert ss['a3'] == 210
#ss['b1'] = '=sin(pi/4)'
#print(ss['b1'])
#assert ss['b1'] == 0.7071067811865476
#assert ss.getformula('b1') == '=sin(pi/4)'
