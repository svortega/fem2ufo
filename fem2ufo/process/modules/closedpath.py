﻿#Imports System

class Closedpath:
	# A global point needed for  sorting points with reference
	# to the first point. Used in compare function of qsort()
	#Public Shared p0 As New Point()
	#
	def __init__(self, P) -> None:
		"""
		"""
		self.points = P
	#
	#
	@staticmethod
	def swap(p1, p2) -> int:# As Integer
		"""A utility function to swap two points"""
		#temp = p1
		p1 = p2 = p1
		#p2 = temp
	# 
	@staticmethod
	def dist(p1, p2) -> int: #As Integer
		"""A utility function to return square of distance between p1 and p2"""
		return (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y)
	#
	@staticmethod
	def orientation(p, q, r) -> int: #As Integer
		"""
		To find orientation of ordered triplet (p, q, r).
		The function returns following values
		0 --> p, q and r are colinear
		1 --> Clockwise
		2 --> Counterclockwise
		"""
		val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y)
		if val == 0 :
			return 0 # colinear
		#End If
		if val > 0:
			return 1
		else:
			return 2
		# return if val > 0, 1, 2 # clockwise or counterclock wise
	#
	#
	def compare(vp1, vp2) -> int: #As Integer
		"""
		A function used by library function qsort() to sort
		an array of points with respect to the first point
		"""
		#Dim p1 As Point = DirectCast(vp1, Point)
		#Dim p2 As Point = DirectCast(vp2, Point)
		#
		# Find orientation
		orientation = self.orientation(p0, p1, p2)
		if orientation == 0 :
			#ORIGINAL LINE: Return (dist(p0, *p2) >= dist(p0, *p1))? -1 : 1;
			if self.dist(p0, p2) >= self.dist(p0, p1):
				return -1
			else:
				return 1
			#return if(self.dist(p0, p2) >= self.dist(p0, p1), -1, 1)
		#End If
		if orientation == 2:
			return -1
		else:
			return 1
		#return if orientation == 2, -1, 1
	#
	# Prints simple closed path for a set of n points.
	def printClosedPath(self):
		"""
		"""
		n = len(self.points)
		# Find the bottom most point
		ymin: int = self.points[0].y
		_min: int = 0
		for i in range(1, n - 1):
			y = self.points[i].y
			# Pick the bottom-most. In case of tie, chose the
			# left most point
			if (y < ymin) or (ymin == y and self.points[i].x < self.points[_min].x) :
				ymin = self.points[i].y
				_min = i
			#End If
		#Next i
		# Place the bottom-most point at first position
		self.swap(self.points[0], self.points[_min])
 
		# Sort n-1 points with respect to the first point.
		# A point p1 comes before p2 in sorted ouput if p2
		# has larger polar angle (in counterclockwise
		# direction) than p1
		p0 = self.points[0]
		#C++ TO VB CONVERTER TODO TASK: There is no VB equivalent to 'sizeof':
		#qsort(self.points[1], n-1, len(self.point), compare)
 
		# Now stack has the output points, print contents
		# of stack
		for i in range(n-1):
			print("(")
			print(self.points[i].x)
			print(", ")
			print(self.points[i].y)
			print("), ")
		#Next i
	#End Sub
#End Class

# A C++ program to find simple closed path for n points
# for explanation of orientation()

#class Point:
#	Public x As Integer
#	Public y As Integer
#End Class