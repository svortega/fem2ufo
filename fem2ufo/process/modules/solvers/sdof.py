#
# Copyright (c) 2009-2019 fem2ufo
#

# Python stdlib imports
import sys
from math import pi, sqrt, floor, log, sin, cos, exp
from bisect import bisect_left

# package imports
#
try:
    import numpy as np
except ImportError:
    pass
#
#
# SDoF calculation
def motion_constants(T, gamma):
    """
    """
    # frequency (Hz)
    try : 
        fn = 1.0 / T
    except ZeroDivisionError :
        return
    # undamped natural frequency
    omegan = 2 * pi * fn
    # damped natural frequency
    _beta = omegan * sqrt(1 - pow(gamma,2))    
    # damping C
    _alpha = gamma * omegan
    two_domegan = 2 * _alpha
    # stiffness K
    omn2 = omegan**2
    #
    return omegan, two_domegan, omn2, _alpha, _beta
    #
#
# Response to Arbitrary Base Input
def ode_base_input(time, acceleration,  period, gamma):
    """
    Input:
    a  : Time(sec)
    b  : Accel(G)
    T  : period (sec)
    damp : damping ratio
    grav : gravity
    Output
    x : relative displacement
    y : relative velocity
    accel : Acceleration
    """
    #
    #
    # find numpy module
    try:
        import numpy as np
        #
        periods = [period]
        td = time[len(time)-1]- time[len(time)-2]
        #
        respose = NigamJennings(acceleration, periods, gamma, time_step=td)
        #respose = NewmarkBeta(acceleration, periods, gamma, time_step=td)
        accel, vel, disp = respose.evaluate()
        accel, vel, disp = accel.T, vel.T, disp.T
        return [time, disp[0].tolist(), vel[0].tolist(), accel[0].tolist()]
        
        #def F(f, x, y):
        #    F = np.zeros((2))
        #    F[0] = y[1] 
        #    F[1] = f - two_domegan * y[1] - omn2 * y[0]
        #    return F
        #
        #X, Y = integrate_rk4(F, acceleration, time)
        #Z = np.array((two_domegan * Y[:,1] - omn2 * Y[:,0]))
        #return [X.tolist(), Y[:,0].tolist(), Y[:,1].tolist(), Z.tolist()]
    # pure python
    except ImportError:
        omegan, two_domegan, omn2, _alpha, _beta = motion_constants(period, gamma)
        x = rk4(acceleration, time, two_domegan, omn2)
        return [time, x[0], x[1], x[2]]
    #
#
# Response to Arbitrary Applied Force 
def ode_arbitrary_force(time, force, period, gamma):
    """
    fn    = frequency
    gamma = damping ratio
    force     = force
    """
    omegan, two_domegan, omn2, _alpha, _beta = motion_constants(period, gamma)
    # check if numpy module
    try:
        import numpy as np
        # check if scipy module
        try:
            #import fem2ufo as f2u
            import scipy as sp
            x = drfr_f(force, time, omegan, gamma)
            return [time, x[0], x[1], x[2]]
        # use runge kutta 4 
        except ImportError:
            def F(f, x, y):
                F = np.zeros((2))
                F[0] = y[1] 
                F[1] = f - two_domegan * y[1] - omn2 * y[0]
                return F
            #
            X,Y = integrate_rk4(F, force, time)
            Z = np.array(force - two_domegan * Y[:,1] - omn2 * Y[:,0])
            #
            return [X, Y[:,0], Y[:,1], Z]
    # use runge kutta 4 no optimized
    except ImportError:
        x = rk4(force, time, two_domegan, omn2)
        x[2] =  [x[2][i] + force[i] for i in range(len(x[2]))]
        return [time, x[0], x[1], x[2]]
    #
    #    
#
#
# Solvers
#
# Runge Kutta 4
def rk4(f, t, two_domegan, omn2):
    """
    """
    #
    x = [[0], [0], [0]]
    nt = len(f) - 1
    for i in range(nt):
        h = t[i+1] - t[i]
        hd2 = h/2.0
        #
        a1 = f[i]
        a2 = (f[i+1] + f[i]) / 2.0
        a3 = a2
        a4 = f[i+1]
        #
        X1 = x[0][i]
        Y1 = x[1][i]      
        F1 = a1 - two_domegan * Y1 - omn2 * X1
        #
        X2= X1 + Y1 * hd2
        Y2= Y1 + F1 * hd2        
        F2 = a2 - two_domegan * Y2 - omn2 * X2
        #
        X3= X1 + Y2 * hd2
        Y3= Y1 + F2 * hd2        
        F3= a3 - two_domegan * Y3 - omn2 * X3
        #
        X4= X1 + Y3 * h
        Y4= Y1 + F3 * h
        F4= a4 - two_domegan * Y4 - omn2 * X4
        #
        x[0].append(X1 + (h/6.)*( Y1 + 2*Y2 + 2*Y3 + Y4 ))   # relative displacement
        x[1].append(Y1 + (h/6.)*( F1 + 2*F2 + 2*F3 + F4 ))   # relative velocity
        x[2].append(-two_domegan * x[1][i] - omn2 * x[0][i]) # acceleration
    #
    return x
#
#
# numpy module
#
# Runge_kuta4 numpy
def integrate_rk4(F, f, t):
    """
    """
    import numpy as np
    #
    def run_kut4(F, x, y):
        K0 = h * F(a1, x, y)
        K1 = h * F(a2, x + h/2.0, y + K0/2.0)
        K2 = h * F(a3, x + h/2.0, y + K1/2.0)
        K3 = h * F(a4, x + h, y + K2)
        return (K0 + 2.0*K1 + 2.0*K2 + K3)/6.0
    
    # Start of Integration
    x = 0.0
    # Initial Value of {y}
    y = np.array([0.0, 0.0])
    # End of Integration
    xStop = len(f) - 1
    X = [x]
    Y = [y]
    #
    for i in range(xStop):
        # Step size
        h = t[i+1] - t[i]
        #
        a1 = f[i]
        a2 = (f[i+1] + f[i]) / 2.0
        a3 = a2
        a4 = f[i+1]
        #
        y = y + run_kut4(F, x, y)
        x = x + h
        #
        X.append(x)
        Y.append(y)
    #
    return np.array(X), np.array(Y)
#
#
# scipy &  numpy module
#
# Alternative methods for SDoF Arbitrary Applied Force
#
def drfr_f(f, t, omegan, gamma):
    """
    Digital Recursive Filtering Relationship ODE Solver
    Arbitrary Applied Force - Time Domain Response for a given SDOF System
    """
    from scipy.signal import lfilter
    from numpy import zeros
    
    #
    num = len(f)
    dur = t[num-1]-t[0]
    dt = dur/float(num-1)
    
    #
    ac=zeros(3,'f')
    bc=zeros(3,'f')
    
    omegad = omegan * sqrt(1. - gamma**2)
    domegan = gamma * omegan
    
    cosd = cos(omegad * dt)
    sind = sin(omegad * dt)
    
    domegadt = domegan * dt
    
    eee1 = exp(-domegadt)
    eee2 = exp(-2. * domegadt)
    
    ecosd = eee1 * cosd
    esind = eee1 * sind 
    
    omeganT = omegan * dt
    phi = (2 * gamma)**2 - 1.0
    
    #
    ac[0] = 1.0
    ac[1] = -2.0 * ecosd
    ac[2] = eee2
    #
    # displacement response
    DD1 = (omegan / omegad) * phi
    DD2 = 2 * DD1
    MD = (omegan**3 * dt)
    bc[0] = ((2*gamma*(ecosd-1) + DD1*esind + omeganT) / MD)
    bc[1] = ((-2*omeganT*ecosd + 2*gamma*(1.0 - eee2) - DD2*esind) / MD)
    bc[2] = (((2*gamma + omeganT)*eee2 + (DD1*esind - 2.0*gamma*ecosd)) / MD)
    
    disp_resp = lfilter(bc, ac, f)
    #   
    # velocity response
    VV1 = gamma * omegan / omegad
    VD = (omegan**2 * dt)
    bc[0] = ((-ecosd + VV1*esind) + 1) / VD
    bc[1] = (eee2 - 2*VV1*esind - 1) / VD
    bc[2] = (ecosd + VV1*esind - eee2) / VD
    
    vel_resp = lfilter(bc, ac, f)
    #    
    # acceleration response
    af1 = esind / (omegad*dt)
    bc[0] = af1
    bc[1] = -2 * af1
    bc[2] = af1
    
    acc_resp = lfilter(bc, ac, f)
    
    return [disp_resp, vel_resp, acc_resp]
#
#
#
# Alternative methods for SDoF base montion (not used)
#
def z_method(t1, b,  T, gamma, grav):
    """
    Input:
    a  : Time(sec)
    b  : Accel(G)
    T  : period (sec)
    damp : damping ratio
    grav : gravity
    Output
    x : relative displacement
    y : relative velocity
    accel : Acceleration
    """    
    #
    omegan, two_domegan, omn2, _alpha, _beta = motion_constants(T, gamma)  
    #
    nt = len(b) - 1
    
    _a0 = lambda h : (exp(-_alpha*h) * sin(_beta * h)) / _beta
    _b = lambda h :2 * exp(-_alpha*h) * cos(_beta * h)
    _c = lambda h :exp(-2*_alpha*h)
    _gamma2 = lambda h : ((1 - _b(h) + _c(h)) / (_a0(h) * h * omn2))
    #e1 = lambda h : -_a0(h)/h
    #e2 = lambda h : 2*_a0(h)/h

    _x = [[0, 0], [0, 0], [0, 0]]

    for i in range(1, nt):
        #
        h = t1[i]-t1[i-1]
        #
        
        _x[0].append(_b(h) * _x[0][i] - _c(h) * _x[0][i-1] 
                     - _a0(h) * h * b[i])
        
        _x[1].append(_b(h) * _x[1][i] - _c(h) * _x[1][i-1] 
                     - _gamma2(h) * _a0(h) * (b[i] - b[i-1]))
        
        #_x[2].append(_b(h) * _x[2][i] - _c(h) * _x[2][i-1] 
        #             - _gamma2(h) * _a0(h) * (b[i+1] - 2*b[i] + b[i-1]))
        
        #_x[2].append(_b(h) * _x[2][i] - _c(h) * _x[2][i-1] 
        #             + (e1(h) * b[i+1] + e2(h) * b[i] + e1(h) * b[i-1]))
        _x[2].append((two_domegan * _x[1][i] - omn2 * _x[0][i]) / grav)
    #
    #
    #
    return t1, _x[0], _x[1], _x[2]
    #
#
def colvin_morris_method(t1, b,  T, gamma, grav):
    """
    """
    omegan, two_domegan, omn2, _alpha, _beta = motion_constants(T, gamma)   
    #
    nt = len(b) - 1
    h = lambda i: t1[i+1]-t1[i]
    C1 = lambda h: 2 * exp(-gamma*omegan*h) * cos(_beta*h)
    C2 = lambda h: - exp(-2*gamma*omegan*h)
    C3 = lambda h: (h / _beta) * exp(-gamma*omegan*h) * sin(_beta*h)
    
    _x = [0, 0]

    for i in range(1, nt):
        _x.append(C1(h(i)) * _x[i] 
                     + C2(h(i)) * _x[i-1] 
                     + C3(h(i)) * b[i])
    
    #x = [C1(h(i)) * _x[0][i] + C2(h(i)) * _x[0][i-1] + C3(h(i)) * b[i] for i in range(1, nt)]
    #
    return t1, _x
#
def colvin_morris_method_full(t1, b,  T, gamma, grav):
    """
    """
    omegan, two_domegan, omn2, _alpha, _beta = motion_constants(T, gamma)   
    #
    nt = len(b) - 1

    h = lambda i: t1[i+1]-t1[i]
    C1 = lambda h: 2 * exp(-gamma*omegan*h) * cos(_beta*h)
    C2 = lambda h: - exp(-2*gamma*omegan*h)
    C3 = lambda h: (h / _beta) * exp(-gamma*omegan*h) * sin(_beta*h)

    _x = [[0, 0], [0, 0], [0, 0]]

    for i in range(1, nt):
        _x[0].append(C1(h(i)) * _x[0][i] + C2(h(i)) * _x[0][i-1] + C3(h(i)) * b[i])
        _x[1].append(C1(h(i)) * _x[1][i] + C2(h(i)) * _x[1][i-1] + C3(h(i)) * (b[i] - b[i-1]) / h(i))
        _x[2].append((two_domegan * _x[1][i] - omn2 * _x[0][i]) / grav)
    #
    return t1, _x[0], _x[1], _x[2]
#
#
#
def interpolate(x, y, step):
    """
    """
    # check if numpy & scipy modules
    try:
        import fe2ufo as f2u
        import numpy as np
        from scipy import interpolate
        
        f = interpolate.interp1d(x, y)
        xnew = np.arange(0, x[-1], step)
        ynew = f(xnew)
    
    except ImportError:
        f = Interpolate(x, y)
        time = 0
        xnew = [0]
        ynew = [0]
        while time < x[-1]:
            time += step
            ynew.append(f[time])
            xnew.append(time)
    #
    return xnew, ynew
#
class Interpolate(object):
    
    def __init__(self, x_list, y_list):
        
        if any([y - x <= 0 for x, y in zip(x_list, x_list[1:])]):
            raise ValueError("x_list must be in strictly ascending order!")
        
        x_list = self.x_list = list(map(float, x_list))
        y_list = self.y_list = list(map(float, y_list))
        
        intervals = zip(x_list, x_list[1:], y_list, y_list[1:])
        self.slopes = [(y2 - y1)/(x2 - x1) for x1, x2, y1, y2 in intervals]
    
    def __getitem__(self, x):
        
        if x <= self.x_list[0]:
            return self.y_list[0]
        
        elif x >= self.x_list[-1]:
            return self.y_list[-1]
        
        else:
            i = bisect_left(self.x_list, x) - 1
            return self.y_list[i] + self.slopes[i] * (x - self.x_list[i])
#
#
class NewmarkBeta():
    '''
    Evaluates the response spectrum using the Newmark-Beta methodology
    '''
    
    def __init__(self, acceleration, periods, gamma, time_step):
        """
        """
        self.acceleration = acceleration
        self.periods = np.asarray(periods)
        self.damping = gamma
        self.d_t = time_step
        self.num_steps = len(self.acceleration)
        self.num_per = len(self.periods)
    
    def evaluate(self, output=''):
        '''
        Evaluates the response spectrum
        
        :returns:
            Response Spectrum - Dictionary containing all response spectrum
                                data
                'Time' - Time (s)
                'Acceleration' - Acceleration Response Spectrum (cm/s/s)
                'Velocity' - Velocity Response Spectrum (cm/s)
                'Displacement' - Displacement Response Spectrum (cm)
                'Pseudo-Velocity' - Pseudo-Velocity Response Spectrum (cm/s)
                'Pseudo-Acceleration' - Pseudo-Acceleration Response Spectrum 
                                       (cm/s/s)

            Time Series - Dictionary containing all time-series data
                'Time' - Time (s)
                'Acceleration' - Acceleration time series (cm/s/s)
                'Velocity' - Velocity time series (cm/s)
                'Displacement' - Displacement time series (cm)
                'PGA' - Peak ground acceleration (cm/s/s)
                'PGV' - Peak ground velocity (cm/s)
                'PGD' - Peak ground displacement (cm)

            accel - Acceleration response of Single Degree of Freedom Oscillator 
            vel - Velocity response of Single Degree of Freedom Oscillator 
            disp - Displacement response of Single Degree of Freedom Oscillator 
        '''
        omega = 2 * np.pi / self.periods
        cval = self.damping * 2. * omega
        kval = (2 * np.pi / self.periods)**2

        # Perform Newmark - Beta integration
        accel, vel, disp, a_t = self._newmark_beta(omega, cval, kval)

        self.response_spectrum = {'period': self.periods,
                                  'acceleration': np.max(np.fabs(a_t), axis=0),
                                  'velocity': np.max(np.fabs(vel), axis=0),
                                  'displacement': np.max(np.fabs(disp), axis=0)}

        self.response_spectrum['Pseudo-Velocity'] = omega * self.response_spectrum['displacement']

        self.response_spectrum['Pseudo-Acceleration'] = omega**2 * self.response_spectrum['displacement']

        #time_series = {
        #    'Time-Step': self.d_t,
        #    'Acceleration': self.acceleration,
        #    'Velocity': self.velocity,
        #    'Displacement': self.displacement,
        #    'PGA': np.max(np.fabs(self.acceleration)),
        #    'PGV': np.max(np.fabs(self.velocity)),
        #    'PGD': np.max(np.fabs(self.displacement))}

        #return self.response_spectrum, time_series, accel, vel, disp
        if 'spectrum' in output:
            return self.response_spectrum
        #
        return a_t, vel, disp
        #return accel, vel, disp


    def _newmark_beta(self, omega, cval, kval):
        '''
        Newmark-beta integral

        :param numpy.ndarray omega:
            Angular period - (2 * pi) / T
        :param numpy.ndarray cval:
            Damping * 2 * omega
        :param numpy.ndarray kval:
            ((2. * pi) / T) ** 2.

        :returns:
            accel - Acceleration time series
            vel - Velocity response of a SDOF oscillator
            disp - Displacement response of a SDOF oscillator
            a_t - Acceleration response of a SDOF oscillator
        '''

        # Pre-allocate arrays
        accel = np.zeros([self.num_steps, self.num_per], dtype=float)
        vel = np.zeros([self.num_steps, self.num_per], dtype=float)
        disp = np.zeros([self.num_steps, self.num_per], dtype=float)
        a_t = np.zeros([self.num_steps, self.num_per], dtype=float)

        # Initial line
        accel[0, :] = ((-self.acceleration[0] - (cval * vel[0, :])) 
                       - (kval * disp[0, :]))
        
        a_t[0, :] = accel[0, :] + accel[0, :]

        for j in range(1, self.num_steps):
            disp[j, :] = (disp[j-1, :] + (self.d_t * vel[j-1, :]) + 
                          (((self.d_t ** 2.) / 2.) * accel[j-1, :]))

            accel[j, :] = ((1./ (1. + self.d_t * 0.5 * cval)) 
                           * (-self.acceleration[j] - kval * disp[j, :] - 
                              cval * (vel[j-1, :] + (self.d_t * 0.5) * accel[j-1, :])))

            vel[j, :] = vel[j - 1, :] + self.d_t * (0.5 * accel[j - 1, :] + 0.5 * accel[j, :])

            a_t[j, :] = self.acceleration[j] + accel[j, :]

        return accel, vel, disp, a_t

#
#
class NigamJennings():
    """
    Evaluate the response spectrum using the algorithm of Nigam & Jennings
    (1969)

    In general this is faster than the classical Newmark-Beta method, and
    can provide estimates of the spectra at frequencies higher than that
    of the sampling frequency.
    """
    
    def __init__(self, acceleration, periods, gamma, time_step):
        """
        """
        self.acceleration = acceleration
        self.periods = np.array(periods)
        self.damping = gamma
        self.d_t = time_step
        self.num_steps = len(self.acceleration)
        self.num_per = len(self.periods)

    def evaluate(self, output=''):
        """
        Define the response spectrum
        """
        omega = 2 * np.pi / self.periods
        omega2 = omega**2
        omega3 = omega**3
        omega_d = omega * np.sqrt(1.0 - (self.damping**2))

        const = {'f1': (2.0 * self.damping) / (omega3 * self.d_t),
                 'f2': 1.0 / omega2,
                 'f3': self.damping * omega,
                 'f4': 1.0 / omega_d}

        const['f5'] = const['f3'] * const['f4']
        const['f6'] = 2.0 * const['f3']
        const['e'] = np.exp(-const['f3'] * self.d_t)
        const['s'] = np.sin(omega_d * self.d_t)
        const['c'] = np.cos(omega_d * self.d_t)
        const['g1'] = const['e'] * const['s']
        const['g2'] = const['e'] * const['c']
        const['h1'] = (omega_d * const['g2']) - (const['f3'] * const['g1'])
        const['h2'] = (omega_d * const['g1']) + (const['f3'] * const['g2'])

        x_a, x_v, x_d = self._get_time_series(const, omega2)

        self.response_spectrum = {'period': self.periods,
                                  'acceleration': np.amax(x_a, axis=0),
                                  'velocity': np.amax(x_v, axis=0),
                                  'displacement': np.amax(x_d, axis=0)}

        self.response_spectrum['Pseudo-Velocity'] =  omega * self.response_spectrum['displacement']

        self.response_spectrum['Pseudo-Acceleration'] =  omega**2 * self.response_spectrum['displacement']

        #time_series = {
        #    'Time-Step': self.d_t,
        #    'Acceleration': self.acceleration,
        #    'Velocity': self.velocity,
        #    'Displacement': self.displacement,
        #    'PGA': np.max(np.fabs(self.acceleration)),
        #    'PGV': np.max(np.fabs(self.velocity)),
        #    'PGD': np.max(np.fabs(self.displacement))}

        #return self.response_spectrum, time_series, x_a, x_v, x_d
        if 'spectrum' in output:
            return self.response_spectrum
        #
        return x_a, x_v, x_d


    def _get_time_series(self, const, omega2):
        """
        Calculates the acceleration, velocity and displacement time series for
        the SDOF oscillator

        :param dict const:
            Constants of the algorithm

        :param np.ndarray omega2:
            Square of the oscillator period

        :returns:
            x_a = Acceleration time series
            x_v = Velocity time series
            x_d = Displacement time series
        """
        x_d = np.zeros([self.num_steps, self.num_per], dtype=float)
        x_v = np.zeros_like(x_d)
        x_a = np.zeros_like(x_d)

        for k in range(self.num_steps - 1):
            dug = self.acceleration[k + 1] - self.acceleration[k]
            z_1 = const['f2'] * dug
            z_2 = const['f2'] * self.acceleration[k]
            z_3 = const['f1'] * dug
            z_4 = z_1 / self.d_t
            
            try:
                1.0/k
                b_val = x_d[k - 1, :] + z_2 - z_3
                a_val = (const['f4'] * x_v[k - 1, :] 
                         + const['f5'] * b_val 
                         + const['f4'] * z_4)                
            except ZeroDivisionError:
                b_val = z_2 - z_3
                a_val = (const['f5'] * b_val) + (const['f4'] * z_4)                

            #if k == 0:
            #    b_val = z_2 - z_3
            #    a_val = (const['f5'] * b_val) + (const['f4'] * z_4)
            #else:    
            #    b_val = x_d[k - 1, :] + z_2 - z_3
            #    a_val = (const['f4'] * x_v[k - 1, :] 
            #             + const['f5'] * b_val 
            #             + const['f4'] * z_4)

            x_d[k, :] = (a_val * const['g1']) + (b_val * const['g2']) + z_3 - z_2 - z_1
            x_v[k, :] = (a_val * const['h1']) - (b_val * const['h2']) - z_4
            x_a[k, :] = (-const['f6'] * x_v[k, :]) - (omega2 * x_d[k, :])

        return x_a, x_v, x_d
#