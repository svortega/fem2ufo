#
# # Copyright (c) 2009-2017 fem2ufo
#

# Python stdlib imports


#
# Fast Fourier transform
#
# pure python
def dft(x, inverse=False, verbose=False) :
    """
    """
    t = time.clock()
    N = len(x)
    X =[0] * N
    inv = -1 if not inverse else 1
    
    if N == 1 :
        X[0] = x[0]
    
    elif N == 2 :
        X[0] = x[0] + x[1]
        X[1] = x[0] - x[1]
    
    elif N == 4 :
        a = x[0] + x[2]
        b = x[0] - x[2]
        c = x[1] + x[3]
        d = x[1] - x[3]
        X[0] = a + c
        X[1] = b + inv * 1j * d 
        X[2] = a - c
        X[3] = b - inv * 1j * d 
        
        if inverse :
            X = [k/N for k in X]
    else : 
        twiddles = [math.e**(inv*2j*pi*k/N) for k in range(N//2+1)]
        
        for k in range(N//2+1, N) :
            twiddles += [twiddles[N-k].conjugate()]
        
        for k in range(N) :
            for n in range(N) :
                X[k] += x[n] * twiddles[n * k % N]
            
            if inverse : 
                X[k] /= N
    
    t = time.clock() - t 
    if verbose :
        print("Computed","an inverse" if inverse else "a","DFT of size"),N,
        print("in",t,"sec.")
    
    return X 
#
# numpy
def FFT_vectorized(x):
    """
    A vectorized, non-recursive version of the Cooley-Tukey FFT
    """
    import numpy as np
    
    x = np.asarray(x, dtype=float)
    N = x.shape[0]

    if np.log2(N) % 1 > 0:
        raise ValueError("size of x must be a power of 2")

    # N_min here is equivalent to the stopping condition above,
    # and should be a power of 2
    N_min = min(N, 32)
    
    # Perform an O[N^2] DFT on all length-N_min sub-problems at once
    n = np.arange(N_min)
    k = n[:, None]
    M = np.exp(-2j * np.pi * n * k / N_min)
    X = np.dot(M, x.reshape((N_min, -1)))

    # build-up each level of the recursive calculation all at once
    while X.shape[0] < N:
        X_even = X[:, :X.shape[1] / 2]
        X_odd = X[:, X.shape[1] / 2:]
        factor = np.exp(-1j * np.pi * np.arange(X.shape[0])
                        / X.shape[0])[:, None]
        X = np.vstack([X_even + factor * X_odd,
                       X_even - factor * X_odd])

    return X.ravel()

#
def fft(x):
    """
    """
    from numpy.fft import fft
    X = fft(x)
    return X
#
#