﻿#
# Copyright 2001 softSurfer, 2012 Dan Sunday
# This code may be freely used and modified for any purpose
# providing that this copyright notice is included with it.
# SoftSurfer makes no warranty for this code, and cannot be held
# liable for any real or imagined damage resulting from its use.
# Users of this code must verify correctness for their application.
#
#
class ChainHullAlgorithm:
	""" 
	Andrew's monotone chain 2D convex hull algorithm
	"""
	def __init__(self, P) -> None:
		"""
		Input:  P[] = an array of 2D points
					  presorted by increasing x and y-coordinates
		
		Assume that a class is already given for the object:
		Point with coordinates [x, y, z]
		"""
		self.P = P
		self.n = len(P)
	#
	#===================================================================
    #
	@staticmethod
	def isLeft(P0, P1, P2) -> float: 
		"""
		isLeft(): tests if a point is Left|On|Right of an infinite line.
		
		Input:  three points P0, P1, and P2
		Return: >0 for P2 left of the line through P0 and P1
				=0 for P2 on the line
				<0 for P2 right of the line
		See: Algorithm 1 on Area of Triangles
		"""
		return ((P1.coordinates.x - P0.coordinates.x) * (P2.coordinates.y - P0.coordinates.y) 
		        - (P2.coordinates.x - P0.coordinates.x) * (P1.coordinates.y - P0.coordinates.y))
	#
	#===================================================================
    #
	def chainHull_2D(self):
		"""
		Return: H[] = an array of the convex hull vertices (max is n)
		"""
		# the output array H[] will be used as the stack
		bot: int = 0 # indices for bottom and top of the stack
		top: int = -1
		self.H:list = [None for x in range(self.n)]

		# Get the indices of points with min x-coord and min|max y-coord
		minmin: int = 0
		minmax: int
		xmin: float = self.P[0].coordinates.x
		
		for i in range(1, self.n - 1):
			if self.P[i].coordinates.x != xmin:
				break
		#
		minmax = i-1
		# degenerate case: all x-coords == xmin
		if minmax == self.n-1 :
			top += 1
			self.H[top] = self.P[minmin]
			# a nontrivial segment
			if self.P[minmax].coordinates.y != self.P[minmin].coordinates.y : 
				top += 1
				self.H[top] = self.P[minmax]
			# add polygon endpoint
			top += 1
			self.H[top] = self.P[minmin]
			return H # top+1
		#
		# Get the indices of points with max x-coord and min|max y-coord
		maxmax: int = self.n - 1
		xmax: float = self.P[self.n - 1].coordinates.x
		for i in range(self.n-2, 0, -1):
			if self.P[i].coordinates.x != xmax:
				break
		#
		maxmin:int = i+1
		# Compute the lower hull on the stack H
		top += 1
		self.H[top] = self.P[minmin] # push  minmin point onto stack
		i = minmax
        #ORIGINAL LINE: while (++i <= maxmin)
		i += 1
		while i <= maxmin:
			#i += 1
			# the lower line joins P[minmin]  with P[maxmin]
			if self.isLeft(self.P[minmin], self.P[maxmin], self.P[i]) >= 0 and i < maxmin:
				i += 1
				continue # ignore P[i] above or on the lower line

			while top > 0 : # there are at least 2 points on the stack
				# test if  P[i] is left of the line at the stack top
				if self.isLeft(self.H[top-1], self.H[top], self.P[i]) > 0 :
					break # P[i] is a new hull  vertex
				else:
					top -= 1 # pop top point off  stack
			# push P[i] onto stack
			top += 1
			self.H[top] = self.P[i]
			i += 1
        #
		# Next, compute the upper hull on the stack H above  the bottom hull
		if maxmax != maxmin : # if  distinct xmax points
			top += 1
			self.H[top] = self.P[maxmax] # push maxmax point onto stack
		# the bottom point of the upper hull stack
		bot = top 
		i = maxmin
        #ORIGINAL LINE: while (--i >= minmax)
		i -= 1
		while i >= minmax :
			#i -= 1
			# the upper line joins P[maxmax]  with P[minmax]
			if self.isLeft(self.P[maxmax], self.P[minmax], self.P[i]) >= 0 and i > minmax :
				i -= 1
				continue # ignore P[i] below or on the upper line
			#
			# at least 2 points on the upper stack
			while top > bot : 
				# test if  P[i] is left of the line at the stack top
				if self.isLeft(self.H[top-1], self.H[top], self.P[i]) > 0 :
					break # P[i] is a new hull  vertex
				else:
					top -= 1 # pop top point off  stack
			# push P[i] onto stack
			top += 1
			self.H[top] = self.P[i]
			i -= 1
		# push  joining endpoint onto stack
		if minmax != minmin :
			top += 1
			self.H[top] = self.P[minmin]
		#
		return self.H # top+1
	#
	@property
	def area(self) -> float:
		"""Find area of polygon via Exterior algebra"""
		#
		vertices = self.chainHull_2D()
		n = len(vertices) # of corners
		a = 0.0
		for i in range(n):
			j = (i + 1) % n
			a += abs(vertices[i].coordinates.x * vertices[j].coordinates.y 
			         - vertices[j].coordinates.x * vertices[i].coordinates.y)
		result = a / 2.0
		return result
#End Class
#