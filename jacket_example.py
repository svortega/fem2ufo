#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
#
# EXAMPLE
#
# Import fem2ufo module
#
import fem2ufo as f2u
#
#
# ----------------------------------------------------------------------------------
#                                 FE Model Manipulation
# ----------------------------------------------------------------------------------
#

# Initiate model object
f2u_model = f2u.SESAM_model()

# Define path to the working directory
# mac
#path = '/Volumes/ANALYSIS/Python/fem2ufo_3/examples/jacket'
# windows
path = r'D:\svort\Documents\python\f2u_examples\jacket'
path = r'D:\python\f2u3\examples\jacket'
path = r'D:\Python\f2u_examples\jacket'
#path = r'C:\Temp\Temp\python\f2u30\examples\jacket'
#path = r"/media/chava/Backup/python/f2u_examples/jacket"
#path = r"C:\Temp\Tools\python\f2u30\examples\jacket"

f2u_model.directory_path(path)

# Define input units (mandatory)
f2u_model.units(force='N', length='mm', mass='kg')

# input moddel data
f2u_model.geometry('jacket_T1.fem') # mandatory
f2u_model.loading('jacket_L1.fem')
f2u_model.foundation('jacket_GENSOD.LIS')
f2u_model.metocean('jacket_wajac.inp')

# updating material by number
f2u_model.update_material(number=1, Fy=4.0000E+08, Young=0)

# updating material by name
f2u_model.update_material(name='MAT1', Fy=6.0000E+08, Young=2)

# adding new material
f2u_model.add_material(name='mat300', 
                       Fy=3000,
                       Young=20100,
                       poisson=0.29,
                       rho=8000)

# converting load to mass
#f2u_model.load_to_mass('CASE101_', factor=0.8, option='member')

#f2u_model.load_to_mass('CASE103_',  'CASE104_', 'CASE105_',
#                   'CASE106_',  'CASE107_', 'CASE108_',
#                   'CASE109_', factor=0.95, option='member')

f2u_model.load_to_mass(1, 2, 100, factor=0.7, option='node')

f2u_model.load_to_mass(101, factor=0.9, option='member')

# deleting elements
f2u_model.delete_element(15, 'Bm152')
f2u_model.delete_element_group('feast')

# load numbering is redefine in consecutive manner
f2u_model.load_fill_gap()


#
#
#
# ----------------------------------------------------------------------------------
#                                  Conversion Process
# ----------------------------------------------------------------------------------
#
#

usfos = f2u.USFOS_conversion()
# input concept model
usfos.model(f2u_model)

# Analysis type
usfos.analysis('dynamic')

# define mean material by increasing all material's Fy by 15%
usfos.set_mean_material(15)

# define control displacement
usfos.set_control_displacement('x', 'y', 'z', factor=2.0)

usfos.element_control('Bm1', 100, end=1)
usfos.element_control('Bm2', 20, end=2)

usfos.node_control(100)

# Element manipulation

# Linear
usfos.element_assign_linear(20, 'Bm100')

usfos.element_group_assign_linear('PILESAMU',
                                  'PSLEEV')

# Non structural
usfos.element_assign_nonstructural(20)

usfos.element_group_assign_nonstructural('RISER_2')

# Jelly
usfos.element_group_assign_jelly('RISER_1', factor=10000)

# reinforced
usfos.element_group_assign_reinforce('Topside', factor=10000)

# Fracture
usfos.element_group_assign_fracture('JACKET',
                                    'JACKET2',
                                    'TOPSIDE',
                                    13, 14, 37,
                                    strain=15)

# set join definition
usfos.set_join_rule('msl')

# set hydrostatic pressure
usfos.set_hydropressure(surface=600,
                        mudline=-24470,
                        fluid=1.025E-9)

usfos.element_group_assign_hydropressure('nonFLOOD',
                                         18, 15)

usfos.element_assign_hydropressure(677)
#
# set units output
usfos.units(force='kN', length='m')
# print ufo model file
usfos.print_model()
# print ufo control file
usfos.print_control()
#
#
print('')
print('------------------------ END -----------------------')
#ExitProg = input('Press Enter Key To Continue')